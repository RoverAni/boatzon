import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import VerticalTabs from "../../components/tabs/vertical-tabs";
import {
  BG_LightGREY_COLOR,
  GREY_VARIANT_2,
  FONTGREY_COLOR_Dark,
  Question_LightGrey_Icon,
  Tick_Bulletpoint_Icon_Green,
  WHITE_COLOR,
  Close_Light_Grey_Icon_Varient,
  GREY_VARIANT_3,
  GREY_VARIANT_6,
} from "../../lib/config";
import AnnualPlans from "./annual-plans";

class MembershipPlansPage extends Component {
  state = {
    openPage: false,
  };

  componentDidMount() {
    this.setState({
      openPage: true,
    });
  }
  render() {
    const Annual = <AnnualPlans />;
    const Monthly = <h1>Monthly</h1>;
    return (
      <Wrapper>
        {this.state.openPage ? (
          <div className="membershipPlansPage">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto">
                <div className="col-12 p-0 py-5">
                  <VerticalTabs
                    planHeading="Choose a plan that fits your business needs."
                    saveMsg="* Save with annual billing"
                    tabs={[
                      {
                        label: `Annual`,
                      },
                      {
                        label: `Monthly`,
                      },
                    ]}
                    tabcontent={[{ content: Annual }, { content: Monthly }]}
                  />
                </div>
                <div className="col-12 p-0">
                  <div className="row m-0">
                    <div className="col-3 planTitle py-1">
                      Marketing & Advertising
                    </div>
                    <div className="col-3 py-1 iconSec"></div>
                    <div className="col-3 py-1 iconSec"></div>
                    <div className="col-3 py-1"></div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Custom URL{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Premium Directory Listing{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Credit Card Processing Account{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Live Video with Customers{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Bulk Product / Boat Upload{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Close_Light_Grey_Icon_Varient}
                        className="closeIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Close_Light_Grey_Icon_Varient}
                        className="closeIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Shipping & Label Management{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Close_Light_Grey_Icon_Varient}
                        className="closeIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Close_Light_Grey_Icon_Varient}
                        className="closeIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                </div>

                <div className="col-12 p-0 py-5">
                  <div className="row m-0">
                    <div className="col-3 planTitle py-1">Lead Management</div>
                    <div className="col-3 py-1 iconSec"></div>
                    <div className="col-3 py-1 iconSec"></div>
                    <div className="col-3 py-1"></div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Messaging & Client Dashboard{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Project Match Leads{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Advice Match Leads{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 text-center iconSec py-1">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center iconSec">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                  <div className="row m-0 featureRow align-items-center">
                    <div className="col-3 planFeature pr-0 py-1">
                      Product & Boat Leads{" "}
                      <img
                        src={Question_LightGrey_Icon}
                        className="questionIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 iconSec text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 iconSec text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                    <div className="col-3 py-1 text-center">
                      <img
                        src={Tick_Bulletpoint_Icon_Green}
                        className="tickIcon"
                      ></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .membershipPlansPage {
              background: ${BG_LightGREY_COLOR};
            }
            .planTitle {
              font-size: 0.732vw;
              margin: 0;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              font-weight: 500 !important;
            }
            .featureRow {
              position: relative;
              cursor: pointer;
            }
            .featureRow:hover {
              background: ${GREY_VARIANT_6};
            }
            .planFeature {
              font-size: 0.805vw;
              margin: 0;
              font-family: "Open Sans-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .questionIcon {
              position: absolute;
              right: 0.732vw;
              width: 0.951vw;
              top: 50%;
              transform: translate(0, -50%);
            }
            .iconSec {
              border-right: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .tickIcon {
              width: 0.878vw;
            }
            .closeIcon {
              width: 0.732vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default MembershipPlansPage;
