import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  GREY_VARIANT_8,
  GREY_VARIANT_1,
  BLACK_COLOR,
  FONTGREY_COLOR_Dark,
  BG_LightGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_2,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";

class AnnualPlans extends Component {
  render() {
    return (
      <Wrapper>
        <div className="plansPage col-12 p-0">
          <div className="row m-0 align-items-center justify-content-center">
            <div className="col-4 pl-0">
              <div className="planCard text-center">
                <h6 className="cardHeading">For Professionals</h6>
                <p className="planFor">
                  Mechanics, Upholstery, Detailing, Electrical, etc
                </p>
                <h2 className="planCost">Free</h2>
                <p className="line"></p>
                <p className="trialPeriod">1 year trial</p>
                <p className="monthlyCharges">then $29.99/mo</p>
                <div className="signUp_btn">
                  <ButtonComp>Sign Up</ButtonComp>
                </div>
              </div>
            </div>
            <div className="col-4 pl-0">
              <div className="planCard text-center">
                <h6 className="cardHeading">Small Boat Dealer</h6>
                <p className="planFor">Less than 50 Boats</p>
                <h2 className="planCost">Free</h2>
                <p className="line"></p>
                <p className="trialPeriod">1 year trial</p>
                <p className="monthlyCharges">then $99/mo</p>
                <div className="signUp_btn">
                  <ButtonComp>Sign Up</ButtonComp>
                </div>
              </div>
            </div>
            <div className="col-4 pl-0">
              <div className="planCard text-center">
                <h6 className="cardHeading">Large Boat Dealer</h6>
                <p className="planFor">More than 50 Boats</p>
                <h2 className="planCost">Free</h2>
                <p className="line"></p>
                <p className="trialPeriod">1 year trial</p>
                <p className="monthlyCharges">then $299/mo</p>
                <div className="signUp_btn">
                  <ButtonComp>Sign Up</ButtonComp>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .planCard {
              background: ${WHITE_COLOR};
              padding: 1.464vw;
              box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
              border-radius: 0.219vw;
              width: 100%;
              height: 21.229vw;
              position: relative;
            }
            .cardHeading {
              font-size: 1.171vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_8};
              font-weight: 600;
              padding: 0.585vw 0 0 0;
            }
            .planFor {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              height: 2.489vw;
            }
            .planCost {
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_8};
              font-weight: 600;
              margin: 0 0 0.366vw 0;
              font-size: 2.342vw;
            }
            .line {
              border-bottom: 0.0732vw solid ${BLACK_COLOR};
              width: 3.66vw;
              margin: 0 auto 0.878vw auto;
            }
            .trialPeriod,
            .monthlyCharges {
              font-size: 0.878vw;
              margin: 0;
              font-family: "Open Sans-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .signUp_btn {
              position: absolute;
              bottom: 2.196vw;
              left: 50%;
              transform: translate(-50%);
            }
            :global(.signUp_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.signUp_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.signUp_btn button:focus),
            :global(.signUp_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.signUp_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default AnnualPlans;
