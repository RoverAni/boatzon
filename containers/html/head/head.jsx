// Main React Components
import React from "react";
import Head from "next/head";

// asstes and colors
import { APP_NAME, OG_DESC, OG_IMAGE, OG_Caption } from "../../../lib/config";

const CustomHead = (props) => {
  return (
    <Head>
      <meta charSet="UTF-8" />
      <title>{props.ogTitle || APP_NAME}</title>
      <meta
        property="og:description"
        content={props.ogDescription || OG_DESC}
      />
      {/* <meta property="og:url" content={props.url || defaultOGURL} /> */}
      <meta property="og:title" content={props.ogDesc || OG_Caption} />
      <meta property="og:image" content={props.ogImage || OG_IMAGE} />
      <meta property="og:image:width" content="512" />
      <meta property="og:image:height" content="512" />
    </Head>
  );
};

export default CustomHead;
