import React from "react";

import { GuidelinesData } from "../../fixtures/guidelines/guidelines";
import Wrapper from "../../hoc/Wrapper";

export const Guidelines = props => {
  return (
    <Wrapper>
      <div className="guideLinesCont">
        {GuidelinesData &&
          GuidelinesData.map((item, guideLineIndex) => (
            <p key={"guide-line-" + guideLineIndex}>{item.title}</p>
          ))}
      </div>

      <style jsx>{`
        .guideLinesCont {
          padding: 20px;
        }

        .guideLinesCont p {
          font-size: 20px;
          text-align: center;
        }
      `}</style>
    </Wrapper>
  );
};
