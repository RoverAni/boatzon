import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Professional_User_Banner,
  Professional_Profile_Dp,
  Email_White_Icon,
  WHITE_COLOR,
  GREEN_COLOR,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
  Mobile_White_Icon,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  Dollar_White_Icon,
  UserPlaceholder,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import Tabs from "../../components/tabs/tabs";
import UserBoatPage from "./user-boat-page";
import UserProductPage from "./user-product-page";
import moment from "moment";

class UserProfilePage extends Component {
  state = {
    openPage: false,
    tabsValue: 0,
  };

  setTabValue = (val) => this.setState({ tabsValue: val });

  componentDidMount() {
    this.setState({
      openPage: true,
    });
  }

  render() {
    let { userProfileInfo, ownProductsPost, ownBoatsPost } = this.props;

    const Boats = <UserBoatPage ownBoatsPost={ownBoatsPost} />;
    const Products = <UserProductPage ownProductsPost={ownProductsPost} />;
    return (
      <Wrapper>
        {this.state.openPage ? (
          <Wrapper>
            <div className="ProfessionalUserProfile_BannerSec"></div>
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0">
                <div className="row m-0 ProfessionalUserProfile_ContentSec">
                  <div className="col-md-3 LeftSec col-lg-3 px-2">
                    <img
                      src={userProfileInfo && userProfileInfo.profilePicUrl}
                      onError={(e) => (e.target.src = UserPlaceholder)}
                      className="profileDp"
                    ></img>
                    <div className="py-3 userInfo_Sec">
                      <h6 className="userName">{userProfileInfo && userProfileInfo.fullName}</h6>
                      <p className="profileJoineDate">
                        Joined {userProfileInfo && moment(parseInt(userProfileInfo.registeredOn)).format("MMMM Y")}
                      </p>
                      <p className="profileRespondTime">Responed within 1 hour</p>
                    </div>
                    <div className="py-3 userBtn_Sec">
                      <div className="userProfile_btn">
                        <ButtonComp>
                          <img src={Dollar_White_Icon} className="paymentIcon"></img>
                          Verified for payments
                        </ButtonComp>
                      </div>
                      <div className="userProfile_btn">
                        <ButtonComp>
                          <img src={Mobile_White_Icon} className="phoneIcon"></img>
                          Confirmed phone
                        </ButtonComp>
                      </div>
                      <div className="userProfile_btn">
                        <ButtonComp className="m-0">
                          <img src={Email_White_Icon} className="emailIcon"></img>
                          Confirmed email
                        </ButtonComp>
                      </div>
                    </div>
                    <div className="py-3 userConnectionsInfo_Sec">
                      <h6 className="connectionLabel">Connections</h6>
                      <div className="d-flex align-items-center justify-content-between">
                        <p className="followersNum">
                          <span>0</span> Followers
                        </p>
                        <p className="followingNum mr-3">
                          <span>2</span> Following
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md col-lg py-2">
                    <div className="TabSec">
                      <span className="saleCount">
                        {this.state.tabsValue == 0 ? ownBoatsPost && ownBoatsPost.length : ownProductsPost && ownProductsPost.length} Items
                        for Sale
                      </span>
                      <Tabs
                        normalsmalltabs={true}
                        setTabValue={this.setTabValue}
                        hasManualTabSelector={true}
                        tabs={[
                          {
                            label: `Boats`,
                          },
                          {
                            label: `Products`,
                          },
                        ]}
                        tabcontent={[{ content: Boats }, { content: Products }]}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Wrapper>
        ) : (
          ""
        )}

        <style jsx>
          {`
            .saleCount {
              position: absolute;
              top: 0.732vw;
              right: 0.732vw;
              font-size: 0.732vw;
              z-index: 1;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
            }
            .LeftSec {
              max-width: 13.177vw;
            }
            .ProfessionalUserProfile_BannerSec {
              background-image: url(${Professional_User_Banner});
              min-height: 10.98vw;
              background-repeat: no-repeat;
              background-size: cover;
              position: relative;
            }
            .userInfo_Sec {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .userConnectionsInfo_Sec {
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .followersNum span {
              font-family: "Open Sans-Semibold" !important;
            }
            .followingNum span {
              color: ${GREEN_COLOR};
              font-family: "Open Sans-Semibold" !important;
            }
            .profileJoineDate,
            .profileRespondTime,
            .followersNum,
            .followingNum {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .connectionLabel {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.366vw;
            }
            .profileDp {
              width: 100%;
              border-radius: 0.292vw;
              object-fit: cover;
            }

            :global(.userProfile_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0 0 0.585vw 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.userProfile_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.userProfile_btn button:focus),
            :global(.userProfile_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.userProfile_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .paymentIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.585vw;
            }
            .phoneIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.585vw;
            }
            .emailIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.732vw;
            }

            :global(.media_btn button) {
              width: 100%;
              padding: 0.219vw 1.024vw;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_1};
              border: 0.0732vw solid ${BG_LightGREY_COLOR};
              margin: 0.585vw 0;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.658vw;
              position: relative;
            }
            :global(.media_btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.658vw;
            }
            :global(.media_btn button:focus),
            :global(.media_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.media_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .boderTop {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .facebookIcon {
              margin-right: 0.585vw;
              margin-bottom: 0.219vw;
              width: 0.366vw;
            }
            .twitterIcon {
              margin-right: 0.585vw;
              width: 0.658vw;
              margin-bottom: 0.146vw;
            }
            .professionalUser_Sec .label {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .specializationList li {
              background: ${BG_LightGREY_COLOR};
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.292vw 0.732vw;
              font-size: 0.658vw;
              margin-right: 0.366vw;
              margin-top: 0.439vw;
              cursor: pointer;
            }
            .ProfessionalUserProfile_ContentSec {
              position: relative;
              top: -5.49vw;
              z-index: 1;
              width: 100%;
            }
            .TabSec {
              position: relative;
              top: 5.856vw;
            }
            .userName {
              color: ${FONTGREY_COLOR};
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.366vw;
            }
            .reviwesNum {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              margin-left: 0.585vw;
              text-transform: capitalize;
            }
            .professionalUserLocation {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default UserProfilePage;
