import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import ProductCard from "../post-card/product-card";
import CustomLink from "../../components/Link/Link";
import { FONTGREY_COLOR, Wishlist_Icon } from "../../lib/config";

class UserProductPage extends Component {
  render() {
    let { ownProductsPost } = this.props;
    return (
      <Wrapper>
        <div className="col-12 p-0 pt-3">
          <h6 className="heading">Products for Sale:</h6>
          <div className="row m-0 my-2">
            {ownProductsPost &&
              ownProductsPost.map((item, index) =>
                index <= 2 ? (
                  <div key={index} className="col-md-4 col-lg-3 p-0 boatCard">
                    <CustomLink href="/product-detail">
                      <a target="_blank">
                        <ProductCard
                          Productdetails={item}
                          wishlist={true}
                          professionalpage={true}
                        />
                      </a>
                    </CustomLink>
                    <img
                      src={Wishlist_Icon}
                      id={index}
                      className="wishlistIcon"
                      //   onClick={this.handleWishlistPost.bind(this, index)}
                    ></img>
                  </div>
                ) : (
                  ""
                )
              )}
          </div>
        </div>
        <style jsx>
          {`
            .heading {
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0219vw !important;
              text-transform: initial;
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .wishlistIcon {
              position: absolute;
              bottom: 0.732vw;
              right: 0.732vw;
              width: 1.024vw;
            }
            .boatCard {
              cursor: pointer;
              max-width: 11.713vw;
              min-height: 13.909vw;
              box-shadow: 0 0px 6px 0 rgba(48, 56, 97, 0.1);
              margin-right: 0.878vw !important;
              position: relative;
            }
            .wishlistIconFill {
              width: 1.464vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default UserProductPage;
