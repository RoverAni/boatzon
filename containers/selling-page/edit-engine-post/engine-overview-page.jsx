import React, { Component } from "react";
import Wrapper from "../../../hoc/Wrapper";
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  WHITE_COLOR,
  Refresh_White_Icon,
  Delete_White_Icon,
  Img_Download_Icon,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
} from "../../../lib/config";
import TextAreaBox from "../../../components/input-box/text-area-input";
import { ReactSortable } from "react-sortablejs";

class EngineOverviewPage extends Component {
  render() {
    const { description } = { ...this.props.inputpayload };
    const {
      handleOnchangeInput,
      images,
      handleReorderImage,
      handleDeleteImage,
      handleFileHandler,
    } = this.props;
    return (
      <Wrapper>
        <div className="col-12 p-0 py-3 EngineOverviewPage">
          <div className="row m-0">
            <div className="col-6 pl-0">
              <div>
                <h6 className="secHeading">Description</h6>
                <div className="descFormInput">
                  <TextAreaBox
                    type="textarea"
                    id="about"
                    className="textareaBox form-control"
                    placeholder="Tell your customers about yourself and what makes your business unique."
                    name="description"
                    value={description}
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  ></TextAreaBox>
                </div>
              </div>
            </div>
            <div className="col-6 pr-0">
              <div>
                <h6 className="secHeading">Upload Photos</h6>
                <div className="d-flex align-items-center justify-content-start imagesContainer flex-wrap">
                  <ReactSortable
                    direction="horizontal"
                    animation={100}
                    className="d-flex align-items-center justify-content-start"
                    handle=".images"
                    list={images}
                    setList={handleReorderImage}
                  >
                    {images.map((item) => (
                      <div className="images" key={item.id}>
                        <div className="content-overlay">
                          <img
                            src={Delete_White_Icon}
                            className="deleteIcon mr-2"
                            onClick={handleDeleteImage.bind(this, item)}
                          ></img>
                          {/* <label for="reuploadImage" className="mb-0 cursorPtr">
                            <div>
                              <img
                                src={Refresh_White_Icon}
                                className="deleteIcon"
                              ></img>
                              <input
                                id="reuploadImage"
                                type="file"
                                style={{ display: "none" }}
                                onChange={handleFileHandler.bind(this, item)}
                                accept="image/*,application/pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                              />
                            </div>
                          </label> */}
                        </div>
                        <img src={item.name}></img>
                      </div>
                    ))}
                  </ReactSortable>
                  <label for="uploadImage" className="UploadSec">
                    <div>
                      <div className="UploadImgSec">
                        <img src={Img_Download_Icon}></img>
                        <h6>
                          Drop or <span>browse</span> image
                        </h6>
                      </div>
                      <input
                        id="uploadImage"
                        type="file"
                        style={{ display: "none" }}
                        onChange={handleFileHandler.bind(this, null)}
                        accept="image/*,application/pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                      />
                    </div>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.imagesContainer) {
              padding-bottom: 0.732vw;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .sizeMsg {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              padding: 0.292vw 0;
              text-transform: capitalize;
            }
            .images {
              width: 8.418vw;
              height: 6.222vw;
              margin-right: 0.366vw;
              margin-bottom: 0.366vw;
              border-radius: 0.146vw;
              position: relative;
            }
            .images img {
              width: 100%;
              height: 100%;
              object-fit: cover;
            }
            :global(.imagesContainer .images:first-child) {
              border: 0.146vw solid ${GREEN_COLOR};
            }
            :global(.imagesContainer .images:first-child:before) {
              content: "Main images";
              position: absolute;
              bottom: 0;
              left: 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-size: 0.732vw;
              padding: 0.219vw 0.366vw;
              font-family: "Open Sans" !important;
            }
            .content-overlay {
              background: rgba(0, 0, 0, 0.5);
              position: absolute;
              width: 8.418vw;
              height: 6.222vw;
              left: 0;
              top: 0;
              bottom: 0;
              cursor: pointer;
              right: 0;
              opacity: 0;
              -webkit-transition: all 0.4s ease-in-out 0s;
              -moz-transition: all 0.4s ease-in-out 0s;
              transition: all 0.4s ease-in-out 0s;
              z-index: 1;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .images:hover .content-overlay {
              opacity: 1;
            }
            .deleteIcon {
              width: 1.098vw !important;
              object-fit: cover !important;
              margin-bottom: 0.219vw;
              height: unset !important;
              border-radius: none !important;
            }
            .UploadSec {
              background: ${Border_LightGREY_COLOR};
              border: 0.0732vw dashed ${GREY_VARIANT_3};
              width: 8.418vw;
              height: 6.222vw;
              border-radius: 0.146vw;
              position: relative;
              cursor: pointer;
              margin: 0 0 0.366vw 0;
              box-shadow: 0px 2px 4px 2px rgba(209, 208, 208, 0.3);
            }
            .UploadImgSec {
              width: 8.418vw;
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
              text-align: center;
            }
            .uploadImg {
              width: 1.83vw;
              object-fit: cover;
            }
            .UploadImgSec > h6 {
              color: ${GREY_VARIANT_1};
              font-size: 0.732vw;
              padding: 0.585vw;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              display: grid;
            }
            .UploadImgSec > h6 span {
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            .secHeading {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.805vw;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              line-height: 1;
              margin-bottom: 0.732vw;
            }
            :global(.EngineOverviewPage .descFormInput .textareaBox) {
              display: block;
              width: 100%;
              height: 21.961vw;
              padding: 0.951vw 0.878vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.EngineOverviewPage .descFormInput .textareaBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.textareaBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EngineOverviewPage;
