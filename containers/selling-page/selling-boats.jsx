import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import {
  GREEN_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  GREY_VARIANT_1,
  GREY_VARIANT_6,
  Share_Menu_Grey_Icon,
  GREY_VARIANT_3,
  Orange_Color,
  Search_Blue_Icon,
  Checked_Darkgrey_Icon,
  Radio_Darkgrey_Icon,
  Chevron_Down,
  Plus_Icon_White,
  View_Grey_Filled_Icon,
  Light_Blue_Color,
  Hand_Grey_Icon,
  Chat_Icon_Grey,
  Pending_White_Icon,
  Cancel_White_Icon,
  Refund_White_Icon,
  Red_Color_1,
  Review_Grey_Icon,
  Facebook_Blue_Icon,
  Email_Icon,
  No_Boat_Post_Img,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import { Picky } from "react-picky";
// Reactstrap Components
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import currencies from "../../translations/currency.json";
import Model from "../../components/model/model";
import MarkSoldModel from "./mark-sold-model";
import SelectInput from "../../components/input-box/select-input";
import CreatePost from "../create-post/create-post";
import MainDrawer from "../../components/Drawer/Drawer";
import { handleFBShare, handleEmailShare } from "../../lib/share/handleShare";
import { FacebookShareButton, EmailShareButton } from "react-share";
import PageLoader from "../../components/loader/page-loader";

class SellingBoats extends PureComponent {
  state = {
    peopleList: true,
    selectedStatusType: [],
    selectedCategoryType: [],
    open: false,
    markSoldPopup: false,
    createPost: false,
    selectedStatus: "", // [Cancel", "Pending", "Drop", "Refund", "Completed]
    boatData: {},
  };
  // Function for inputpayload for selectInput
  // handleOnSelectInput = (name, index) => (event) => {

  //   x[0].style.color = "#FFF";
  //   let inputControl = event;
  //   if (inputControl && inputControl.length > 0) {
  //     let tempArray = [...this.state[name]];
  //     tempArray = event ? event.map((options) => options.value) : [];
  //     this.setState({ [name]: tempArray }, () => {
  //       let temppayload = this.state.inputpayload;
  //       temppayload[[name]] = [...this.state[name]];
  //       this.setState({ inputpayload: temppayload });
  //     });
  //   } else if (inputControl && inputControl.value) {
  //     let temppayload = this.state.inputpayload;
  //     temppayload[[name]] = inputControl.value;
  //     this.setState({
  //       inputpayload: temppayload,
  //     });
  //   }
  // };

  storeBoatData = (data) => this.setState({ boatData: data });

  handleOnSelectInput = (event, index) => {
    let selectedId = document.getElementById(`${`sellingBoats_${index}`}`);
    let selectedNode = selectedId.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[1].childNodes[0].childNodes[4].childNodes[0].childNodes[0].getElementsByClassName(
      "react-select__control"
    );
    selectedNode[0].style.backgroundColor = event.style.background;
    this.setState({ selectedStatus: event.value.label });
  };

  handleConditionOnSelect = (value) => {
    this.setState({
      selectedCondition: value,
    });
  };

  handleTypeOnSelect = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  handleBackgroundClr = (data) => {
    switch (data) {
      case "Refund":
        return `${Orange_Color}`;
        break;
      case "Pending":
        return `${THEME_COLOR}`;
        break;
      case "Cancel":
        return `${Red_Color_1}`;
        break;
    }
  };

  handleStatusIcon = (data) => {
    switch (data) {
      case "Refund":
        return `${Refund_White_Icon}`;
        break;
      case "Pending":
        return `${Pending_White_Icon}`;
        break;
      case "Cancel":
        return `${Cancel_White_Icon}`;
        break;
    }
  };

  handleToggle = (name) => {
    if (this.state[name] === undefined) {
      this.handleSetOpenState(name);
    }
    this.setState({
      [name]: !this.state[name],
    });
  };

  handleSetOpenState = (name) => {
    this.setState({
      [name]: false,
    });
  };

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  handleMarkSoldPopup = () => {
    this.setState({
      markSoldPopup: !this.state.markSoldPopup,
    });
  };

  handleCreateNewPost = () => {
    this.setState({
      createPost: !this.state.createPost,
    });
  };

  render() {
    const Conditions = ["All Conditions", "New", "Used"];
    const StatusTypes = ["Cancel", "Pending", "Drop", "Refund", "Completed"];

    const BoatsTypes =
      this.props.BoatsSubCategoriesList &&
      this.props.BoatsSubCategoriesList.map(
        (data) => `${data.subCategoryName}`
      );

    const StatusOptions = [
      {
        value: "Cancel",
        label: "Cancel",
        style: { background: "#FF0000" }, // red
      },
      {
        value: "Pending",
        label: "Pending",
        style: { background: "#387BCB" }, // blue
      },
      {
        value: "Drop",
        label: "Drop",
        style: { background: "#7C5CB4" }, // misc violet
      },
      {
        value: "Refund",
        label: "Refund",
        style: { background: "#FFAB2D" }, // orange
      },
      {
        value: "Completed",
        label: "Completed",
        style: { background: "#32C365" }, // green
      },
    ];

    const { open } = this.state;
    const { BoatPosts, loader } = this.props;
    console.log("fnekf", BoatPosts);
    return (
      <Wrapper>
        <div className="BuyingBoatsPage my-3">
          <div className="row m-0 align-items-center d-flex flex-nowrap">
            <div className="searchInputSec mb-2">
              <div className="searchInput">
                <InputBox
                  id="searchInput"
                  placeholder="Search"
                  type="text"
                  className="inputBox form-control"
                  onChange={this.searchItem}
                />
                <img src={Search_Blue_Icon} className="searchIcon"></img>
              </div>
            </div>

            <div className="radioSelectInput mb-2">
              <Picky
                id="radioPicky"
                keepOpen={false}
                placeholder="All Condition"
                options={Conditions}
                value={this.state.selectedCondition}
                onChange={this.handleConditionOnSelect}
              />
            </div>
            <div className="checkboxSelectInput mb-2">
              <Picky
                id="picky"
                keepOpen={false}
                placeholder="Status"
                multiple={true}
                numberDisplayed={1}
                options={StatusTypes}
                value={this.state.selectedStatusType}
                onChange={this.handleTypeOnSelect.bind(
                  this,
                  "selectedStatusType"
                )}
              />
            </div>
            <div className="checkboxSelectInput mb-2">
              <Picky
                id="picky"
                keepOpen={false}
                placeholder="Categories"
                multiple={true}
                numberDisplayed={1}
                options={BoatsTypes}
                value={this.state.selectedCategoryType}
                onChange={this.handleTypeOnSelect.bind(
                  this,
                  "selectedCategoryType"
                )}
              />
            </div>

            <div className="addListing_btn mb-2">
              <ButtonComp onClick={this.handleCreateNewPost}>
                <img src={Plus_Icon_White} className="plusIcon"></img> Add
                Listing
              </ButtonComp>
            </div>
          </div>
          {loader ? (
            <div className="pageLoader">
              <PageLoader loading={loader} />
            </div>
          ) : BoatPosts && BoatPosts.length > 0 ? (
            BoatPosts.map((data, index) => (
              <div
                className={"col-12 ChatListCard p-0"}
                id={"sellingBoats_" + index}
              >
                <div className="section">
                  <div className="row m-0 align-items-center" key={index}>
                    <div className="col p-0 LeftSec">
                      <div className="row m-0 align-items-center">
                        <div className="col-4 p-0 boatprofileSec">
                          <div className="row m-0 d-flex align-items-center">
                            <div className="col-3 p-0">
                              <img src={data.mainUrl} className="BoatImg"></img>
                            </div>
                            <div className="col-9 p-0 boatInfoSec">
                              <h6 className="boatName">{data.productName}</h6>
                            </div>
                          </div>
                        </div>
                        <div className="col p-0">
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 listing_status pr-2">
                              {data.sold == 0 ? (
                                <div className="active_btn">
                                  <ButtonComp>Active</ButtonComp>
                                </div>
                              ) : data.sold == 5 ? (
                                <div className="inActive_btn">
                                  <ButtonComp>Inactive</ButtonComp>
                                </div>
                              ) : data.sold == 1 ? (
                                <div className="inActive_btn">
                                  <ButtonComp>Sold</ButtonComp>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="col-4 p-0 listingStatusCounts">
                              <div className="row m-0 d-flex align-items-center justify-content-evenly">
                                <div className="col-4 text-center p-0">
                                  <p className="viewCount">
                                    <img
                                      src={View_Grey_Filled_Icon}
                                      className="viewIcon"
                                    ></img>
                                    <span className="countNum">
                                      {this.props.boatsTotalView[index]}
                                    </span>
                                  </p>
                                </div>
                                <div className="col-4 text-center p-0">
                                  <p className="clickedCount">
                                    <img
                                      src={Hand_Grey_Icon}
                                      className="clickIcon"
                                    ></img>
                                    <span className="countNum">
                                      {data.likeStatus}
                                    </span>
                                  </p>
                                </div>
                                <div className="col-4 text-center p-0">
                                  <p className="commentCount">
                                    <img
                                      src={Chat_Icon_Grey}
                                      className="commentIcon"
                                    ></img>
                                    <span className="countNum">
                                      {data.totalComments}
                                    </span>
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="col-2 p-0 priceSec px-2">
                              <p className="boatPrice">
                                {this.handleCurrencySymbol(data.currency)}
                                {data.price}
                              </p>
                            </div>
                            <div className="col-2 p-0">
                              {data.sellingboat_listing_status == "InActive" ? (
                                ""
                              ) : (
                                <div className="makeSold_btn">
                                  <ButtonComp
                                    onClick={() => {
                                      this.storeBoatData(data);
                                      this.handleMarkSoldPopup();
                                    }}
                                  >
                                    Make Sold
                                  </ButtonComp>
                                </div>
                              )}
                            </div>
                            <div className="col p-0 pl-2">
                              <div className="sellingstatus_btn">
                                {/* <ButtonComp
                                  style={{
                                    background: this.handleBackgroundClr(
                                      data.selling_status
                                    ),
                                  }}
                                >
                                  <img
                                    src={this.handleStatusIcon(
                                      data.selling_status
                                    )}
                                    className="statusIcon"
                                  ></img>
                                  {data.selling_status}
                                </ButtonComp> */}
                                <SelectInput
                                  sellingPage={true}
                                  value={this.state[`sellingStatus${index}`]}
                                  placeholder="Status"
                                  options={StatusOptions}
                                  setColor={this.state.setColor}
                                  onChange={(event) =>
                                    this.handleOnSelectInput(event, index)
                                  }
                                  // onChange={this.handleOnSelectInput(`sellingStatus${index}`, index)}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-1 p-0 pl-2 d-flex align-items-center justify-content-between EditSec">
                      <div className="edit_btn">
                        <ButtonComp
                          onClick={this.props.handleEditBoatPost.bind(
                            this,
                            data
                          )}
                        >
                          <img
                            src={Review_Grey_Icon}
                            className="editIcon"
                          ></img>
                          Edit
                        </ButtonComp>
                      </div>
                      <div>
                        <ButtonDropdown
                          isOpen={this.state[`open${index}`]}
                          toggle={this.handleToggle.bind(this, `open${index}`)}
                          className="shareDropdownBtn"
                        >
                          <DropdownToggle>
                            <img
                              src={Share_Menu_Grey_Icon}
                              className="shareMenuIcon"
                            ></img>
                          </DropdownToggle>
                          <DropdownMenu className="shareDropdownList">
                            <a
                              onClick={handleFBShare.bind(this, data)}
                              title="Share on Facebook"
                            >
                              <DropdownItem className="dropdownItem">
                                <FacebookShareButton>
                                  <div className="d-flex align-items-center">
                                    <img
                                      src={Facebook_Blue_Icon}
                                      className="facebookIcon"
                                    ></img>
                                    <span className="label">
                                      Facebook Share
                                    </span>
                                  </div>
                                </FacebookShareButton>
                              </DropdownItem>
                            </a>
                            <a
                              onClick={handleEmailShare.bind(this, data)}
                              title="Share with Email"
                            >
                              <DropdownItem className="dropdownItem">
                                <EmailShareButton>
                                  <div className="d-flex align-items-center">
                                    <img
                                      src={Email_Icon}
                                      className="emailIcon"
                                    ></img>
                                    <span className="label">
                                      Share Via Email
                                    </span>
                                  </div>
                                </EmailShareButton>
                              </DropdownItem>
                            </a>
                          </DropdownMenu>
                        </ButtonDropdown>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="noFavorites">
              <div className="text-center">
                <img src={No_Boat_Post_Img} className="noFavFollowingImg"></img>
                <p className="noFavoritesMsg">No Boat Post</p>
              </div>
            </div>
          )}
        </div>

        {/* Mark Sold Model */}
        <Model
          open={this.state.markSoldPopup}
          onClose={this.handleMarkSoldPopup}
        >
          <MarkSoldModel
            BoatPosts={BoatPosts}
            markAsSoldAndUpdateBoatData={this.props.markAsSoldAndUpdateBoatData}
            type="Boat"
            boatData={this.state.boatData}
            onClose={this.handleMarkSoldPopup}
          />
        </Model>

        {/* Create Post Drawer */}
        <MainDrawer
          open={this.state.createPost}
          onClose={this.handleCreateNewPost}
          anchor="right"
        >
          <CreatePost onClose={this.handleCreateNewPost} />
        </MainDrawer>
        <style jsx>
          {`
            :global(.noFavFollowingImg) {
              width: 14.641vw;
              object-fit: cover;
            }
            :global(.noFavorites) {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 18.942vw;
              text-align: center;
              background: ${WHITE_COLOR};
            }
            :global(.noFavoritesMsg) {
              max-width: 80%;
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            :global(.shareDropdownBtn > button) {
              background: ${Border_LightGREY_COLOR} !important;
              border: none;
              padding: 0 0.439vw !important;
              margin: 0 0 0 0.366vw !important;
              border-radius: 0.146vw;
              height: 1.756vw;
            }
            :global(.shareDropdownBtn > button:active),
            :global(.shareDropdownBtn > button:focus) {
              outline: none !important;
              background: ${Border_LightGREY_COLOR} !important;
              box-shadow: none !important;
            }
            :global(.shareDropdownBtn > button:hover) {
              background: ${Border_LightGREY_COLOR} !important;
            }
            :global(.shareDropdownBtn > .shareDropdownList > .dropdownItem) {
              padding: 0.366vw 0.585vw !important;
              cursor: pointer;
            }
            :global(.shareDropdownList .dropdownItem:hover) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.shareDropdownList .dropdownItem:hover .label) {
              color: ${THEME_COLOR};
            }
            .shareMenuIcon {
              width: 0.219vw;
              margin: 0 0.146vw;
              display: flex;
            }
            .facebookIcon {
              width: 0.512vw;
              margin-right: 0.951vw;
              margin-left: 0.219vw;
            }
            .emailIcon {
              width: 0.805vw;
              margin-right: 0.805vw;
            }
            .label {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              margin: 0;
              letter-spacing: 0.021vw !important;
            }
            :global(.shareDropdownList) {
              padding: 0.146vw !important;
            }
            .editIcon {
              width: 0.585vw;
              margin-right: 0.219vw;
            }
            .statusIcon {
              width: 40%;
              margin-right: 0.585vw;
            }
            .viewCount,
            .commentCount {
              margin: 0;
            }
            .clickedCount {
              margin: 0;
            }
            .viewIcon,
            .clickIcon,
            .commentIcon {
              width: 0.805vw;
              margin-right: 0.366vw;
            }
            .countNum {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0 0.366vw 0 0;
              letter-spacing: 0.021vw !important;
            }
            .searchInputSec {
              width: auto;
              flex: 1;
            }
            .radioSelectInput,
            .checkboxSelectInput {
              margin: 0 0.585vw 0 0;
            }
            :global(.radioSelectInput > div),
            :global(.checkboxSelectInput > div) {
              min-width: fit-content;
              height: 2.342vw;
              position: relative;
            }
            :global(.radioSelectInput > div > button),
            :global(.checkboxSelectInput > div > button) {
              position: relative;
              height: 2.342vw;
              border-radius: 0.219vw;
              border: 0.073vw solid ${Border_LightGREY_COLOR} !important;
              padding: 0 0.366vw !important;
            }
            :global(.radioSelectInput > div > button > span),
            :global(.checkboxSelectInput > div > button > span) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              align-items: center;
              position: relative;
              padding: 0 0.732vw 0 0;
              border-radius: 0.219vw;
            }
            :global(.radioSelectInput > div > button:focus),
            :global(.radioSelectInput > div > button:active),
            :global(.checkboxSelectInput > div > button:focus),
            :global(.checkboxSelectInput > div > button:active) {
              outline: none !important;
            }
            :global(.radioSelectInput > div > button::after),
            :global(.checkboxSelectInput > div > button::after) {
              content: url(${Chevron_Down});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.292vw !important;
              position: relative;
              top: -0.146vw;
              left: 0.146vw;
            }
            :global(.radioSelectInput .picky__dropdown),
            :global(.checkboxSelectInput .picky__dropdown) {
              min-width: fit-content !important;
              width: max-content !important;
              overflow: auto !important;
              padding: 0 0.512vw 0.585vw 0.512vw !important;
              margin: 0;
              box-shadow: 0px 0.146vw 0.366vw rgba(0, 0, 0, 0.1) !important;
              z-index: 1 !important;
              max-height: 11.713vw !important;
              scrollbar-width: thin !important;
              scrollbar-color: "#c4c4c4 #f5f5f5";
            }
            :global(.radioSelectInput .picky__dropdown > div > div),
            :global(.checkboxSelectInput .picky__dropdown > div > div) {
              width: auto;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              border-bottom: none !important;
              letter-spacing: 0.021vw !important;
              text-transform: capitalize;
              background: ${WHITE_COLOR} !important;
              display: flex;
              align-items: center;
            }
            :global(.radioSelectInput .picky__dropdown > div > div input) {
              -webkit-appearance: none;
              border: 0.073vw solid ${GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 1.83vw;
              display: block;
              position: relative;
              margin-right: 0.366vw;
            }
            :global(.radioSelectInput
                .picky__dropdown
                > div
                > div
                input:checked::after) {
              content: url(${Radio_Darkgrey_Icon});
              position: absolute;
              top: 50%;
              transform: translate(0, -51%);
              right: 0.146vw;
            }
            :global(.checkboxSelectInput .picky__dropdown > div > div input) {
              -webkit-appearance: none;
              border: 0.073vw solid ${GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 0.219vw;
              display: block;
              margin-right: 0.366vw !important;
              position: relative;
              margin-right: 0.292vw;
            }
            :global(.checkboxSelectInput
                .picky__dropdown
                > div
                > div
                input:checked::after) {
              content: url(${Checked_Darkgrey_Icon});
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            :global(.BuyingBoatsPage .searchInput) {
              position: relative;
              margin: 0 0.585vw 0 0;
            }
            :global(.BuyingBoatsPage .searchInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.073vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.BuyingBoatsPage .searchInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.BuyingBoatsPage .searchInput .inputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            .searchIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 1.098vw;
              width: 0.951vw;
            }
            .postedLabel {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0 0.366vw 0 0;
              letter-spacing: 0.021vw !important;
            }
            .availableIcon {
              width: 0.658vw;
              margin-right: 0.219vw;
            }
            .soldIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .mothlyPlan {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              margin: 0.146vw 0 0 0;
              position: absolute;
              bottom: -1.024vw;
              left: 50%;
              width: 100%;
              transform: translate(-50%);
            }
            .priceSec {
              max-width: 5vw;
            }
            .chatIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .LeftSec {
              max-width: 90%;
            }
            .boatprofileSec {
              max-width: 13.909vw;
            }
            .EditSec {
              max-width: 7.32vw;
              margin: 0 0 0 auto;
            }
            :global(.edit_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.512vw;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.edit_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.edit_btn button:focus),
            :global(.edit_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.edit_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .ChatListCard {
              cursor: pointer;
            }
            .ChatListCard:hover {
              background: ${GREY_VARIANT_6};
            }
            .ChatListCard:last-child .section {
              border: none;
            }
            .section {
              padding: 0.805vw 0;
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR};
            }
            .BoatImg {
              width: 100%;
              height: 2.562vw;
              object-fit: cover;
              border-radius: 0.366vw;
            }
            .boatInfoSec {
              padding: 0 0 0 0.512vw !important;
            }
            .boatName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0 0 0.219vw 0;
            }
            .boatPrice {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .boatSellerName {
              display: flex;
              align-items: center;
              margin: 0;
            }
            .sellerPic {
              width: 13px;
            }
            .boatSellerName span {
              font-size: 9.219vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              color: ${THEME_COLOR};
              padding: 0 0 0 0.366vw;
            }
            .chatDate {
              font-size: 0.585vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .listing_status {
              max-width: 6.954vw;
            }
            :global(.active_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${Light_Blue_Color};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
              border: 0.073vw solid ${Light_Blue_Color};
            }
            :global(.active_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.active_btn button:focus),
            :global(.active_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.active_btn button:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.inActive_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.512vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_2};
              margin: 0;
              border-radius: 0.146vw;
              border: 0.073vw solid ${GREY_VARIANT_2};
              text-transform: capitalize;
              position: relative;
            }
            :global(.inActive_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              line-height: 1;
            }
            :global(.inActive_btn button:focus),
            :global(.inActive_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.inActive_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .listingStatusCounts {
              max-width: 9.516vw;
            }

            :global(.makeSold_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.512vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.makeSold_btn button span) {
              font-family: "Open Sans" !important;
              width: fit-content;
              font-size: 0.805vw;
              line-height: 1;
            }
            :global(.makeSold_btn button:focus),
            :global(.makeSold_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.makeSold_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .plusIcon {
              width: 0.658vw;
              margin-right: 0.366vw;
            }
            .addListing_btn {
              display: flex;
              align-items: center;
              justify-content: flex-end;
              width: 11vw;
            }
            :global(.addListing_btn button) {
              width: 100%;
              min-width: fit-content;
              padding: 0.512vw 1.098vw;
              height: 2.342vw;
              background: ${GREEN_COLOR};
              color: ${GREY_VARIANT_6};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.addListing_btn button span) {
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              width: fit-content;
              font-size: 0.805vw;
              line-height: 1;
            }
            :global(.addListing_btn button:focus),
            :global(.addListing_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.addListing_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .sellingstatus_btn {
              width: auto;
              flex: 1;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    BoatsSubCategoriesList: state.BoatsSubCategoriesList,
    currentLocation: state.currentLocation,
    engineData: state.engineData,
  };
};

export default connect(mapStateToProps)(SellingBoats);
