import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import {
  Long_Arrow_Left_Blue,
  THEME_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  Edit_Icon,
  Border_LightGREY_COLOR,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  VIDEO_UPLOAD_SIZE,
  IMAGE_UPLOAD_SIZE,
} from "../../../lib/config";
import ButtonComp from "../../../components/button/button";
import InputBox from "../../../components/input-box/input-box";
import Tabs from "../../../components/tabs/tabs";
import ProductOverviewPage from "./product-overview-page";
import ProductDetailsPage from "./product-details-page";
import DeliveryMethodPage from "./delivery-method-page";
import currencies from "../../../translations/currency.json";
import CategoryDropdown from "../../create-post/category-dropdown";
import SubCategoryDropdown from "../../create-post/sub-category-dropdown";
import { editPost } from "../../../services/edit-post";
import Snackbar from "../../../components/snackbar";

const addAddress = (
  <div className="addAddress_btn">
    <ButtonComp>Add Address</ButtonComp>
  </div>
);

class EditProductPost extends Component {
  state = {
    editTitle: false,
    inputpayload: {},
    images: [],
    categoryDropdown: false,
    AddressList: [
      {
        value: "",
        label: addAddress,
      },
    ],
    selectedAddress: {},
    addAddressModel: false,
    newAddress: "",
    apiImagePayload: [],
    negotiable: false,
    subCategoryName: "",
  };

  componentDidMount() {
    console.log("fwndiwj", this.props.selectedProduct);
    const {
      productName,
      description,
      mainUrl,
      imageUrl1,
      condition,
      manufacturer,
      manufacturerId,
      subCategory,
      price,
      currency,
      postFilter,
      cloudinaryPublicId,
      cloudinaryPublicId1,
      imageCount,
      negotiable,
      containerWidth,
      containerHeight,
    } = this.props.selectedProduct;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[`productName`] = productName;
    tempPayload[`description`] = description;
    // const subCategoryName =
    //   postFilter &&
    //   postFilter.map((data) => {
    //     return data.values;
    //   });
    const subCategoryName = "";
    const currencySymbol = this.handleCurrencySymbol(currency);
    let tempAddPayload = [...this.state.AddressList];
    let currentAddress = {
      value: {
        address: this.props.selectedProduct.place,
        lat: this.props.selectedProduct.latitude,
        lng: this.props.selectedProduct.longitude,
        city: this.props.selectedProduct.city,
        countrySname: this.props.selectedProduct.countrySname,
      },
      label: this.props.selectedProduct.place,
    };
    tempAddPayload.unshift(currentAddress);

    let apiImagePayload = [...this.state.apiImagePayload];
    let imagepayload = {};
    imagepayload.url = mainUrl;
    let mainUrlType = mainUrl.split(".");
    imagepayload.urlType =
      mainUrlType[mainUrlType.length - 1] == "jpeg" || mainUrlType[mainUrlType.length - 1] == "png"
        ? "0"
        : mainUrlType[mainUrlType.length - 1] == "mp4"
        ? "1"
        : "";
    imagepayload.cloudinaryPublicId = cloudinaryPublicId;
    imagepayload.containerHeight = containerHeight;
    imagepayload.containerWidth = containerWidth;
    apiImagePayload.push(imagepayload);
    if (imageCount > 1) {
      let tempimageCount = imageCount;
      for (let i = tempimageCount; i > 1; i--) {
        let otherimgpayload = {};
        otherimgpayload.url = this.props.selectedProduct[`imageUrl${i - 1}`];
        let otherUrlType = this.props.selectedProduct[`imageUrl${i - 1}`].split(".");
        otherimgpayload.urlType =
          otherUrlType[otherUrlType.length - 1] == "jpeg" || otherUrlType[otherUrlType.length - 1] == "png"
            ? "0"
            : otherUrlType[otherUrlType.length - 1] == "mp4"
            ? "1"
            : "";
        otherimgpayload.cloudinaryPublicId = this.props.selectedProduct[`cloudinaryPublicId${i - 1}`];
        apiImagePayload.push(otherimgpayload);
      }
    }
    let tempImagesPayload =
      apiImagePayload &&
      apiImagePayload.map((data, index) => {
        return {
          id: index + 1,
          name: data.url,
        };
      });
    this.setState(
      {
        apiImagePayload,
        inputpayload: { ...tempPayload },
        images: [...tempImagesPayload],
        condition: condition ? { value: condition, label: condition } : "",
        manufacturer: manufacturer ? { value: manufacturerId, label: manufacturer } : "",
        subCategoryName: subCategoryName ? subCategoryName : "",
        categoryName: subCategory ? subCategory : "",
        currency: currencySymbol ? { value: currency, label: currencySymbol } : "",
        price: price ? price : "",
        selectedAddress: { ...currentAddress },
        negotiable: negotiable ? true : false,
      },
      () => {
        console.log("fnjdhe", this.state.selectedAddress);
        let tempInputpayload = { ...this.state.inputpayload };
        tempInputpayload.selectedAddress = this.state.selectedAddress.value;
        tempInputpayload.subCategory = this.state.subCategoryName;
        this.setState(
          {
            AddressList: [...tempAddPayload],
            inputpayload: { ...tempInputpayload },
          },
          () => {
            // this.checkIfFormValid();
            console.log("PAYLOAD--1", this.state.inputpayload);
          }
        );
      }
    );
  }

  // Function for inputpayload for selectInput
  handleAddressSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl.value == "") {
      this.setState({
        addAddressModel: true,
      });
    }
    if (inputControl && inputControl.length > 0) {
      let tempArray = this.state[name];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = { ...this.state.inputpayload };
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      console.log("feisdj", name, inputControl.value, inputControl.label);
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl.value;

      let tempSelectedAddress = {
        value: inputControl.value,
        label: inputControl.label,
      };
      let tempInputpayload = { ...this.state.inputpayload };
      tempInputpayload.selectedAddress = tempSelectedAddress.value;
      this.setState(
        {
          [name]: { ...tempSelectedAddress },
          inputpayload: { ...tempInputpayload },
        },
        () => {
          //   this.checkIfFormValid();
          console.log("PAYLOAD--2", this.state.inputpayload);
        }
      );
    }
  };

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] = inputControl.value && inputControl.value.length ? inputControl.value : "";
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
        [inputControl.name]: inputControl.value,
      },
      () => {
        console.log("inputpayload", this.state.inputpayload);
      }
    );
  };

  handleEditTitle = () => {
    this.setState({
      editTitle: true,
    });
  };

  handleReorderImage = (newOrder) => {
    this.setState(
      {
        images: newOrder,
      },
      () => {
        let apiImagePayload = [...this.state.apiImagePayload];
        const final = this.state.images.map((data) => apiImagePayload.find((item) => item.url == data.name));
        this.setState({
          apiImagePayload: [...final],
        });
      }
    );
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleDeleteImage = (imageObject) => {
    let tempImages = [...this.state.images];
    let elementIndex =
      tempImages &&
      tempImages.findIndex((data) => {
        return data.id == imageObject.id;
      });
    elementIndex != -1 ? tempImages.splice(elementIndex, 1) : "";

    let tempApiImagePayload = [...this.state.apiImagePayload];
    let elemntIdApiImagePayload =
      tempApiImagePayload &&
      tempApiImagePayload.findIndex((data) => {
        return data.url == imageObject.name;
      });
    elemntIdApiImagePayload != -1 ? tempApiImagePayload.splice(elemntIdApiImagePayload, 1) : "";

    this.setState({
      images: [...tempImages],
      apiImagePayload: [...tempApiImagePayload],
    });
  };

  // used to upload image from local storage
  handleUploadPic = async (event) => {
    let files = event.target.files;
    let file_type = files[0].type.split("/")[0];
    if (files && files[0] && (files[0].type == "image/jpeg" || files[0].type == "image/png")) {
      const data = new FormData();
      data.append("file", files[0]);
      data.append("upload_preset", "avzsfq8q");
      const res = await fetch(`https://api.cloudinary.com/v1_1/boatzon/${file_type}/upload`, {
        method: "POST",
        body: data,
      });
      const file = await res.json();
      try {
        if (file) {
          let apiImagePayload = [...this.state.apiImagePayload];
          let imagePayload = {};
          imagePayload.url = file.secure_url;
          imagePayload.cloudinaryPublicId = file.public_id;
          imagePayload.urlType = file.resource_type == "image" ? "0" : file.resource_type == "video" ? "1" : "";
          // imagePayload.containerWidth = file.width;
          // imagePayload.containerHeight = file.height;
          apiImagePayload.push(imagePayload);
          let ProductImg = [...this.state.images];
          let tempImagesObject = {};
          tempImagesObject.id = ProductImg.length + 1;
          tempImagesObject.name = file.secure_url;
          ProductImg.push(tempImagesObject);
          if (this.isFileSizeValid(files[0])) {
            this.setState(
              {
                productpic: true,
                fileErr: null,
                isDocValid: true,
              },
              () => {
                // let tempInputPayload = { ...this.state.inputpayload };
                // tempInputPayload.productImgs = [...ProductImg];
                this.setState(
                  {
                    apiImagePayload,
                    images: [...ProductImg],
                    // inputpayload: tempInputPayload,
                  },
                  () => {
                    // console.log("PAYLOAD", this.state.inputpayload);
                  }
                );
              }
            );
          } else {
            this.setState({
              fileErr: files[0].type == "video" ? "File size should be less than 4MB!" : "File size should be less than 2MB!",
              isDocValid: false,
            });
          }
        }
      } catch (err) {
        console.log("imager url error", err);
        this.setState({
          fileErr: "imager url error",
          isDocValid: false,
          productpic: false,
        });
      }
    } else {
      this.setState({
        fileErr: "File format is invalid! Image should be of .jpeg or .png format",
        isDocValid: false,
      });
    }
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    switch (fileData.type.split("/")[0]) {
      case "video":
        return fileData.size <= VIDEO_UPLOAD_SIZE;
        break;
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };

  handleSubCategorySelection = (data) => {
    this.setState(
      {
        subCategoryName: data,
      },
      () => {
        let filterLabel = `${this.state.categoryName} Type`;
        let tempInputPayload = { ...this.state.inputpayload };
        tempInputPayload.subCategory = this.state.categoryName;
        tempInputPayload.filter = {
          [`${filterLabel}`]: `${this.state.subCategoryName}`,
        };
        this.setState(
          {
            inputpayload: { ...tempInputPayload },
            categoryDropdown: false,
          },
          () => {
            // this.checkIfFormValid();
            console.log("PAYLOAD", this.state.inputpayload);
          }
        );
      }
    );
  };

  handleToggleCategoryDropdown = () => {
    this.setState({
      categoryDropdown: !this.state.categoryDropdown,
    });
  };

  handleCategorySelection = (data) => {
    console.log("fnejdn", data);
    if (data.filter && data.filter.length) {
      this.setState(
        {
          categoryName: data.subCategoryName,
          subCat: true,
          subCategoryName: "",
        },
        () => {
          this.setState(
            {
              categoryDropdown: true,
            },
            () => {
              let subCatList = data.filter && data.filter.length ? data.filter[0].values.split(",") : "";
              this.updateDropdownScreen(
                <SubCategoryDropdown
                  handleSubCategorySelection={this.handleSubCategorySelection}
                  categoryName={this.state.categoryName}
                  subCatList={subCatList}
                  backtoCategoryList={this.backtoCategoryList}
                />
              );
            }
          );
        }
      );
    } else if (data.filter && data.filter.length <= 0) {
      this.setState(
        {
          categoryName: data.subCategoryName,
          subCategoryName: "",
        },
        () => {
          let tempInputPayload = { ...this.state.inputpayload };
          tempInputPayload.subCategory = this.state.categoryName;
          this.setState(
            {
              inputpayload: { ...tempInputPayload },
              categoryDropdown: false,
            },
            () => {
              //   this.checkIfFormValid();
              console.log("PAYLOAD", this.state.inputpayload);
            }
          );
        }
      );
    }
  };

  backtoCategoryList = () => {
    this.updateDropdownScreen(
      <CategoryDropdown
        handleCategorySelection={this.handleCategorySelection}
        ProductsSubCategoriesList={this.props.ProductsSubCategoriesList}
      />
    );
  };

  // Function for changing screen
  updateDropdownScreen = (screen) => this.setState({ currentDropdownScreen: screen });

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    console.log("PAYLOADnsjcf", name, event);
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      console.log("PAYLOADnsjcf", name, event);
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      let tempValue = {};
      tempValue.value = inputControl.value;
      tempValue.label = inputControl.label;
      this.setState(
        {
          inputpayload: { ...temppayload },
          [name]: { ...tempValue },
        },
        () => {
          //   this.checkIfFormValid();
          console.log("PAYLOADnsjcf", this.state[name]);
        }
      );
    }
  };

  handleCloseAddAddress = () => {
    this.setState({
      addAddressModel: false,
    });
  };

  handleAddAddress = (AddressData) => {
    this.setState(
      {
        newAddress: AddressData.address,
        newAddressLat: AddressData.lat,
        newAddressLng: AddressData.lng,
        newAddresscity: AddressData.city,
        newAddresscountrySname: AddressData.country,
      },
      () => {
        let tempPayload = [...this.state.AddressList];
        let elementPos = tempPayload.findIndex((data) => {
          return data.value == this.state.newAddress;
        });
        if (elementPos == -1) {
          let payload = {
            address: this.state.newAddress,
            lat: this.state.newAddressLat,
            lng: this.state.newAddressLng,
            city: this.state.newAddresscity,
            countrySname: this.state.newAddresscountrySname,
          };
          let selectPayload = {
            value: { ...payload },
            label: this.state.newAddress,
          };
          tempPayload.unshift(selectPayload);
          let tempInputpayload = { ...this.state.inputpayload };
          tempInputpayload.selectedAddress = selectPayload.value;
          this.setState(
            {
              AddressList: [...tempPayload],
              selectedAddress: { ...selectPayload },
              inputpayload: tempInputpayload,
              addAddressModel: false,
            },
            () => {
              console.log("PAYLOAD--3", this.state.inputpayload);
            }
          );
        }
      }
    );
  };

  handleEditPostProduct = () => {
    let tempOtherImageUrls =
      this.state.apiImagePayload &&
      this.state.apiImagePayload.filter((data, index) => {
        return index > 0;
      });
    let finalPayload = {
      postId: this.props.selectedProduct.postId,
      mainUrl: this.state.apiImagePayload[0].url,
      thumbnailUrl: this.state.apiImagePayload[0].url,
      mainUrlType: this.state.apiImagePayload[0].urlType,
      thumbnailImageUrl: this.state.apiImagePayload[0].url,
      cloudinaryPublicId: this.state.apiImagePayload[0].cloudinaryPublicId,
      otherUrls: JSON.stringify([...tempOtherImageUrls]),
      imageCount: this.state.apiImagePayload.length,
      containerHeight: this.props.selectedProduct.containerHeight,
      containerWidth: this.props.selectedProduct.containerWidth,
      price: this.state.price,
      currency: this.state.currency.value,
      productName: this.state.inputpayload.productName,
      condition: this.state.condition.value,
      category: this.props.selectedProduct.category,
      manufactorId: this.state.manufacturer.value.toString(),
      manufactor: this.state.manufacturer.label,
      nationWide: "false",
      location: this.state.selectedAddress.value.address,
      latitude: this.state.selectedAddress.value.lat,
      longitude: this.state.selectedAddress.value.lng,
      city: this.state.selectedAddress.value.city,
      countrySname: this.state.selectedAddress.value.countrySname,
      businessPost: false, // 1: true, 2: false
      isSwap: 0, // 0: false, 1: true
      subCategory: this.state.categoryName,
      filter:
        this.state.subCategoryName != "" || this.state.subCategoryName != "null"
          ? JSON.stringify({ ...this.state.inputpayload.filter })
          : JSON.stringify({}),
      title: this.state.inputpayload.productName,
      description: this.state.inputpayload.description,
      negotiable: this.state.inputpayload.negotiable ? "1" : "0",
    };
    console.log("<--finalPayload-->", finalPayload);

    editPost(finalPayload)
      .then((res) => {
        console.log("RESPONSE--->>>", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            // window.location.reload();
          }
        }
      })
      .catch((err) => {
        console.log("ERROR", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204 || 422:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      default:
        return "error";
        break;
    }
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    this.setState({ [name]: event.target.checked }, () => {
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[name] = this.state[name];
      this.setState(
        {
          inputpayload: { ...tempInputPayload },
        },
        () => {
          console.log("PAYLOAD", this.state.inputpayload);
        }
      );
    });
  };

  render() {
    const Overview = (
      <ProductOverviewPage
        selectedProduct={this.props.selectedProduct}
        handleUploadPic={this.handleUploadPic}
        images={this.state.images}
        handleReorderImage={this.handleReorderImage}
        handleDeleteImage={this.handleDeleteImage}
        inputpayload={this.state.inputpayload}
        handleOnchangeInput={this.handleOnchangeInput}
      />
    );
    const Products_Details = (
      <ProductDetailsPage
        selectedProduct={this.props.selectedProduct}
        handleOnSelectInput={this.handleOnSelectInput}
        condition={this.state.condition}
        manufacturer={this.state.manufacturer}
        subCategoryName={this.state.subCategoryName}
        categoryName={this.state.categoryName}
        currency={this.state.currency}
        price={this.state.price}
        categoryDropdown={this.state.categoryDropdown}
        handleSubCategorySelection={this.handleSubCategorySelection}
        handleToggleCategoryDropdown={this.handleToggleCategoryDropdown}
        handleCategorySelection={this.handleCategorySelection}
        currentDropdownScreen={this.state.currentDropdownScreen}
        subCat={this.state.subCat}
        handleOnchangeInput={this.handleOnchangeInput}
        negotiable={this.state.negotiable}
        handleOnchangeCheckbox={this.handleOnchangeCheckbox}
      />
    );
    const Delivery_Method = (
      <DeliveryMethodPage
        selectedProduct={this.props.selectedProduct}
        handleAddressSelectInput={this.handleAddressSelectInput}
        selectedAddress={this.state.selectedAddress}
        addAddressModel={this.state.addAddressModel}
        AddressList={this.state.AddressList}
        handleCloseAddAddress={this.handleCloseAddAddress}
        handleAddAddress={this.handleAddAddress}
      />
    );

    const { productName } = { ...this.state.inputpayload };
    return (
      <Wrapper>
        <div className="back_btn">
          <ButtonComp onClick={this.props.handleBackScreen.bind(this, 1)}>
            <img src={Long_Arrow_Left_Blue} className="backarrowIcon"></img> Back to My Listing
          </ButtonComp>
        </div>
        <div className="col-12 editProductPostPage">
          <div className="row m-0">
            <div className="col-6 p-0">
              <div className="titleSec">
                {this.state.editTitle ? (
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="productName"
                    value={productName}
                    onChange={this.handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                ) : (
                  <div className="heading">
                    <h6>{productName}</h6>
                    <img src={Edit_Icon} className="editIcon" onClick={this.handleEditTitle}></img>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row m-0 my-3">
            <Tabs
              followingsmalltabs={true}
              tabs={[
                {
                  label: `Overview`,
                },
                {
                  label: `Product Details`,
                },
                {
                  label: `Delivery Method`,
                },
              ]}
              tabcontent={[{ content: Overview }, { content: Products_Details }, { content: Delivery_Method }]}
            />
          </div>
          <div className="row m-0 align-items-center justify-content-end">
            <div className="col-4 p-0">
              <div className="row m-0 d-flex align-items-center justify-content-end">
                <div className="col-5 pl-0">
                  <div className="cancel_btn">
                    <ButtonComp onClick={this.props.handleBackScreen.bind(this, 1)}>Cancel</ButtonComp>
                  </div>
                </div>
                <div className="col-7 p-0">
                  <div className="save_btn">
                    <ButtonComp onClick={this.handleEditPostProduct}>Save</ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        <style jsx>
          {`
            .back_btn {
              position: absolute;
              top: -2.562vw;
              left: 3%;
            }
            :global(.back_btn button) {
              min-width: fit-content !important;
              width: fit-content;
              padding: 0.732vw 0 1.464vw 0;
              background: transparent;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.back_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              letter-spacing: 0.021vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.cancel_btn button) {
              min-width: 100% !important;
              width: 100%;
              padding: 0.951vw 0;
              background: ${Border_LightGREY_COLOR};
              border-radius: 0.146vw;
              position: relative;
              border: none;
            }
            :global(.cancel_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize;
              line-height: 1;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.save_btn button) {
              min-width: 100% !important;
              width: 100%;
              padding: 0.951vw 0;
              background: ${GREEN_COLOR};
              border-radius: 0.146vw;
              position: relative;
              border: none;
            }
            :global(.save_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
              letter-spacing: 0.021vw !important;
              text-transform: capitalize;
              color: ${BG_LightGREY_COLOR};
              line-height: 1;
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .backarrowIcon {
              width: 0.805vw;
              margin-right: 0.292vw;
            }
            .editIcon {
              position: absolute;
              top: 50%;
              right: 0.732vw;
              transform: translate(0, -50%);
              display: none;
              cursor: pointer;
            }
            .heading {
              height: 2.635vw;
              display: flex;
              align-items: center;
              justify-content: flex-start;
            }
            .heading:hover {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
            }
            .heading:hover .editIcon {
              display: block;
            }
            .heading h6 {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0 0.732vw;
            }
            :global(.editProductPostPage .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.021vw;
              font-family: "Open Sans" !important;
            }
            :global(.editProductPostPage .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
            :global(.addAddress_btn button) {
              width: 100%;
              padding: 0.219vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              border-radius: 0 !important;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            :global(.addAddress_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-size: 0.878vw;
            }
            :global(.addAddress_btn button:focus),
            :global(.addAddress_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.addAddress_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ProductsSubCategoriesList: state.ProductsSubCategoriesList,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(EditProductPost);
