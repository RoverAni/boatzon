import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import SelectTopLabel from "../../../components/input-box/select-top-label";
// Reactstrap Components
import { ButtonDropdown, DropdownToggle, DropdownMenu } from "reactstrap";
import CategoryDropdown from "../../create-post/category-dropdown";
import {
  SelectInput_Chevron_Down_Grey,
  Close_White_Icon,
  Orange_Color,
  Dropdown_1,
  BG_LightGREY_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
  FONTGREY_COLOR,
  Border_LightGREY_COLOR_2,
  WHITE_COLOR,
  THEME_COLOR_Opacity,
  Checked_Darkgrey_Icon,
  GREY_VARIANT_3,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR,
} from "../../../lib/config";
import InputBox from "../../../components/input-box/input-box";
import SelectInput from "../../../components/input-box/select-input";
import currencies from "../../../translations/currency.json";
import CheckBox from "../../../components/input-box/check-box";

let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class ProductDetailsPage extends Component {
  state = {
    inputpayload: {
      currency: CurrencyArray[0].code,
    },
    categoryName: "",
    currency: [
      {
        value: CurrencyArray[0].code,
        label: CurrencyArray[0].symbol_native,
      },
    ],
  };

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  render() {
    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    
    const ManufacturersTypes = [
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
    ];

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));

    return (
      <Wrapper>
        <div className="col-12 p-0 ProductDetailsPage pb-4">
          <div className="row m-0 align-items-center">
            <div className="col-9 p-0">
              <div className="row m-0 align-items-center">
                <div className="col-6 pl-0">
                  <div className="selectInputSec">
                    <SelectTopLabel
                      editPost={true}
                      label="Condition"
                      placeholder="Used"
                      value={this.props.condition}
                      options={Conditions}
                      onChange={this.props.handleOnSelectInput(`condition`)}
                    />
                  </div>
                </div>
                <div className="col-6 pl-0">
                  <div className="selectInputSec">
                    <SelectTopLabel
                      editPost={true}
                      label="Product Came Off"
                      placeholder="Product Came Off"
                      value={this.props.manufacturer}
                      options={ManufacturersTypes}
                      onChange={this.props.handleOnSelectInput(`manufacturer`)}
                    />
                  </div>
                </div>
              </div>
              <div className="row m-0 align-items-center">
                <div className="col-6 pl-0">
                  <div className="MultiSelectCheckbox categoryCheckbox">
                    <label className="multiselectLabel">Category</label>
                    <ButtonDropdown
                      isOpen={this.props.categoryDropdown}
                      toggle={this.props.handleToggleCategoryDropdown}
                      className="categoryDropdownBtn"
                    >
                      <DropdownToggle>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="Choose Category"
                          value={
                            this.props.subCategoryName
                              ? `${this.props.categoryName} ${" "} > ${" "} ${
                                  this.props.subCategoryName
                                }`
                              : `${this.props.categoryName}`
                          }
                          autoComplete="off"
                        ></InputBox>
                        <div className="chevronIcon">
                          <img src={SelectInput_Chevron_Down_Grey}></img>
                        </div>
                      </DropdownToggle>
                      <DropdownMenu className="categoryDropdownList">
                        {!this.props.currentDropdownScreen ? (
                          <CategoryDropdown
                            handleCategorySelection={
                              this.props.handleCategorySelection
                            }
                            ProductsSubCategoriesList={
                              this.props.ProductsSubCategoriesList
                            }
                          />
                        ) : (
                          this.props.currentDropdownScreen
                        )}
                      </DropdownMenu>
                    </ButtonDropdown>

                    {this.props.subCat && this.props.subCategoryName == "" ? (
                      <p className="errMessage">
                        subCategory type is not selected
                      </p>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                <div className="col-3 pl-0">
                  <div className="FormInput">
                    <label>Price</label>
                    <div className="d-flex align-items-center priceInputContainer">
                      <SelectInput
                        defaultValue={this.props.currency}
                        currency={true}
                        placeholder="$"
                        value={this.props.currency}
                        options={CurrencyOptions}
                        onChange={this.props.handleOnSelectInput(`currency`)}
                      />
                      <div className="inputDiv">
                        <InputBox
                          type="text"
                          className="priceInputBox form-control"
                          name="price"
                          value={this.props.price}
                          onChange={this.props.handleOnchangeInput}
                          autoComplete="off"
                        ></InputBox>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row m-0 align-items-center">
                <div className="col-6 p-0 my-2">
                  <CheckBox
                    checked={this.props.negotiable}
                    plaincheckbox={true}
                    onChange={this.props.handleOnchangeCheckbox("negotiable")}
                    value="negotiable"
                    label="Is price negotiable?"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            // :global(.ProductDetailsPage .selectInputSec) {
            //   margin-top: 1.098vw !important;
            // }
            // :global(.ProductDetailsPage .react-select__placeholder) {
            //   font-size: 0.805vw !important;
            // }
            // :global(.ProductDetailsPage .react-select__input input) {
            //   font-size: 0.805vw !important;
            // }
            // :global(.ProductDetailsPage .react-select__control) {
            //   min-height: 2.342vw !important;
            // }
            // :global(.ProductDetailsPage .greyChevronIcon) {
            //   width: 0.805vw !important;
            //   margin-bottom: 0.219vw;
            // }
            // :global(.ProductDetailsPage .react-select__single-value) {
            //   font-size: 0.805vw !important;
            // }
            // :global(.ProductDetailsPage .react-select__option) {
            //   font-size: 0.805vw !important;
            // }
            :global(.categoryDropdownBtn) {
              width: 100%;
            }
            :global(.categoryDropdownBtn > button) {
              background: ${WHITE_COLOR} !important;
              border: none;
              padding: 0 !important;
              width: 100%;
              position: relative;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              padding: 0.585vw 0 0 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            .chevronIcon {
              position: absolute;
              right: 0.585vw;
              top: 50%;
              transform: translate(0, -50%);
            }
            .chevronIcon img {
              width: 0.732vw;
            }

            :global(.categoryDropdownBtn > button > .inputBox) {
              width: 100%;
              height: 2.342vw;
              padding: 0.375rem 0.292vw;
              line-height: 1.5;
              background-clip: padding-box;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              border-radius: 0 !important;
              border: none !important;
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              cursor: context-menu;
            }
            :global(.categoryDropdownBtn > button > .inputBox:focus) {
              box-shadow: none !important;
              outline: none !important;
            }
            :global(.categoryDropdownBtn > button:active),
            :global(.categoryDropdownBtn > button:focus) {
              outline: none !important;
              background: none !important;
              box-shadow: none !important;
            }
            :global(.categoryDropdownBtn
                > .categoryDropdownList
                > .dropdownItem) {
              display: flex !important;
              align-items: center !important;
              justify-content: space-between !important;
              padding: 0.366vw 0.585vw !important;
              cursor: pointer;
            }
            :global(.categoryDropdownBtn
                > .categoryDropdownList
                > .dropdownItem
                .catName) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
            }
            :global(.categoryDropdownList .dropdownItem:hover) {
              background: ${BG_LightGREY_COLOR} !important;
            }
            :global(.categoryDropdownList) {
              padding: 0.146vw !important;
              width: 100%;
              box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.2);
              border-radius: 0.219vw;
              border: none;
              max-height: 14.641vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .ProductDetailsPage .MultiSelectCheckbox {
              margin: 0;
              height: auto;
              border: none;
              position: relative;
              border-radius: 0.219vw;
            }
            .multiselectLabel {
              font-size: 0.805vw;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0 0 0.366vw 0;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .optionContainer) {
              border: none !important;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.014vw !important;
              padding: 0.366vw 0.732vw;
              text-transform: capitalize;
              background: ${WHITE_COLOR} !important;
              display: flex;
              align-items: center;
              position: relative;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option
                .checkbox) {
              -webkit-appearance: none;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 0.146vw;
              display: inline-block;
              float: right;
              position: absolute;
              right: 0.732vw;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option
                .checkbox:checked:after) {
              content: url(${Checked_Darkgrey_Icon});
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option:hover) {
              background: ${THEME_COLOR_Opacity} !important;
              color: ${THEME_COLOR} !important;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .Dv7FLAST-3bZceA5TIxEN),
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer) {
              z-index: 1 !important;
              margin: 0.732vw 0 0 0 !important;
              box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1) !important;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact) {
              background: ${WHITE_COLOR};
              border: none !important;
              border-radius: 0.292vw;
              width: auto;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              border-radius: 0 !important;
              margin: 0 !important;
              padding: 0 !important;
              display: flex;
              flex-wrap: wrap;
              align-items: center;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI
                > :first-of-type) {
              margin-left: 0 !important;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .searchBox) {
              margin-top: 0 !important;
              font-size: 0.805vw !important;
              width: 10.98vw !important;
              max-width: 10.98vw !important;
              padding: 0.366vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .searchBox::placeholder) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.014vw !important;
            }

            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .chip) {
              background: ${THEME_COLOR} !important;
              color: ${BG_LightGREY_COLOR} !important;
              font-family: "Open Sans" !important;
              border-radius: 0.146vw !important;
              font-size: 0.805vw !important;
              text-transform: capitalize;
              padding: 0.292vw 0.585vw !important;
              margin: 0 0.366vw 0.366vw 0 !important;
              display: flex;
              align-items: center;
              width: fit-content;
            }
            :global(.ProductDetailsPage
                .categoryCheckbox
                #multiselectContainerReact
                > div
                .chip:first-child) {
              background: ${Dropdown_1} !important;
            }
            :global(.ProductDetailsPage
                .categoryCheckbox
                #multiselectContainerReact
                > div
                .chip:nth-child(2)) {
              background: ${Orange_Color} !important;
            }

            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .chip
                > i) {
              font-size: unset;
              width: 0.732vw;
              height: 0.805vw;
            }
            :global(.ProductDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .chip
                > i::before) {
              content: url(${Close_White_Icon});
              line-height: 1 !important;
            }
            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: fit-content;
              flex: 1;
            }
            .inputDiv {
              width: auto;
              flex: 3;
            }
            :global(.ProductDetailsPage .FormInput .priceInputBox) {
              width: 100%;
              height: auto;
              padding: 0 0 0 0.292vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ProductDetailsPage .FormInput > div .priceInputBox:focus),
            :global(.ProductDetailsPage
                .FormInput
                > div
                .priceInputBox:active) {
              outline: none !important;
              box-shadow: none !important;
            }
            :global(.ProductDetailsPage .FormInput > .priceInputContainer) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
              padding: 0 0.292vw;
            }
            :global(.ProductDetailsPage
                .FormInput
                .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.021vw !important;
            }

            :global(.ProductDetailsPage .FormInput label) {
              font-size: 0.805vw;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.021vw !important;
              margin: 0 0 0.366vw 0;
              padding: 0;
              color: ${FONTGREY_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ProductsSubCategoriesList: state.ProductsSubCategoriesList,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(ProductDetailsPage);
