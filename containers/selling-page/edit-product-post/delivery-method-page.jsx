import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import SwitchInput from "../../../components/input-box/switch-input";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREEN_COLOR,
  GREY_VARIANT_3,
  Border_LightGREY_COLOR_2,
  BG_LightGREY_COLOR,
  THEME_COLOR_Opacity,
  THEME_COLOR,
  InfoIcon,
} from "../../../lib/config";
// reactstrap components
import { InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import Model from "../../../components/model/model";
import SelectInput from "../../../components/input-box/select-input";
import MyLocationModel from "../../filter-sec/my-location-model";

class DeliveryMethodPage extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
  };

  // used to store the switch checked status
  handleChange = (name) => (event) => {
    let inputControl = event;
    this.setState({ [name]: inputControl.target.checked }, () => {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = this.state[name];
      this.setState({
        inputpayload: tempPayload,
      });
    });
  };

  // used to create inputpayload
  handleOnchangeInput = (name) => (e) => {
    let temppayload = { ...this.state.inputpayload };
    temppayload[[name]] = e.target.value;
    this.setState({ inputpayload: temppayload });
  };

  render() {
    const serviceNationwideLabel = (
      <div>
        <h6 className="labelTitle">Sell & Ship Nationwide</h6>
        <p className="labelDesc">
          A low 9.9% service fee applies <a target="_blank">Shipping policy</a>
        </p>
      </div>
    );

    const buyNowEnabledLabel = (
      <div className="position-relative">
        <img src={InfoIcon} alt="info" height={9} width={9} className="infoIcon" />
        <h6 className="labelTitle">Enable Buy Now</h6>
        <p className="labelDesc">Auto-accept first full-price shipping offer</p>
      </div>
    );

    const addressList = this.props.AddressList;

    return (
      <Wrapper>
        <div className="col-12 p-0 DeliveryMethodPage py-3">
          <div className="row m-0 justify-content-center">
            <div className="col-6 pl-0 borderRight">
              <div className="formInput">
                <SwitchInput
                  triggerOnChange={true}
                  label={serviceNationwideLabel}
                  onChange={this.handleChange("isServiceNationwide")}
                  value={this.state.isServiceNationwide}
                  name="isServiceNationwide"
                />
              </div>
              <div className="formInput">
                <SwitchInput
                  triggerOnChange={true}
                  label={buyNowEnabledLabel}
                  onChange={this.handleChange("buyNowEnabled")}
                  value={this.state.buyNowEnabled}
                  name="buyNowEnabled"
                />
              </div>
              <div className="formInput border-0">
                <label className="labelTitle">Ship form:</label>
                <SelectInput
                  id="addressInput"
                  options={addressList}
                  value={this.props.selectedAddress}
                  onChange={this.props.handleAddressSelectInput(`selectedAddress`)}
                  buttonOption={true}
                />
              </div>
            </div>
            <div className="col-6 pr-0">
              <div className="FormInput">
                <label className="labelTitle">Estimate the item’s weight:</label>
                <div className="row m-0">
                  <div className="col-8 p-0">
                    <label className="labelName">Package weight</label>
                    <div className="row m-0 align-items-center">
                      <div className="col-6 p-0 DaysFormInput">
                        <InputGroup>
                          <Input className="packageInput" type="text" name="packageWeightLB" onChange={this.handleOnchangeInput(`packageWeightLB`)} />
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>lb</InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </div>
                      <div className="col-6 p-0 DaysFormInput">
                        <InputGroup>
                          <Input className="packageInput" type="text" name="packageWeightOZ" onChange={this.handleOnchangeInput(`packageWeightOZ`)} />
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>oz</InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row m-0 pt-3">
                  <div className="col-12 p-0">
                    <label className="labelName">Package dimensions</label>
                    <div className="row m-0 align-items-center">
                      <div className="col-4 p-0 DaysFormInput">
                        <InputGroup>
                          <Input className="packageInput" type="text" name="packageLength" onChange={this.handleOnchangeInput(`packageLength`)} />
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>in</InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </div>
                      <div className="col-4 p-0 DaysFormInput">
                        <InputGroup>
                          <Input className="packageInput" type="text" name="packageWidth" onChange={this.handleOnchangeInput(`packageWidth`)} />
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>in</InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </div>
                      <div className="col-4 p-0 DaysFormInput">
                        <InputGroup>
                          <Input className="packageInput" type="text" name="packageHeight" onChange={this.handleOnchangeInput(`packageHeight`)} />
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>in</InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Model open={this.props.addAddressModel} onClose={this.props.handleCloseAddAddress}>
          <MyLocationModel createPost={true} onClose={this.props.handleCloseAddAddress} handleAddAddress={this.props.handleAddAddress} />
        </Model>
        <style jsx>
          {`
            :global(.infoIcon) {
              position: absolute;
              left: 8.7vw;
              top: -2px;
            }
            :global(.labelTitle) {
              font-size: 0.951vw;
              margin-bottom: 0.366vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.021vw !important;
            }
            .borderRight {
              border-right: 0.073vw solid ${Border_LightGREY_COLOR_2};
            }
            .formInput {
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR_2};
              padding: 1.098vw 0;
              margin: 0 1.464vw 0 0;
            }
            .FormInput {
              padding: 1.098vw 0;
              margin: 0 0 0 1.464vw;
            }
            .labelName {
              font-size: 0.805vw;
              margin-bottom: 0.292vw !important;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.labelDesc) {
              font-size: 0.805vw;
              margin: 0;
              max-width: 75%;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
            }
            :global(.labelDesc a) {
              color: ${GREEN_COLOR} !important;
              text-decoration: none;
              font-family: "Open Sans" !important;
            }
            .DaysFormInput {
              position: relative;
            }
            :global(.DaysFormInput > div) {
              width: 90%;
              border: 0.073vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
            }
            :global(.DaysFormInput > div > div) {
              height: 2.342vw;
              width: 2.635vw !important;
            }
            :global(.DaysFormInput > div > div > span) {
              border: none !important;
              font-size: 0.805vw;
              width: 97%;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_2};
              background: ${BG_LightGREY_COLOR};
            }
            :global(.packageInput) {
              padding: 0.146vw 0.585vw;
              height: 2.342vw;
              font-size: 0.805vw;
              border: none !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.packageInput:focus) {
              color: #495057;
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity} THEME_COLOR;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(DeliveryMethodPage);
