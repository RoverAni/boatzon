import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import { getYearsList } from "../../../lib/date-operation/date-operation";
import { DatePicker } from "antd";
import SelectTopLabel from "../../../components/input-box/select-top-label";
import InputBox from "../../../components/input-box/input-box";
import {
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
  THEME_COLOR,
  GREY_VARIANT_3,
  WHITE_COLOR,
} from "../../../lib/config";
import CheckBox from "../../../components/input-box/check-box";

const YearsList = getYearsList();

class EngineDetailsPage extends Component {
  render() {
    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const FuelTypes =
      this.props.engineData.fuelType &&
      this.props.engineData.fuelType.map((data) => {
        return { value: data, label: data };
      });
    const EngineMakeTypes =
      this.props.engineData.engines &&
      this.props.engineData.engines.map((data) => {
        return { value: data, label: data };
      });
    const EngineTypes =
      this.props.engineData.engineTypes &&
      this.props.engineData.engineTypes.map((data) => {
        return { value: data, label: data };
      });

    const NumOfEngines = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
    ];
    const { enginenotAvaiable } = this.props;
    return (
      <Wrapper>
        <div className="col-12 p-0 EngineDetailsPage">
          <div className="row m-0 justify-content-center">
            <div className="col-6 pl-0">
              <div className="selectInputSec">
                <SelectTopLabel
                  editPost={true}
                  label="Engine Make"
                  value={this.props.engineDetails[0].engine_make}
                  placeholder="Engine Make Types"
                  options={EngineMakeTypes}
                  isDisabled={enginenotAvaiable ? true : false}
                  onChange={this.props.handleOnSelectInput(`engine_make`)}
                />
              </div>
              <div className="FormInput sectionInput">
                <label>Engine Model</label>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="engine_model"
                  value={this.props.engineDetails[0].engine_model}
                  placeholder="Engine Model"
                  onChange={this.props.handleEngineDetailsInput}
                  disabled={enginenotAvaiable ? true : false}
                  autoComplete="off"
                ></InputBox>
              </div>
              <div className="row m-0 align-items-center">
                <div className="col-4 p-0">
                  <div className="selectInputSec m-0">
                    <SelectTopLabel
                      editPost={true}
                      label="Year"
                      value={this.props.engineDetails[0].engine_year}
                      placeholder="ex: 2013"
                      options={YearOptions}
                      isDisabled={enginenotAvaiable ? true : false}
                      onChange={this.props.handleOnSelectInput(`engine_year`)}
                    />
                  </div>
                </div>
                <div className="col-4 p-0 px-2">
                  <div className="FormInput">
                    <label>Horse Power</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="engine_horse_power"
                      value={this.props.engineDetails[0].engine_horse_power}
                      placeholder="90"
                      disabled={enginenotAvaiable ? true : false}
                      onChange={this.props.handleEngineDetailsInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-4 p-0">
                  <div className="selectInputSec m-0">
                    <SelectTopLabel
                      value={this.props.num_of_engines}
                      editPost={true}
                      label="Number of Engines"
                      placeholder="5"
                      options={NumOfEngines}
                      isDisabled={enginenotAvaiable ? true : false}
                      onChange={this.props.handleOnSelectInput(
                        `num_of_engines`
                      )}
                    />
                  </div>
                </div>
              </div>
              <div className="mb-2">
                <CheckBox
                  checked={enginenotAvaiable}
                  plaincheckbox={true}
                  onChange={this.props.handleOnchangeCheckbox(
                    "enginenotAvaiable"
                  )}
                  value="enginenotAvaiable"
                  label="This boat does not have an engine"
                />
              </div>
            </div>
            <div className="col-6 pr-0">
              <div className="row">
                <div className="col-6">
                  <div className="FormInput sectionRow sectionInput">
                    <label>Engine Hours</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="engine_hour"
                      value={this.props.engineDetails[0].engine_hour}
                      placeholder="7600"
                      disabled={enginenotAvaiable ? true : false}
                      onChange={this.props.handleEngineDetailsInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-6">
                  <div className="selectInputSec">
                    <SelectTopLabel
                      editPost={true}
                      value={this.props.engineDetails[0].engine_type}
                      label="Engine Type"
                      placeholder="Electric"
                      options={EngineTypes}
                      isDisabled={enginenotAvaiable ? true : false}
                      onChange={this.props.handleOnSelectInput(`engine_type`)}
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="selectInputSec m-0">
                    <SelectTopLabel
                      editPost={true}
                      label="Fuel Type"
                      value={this.props.engineDetails[0].engine_fuel_type}
                      placeholder="Diesel"
                      options={FuelTypes}
                      isDisabled={enginenotAvaiable ? true : false}
                      onChange={this.props.handleOnSelectInput(
                        `engine_fuel_type`
                      )}
                    />
                  </div>
                </div>
                <div className="col-6">
                  {/* {this.props.finalpayload.hasWarranty == "Yes" ? ( */}
                  <div className="FormInput sectionInput">
                    <label>Warranty Till?</label>
                    <DatePicker
                      picker="month"
                      format="MM-YYYY"
                      placeholder="MM-YYYY"
                      disabled={enginenotAvaiable ? true : false}
                      value={this.props.engineDetails[0].warrantyDate}
                      onChange={this.props.handleMonthPicker}
                      disabledDate={this.props.disabledDate}
                    />
                  </div>
                  {/* ) : (
                    ""
                  )} */}
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            :global(.EngineDetailsPage .selectInputSec) {
              margin-top: 1.098vw;
            }
            .sectionRow {
              margin-top: 1.098vw !important;
            }
            .sectionInput {
              padding: 0.366vw 0;
            }
            :global(.EngineDetailsPage .FormInput label) {
              font-size: 0.805vw !important;
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
              padding: 0;
              color: ${FONTGREY_COLOR};
            }
            .engineContLabel {
              font-size: 0.951vw !important;
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
              padding: 0.732vw 0 0 0;
              color: ${FONTGREY_COLOR};
            }
            :global(.EngineDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              margin: 0.366vw 0;
              padding: 0.439vw 0.585vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.EngineDetailsPage .FormInput .inputBox:focus),
            :global(.EngineDetailsPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.EngineDetailsPage .FormInput .zipcodeInput) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.ant-picker-dropdown) {
              z-index: 1500 !important;
              font-size: 0.805vw !important;
            }
            :global(.ant-picker-month-panel) {
              width: 14.641vw !important;
            }
            :global(.ant-picker-content) {
              height: 14.641vw !important;
            }
            :global(.ant-picker) {
              cursor: pointer;
              width: 100%;
              padding: 0.439vw 0.585vw;
              margin: 0.366vw 0;
              height: 2.342vw;
              border: none;
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              font-size: 0.805vw !important;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-disabled) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.ant-picker-focused) {
              outline: 0;
              box-shadow: none !important;
            }
            :global(.ant-picker-input input) {
              font-size: 0.805vw !important;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-input input::placeholder) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    engineData: state.engineData,
  };
};

export default connect(mapStateToProps)(EngineDetailsPage);
