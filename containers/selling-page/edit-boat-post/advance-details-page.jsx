import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  THEME_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  BG_LightGREY_COLOR,
  THEME_COLOR_Opacity,
} from "../../../lib/config";
// reusable component
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { withStyles } from "@material-ui/core";
import clsx from "clsx";
import SelectTopLabel from "../../../components/input-box/select-top-label";
// reactstrap components
import { InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import SelectInput from "../../../components/input-box/select-input";

const styles = (theme) => ({
  radiogrouRoot: {
    flexDirection: "row",
  },
  radioLabelRoot: {
    margin: "0",
  },
  radioLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Museo-Sans !important",
  },
  label: {
    color: `${FONTGREY_COLOR}`,
    margin: "0",
    fontSize: "0.951vw",
    fontFamily: "Museo-Sans !important",
  },
  root: {
    paddingLeft: "0",
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  checked: {
    color: `${THEME_COLOR} !important`,
  },
  plainicon: {
    borderRadius: 3,
    width: "1.024vw",
    height: "1.024vw",
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto rgba(19,124,189,.6)",
      outlineOffset: 2,
    },
    "input:hover ~ &": {
      backgroundColor: "#ebf1f5",
    },
    "input:disabled ~ &": {
      boxShadow: "none",
      background: "rgba(206,217,224,.5)",
    },
  },
  plaincheckedIcon: {
    backgroundColor: `${WHITE_COLOR}`,
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: "1.024vw",
      height: "1.024vw",
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%232A3656'/%3E%3C/svg%3E\")",
      content: '""',
    },
    "input:hover ~ &": {
      backgroundColor: `${WHITE_COLOR}`,
    },
  },
});

class AdvanceDetailsPage extends Component {
  state = {
    inputpayload: {},
    newAddress: "",
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = inputControl.value;
      let selectPayload = { ...this.state[name] };
      selectPayload.label = inputControl.label;
      selectPayload.value = inputControl.value;
      this.setState(
        {
          inputpayload: tempPayload,
          [name]: { ...selectPayload },
        },
        () => {
          //   this.checkIfFormValid();
          console.log("PAYLOAD", this.state.inputpayload);
        }
      );
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: tempPayload,
        [inputControl.name]: inputControl.value,
      },
      () => {
        console.log("PAYLOAD", this.state.inputpayload);
      }
    );
  };

  render() {
    const { classes, trailerValue } = this.props;

    const FuelTypes =
      this.props.engineData.fuelType &&
      this.props.engineData.fuelType.map((data) => {
        return { value: data, label: data };
      });

    const Hull_Material =
      this.props.engineData.hullMaterial &&
      this.props.engineData.hullMaterial.map((data) => {
        return { value: data, label: data };
      });

    const Hull_Shape_Types =
      this.props.engineData.hullShape &&
      this.props.engineData.hullShape.map((data) => {
        return { value: data, label: data };
      });

    const HeadsNum = [
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
      { value: "6", label: "6" },
      { value: "7", label: "7" },
      { value: "8", label: "8" },
      { value: "9", label: "9" },
      { value: "10", label: "10" },
    ];

    return (
      <Wrapper>
        <div className="col-12 p-0 AdvancedDetailsPage">
          <div className="row m-0 justify-content-center">
            <div className="col-5 pl-0 borderRight mt-3">
              <h6 className="subHeading">General</h6>
              <div>
                <FormControl component="fieldset">
                  <FormLabel
                    classes={{
                      root: classes.label,
                    }}
                    component="legend"
                  >
                    Trailer
                  </FormLabel>
                  <RadioGroup
                    classes={{
                      root: classes.radiogrouRoot,
                    }}
                    aria-label="trailerAvaibility"
                    name="trailerAvaibility"
                    value={trailerValue}
                    onChange={this.props.handleTrailerValue}
                  >
                    <FormControlLabel
                      classes={{
                        root: classes.radioLabelRoot,
                        label: classes.radioLabel,
                      }}
                      value="yes"
                      control={
                        <Radio
                          classes={{
                            root: classes.root,
                            checked: classes.checked,
                          }}
                          checkedIcon={
                            <span
                              className={clsx(
                                classes.plainicon,
                                classes.plaincheckedIcon
                              )}
                            />
                          }
                          icon={<span className={classes.plainicon} />}
                        />
                      }
                      label="Yes"
                    />
                    <FormControlLabel
                      classes={{
                        root: classes.radioLabelRoot,
                        label: classes.radioLabel,
                      }}
                      value="no"
                      control={
                        <Radio
                          classes={{
                            root: classes.root,
                            checked: classes.checked,
                          }}
                          checkedIcon={
                            <span
                              className={clsx(
                                classes.plainicon,
                                classes.plaincheckedIcon
                              )}
                            />
                          }
                          icon={<span className={classes.plainicon} />}
                        />
                      }
                      label="No"
                    />
                  </RadioGroup>
                </FormControl>
              </div>
              <div className="selectInputSec m-0 mr-3">
                <SelectTopLabel
                  editPost={true}
                  label="Fuel Type"
                  value={this.props.boat_fuel_type}
                  placeholder="Gas"
                  options={FuelTypes}
                  onChange={this.props.handleOnSelectInput(`boat_fuel_type`)}
                />
              </div>
              <div className="selectInputSec m-0 mr-3">
                <SelectTopLabel
                  editPost={true}
                  value={this.props.hull_material}
                  label="Hull Material"
                  placeholder="Ferro-Cement"
                  options={Hull_Material}
                  onChange={this.props.handleOnSelectInput(`hull_material`)}
                />
              </div>
              <div className="selectInputSec m-0 mr-3">
                <SelectTopLabel
                  editPost={true}
                  label="Hull Shape"
                  value={this.props.hull_shape}
                  placeholder="Semi Displacement"
                  options={Hull_Shape_Types}
                  onChange={this.props.handleOnSelectInput(`hull_shape`)}
                />
              </div>
            </div>
            <div className="col-7 pr-0 mt-3">
              <div>
                <div className="mb-3">
                  <h6 className="subHeading">Dimensions & Weights</h6>
                  <div className="row m-0 pt-1">
                    <div className="col-12 p-0">
                      <div className="row m-0 align-items-center">
                        <div className="col-4 p-0 DaysFormInput">
                          <label className="labelName">Nominal Length</label>
                          <InputGroup>
                            <Input
                              className="packageInput"
                              type="text"
                              name="nominalLength"
                              value={this.props.nominalLength}
                              onChange={this.props.handleOnchangeInput}
                            />
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>ft</InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </div>
                        <div className="col-4 p-0 DaysFormInput">
                          <label className="labelName">Overall Length</label>
                          <InputGroup>
                            <Input
                              className="packageInput"
                              type="text"
                              value={this.props.overallLength}
                              name="overallLength"
                              onChange={this.props.handleOnchangeInput}
                            />
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>ft</InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </div>
                        <div className="col-4 p-0 DaysFormInput">
                          <label className="labelName">Beam</label>
                          <InputGroup className="w-100">
                            <Input
                              className="packageInput"
                              type="text"
                              value={this.props.beam}
                              name="beam"
                              onChange={this.props.handleOnchangeInput}
                            />
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>ft</InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </div>
                      </div>
                    </div>
                    <div className="row m-0">
                      <div className="col-12 p-0 pt-2">
                        <div className="row m-0 align-items-center">
                          <div className="col-4 p-0 DaysFormInput">
                            <label className="labelName">Draft</label>
                            <InputGroup>
                              <Input
                                className="packageInput"
                                type="text"
                                value={this.props.draft}
                                name="draft"
                                onChange={this.props.handleOnchangeInput}
                              />
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>in</InputGroupText>
                              </InputGroupAddon>
                            </InputGroup>
                          </div>
                          <div className="col-4 p-0 DaysFormInput">
                            <label className="labelName">Dry Weight</label>
                            <InputGroup className="w-100">
                              <Input
                                className="packageInput"
                                type="text"
                                value={this.props.dry_weight}
                                name="dry_weight"
                                onChange={this.props.handleOnchangeInput}
                              />
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>lb</InputGroupText>
                              </InputGroupAddon>
                            </InputGroup>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mb-3">
                  <h6 className="subHeading">Tanks & Accommodations</h6>
                  <div className="row m-0 align-items-center">
                    <div className="col-4 p-0 DaysFormInput">
                      <label className="labelName">Fuel Tank Capacity</label>
                      <InputGroup>
                        <Input
                          className="packageInput"
                          type="text"
                          value={this.props.fuelTank_capacity}
                          name="fuelTank_capacity"
                          onChange={this.props.handleOnchangeInput}
                        />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>gal</InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                    <div className="col-4 p-0">
                      <label className="labelName m-0">Heads</label>
                      <div>
                        <SelectInput
                          placeholder="8"
                          value={this.props.heads_count}
                          options={HeadsNum}
                          onChange={this.props.handleOnSelectInput(
                            `heads_count`
                          )}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .borderRight {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .subHeading {
              font-size: 0.951vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${FONTGREY_COLOR};
              margin: 0 0 0.878vw 0;
            }
            :global(.AdvancedDetailsPage .selectInputSec) {
              margin-top: 1.098vw;
            }
            :global(.DaysFormInput > div) {
              width: 90%;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              height: 2.342vw;
              margin: 0.366vw 0;
            }
            :global(.DaysFormInput > div > div) {
              height: auto;
              width: 2.635vw !important;
            }
            :global(.DaysFormInput > div > div > span) {
              border: none !important;
              font-size: 0.805vw;
              width: 97%;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              background: ${BG_LightGREY_COLOR};
            }
            :global(.packageInput) {
              padding: 0.146vw 0.585vw;
              height: auto;
              font-size: 0.805vw;
              border: none !important;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.packageInput:focus) {
              color: #495057;
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            .labelName {
              font-size: 0.805vw;
              margin: 0;
              color: ${FONTGREY_COLOR};
              text-transform: "capitalize";
              font-family: "Open Sans-SemiBold" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    engineData: state.engineData,
  };
};

export default connect(mapStateToProps)(withStyles(styles)(AdvanceDetailsPage));
