import React, { Component } from "react";
import Wrapper from "../../../hoc/Wrapper";
import {
  Long_Arrow_Left_Blue,
  THEME_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  Edit_Icon,
  Border_LightGREY_COLOR,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  VIDEO_UPLOAD_SIZE,
  IMAGE_UPLOAD_SIZE,
} from "../../../lib/config";
import ButtonComp from "../../../components/button/button";
import InputBox from "../../../components/input-box/input-box";
import Tabs from "../../../components/tabs/tabs";
import BoatOverviewPage from "./boat-overview-page";
import BoatDetailsPage from "./boat-details-page";
import EngineDetailsPage from "./engine-details-page";
import AdvanceDetailsPage from "./advance-details-page";
import Snackbar from "../../../components/snackbar";
import currencies from "../../../translations/currency.json";
import moment from "moment";
import { editPost } from "../../../services/edit-post";

const addAddress = (
  <div className="addAddress_btn">
    <ButtonComp>Add Address</ButtonComp>
  </div>
);

class EditBoatPost extends Component {
  state = {
    editTitle: false,
    inputpayload: {},
    images: [],
    categoryDropdown: false,
    AddressList: [
      {
        value: "",
        label: addAddress,
      },
    ],
    selectedAddress: {},
    addAddressModel: false,
    newAddress: "",
    apiImagePayload: [],
    negotiable: false,
    enginenotAvaiable: false,
    engineDetails: [],
  };

  componentDidMount() {
    const {
      productName,
      description,
      mainUrl,
      imageUrl1,
      condition,
      manufacturer,
      manufacturerId,
      subCategory,
      price,
      currency,
      postFilter,
      cloudinaryPublicId,
      cloudinaryPublicId1,
      imageCount,
      negotiable,
      containerWidth,
      containerHeight,
      year,
      engineCount,
      boatModel,
      hasWarranty,
      length,
      trailer,
      fuelType,
      hullMaterial,
      hullShape,
      zipCode,
      nominalLength,
      overallLength,
      beam,
      draft,
      dryWeight,
      heads,
      fuelTankCapacity,
    } = this.props.selectedBoat;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[`productName`] = productName;
    tempPayload[`description`] = description;
    const subCategoryName =
      postFilter &&
      postFilter.map((data) => {
        return data.values;
      });
    const currencySymbol = this.handleCurrencySymbol(currency);
    let tempAddPayload = [...this.state.AddressList];
    let currentAddress = {
      value: {
        address: this.props.selectedBoat.place,
        lat: this.props.selectedBoat.latitude,
        lng: this.props.selectedBoat.longitude,
        city: this.props.selectedBoat.city,
        countrySname: this.props.selectedBoat.countrySname,
      },
      label: this.props.selectedBoat.place,
    };
    tempAddPayload.unshift(currentAddress);

    let apiImagePayload = [...this.state.apiImagePayload];
    let imagepayload = {};
    imagepayload.url = mainUrl;
    let mainUrlType = mainUrl.split(".");
    imagepayload.urlType =
      mainUrlType[mainUrlType.length - 1] == "jpeg" ||
      mainUrlType[mainUrlType.length - 1] == "png"
        ? "0"
        : mainUrlType[mainUrlType.length - 1] == "mp4"
        ? "1"
        : "";
    imagepayload.cloudinaryPublicId = cloudinaryPublicId;
    imagepayload.containerHeight = containerHeight;
    imagepayload.containerWidth = containerWidth;
    apiImagePayload.push(imagepayload);
    // if (imageCount > 1) {
    //   let tempimageCount = imageCount;
    //   for (let i = tempimageCount; i > 1; i--) {
    //     let otherimgpayload = {};
    //     otherimgpayload.url = this.props.selectedBoat[`imageUrl${i - 1}`];
    //     let otherUrlType = this.props.selectedBoat[`imageUrl${i - 1}`].split(
    //       "."
    //     );
    //     otherimgpayload.urlType =
    //       otherUrlType[otherUrlType.length - 1] == "jpeg" ||
    //       otherUrlType[otherUrlType.length - 1] == "png"
    //         ? "0"
    //         : otherUrlType[otherUrlType.length - 1] == "mp4"
    //         ? "1"
    //         : "";
    //     otherimgpayload.cloudinaryPublicId = this.props.selectedBoat[
    //       `cloudinaryPublicId${i - 1}`
    //     ];
    //     apiImagePayload.push(otherimgpayload);
    //   }
    // }
    let engineDetails = [];

    let engineObject = {};
    engineObject.engine_make = {
      value: this.props.selectedBoat[`engineMake1`],
      label: this.props.selectedBoat[`engineMake1`],
    };
    engineObject.engine_model = this.props.selectedBoat[`engineModel1`];
    engineObject.engine_year = {
      value: this.props.selectedBoat[`engineYear1`],
      label: this.props.selectedBoat[`engineYear1`],
    };
    engineObject.engine_horse_power = this.props.selectedBoat[
      `engineHorsePower1`
    ];
    engineObject.engine_hour = this.props.selectedBoat[`engineHour1`];
    engineObject.engine_type = this.props.selectedBoat[`engineType1`];
    engineObject.engine_fuel_type = {
      value: this.props.selectedBoat[`engineFuelType1`],
      label: this.props.selectedBoat[`engineFuelType1`],
    };
    engineObject.warrantyDate = moment(
      this.props.selectedBoat[`warrantyDate1`]
    );
    engineDetails.push(engineObject);

    let tempImagesPayload =
      apiImagePayload &&
      apiImagePayload.map((data, index) => {
        return {
          id: index + 1,
          name: data.url,
        };
      });
    this.setState(
      {
        engineDetails,
        apiImagePayload,
        inputpayload: { ...tempPayload },
        images: [...tempImagesPayload],
        condition: condition ? { value: condition, label: condition } : "",
        manufacturer: manufacturer
          ? { value: manufacturerId, label: manufacturer }
          : "",
        subCategoryName: subCategoryName ? subCategoryName[0] : "",
        boat_type: subCategory
          ? { value: subCategory, label: subCategory }
          : "",
        currency: currencySymbol
          ? { value: currency, label: currencySymbol }
          : "",
        price: price ? price : "",
        selectedAddress: { ...currentAddress },
        negotiable: negotiable ? Boolean(negotiable) : "",
        boat_year: year ? { value: year, label: year } : "",
        num_of_engines: engineCount
          ? { value: engineCount, label: engineCount }
          : "",
        boat_model: boatModel ? boatModel : "",
        hasWarranty: hasWarranty
          ? { value: hasWarranty, label: hasWarranty }
          : "",
        boat_length: length ? length : "",
        zipCode: zipCode ? zipCode : "",
        trailerValue: trailer ? trailer : "",
        boat_fuel_type: fuelType ? { value: fuelType, label: fuelType } : "",
        hull_material: hullMaterial
          ? { value: hullMaterial, label: hullMaterial }
          : "",
        hull_shape: hullShape ? { value: hullShape, label: hullShape } : "",
        nominalLength: nominalLength ? nominalLength : "",
        overallLength: overallLength ? overallLength : "",
        beam: beam ? beam : "",
        draft: draft ? draft : "",
        dry_weight: dryWeight ? dryWeight : "",
        heads_count: heads ? { value: heads, label: heads } : "",
        fuelTank_capacity: fuelTankCapacity ? fuelTankCapacity : "",
      },
      () => {
        console.log("fnjdhe", this.state.selectedAddress);
        let tempInputpayload = { ...this.state.inputpayload };
        tempInputpayload.selectedAddress = this.state.selectedAddress.value;
        let filterLabel = `${this.state.categoryName} Type`;
        tempInputpayload.filter = {
          [`${filterLabel}`]: `${this.state.subCategoryName}`,
        };
        this.setState(
          {
            AddressList: [...tempAddPayload],
            inputpayload: { ...tempInputpayload },
          },
          () => {
            // this.checkIfFormValid();
            console.log("PAYLOAD--1", this.state.inputpayload);
          }
        );
      }
    );
  }

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  handleMonthPicker = (event) => {
    if (event) {
      let Date = event._d;
      let tempengineDetails = [...this.state.engineDetails];
      tempengineDetails[engineCount - 1][[`warrantyDate`]] = moment(Date);
      this.setState({
        engineDetails: [...tempengineDetails],
      });
    } else if (event == null) {
      let tempengineDetails = [...this.state.engineDetails];
      tempengineDetails[engineCount - 1][[`warrantyDate`]] = "";
      this.setState({
        engineDetails: [...tempengineDetails],
      });
    }
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    this.setState({
      inputpayload: { ...tempsignUpPayload },
      [inputControl.name]: inputControl.value,
    });
  };

  handleEngineDetailsInput = (event) => {
    let inputControl = event.target;
    if (
      inputControl.name == "engine_model" ||
      inputControl.name == "engine_horse_power" ||
      inputControl.name == "engine_hour"
    ) {
      let tempengineDetails = [...this.state.engineDetails];
      tempengineDetails[0][[inputControl.name]] =
        inputControl.value && inputControl.value.length
          ? inputControl.value
          : "";
      this.setState({
        engineDetails: [...tempengineDetails],
      });
    }
  };

  handleEditTitle = () => {
    this.setState({
      editTitle: true,
    });
  };

  handleTrailerValue = (event) => {
    this.setState({
      trailerValue: event.target.value,
    });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // used to upload image from local storage
  handleUploadPic = async (event) => {
    let files = event.target.files;
    let file_type = files[0].type.split("/")[0];
    if (
      files &&
      files[0] &&
      (files[0].type == "image/jpeg" || files[0].type == "image/png")
    ) {
      const data = new FormData();
      data.append("file", files[0]);
      data.append("upload_preset", "avzsfq8q");
      const res = await fetch(
        `https://api.cloudinary.com/v1_1/boatzon/${file_type}/upload`,
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      try {
        if (file) {
          let apiImagePayload = [...this.state.apiImagePayload];
          let imagePayload = {};
          imagePayload.url = file.secure_url;
          imagePayload.cloudinaryPublicId = file.public_id;
          imagePayload.urlType =
            file.resource_type == "image"
              ? "0"
              : file.resource_type == "video"
              ? "1"
              : "";
          // imagePayload.containerWidth = file.width;
          // imagePayload.containerHeight = file.height;
          apiImagePayload.push(imagePayload);
          let ProductImg = [...this.state.images];
          let tempImagesObject = {};
          tempImagesObject.id = ProductImg.length + 1;
          tempImagesObject.name = file.secure_url;
          ProductImg.push(tempImagesObject);
          if (this.isFileSizeValid(files[0])) {
            this.setState(
              {
                productpic: true,
                fileErr: null,
                isDocValid: true,
              },
              () => {
                // let tempInputPayload = { ...this.state.inputpayload };
                // tempInputPayload.productImgs = [...ProductImg];
                this.setState(
                  {
                    apiImagePayload,
                    images: [...ProductImg],
                    // inputpayload: tempInputPayload,
                  },
                  () => {
                    // console.log("PAYLOAD", this.state.inputpayload);
                  }
                );
              }
            );
          } else {
            this.setState({
              fileErr:
                files[0].type == "video"
                  ? "File size should be less than 4MB!"
                  : "File size should be less than 2MB!",
              isDocValid: false,
            });
          }
        }
      } catch (err) {
        console.log("imager url error", err);
        this.setState({
          fileErr: "imager url error",
          isDocValid: false,
          productpic: false,
        });
      }
    } else {
      this.setState({
        fileErr:
          "File format is invalid! Image should be of .jpeg or .png format",
        isDocValid: false,
      });
    }
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    switch (fileData.type.split("/")[0]) {
      case "video":
        return fileData.size <= VIDEO_UPLOAD_SIZE;
        break;
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };

  handleReorderImage = (newOrder) => {
    this.setState(
      {
        images: newOrder,
      },
      () => {
        let apiImagePayload = [...this.state.apiImagePayload];
        const final = this.state.images.map((data) =>
          apiImagePayload.find((item) => item.url == data.name)
        );
        this.setState({
          apiImagePayload: [...final],
        });
      }
    );
  };

  handleDeleteImage = (imageObject) => {
    let tempImages = [...this.state.images];
    let elementIndex =
      tempImages &&
      tempImages.findIndex((data) => {
        return data.id == imageObject.id;
      });
    elementIndex != -1 ? tempImages.splice(elementIndex, 1) : "";

    let tempApiImagePayload = [...this.state.apiImagePayload];
    let elemntIdApiImagePayload =
      tempApiImagePayload &&
      tempApiImagePayload.findIndex((data) => {
        return data.url == imageObject.name;
      });
    elemntIdApiImagePayload != -1
      ? tempApiImagePayload.splice(elemntIdApiImagePayload, 1)
      : "";

    this.setState({
      images: [...tempImages],
      apiImagePayload: [...tempApiImagePayload],
    });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;

    if (
      name == "engine_make" ||
      name == "engine_year" ||
      name == "engine_type" ||
      name == "engine_fuel_type"
    ) {
      let tempengineDetails = [...this.state.engineDetails];
      let tempValue = {};
      tempValue.value = inputControl.value;
      tempValue.label = inputControl.label;
      tempengineDetails[0][[name]] = { ...tempValue };
      this.setState({
        engineDetails: [...tempengineDetails],
      });
    } else {
      if (inputControl && inputControl.value) {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = inputControl.value;
        let tempValue = {};
        tempValue.value = inputControl.value;
        tempValue.label = inputControl.label;
        this.setState({
          inputpayload: { ...temppayload },
          [name]: { ...tempValue },
        });
      }
    }
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    let tempInputPayload = { ...this.state.inputpayload };
    tempInputPayload[name] = event.target.checked;
    this.setState(
      {
        [name]: event.target.checked,
        inputpayload: { ...tempInputPayload },
      },
      () => {
        if (this.state.inputpayload.enginenotAvaiable) {
          let InputPayload = { ...this.state.inputpayload };
          InputPayload[`num_of_engines`] = 0;
          let engineObject = {};
          engineObject.engine_make = {};
          engineObject.engine_model = "";
          engineObject.engine_year = {};
          engineObject.engine_horse_power = "";
          engineObject.engine_hour = "";
          engineObject.engine_type = "";
          engineObject.engine_fuel_type = {};
          engineObject.warrantyDate = "";
          this.setState({
            inputpayload: { ...InputPayload },
            num_of_engines: {
              value: 0,
              label: 0,
            },
            engineDetails: [{ ...engineObject }],
          });
        } else if (!this.state.inputpayload.enginenotAvaiable) {
          let engineDetails = [...this.state.engineDetails];
          let engineObject = {};
          engineObject.engine_make = {
            value: this.props.selectedBoat[`engineMake1`],
            label: this.props.selectedBoat[`engineMake1`],
          };
          engineObject.engine_model = this.props.selectedBoat[`engineModel1`];
          engineObject.engine_year = {
            value: this.props.selectedBoat[`engineYear1`],
            label: this.props.selectedBoat[`engineYear1`],
          };
          engineObject.engine_horse_power = this.props.selectedBoat[
            `engineHorsePower1`
          ];
          engineObject.engine_hour = this.props.selectedBoat[`engineHour1`];
          engineObject.engine_type = this.props.selectedBoat[`engineType1`];
          engineObject.engine_fuel_type = {
            value: this.props.selectedBoat[`engineFuelType1`],
            label: this.props.selectedBoat[`engineFuelType1`],
          };
          engineObject.warrantyDate = moment(
            this.props.selectedBoat[`warrantyDate1`]
          );
          this.setState({
            engineDetails: [{ ...engineObject }],
            num_of_engines: this.props.selectedBoat.engineCount
              ? {
                  value: this.props.selectedBoat.engineCount,
                  label: this.props.selectedBoat.engineCount,
                }
              : "",
          });
        }
      }
    );
  };

  handleEditPostProduct = () => {
    let tempOtherImageUrls =
      this.state.apiImagePayload &&
      this.state.apiImagePayload.filter((data, index) => {
        return index > 0;
      });
    let finalPayload = {
      postId: this.props.selectedBoat.postId,
      mainUrl: this.state.apiImagePayload[0].url,
      thumbnailUrl: this.state.apiImagePayload[0].url,
      mainUrlType: this.state.apiImagePayload[0].urlType,
      thumbnailImageUrl: this.state.apiImagePayload[0].url,
      cloudinaryPublicId: this.state.apiImagePayload[0].cloudinaryPublicId,
      otherUrls: JSON.stringify([...tempOtherImageUrls]),
      imageCount: this.state.apiImagePayload.length,
      containerHeight: this.props.selectedBoat.containerHeight,
      containerWidth: this.props.selectedBoat.containerWidth,

      price: this.state.price,
      currency: this.state.currency.value,
      productName: this.state.inputpayload.productName,
      condition: this.state.condition.value,
      negotiable: this.state.inputpayload.negotiable ? "1" : "0",

      category: this.props.selectedBoat.category,
      manufactor: this.state.manufacturer.label,
      manufactorId: this.state.manufacturer.value.toString(),
      location:
        this.props.currentLocation && this.props.currentLocation.address,
      latitude:
        this.props.currentLocation && this.props.currentLocation.latitude,
      longitude:
        this.props.currentLocation && this.props.currentLocation.longitude,
      city: this.props.currentLocation && this.props.currentLocation.city,
      countrySname:
        this.props.currentLocation &&
        this.props.currentLocation.countryShortName,
      subCategory: this.state.boat_type.value,
      zipCode: this.state.zipCode,
      engineDetails: JSON.stringify([...this.state.engineDetails]),
      hasEngine: this.state.enginenotAvaiable ? false : true,
      engineCount: this.state.num_of_engines,
      hasWarranty: this.state.hasWarranty.value,
      trailer: this.state.trailerValue,
      fuelType: this.state.boat_fuel_type.value,
      hullMaterial: this.state.hull_material.value,
      hullShape: this.state.hull_shape.value,
      nominalLength: this.state.nominalLength,
      overallLength: this.state.overallLength,
      beam: this.state.beam,
      draft: this.state.draft,
      dryWeight: this.state.dry_weight,
      fuelTankCapacity: this.state.fuelTank_capacity,
      heads: this.state.heads_count.values,
      length: parseInt(this.state.boat_length, 10),
      year: this.state.boat_year,
      boatModel: this.state.boat_model,
      isSwap: 0, // 0: false, 1: true
      businessPost: false, // 1: true, 2: false
      title: this.state.inputpayload.productName,
      description: this.state.inputpayload.description,
      nationWide: "false",
    };

    editPost(finalPayload)
      .then((res) => {
        console.log("RESPONSE--->>>", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            // window.location.reload();
          }
        }
      })
      .catch((err) => {
        console.log("ERROR", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204 || 422:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      default:
        return "error";
        break;
    }
  };

  render() {
    const Overview = (
      <BoatOverviewPage
        selectedBoat={this.props.selectedBoat}
        handleUploadPic={this.handleUploadPic}
        images={this.state.images}
        handleReorderImage={this.handleReorderImage}
        handleDeleteImage={this.handleDeleteImage}
        inputpayload={this.state.inputpayload}
        handleOnchangeInput={this.handleOnchangeInput}
      />
    );
    const Boats_Details = (
      <BoatDetailsPage
        selectedBoat={this.props.selectedBoat}
        handleOnSelectInput={this.handleOnSelectInput}
        boat_type={this.state.boat_type}
        manufacturer={this.state.manufacturer}
        handleOnchangeInput={this.handleOnchangeInput}
        boat_model={this.state.boat_model}
        condition={this.state.condition}
        boat_year={this.state.boat_year}
        boat_length={this.state.boat_length}
        hasWarranty={this.state.hasWarranty}
        currency={this.state.currency}
        price={this.state.price}
        zipCode={this.state.zipCode}
        handleOnchangeCheckbox={this.handleOnchangeCheckbox}
        negotiable={this.state.negotiable}
      />
    );
    const Engine_Details = (
      <EngineDetailsPage
        engineDetails={this.state.engineDetails}
        handleOnSelectInput={this.handleOnSelectInput}
        handleEngineDetailsInput={this.handleEngineDetailsInput}
        handleMonthPicker={this.handleMonthPicker}
        disabledDate={this.disabledDate}
        enginenotAvaiable={this.state.enginenotAvaiable}
        handleOnchangeCheckbox={this.handleOnchangeCheckbox}
        num_of_engines={this.state.num_of_engines}
      />
    );
    const Advanced_Details = (
      <AdvanceDetailsPage
        handleTrailerValue={this.handleTrailerValue}
        trailerValue={this.state.trailerValue}
        handleOnSelectInput={this.handleOnSelectInput}
        boat_fuel_type={this.state.boat_fuel_type}
        hull_material={this.state.hull_material}
        hull_shape={this.state.hull_shape}
        handleOnchangeInput={this.handleOnchangeInput}
        nominalLength={this.state.nominalLength}
        overallLength={this.state.overallLength}
        beam={this.state.beam}
        draft={this.state.draft}
        dry_weight={this.state.dry_weight}
        heads_count={this.state.heads_count}
        fuelTank_capacity={this.state.fuelTank_capacity}
      />
    );

    const { productName } = { ...this.state.inputpayload };
    return (
      <Wrapper>
        <div className="back_btn">
          <ButtonComp onClick={this.props.handleBackScreen.bind(this, 0)}>
            <img src={Long_Arrow_Left_Blue} className="backarrowIcon"></img>{" "}
            Back to My Listing
          </ButtonComp>
        </div>
        <div className="col-12 editBoatPostPage">
          <div className="row m-0">
            <div className="col-6 p-0">
              <div className="titleSec">
                {this.state.editTitle ? (
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="productName"
                    value={productName}
                    onChange={this.handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                ) : (
                  <div className="heading">
                    <h6>{productName}</h6>
                    <img
                      src={Edit_Icon}
                      className="editIcon"
                      onClick={this.handleEditTitle}
                    ></img>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row m-0 my-3">
            <Tabs
              followingsmalltabs={true}
              tabs={[
                {
                  label: `Overview`,
                },
                {
                  label: `Boat Details`,
                },
                {
                  label: `Engine Details`,
                },
                {
                  label: `Advanced Details`,
                },
              ]}
              tabcontent={[
                { content: Overview },
                { content: Boats_Details },
                { content: Engine_Details },
                { content: Advanced_Details },
              ]}
            />
          </div>
          <div className="row m-0 align-items-center justify-content-end">
            <div className="col-4 p-0">
              <div className="row m-0 d-flex align-items-center justify-content-end">
                <div className="col-5 pl-0">
                  <div className="cancel_btn">
                    <ButtonComp
                      onClick={this.props.handleBackScreen.bind(this, 0)}
                    >
                      Cancel
                    </ButtonComp>
                  </div>
                </div>
                <div className="col-7 p-0">
                  <div className="save_btn">
                    <ButtonComp onClick={this.handleEditPostProduct}>
                      Save
                    </ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        <style jsx>
          {`
            .back_btn {
              position: absolute;
              top: -2.562vw;
              left: 3%;
            }
            :global(.back_btn button) {
              min-width: fit-content !important;
              width: fit-content;
              padding: 0.732vw 0 1.464vw 0;
              background: transparent;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.back_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.cancel_btn button) {
              min-width: 100% !important;
              width: 100%;
              padding: 0.951vw 0;
              background: ${Border_LightGREY_COLOR};
              border-radius: 0.146vw;
              position: relative;
              border: none;
            }
            :global(.cancel_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize;
              line-height: 1;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.save_btn button) {
              min-width: 100% !important;
              width: 100%;
              padding: 0.951vw 0;
              background: ${GREEN_COLOR};
              border-radius: 0.146vw;
              position: relative;
              border: none;
            }
            :global(.save_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
              color: ${BG_LightGREY_COLOR};
              line-height: 1;
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .backarrowIcon {
              width: 0.805vw;
              margin-right: 0.292vw;
            }
            .editIcon {
              position: absolute;
              top: 50%;
              right: 0.732vw;
              transform: translate(0, -50%);
              display: none;
              cursor: pointer;
            }
            .heading {
              height: 2.635vw;
              display: flex;
              align-items: center;
              justify-content: flex-start;
            }
            .heading:hover {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
            }
            .heading:hover .editIcon {
              display: block;
            }
            .heading h6 {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0 0.732vw;
            }
            :global(.editBoatPostPage .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.editBoatPostPage .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EditBoatPost;
