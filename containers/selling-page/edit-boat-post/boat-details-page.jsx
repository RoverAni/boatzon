import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import SelectTopLabel from "../../../components/input-box/select-top-label";
import { getYearsList } from "../../../lib/date-operation/date-operation";
import currencies from "../../../translations/currency.json";
import InputBox from "../../../components/input-box/input-box";
import {
  GREY_VARIANT_3,
  THEME_COLOR,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
} from "../../../lib/config";
import SelectInput from "../../../components/input-box/select-input";
import { NumberValidator } from "../../../lib/validation/validation";
import CheckBox from "../../../components/input-box/check-box";

const YearsList = getYearsList();
let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class BoatDetailsPage extends Component {
  state = {
    selectedType: [],
    selectedManufacturerType: [],
    lengthValue: [20, 50],
    lengthDropdown: false,
    currency: {
      value: CurrencyArray[0].code,
      label: CurrencyArray[0].symbol_native,
    },
    num_of_engines: {
      value: 1,
      label: 1,
    },
    inputpayload: {
      negotiable: false,
      num_of_engines: 1,
      currency: CurrencyArray[0].code,
    },
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = inputControl.value;
      let selectPayload = { ...this.state[name] };
      selectPayload.label = inputControl.label;
      selectPayload.value = inputControl.value;
      this.setState(
        {
          inputpayload: tempPayload,
          [name]: { ...selectPayload },
        },
        () => {
          //   this.checkIfFormValid();
          console.log("PAYLOAD", this.state.inputpayload);
        }
      );
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: tempPayload,
        [inputControl.name]: inputControl.value,
      },
      () => {
        // this.checkIfFormValid();
        console.log("PAYLOAD", this.state.inputpayload);
      }
    );
  };

  render() {
    const BoatsTypes =
      this.props.BoatsSubCategoriesList &&
      this.props.BoatsSubCategoriesList.map((data) => ({
        value: data.subCategoryName,
        label: data.subCategoryName,
      }));

    const ManufacturersTypes = [
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
    ];

    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const warrantyConditions = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));

    const { negotiable } = this.props;

    return (
      <Wrapper>
        <div className="col-12 p-0 BoatDetailsPage">
          <div className="row m-0 justify-content-center">
            <div className="col-6 pl-0">
              <div className="selectInputSec">
                <SelectTopLabel
                  editPost={true}
                  label="Type"
                  placeholder="All Boat Types"
                  value={this.props.boat_type}
                  options={BoatsTypes}
                  onChange={this.props.handleOnSelectInput(`boat_type`)}
                />
              </div>
              <div className="selectInputSec m-0">
                <SelectTopLabel
                  editPost={true}
                  value={this.props.manufacturer}
                  label="Manufacturer"
                  placeholder="Manufacturer List"
                  options={ManufacturersTypes}
                  onChange={this.props.handleOnSelectInput(`manufacturer`)}
                />
              </div>
              <div className="FormInput sectionInput">
                <label>Model</label>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="boat_model"
                  value={this.props.boat_model}
                  placeholder="VR6 Bowrider"
                  onChange={this.props.handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
              <div className="row m-0 align-items-center justify-content-center editRow">
                <div className="col-6 p-0 pr-2">
                  <SelectTopLabel
                    editPost={true}
                    label="Condition"
                    value={this.props.condition}
                    placeholder="Used"
                    options={Conditions}
                    onChange={this.props.handleOnSelectInput(`condition`)}
                  />
                </div>
                <div className="col-6 p-0 pl-2">
                  <SelectTopLabel
                    editPost={true}
                    value={this.props.boat_year}
                    label="Year"
                    placeholder="ex: 2013"
                    options={YearOptions}
                    onChange={this.props.handleOnSelectInput(`boat_year`)}
                  />
                </div>
              </div>
            </div>
            <div className="col-6 pr-0">
              <div className="row m-0 align-items-center justify-content-center">
                <div className="col-6 p-0 pr-2">
                  <div className="FormInput sectionRow">
                    <label>Length</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="boat_length"
                      value={this.props.boat_length}
                      placeholder="50'"
                      onChange={this.props.handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-6 p-0 pl-2">
                  <div className="selectInputSec">
                    <SelectTopLabel
                      editPost={true}
                      value={this.props.hasWarranty}
                      label="Has Warranty"
                      placeholder="Yes"
                      options={warrantyConditions}
                      onChange={this.props.handleOnSelectInput(`hasWarranty`)}
                    />
                  </div>
                </div>
              </div>
              <div className="row m-0 align-items-center justify-content-center">
                <div className="col-6 p-0 pr-2">
                  <div className="FormInput sectionInput">
                    <label>Price</label>
                    <div className="d-flex align-items-center justify-content-between priceInputContainer">
                      <SelectInput
                        value={this.props.currency}
                        currency={true}
                        placeholder="$"
                        options={CurrencyOptions}
                        onChange={this.props.handleOnSelectInput(`currency`)}
                      />
                      <InputBox
                        type="text"
                        className="priceInputBox form-control"
                        name="price"
                        value={this.props.price}
                        onChange={this.props.handleOnchangeInput}
                        autoComplete="off"
                      ></InputBox>
                    </div>
                  </div>
                </div>
                <div className="col-6 p-0 pl-2">
                  <div className="FormInput">
                    <label className="m-0">Zip Code</label>
                    <InputBox
                      type="text"
                      className="inputBox zipcodeInput form-control"
                      name="zipCode"
                      value={this.props.zipCode}
                      autoComplete="off"
                      onChange={this.props.handleOnchangeInput}
                      onKeyPress={NumberValidator}
                    ></InputBox>
                  </div>
                </div>
              </div>
              <div className="mt-2">
                <CheckBox
                  checked={negotiable}
                  plaincheckbox={true}
                  onChange={this.props.handleOnchangeCheckbox("negotiable")}
                  value="negotiable"
                  label="Is price negotiable?"
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .editRow {
              margin: 0.366vw 0 !important;
            }
            .sectionRow {
              margin-top: 1.098vw !important;
            }
            :global(.BoatDetailsPage .selectInputSec) {
              margin-top: 1.098vw;
            }
            .sectionInput {
              padding: 0.366vw 0;
            }
            :global(.BoatDetailsPage .FormInput label) {
              font-size: 0.805vw !important;
              font-family: "Open Sans-SemiBold" !important;
              margin: 0 !important;
              padding: 0;
              color: ${FONTGREY_COLOR};
            }
            :global(.BoatDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              margin: 0.366vw 0;
              padding: 0.439vw 0.585vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.BoatDetailsPage .FormInput .inputBox:focus),
            :global(.BoatDetailsPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.BoatDetailsPage .FormInput .zipcodeInput) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .priceInputContainer {
              margin: 0.366vw 0;
            }
            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: 100%;
              flex: 1;
            }
            .inputDiv {
              width: auto;
              flex: 3;
            }
            :global(.BoatDetailsPage .FormInput .priceInputBox) {
              width: 100%;
              height: auto;
              padding: 0 0 0 0.292vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.BoatDetailsPage .FormInput > div .priceInputBox:focus),
            :global(.BoatDetailsPage .FormInput > div .priceInputBox:active) {
              outline: none !important;
              box-shadow: none !important;
            }
            :global(.BoatDetailsPage .FormInput > .priceInputContainer) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
              padding: 0 0.292vw;
            }
            :global(.BoatDetailsPage .FormInput .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }

            :global(.BoatDetailsPage .FormInput label) {
              font-size: 0.805vw;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0 0 0.366vw 0;
              padding: 0;
              color: ${FONTGREY_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    BoatsSubCategoriesList: state.BoatsSubCategoriesList,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(BoatDetailsPage);
