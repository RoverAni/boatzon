import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR,
  Archive_Blue_Filled_Icon,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import SellingBoats from "./selling-boats";
import SellingProducts from "./selling-products";
import EditProductPost from "./edit-product-post/edit-product-post";
import EditBoatPost from "./edit-boat-post/edit-boat-post";
import { getPostInsights } from "../../services/my-listing";
import { getChatsWithPageNo } from "../../services/chat";
import { connect } from "react-redux";
import SellingEngines from "./selling-engines";
import EditEnginePost from "./edit-engine-post/edit-engine-post";
import SellingTrailers from "./selling-trailers";
import EditTrailerPost from "./edit-trailer-post/edit-trailer-post";

class SellingPage extends Component {
  state = {
    ProductPosts: [],
    productsTotalView: [],
    boatsTotalView: [],
    tabValue: 0,
  };

  // function to change tabValue
  handleTabChange = (event, value) => {
    this.setState({ tabValue: value });
  };

  componentDidMount() {
    this.setState({
      loader: true,
    });
    let BoatPosts =
      this.props.getOwnPostsData && this.props.getOwnPostsData.length
        ? this.props.getOwnPostsData.filter((data) => {
            return data.category == "Boats";
          })
        : [];
    let ProductPosts =
      this.props.getOwnPostsData && this.props.getOwnPostsData.length
        ? this.props.getOwnPostsData.filter((data) => {
            return data.category == "Products";
          })
        : [];

    if (this.props && this.props.allChats && this.props.allChats.length === 0) {
      getChatsWithPageNo(0)
        .then((res) => {})
        .catch((err) => console.log("selling page [get all chats] error"));
    }

    this.setState(
      {
        ProductPosts,
        BoatPosts,
        loader: false,
      },
      () => {
        this.state.ProductPosts &&
          this.state.ProductPosts.map((item) => {
            this.handleGetProductsTotalViews(item.postId);
          });
        this.state.BoatPosts &&
          this.state.BoatPosts.map((item) => {
            this.handleGetBoatsTotalViews(item.postId);
          });
      }
    );
  }

  handleGetBoatsTotalViews = (postId) => {
    let payload = {
      postId: postId,
    };
    getPostInsights(payload)
      .then((res) => {
        console.log("restotalViews", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            let tempBoatsTotalView = [...this.state.boatsTotalView];
            tempBoatsTotalView.push(
              response.data[0].basicInsight.data[0].totalViews
            );
            this.setState({
              boatsTotalView: [...tempBoatsTotalView],
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleGetProductsTotalViews = (postId) => {
    let payload = {
      postId: postId,
    };
    getPostInsights(payload)
      .then((res) => {
        console.log("restotalViews", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            let tempProductsTotalView = [...this.state.productsTotalView];
            tempProductsTotalView.push(
              response.data[0].basicInsight.data[0].totalViews
            );
            this.setState({
              productsTotalView: [...tempProductsTotalView],
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleEditProductPost = (selectedProduct) => {
    this.updateScreen(
      <EditProductPost
        handleBackScreen={this.handleBackScreen}
        selectedProduct={selectedProduct}
      />
    );
  };

  handleEditBoatPost = (selectedBoat) => {
    this.updateScreen(
      <EditBoatPost
        handleBackScreen={this.handleBackScreen}
        selectedBoat={selectedBoat}
      />
    );
  };

  handleEditEnginePost = (selectedEngine) => {
    this.updateScreen(
      <EditEnginePost
        handleBackScreen={this.handleBackScreen}
        selectedEngine={selectedEngine}
      />
    );
  };

  handleEditTrailerPost = (selectedTrailer) => {
    this.updateScreen(
      <EditTrailerPost
        handleBackScreen={this.handleBackScreen}
        selectedTrailer={selectedTrailer}
      />
    );
  };

  handleBackScreen = (tabValue) => {
    console.log("jidjs", tabValue);
    this.setState(
      {
        tabValue,
      },
      () => {
        this.updateScreen();
      }
    );
  };

  // type of data is boatData or productData
  markAsSoldAndUpdateBoatData = (postId) => {
    let exBoatData = [...this.state.BoatPosts];
    let index = exBoatData.findIndex((k) => k.postId === postId);
    if (index > -1) {
      exBoatData.splice(index, 1);
      this.setState({ BoatPosts: exBoatData });
    }
  };

  markAsSoldAndUpdateProductData = (postId) => {
    let exProductPosts = [...this.state.ProductPosts];
    let index = exProductPosts.findIndex((k) => k.postId === postId);
    if (index > -1) {
      exProductPosts.splice(index, 1);
      this.setState({ ProductPosts: exProductPosts });
    }
  };

  render() {
    const Boats = (
      <SellingBoats
        BoatPosts={this.state.BoatPosts}
        boatsTotalView={this.state.boatsTotalView}
        handleEditBoatPost={this.handleEditBoatPost}
        markAsSoldAndUpdateBoatData={this.markAsSoldAndUpdateBoatData}
        loader={this.state.loader}
      />
    );
    const Products = (
      <SellingProducts
        ProductPosts={this.state.ProductPosts}
        productsTotalView={this.state.productsTotalView}
        handleEditProductPost={this.handleEditProductPost}
        markAsSoldAndUpdateProductData={this.markAsSoldAndUpdateProductData}
        loader={this.state.loader}
      />
    );
    const Engines = (
      <SellingEngines
        boatsTotalView={this.state.boatsTotalView}
        handleEditEnginePost={this.handleEditEnginePost}
        markAsSoldAndUpdateBoatData={this.markAsSoldAndUpdateBoatData}
      />
    );
    const Trailers = (
      <SellingTrailers
        boatsTotalView={this.state.boatsTotalView}
        handleEditTrailerPost={this.handleEditTrailerPost}
        markAsSoldAndUpdateBoatData={this.markAsSoldAndUpdateBoatData}
      />
    );

    const { openPage } = this.props;
    console.log("getOwnPostsData", this.props.getOwnPostsData);
    const { tabValue } = this.state;
    const { handleTabChange } = this;
    return (
      <Wrapper>
        {openPage ? (
          <div className="SellingPage">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0 py-5">
                <div className="col-11 p-0 mx-auto">
                  <div className="row mx-0 py-3 justify-content-center PageContent">
                    {!this.state.currentScreen ? (
                      <div className="col-12 paddingXThree">
                        <h6 className="heading">My Listing</h6>
                        <div className="my-2">
                          <Tabs
                            followingsmalltabs={true}
                            editPostValue={tabValue}
                            handleEditTabChange={handleTabChange}
                            tabs={[
                              {
                                label: `Boats`,
                              },
                              {
                                label: `Products`,
                              },
                              {
                                label: `Engines`,
                              },
                              {
                                label: `Trailers`,
                              },
                            ]}
                            tabcontent={[
                              { content: Boats },
                              { content: Products },
                              { content: Engines },
                              { content: Trailers },
                            ]}
                          />
                        </div>
                      </div>
                    ) : (
                      this.state.currentScreen
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}

        <style jsx>
          {`
            .SellingPage {
              background: ${BG_LightGREY_COLOR};
            }
            .PageContent {
              background: ${WHITE_COLOR};
              max-width: 95%;
              margin: 0 auto !important;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allChats: state.allChats,
  };
};

export default connect(mapStateToProps, null)(SellingPage);
