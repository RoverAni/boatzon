import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  GREY_VARIANT_3,
  GREY_VARIANT_1,
  FONTGREY_COLOR_Dark,
  Make_Offer_Lightgrey_Icon,
  Border_LightGREY_COLOR_2,
  FONTGREY_COLOR,
  BG_LightGREY_COLOR,
  GREEN_COLOR,
} from "../../lib/config";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { withStyles } from "@material-ui/core";
import InputBox from "../../components/input-box/input-box";
import { requiredValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import SelectInput from "../../components/input-box/select-input";
import {
  markAsSold,
  soldElseWhere,
  getBuyersForPost,
} from "../../services/buy-with-boatzon";
import { connect } from "react-redux";

const styles = (theme) => ({
  radiogrouRoot: {
    flexDirection: "column",
  },
  radioLabelRoot: {
    margin: "0",
  },
  radioLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    top: "0.219vw",
    fontSize: "0.878vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Museo-Sans !important",
  },
  root: {
    padding: "0.658vw 0.658vw 0.219vw 0",
    color: `${GREY_VARIANT_3}`,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
    "& span": {
      "& div": {
        "& svg": {
          width: "0.819vw",
        },
      },
    },
  },
  checked: {
    color: `${GREY_VARIANT_1} !important`,
  },
});

class MarkSoldModel extends Component {
  state = {
    value: "SoldTo",
    selectedPersonToSell: "",
    peopleWhoWantToBuyYourStuff: [],
    offerPrice: "",
  };
  handleChange = (event) => {
    console.log("event.target.value", event.target.value);
    if (event.target.value === "SoldElseWhere") {
      this.setState({ offerPrice: "" });
    }
    this.setState({
      value: event.target.value,
    });
  };

  componentDidMount() {
    getBuyersForPost({
      postId: this.props && this.props.boatData && this.props.boatData.postId,
      postType: "0",
    })
      .then((res) => {
        if (res.data.data.length > 0)
          this.setState({ peopleWhoWantToBuyYourStuff: res.data.data });
      })
      .catch((err) => console.log("err getBuyersForPost"));
  }

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    console.log("fejf-->", event.target.value, event.target.name);
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate }, () => {
      //   this.checkIfFormValid();
    });
    console.log("fejf", inputControl);
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    console.log("fejf", tempsignUpPayload);
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
      },
      () => {
        // this.props.dispatch(setSignupData(this.state.inputpayload));
        console.log("inputpayload", this.state.inputpayload);
      }
    );
  };

  callApi = () => {
    let { value } = this.state.selectedPersonToSell;
    console.log("selectedPersonToSell", this.state.selectedPersonToSell);
    let { postId } = this.props.boatData;
    if (this.props.type === "Boat") {
      if (this.state.value === "SoldElseWhere") {
        let obj = {
          postId: postId.toString(),
          type: "0",
          soldType: "0",
        };
        soldElseWhere(obj)
          .then((res) => {
            if (res.data.code === "200" || res.data.code === 200) {
              this.props.markAsSoldAndUpdateBoatData(postId);
              console.log("success selling", res);
              this.props.onClose();
            }
          })
          .catch((err) => console.log("error selling", err));
      } else {
        let obj = {
          postId: postId,
          type: "0",
          membername: value,
          ratings: 4,
        };
        markAsSold(obj)
          .then((res) => {
            if (res.data.code === "200" || res.data.code === 200) {
              this.props.markAsSoldAndUpdateBoatData(postId);
              console.log("success selling", res);
              this.props.onClose();
            }
          })
          .catch((err) => console.log("error selling", err));
      }
    } else {
      if (this.state.value === "SoldElseWhere") {
        let obj = {
          postId: postId.toString(),
          type: "0",
          soldType: "0",
        };
        soldElseWhere(obj)
          .then((res) => {
            if (res.data.code === "200" || res.data.code === 200) {
              this.props.markAsSoldAndUpdateProductData(postId);
              console.log("success selling", res);
              this.props.onClose();
            }
          })
          .catch((err) => console.log("error selling", err));
      } else {
        let obj = {
          postId: postId.toString(),
          type: "0",
          membername: value,
          ratings: "4",
        };
        markAsSold(obj)
          .then((res) => {
            if (res.data.code === "200" || res.data.code === 200) {
              this.props.markAsSoldAndUpdateProductData(postId);
              console.log("success selling", res);
              this.props.onClose();
            }
          })
          .catch((err) => console.log("error selling", err));
      }
    }
  };

  handleOnSelectInput = (event, index) => {
    console.log("selectedPerson", event);
    this.setState({
      selectedPersonToSell: event,
      offerPrice: event.offerPrice,
    });
  };

  render() {
    const { value } = this.state;
    const { classes } = this.props;
    const { soldPrice, username } = { ...this.state.inputpayload };
    let allPeopleOptions = [];
    if (this.state.peopleWhoWantToBuyYourStuff.length > 0) {
      for (let i = 0; i < this.state.peopleWhoWantToBuyYourStuff.length; i++)
        allPeopleOptions.push({
          label: this.state.peopleWhoWantToBuyYourStuff[i].buyername,
          value: this.state.peopleWhoWantToBuyYourStuff[i].buyername,
          postId: this.state.peopleWhoWantToBuyYourStuff[i].postId,
          offerPrice: this.state.peopleWhoWantToBuyYourStuff[i].offerPrice,
        });
    }

    const SoldToLabel = (
      <div className="d-flex align-items-center justify-content-center UsernamSec">
        <label>Sold to</label>
        <SelectInput
          markSold={true}
          value={
            this.state.value === "SoldElseWhere"
              ? ""
              : this.state.selectedPersonToSell.buyername
          }
          options={allPeopleOptions}
          onChange={this.handleOnSelectInput}
        />
      </div>
    );
    return (
      <Wrapper>
        <div className="col-12 p-0 MarkSoldModel">
          <img
            src={Close_Icon}
            className="closeIcon"
            onClick={this.props.onClose}
          ></img>
          <div className="modelContent">
            <h6 className="text-center heading">Boat is Sold</h6>
            <p className="text-center caption">
              Ocean Alexander 120 Grey Marine
            </p>
            <FormControl component="fieldset" className="my-3">
              <RadioGroup
                classes={{
                  root: classes.radiogrouRoot,
                }}
                aria-label="soldStatus"
                name="soldStatus"
                value={value}
                onChange={this.handleChange}
              >
                <FormControlLabel
                  classes={{
                    root: classes.radioLabelRoot,
                    label: classes.radioLabel,
                  }}
                  value="SoldTo"
                  control={
                    <Radio
                      classes={{
                        root: classes.root,
                        checked: classes.checked,
                      }}
                    />
                  }
                  label={SoldToLabel}
                />
                <FormControlLabel
                  classes={{
                    root: classes.radioLabelRoot,
                    label: classes.radioLabel,
                  }}
                  value="SoldElseWhere"
                  control={
                    <Radio
                      classes={{
                        root: classes.root,
                        checked: classes.checked,
                      }}
                    />
                  }
                  label="Not sold on Boatzon"
                />
              </RadioGroup>
            </FormControl>
            <div className="d-flex align-items-center justify-content-center SoldPriceSec">
              <label>
                <img
                  src={Make_Offer_Lightgrey_Icon}
                  className="offerIcon"
                ></img>{" "}
                Sold Price:
              </label>
              <InputBox
                type="text"
                className="inputBox form-control"
                name="soldPrice"
                value={this.state.offerPrice}
                placeholder="Price"
                onChange={this.handleOnchangeInput}
                autoComplete="off"
              ></InputBox>
            </div>
            <div className="submit_btn">
              <ButtonComp onClick={this.callApi}>Submit</ButtonComp>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .modelContent {
              padding: 1.83vw;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              font-size: 1.639vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0;
            }
            .caption {
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .SoldPriceSec {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              margin: 0;
            }
            .offerIcon {
              width: 0.951vw;
              margin-right: 0.366vw;
              margin-bottom: 0.219vw;
            }
            .SoldPriceSec label {
              width: 100%;
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            :global(.modelContent .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.modelContent .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.modelContent .inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
            :global(.UsernamSec label) {
              width: 100%;
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0 1.098vw 0 0;
            }
            :global(.modelContent .userName) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.modelContent .submit_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 1.098vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
              border: none;
            }
            :global(.modelContent .submit_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.951vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.modelContent .submit_btn button:focus),
            :global(.modelContent .submit_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.modelContent .submit_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allChats: state.allChats,
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(styles)(MarkSoldModel));
