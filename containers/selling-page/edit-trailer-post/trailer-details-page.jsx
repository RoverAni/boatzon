import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../../hoc/Wrapper";
import { getYearsList } from "../../../lib/date-operation/date-operation";
import { DatePicker } from "antd";
import SelectTopLabel from "../../../components/input-box/select-top-label";
import InputBox from "../../../components/input-box/input-box";
import {
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
  THEME_COLOR,
  GREY_VARIANT_3,
  WHITE_COLOR,
  GREY_VARIANT_2,
} from "../../../lib/config";
import SelectInput from "../../../components/input-box/select-input";
import currencies from "../../../translations/currency.json";

const YearsList = getYearsList();
let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class TrailerDetailsPage extends Component {
  render() {
    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const TrailerTypeOptions =
      this.props.TrailerData &&
      this.props.TrailerData.length &&
      this.props.TrailerData.map((data) => {
        return { value: data, label: data };
      });

    const ManufacturersTypes = [
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
    ];

    const NumOfAxies = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
    ];

    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    const warrantyConditions = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));

    const {
      condition,
      trailerType,
      manufacturer,
      trailerYear,
      trailer_length,
      num_of_axies,
      hasWarranty,
      warrantyDate,
      currency,
      price,
      handleOnSelectInput,
      handleOnchangeInput,
      handleMonthPicker,
      disabledDate,
    } = this.props;
    console.log("jfwij", hasWarranty);
    return (
      <Wrapper>
        <div className="col-12 p-0 TrailerDetailsPage">
          <div className="row m-0 justify-content-center">
            <div className="col-6 pl-0">
              <div className="selectInputSec m-0">
                <SelectTopLabel
                  editPost={true}
                  label="Condition"
                  value={condition}
                  options={Conditions}
                  onChange={handleOnSelectInput(`condition`)}
                />
              </div>
              <div className="selectInputSec m-0">
                <SelectTopLabel
                  editPost={true}
                  label="Trailer Type"
                  value={trailerType}
                  options={TrailerTypeOptions}
                  onChange={handleOnSelectInput(`trailerType`)}
                />
              </div>
              <div className="selectInputSec m-0">
                <SelectTopLabel
                  editPost={true}
                  value={manufacturer}
                  label="Manufacturer"
                  placeholder="Manufacturer List"
                  options={ManufacturersTypes}
                  onChange={handleOnSelectInput(`manufacturer`)}
                />
              </div>
              <div className="row m-0 align-items-center">
                <div className="col-6 p-0">
                  <div className="selectInputSec m-0">
                    <SelectTopLabel
                      editPost={true}
                      label="Year"
                      value={trailerYear}
                      options={YearOptions}
                      onChange={handleOnSelectInput(`trailerYear`)}
                    />
                  </div>
                </div>
                <div className="col-6 p-0">
                  <div className="selectInputSec m-0">
                    <SelectTopLabel
                      value={num_of_axies}
                      editPost={true}
                      label="Axies"
                      options={NumOfAxies}
                      onChange={handleOnSelectInput(`num_of_axies`)}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6 pr-0">
              <div className="row">
                <div className="col-6">
                  <div className="FormInput">
                    <label>Length</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="trailer_length"
                      value={trailer_length}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-6">
                  <div className="selectInputSec m-0">
                    <SelectTopLabel
                      editPost={true}
                      label="Has Warranty"
                      value={hasWarranty}
                      options={warrantyConditions}
                      onChange={handleOnSelectInput(`hasWarranty`)}
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                {hasWarranty && hasWarranty.value == "Yes" ? (
                  <div className="col-6">
                    <div className="FormInput sectionInput">
                      <label>Warranty Till?</label>
                      <DatePicker
                        picker="month"
                        format="MM-YYYY"
                        value={warrantyDate}
                        onChange={handleMonthPicker}
                        disabledDate={disabledDate}
                      />
                    </div>
                  </div>
                ) : (
                  ""
                )}
                <div className="col-6">
                  <div className="FormInput sectionInput">
                    <label>Price</label>
                    <div className="d-flex align-items-center justify-content-between priceInputContainer">
                      <SelectInput
                        value={currency}
                        currency={true}
                        options={CurrencyOptions}
                        onChange={handleOnSelectInput(`currency`)}
                      />
                      <InputBox
                        type="text"
                        className="priceInputBox form-control"
                        name="price"
                        value={price}
                        onChange={handleOnchangeInput}
                        autoComplete="off"
                      ></InputBox>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            :global(.TrailerDetailsPage .selectInputSec) {
              margin-top: 1.098vw;
            }
            .sectionRow {
              margin-top: 1.098vw !important;
            }
            .sectionInput {
              padding: 0.366vw 0;
            }
            :global(.TrailerDetailsPage .FormInput label) {
              font-size: 0.805vw !important;
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
              padding: 0;
              color: ${FONTGREY_COLOR};
            }
            .engineContLabel {
              font-size: 0.951vw !important;
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
              padding: 0.732vw 0 0 0;
              color: ${FONTGREY_COLOR};
            }
            :global(.TrailerDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              margin: 0.366vw 0;
              padding: 0.439vw 0.585vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.TrailerDetailsPage .FormInput .inputBox:focus),
            :global(.TrailerDetailsPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.TrailerDetailsPage .FormInput .zipcodeInput) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.ant-picker-dropdown) {
              z-index: 1500 !important;
              font-size: 0.805vw !important;
            }
            :global(.ant-picker-month-panel) {
              width: 14.641vw !important;
            }
            :global(.ant-picker-content) {
              height: 14.641vw !important;
            }
            :global(.ant-picker) {
              cursor: pointer;
              width: 100%;
              padding: 0.439vw 0.585vw;
              margin: 0.366vw 0;
              height: 2.342vw;
              border: none;
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              font-size: 0.805vw !important;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-disabled) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.ant-picker-focused) {
              outline: 0;
              box-shadow: none !important;
            }
            :global(.ant-picker-input input) {
              font-size: 0.805vw !important;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-input input::placeholder) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }

            .priceInputContainer {
              margin: 0.366vw 0;
            }
            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: 100%;
              flex: 1;
            }
            .inputDiv {
              width: auto;
              flex: 3;
            }
            :global(.TrailerDetailsPage .FormInput .priceInputBox) {
              width: 100%;
              height: auto;
              padding: 0 0 0 0.292vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.TrailerDetailsPage .FormInput > div .priceInputBox:focus),
            :global(.TrailerDetailsPage
                .FormInput
                > div
                .priceInputBox:active) {
              outline: none !important;
              box-shadow: none !important;
            }
            :global(.TrailerDetailsPage .FormInput > .priceInputContainer) {
              border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
              border-radius: 0.146vw;
              padding: 0 0.292vw;
            }
            :global(.TrailerDetailsPage
                .FormInput
                .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    engineData: state.engineData,
    TrailerData: state.TrailerData,
  };
};

export default connect(mapStateToProps)(TrailerDetailsPage);
