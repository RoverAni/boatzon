import React, { Component } from "react";
import Wrapper from "../../../hoc/Wrapper";
import {
  Long_Arrow_Left_Blue,
  THEME_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  Edit_Icon,
  Border_LightGREY_COLOR,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  VIDEO_UPLOAD_SIZE,
  IMAGE_UPLOAD_SIZE,
} from "../../../lib/config";
import ButtonComp from "../../../components/button/button";
import InputBox from "../../../components/input-box/input-box";
import Tabs from "../../../components/tabs/tabs";
import TrailerOverviewPage from "./trailer-overview-page";
import TrailerDetailsPage from "./trailer-details-page";
import Snackbar from "../../../components/snackbar";
import currencies from "../../../translations/currency.json";
import moment from "moment";
import { editTrailerPost } from "../../../services/edit-post";
import { __BlobUpload } from "../../../lib/image/cloudinaryUpload";

const addAddress = (
  <div className="addAddress_btn">
    <ButtonComp>Add Address</ButtonComp>
  </div>
);

class EditTrailerPost extends Component {
  state = {
    editTitle: false,
    inputpayload: {},
    images: [],
    newAddress: "",
    newImages: false,
    apiImagePayload: [],
    tabValue: 0,
  };

  componentDidMount() {
    const {
      title,
      description,
      mainUrl,
      mainUrlType,
      condition,
      price,
      currency,
      cloudinaryPublicId,
      imageCount,
      containerWidth,
      containerHeight,
      hasWarranty,
      trailerType,
      year,
      warrantyDate,
      manufactor,
      manufactorId,
      axles,
      length,
    } = this.props.selectedTrailer;

    console.log("gugu", this.props.selectedTrailer);
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[`title`] = title;
    tempPayload[`description`] = description;

    const currencySymbol = this.handleCurrencySymbol(currency);

    // image payload
    let apiImagePayload = [...this.state.apiImagePayload];
    let imagepayload = {};
    imagepayload.url = mainUrl;
    imagepayload.urlType = mainUrlType;
    imagepayload.cloudinaryPublicId = cloudinaryPublicId;
    imagepayload.containerHeight = containerHeight;
    imagepayload.containerWidth = containerWidth;
    apiImagePayload.push(imagepayload);
    if (imageCount > 1) {
      let tempimageCount = imageCount;
      for (let i = tempimageCount; i > 1; i--) {
        let otherimgpayload = {};
        otherimgpayload.url = this.props.selectedTrailer[`imageUrl${i - 1}`];
        let otherUrlType = this.props.selectedTrailer[`imageUrl${i - 1}`].split(
          "."
        );
        otherimgpayload.urlType =
          otherUrlType[otherUrlType.length - 1] == "jpeg" ||
          otherUrlType[otherUrlType.length - 1] == "png"
            ? "0"
            : otherUrlType[otherUrlType.length - 1] == "mp4"
            ? "1"
            : "";
        otherimgpayload.cloudinaryPublicId = this.props.selectedTrailer[
          `cloudinaryPublicId${i - 1}`
        ];
        apiImagePayload.push(otherimgpayload);
      }
    }

    let tempImagesPayload =
      apiImagePayload &&
      apiImagePayload.map((data, index) => {
        return {
          id: index + 1,
          name: data.url,
        };
      });

    this.setState({
      apiImagePayload,
      inputpayload: { ...tempPayload },
      images: [...tempImagesPayload],
      condition: condition ? { value: condition, label: condition } : "",
      trailerType: trailerType
        ? { value: trailerType, label: trailerType }
        : "",
      manufacturer: manufactor
        ? { value: manufactorId, label: manufactor }
        : "",
      trailerYear: year ? { value: year, label: year } : "",
      trailer_length: length ? length : "",
      num_of_axies: axles ? { value: axles, label: axles } : "",
      hasWarranty: hasWarranty
        ? { value: hasWarranty, label: hasWarranty }
        : "",
      warrantyDate:
        warrantyDate !== "null" || warrantyDate !== null
          ? moment(warrantyDate)
          : "",
      price: price ? price : "",
      currency: currencySymbol
        ? { value: currency, label: currencySymbol }
        : "",
    });
  }

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  handleMonthPicker = (event) => {
    if (event) {
      let Date = event._d;
      this.setState({
        warrantyDate: moment(Date),
      });
    } else if (event == null) {
      this.setState({
        warrantyDate: "",
      });
    }
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    this.setState({
      inputpayload: { ...tempsignUpPayload },
      [inputControl.name]: inputControl.value,
    });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      if (name == "hasWarranty") {
        this.setState({
          [name]: { ...inputControl },
          warrantyDate: "",
        });
      } else {
        this.setState({
          [name]: { ...inputControl },
        });
      }
    }
  };

  // Function to toggle edit title input
  handleEditTitle = () => {
    this.setState({
      editTitle: true,
    });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // used to create image payload
  handleFileHandler = (imgItem, event) => {
    let files = event.target.files;
    if (
      files &&
      files[0] &&
      (files[0].type == "image/jpeg" || files[0].type == "image/png")
    ) {
      if (this.isFileSizeValid(files[0])) {
        let arr = [...this.state.images];
        if (imgItem == null) {
          arr.push({
            fileDetails: files[0],
            name: URL.createObjectURL(files[0]),
            id: arr.length + 1,
          });
        }
        // else {
        //   arr[imgItem.id - 1] = {
        //     fileDetails: files[0],
        //     name: URL.createObjectURL(files[0]),
        //     id: imgItem,
        //   };
        // }
        this.setState({
          fileErr: null,
          images: [...arr],
          isDocValid: true,
          newImages: true,
        });
      } else {
        this.setState({
          fileErr:
            files[0].type == "video"
              ? "File size should be less than 4MB!"
              : "File size should be less than 2MB!",
          isDocValid: false,
        });
      }
    } else {
      this.setState({
        fileErr:
          "File format is invalid! Image should be of .jpeg or .png format",
        isDocValid: false,
      });
    }
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    switch (fileData.type.split("/")[0]) {
      case "video":
        return fileData.size <= VIDEO_UPLOAD_SIZE;
        break;
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };

  // used to reorder the images
  handleReorderImage = (newOrder) => {
    this.setState({
      images: newOrder,
    });
  };

  // used to delete the images
  handleDeleteImage = (imageObject) => {
    let tempImages = [...this.state.images];
    let elementIndex =
      tempImages &&
      tempImages.findIndex((data) => {
        return data.id == imageObject.id;
      });
    elementIndex != -1 ? tempImages.splice(elementIndex, 1) : "";

    let tempApiImagePayload = [...this.state.apiImagePayload];
    let elemntIdApiImagePayload =
      tempApiImagePayload &&
      tempApiImagePayload.findIndex((data) => {
        return data.url == imageObject.name;
      });
    elemntIdApiImagePayload != -1
      ? tempApiImagePayload.splice(elemntIdApiImagePayload, 1)
      : "";

    this.setState({
      images: [...tempImages],
      apiImagePayload: [...tempApiImagePayload],
    });
  };

  // used to upload image from local storage
  handleUploadPic = () => {
    if (this.state.newImages) {
      let imgArr = [...this.state.images];
      for (let i = 0; i < imgArr.length; i++) {
        if (imgArr[i].fileDetails) {
          __BlobUpload(imgArr[i].fileDetails)
            .then((res) => {
              console.log("feduf-->image upload success", res);
              let imgUrlType = res.body.secure_url.split(".");
              let payload = {
                url: res.body.secure_url,
                urlType:
                  imgUrlType[imgUrlType.length - 1] == "jpeg" ||
                  imgUrlType[imgUrlType.length - 1] == "png"
                    ? "0"
                    : imgUrlType[imgUrlType.length - 1] == "mp4"
                    ? "1"
                    : "",
                cloudinaryPublicId: res.body.public_id,
                containerHeight: res.body.height,
                containerWidth: res.body.width,
              };
              let imagePayload = [...this.state.apiImagePayload];
              imagePayload.push(payload);
              this.setState(
                {
                  apiImagePayload: [...imagePayload],
                },
                () => {
                  if (i == imgArr.length - 1) {
                    this.handleEditPostProduct();
                  }
                }
              );
            })
            .catch((err) => {
              console.log("feduf-->err uploading image", err);
            });
        }
      }
    } else {
      this.handleEditPostProduct();
    }
  };

  handleEditPostProduct = async () => {
    const {
      apiImagePayload,
      inputpayload,
      price,
      currency,
      condition,
      hasWarranty,
      warrantyDate,
      trailerType,
      trailer_length,
      num_of_axies,
      trailerYear,
    } = this.state;
    const { selectedTrailer } = this.props;
    let imagePayload = [...apiImagePayload];
    console.log("nfif", imagePayload);

    let tempOtherImageUrls =
      imagePayload &&
      imagePayload.filter((data, index) => {
        return index > 0;
      });
    console.log("nfif", tempOtherImageUrls);
    let finalPayload = {
      postId: selectedTrailer.postId,
      mainUrl: imagePayload[0].url,
      mainUrlType:
        imagePayload[0].urlType == "image"
          ? "0"
          : imagePayload[0].urlType == "video"
          ? "1"
          : imagePayload[0].urlType,
      thumbnailUrl: imagePayload[0].url,
      thumbnailImageUrl: imagePayload[0].url,
      otherUrls: JSON.stringify([...tempOtherImageUrls]),
      imageCount: imagePayload.length,
      containerHeight: imagePayload[0].imgHeight,
      containerWidth: imagePayload[0].imgWidth,
      price: price,
      currency: currency.value,
      title: inputpayload.title,
      condition: condition.value,
      category: selectedTrailer.category,
      description: inputpayload.description,
      cloudinaryPublicId: imagePayload[0].cloudinaryPublicId,
      location: selectedTrailer.location,
      latitude: selectedTrailer.latitude,
      longitude: selectedTrailer.longitude,
      city: selectedTrailer.city,
      countrySname: selectedTrailer.countrySname,
      stateSname: selectedTrailer.stateSname, // undefined from API
      zipCode: selectedTrailer.zipCode,
      hasWarranty: hasWarranty.value,
      warrantyDate: warrantyDate,
      trailerType: trailerType.value,
      length: trailer_length,
      axles: num_of_axies.value,
      year: trailerYear.value,
      manufactor: this.state.manufacturer.label,
      manufactorId: this.state.manufacturer.value.toString(),
    };

    editTrailerPost(finalPayload)
      .then((res) => {
        console.log("RESPONSE--->>>", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.props && this.props.handleBackScreen.bind(this, 2);
          }
        }
      })
      .catch((err) => {
        console.log("ERROR", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204 || 422:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      default:
        return "error";
        break;
    }
  };

  // function to change tabValue
  handleTabChange = (event, value) => {
    this.setState({ tabValue: value });
  };

  render() {
    const {
      images,
      inputpayload,
      condition,
      trailerType,
      manufacturer,
      engineModel,
      trailerYear,
      trailer_length,
      num_of_axies,
      engineHour,
      engineType,
      engineFuelType,
      warrantyDate,
      currency,
      hasWarranty,
      price,
      tabValue,
      editTitle,
      variant,
      usermessage,
      open,
      vertical,
      horizontal,
    } = this.state;
    const {
      handleFileHandler,
      handleReorderImage,
      handleDeleteImage,
      handleOnchangeInput,
      handleOnSelectInput,
      handleMonthPicker,
      disabledDate,
      handleTabChange,
      handleUploadPic,
      handleEditTitle,
      handleSnackbarClose,
    } = this;
    const { handleBackScreen } = this.props;
    const { title } = { ...this.state.inputpayload };
    console.log("jfwij", hasWarranty);

    const Overview = (
      <TrailerOverviewPage
        handleFileHandler={handleFileHandler}
        images={images}
        handleReorderImage={handleReorderImage}
        handleDeleteImage={handleDeleteImage}
        inputpayload={inputpayload}
        handleOnchangeInput={handleOnchangeInput}
      />
    );

    const Trailer_Details = (
      <TrailerDetailsPage
        condition={condition}
        trailerType={trailerType}
        manufacturer={manufacturer}
        engineModel={engineModel}
        trailerYear={trailerYear}
        trailer_length={trailer_length}
        num_of_axies={num_of_axies}
        engineHour={engineHour}
        engineType={engineType}
        engineFuelType={engineFuelType}
        warrantyDate={warrantyDate}
        currency={currency}
        hasWarranty={hasWarranty}
        price={price}
        handleOnSelectInput={handleOnSelectInput}
        handleOnchangeInput={handleOnchangeInput}
        handleMonthPicker={handleMonthPicker}
        disabledDate={disabledDate}
      />
    );

    return (
      <Wrapper>
        <div className="engine_back_btn">
          <ButtonComp onClick={handleBackScreen.bind(this, 3)}>
            <img src={Long_Arrow_Left_Blue} className="backarrowIcon"></img>{" "}
            Back to My Listing
          </ButtonComp>
        </div>
        <div className="col-12 editBoatPostPage">
          <div className="row m-0">
            <div className="col-6 p-0">
              <div className="titleSec">
                {editTitle ? (
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="title"
                    value={title}
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                ) : (
                  <div className="heading">
                    <h6>{title}</h6>
                    <img
                      src={Edit_Icon}
                      className="editIcon"
                      onClick={handleEditTitle}
                    ></img>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row m-0 my-3">
            <Tabs
              followingsmalltabs={true}
              editPostValue={tabValue}
              handleEditTabChange={handleTabChange}
              tabs={[
                {
                  label: `Overview`,
                },
                {
                  label: `Trailer Details`,
                },
              ]}
              tabcontent={[{ content: Overview }, { content: Trailer_Details }]}
            />
          </div>
          <div className="row m-0 align-items-center justify-content-end">
            <div className="col-4 p-0">
              <div className="row m-0 d-flex align-items-center justify-content-end">
                <div className="col-5 pl-0">
                  <div className="cancel_btn">
                    <ButtonComp onClick={handleBackScreen.bind(this, 2)}>
                      Cancel
                    </ButtonComp>
                  </div>
                </div>
                <div className="col-7 p-0">
                  <div className="save_btn">
                    <ButtonComp onClick={handleUploadPic}>Save</ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={variant}
          message={usermessage}
          open={open}
          onClose={handleSnackbarClose}
          vertical={vertical}
          horizontal={horizontal}
        />
        <style jsx>
          {`
            .engine_back_btn {
              position: absolute;
              top: -2.562vw;
              left: 3%;
            }
            :global(.engine_back_btn button) {
              min-width: fit-content !important;
              width: fit-content;
              padding: 0.732vw 0 1.464vw 0;
              background: transparent;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.engine_back_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.engine_back_btn button:focus),
            :global(.engine_back_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.engine_back_btn button:hover) {
              background: transparent;
            }
            :global(.cancel_btn button) {
              min-width: 100% !important;
              width: 100%;
              padding: 0.951vw 0;
              background: ${Border_LightGREY_COLOR};
              border-radius: 0.146vw;
              position: relative;
              border: none;
            }
            :global(.cancel_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize;
              line-height: 1;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.save_btn button) {
              min-width: 100% !important;
              width: 100%;
              padding: 0.951vw 0;
              background: ${GREEN_COLOR};
              border-radius: 0.146vw;
              position: relative;
              border: none;
            }
            :global(.save_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
              color: ${BG_LightGREY_COLOR};
              line-height: 1;
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .backarrowIcon {
              width: 0.805vw;
              margin-right: 0.292vw;
            }
            .editIcon {
              position: absolute;
              top: 50%;
              right: 0.732vw;
              transform: translate(0, -50%);
              display: none;
              cursor: pointer;
            }
            .heading {
              height: 2.635vw;
              display: flex;
              align-items: center;
              justify-content: flex-start;
            }
            .heading:hover {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
            }
            .heading:hover .editIcon {
              display: block;
            }
            .heading h6 {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0 0.732vw;
            }
            :global(.editBoatPostPage .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.editBoatPostPage .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EditTrailerPost;
