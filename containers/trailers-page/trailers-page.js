import React, { Component } from "react";
import CustomBreadCrumbs from "../../components/breadcrumbs/breadcrumbs";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import TrailersFilters from "./trailers-filters";
import { connect } from "react-redux";
import { getCurrentLocation } from "../../redux/actions/location/location";
import TrailersList from "./trailers-list";
import TrailersGrids from "./trailers-grids";
import debounce from "lodash.debounce";
import { isEmpty } from "../../lib/global";

class TrailersPage extends Component {
  state = {
    condition: "",
    lengthDropdown: false,
    lengthValue: [5, 100],
    minLength: 5,
    maxLength: 100,
    radiusDropdown: false,
    inputpayload: {},
    engineHoursDropdown: false,
    engineHoursValue: [0, 4000],
    filterToggle: false,
    yearDropdown: false,
    yearsValue: [1970, 2017],
    minYears: 1970,
    maxYears: 2017,
    fromYear: 1970,
    toYear: 2017,
    yearsRange: [1970, 2017],
    breadcrumbsData: ["Trailers"],
  };

  componentDidMount() {
    if (!isEmpty(this.props.urlString)) {
      const {
        condition,
        manufactor,
        fromLength,
        toLength,
        hasWarranty,
        postedWithin,
        distanceMax,
        fromLoan,
        toLoan,
        fromPrice,
        toPrice,
        fromYear,
        toYear,
        trailerType,
        axles,
      } = { ...this.props.urlString };

      // price payload
      let tempPayload = { ...this.state.inputpayload };
      tempPayload.minPrice = fromPrice;
      tempPayload.maxPrice = toPrice;

      // year payload
      let tempYearValue = [];
      fromYear
        ? tempYearValue.push(parseInt(fromYear, 10))
        : this.state.minYears;
      toYear ? tempYearValue.push(parseInt(toYear, 10)) : this.state.maxYears;

      // length payload
      let templengthValue = [];
      fromLength
        ? templengthValue.push(parseInt(fromLength, 10))
        : this.state.minLength;
      toLength
        ? templengthValue.push(parseInt(toLength, 10))
        : this.state.maxLength;

      // Manufacturer payload
      let manufactureIdList = manufactor && manufactor.split(",");
      let tempPay =
        manufactureIdList?.length > 0
          ? manufactureIdList.map((data) => {
              return { label: data };
            })
          : "";

      // postedWithin payload
      let tempPostedWithin =
        postedWithin == "24hr"
          ? { value: postedWithin, label: "last 24 hr" }
          : postedWithin == "15days"
          ? { value: postedWithin, label: "last 15 days" }
          : postedWithin == "30days"
          ? { value: postedWithin, label: "last 30 days" }
          : "";

      const FinanceTypeOptions = [
        { value: "0000000000", label: "All" },
        { value: 149, label: "Trailers under $149/mo" },
        { value: 299, label: "Trailers under $299/mo" },
        { value: 499, label: "Trailers under $499/mo" },
        { value: 699, label: "Trailers under $699/mo" },
        { value: 999, label: "Trailers under $999/mo" },
        { value: 1500, label: "Trailers under $1500/mo" },
        { value: 2000, label: "Trailers under $2000/mo" },
        { value: 2500, label: "Trailers under $2500/mo" },
      ];

      let tempBudget =
        FinanceTypeOptions?.length > 0 && toLoan
          ? FinanceTypeOptions.filter((data) => data.value == toLoan)
          : "";

      let tempbreadcrumbsData = [...this.state.breadcrumbsData];

      let value =
        manufactureIdList?.length > 0
          ? manufactureIdList[manufactureIdList.length - 1]
          : null;

      tempbreadcrumbsData[1] = value;
      this.setState(
        {
          inputpayload: { ...tempPayload },
          condition: condition,
          manufactor: tempPay,
          lengthValue: templengthValue,
          minLength: fromLength ? fromLength : this.state.minLength,
          maxLength: toLength ? toLength : this.state.maxLength,
          fromLength: fromLength
            ? parseInt(fromLength, 10)
            : this.state.minLength,
          toLength: toLength ? parseInt(toLength, 10) : this.state.maxLength,
          LengthRange: templengthValue,
          valueRadius: distanceMax ? distanceMax : 0,
          Budget: tempBudget,
          minPrice: fromPrice ? fromPrice : "",
          maxPrice: toPrice ? toPrice : "",
          yearsValue: tempYearValue,
          minYears: fromYear ? fromYear : this.state.minYears,
          maxYears: toYear ? toYear : this.state.maxYears,
          fromYear: fromYear ? parseInt(fromYear, 10) : this.state.minYears,
          toYear: toYear ? parseInt(toYear, 10) : this.state.maxYears,
          yearsRange: tempYearValue,
          hasWarranty: hasWarranty,
          trailerType: { value: trailerType, label: trailerType },
          axles: { value: axles, label: axles },
          breadcrumbsData: [...tempbreadcrumbsData],
          postedWithin: { ...tempPostedWithin },
        },
        () => {
          let apiPayload = { ...this.state.payload, ...this.props.urlString };
          if (fromLength && toLength) {
            apiPayload.length = {
              fromLength,
              toLength,
            };
            delete apiPayload.fromLength;
            delete apiPayload.toLength;
          }
          if (fromLoan && toLoan) {
            apiPayload.loan = {
              fromLoan,
              toLoan,
            };
            delete apiPayload.fromLoan;
            delete apiPayload.toLoan;
          }
          if (fromYear && toYear) {
            apiPayload.year = {
              fromYear,
              toYear,
            };
            delete apiPayload.fromYear;
            delete apiPayload.toYear;
          }
          if (fromPrice && toPrice) {
            apiPayload.priceRange = {
              fromPrice,
              toPrice,
            };
            delete apiPayload.fromPrice;
            delete apiPayload.toPrice;
          }
          if (hasWarranty) {
            apiPayload.hasWarranty =
              hasWarranty == "All Options"
                ? ""
                : hasWarranty == "With a Warranty"
                ? "Yes"
                : hasWarranty == "No Warranty"
                ? "No"
                : null;
          }
          this.setState(
            {
              payload: apiPayload,
            },
            () => {
              this.props.handlePostProductsAPI(this.state.payload);
            }
          );
        }
      );
    }
  }

  handleTypeOnSelect = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        if (name == "hasWarranty") {
          apiPayload[`${name}`] =
            this.state.hasWarranty == "All Options"
              ? ""
              : this.state.hasWarranty == "With a Warranty"
              ? "Yes"
              : this.state.hasWarranty == "No Warranty"
              ? "No"
              : null;
          this.props.handleUrlParams(`${name}`, this.state[name]); // function to add filterdata in url params
        } else {
          apiPayload[`${name}`] = this.state[name];
          this.props.handleUrlParams(`${name}`, this.state[name]); // function to add filterdata in url params
        }
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  handleChangeCondition = (conditionName) => {
    this.setState(
      {
        condition: conditionName,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`condition`] = this.state.condition;
        this.props.handleUrlParams(`condition`, this.state.condition); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  /** function to toggle radius dropdown */
  radiusDropdownOpen = () => {
    this.props.currentLocation
      ? this.setState({
          radiusDropdown: !this.state.radiusDropdown,
        })
      : this.props.dispatch(getCurrentLocation());
  };

  /** function to set radius value on slide */
  onChangeRadius = (event, value) => {
    this.setState(
      {
        valueRadius: value,
      },
      () => {
        this.handleApiCallforRadius(this.state.valueRadius);
      }
    );
  };

  handleApiCallforRadius = debounce((value) => {
    let urlParams = {};
    urlParams[`distanceMax`] = value;
    urlParams[`latitude`] = this.props.currentLocation.latitude;
    urlParams[`longitude`] = this.props.currentLocation.longitude;
    let apiPayload = { ...this.state.payload, ...urlParams };

    this.props.handleUrlParams("distanceMax", urlParams); // function to add filterdata in url params
    this.setState(
      {
        payload: apiPayload,
      },
      () => {
        this.props.handlePostProductsAPI(this.state.payload); // API call
      }
    );
  }, 500);

  /** function to toggle price dropdown */
  priceDropdownOpen = () => {
    if (!this.state.minPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.minPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    if (!this.state.maxPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.maxPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    this.setState({
      priceDropdown: !this.state.priceDropdown,
    });
  };

  /** function to handle priceInput */
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  /** function to set min and max price */
  handlePriceRange = () => {
    let { minPrice, maxPrice } = this.state.inputpayload;
    this.setState(
      {
        minPrice,
        maxPrice,
        priceDropdown: !this.state.priceDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`priceRange`] = {};
        apiPayload[`priceRange`].fromPrice = this.state.minPrice;
        apiPayload[`priceRange`].toPrice = this.state.maxPrice;
        this.props.handleUrlParams("priceRange", apiPayload.priceRange); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API call
          }
        );
      }
    );
  };

  handleToggleFilters = () => {
    this.setState({
      filterToggle: !this.state.filterToggle,
    });
  };

  /** Function to inputpayload for selectInput */
  handleOnSelectInput = (name) => (event, { option }) => {
    console.log("dwhudh", event);
    if (
      event != null &&
      option != undefined &&
      name == "manufactor" &&
      this.state[name]?.length > 0
    ) {
      let tempPayload = this.state[name];
      let elementId = tempPayload.findIndex(
        (data) => data.label == option.label
      );
      if (elementId != -1) {
        tempPayload.splice(elementId, 1);
      } else {
        tempPayload = event;
      }
      this.setState(
        {
          [name]: tempPayload,
        },
        () => {
          this.handleAPIPayload(event, name);
        }
      );
    } else {
      this.setState(
        {
          [name]: event,
        },
        () => {
          this.handleAPIPayload(event, name);
        }
      );
    }
  };

  /** Function to inputpayload for selectInput */
  handleAPIPayload = (event, name) => {
    if (name == "manufactor") {
      let tempPayload = [...this.state.breadcrumbsData];
      if (this.state.manufactor != null) {
        let value =
          this.state.manufactor?.length > 0
            ? this.state.manufactor[this.state.manufactor.length - 1]?.label
            : null;
        tempPayload[1] = value;
      } else {
        tempPayload.pop();
      }
      this.setState({
        breadcrumbsData: [...tempPayload],
      });
    }
    if (event instanceof Array || this.state[name] == null) {
      let apiPayload = { ...this.state.payload };
      let labelArr =
        this.state[name] != null &&
        this.state[name] &&
        this.state[name].length > 0
          ? this.state[name].map((data) => data.label)
          : "";
      apiPayload[name] = labelArr.toString();
      this.props.handleUrlParams(`${name}`, labelArr.toString()); // function to add filterdata in url params
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    } else {
      let apiPayload = { ...this.state.payload };
      if (name == "Budget") {
        apiPayload[`loan`] = {};
        apiPayload[`loan`].fromLoan = 0;
        apiPayload[`loan`].toLoan = this.state[name].value;
        let urlPayload = this.state[name].label == "All" ? "" : apiPayload.loan;
        this.props.handleUrlParams("loan", urlPayload); // function to add filterdata in url params
      } else {
        let payloadName = name;
        apiPayload[payloadName] =
          name == "postedWithin"
            ? this.handlePostedWithin(this.state[name].value)
            : this.state[name].value;
        this.props.handleUrlParams(`${payloadName}`, this.state[name].value); // function to add filterdata in url params
      }
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    }
  };

  /** function to set API postedWithin payload  */
  handlePostedWithin = (value) => {
    switch (value) {
      case "24hr":
        return "1";
        break;
      case "15days":
        return "2";
        break;
      case "30days":
        return "3";
        break;
    }
  };

  /** function to toggle length dropdown */
  lengthDropdownOpen = () => {
    if (
      this.state.lengthDropdown == true &&
      (this.state.fromLength || this.state.toLength)
    ) {
      this.setState({
        minLength: this.state.fromLength,
        maxLength: this.state.toLength,
        lengthValue: this.state.LengthRange,
      });
    } else if (
      this.state.lengthDropdown == true &&
      (!this.state.fromLength || !this.state.toLength)
    ) {
      this.setState({
        minLength: 5,
        maxLength: 200,
        lengthValue: [5, 200],
      });
    }
    this.setState({
      lengthDropdown: !this.state.lengthDropdown,
    });
  };

  /** function to set min and max length */
  handleLengthRange = () => {
    this.setState(
      {
        lengthDropdown: !this.state.lengthDropdown,
        fromLength: this.state.minLength
          ? parseInt(this.state.minLength, 10)
          : 0,
        toLength: this.state.maxLength ? parseInt(this.state.maxLength, 10) : 0,
      },
      () => {
        this.setState(
          {
            LengthRange: [this.state.fromLength, this.state.toLength],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`length`] = {};
            apiPayload[`length`].fromLength = this.state.fromLength;
            apiPayload[`length`].toLength = this.state.toLength;
            this.props.handleUrlParams("length", apiPayload.length); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set length value on slide */
  handleSliderLegthRange = (event, newValue) => {
    this.setState(
      {
        lengthValue: newValue,
      },
      () => {
        this.handleMinMaxValues(this.state.lengthValue);
      }
    );
  };

  /** function to set min and max length value from slider value */
  handleMinMaxValues = (value) => {
    let minLength = value[0];
    let maxLength = value[1];
    this.setState({
      minLength,
      maxLength,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeLengthInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minLength") {
      this.setState(
        {
          maxLength: this.state.maxLength,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minLength, this.state.maxLength];
          this.setState({
            lengthValue: newValue,
          });
        }
      );
    } else if (name == "maxLength") {
      this.setState(
        {
          minLength: this.state.minLength,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minLength, this.state.maxLength];
          this.setState({
            lengthValue: newValue,
          });
        }
      );
    }
  };

  /** function to toggle Year dropdown */
  yearDropdownOpen = () => {
    if (
      this.state.yearDropdown == true &&
      (this.state.fromYear || this.state.toYear)
    ) {
      this.setState({
        minYears: this.state.fromYear,
        maxYears: this.state.toYear,
        yearsValue: this.state.yearsRange,
      });
    } else if (
      this.state.yearDropdown == true &&
      (!this.state.fromYear || !this.state.toYear)
    ) {
      this.setState({
        minYears: 1970,
        maxYears: 2017,
        yearsValue: [1970, 2017],
      });
    }
    this.setState({
      yearDropdown: !this.state.yearDropdown,
    });
  };

  /** function to set min and max Year */
  handleYearsRange = () => {
    this.setState(
      {
        yearDropdown: !this.state.yearDropdown,
        fromYear: this.state.minYears ? parseInt(this.state.minYears, 10) : 0,
        toYear: this.state.maxYears ? parseInt(this.state.maxYears, 10) : 0,
      },
      () => {
        this.setState(
          {
            yearsRange: [this.state.fromYear, this.state.toYear],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`year`] = {};
            apiPayload[`year`].fromYear = this.state.fromYear;
            apiPayload[`year`].toYear = this.state.toYear;
            this.props.handleUrlParams("year", apiPayload.year); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set Year value on slide */
  handleSliderYearsRange = (event, newValue) => {
    this.setState(
      {
        yearsValue: newValue,
      },
      () => {
        this.handleYearMinMaxValues(this.state.yearsValue);
      }
    );
  };

  /** function to set min and max Year value from slider value */
  handleYearMinMaxValues = (value) => {
    let minYears = value[0];
    let maxYears = value[1];
    this.setState({
      minYears,
      maxYears,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeYearInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minYears") {
      this.setState(
        {
          maxYears: this.state.maxYears,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minYears, this.state.maxYears];
          this.setState({
            yearsValue: newValue,
          });
        }
      );
    } else if (name == "maxYears") {
      this.setState(
        {
          minYears: this.state.minYears,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minYears, this.state.maxYears];
          this.setState({
            yearsValue: newValue,
          });
        }
      );
    }
  };

  handleManufacturerFilter = (data) => {
    if (data.value != "Trailers Parts") {
      let apiPayload = { ...this.state.payload };
      apiPayload[`manufactor`] = data.value;
      this.props.handleUrlParams("manufactor", data.value); // function to add filterdata in url params
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    }
  };

  render() {
    const { breadcrumbsData } = this.state;
    return (
      <Wrapper>
        <div className="row m-0 align-items-center ProfessionalsPageSec">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 my-3 py-3">
              <div className="row m-0">
                <CustomBreadCrumbs
                  boatsPage={true}
                  breadcrumbs={breadcrumbsData}
                  separator={
                    <img
                      src={env.Chevron_Right_Darkgrey}
                      width="4"
                      style={{ margin: "0 3px" }}
                    ></img>
                  }
                />
              </div>
              <div className="row m-0 align-items-center justify-content-between my-3">
                <div className="col p-0">
                  <h6 className="Heading">
                    Trailers <span>(54)</span>
                  </h6>
                </div>
              </div>
              <div>
                <TrailersGrids
                  handleManufacturerFilter={this.handleManufacturerFilter}
                />
              </div>
              <div className="row m-0 mt-3">
                <TrailersFilters
                  manufactor={this.state.manufactor}
                  condition={this.state.condition}
                  handleChangeCondition={this.handleChangeCondition}
                  handleTypeOnSelect={this.handleTypeOnSelect}
                  selectedYear={this.state.selectedYear}
                  radiusDropdownOpen={this.radiusDropdownOpen}
                  radiusDropdown={this.state.radiusDropdown}
                  onChangeRadius={this.onChangeRadius}
                  valueRadius={this.state.valueRadius}
                  priceDropdownOpen={this.priceDropdownOpen}
                  handleOnchangeInput={this.handleOnchangeInput}
                  handlePriceRange={this.handlePriceRange}
                  inputpayload={this.state.inputpayload}
                  priceDropdown={this.state.priceDropdown}
                  hasWarranty={this.state.hasWarranty}
                  handleToggleFilters={this.handleToggleFilters}
                  filterToggle={this.state.filterToggle}
                  engineData={this.props.engineData}
                  handleOnSelectInput={this.handleOnSelectInput}
                  trailerType={this.state.trailerType}
                  engineFuelType={this.state.engineFuelType}
                  saleBy={this.state.saleBy}
                  FinanceType={this.state.FinanceType}
                  horsePower={this.state.horsePower}
                  lengthDropdownOpen={this.lengthDropdownOpen}
                  lengthDropdown={this.state.lengthDropdown}
                  handleLengthRange={this.handleLengthRange}
                  handleSliderLegthRange={this.handleSliderLegthRange}
                  lengthValue={this.state.lengthValue}
                  handleOnchangeLengthInput={this.handleOnchangeLengthInput}
                  maxLength={this.state.maxLength}
                  minLength={this.state.minLength}
                  axles={this.state.axles}
                  TrailerData={this.props.TrailerData}
                  yearDropdown={this.state.yearDropdown}
                  yearDropdownOpen={this.yearDropdownOpen}
                  handleYearsRange={this.handleYearsRange}
                  handleSliderYearsRange={this.handleSliderYearsRange}
                  yearsValue={this.state.yearsValue}
                  handleOnchangeYearInput={this.handleOnchangeYearInput}
                  maxYears={this.state.maxYears}
                  minYears={this.state.minYears}
                  Budget={this.state.Budget}
                />
              </div>
              <div className="row m-0 BoatListSec justify-content-center position-relative">
                {/* Trailers List Module */}
                <TrailersList
                  boatsPage={true}
                  handleOnSelectInput={this.handleOnSelectInput}
                  postedWithin={this.state.postedWithin}
                  TrailersList={this.props.filterPostData}
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .ProfessionalsPageSec {
              background: ${env.BG_LightGREY_COLOR};
            }
            .Heading {
              margin: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 1.25vw;
              color: ${env.FONTGREY_COLOR};
            }
            .Heading span {
              font-size: 0.878vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${env.GREY_VARIANT_3};
            }
            .BoatListSec {
              padding: 0.732vw 0;
              background: ${env.WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    apiCallLoader: state.apiCallLoader,
  };
};

export default connect(mapStateToProps)(TrailersPage);
