import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomLink from "../../components/Link/Link";

class TrailersGrids extends Component {
  state = {
    Score: 660,
  };

  render() {
    const ManufacturersImgs = [
      { img: env.Trailers_1, value: "LOAD RITE TRAILERS" },
      { img: env.Trailers_2, value: "MAGIC TILT TRAILERS" },
      { img: env.Trailers_3, value: "E-Z LOADER TRAILERS" },
      { img: env.Trailers_5, value: "CONTINENTAL" },
      { img: env.Trailers_4, value: "Trailers Parts" },
    ];

    return (
      <Wrapper>
        <div className="col-12 p-0">
          <div className="d-flex flex-nowrap justify-content-between align-items-center">
            {ManufacturersImgs &&
              ManufacturersImgs.map((data, index) => (
                <div
                  className="manufacturerCard"
                  key={index}
                  onClick={this.props.handleManufacturerFilter.bind(this, data)}
                >
                  <img src={data.img}></img>
                </div>
              ))}
            <div className="getApproval_Sec">
              <div className="px-2">
                <h4 className="heading text-left">Get Pre-Approved Now</h4>
                <p className="getApproval_desc text-left">
                  in 2 minutes and view personalized monthly payments for our
                  inventory of new trailers
                </p>
              </div>

              <div className="d-flex align-items-center justify-content-around scoreBar">
                <div
                  className={
                    this.state.Score >= 300 && this.state.Score < 410
                      ? "firstScoreDiv currentScoreDiv"
                      : "firstScoreDiv"
                  }
                >
                  <p className="cardLabel">
                    {this.state.Score >= 300 && this.state.Score < 410
                      ? "Poor"
                      : " "}
                  </p>
                </div>
                <div
                  className={
                    this.state.Score >= 410 && this.state.Score < 520
                      ? "secondScoreDiv currentScoreDiv"
                      : "secondScoreDiv"
                  }
                >
                  <p className="cardLabel">
                    {this.state.Score >= 410 && this.state.Score < 520
                      ? "Fair"
                      : " "}
                  </p>
                </div>
                <div
                  className={
                    this.state.Score >= 520 && this.state.Score < 630
                      ? "thirdScoreDiv currentScoreDiv"
                      : "thirdScoreDiv"
                  }
                >
                  <p className="cardLabel">
                    {this.state.Score >= 520 && this.state.Score < 630
                      ? "Good"
                      : " "}
                  </p>
                </div>
                <div
                  className={
                    this.state.Score >= 630 && this.state.Score < 740
                      ? "fourthScoreDiv currentScoreDiv"
                      : "fourthScoreDiv"
                  }
                >
                  <p className="cardLabel">
                    {this.state.Score >= 630 && this.state.Score < 740
                      ? "Good"
                      : " "}
                  </p>
                </div>
                <div
                  className={
                    this.state.Score >= 740 && this.state.Score < 850
                      ? "lastScoreDiv currentScoreDiv"
                      : "lastScoreDiv"
                  }
                >
                  <p className="cardLabel">
                    {this.state.Score >= 740 && this.state.Score < 850
                      ? "Exceptional"
                      : " "}
                  </p>
                </div>
              </div>
              <CustomLink href={`/get-pre-qualified`}>
                <div className="getPreApproved_btn">
                  <ButtonComp>Get Pre-Approved</ButtonComp>
                </div>
              </CustomLink>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .manufacturerCard {
              max-width: 12.292vw;
              width: 12.292vw;
              background: ${env.WHITE_COLOR};
              height: 9.375vw;
              border-radius: 0.208vw;
            }
            .manufacturerCard img {
              width: 100%;
              height: 9.375vw;
              object-fit: contain;
              border-radius: 0.208vw;
            }
            .manufacturerCard:hover > img {
              border: 1.5px solid ${env.GREY_VARIANT_3};
            }
            .getApproval_Sec {
              background: ${env.GREEN_COLOR};
              margin: 0;
              position: relative;
              padding: 0.732vw 0;
              width: 12.692vw;
              height: 9.375vw;
              text-align: center;
              border-radius: 0.208vw;
            }
            .heading {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.938vw;
              color: ${env.WHITE_COLOR};
              margin: 0;
              font-weight: 600;
              padding: 0 0.417vw 0.417vw 0.417vw;
            }
            .getApproval_desc {
              font-family: "Open Sans" !important;
              font-size: 0.625vw;
              color: ${env.WHITE_COLOR};
              margin: 0;
              padding: 0 0.417vw 0.366vw 0.417vw;
            }
            .scoreBar {
              overflow-x: hidden;
            }
            .cardLabel {
              font-size: 0.729vw;
              color: ${env.GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0;
              line-height: 1;
              position: relative;
              z-index: 1;
              // bottom: 0.0732vw;
            }
            .firstScoreDiv {
              width: 11%;
              height: 0.417vw;
              border-radius: 0.0732vw;
              background: ${env.Red_Color_1};
              position: relative;
            }
            .secondScoreDiv {
              width: 23%;
              height: 0.417vw;
              border-radius: 0.0732vw;
              background: ${env.Orange_Color};
              position: relative;
            }
            .thirdScoreDiv {
              width: 23%;
              height: 0.417vw;
              border-radius: 0.0732vw;
              background: ${env.Yellow_Color};
              position: relative;
            }
            .fourthScoreDiv {
              width: 22%;
              height: 0.417vw;
              border-radius: 0.0732vw;
              opacity: 0.9;
              background: ${env.WHITE_COLOR};
              position: relative;
            }
            .lastScoreDiv {
              width: 15%;
              height: 0.417vw;
              border-radius: 0.0732vw;
              opacity: 0.9;
              background: ${env.Dark_Green_Color};
              position: relative;
            }
            .currentScoreDiv {
              padding: 0.219vw 0.439vw;
              height: 1.23vw;
              width: fit-content;
              text-align: center;
            }
            :global(.getApproval_Sec .getPreApproved_btn) {
              position: absolute;
              width: 100%;
              left: 50%;
              transform: translate(-50%, 0);
              bottom: 8px;
            }
            :global(.getApproval_Sec .getPreApproved_btn button) {
              width: 10.709vw;
              height: 1.927vw;
              padding: 0;
              background: transparent;
              color: ${env.WHITE_COLOR};
              margin: 0;
              border: 0.0732vw solid ${env.WHITE_COLOR};
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.getApproval_Sec .getPreApproved_btn button span) {
              font-family: "Open Sans" !important;
              line-height: 1;
              font-size: 0.833vw;
            }
            :global(.getApproval_Sec .getPreApproved_btn button:focus),
            :global(.getApproval_Sec .getPreApproved_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.getApproval_Sec .getPreApproved_btn button:hover) {
              background: transparent;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default TrailersGrids;
