import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";

// react picky input package
import { Picky } from "react-picky";


// Reactstrap Components
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import MinMaxSlider from "../../components/range-slider/min-max-slider";
import InputBox from "../../components/input-box/input-box";
import { NumberValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import CustomizedSlider from "../../components/range-slider/custom-range-slider";
import PriceDropdown from "../filter-sec/price-dropdown";
import SelectInput from "../../components/input-box/select-input";
import { getManufactures } from "../../services/manufacturer";
import { isEmpty } from "../../lib/global";

class TrailersFilter extends Component {
  state = {
    priceRange: "Price",
    radiusRange: "Radius",
    limit: 15,
    trailerManufacturerList: [],
    total_count: "",
  };

  // Async Boat Manufacturer List API call
  handleTrailerManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "3", // 0-all manufactureres,1-boats and products,2-engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.trailerManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName.toLowerCase(),
                };
              })
            : [];
        this.setState({
          trailerManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.trailerManufacturerList &&
      this.state.trailerManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.trailerManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const { condition, handleChangeCondition } = this.props;
    const { handleTrailerManufacturerList } = this;

    const warrantyConditions = [
      "All Options",
      "With a Warranty",
      "No Warranty",
    ];

    let firstTypes = [{ value: "All", label: "All" }];

    let tempTrailerType =
      this.props?.TrailerData?.length > 0
        ? this.props.TrailerData.map((data) => {
            return { value: data, label: data };
          })
        : [];

    const TrailerType = [...firstTypes, ...tempTrailerType];

    const FinanceTypeOptions = [
      { value: "0000000000", label: "All" },
      { value: 149, label: "Trailers under $149/mo" },
      { value: 299, label: "Trailers under $299/mo" },
      { value: 499, label: "Trailers under $499/mo" },
      { value: 699, label: "Trailers under $699/mo" },
      { value: 999, label: "Trailers under $999/mo" },
      { value: 1500, label: "Trailers under $1500/mo" },
      { value: 2000, label: "Trailers under $2000/mo" },
      { value: 2500, label: "Trailers under $2500/mo" },
    ];

    const lengthMarks = [
      {
        value: 0,
      },
      {
        value: 80,
      },
      {
        value: 120,
      },
      {
        value: 200,
      },
    ];

    const AxiesNum = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
    ];

    const yearsMarks = [
      {
        value: 1970,
      },
      {
        value: 1990,
      },
      {
        value: 2005,
      },
      {
        value: 2015,
      },
      {
        value: 2021,
      },
    ];

    return (
      <Wrapper>
        <div className="col-12 FilterSec">
          <div className="row m-0 flex-nowrap align-items-center justify-content-start">
            <div className="col col5Width pl-0">
              <label className="label">Condition</label>
              <div className="d-flex align-items-center justify-content-center mb-2">
                <div
                  className={
                    condition == "All"
                      ? "activeCondition leftBorderRadius norightBorder"
                      : "inactiveCondition leftBorderRadius norightBorder"
                  }
                  onClick={() => handleChangeCondition("All")}
                >
                  <p>All</p>
                </div>
                <div
                  className={
                    condition == "New"
                      ? "activeCondition norightBorder"
                      : "inactiveCondition norightBorder"
                  }
                  onClick={() => handleChangeCondition("New")}
                >
                  <p>New</p>
                </div>
                <div
                  className={
                    condition == "Used"
                      ? "activeCondition rightBorderRadius"
                      : "inactiveCondition rightBorderRadius"
                  }
                  onClick={() => handleChangeCondition("Used")}
                >
                  <p>Used</p>
                </div>
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Manufacturer</label>
              <div className="mb-2">
                <SelectInput
                  asyncSelect={true}
                  boatsPage={true}
                  placeholder="Manufacturer"
                  value={this.props.manufactor}
                  loadOptions={handleTrailerManufacturerList}
                  onChange={this.props.handleOnSelectInput(`manufactor`)}
                  additional={{
                    page: 0,
                  }}
                  hideSelectedOptions={false}
                  isMulti={true}
                />
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Trailer Type</label>
              <div className="SelectInput mb-2">
                <SelectInput
                  boatsPage={true}
                  placeholder="Any"
                  value={this.props.trailerType}
                  options={TrailerType}
                  onChange={this.props.handleOnSelectInput(`trailerType`)}
                />
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Length</label>
              <div className="LengthInput mb-2">
                <ButtonDropdown
                  isOpen={this.props.lengthDropdown}
                  toggle={this.props.lengthDropdownOpen}
                  className="dropdownBtn"
                >
                  <DropdownToggle className="dropdownInput">
                    <p className="inputValue">Length</p>
                  </DropdownToggle>
                  <DropdownMenu className="dropdownList">
                    <DropdownItem className="dropdownItem">
                      <MinMaxSlider
                        handleChange={this.props.handleSliderLegthRange}
                        value={this.props.lengthValue}
                        marks={lengthMarks}
                        max={200}
                        min={5}
                      />
                      <div className=" d-flex align-items-center justify-content-between">
                        <InputBox
                          type="text"
                          className="inputBox"
                          name="minLength"
                          value={
                            this.props.minLength || this.props.lengthValue[0]
                          }
                          max={this.props.maxLength - 1}
                          onChange={this.props.handleOnchangeLengthInput(
                            `minLength`
                          )}
                          onKeyPress={NumberValidator}
                          autoComplete="off"
                          placeholder="Min"
                        ></InputBox>
                        <p className="toSpan px-2">to</p>
                        <InputBox
                          type="text"
                          className="inputBox"
                          name="maxLength"
                          value={
                            this.props.maxLength || this.props.lengthValue[1]
                          }
                          onChange={this.props.handleOnchangeLengthInput(
                            `maxLength`
                          )}
                          onKeyPress={NumberValidator}
                          autoComplete="off"
                          placeholder="Max"
                        ></InputBox>
                        <p className="m-0 lengthUnit ml-2">ft.</p>
                      </div>
                      <div
                        className="apply_btn"
                        onClick={this.props.handleLengthRange}
                      >
                        <ButtonComp>Apply</ButtonComp>
                      </div>
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Axles</label>
              <div className="SelectInput mb-2">
                <SelectInput
                  boatsPage={true}
                  placeholder="Any"
                  value={this.props.axles}
                  options={AxiesNum}
                  onChange={this.props.handleOnSelectInput(`axles`)}
                />
              </div>
            </div>
          </div>
          <div
            className="row m-0 align-items-center justify-content-start"
            style={{
              borderBottom: this.props.filterToggle
                ? "none"
                : `1px solid ${env.Border_LightGREY_COLOR}`,
            }}
          >
            <div
              className={
                this.props.Budget ? "col col5Width pl-0" : "col col5Width pl-0"
              }
            >
              <label className="financeLabel">My Budget</label>
              <div
                className={
                  this.props.Budget
                    ? "FinanceSelectInput mb-3"
                    : "SelectInput mb-3"
                }
              >
                <SelectInput
                  financeBoat={true}
                  placeholder="My Budget"
                  value={this.props.Budget}
                  options={FinanceTypeOptions}
                  onChange={this.props.handleOnSelectInput(`Budget`)}
                />
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Price</label>
              <div className="priceInput mb-3">
                <ButtonDropdown
                  isOpen={this.props.priceDropdown}
                  toggle={this.props.priceDropdownOpen}
                  className="dropdownBtn"
                >
                  <DropdownToggle className="dropdownInput">
                    <p className="inputValue">{this.state.priceRange}</p>
                  </DropdownToggle>
                  <DropdownMenu className="dropdownList">
                    <DropdownItem>
                      <PriceDropdown
                        inputpayload={this.props.inputpayload}
                        handleOnchangeInput={this.props.handleOnchangeInput}
                        handlePriceRange={this.props.handlePriceRange}
                      />
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Year Range</label>{" "}
              <div className="LengthInput mb-3">
                <ButtonDropdown
                  isOpen={this.props.yearDropdown}
                  toggle={this.props.yearDropdownOpen}
                  className="dropdownBtn"
                >
                  <DropdownToggle className="dropdownInput">
                    <p className="inputValue">Year</p>
                  </DropdownToggle>
                  <DropdownMenu className="dropdownList">
                    <DropdownItem className="dropdownItem">
                      <MinMaxSlider
                        handleChange={this.props.handleSliderYearsRange}
                        value={this.props.yearsValue}
                        marks={yearsMarks}
                        max={2021}
                        min={1970}
                      />
                      <div className=" d-flex align-items-center justify-content-between">
                        <InputBox
                          type="text"
                          className="inputBox"
                          name="minYears"
                          value={
                            this.props.minYears || this.props.yearsValue[0]
                          }
                          max={this.props.maxYears - 1}
                          onChange={this.props.handleOnchangeYearInput(
                            `minYears`
                          )}
                          onKeyPress={NumberValidator}
                          autoComplete="off"
                          placeholder="Min"
                        ></InputBox>
                        <p className="toSpan px-2">to</p>
                        <InputBox
                          type="text"
                          className="inputBox"
                          name="maxYears"
                          value={
                            this.props.maxYears || this.props.yearsValue[1]
                          }
                          onChange={this.props.handleOnchangeYearInput(
                            `maxYears`
                          )}
                          onKeyPress={NumberValidator}
                          autoComplete="off"
                          placeholder="Max"
                        ></InputBox>
                      </div>
                      <div
                        className="apply_btn"
                        onClick={this.props.handleYearsRange}
                      >
                        <ButtonComp>Apply</ButtonComp>
                      </div>
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Has Warranty ?</label>
              <div
                className={
                  !isEmpty(this.props.hasWarranty)
                    ? "radioSelectInput selectedRadiodropddown mb-3"
                    : "radioSelectInput mb-3"
                }
              >
                <Picky
                  id="radioPicky"
                  keepOpen={false}
                  multiple={true}
                  placeholder="Has Warranty?"
                  options={warrantyConditions}
                  value={this.props.hasWarranty}
                  onChange={this.props.handleTypeOnSelect.bind(
                    this,
                    "hasWarranty"
                  )}
                />
              </div>
            </div>
            <div className="col col5Width pl-0">
              <label className="label">Search Radius</label>
              <div className="RadiusInput mb-3">
                <ButtonDropdown
                  isOpen={this.props.radiusDropdown}
                  toggle={this.props.radiusDropdownOpen}
                  className="dropdownBtn"
                >
                  <DropdownToggle className="dropdownInput">
                    <p className="inputValue">{this.state.radiusRange}</p>
                  </DropdownToggle>
                  <DropdownMenu className="dropdownList">
                    <DropdownItem className="dropdownItem">
                      <CustomizedSlider
                        onChange={this.props.onChangeRadius}
                        value={this.props.valueRadius}
                      />
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .FilterSec {
              background: ${env.WHITE_COLOR};
              padding-top: 0.417vw;
            }
            .col5Width {
              width: 15.208vw;
              max-width: 15.208vw;
            }
            .FilterSec .label {
              font-size: 0.659vw;
              color: ${env.GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.366vw;
            }
            .FilterSec .financeLabel {
              font-size: 0.659vw;
              color: ${env.GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.366vw;
            }
            .activeCondition {
              background: ${env.THEME_COLOR};
              padding: 0.219vw 0.366vw;
              // min-width: 3.281vw;
              width: 33%;
              height: 2.187vw;
              border: 1px solid ${env.THEME_COLOR};
              cursor: pointer;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .activeCondition p {
              color: ${env.WHITE_COLOR};
              font-size: 0.729166vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .leftBorderRadius {
              border-top-left-radius: 0.219vw;
              border-bottom-left-radius: 0.219vw;
            }
            .rightBorderRadius {
              border-top-right-radius: 0.219vw;
              border-bottom-right-radius: 0.219vw;
            }
            .norightBorder {
              border-right: none !important;
            }
            .inactiveCondition {
              background: ${env.WHITE_COLOR};
              padding: 0.219vw 0.366vw;
              // min-width: 3.281vw;
              width: 33%;
              height: 2.187vw;
              border: 0.0732vw solid ${env.Border_LightGREY_COLOR};
              cursor: pointer;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .inactiveCondition p {
              color: ${env.GREY_VARIANT_1};
              font-size: 0.729166vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            :global(.radioSelectInput > div),
            :global(.checkboxSelectInput > div) {
              min-width: fit-content;
              height: 2.187vw;
              position: relative;
            }
            :global(.radioSelectInput > div > button),
            :global(.checkboxSelectInput > div > button) {
              position: relative;
              height: 2.187vw;
              font-weight: 600;
              border-radius: 0.219vw;
              border: 0.0732vw solid ${env.Border_LightGREY_COLOR} !important;
              padding: 0 0.625vw !important;
              cursor: pointer;
              display: flex;
              align-items: center;
            }
            :global(.selecteddropddown > div > button) {
              position: relative;
              height: 2.187vw;
              border-radius: 0.219vw;
              border: 0.0732vw solid ${env.Border_LightGREY_COLOR} !important;
              padding: 0 0.625vw !important;
              cursor: pointer;
              display: flex;
              align-items: center;
              background: ${env.GREY_VARIANT_3};
            }
            :global(.selectedModeldropddown > div > button) {
              position: relative;
              height: 2.187vw;
              border-radius: 0.219vw;
              border: 0.0732vw solid ${env.Border_LightGREY_COLOR} !important;
              padding: 0 0.625vw !important;
              cursor: pointer;
              display: flex;
              align-items: center;
              background: ${env.Light_Yellow_Color_1};
            }

            :global(.radioSelectInput > div > button > span),
            :global(.checkboxSelectInput > div > button > span) {
              font-size: 0.729166vw;
              font-weight: 600;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_3};
              align-items: center;
              position: relative;
              padding: 0 0.732vw 0 0;
              border-radius: 0.219vw;
              width: 100%;
            }
            :global(.selectedRadiodropddown > div > button > span) {
              font-size: 0.729166vw;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_1};
              align-items: center;
              position: relative;
              padding: 0 0.732vw 0 0;
              border-radius: 0.219vw;
              width: 100%;
            }
            :global(.selectedModeldropddown > div > button > span) {
              font-size: 0.729166vw;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_10};
              align-items: center;
              position: relative;
              padding: 0 0.732vw 0 0;
              border-radius: 0.219vw;
              width: 100%;
            }
            :global(.selecteddropddown > div > button > span) {
              font-size: 0.729166vw;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_1};
              align-items: center;
              position: relative;
              padding: 0 0.732vw 0 0;
              border-radius: 0.219vw;
              width: 100%;
            }
            :global(.radioSelectInput > div > button:focus),
            :global(.radioSelectInput > div > button:active),
            :global(.checkboxSelectInput > div > button:focus),
            :global(.checkboxSelectInput > div > button:active) {
              outline: none !important;
            }
            :global(.radioSelectInput > div > button::after),
            :global(.checkboxSelectInput > div > button::after) {
              content: url(${env.Chevron_Down});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.292vw !important;
              position: relative;
              top: -50%;
              right: 0.732vw;
            }
            :global(.selecteddropddown > div > button::after) {
              content: "";
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.292vw !important;
              position: relative;
              top: -50%;
              right: 0.732vw;
            }
            :global(.radioSelectInput .picky__dropdown),
            :global(.checkboxSelectInput .picky__dropdown) {
              min-width: fit-content !important;
              width: max-content !important;
              overflow: auto !important;
              padding: 0.585vw 0.512vw !important;
              margin: 0.366vw 0 0 0;
              // box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1) !important;
              z-index: 1 !important;
              max-height: unset !important;
              transform: translate3d(0px, 46px, 0px);
              position: absolute;
              top: -1.406vw;
              left: 0;
              border: 0.0732vw solid rgba(0, 0, 0, 0.15);
              border-radius: 0.25rem;
              background-color: #fff;
              background-clip: padding-box;
            }

            :global(.checkboxSelectInput .picky__dropdown > :last-child) {
              max-height: 14.641vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            :global(.checkboxSelectInput .picky__dropdown .picky__filter) {
              border-bottom: 0.0732vw solid ${env.BG_LightGREY_COLOR} !important;
              padding: 0;
            }
            :global(.checkboxSelectInput
                .picky__dropdown
                .picky__filter
                .picky__filter__input) {
              border: none !important;
              font-size: 0.729166vw;
              height: auto;
              padding: 0.292vw 0 !important;
              line-height: 1;
              font-family: "Museo-Sans" !important;
              color: ${env.FONTGREY_COLOR};
            }

            :global(.radioSelectInput .picky__dropdown > div > div),
            :global(.checkboxSelectInput .picky__dropdown > div > div) {
              width: auto;
              font-size: 0.729166vw;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_1};
              border-bottom: none !important;
              letter-spacing: 0.0146vw !important;
              text-transform: capitalize;
              background: ${env.WHITE_COLOR} !important;
              display: flex;
              align-items: center;
            }
            :global(.radioSelectInput .picky__dropdown > div > div input) {
              -webkit-appearance: none;
              border: 0.0732vw solid ${env.GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 1.83vw;
              display: block;
              position: relative;
              margin-right: 0.366vw !important;
            }
            :global(.radioSelectInput
                .picky__dropdown
                > div
                > div
                input:checked::after) {
              content: url(${env.Radio_Darkgrey_Icon});
              position: absolute;
              top: -0.219vw !important;
              right: 0.146vw !important;
            }
            :global(.checkboxSelectInput .picky__dropdown > div > div input) {
              -webkit-appearance: none;
              border: 0.0732vw solid ${env.GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 0.219vw;
              display: block;
              margin-right: 0.366vw !important;
              position: relative;
              margin-right: 0.292vw;
            }

            :global(.picky__placeholder) {
              color: ${env.GREY_VARIANT_3};
            }

            :global(.checkboxSelectInput
                .picky__dropdown
                > div
                > div
                input:checked::after) {
              content: url(${env.Checked_Darkgrey_Icon});
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            .closeIconSmall {
              position: absolute;
              top: 65%;
              transform: translate(0, -50%);
              right: 1.796vw;
              cursor: pointer;
            }
            :global(.PriceInput .dropdownBtn),
            :global(.priceInput .dropdownBtn),
            :global(.RadiusInput .dropdownBtn),
            :global(.LengthInput .dropdownBtn),
            :global(.LocationInput .dropdownBtn) {
              border-radius: 0.219vw;
              border: 0.0732vw solid ${env.Border_LightGREY_COLOR} !important;
              margin: 0;
              min-width: fit-content;
              height: 2.187vw;
              position: relative;
              width: 100%;
            }
            :global(.PriceInput .dropdownBtn .dropdownInput),
            :global(.priceInput .dropdownBtn .dropdownInput),
            :global(.RadiusInput .dropdownBtn .dropdownInput),
            :global(.LengthInput .dropdownBtn .dropdownInput),
            :global(.LocationInput .dropdownBtn .dropdownInput) {
              padding: 0.366vw 0.732vw !important;
              width: 100%;
              display: flex;
              align-items: center;
              background: ${env.WHITE_COLOR};
              border: none !important;
              border-top-right-radius: 0.25rem !important;
              border-bottom-right-radius: 0.25rem !important;
            }
            :global(.PriceInput .dropdownBtn .dropdownInput:active),
            :global(.PriceInput .dropdownBtn .dropdownInput:focus),
            :global(.priceInput .dropdownBtn .dropdownInput:active),
            :global(.priceInput .dropdownBtn .dropdownInput:focus),
            :global(.RadiusInput .dropdownBtn .dropdownInput:active),
            :global(.RadiusInput .dropdownBtn .dropdownInput:focus),
            :global(.LengthInput .dropdownBtn .dropdownInput:active),
            :global(.LengthInput .dropdownBtn .dropdownInput:focus),
            :global(.LocationInput .dropdownBtn .dropdownInput:active),
            :global(.LocationInput .dropdownBtn .dropdownInput:focus) {
              background: ${env.WHITE_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
              border: none !important;
            }
            :global(.PriceInput .dropdownBtn .dropdownInput::after),
            :global(.priceInput .dropdownBtn .dropdownInput::after),
            :global(.RadiusInput .dropdownBtn .dropdownInput::after),
            :global(.LengthInput .dropdownBtn .dropdownInput::after),
            :global(.LocationInput .dropdownBtn .dropdownInput::after) {
              content: url(${env.Chevron_Down});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.292vw !important;
              position: relative;
              top: -0.146vw;
              left: 0.146vw;
            }
            .inputValue,
            .toSpan {
              margin: 0;
              font-weight: 600;
              font-size: 0.729166vw;
              padding: 0 0.732vw 0 0;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_3};
              width: 100%;
              text-align: left;
            }
            .locationOption {
              margin: 0;
              font-size: 0.729166vw;
              padding: 0.366vw;
              font-family: "Open Sans" !important;
              color: ${env.GREY_VARIANT_3};
              width: max-content;
              cursor: pointer;
            }
            :global(.LocationInput .dropdownBtn .dropdownList) {
              padding: 0.146vw;
              min-width: auto;
              margin: 0.366vw 0 0 0;
              width: 10.029vw;
            }
            :global(.priceInput .dropdownBtn .dropdownList) {
              padding: 0.732vw 1.098vw;
              min-width: auto;
              margin: 0.366vw 0 0 0;
              width: 25.622vw;
            }
            :global(.PriceInput .dropdownBtn .dropdownList),
            :global(.LengthInput .dropdownBtn .dropdownList) {
              padding: 0.732vw 1.098vw;
              min-width: auto;
              margin: 0.366vw 0 0 0;
              width: 14.641vw;
            }
            :global(.RadiusInput .dropdownBtn .dropdownList) {
              padding: 1.83vw 1.098vw 0.732vw 1.098vw;
              min-width: auto;
              margin: 0.366vw 0 0 0;
            }
            :global(.PriceInput .dropdownBtn .dropdownList .dropdownItem:hover),
            :global(.RadiusInput
                .dropdownBtn
                .dropdownList
                .dropdownItem:hover),
            :global(.LengthInput
                .dropdownBtn
                .dropdownList
                .dropdownItem:hover) {
              background: none !important;
            }
            :global(.LocationInput
                .dropdownBtn
                .dropdownList
                .dropdownItem:hover) {
              background: ${env.THEME_COLOR_Opacity} !important;
            }
            :global(.PriceInput .dropdownBtn .dropdownList .dropdownItem),
            :global(.RadiusInput .dropdownBtn .dropdownList .dropdownItem),
            :global(.LengthInput .dropdownBtn .dropdownList .dropdownItem),
            :global(.LocationInput .dropdownBtn .dropdownList .dropdownItem) {
              display: block !important;
            }
            .lengthUnit {
              background: ${env.THEME_COLOR};
              color: ${env.WHITE_COLOR};
              padding: 0.292vw 0.732vw;
              display: block;
              font-family: "Open Sans" !important;
              line-height: 1.5;
              font-size: 0.729166vw;
              border-radius: 0.146vw;
            }
            :global(.PriceInput
                .dropdownBtn
                .dropdownList
                .dropdownItem
                > div
                .inputBox),
            :global(.LengthInput
                .dropdownBtn
                .dropdownList
                .dropdownItem
                > div
                .inputBox) {
              display: block;
              min-width: 3.66vw !important;
              max-width: fit-content !important;
              padding: 0.292vw 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${env.GREY_VARIANT_3};
              border-radius: 0.146vw;
              box-sizing: border-box;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.732vw;
              color: ${env.GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.PriceInput
                .dropdownBtn
                .dropdownList
                .dropdownItem
                > div
                .inputBox:focus),
            :global(.LengthInput
                .dropdownBtn
                .dropdownList
                .dropdownItem
                > div
                .inputBox:focus) {
              color: ${env.GREY_VARIANT_2};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.PriceInput
                .dropdownBtn
                .dropdownList
                .dropdownItem
                > div
                .inputBox::placeholder),
            :global(.LengthInput
                .dropdownBtn
                .dropdownList
                .dropdownItem
                > div
                .inputBox::placeholder) {
              font-size: 0.732vw;
              color: ${env.GREY_VARIANT_3};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .apply_btn {
              text-align: center;
            }
            :global(.apply_btn button) {
              width: 50%;
              padding: 0.292vw 0;
              background: ${env.THEME_COLOR};
              color: ${env.WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.apply_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              font-size: 0.732vw;
            }
            :global(.apply_btn button:focus),
            :global(.apply_btn button:active) {
              background: ${env.THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.apply_btn button:hover) {
              background: ${env.THEME_COLOR};
            }
            .moreFilter_btn,
            .lessFilter_btn {
              text-align: center;
            }
            :global(.moreFilter_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              background: ${env.WHITE_COLOR};
              color: ${env.GREY_VARIANT_1};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              border-radius: 0.219vw;
              border: 0.0732vw solid ${env.Border_LightGREY_COLOR} !important;
              height: 2.187vw;
            }
            :global(.moreFilter_btn button:focus),
            :global(.moreFilter_btn button:active) {
              background: ${env.WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.moreFilter_btn button:hover) {
              background: ${env.WHITE_COLOR};
            }
            :global(.lessFilter_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              background: ${env.GREY_VARIANT_3};
              color: ${env.GREY_VARIANT_1};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              border-radius: 0.219vw;
              height: 2.187vw;
            }
            :global(.moreFilter_btn button span),
            :global(.lessFilter_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              font-size: 0.729166vw;
            }
            :global(.lessFilter_btn button:focus),
            :global(.lessFilter_btn button:active) {
              background: ${env.GREY_VARIANT_3};
              outline: none;
              box-shadow: none;
            }
            :global(.lessFilter_btn button:hover) {
              background: ${env.GREY_VARIANT_3};
            }
            :global(.FinanceSelectInput .react-select__indicators) {
              background: ${env.GREEN_COLOR};
            }
            :global(.FinanceSelectInput
                .react-select__value-container--has-value) {
              background: ${env.GREEN_COLOR};
            }
            :global(.FinanceSelectInput .react-select__control) {
              background: ${env.GREEN_COLOR};
              width: max-content !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default TrailersFilter;
