import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  Chevron_Left,
  Map_Marker,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
  GREEN_COLOR,
  BOX_SHADOW_GREY,
  Map_Marker_LightGrey_Filled,
  View_Listing_icon,
  Professional_Products_Icon,
  Email_White_Icon,
  Review_Icon,
  Orange_Color,
  PROFESSIONALS,
  BOX_SHADOW_GREY_1,
  NoBusinessFollowing_Img,
} from "../../lib/config";
import CustomBreadCrumbs from "../../components/breadcrumbs/breadcrumbs";
import { getUrlSlugs } from "../../lib/url/getSlugs";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import StarRating from "../../components/rating/star-rating";
import SelectInput from "../../components/input-box/select-input";
import Model from "../../components/model/model";
import MyLocationModel from "../filter-sec/my-location-model";
import { setCurrentLocation } from "../../redux/actions/location/location";
import { connect } from "react-redux";
import { getCountries } from "country-state-picker";
import Router from "next/router";
let proLocation;

class EngineServices extends Component {
  state = {
    slugData: [],
    inputpayload: {},
    locationModel: false,
    location: "nearby",
    professionalSearchData: [],
  };

  componentDidMount() {
    let slugData = getUrlSlugs();
    this.setState({
      slugData: slugData,
      location:
        this.props.currentLocation &&
          this.props.currentLocation.city &&
          this.props.currentLocation.city != "" &&
          this.props.currentLocation.countryShortName &&
          this.props.currentLocation.countryShortName != ""
          ? `${this.props.currentLocation.city}`
          : "nearby",
    });
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.currentLocation != this.props.currentLocation) {
      this.setState({
        location:
          this.props.currentLocation &&
            this.props.currentLocation.city &&
            this.props.currentLocation.city != "" &&
            this.props.currentLocation.countryShortName &&
            this.props.currentLocation.countryShortName != ""
            ? `${this.props.currentLocation.city}`
            : "nearby",
      });
    }
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: temppayload,
      });
    }
  };

  // ProfessionalsList={ProfessionalsList}
  // ProfessionalsData={ProfessionalsData}

  /** POST /professional API
   * filters data based on selected location
   */
  professionalSearchByInputText = (distanceRange, searchInput) => {
    if (distanceRange == "" && searchInput == "") {
      this.props.resetData();
    } else {
      this.props.professionalSearchByInputText("", "");
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  handleLocationModel = () => {
    this.setState({
      locationModel: !this.state.locationModel,
    });
  };

  handleCurrentAddAddress = (AddressData) => {
    console.log("AddressData", AddressData);
    let payload = {
      address: AddressData.address,
      latitude: AddressData.lat,
      longitude: AddressData.lng,
      city: AddressData.city,
      country: AddressData.country,
      countryShortName: AddressData.countryShortName,
      zipCode: AddressData.zipCode,
    };
    this.props.dispatch(setCurrentLocation(payload));
    this.setState({
      locationModel: false,
    });
  };

  handleCityLocation = (location) => {
    try {
      if (typeof location === "object") {
      } else {
        let locationArr = location
          ? location.split(",").map((data) => {
            return data.trim();
          })
          : "";
        let countryArr = getCountries().filter((data) => {
          return data.name == locationArr[locationArr.length - 1];
        });
        let countryShortName =
          locationArr[locationArr.length - 1] == "USA"
            ? locationArr[locationArr.length - 1]
            : countryArr
              ? countryArr
                .map((item) => {
                  return item.code;
                })
                .toString()
                .toUpperCase()
              : "";
        proLocation = locationArr[locationArr.length - 3] + ", " + countryShortName;
        return proLocation;
      }
    } catch (e) { }
  };

  onError = (e) => (e.target.src = PROFESSIONALS);

  navigateToProfessionalProfilePage = (name) => {
    Router.push(`/professional-other-profile/${name}`);
  };

  render() {
    const SortTypes = [
      { value: "Best Match", label: "Best Match" },
      { value: "Recommended", label: "Recommended" },
      { value: "Popular", label: "Popular" },
      { value: "Recent", label: "Recent" },
    ];
    const { slugData, professionalSearchData } = this.state;
    const { ProfessionalsData } = this.props;
    console.log("ProfessionalsData", ProfessionalsData);
    const lastNode = slugData ? slugData[`${slugData.length - 1}`] : "";
    const breadcrumbsData =
      slugData && slugData[`${slugData.length - 1}`] == "all-professionals"
        ? ["all professionals"]
        : ["all professionals", lastNode ? lastNode.replace("-", " ") : ""];
    return (
      <Wrapper>
        <div className="section col-12 py-3">
          <div>
            <CustomBreadCrumbs breadcrumbs={breadcrumbsData} />
            <h4 className="heading">{lastNode ? lastNode.replace("-", " ") : ""}</h4>
            <div className="row m-0 align-items-center">
              <div className="col-md-10 col-lg-10 col-xl-10 FilterSec">
                <div className="row m-0 d-flex align-items-center">
                  <div className="col-md-5 col-lg-6 p-0">
                    <div className="FilterInput">
                      <InputBox
                        type="text"
                        className="inputBox form-control"
                        name="inputText"
                        placeholder={lastNode ? lastNode.replace("-", " ") : ""}
                        value={this.props.obj && this.props.obj.inputText}
                        onChange={this.props.handler}
                        autoComplete="off"
                      />
                    </div>
                  </div>
                  <div className="col-md-4 col-lg-3">
                    <div className="borderRight d-flex align-items-center">
                      <img src={Map_Marker} className="loactionmapMarker"></img>
                      <span className="nearByLocation" onClick={this.handleLocationModel}>
                        {this.state.location}
                      </span>
                    </div>
                  </div>
                  <div className="col-3 p-0">
                    <div className="d-flex align-items-center justify-content-between cursor-pointer">
                      <div className="distanceInput">
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="distanceMax"
                          placeholder="within 20 miles"
                          value={this.props.obj && this.props.obj.distanceMax}
                          onChange={this.props.handler}
                          autoComplete="off"
                        />
                      </div>
                      <img src={Chevron_Left} className="leftChevron"></img>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-2 col-lg-2 col-xl-2 p-0 FilterSearch_btn">
                <ButtonComp
                  onClick={() =>
                    this.professionalSearchByInputText(
                      this.props.obj && this.props.obj.distanceMax,
                      this.props.obj && this.props.obj.searchInput
                    )
                  }
                >
                  Search
                </ButtonComp>
              </div>
            </div>
          </div>
        </div>

        <div className="col-12 pt-3 px-0">
          <div className="row m-0 align-items-center justify-content-between mb-2">
            <div className="d-flex align-items-center">
              <p className="pageNumLabel">View 1 -15 of 288</p>
            </div>
            <div className="d-flex align-items-center sorting_Sec">
              <p className="sortLabel">Sorting</p>
              <div className="SelectInput">
                <SelectInput
                  noboxstyle={true}
                  placeholder="Best Match"
                  options={SortTypes}
                  onChange={this.handleOnSelectInput(`sortTypes`)}
                />
              </div>
            </div>
          </div>
          {/* NoBusinessFollowing_Img */}
          {ProfessionalsData && ProfessionalsData.length > 0 ? (
            ProfessionalsData.map((data, index) => (
              <div className="professional-single-card" key={index}>
                <div className="row m-0">
                  <div style={{ width: "78%" }} className="row m-0">
                    <div className="ImageSec p-0">
                      <img
                        src={(data && data._source && data._source.businessLogo) || data.businessLogo}
                        onError={this.onError}
                        className="productImg"
                      ></img>
                    </div>
                    <div className="col professional-box">
                      <div className="row m-0 align-items-center professionalUser_Sec">
                        <div style={{ width: "10%" }}>
                          <img src={data && data._source && data._source.profilePicUrl} className="professionalUserPic"></img>
                        </div>
                        <div style={{ width: "90%" }}>
                          <div>
                            <p className="professionalUserName m-0 pt-1">
                              {(data && data._source && data._source.businessName) || data.username}
                            </p>
                          </div>
                          <div>
                            <div className="d-flex align-items-center">
                              {data.avgRating ? (
                                <div className="ratingSec" style={{ paddingTop: "5px" }}>
                                  <StarRating professionalPage={true} startFontSize={"1.09375vw"} value={data.avgRating} />
                                </div>
                              ) : data && data._source && data._source.rating ? (
                                <div className="ratingSec" style={{ paddingTop: "5px" }}>
                                  <StarRating value={data.rating} startFontSize={"1.09375vw"} />
                                </div>
                              ) : (
                                <div className="ratingSec" style={{ paddingTop: "5px" }}>
                                  <StarRating professionalPage={true} startFontSize={"1.09375vw"} value={data.avgRating || 0} />
                                </div>
                              )}
                              {data.ratedByCount ? (
                                <div className="reviewSec pl-2">
                                  <p className="reviwesNum">{data.ratedByCount} Reviwes</p>
                                </div>
                              ) : data && data._source && data._source.ratingCount > -1 ? (
                                <div className="reviewSec pl-2">
                                  <p className="reviwesNum">{data && data._source && data._source.ratingCount} Reviwes</p>
                                </div>
                              ) : (
                                ""
                              )}
                              {(data && data._source && data._source.location && data._source.location.length > 0) ||
                                (data && data.location && data.location.length > 0) ? (
                                <div className="locationSec pl-2">
                                  <p className="d-flex align-items-center m-0">
                                    <img src={Map_Marker_LightGrey_Filled} className="mapMarker"></img>
                                    <p className="professionalUserLocation">{this.handleCityLocation(data.location)}</p>
                                  </p>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <p className="professionalUser_Desc">
                        {data && data._source && data._source.about
                          ? data && data._source.about
                          : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in tortor rhoncus, tempus metus sit amet, ultricies nulla. Phasellus odio eros"}
                      </p>
                      <div className="py-2 align-items-center professionalUser_Sec">
                        <p className="label">Specialization:</p>

                        <ul className="list-unstyled d-flex align-items-center flex-wrap m-0 specializationList">
                          {data && data.specialityData && data.specialityData.length > 0
                            ? data.specialityData.map((speciality) => <li>{speciality.name}</li>)
                            : data &&
                            data._source &&
                            data._source.subSpeciality &&
                            data._source.subSpeciality.map((speciality) => <li>{speciality}</li>)}
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div style={{ width: "22%" }}>
                    <div className="col-12 p-0">
                      <div className="viewListing_btn">
                        <ButtonComp onClick={() => this.navigateToProfessionalProfilePage(data._source.businessName)}>
                          <img src={View_Listing_icon} className="viewListingIcon"></img>
                          View Listing
                        </ButtonComp>
                      </div>
                      <div className="professionalProduct_btn">
                        <ButtonComp>
                          <img src={Professional_Products_Icon} className="professionalProductIcon"></img>
                          Products
                        </ButtonComp>
                      </div>
                      <div className="review_btn">
                        <ButtonComp>
                          <img src={Review_Icon} className="reviewIcon"></img>
                          Write Review
                        </ButtonComp>
                      </div>
                      <div className="contactMe_btn">
                        <ButtonComp onClick={() => this.navigateToProfessionalProfilePage(data._source.businessName)}>
                          <img src={Email_White_Icon} className="contactMeIcon"></img>
                          Contact Me
                        </ButtonComp>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="col-12 flex-column d-flex align-items-center justify-content-center" style={{ height: "30vw" }}>
              <div>
                <img src={NoBusinessFollowing_Img} alt="NoBusinessFollowing_Img" />
              </div>
              <div className="pt-3">
                <h4 className="heading">No Business Found.</h4>
              </div>
            </div>
          )}
        </div>

        {/* Location Model */}
        <Model open={this.state.locationModel} onClose={this.handleLocationModel}>
          <MyLocationModel
            setCurrentLocation={true}
            onClose={this.handleLocationModel}
            handleCurrentAddAddress={this.handleCurrentAddAddress}
          />
        </Model>
        <style jsx>
          {`
            .professional-single-card {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px 0px ${BOX_SHADOW_GREY_1} !important;
              margin-bottom: 1.098vw;
              border-radius: 3px;
            }
            .section {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px 0px ${BOX_SHADOW_GREY} !important;
              margin-bottom: 1.098vw;
            }
            .professional-box {
              padding: 0.9375vw;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
            .FilterSec {
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              padding: 0.219vw 0.732vw;
            }
            :global(.FilterInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.049vw;
              padding: 0;
              line-height: 1.5;
              background: ${WHITE_COLOR};
              font-size: 0.805vw;
              border: none;
              text-transform: capitalize;
              border-right: 0.0732vw solid ${GREY_VARIANT_2};
              border-radius: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }
            :global(.FilterInput .inputBox:focus),
            :global(.FilterInput .inputBox:active) {
              outline: none;
              box-shadow: none;
              color: ${GREY_VARIANT_1};
            }
            :global(.FilterInput .inputBox::placeholder) {
              color: ${GREY_VARIANT_1};
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }
            :global(.distanceInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.049vw;
              padding: 0;
              line-height: 1.5;
              background: ${WHITE_COLOR};
              font-size: 0.805vw;
              border: none;
              text-transform: capitalize;
              border-radius: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }
            :global(.distanceInput .inputBox:focus),
            :global(.distanceInput .inputBox:active) {
              outline: none;
              box-shadow: none;
              color: ${GREY_VARIANT_1};
            }
            :global(.distanceInput .inputBox::placeholder) {
              color: ${GREY_VARIANT_1};
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }

            .borderRight {
              height: 2.049vw;
              border-right: 0.0732vw solid ${GREY_VARIANT_2};
            }
            .nearByLocation {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              cursor: pointer;
            }
            .FilterSec .loactionmapMarker {
              width: 0.585vw;
              height: 0.732vw;
              margin-right: 0.219vw !important;
            }
            :global(.FilterSearch_btn button) {
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              text-transform: capitalize;
              border: none;
              width: 90%;
              height: 2.635vw;
              padding: 0 0.732vw;
              margin-left: 0.732vw;
            }
            :global(.FilterSearch_btn button span) {
              font-size: 0.951vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }
            :global(.FilterSearch_btn button:focus),
            :global(.FilterSearch_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.FilterSearch_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .leftChevron {
              width: 0.366vw;
              margin-right: 0.366vw;
              cursor: pointer;
            }
            .ImageSec {
              max-width: 14.641vw;
            }
            .productImg {
              width: 100%;
              height: 100%;
              object-fit: cover;
              border-top-left-radius: 3px;
              border-bottom-left-radius: 3px;
            }
            .professionalUserPic {
              // width: 80%;
              width: 84%;
              object-fit: cover;
            }
            .professionalUserName {
              font-size: 0.805vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0.366vw;
              margin-left: 0.146vw;
              cursor: pointer;
              text-transform: capitalize;
              line-height: 1;
            }
            .ratingSec,
            .reviewSec {
              width: max-content;
              margin-right: 0.512vw;
            }
            .locationSec {
              padding-left: 1.25vw !important;
              width: max-content;
              line-height: 1;
            }
            .reviwesNum {
              // font-size: 0.732vw;
              font-size: 0.625vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0;
              text-transform: capitalize;
            }
            .professionalUser_Sec .mapMarker {
              width: 0.512vw;
              margin-right: 0.292vw;
            }
            .professionalUserLocation {
              // font-size: 0.732vw;
              font-size: 0.625vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0;
              text-transform: capitalize;
            }
            .professionalUser_Sec .label {
              font-size: 0.625vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0 !important;
            }
            .specializationList li {
              background: ${BG_LightGREY_COLOR};
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.146vw 0.366vw;
              // font-size: 0.805vw;
              font-size: 0.625vw;
              margin-right: 0.366vw;
              margin-top: 0.366vw;
              cursor: pointer;
            }
            .professionalUser_Desc {
              font-size: 0.805vw;
              padding-top: 0.9375vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0.585vw;
              line-height: 1.3;
            }
            :global(.viewListing_btn button),
            :global(.professionalProduct_btn button) {
              width: 95%;
              border-radius: 2px;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              border: 0.0732vw solid ${GREY_VARIANT_2};
              color: ${GREY_VARIANT_1};
              margin: 0.585vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.viewListing_btn button span),
            :global(.professionalProduct_btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
              margin-left: 0.732vw;
            }
            :global(.viewListing_btn button:focus),
            :global(.viewListing_btn button:active),
            :global(.professionalProduct_btn button:focus),
            :global(.professionalProduct_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.viewListing_btn button:hover),
            :global(.professionalProduct_btn button:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.review_btn button) {
              width: 95%;
              border-radius: 2px;
              padding: 0.366vw 0;
              background: ${Orange_Color};
              color: ${WHITE_COLOR};
              margin: 0.585vw 0 0 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.review_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
              margin-left: 0.732vw;
            }
            :global(.review_btn button:focus),
            :global(.review_btn button:active) {
              background: ${Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.review_btn button:hover) {
              background: ${Orange_Color};
            }

            :global(.contactMe_btn button) {
              border-radius: 2px;
              width: 95%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              // margin: 0.585vw 0 0 0;
              margin: 0.585vw 0 0.585vw 0 !important;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.contactMe_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
              margin-left: 0.732vw;
            }
            :global(.contactMe_btn button:focus),
            :global(.contactMe_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.contactMe_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .viewListingIcon,
            .contactMeIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 0.878vw;
            }
            .professionalProductIcon,
            .reviewIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.585vw;
              width: 0.585;
            }
            .SelectInput {
              margin: 0 0.732vw 0 0.366vw;
              height: auto;
            }
            .sortLabel {
              font-size: 0.768vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0;
              letter-spacing: 0.0219vw !important;
            }
            .pageNumLabel {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin: 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(EngineServices);
