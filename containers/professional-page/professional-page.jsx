import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { BG_LightGREY_COLOR, GREY_VARIANT_2, WHITE_COLOR, THEME_COLOR, THEME_COLOR_Opacity } from "../../lib/config";
import { getUrlSlugs } from "../../lib/url/getSlugs";
import NestedList from "../../components/custom-list/nested-list";
import EngineServices from "./engine-services";

let activeLinkList = [];

class ProfessionalPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeLink: "",
      openPage: false,
      activeLinkList: [],
    };
  }

  // On Component load this called
  componentDidMount = () => {
    this.props.ProfessionalsList &&
      this.props.ProfessionalsList.map((data) => {
        let listNode = data.name.toLowerCase().replace(/ /g, "_");
        this.setState(
          {
            [listNode]: false,
          },
          () => {
            data.subSpeciality &&
              data.subSpeciality.map((item) => {
                let activeLinkNode = item.name.toLowerCase().replace(/ /g, "-");
                let activeLink = data.name.toLowerCase().replace(/ /g, "-").replace("&", "and") + "/" + activeLinkNode;
                let activeLinkList = this.state.activeLinkList;
                activeLinkList.push(activeLink);
                this.setState({
                  activeLinkList,
                  activeLink: activeLinkList[0],
                });
              });
          }
        );
      });

    let slugData = getUrlSlugs();
    let activeLink = slugData.toString().replace(/,/g, "/");
    this.setState({
      activeLink,
      openPage: true,
      slugData: slugData,
    });
  };

  // Used to get Current Slug
  handelTabClick = (mainNode, endNode, nodeId, type) => {
    // type 1 -> specialityId || type 2 -> subSpecialityId
    // let MainNode = mainNode
    //   .toLowerCase()
    //   .replace(/ /g, "-")
    //   .replace("&", "and");
    // let EndNode = endNode.toLowerCase().replace(/ /g, "-");
    // console.log("asasdasdasd--->", MainNode, EndNode);
    // let slug = MainNode + "/" + EndNode;
    // Router.push(`/professionals/${slug}`);
    let payload = {};
    if (type === 1) {
      // payload["specialityId"] = nodeId;
    } else {
      payload["subSpecialityId"] = nodeId;
      this.props.ProfessionalsDataAPI(payload);
    }
  };

  handleListToggle = (name) => {
    this.setState({
      [name]: !this.state[name],
    });
  };

  render() {
    let pageContent = (
      <EngineServices
        resetData={this.props.resetData}
        setUpdatedProfessionalData={this.props.setUpdatedProfessionalData}
        ProfessionalsData={this.props.ProfessionalsData}
        professionalSearchByInputText={this.props.professionalSearchByInputText}
        handler={this.props.handler}
        obj={this.props.obj}
      />
    );

    const { ProfessionalsList, professionalSearchByInputText } = this.props;
    const { activeLinkList } = this.state;
    console.log("ProfessionalsList", ProfessionalsList);
    return (
      <Wrapper>
        {this.state.openPage ? (
          <div className="row m-0 align-items-center ProfessionalsPageSec">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0 my-3 py-3">
                <div className="row m-0">
                  <div className="col-md-3 col-lg-3 Tabs pl-0">
                    <ul className="list-unstyled AccountTabs">
                      {ProfessionalsList &&
                        ProfessionalsList.map((data, index) => (
                          <li key={index}>
                            <NestedList
                              professionals={true}
                              name={data.name}
                              open={this.state[data.name.toLowerCase().replace(/ /g, "_")]}
                              onClick={() => {
                                // this.handelTabClick("", "", data.nodeId, 1);
                                this.handleListToggle(data.name.toLowerCase().replace(/ /g, "_"));
                              }}
                            >
                              {data.subSpeciality.length ? (
                                <ul className="list-unstyled engineService_List">
                                  {data.subSpeciality &&
                                    data.subSpeciality.map((items, key) => (
                                      <li
                                        key={key}
                                        className={activeLinkList[key] === this.state.activeLink ? "activeLink" : " "}
                                        onClick={() => {
                                          this.handelTabClick.bind(this, data.name, items.name, items.nodeId, 2);
                                          professionalSearchByInputText(data.name, items.name);
                                        }}
                                      >
                                        {items.name}
                                      </li>
                                    ))}
                                </ul>
                              ) : (
                                ""
                              )}
                            </NestedList>
                          </li>
                        ))}
                    </ul>
                  </div>

                  <div className="col TabPanel p-0">{pageContent}</div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .ProfessionalsPageSec {
              background: ${BG_LightGREY_COLOR};
            }
            .AccountTabs {
              background: ${WHITE_COLOR};
              padding: 0 0.585vw;
            }
            .TabPanel {
              margin-left: 0.585vw;
            }
            .activeLink {
              color: ${THEME_COLOR} !important;
            }
            .engineService_List li {
              cursor: pointer;
              font-size: 0.805vw;
              padding: 0.219vw 0;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            .engineService_List li:hover {
              color: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProfessionalPage;
