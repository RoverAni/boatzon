import React, { Component } from "react";
import TextAreaBox from "../../components/input-box/text-area-input";
import StarRating from "../../components/rating/star-rating";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";

class ReviewModel extends Component {
  state = {
    ratingValue: 0,
  };

  // Function for review rating value
  handleRating = (event, value) => {
    this.setState({
      ratingValue: value,
    });
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    this.setState({
      [inputControl.name]:
        inputControl.value && inputControl.value.length
          ? inputControl.value
          : "",
    });
  };

  render() {
    return (
      <Wrapper>
        <div className="col-12 reviewModel py-3 px-4">
          <h6 className="heading">Add Review</h6>
          <div className="row m-0 my-3 align-items-center">
            <div className="col-1 pl-0">
              <img src={env.Profile_Logo} className="profilePic"></img>
            </div>
            <div className="col-11 p-0">
              <h6 className="proName">Lion Barcel</h6>
              <p className="m-0 caption">
                Your review will be posted publicly on the Boatzon.
              </p>
            </div>
          </div>
          <div className="row m-0">
            <StarRating
              startFontSize={18}
              value={this.state.ratingValue}
              onChange={this.handleRating}
            />
          </div>
          <div className="row m-0 my-3">
            <TextAreaBox
              type="textarea"
              className="textareaBox form-control"
              placeholder="Share details of your own experience about this professional. Be specific and concise."
              name="reviewComments"
              value={this.state.reviewComments}
              onChange={this.handleOnchangeInput}
              autoComplete="off"
            ></TextAreaBox>
          </div>
          <div className="row m-0 align-items-center justify-content-end">
            <div className="col-5 p-0">
              <div className="row m-0 align-items-center justify-content-center">
                <div className="col-6 p-0">
                  <div className="cancel_btn">
                    <ButtonComp>Cancel</ButtonComp>
                  </div>
                </div>
                <div className="col-6 p-0">
                  <div className="post_btn">
                    <ButtonComp>Post</ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.map_btn_placements) {
              margin-top: 22vw;
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: inherit !important;
            }
            .reviewModel {
              width: 40vw;
              position: relative;
              background: ${env.BG_LightGREY_COLOR} !important;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${env.GREY_VARIANT_8};
              margin: 0;
            }
            .proName {
              font-size: 0.951vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${env.FONTGREY_COLOR_Dark};
              margin: 0;
            }
            .caption {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${env.GREY_VARIANT_2};
              margin: 0;
            }
            :global(.reviewModel .textareaBox) {
              display: block;
              width: 100%;
              height: 7.32vw;
              padding: 13px;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${env.FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.reviewModel .textareaBox:focus) {
              color: ${env.FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${env.THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.textareaBox::placeholder) {
              font-size: 0.878vw;
              color: ${env.GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.cancel_btn button) {
              width: 95%;
              padding: 0.366vw 0;
              background: ${env.GREY_VARIANT_10};
              color: ${env.WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.cancel_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${env.GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${env.GREY_VARIANT_10};
            }

            :global(.post_btn button) {
              width: 95%;
              padding: 0.366vw 0;
              background: ${env.GREEN_COLOR};
              color: ${env.WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
              float: right;
            }
            :global(.post_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.post_btn button:focus),
            :global(.post_btn button:active) {
              background: ${env.GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.post_btn button:hover) {
              background: ${env.GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default ReviewModel;
