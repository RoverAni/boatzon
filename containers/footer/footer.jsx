// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// asstes and colors
import {
  WHITE_COLOR,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  Border_LightGREY_COLOR,
  Footer_BG,
  APP_Footer_LOGO_WHITE,
  Play_Store_White_Icon,
  Apple_White_Icon,
  GREY_VARIANT_14,
  Twitter_White_Icon,
  Facebook_White_Icon,
  Instagram_White_Icon,
  LinkedIn_White_Icon,
  Pinterest_White_Icon,
  BZ_TWITTER_LINK,
  BZ_FB_LINK,
  BZ_INSTAGRAM_LINK,
  BZ_PINTEREST_LINK,
} from "../../lib/config";

// reusable component
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";

// Validation functions
import { requiredValidator } from "../../lib/validation/validation";

// redux context component
import Context from "../../context/context";

import CustomLink from "../../components/Link/Link";

class Footer extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
  };

  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
      let tempsignUpPayload = this.state.inputpayload;
      tempsignUpPayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: tempsignUpPayload,
      });
    });
  };

  // Funtion for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state.email == 1 ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  render() {
    return (
      <Context.Consumer>
        {(Context) => {
          return (
            <Wrapper>
              <div className="FooterPageBG">
                <div className="container p-md-0">
                  <div className="screenWidth mx-auto pt-4 pb-3">
                    <div className="row m-0 header align-items-end">
                      <div className="col-2 mb-3 p-0">
                        <img src={APP_Footer_LOGO_WHITE} className="appLogo"></img>
                      </div>
                      <div className="col-10 p-0 mb-2">
                        <div className="row m-0 align-items-end">
                          <div className="col-8 p-0">
                            <div className="d-flex align-items-end px-md-2 py-2">
                              <img src={Play_Store_White_Icon} className="playStoreIcon"></img>
                              <img src={Apple_White_Icon} className="appleIcon"></img>
                            </div>
                          </div>
                          <div className="col-4 p-0 ml-auto">
                            <div className="row m-0 align-items-end">
                              <div className="col-7 pl-0 pr-2 py-2">
                                <div className="d-flex align-items-center justify-content-between">
                                  <CustomLink href={BZ_TWITTER_LINK}>
                                    <a target="_blank">
                                      <img src={Twitter_White_Icon} className="shareIcon"></img>
                                    </a>
                                  </CustomLink>
                                  {/* <CustomLink> */}
                                  <a target="_blank" onClick={() => window.open(BZ_FB_LINK)}>
                                    <img src={Facebook_White_Icon} className="shareIcon"></img>
                                  </a>
                                  {/* </CustomLink> */}
                                  {/* <CustomLink> */}
                                  <a target="_blank" onClick={() => window.open(BZ_INSTAGRAM_LINK)}>
                                    <img src={Instagram_White_Icon} className="shareIcon"></img>
                                  </a>
                                  {/* </CustomLink> */}
                                  <img src={LinkedIn_White_Icon} className="shareIcon"></img>
                                  <CustomLink href={BZ_PINTEREST_LINK}>
                                    <a target="_blank">
                                      <img src={Pinterest_White_Icon} className="shareIcon"></img>
                                    </a>
                                  </CustomLink>
                                </div>
                              </div>
                              <div className="col-5 p-0">
                                <div className="contactUs_btn">
                                  <ButtonComp>Contact Us</ButtonComp>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row m-0 content">
                      <div className="col-5 p-0">
                        <div className="row m-0">
                          <div className="col-6 p-0">
                            <div>
                              <h6 className="listHeading">{Context.locale.company}</h6>
                              <ul className="list-unstyled FooterLinks">
                                <li>
                                  <a href="/about-us">
                                    {Context.locale.about} {Context.locale.appName}
                                  </a>
                                </li>
                                <li>
                                  <a href="/terms-of-service">Terms of Use</a>
                                </li>
                                <li>
                                  <a href="/privacy-policy">General Privacy Policy</a>
                                </li>
                                <li>
                                  <a href="/financial-privacy-policy">Financial Privacy Policy</a>
                                </li>
                                <li>
                                  <a href="/community-guidelines">Community Guidelines</a>
                                </li>
                                <li>
                                  <a href="/prohibited-items-guidelines">Prohibited Items Guidelines</a>
                                </li>
                                <li>
                                  <a href="/posting-rules">Posting Rules</a>
                                </li>
                                <li>
                                  <a href="/shipping-policies">Shipping Policies</a>
                                </li>
                                <li>
                                  <a href="/responsible-disclosure-policy">Responsible Disclosure Policy</a>
                                </li>
                                <li>
                                  <a href="/ca-supply-chain-disclosure">CA Supply Chain Disclosure</a>
                                </li>
                                <li>
                                  <a href="/accessibility-commitment">Accessibility Commitment</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div className="col-6 p-0">
                            <div>
                              <h6 className="listHeading">{Context.locale.appName}</h6>
                              <ul className="list-unstyled FooterLinks">
                                <li>
                                  <a href="/boats">Boats for Sale Near Me</a>
                                </li>
                                <li>
                                  <a href="/products">Products for Sale Near Me</a>
                                </li>
                                <li>
                                  <a href="/professionals">Talk to Professional</a>
                                </li>
                                <li>
                                  <a href="/discussions">Join a Discussion</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-7 p-0">
                        <div className="row m-0">
                          <div className="col-6">
                            <div>
                              <h6 className="listHeading">Business Services</h6>
                              <ul className="list-unstyled FooterLinks">
                                <li>
                                  <a target="_blank">For Professionals</a>
                                </li>
                                <li>
                                  <a target="_blank">For Boat Dealers</a>
                                </li>
                                <li>
                                  <a target="_blank">For Retailers</a>
                                </li>
                                <li>
                                  <a target="_blank">Boat Insurance Quote</a>
                                </li>
                                <li>
                                  <a target="_blank">Boat Loans Quote</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div className="col-6 pr-0">
                            <div className="FooterSubscription">
                              <h6 className="listHeading">{Context.locale.stayInTouch}</h6>
                              <div className="FormInput">
                                <label>{Context.locale.subscribeHeading}!</label>
                                <InputBox
                                  type="email"
                                  className="inputBox form-control"
                                  placeholder="Your Email Address"
                                  name="email"
                                  onChange={this.handleOnchangeInput("email")}
                                  autoComplete="off"
                                ></InputBox>

                                {this.state.email == 0 || this.state.email == 2 ? (
                                  <p className="errMessage">
                                    {Context.locale.enter} {Context.locale.valid} {Context.locale.email}
                                  </p>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="Subscribe_btn">
                                <ButtonComp>{Context.locale.subscribe}</ButtonComp>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row m-0 FooterCopyright">
                      <div className="col-6 p-0">
                        <p>{Context.locale.copyRight}</p>
                      </div>
                      <div className="col-6 p-0">
                        <p className="float-right">
                          {Context.locale.needHelp}? {Context.locale.call} <a target="_blank">1-800-368-4268</a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <style jsx>
                {`
                  .FooterPageBG {
                    min-height: auto;
                    background-image: url(${Footer_BG});
                    background-repeat: no-repeat;
                    background-size: cover;
                    background-position: center;
                    position: relative;
                  }
                  .header {
                    border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
                  }
                  .playStoreIcon {
                    width: 2.562vw;
                    padding-right: 1.098vw;
                    border-right: 0.0732vw solid ${Border_LightGREY_COLOR};
                  }
                  .appleIcon {
                    width: 2.562vw;
                    padding-left: 1.098vw;
                  }
                  .appLogo {
                    width: 9.516vw;
                    cursor: pointer;
                  }
                  .shareIcon {
                    width: 1.317vw;
                  }
                  .websiteDesc {
                    max-width: 27.086vw;
                  }
                  .websiteDesc p {
                    font-size: 0.805vw;
                    padding: 1.098vw 0;
                    margin: 0;
                    color: ${GREY_VARIANT_2};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    max-width: 90%;
                  }
                  .content {
                    padding: 2.049vw 0;
                  }
                  .listHeading {
                    font-size: 1.098vw;
                    text-transform: capitalize;
                    color: ${WHITE_COLOR};
                    font-family: "Museo-Sans" !important;
                    font-weight: 600;
                    margin-bottom: 0.732vw;
                  }
                  .FooterLinks li {
                    line-height: 1.25;
                  }
                  .FooterLinks li a {
                    font-size: 0.805vw;
                    color: ${WHITE_COLOR};
                    text-transform: capitalize;
                    font-family: "Open Sans" !important;
                    line-height: 1;
                    cursor: pointer;
                    text-decoration: none;
                  }
                  .FooterLinks li a:hover {
                    color: ${FONTGREY_COLOR};
                  }
                  .FormInput label {
                    font-size: 0.732vw;
                    margin-bottom: 0.732vw;
                    color: ${WHITE_COLOR};
                    font-family: "Open Sans" !important;
                    max-width: 80%;
                  }
                  .FormInput label:first-letter {
                    text-transform: capitalize;
                  }
                  :global(.FooterSubscription .inputBox) {
                    display: block;
                    width: 100%;
                    height: 2.489vw;
                    padding: 0.375rem 0.75rem;
                    line-height: 1.5;
                    background-color: #fff;
                    background-clip: padding-box;
                    border: 0.0732vw solid ${Border_LightGREY_COLOR};
                    border-radius: 0.146vw;
                    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                    font-size: 0.805vw;
                    color: ${GREY_VARIANT_14};
                    font-family: "Open Sans" !important;
                  }
                  :global(.FooterSubscription .inputBox:focus) {
                    color: ${GREY_VARIANT_14};
                    background-color: #fff;
                    border-color: ${THEME_COLOR};
                    outline: 0;
                    box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
                  }
                  :global(.inputBox::placeholder) {
                    font-size: 0.805vw;
                    color: ${GREY_VARIANT_14};
                    font-family: "Open Sans" !important;
                  }
                  .errMessage {
                    font-size: 0.732vw;
                    text-align: left;
                    color: red;
                    padding-top: 0.732vw;
                    margin-bottom: 0;
                    text-transform: capitalize;
                  }
                  .contactUs_btn {
                    text-align: right;
                    padding: 0.585vw 0 0 0;
                  }
                  :global(.contactUs_btn button) {
                    width: 90%;
                    padding: 0.585vw 0;
                    background: transparent;
                    color: ${WHITE_COLOR};
                    border: 0.0732vw solid ${WHITE_COLOR};
                    margin: 0.146vw 0;
                    border-radius: 0.146vw;
                    text-transform: capitalize;
                  }
                  :global(.contactUs_btn button span) {
                    font-family: "Museo-Sans" !important;
                    font-weight: 600;
                    font-size: 0.878vw;
                    line-height: 1;
                  }
                  :global(.contactUs_btn button:focus),
                  :global(.contactUs_btn button:active) {
                    background: transparent;
                    outline: none;
                    box-shadow: none;
                  }
                  :global(.contactUs_btn button:hover) {
                    background: transparent;
                  }

                  :global(.Subscribe_btn button) {
                    width: 100%;
                    padding: 0.366vw 0;
                    background: ${THEME_COLOR};
                    color: ${WHITE_COLOR};
                    margin: 0.732vw 0;
                    border-radius: 0.146vw;
                    text-transform: capitalize;
                  }
                  :global(.Subscribe_btn button span) {
                    font-family: "Museo-Sans" !important;
                    font-weight: 600;
                    font-size: 0.878vw;
                  }
                  :global(.Subscribe_btn button:focus),
                  :global(.Subscribe_btn button:active) {
                    background: ${THEME_COLOR};
                    outline: none;
                    box-shadow: none;
                  }
                  :global(.Subscribe_btn button:hover) {
                    background: ${THEME_COLOR};
                  }
                  .FooterCopyright {
                    padding-top: 0.732vw;
                  }
                  .FooterCopyright p {
                    margin: 0;
                    font-size: 0.732vw;
                    color: ${WHITE_COLOR};
                    text-transform: capitalize;
                    font-family: "Open Sans" !important;
                  }
                  .FooterCopyright p a {
                    font-size: 0.732vw;
                    font-family: "Open Sans" !important;
                    cursor: pointer;
                    text-decoration: none;
                  }
                `}
              </style>
            </Wrapper>
          );
        }}
      </Context.Consumer>
    );
  }
}

export default Footer;
