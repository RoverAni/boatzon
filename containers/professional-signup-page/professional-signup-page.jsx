import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  ProfessionalSignup_Banner_BG,
  THEME_COLOR,
  FONTGREY_COLOR_Dark,
  FONTGREY_COLOR,
  WHITE_COLOR,
  Email_Icon,
  Password_Icon,
  BLACK_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  Border_LightGREY_COLOR_1,
  GREEN_COLOR,
  GREY_VARIANT_3,
  GREY_VARIANT_5,
  Google_Icon,
  Border_LightGREY_COLOR,
  Light_Blue_Color,
  BG_LightGREY_COLOR,
  Vector_Small,
  Customer_Icon,
  Leads_To_Customers_Icon,
  Boat_Dealers_Icon,
  Boats_Products_Everywhere,
  Credit_Card_Processing_Icon,
  Professionals_Feature_Icon,
  ProfessionalSignup_FooterBanner_BG,
  NotVerified_Icon,
  Verified_Icon,
  Red_Color,
  Professional_Type_Icon,
  FB_APP_ID,
  Facebook_Icon,
  Facebook_Blue,
  Facebook_Signup_Type,
  Web_deviceType,
  Google_Signup_Type,
  Info_Icon_Red,
  ProfileName_Icon,
  View_Grey_Filled_Icon,
  View_Grey_UnFilled_Icon,
} from "../../lib/config";
import InputWithIcon from "../../components/input-box/input-with-icon";
import {
  EMAIL,
  PASSWORD,
  FULL_NAME,
} from "../../lib/input-control-data/input-definitions";
import ButtonComp from "../../components/button/button";
import {
  requiredValidator,
  PureTextValidator,
} from "../../lib/validation/validation";
import ProfessionalProfilePage from "./professional-profile-page";
import { proEmailVerfication } from "../../services/pro-auth";
import { UserNameVerfication } from "../../services/auth";
import { getCookie } from "../../lib/session";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login";
import Snackbar from "../../components/snackbar";
import Model from "../../components/model/model";
import LoginPage from "../auth-modals/login/login-page";
import InfoTooltip from "../../components/tooltip/info-tooltip";

class ProfessionalSignupPage extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
    isRemembered: false,
    loginPage: false,
    passView: false,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      AuthPass,
    });
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.userProfileData != this.props.userProfileData) {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload.username = this.state.AuthPass
        ? this.props.userProfileData && this.props.userProfileData.username
        : "";
      tempPayload.Email = this.state.AuthPass
        ? this.props.userProfileData && this.props.userProfileData.email
        : "";
      tempPayload.FullName = this.state.AuthPass
        ? this.props.userProfileData && this.props.userProfileData.fullName
        : "";
      this.setState({
        inputpayload: { ...tempPayload },
      });
    }
  };
  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    if (event.target.name == "username") {
      this.setState({
        usernameChecked: false,
        usernameErrMsg: "",
        usernameErrMsgClr: "",
        usernameValid: false,
      });
    }
    if (event.target.name == "Email") {
      this.setState({
        emailChecked: false,
        emailErrMsg: "",
        emailErrMsgClr: "",
        emailValid: false,
      });
    }

    console.log("fejf-->", event.target.value, event.target.name);
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    console.log("fejf", inputControl);
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    console.log("fejf", tempsignUpPayload);
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
      },
      () => {
        // this.props.dispatch(setSignupData(this.state.inputpayload));
        console.log("inputpayload", this.state.inputpayload);
      }
    );
  };

  handleErrMsgColor = (response) => {
    switch (response.code) {
      case 200:
        return `${GREEN_COLOR}`;
        break;
      case 409:
        return `${Red_Color}`;
        break;
    }
  };

  handleProEmailCheck = () => {
    if (this.state[EMAIL] == 1) {
      let payload = {
        email: this.state.inputpayload.Email || this.state.inputpayload.email,
      };
      console.log("PAYNFHFUIE", payload);
      proEmailVerfication(payload)
        .then((res) => {
          let response = res.data;
          console.log("PAYNFHFUIE", response);

          if (response) {
            this.setState(
              {
                emailChecked: true,
                emailErrMsg: response.message,
                emailErrMsgClr: this.handleErrMsgColor(response),
                emailValid: response.code == 200 ? true : false,
              },
              () => {
                this.checkIfFormValid();
              }
            );
          }
        })
        .catch((err) => {
          console.log("err", err, err.message);
          this.setState(
            {
              emailErrMsg: err.message,
              emailErrMsgClr: `${Red_Color}`,
              emailValid: false,
            },
            () => {
              this.checkIfFormValid();
            }
          );
        });
    } else {
      this.setState(
        {
          emailChecked: false,
          emailValid: false,
          emailErrMsg: "",
          emailErrMsgClr: "",
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  handleProfessionalProfile = () => {
    this.updateScreen(
      <ProfessionalProfilePage
        updateScreen={this.updateScreen}
        inputpayload={this.state.inputpayload}
        currentLocation={this.props.currentLocation}
        AuthPass={this.state.AuthPass}
      />
    );
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleUserLogin = () => {
    if (!this.state.usernameValid) {
      this.setState(
        {
          usermessage: "Please Login Before Pro Signup",
          variant: "warning",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        },
        () => {
          // this.handleLogin();
        }
      );
    }
  };
  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.FullName == 1 &&
      this.state[EMAIL] == 1 &&
      this.state.emailValid &&
      this.state[PASSWORD] === 1 &&
      this.state.username === 1 &&
      this.state.usernameValid
        ? true
        : false;
    this.setState({ isFormValid: isFormValid });
  };

  handleUsernameErrMsgColor = (response) => {
    switch (response.code) {
      case 200:
        return `${GREEN_COLOR}`;
        break;
      case 409:
        return `${Red_Color}`;
        break;
    }
  };

  handleUsernameErrMsg = (response) => {
    switch (response.code) {
      case 200:
        return response.message;
        break;
      case 409:
        return `username is registered`;
        break;
    }
  };

  handleUserNameCheck = () => {
    if (this.state.username) {
      let userNamepayload = {
        username: this.state.inputpayload.username,
      };
      UserNameVerfication(userNamepayload)
        .then((res) => {
          console.log("res--->", res);
          let response = res.data;
          if (response) {
            this.setState(
              {
                usernameChecked: true,
                usernameErrMsg: this.handleUsernameErrMsg(response),
                usernameErrMsgClr: this.handleUsernameErrMsgColor(response),
                usernameValid: response.code == 200 ? true : false,
              },
              () => {
                this.checkIfFormValid();
                this.handleUserLogin();
              }
            );
          }
        })
        .catch((err) => {
          console.log("err", err, err.message);
          this.setState(
            {
              usernameErrMsg: err.message,
              usernameErrMsgClr: `${Red_Color}`,
              usernameValid: false,
            },
            () => {
              this.checkIfFormValid();
              this.handleUserLogin();
            }
          );
        });
    }
  };

  googleResponseOnSuccess = (response) => {
    let googleResponse = response;

    console.log("Google", googleResponse);

    let googleLoginPayload = {
      signupType: Google_Signup_Type,
      deviceType: Web_deviceType,
      googleId: googleResponse.googleId,
      Email: googleResponse.profileObj.email,
      FullName: googleResponse.profileObj.name,
      profilePicUrl: googleResponse.profileObj.imageUrl,
      accessToken: googleResponse.accessToken,
      googleToken: googleResponse.tokenId,
    };
    this.setState(
      {
        inputpayload: googleLoginPayload,
        emailChecked: true,
        emailValid: true,
        Email: 1,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  googleResponseOnFailure = (error) => {
    console.log("googleError", error);
    this.setState({
      usermessage: error.error,
      variant: "error",
      open: true,
      vertical: "bottom",
      horizontal: "left",
    });
  };

  responseFacebook = (response) => {
    console.log("FacebookResponse", response);
    let facebookResponse = response;

    let facebookLoginPayload = {
      signupType: Facebook_Signup_Type,
      deviceType: Web_deviceType,
      // profilePicUrl: facebookResponse.profilePicUrl,
      facebookId: facebookResponse.userID,
      Email: facebookResponse.email,
      FullName: facebookResponse.name,
      accessToken: facebookResponse.accessToken,
    };
    this.setState(
      {
        inputpayload: { ...facebookLoginPayload },
        emailChecked: true,
        emailValid: true,
        Email: 1,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  handlePasswordView = () => {
    this.setState({
      passView: !this.state.passView,
    });
  };

  render() {
    const { isFormValid, isRemembered, AuthPass, passView } = this.state;
    const { Email, Password, username, FullName } = {
      ...this.state.inputpayload,
    };

    const passHits = (
      <div className="passHitsContent">
        <ol className="m-0 pl-2">
          <li>Your password must be at least 8 characters</li>
          <li>Must have at least 1 number</li>
          <li>Must have at least 1 upper case and 1 lower case character</li>
          <li>Must have at least 1 special character</li>
        </ol>
      </div>
    );

    return (
      <Wrapper>
        {!this.state.currentScreen ? (
          <Wrapper>
            <div className="ProfessionaSignupPageBanner">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div className="row m-0 align-items-center py-5">
                    <div className="col-1 p-0"></div>
                    <div className="col-11 px-4">
                      <div className="SignupSec">
                        <p className="Title">Introducing Boatzon Pro</p>
                        <h5 className="bannerCaption">
                          Are you ready to meet new customers?
                        </h5>
                        <p className="bannerDesc">
                          Boatzon Pro is an all-in-one managment and marketing
                          solution built for boat professionals and boat
                          dealers.
                        </p>
                        <div className="signUpCard">
                          <h5 className="heading">Sign Up</h5>
                          <div className="FormInput">
                            <InputWithIcon
                              type="text"
                              className="SignUpInputBox form-control"
                              name={FULL_NAME}
                              autoFocus={AuthPass ? false : true}
                              readOnly={AuthPass ? true : false}
                              value={FullName}
                              onKeyPress={PureTextValidator}
                              inputAdornment={
                                <img
                                  src={ProfileName_Icon}
                                  className="nameIcon"
                                ></img>
                              }
                              placeholder="Name"
                              onChange={this.handleOnchangeInput}
                              autoComplete="off"
                            />

                            {this.state[FULL_NAME] == 0 ? (
                              <p
                                className="errMessage"
                                style={{ color: `${Red_Color}` }}
                              >
                                Name field can't be empty
                              </p>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="FormInput">
                            <div className="position-relative">
                              <InputWithIcon
                                type="text"
                                className="SignUpInputBox form-control"
                                name="username"
                                inputAdornment={
                                  <img
                                    src={Professional_Type_Icon}
                                    className="profileIcon"
                                  ></img>
                                }
                                readOnly={AuthPass ? true : false}
                                placeholder="Username"
                                value={username}
                                onBlur={this.handleUserNameCheck}
                                onChange={
                                  AuthPass ? "" : this.handleOnchangeInput
                                }
                                autoComplete="off"
                              />
                              {this.state.usernameChecked ? (
                                this.state.usernameValid &&
                                this.state.username == 1 ? (
                                  <img
                                    src={Verified_Icon}
                                    className="verifiedIcon"
                                  ></img>
                                ) : (
                                  <img
                                    src={NotVerified_Icon}
                                    className="notVerifiedIcon"
                                  ></img>
                                )
                              ) : (
                                ""
                              )}
                            </div>
                            {this.state.usernameChecked ? (
                              <p
                                className="errMessage"
                                style={{
                                  color: this.state.usernameErrMsgClr,
                                }}
                              >
                                {this.state.usernameErrMsg}
                              </p>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="FormInput">
                            <div className="position-relative">
                              <InputWithIcon
                                type="email"
                                className="SignUpInputBox form-control"
                                name={EMAIL}
                                inputAdornment={
                                  <img
                                    src={Email_Icon}
                                    className="emailIcon"
                                  ></img>
                                }
                                value={Email}
                                readOnly={AuthPass ? true : false}
                                placeholder="Enter Your Email"
                                onBlur={this.handleProEmailCheck}
                                onChange={this.handleOnchangeInput}
                                autoComplete="off"
                              />
                              {this.state.emailChecked ? (
                                this.state.emailValid &&
                                this.state[EMAIL] == 1 ? (
                                  <img
                                    src={Verified_Icon}
                                    className="verifiedIcon"
                                  ></img>
                                ) : (
                                  <img
                                    src={NotVerified_Icon}
                                    className="notVerifiedIcon"
                                  ></img>
                                )
                              ) : (
                                ""
                              )}
                            </div>
                            {this.state[EMAIL] == 0 ||
                            this.state[EMAIL] == 2 ? (
                              <p
                                className="errMessage"
                                style={{ color: `${Red_Color}` }}
                              >
                                Enter Valid Email
                              </p>
                            ) : (
                              ""
                            )}
                            {this.state[EMAIL] == 1 ? (
                              this.state.emailChecked ? (
                                <p
                                  className="errMessage"
                                  style={{ color: this.state.emailErrMsgClr }}
                                >
                                  {this.state.emailErrMsg}
                                </p>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}
                          </div>
                          {AuthPass ? (
                            ""
                          ) : (
                            <div className="FormInput">
                              <div className="position-relative">
                                <InputWithIcon
                                  type={passView ? "text" : "password"}
                                  className="SignUpInputBox form-control"
                                  name={PASSWORD}
                                  inputAdornment={
                                    <img
                                      src={Password_Icon}
                                      className="passwordIcon"
                                    ></img>
                                  }
                                  value={Password}
                                  placeholder="Password"
                                  onChange={this.handleOnchangeInput}
                                  autoComplete="off"
                                />
                                {Password ? (
                                  <img
                                    src={
                                      passView
                                        ? View_Grey_Filled_Icon
                                        : View_Grey_UnFilled_Icon
                                    }
                                    className="viewIcon"
                                    onClick={this.handlePasswordView}
                                  ></img>
                                ) : (
                                  ""
                                )}
                              </div>
                              {this.state[PASSWORD] == 3 ? (
                                <p
                                  className="errMessage"
                                  style={{ color: `${Red_Color}` }}
                                >
                                  Not valid password{" "}
                                  <InfoTooltip
                                    tooltipContent={passHits}
                                    placement="bottom"
                                    whiteBg={true}
                                    color={BG_LightGREY_COLOR}
                                  >
                                    <img
                                      src={Info_Icon_Red}
                                      className="passwordHits"
                                    ></img>
                                  </InfoTooltip>
                                </p>
                              ) : (
                                ""
                              )}
                            </div>
                          )}
                          <div
                            className={
                              AuthPass
                                ? "Login_Btn mb-2"
                                : !isFormValid
                                ? "inactive_Btn mb-2"
                                : "Login_Btn mb-2"
                            }
                          >
                            <ButtonComp
                              disabled={AuthPass ? false : !isFormValid}
                              onClick={this.handleProfessionalProfile}
                            >
                              Create Free Account
                            </ButtonComp>
                          </div>
                          <p className="TermsMsg">
                            By continuing, you agree to Boatzon’s{" "}
                            <span>Terms of Use and Privacy Policy.</span>
                          </p>
                          {AuthPass ? (
                            ""
                          ) : (
                            <Wrapper>
                              <div className="position-relative">
                                <p className="text">or</p>
                              </div>
                              <div className="GoogleLogin_Btn">
                                <img
                                  src={Google_Icon}
                                  className="googleIcon"
                                ></img>
                                <GoogleLogin
                                  clientId="1029574855542-sqas8esrvk6ajj1fp2rpqo41raojvtkv.apps.googleusercontent.com"
                                  buttonText={`Sign up with Google`}
                                  onSuccess={this.googleResponseOnSuccess}
                                  onFailure={this.googleResponseOnFailure}
                                  cookiePolicy={"single_host_origin"}
                                  icon={false}
                                />
                              </div>
                              <div className="FacebookLogin_Btn">
                                <img
                                  src={Facebook_Icon}
                                  className="facebookIcon"
                                ></img>
                                <FacebookLogin
                                  appId={FB_APP_ID}
                                  autoLoad={false}
                                  textButton={`Sign up with Facebook`}
                                  fields="name,email,picture"
                                  callback={this.responseFacebook}
                                />
                              </div>
                            </Wrapper>
                          )}
                        </div>
                        <p className="LoginMsg">
                          Already have an Boatzon Pro account?{" "}
                          <span>Log in</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="ProfessionalSignupPageContent">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div className="row m-0 align-items-center py-4">
                    <div className="col-9 mx-auto">
                      <div className="text-center">
                        <h6 className="contentheading">
                          Why Boatzon for Professionals & Boat Dealers?
                        </h6>
                        <p className="caption">
                          We know how you work. We designed Boatzon and our
                          services to help industry professionals and boat
                          dealers to manage and grow their businesses with ease.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0 py-4">
                    <div className="col-6">
                      <div className="row m-0 d-flex justify-content-center">
                        <div className="col-2 pl-0">
                          <img
                            src={Customer_Icon}
                            className="featureIcon"
                          ></img>
                        </div>
                        <div className="col-8 p-0">
                          <div className="pl-2">
                            <img src={Vector_Small} className="vectorImg"></img>
                            <h6 className="featureTitle">
                              Let us introduce you to new customers
                            </h6>
                            <p className="featureDesc">
                              Gain and interact new customers every day. Reach
                              boaters in your area or region, build name
                              recognition, showcase your boats and products,
                              advertise your services, and even get matched with
                              prospective clients across the nation.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="row m-0 d-flex justify-content-center">
                        <div className="col-2 pl-0">
                          <img
                            src={Leads_To_Customers_Icon}
                            className="featureIcon"
                          ></img>
                        </div>
                        <div className="col-8 p-0">
                          <div className="pl-2">
                            <img src={Vector_Small} className="vectorImg"></img>
                            <h6 className="featureTitle">
                              An easer way to turn leads…into customers
                            </h6>
                            <p className="featureDesc">
                              Talk to sellers, even setup live video
                              appointments to view a boat or product and
                              negotiate a price.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0 py-4">
                    <div className="col-6">
                      <div className="row m-0 d-flex justify-content-center">
                        <div className="col-2 pl-0">
                          <img
                            src={Professionals_Feature_Icon}
                            className="featureIcon"
                          ></img>
                        </div>
                        <div className="col-8 p-0">
                          <div className="pl-2">
                            <img src={Vector_Small} className="vectorImg"></img>
                            <h6 className="featureTitle">Professionals</h6>
                            <p className="featureDesc">
                              From engine mechanics, to electricians, to
                              detailers…sign up your business and connect with
                              thousands of users. Share your services and even
                              post boats or products to sell. Join Discussions
                              and meet new clients every day.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="row m-0 d-flex justify-content-center">
                        <div className="col-2 pl-0">
                          <img
                            src={Credit_Card_Processing_Icon}
                            className="featureIcon"
                          ></img>
                        </div>
                        <div className="col-8 p-0">
                          <div className="pl-2">
                            <img src={Vector_Small} className="vectorImg"></img>
                            <h6 className="featureTitle">
                              Your own credit card processing account
                            </h6>
                            <p className="featureDesc">
                              With our partnership with Stripe.com, activate
                              your own credit card processing account to take
                              payment on Boatzon and even in your store.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0 pt-4">
                    <div className="col-6">
                      <div className="row m-0 d-flex justify-content-center">
                        <div className="col-2 pl-0">
                          <img
                            src={Boat_Dealers_Icon}
                            className="featureIcon"
                          ></img>
                        </div>
                        <div className="col-8 p-0">
                          <div className="pl-2">
                            <img src={Vector_Small} className="vectorImg"></img>
                            <h6 className="featureTitle">Boat Dealers</h6>
                            <p className="featureDesc">
                              Experience a new way to showcase your boat
                              dealerships. Don’t just sell your boats…sell any
                              product and showcase your services to thousands of
                              consumers.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="row m-0 d-flex justify-content-center">
                        <div className="col-2 pl-0">
                          <img
                            src={Boats_Products_Everywhere}
                            className="featureIcon"
                          ></img>
                        </div>
                        <div className="col-8 p-0">
                          <div className="pl-2">
                            <img src={Vector_Small} className="vectorImg"></img>
                            <h6 className="featureTitle">
                              Expose your boat or products everywhere
                            </h6>
                            <p className="featureDesc">
                              Promote your boat or product to be viewed on
                              Craigslist, Facebook Marketplace, Instagram, and
                              Google Shopping.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="ProfessionaSignupPageFooter"></div>
            </div>
          </Wrapper>
        ) : (
          this.state.currentScreen
        )}

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>

        <style jsx>
          {`
            .ProfessionaSignupPageFooter {
              min-height: 17.569vw;
              background: url(${ProfessionalSignup_FooterBanner_BG});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              position: relative;
            }
            .nameIcon {
              width: 0.951vw;
            }
            .profileIcon {
              width: 1.098vw;
            }
            .passwordHits {
              width: 0.732vw;
              margin-bottom: 0.146vw;
              cursor: pointer;
            }
            :global(.passHitsContent) {
              padding: 0.732vw;
            }
            :global(.passHitsContent li) {
              font-size: 0.732vw;
              color: ${Red_Color};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
            }
            .featureDesc {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0;
            }
            .featureTitle {
              font-size: 1.171vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0 0 0.585vw 0;
              max-width: 80%;
            }
            .featureIcon {
              width: 100%;
              object-fit: cover;
            }
            .vectorImg {
              width: 4.758vw;
              margin: 0 0 0.732vw 0;
            }
            .contentheading {
              font-size: 1.317vw;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0 0 0.585vw 0;
              font-weight: 600;
            }
            .caption {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0 0 0.732vw 0;
            }
            .ProfessionalSignupPageContent {
              background: ${BG_LightGREY_COLOR};
              position: relative;
            }
            .LoginMsg {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 1.098vw 0 0 0;
            }
            .LoginMsg span {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${Light_Blue_Color};
            }
            .ProfessionaSignupPageBanner {
              min-height: 43.923vw;
              background: url(${ProfessionalSignup_Banner_BG});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              position: relative;
            }
            .SignupSec {
              max-width: 23.426vw;
            }
            .Title {
              font-size: 0.878vw;
              font-family: "Museo-Sans" !important;
              text-transform: capitalize;
              font-weight: 600;
              color: ${THEME_COLOR};
              margin: 0 0 0.585vw 0;
            }
            .bannerCaption {
              font-size: 1.667vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
              letter-spacing: 0.0439vw !important;
              margin: 0 0 0.585vw 0;
              max-width: 78%;
            }
            .bannerCaption:first-letter {
              text-transform: capitalize;
            }
            .bannerDesc {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0 0 1.098vw 0;
            }
            .bannerDesc:first-letter {
              text-transform: capitalize;
            }
            .signUpCard {
              padding: 1.83vw 1.464vw;
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
            }
            .heading {
              margin: 0 0 1.317vw 0;
              font-size: 1.522vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .FormInput {
              margin: 1.098vw 0;
              position: relative;
            }
            .viewIcon {
              position: absolute;
              right: 10px;
              top: 50%;
              transform: translate(-50%, 0);
              cursor: pointer;
              width: 0.951vw;
            }
            .notVerifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            :global(.signUpCard .SignUpInputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0.375rem 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${BLACK_COLOR};
            }
            :global(.signUpCard .SignUpInputBox input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.signUpCard .SignUpInputBox input::placeholder) {
              font-size: 0.878vw;
              text-transform: lowercase;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Open Sans" !important;
            }
            :global(.signUpCard .SignUpInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.signUpCard .SignUpInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.Login_Btn button) {
              width: 100%;
              margin: 0.732vw 0 0.366vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
            }
            :global(.Login_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.Login_Btn button:focus),
            :global(.Login_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.Login_Btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.366vw 0 0 0;
            }
            .TermsMsg {
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              margin: 0.732vw 0;
              padding: 0 0.366vw;
              text-align: center;
            }
            .TermsMsg:first-letter {
              text-transform: capitalize;
            }
            .TermsMsg span {
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
            }
            .text {
              margin: 0;
              text-align: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .text:before {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              right: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .text:after {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              left: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .GoogleLogin_Btn,
            .FacebookLogin_Btn {
              position: relative;
            }
            :global(.GoogleLogin_Btn button) {
              width: 100%;
              margin: 0.732vw 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR} !important;
              color: ${FONTGREY_COLOR} !important;
              position: relative;
              box-shadow: none !important;
              border-radius: 0.146vw;
              opacity: 1 !important;
            }
            :global(.GoogleLogin_Btn button div) {
              padding: 0 !important;
              background: none !important;
            }
            :global(.GoogleLogin_Btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0366vw !important;
              font-size: 0.951vw !important;
              font-weight: 600 !important;
              padding: 0 !important;
              width: 100%;
            }
            :global(.GoogleLogin_Btn button:hover) {
              background: ${Border_LightGREY_COLOR} !important;
              opacity: 1 !important;
            }
            :global(.GoogleLogin_Btn button:focus),
            :global(.GoogleLogin_Btn button:active) {
              background: ${Border_LightGREY_COLOR} !important;
              outline: none !important;
              opacity: 1 !important;
            }

            :global(.FacebookLogin_Btn button) {
              width: 100%;
              margin: 0;
              padding: 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${Facebook_Blue};
              color: ${WHITE_COLOR};
              position: relative !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0366vw !important;
              font-size: 0.951vw;
              font-weight: 600;
              border-radius: 0.146vw;
            }
            :global(.FacebookLogin_Btn button:hover) {
              background: ${Facebook_Blue};
            }
            :global(.FacebookLogin_Btn button:focus),
            :global(.FacebookLogin_Btn button:active) {
              background: ${Facebook_Blue};
              outline: none;
            }
            .googleIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
            .facebookIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(ProfessionalSignupPage);
