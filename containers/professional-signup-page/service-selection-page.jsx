import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  Search_Light_Grey_Icon,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
  GREY_VARIANT_2,
  GREEN_COLOR,
  Active_Dot,
  Inactive_Dot,
  Long_Arrow_Left_LightGrey,
  Border_LightGREY_COLOR_1,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import cloneDeep from "lodash.clonedeep";
import { TreeSelect } from "antd"; 
import { isEmpty } from "../../lib/global";


class ServiceSelectionPage extends Component {
  state = {
    inputpayload: {},
    selectedSpeciality: [],
    selectedSubSpeciality: [],
    isFormValid: false,
    selected: false,
  };

  componentDidMount() {
    let { speciality, subSpeciality } = cloneDeep(
      this.props.inputpayload
    ).__wrapped__;
    this.setState(
      {
        inputpayload: cloneDeep(this.props.inputpayload).__wrapped__,
      },
      () => {
        if (subSpeciality) {
          this.setState(
            {
              selectedSpeciality: [...speciality],
              selectedSubSpeciality: [...subSpeciality],
              value: [...speciality, ...subSpeciality],
              selected: true,
            },
            () => {
              this.checkIfFormValid();
            }
          );
        }
      }
    );
  }

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = !isEmpty(this.state.selectedSubSpeciality) ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  onChange = (totalValues) => {
    const { specialityList } = this.props;
    let mainSpecialityList =
      specialityList && specialityList.map((data) => data.name);

    let selectedSpeciality =
      totalValues &&
      totalValues.filter((data) => mainSpecialityList.includes(data));
    let selectedSubSpeciality =
      totalValues &&
      totalValues.filter((data) => !selectedSpeciality.includes(data));

    this.setState(
      {
        selected: isEmpty(totalValues) || totalValues.length < 8 ? false : true,
        value: totalValues,
        selectedSpeciality,
        selectedSubSpeciality,
      },
      () => {
        this.checkIfFormValid();
        let tempPayload = { ...this.state.inputpayload };
        tempPayload["speciality"] = [...this.state.selectedSpeciality];
        tempPayload["subSpeciality"] = [...this.state.selectedSubSpeciality];
        this.setState({
          inputpayload: cloneDeep(tempPayload).__wrapped__,
        });
      }
    );
  };

  render() {
    const { SHOW_ALL } = TreeSelect;

    const { specialityList, activePage } = this.props;
    const treeData =
      specialityList &&
      specialityList.map((data, pIndex) => ({
        title: data.name,
        nodeId: data.nodeId,
        value: data.name,
        key: `0-${pIndex}`,
        children:
          data &&
          data.subSpeciality &&
          data.subSpeciality.length &&
          data.subSpeciality.map((child, cIndex) => ({
            title: child.name,
            nodeId: child.nodeId,
            value: child.name,
            key: `0-${pIndex}-${cIndex}`,
          })),
      }));

    const { isFormValid } = this.state;
    console.log("specialityList", treeData);
    return (
      <Wrapper>
        <div className="serviceSelectionPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0 align-items-center py-5">
                <div className="col-10 mx-auto">
                  <h5 className="heading text-center">
                    What services do you provide?
                  </h5>
                  <p className="caption text-center">
                    Select all the services you provide.
                  </p>
                  <div className="serviceSelectionContent position-relative">
                    <div id="serviceSelectInput" className="SelectInput">
                      <img
                        src={Search_Light_Grey_Icon}
                        className="searchIcon"
                      ></img>
                      <TreeSelect
                        open={true}
                        value={this.state.value}
                        treeData={treeData}
                        onChange={this.onChange}
                        treeCheckable={true}
                        showCheckedStrategy={SHOW_ALL}
                        placeholder="Please select"
                        showSearch={true}
                        filterTreeNode={(search, item) => {
                          return (
                            item.title
                              .toLowerCase()
                              .indexOf(search.toLowerCase()) >= 0
                          );
                        }}
                        getPopupContainer={() =>
                          document.getElementById("serviceSelectInput")
                        }
                      />
                    </div>
                    <div className="row m-0 justify-content-center">
                      <div className="col-5 mx-aut0">
                        <div
                          className={
                            !isFormValid ? "inactive_Btn" : "continue_btn"
                          }
                        >
                          <ButtonComp
                            disabled={!isFormValid}
                            onClick={this.props.handleUploadProfilePhoto.bind(
                              this,
                              this.state.inputpayload
                            )}
                          >
                            Continue
                          </ButtonComp>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slider-dots">
                    <div className="back_btn">
                      <ButtonComp
                        onClick={this.props.handleBackPrevScreen.bind(
                          this,
                          activePage
                        )}
                      >
                        <img
                          src={Long_Arrow_Left_LightGrey}
                          className="backIcon"
                        ></img>
                        Back
                      </ButtonComp>
                    </div>
                    <div>
                      <img
                        src={activePage == 1 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 2 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 3 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 4 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 5 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 6 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 7 ? Active_Dot : Inactive_Dot}
                      ></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .serviceSelectionPage {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .caption {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .serviceSelectionContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 0.732vw;
              margin: 1.464vw 0;
            }
            .searchIcon {
              width: 0.878vw;
              position: absolute;
              top: 1.724vw;
              left: 1.332vw;
              z-index: 2;
              margin-right: 0.732vw;
            }

            .continue_btn,
            .inactive_Btn {
              margin: 0.732vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            :global(.serviceSelectionContent .SelectInput) {
              max-height: 23.438vw;
              height: 23.438vw;
            }

            :global(.serviceSelectionContent .SelectInput .ant-tree-select) {
              width: 100%;
              max-height: 6.25vw;
            }

            :global(.serviceSelectionContent
                .SelectInput
                .ant-tree-select
                .ant-select-selector) {
              max-height: 6.25vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
              padding: 0.521vw 0;
            }
            :global(.serviceSelectionContent .SelectInput div:nth-child(3)) {
              top: unset !important;
              left: unset !important;
            }

            :global(.ant-select-multiple .ant-select-selector::after) {
              margin: unset !important;
              line-height: unset !important;
              content: unset !important;
            }

            :global(.serviceSelectionContent
                .SelectInput
                .ant-tree-select-dropdown) {
              box-shadow: none;
              scroll-behavior: smooth;
              z-index: 1;
              width: 98% !important;
              min-width: 98% !important;
              top: 0 !important;
              min-height: ${this.state.selected ? "17vw" : "20.45vw"};
              max-height: 17vw;
            }
            :global(.ant-select-selection-overflow) {
              margin-left: 1.7625vw;
              display: flex;
              flex-wrap: wrap;
            }
            :global(.ant-select-multiple .ant-select-selection-placeholder) {
              left: 1.9625vw !important;
            }
            :global(.ant-select-multiple
                .ant-select-selection-search:first-child
                .ant-select-selection-search-input) {
              margin-left: unset !important;
            }
            :global(.serviceSelectionContent
                .SelectInput
                .ant-tree-select
                .ant-select-selection-item) {
              background: ${GREY_VARIANT_3} !important;
              color: ${GREY_VARIANT_1} !important;
              font-family: "Open Sans" !important;
              font-size: 0.729vw !important;
            }
            :global(.serviceSelectionContent
                .SelectInput
                .ant-tree-select
                .ant-select-selection-item
                .ant-select-selection-item-remove) {
              font-size: 0.573vw !important;
            }

            :global(.serviceSelectionContent
                .SelectInput
                .ant-tree-select-dropdown
                .ant-select-tree-title) {
              font-size: 0.833vw !important;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1} !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ServiceSelectionPage;
