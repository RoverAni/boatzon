import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  Successful_SignUp,
  GREY_VARIANT_2,
  FONTGREY_COLOR_Dark,
  THEME_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomLink from "../../components/Link/Link";

class SuccessfullModel extends Component {
  render() {
    return (
      <Wrapper>
        <div className="proAddressPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0 align-items-center py-5">
                <div className="col-9 mx-auto">
                  <div className="addressContent">
                    <div className="text-center">
                      <img
                        src={Successful_SignUp}
                        className="successfullLogo"
                      ></img>
                      <h6 className="heading">Congratulations!</h6>
                      <p className="desc">
                        Your Professional/Pro Dealer account is setup.
                      </p>
                      <div className="redirect_btn">
                        <CustomLink href="/my-account/acc-settings">
                          <ButtonComp>Go to My Account</ButtonComp>
                        </CustomLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .proAddressPage {
              background: ${BG_LightGREY_COLOR};
            }
            .addressContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 2.562vw 1.464vw;
              margin: 1.464vw 0;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0.732vw 0;
            }
            .desc {
              font-family: "Open Sans-Semibold" !important;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              margin: 0 auto;
              max-width: 85%;
            }
            .successfullLogo {
              width: 20%;
            }
            .redirect_btn {
              margin: 1.098vw 0 0 0;
            }
            :global(.redirect_btn button) {
              width: fit-content;
              padding: 0.292vw 1.098vw;
              margin: 0 !important;
              background: ${THEME_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.redirect_btn button span) {
              font-family: "Open Sans-Semibold" !important;
              font-size: 0.951vw;
            }
            :global(.redirect_btn button:focus),
            :global(.redirect_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.redirect_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SuccessfullModel;
