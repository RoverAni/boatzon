import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  WHITE_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_3,
  City_Icon,
  THEME_COLOR,
  GREY_VARIANT_1,
  Light_Blue_Color,
  ProAddress_Img1,
  GREEN_COLOR,
  Border_LightGREY_COLOR_2,
  Active_Dot,
  Inactive_Dot,
  Long_Arrow_Left_LightGrey,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_1,
} from "../../lib/config";
import LocationSearchInput from "../location/location-search-box";
import InputBox from "../../components/input-box/input-box";
import SelectInput from "../../components/input-box/select-input";
import ButtonComp from "../../components/button/button";
import { getStates } from "country-state-picker";
import MyLocationModel from "../filter-sec/my-location-model";
import Model from "../../components/model/model";
import { requiredValidator } from "../../lib/validation/validation";

class ProfessionalBusinessAddressPage extends Component {
  state = {
    city: "",
    zipCode: "",
    inputpayload: {},
    locationModel: false,
    isFormValid: false,
  };

  componentDidMount() {
    let { stateName } = { ...this.props.inputpayload };
    this.setState(
      {
        inputpayload: { ...this.props.inputpayload },
        isAddressSelected: this.props.inputpayload.address_line ? true : false,
      },
      () => {
        if (stateName) {
          let tempStateName = {
            value: stateName,
            label: stateName,
          };
          this.setState({
            stateName: tempStateName,
          });
        }
        this.checkIfFormValid();
      }
    );
  }

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.isAddressSelected && this.state.inputpayload.stateName
        ? true
        : false;

    this.setState({ isFormValid: isFormValid });
  };

  updateUserAddress = (data) => {
    console.log("nficf", data);
    if (data) {
      this.setState(
        {
          stateName: "",
          address_line1: data.address,
          city: data.city,
          zipCode: data.zipCode,
          country: data.country,
          isAddressSelected: true,
          latitude: data.lat,
          logitude: data.lng,
          countryShortName: data.countryShortName,
        },
        () => {
          let tempPayload = { ...this.state.inputpayload };
          tempPayload["address_line"] = this.state.address_line1;
          tempPayload["city"] = this.state.city;
          tempPayload["zipCode"] = this.state.zipCode;
          tempPayload["country"] = this.state.country;
          tempPayload["latitude"] = this.state.latitude;
          tempPayload["logitude"] = this.state.logitude;
          tempPayload["countryShortName"] = this.state.countryShortName;
          this.checkIfFormValid();
          let stateList = getStates(this.state.countryShortName.toLowerCase());
          this.setState({
            stateList,
            locationModel: false,
            inputpayload: { ...tempPayload },
          });
        }
      );
    } else {
      this.setState({ isAddressSelected: false }, () => {
        this.checkIfFormValid();
      });
    }
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    console.log("fejf-->", event.target.value, event.target.name);
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    console.log("fejf", inputControl);
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    console.log("fejf", tempsignUpPayload);
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
      },
      () => {
        // this.props.dispatch(setSignupData(this.state.inputpayload));
        console.log("inputpayload", this.state.inputpayload);
      }
    );
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = { ...this.state.inputpayload };
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl.value;
      this.setState(
        {
          inputpayload: { ...temppayload },
          [name]: {
            value: inputControl.value,
            label: inputControl.value,
          },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  handleLocationModel = () => {
    this.setState({
      locationModel: !this.state.locationModel,
    });
  };

  render() {
    const StateList =
      this.state.stateList &&
      this.state.stateList.map((data) => ({
        value: data,
        label: data,
      }));
    const { activePage } = this.props;
    const { isFormValid, stateName } = this.state;
    const { address_line, city, zipCode } = { ...this.state.inputpayload };
    return (
      <Wrapper>
        <div className="proAddressPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0 align-items-center py-5">
                <div className="col-10 mx-auto">
                  <h5 className="heading text-center">
                    What is your business address?
                  </h5>
                  <div className="addressContent">
                    <div className="row m-0">
                      <div className="col-6 contentForm px-4">
                        <div>
                          <h6 className="title">Address</h6>
                          <div className="FormInput">
                            <LocationSearchInput
                              autoFocus={true}
                              id="user-address-box"
                              locationIcon={true}
                              borderBottomWithIcon={true}
                              address={address_line}
                              updateLocation={this.updateUserAddress}
                              handleChange={this.updateUserAddress}
                              showOnlyAddress={true}
                              showAddressFromProps={true}
                            ></LocationSearchInput>
                          </div>
                          <div className="addFormInput">
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              name="address_line2"
                              placeholder="Address Line 2(Landmark)"
                              onChange={this.handleOnchangeInput}
                              autoComplete="off"
                            ></InputBox>
                          </div>
                          <div className="CityFormInput">
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              name="city"
                              placeholder="City"
                              value={city}
                              autoComplete="off"
                            ></InputBox>
                          </div>
                          <div className="row m-0">
                            <div className="col-6 pl-0">
                              <div>
                                <SelectInput
                                  stateType={true}
                                  placeholder="State"
                                  options={StateList}
                                  value={stateName}
                                  onChange={this.handleOnSelectInput(
                                    "stateName"
                                  )}
                                />
                              </div>
                            </div>
                            <div className="col-6 pr-0">
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                name="zipCode"
                                placeholder="Zip code"
                                value={zipCode}
                                autoComplete="off"
                              ></InputBox>
                            </div>
                          </div>
                          <p className="changeCountryMsg">
                            Not in{" "}
                            {this.state.country
                              ? this.state.country
                              : this.props.currentLocation.country}
                            ?{" "}
                            <span onClick={this.handleLocationModel}>
                              Change Country
                            </span>{" "}
                          </p>
                          <div
                            className={
                              !isFormValid ? "inactive_Btn" : "continue_btn"
                            }
                          >
                            <ButtonComp
                              disabled={!isFormValid}
                              onClick={this.props.handleBusinessDetails.bind(
                                this,
                                this.state.inputpayload
                              )}
                            >
                              Continue
                            </ButtonComp>
                          </div>
                        </div>
                      </div>
                      <div className="col-6 px-4">
                        <div>
                          <h6 className="profileTitle">
                            Get found by clients near you!
                          </h6>
                          <div className="row mx-0 align-items-center my-2">
                            <div className="col-2 p-0">
                              <img
                                src={ProAddress_Img1}
                                className="featureImg"
                              ></img>
                            </div>
                            <div className="col-10 pr-0">
                              <p className="featureDesc m-0">
                                We'll display your business address on your
                                profile and sure you show up in Boatzon search
                                results locally.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slider-dots">
                    <div className="back_btn">
                      <ButtonComp
                        onClick={this.props.handleBackPrevScreen.bind(
                          this,
                          activePage
                        )}
                      >
                        <img
                          src={Long_Arrow_Left_LightGrey}
                          className="backIcon"
                        ></img>
                        Back
                      </ButtonComp>
                    </div>
                    <div>
                      <img
                        src={activePage == 1 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 2 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 3 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 4 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 5 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 6 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 7 ? Active_Dot : Inactive_Dot}
                      ></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* MyLocation Model */}
        <Model
          open={this.state.locationModel}
          onClose={this.handleLocationModel}
        >
          <MyLocationModel
            onClose={this.handleLocationModel}
            currentLocation={this.props.currentLocation}
            updateUserAddress={this.updateUserAddress}
          />
        </Model>
        <style jsx>
          {`
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .proAddressPage {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .addressContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 2.562vw 1.464vw;
              margin: 1.464vw 0;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .FormInput {
              margin: 1.098vw 0;
            }
            :global(.proAddressPage .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 1px solid ${GREY_VARIANT_3};
              border-radius: 0;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.proAddressPage .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
            :global(.proAddressPage .addFormInput .inputBox) {
              padding: 0 0.732vw 0 1.464vw;
            }
            .contentForm {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .CityFormInput {
              margin: 1.098vw 0;
              display: flex;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            :global(.proAddressPage .CityFormInput .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0 0 0 0.366vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.proAddressPage .CityFormInput)::before {
              content: url(${City_Icon});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
              top: 0.292vw;
            }
            :global(.proAddressPage .CityFormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.proAddressPage .CityFormInput .inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
            .changeCountryMsg {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
              margin: 0;
              padding: 0.732vw 0;
            }
            .changeCountryMsg span {
              color: ${Light_Blue_Color};
              cursor: pointer;
            }
            .continue_btn,
            .inactive_Btn {
              margin: 0.732vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .profileTitle {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 1.098vw;
            }
            .featureImg {
              width: 85%;
              object-fit: cover;
            }
            .featureDesc {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
            }
            .featureDesc:first-letter,
            .profileTitle:first-letter {
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default ProfessionalBusinessAddressPage;
