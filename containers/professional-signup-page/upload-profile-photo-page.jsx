import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR,
  Img_Download_White_Icon,
  GREY_VARIANT_2,
  UploadPic_1,
  UploadPic_2,
  Tick_Bulletpoint_Icon_Green,
  Border_LightGREY_COLOR_2,
  Active_Dot,
  Inactive_Dot,
  Long_Arrow_Left_LightGrey,
  IMAGE_UPLOAD_SIZE,
  Camera_Icon,
  Border_LightGREY_COLOR_1,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import cloneDeep from "lodash.clonedeep";

class UploadProfilePhotoPage extends Component {
  state = {
    ProductImg: {},
    isFormValid: false,
  };

  componentDidMount() {
    const { productImgs } = this.props.inputpayload;
    this.setState(
      {
        inputpayload: cloneDeep(this.props.inputpayload).__wrapped__,
      },
      () => {
        this.setState(
          {
            ProductImg: productImgs ? productImgs : {},
            productpic: productImgs && productImgs.url ? true : false,
            isDocValid: productImgs && productImgs.url ? true : false,
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    );
  }

  // used to create image payload
  handleUploadFile = (event) => {
    let files = event.target.files;
    if (
      files &&
      files[0] &&
      (files[0].type == "image/jpeg" || files[0].type == "image/png")
    ) {
      if (this.isFileSizeValid(files[0])) {
        let imgPayload = {
          fileDetails: files[0],
          url: URL.createObjectURL(files[0]),
        };

        let tempInputPayload = { ...this.state.inputpayload };
        tempInputPayload.productImgs = { ...imgPayload };

        this.setState(
          {
            fileErr: null,
            isDocValid: true,
            productpic: true,
            ProductImg: { ...imgPayload },
            inputpayload: { ...tempInputPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      } else {
        this.setState(
          {
            fileErr: "File size should be less than 2MB!",
            isDocValid: false,
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    } else {
      this.setState(
        {
          fileErr:
            "File format is invalid! Image should be of .jpeg or .png format",
          isDocValid: false,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state.isDocValid ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    switch (fileData.type.split("/")[0]) {
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };
  render() {
    const { activePage } = this.props;
    const { isFormValid } = this.state;
    return (
      <Wrapper>
        <div className="serviceSelectionPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0 align-items-center py-5">
                <div className="col-10 mx-auto">
                  <h5 className="heading text-center">
                    Add your profile photo
                  </h5>
                  <p className="caption text-center">
                    Your profile photo will appear on your profile and directory
                    listing. Make it a good one!
                  </p>
                  <div className="serviceSelectionContent">
                    <div className="row m-0">
                      <div className="col-10 p-0 headerRow">
                        <div className="h-100 mr-3">
                          <h6 className="professionalName">Nate Cantalupo</h6>
                          <p className="professionalLocation">Clearwater, FL</p>
                        </div>
                      </div>
                    </div>
                    <div className="row m-0">
                      <div className="col-11">
                        <div className="row m-0">
                          <div className="col-4 pr-0">
                            <div className="ProfilePic_Sec">
                              <label for="cmpRegImage" className="ImageSec">
                                <div className="position-relative">
                                  {this.state.productpic &&
                                  this.state.ProductImg ? (
                                    <div className="profilePicCard">
                                      <div className="PicSec">
                                        <img
                                          src={
                                            this.state.ProductImg &&
                                            this.state.ProductImg.url
                                          }
                                        ></img>
                                        <div className="overlay">
                                          <img
                                            src={Camera_Icon}
                                            className="cameraIcon"
                                          ></img>
                                        </div>
                                      </div>
                                    </div>
                                  ) : (
                                    <div className="imageUploadSec">
                                      <div className="UploadImgSec text-center">
                                        <img
                                          src={Img_Download_White_Icon}
                                          className="uploadImg"
                                        ></img>
                                        <p className="dropMsg">
                                          Drag-n-drop here to upload your photo
                                        </p>

                                        <p className="upload_btn">
                                          Upload Photo
                                        </p>
                                      </div>
                                    </div>
                                  )}
                                </div>
                                <input
                                  id="cmpRegImage"
                                  type="file"
                                  style={{ display: "none" }}
                                  onChange={this.handleUploadFile}
                                  accept="image/png, image/jpeg"
                                />
                              </label>

                              <div className="btn_Div"></div>
                              <div className="btn_Div"></div>
                              <div className="btn_Div"></div>
                              <div className="row m-0 footerBtnSec">
                                <div className="col-6 p-0 pr-1">
                                  <div className="borderDiv"></div>
                                </div>
                                <div className="col-6 p-0 pl-1">
                                  <div className="borderDiv"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-8">
                            <div className="btn_Div"></div>
                            <div className="row m-0">
                              <div className="col-5 p-0">
                                <div className="productPostCard">
                                  <div className="imgDiv"></div>
                                  <div className="lineDiv"></div>
                                  <div className="lineDiv one"></div>
                                  <div className="lineDiv two"></div>
                                  <div className="postbtn_Div"></div>
                                </div>
                              </div>
                              <div className="col-5 pr-0">
                                <div className="productPostCard">
                                  <div className="imgDiv"></div>
                                  <div className="lineDiv"></div>
                                  <div className="lineDiv one"></div>
                                  <div className="lineDiv two"></div>
                                  <div className="postbtn_Div"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="uploadPicMsgSec">
                      <h6 className="title">What makes for great profile?</h6>
                      <div className="row m-0 my-2">
                        <div className="col-6 p-0 pr-1">
                          <img src={UploadPic_1} className="uploadPic"></img>
                        </div>
                        <div className="col-6 p-0 pl-1">
                          <img src={UploadPic_2} className="uploadPic"></img>
                        </div>
                      </div>
                      <div className="row m-0 mb-2">
                        <div className="col-1 p-0">
                          <img
                            src={Tick_Bulletpoint_Icon_Green}
                            className="tickImg"
                          ></img>
                        </div>
                        <div className="col-11 p-0">
                          <p className="msg">
                            Use a photo of you or your team - clients want to
                            see who they're hiring!
                          </p>
                        </div>
                      </div>
                      <div className="row m-0 mb-2">
                        <div className="col-1 p-0">
                          <img
                            src={Tick_Bulletpoint_Icon_Green}
                            className="tickImg"
                          ></img>
                        </div>
                        <div className="col-11 p-0">
                          <p className="msg">
                            If you don't have a photo of you or your team, you
                            can use your logo.
                          </p>
                        </div>
                      </div>
                      <div className="row m-0 mb-2">
                        <div className="col-1 p-0">
                          <img
                            src={Tick_Bulletpoint_Icon_Green}
                            className="tickImg"
                          ></img>
                        </div>
                        <div className="col-11 p-0">
                          <p className="msg">Don't forget to smile!</p>
                        </div>
                      </div>
                    </div>
                    <div className="row m-0 justify-content-center">
                      <div className="col-5 mx-aut0">
                        <div
                          className={
                            !isFormValid ? "inactive_Btn" : "continue_btn"
                          }
                        >
                          <ButtonComp
                            disabled={!isFormValid}
                            onClick={this.props.handleProPlanSelection.bind(
                              this,
                              this.state.inputpayload
                            )}
                          >
                            Continue
                          </ButtonComp>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slider-dots">
                    <div className="back_btn">
                      <ButtonComp
                        onClick={this.props.handleBackPrevScreen.bind(
                          this,
                          activePage
                        )}
                      >
                        <img
                          src={Long_Arrow_Left_LightGrey}
                          className="backIcon"
                        ></img>
                        Back
                      </ButtonComp>
                    </div>
                    <div>
                      <img
                        src={activePage == 1 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 2 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 3 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 4 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 5 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 6 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 7 ? Active_Dot : Inactive_Dot}
                      ></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .serviceSelectionPage {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .caption {
              font-family: "Open Sans" !important;
              font-size: 0.951vw;
              color: ${GREY_VARIANT_1};
              margin: 0 auto;
              max-width: 55%;
            }
            .serviceSelectionContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 1.098vw;
              margin: 1.464vw 0;
              position: relative;
            }
            .continue_btn,
            .inactive_Btn {
              margin: 0.732vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .headerRow {
              height: 8.784vw;
              background: ${Border_LightGREY_COLOR};
              border-radius: 0.146vw;
            }
            .headerRow > div {
              display: flex;
              flex-direction: column;
              justify-content: flex-end;
              align-items: center;
            }
            .professionalName {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              color: ${WHITE_COLOR};
            }
            .professionalLocation {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 1.024vw;
              color: ${WHITE_COLOR};
            }
            .ProfilePic_Sec {
              position: relative;
              top: -6.588vw;
              width: 100%;
              z-index: 0;
              border-radius: 0.146vw;
            }
            .UploadPic_Card {
              background: ${GREEN_COLOR};
              height: 13.177vw;
              border-radius: 0.146vw;
            }
            .btn_Div {
              height: 2.928vw;
              margin: 0.732vw 0;
              background: ${BG_LightGREY_COLOR};
              border-radius: 0.146vw;
            }
            .footerBtnSec {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .borderDiv {
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.146vw;
              height: 1.83vw;
              margin: 0.585vw 0;
            }

            .closeIcon {
              position: absolute;
              top: 0.366vw;
              right: -1.464vw;
              width: 0.805vw;
              cursor: pointer;
            }
            .ImageSec {
              width: 100%;
              height: auto;
              position: relative;
              cursor: pointer;
              margin: 0;
            }
            .PicSec {
              width: 100%;
              height: 100%;
              cursor: pointer;
              margin: 0;
              border-radius: 0.146vw;
              position: relative;
            }
            .PicSec img {
              width: 100%;
              height: 100%;
              object-fit: cover !important;
              object-position: top;
            }
            .overlay {
              background: rgba(255, 255, 255, 0.8);
              position: absolute;
              width: 100%;
              left: 0;
              top: 80%;
              bottom: 0;
              cursor: pointer;
              right: 0;
              opacity: 0;
              -webkit-transition: all 0.4s ease-in-out 0s;
              -moz-transition: all 0.4s ease-in-out 0s;
              transition: all 0.4s ease-in-out 0s;
              z-index: 1;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .cameraIcon {
              width: 1.464vw !important;
              height: 1.464vw !important;
              object-fit: cover;
              margin-bottom: 0.219vw;
            }
            .PicSec:hover .overlay {
              opacity: 1;
            }
            .imageUploadSec {
              display: flex;
              align-items: center;
              justify-content: center;
              background: ${GREEN_COLOR};
              border-radius: 0.146vw;
              height: 15.373vw;
            }
            .profilePicCard {
              height: 15.373vw;
              border-radius: 0.146vw;
              position: relative;
            }
            .uploadImg {
              width: 1.83vw;
              object-fit: cover;
            }
            .UploadImgSec .dropMsg {
              color: ${WHITE_COLOR};
              font-size: 0.805vw;
              padding: 0.585vw;
              margin: 0;
              font-family: "Open Sans" !important;
            }
            .upload_btn {
              padding: 0.366vw 1.098vw;
              background: ${WHITE_COLOR};
              color: ${GREEN_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              text-transform: capitalize;
              font-size: 0.951vw;
              width: 65%;
              border-radius: 0.146vw;
              margin: 0 auto;
            }
            .productPostCard {
              border: 0.0732vw solid ${BG_LightGREY_COLOR};
              box-shadow: 0px 0px 1px 0px rgba(0, 0, 0, 0.1);
            }
            .imgDiv {
              height: 16.105vw;
              background: ${BG_LightGREY_COLOR};
              margin-bottom: 0.732vw;
            }
            .lineDiv {
              height: 0.585vw;
              background: ${BG_LightGREY_COLOR};
              border-radius: 1.464vw;
              margin: 0.732vw;
            }
            .one {
              max-width: 75%;
            }
            .two {
              max-width: 50%;
            }
            .postbtn_Div {
              height: 1.098vw;
              border: 0.0732vw solid ${BG_LightGREY_COLOR};
              border-radius: 1.464vw;
              max-width: 30%;
              margin: 0.732vw;
            }
            .uploadPicMsgSec {
              border-left: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              width: 18.301vw;
              height: 87.5%;
              position: absolute;
              right: 0;
              top: 1.098vw;
              background: ${WHITE_COLOR};
              padding: 1.098vw;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              color: ${GREY_VARIANT_1};
            }
            .uploadPic {
              width: 100%;
              object-fit: cover;
            }
            .tickImg {
              width: 0.732vw;
              margin-bottom: 0.585vw;
            }
            .msg {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default UploadProfilePhotoPage;
