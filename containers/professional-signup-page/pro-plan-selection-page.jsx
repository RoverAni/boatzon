import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  THEME_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_8,
  BLACK_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR_Dark,
  Border_LightGREY_COLOR_2,
  Active_Dot,
  Inactive_Dot,
  Long_Arrow_Left_LightGrey,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import { ProPlanData } from "../../fixtures/pro-plan/pro-plan";
import ButtonComp from "../../components/button/button";
import { getFollowManufactures } from "../../services/manufacturer";

class ProPlanSelectionPage extends Component {
  state = {
    inputpayload: {},
    activeCard: 0,
  };

  componentDidMount() {
    this.props.handlePopularManufacturerList();
    let tempPayload = { ...this.props.inputpayload };
    this.handleGetFollowingManufacturers();
    this.setState({
      inputpayload: { ...tempPayload },
    });
  }

  handleGetFollowingManufacturers = () => {
    getFollowManufactures()
      .then((res) => {
        let response = res.data;
        let tempPayload = { ...this.state.inputpayload };
        let selectedManufacturePayload =
          response.data &&
          response.data.length > 0 &&
          response.data.map((data) => data.manufactureName);
        tempPayload.ManufactureNamesList = [...selectedManufacturePayload];
        this.setState({
          inputpayload: { ...tempPayload },
        });
      })
      .catch((err) => {
        console.log("err", err);
        let tempPayload = { ...this.state.inputpayload };
        let selectedManufacturePayload = [];
        tempPayload.ManufactureNamesList = [...selectedManufacturePayload];
        this.setState({
          inputpayload: { ...tempPayload },
        });
      });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: temppayload,
      });
    }
  };

  handleActiveCard = (index) => {
    this.setState({
      activeCard: index,
    });
  };

  render() {
    const CardType = [
      { value: "Debit Card", label: "Debit Card" },
      { value: "Credit Card", label: "Credit Card" },
    ];
    const { activeCard } = this.state;
    const { activePage } = this.props;
    return (
      <Wrapper>
        <div className="proPlanSelectionPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0 align-items-center py-5">
                <div className="col-10 mx-auto">
                  <h5 className="heading text-center">Boatzon Pro Plan</h5>
                  <div className="proPlanSelectionContent">
                    <div className="row m-0">
                      <div className="col-6 px-4 contentForm">
                        <div>
                          <h6 className="title">Payment information</h6>
                          <div className="SelectInput">
                            <SelectInput
                              stateType={true}
                              placeholder="New debit/credit card"
                              options={CardType}
                              onChange={this.handleOnSelectInput("cardType")}
                            />
                          </div>
                          <p className="paymentMsg">
                            You are enrolled in a 12-month free trial, so no
                            payments or credit card is needed. During these 12
                            months, we plan to bring you new customers and great
                            service.
                          </p>
                        </div>
                      </div>
                      <div className="col-6 pr-0">
                        <h6 className="title">
                          Essential Boatzon Pro Services
                        </h6>
                        <ul className="list-unstyled CardList my-3">
                          {ProPlanData &&
                            ProPlanData.map((data, index) => (
                              <li key={index}>
                                <div
                                  className={
                                    activeCard == index
                                      ? "planCard text-center activeCard"
                                      : "planCard text-center"
                                  }
                                  onClick={this.handleActiveCard.bind(
                                    this,
                                    index
                                  )}
                                >
                                  <h6
                                    className={
                                      activeCard == index
                                        ? "cardHeading activeCardfontColor"
                                        : "cardHeading"
                                    }
                                  >
                                    {data.plan_name}
                                  </h6>
                                  <p
                                    className={
                                      activeCard == index
                                        ? "planFor activeCardfontColor"
                                        : "planFor"
                                    }
                                  >
                                    {data.number_of_boats}
                                  </p>
                                  <h2
                                    className={
                                      activeCard == index
                                        ? "planCost activeCardfontColor"
                                        : "planCost"
                                    }
                                  >
                                    Free
                                  </h2>
                                  <p
                                    className={
                                      activeCard == index
                                        ? "line activeLine"
                                        : "line"
                                    }
                                  ></p>
                                  <p
                                    className={
                                      activeCard == index
                                        ? "trialPeriod activeCardfontColor"
                                        : "trialPeriod"
                                    }
                                  >
                                    1 year trial
                                  </p>
                                  <p
                                    className={
                                      activeCard == index
                                        ? "monthlyCharges activeCardfontColor"
                                        : "monthlyCharges"
                                    }
                                  >
                                    then {data.monthly_price}
                                  </p>
                                </div>
                              </li>
                            ))}
                        </ul>
                        <table className="my-2 PaySlip">
                          <tr>
                            <td className="label">Subtotal</td>
                            <td className="value">$0.00</td>
                          </tr>
                          <tr>
                            <td className="label">Estimated Tax</td>
                            <td className="value">$0.00</td>
                          </tr>
                          <tr>
                            <td className="label">Discount</td>
                            <td className="value">$0.00</td>
                          </tr>
                          <tr>
                            <td className="label">Order Total</td>
                            <td className="value">$0.00</td>
                          </tr>
                          <tr>
                            <td className="totalLabel">Total Due Today</td>
                            <td className="totalValue">$0.00</td>
                          </tr>
                        </table>
                        <div className="continue_btn">
                          <ButtonComp
                            onClick={this.props.handleFollowManufacturer.bind(
                              this,
                              this.state.inputpayload
                            )}
                          >
                            Continue
                          </ButtonComp>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slider-dots">
                    <div className="back_btn">
                      <ButtonComp
                        onClick={this.props.handleBackPrevScreen.bind(
                          this,
                          activePage
                        )}
                      >
                        <img
                          src={Long_Arrow_Left_LightGrey}
                          className="backIcon"
                        ></img>
                        Back
                      </ButtonComp>
                    </div>
                    <div>
                      <img
                        src={activePage == 1 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 2 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 3 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 4 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 5 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 6 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 7 ? Active_Dot : Inactive_Dot}
                      ></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .proPlanSelectionPage {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .proPlanSelectionContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 1.098vw;
              margin: 1.464vw 0;
              position: relative;
            }
            .continue_btn {
              margin: 0.732vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 1.464vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.continue_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .SelectInput {
              margin: 1.098vw 0;
            }
            .paymentMsg {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              text-align: center;
            }
            .planCard {
              background: ${WHITE_COLOR};
              padding: 1.83vw 0.732vw;
              //   box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.219vw;
              width: 11.639vw;
              height: auto;
              position: relative;
              cursor: pointer;
            }
            .activeCard {
              background: ${GREEN_COLOR} !important;
            }
            .cardHeading {
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_8};
              font-weight: 600;
              padding: 0.585vw 0 0 0;
              margin-bottom: 0.439vw;
            }
            .activeCardfontColor {
              color: ${WHITE_COLOR} !important;
            }
            .planFor {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              height: 2.489vw;
            }
            .planCost {
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_8};
              font-weight: 600;
              margin: 0 0 0.366vw 0;
              font-size: 1.756vw;
            }
            .line {
              border-bottom: 0.0732vw solid ${BLACK_COLOR};
              width: 55px;
              margin: 0 auto 0.878vw auto;
            }
            .activeLine {
              border-bottom: 0.0732vw solid ${WHITE_COLOR};
            }
            .trialPeriod,
            .monthlyCharges {
              font-size: 0.805vw;
              margin: 0;
              font-family: "Open Sans-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .CardList {
              display: flex;
              align-items: center;
            }
            .CardList > :nth-child(1) {
              margin-right: 1.098vw !important;
            }
            .contentForm {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }

            .PaySlip {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              padding: 1.098vw 0;
              width: 100%;
            }
            .PaySlip > :first-child td {
              padding: 0.951vw 0 0 0;
            }
            .PaySlip > :nth-last-child(2) td {
              padding: 0.512vw 0 0.951vw 0;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .label,
            .value {
              font-size: 0.878vw;
              margin: 0;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              padding: 0.512vw 0 0 0;
            }
            .totalLabel,
            .totalValue {
              font-size: 0.878vw;
              margin: 0;
              font-family: "Open Sans-Semibold" !important;
              color: ${THEME_COLOR};
              padding: 0.732vw 0;
            }
            .value,
            .totalValue {
              text-align: right;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProPlanSelectionPage;
