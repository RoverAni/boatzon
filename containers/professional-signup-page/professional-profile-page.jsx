import React, { Component } from "react";
import { connect } from "react-redux";

import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  BLACK_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
  Border_LightGREY_COLOR_2,
  Business_Profile_Icon,
  THEME_COLOR_Opacity,
  Phone_Outline_Icon_Grey,
  GREEN_COLOR,
  Profile_Img1,
  Profile_Img2,
  Profile_Img3,
  GREY_VARIANT_1,
  Active_Dot,
  Inactive_Dot,
  Verified_Icon,
  NotVerified_Icon,
  Red_Color,
  Border_LightGREY_COLOR_1,
  Google_Signup_Type,
  Facebook_Signup_Type,
  Noraml_Signup_Type,
  Web_deviceType,
} from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";
import InputWithIcon from "../../components/input-box/input-with-icon";
import SelectInput from "../../components/input-box/select-input";
import IntlTelInput from "react-intl-tel-input";
import {
  MOBILE,
  COUNTRY_CODE,
} from "../../lib/input-control-data/input-definitions";
import ButtonComp from "../../components/button/button";
import ProfessionalBusinessAddressPage from "./professional-business-address-page";
import ProfessionalBusinessDetailsPage from "./professional-business-details-page";
import FollowManufacturersPage from "./follow-manufacturers-page";
import ServiceSelectionPage from "./service-selection-page";
import UploadProfilePhotoPage from "./upload-profile-photo-page";
import ProPlanSelectionPage from "./pro-plan-selection-page";
import SuccessfullModel from "./successfull-model";
import {
  requiredValidator,
  validatePhoneNumber,
} from "../../lib/validation/validation";
import { proUserNameVerfication, proRegister } from "../../services/pro-auth";
import { PhoneNumberVerfication, registerUser } from "../../services/auth";
import VerifyOtp from "../auth-modals/sign-up/verify-otp";
import Model from "../../components/model/model";
import { verifyOTP, requestForOTP } from "../../services/verify-otp";
import { getDeviceId } from "../../lib/fingerprint/fingerprint";
import Snackbar from "../../components/snackbar";
import { getProfessionals } from "../../services/professionals";
import cloneDeep from "lodash.clonedeep";
import {
  getManufactures,
  postfollowManufactures,
} from "../../services/manufacturer";
import {
  setupBusinessName,
  setupBusinessUserId,
  setupToken,
  setupUserId,
  setupMongoId,
  setupMqttId,
} from "../../lib/data-handler/data-handler";
import { getCountry } from "country-state-picker";
import { __BlobUpload } from "../../lib/image/cloudinaryUpload";
import { setReduxState } from "../../redux/actions/auth";
import { isEmpty } from "../../lib/global";

class ProfessionalProfilePage extends Component {
  state = {
    inputpayload: {
      preferredCountries: ["af"],
    },
    activePage: 1,
    isFormValid: false,
    phoneNumVerified: false,
    verifyOtpModel: false,
    professionalType: {},
    preferredCountries: ["af"],
    load: false,
    hasMore: true,
    popularManufacturerList: [],
  };

  componentDidMount() {
    if (this.props.AuthPass) {
      let countyName = getCountry(`${this.props.userProfileData.countryCode}`)
        .code;
      console.log("countyName", countyName);
      let tempPreferredCountries = [...this.state.preferredCountries];
      tempPreferredCountries[0] = countyName;
      console.log("countyName", tempPreferredCountries);
      this.setState(
        {
          mobile: this.props.userProfileData.phoneNumber.replace(
            this.props.userProfileData.countryCode,
            ""
          ),
          preferredCountries: [...tempPreferredCountries],
        },
        () => {
          let tempPayload = { ...this.props.inputpayload };
          tempPayload.mobile = this.state.mobile;
          tempPayload[`preferredCountries`] = this.state.preferredCountries;
          console.log("countyName", tempPayload);

          this.setState(
            {
              inputpayload: { ...tempPayload },
              load: true,
            },
            () => {
              console.log("countyName", this.state.inputpayload);
              this.checkIfFormValid();
            }
          );
        }
      );
    } else {
      this.setState(
        {
          preferredCountries: [
            `${
              this.props.currentLocation &&
              this.props.currentLocation.countryShortName
            }`,
          ],
        },
        () => {
          let tempPayload = { ...this.props.inputpayload };
          tempPayload[`preferredCountries`] = this.state.preferredCountries;
          this.setState(
            {
              inputpayload: { ...tempPayload },
              load: true,
            },
            () => {
              this.checkIfFormValid();
            }
          );
        }
      );
    }
    this.validatePhoneInput();
  }

  // Number Validation function for phoneInput
  validatePhoneInput = () => {
    validatePhoneNumber("phoneNum");
  };

  // Function for changing screen
  updateScreen = (screen) =>
    this.setState({
      currentScreen: screen,
    });

  handleBackPrevScreen = (activePage) => {
    switch (activePage) {
      case 2:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen();
          }
        );
        break;
      case 3:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen(
              <ProfessionalBusinessAddressPage
                activePage={this.state.activePage}
                handleBusinessDetails={this.handleBusinessDetails}
                handleBackPrevScreen={this.handleBackPrevScreen}
                inputpayload={this.state.inputpayload}
                currentLocation={this.props.currentLocation}
              />
            );
          }
        );
        break;
      case 4:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen(
              <ProfessionalBusinessDetailsPage
                handleServiceSelection={this.handleServiceSelection}
                activePage={this.state.activePage}
                handleBackPrevScreen={this.handleBackPrevScreen}
                inputpayload={this.state.inputpayload}
              />
            );
          }
        );
        break;
      case 5:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen(
              <ServiceSelectionPage
                handleUploadProfilePhoto={this.handleUploadProfilePhoto}
                activePage={this.state.activePage}
                handleBackPrevScreen={this.handleBackPrevScreen}
                inputpayload={this.state.inputpayload}
                specialityList={this.state.specialityList}
              />
            );
          }
        );
        break;
      case 6:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen(
              <UploadProfilePhotoPage
                handleProPlanSelection={this.handleProPlanSelection}
                activePage={this.state.activePage}
                handleBackPrevScreen={this.handleBackPrevScreen}
                inputpayload={this.state.inputpayload}
              />
            );
          }
        );
        break;
      case 7:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen(
              <ProPlanSelectionPage
                handleFollowManufacturer={this.handleFollowManufacturer}
                activePage={this.state.activePage}
                handleBackPrevScreen={this.handleBackPrevScreen}
                inputpayload={this.state.inputpayload}
                handlePopularManufacturerList={
                  this.handlePopularManufacturerList
                }
              />
            );
          }
        );
        break;
    }
  };

  handleBusinessAddress = () => {
    this.setState(
      {
        activePage: this.state.activePage + 1,
      },
      () => {
        this.updateScreen(
          <ProfessionalBusinessAddressPage
            activePage={this.state.activePage}
            handleBusinessDetails={this.handleBusinessDetails}
            handleBackPrevScreen={this.handleBackPrevScreen}
            inputpayload={this.state.inputpayload}
            currentLocation={this.props.currentLocation}
          />
        );
      }
    );
  };

  handleBusinessDetails = (payload) => {
    let tempPayload = { ...this.state.inputpayload };
    this.handleGetSpecialityList();
    this.setState(
      {
        activePage: this.state.activePage + 1,
        inputpayload: { ...tempPayload, ...payload },
      },
      () => {
        this.updateScreen(
          <ProfessionalBusinessDetailsPage
            handleServiceSelection={this.handleServiceSelection}
            activePage={this.state.activePage}
            handleBackPrevScreen={this.handleBackPrevScreen}
            inputpayload={this.state.inputpayload}
          />
        );
      }
    );
  };

  handleServiceSelection = (payload) => {
    let tempPayload = { ...this.state.inputpayload };
    this.setState(
      {
        activePage: this.state.activePage + 1,
        inputpayload: { ...tempPayload, ...payload },
      },
      () => {
        if (this.state.specialityList) {
          this.updateScreen(
            <ServiceSelectionPage
              handleUploadProfilePhoto={this.handleUploadProfilePhoto}
              activePage={this.state.activePage}
              handleBackPrevScreen={this.handleBackPrevScreen}
              inputpayload={this.state.inputpayload}
              specialityList={this.state.specialityList}
            />
          );
        }
      }
    );
  };

  handleGetSpecialityList = () => {
    getProfessionals()
      .then((res) => {
        console.log("res", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              specialityList: response.data,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleUploadProfilePhoto = (payload) => {
    let tempPayload = { ...this.state.inputpayload };
    let Payload = cloneDeep(payload).__wrapped__;
    this.setState(
      {
        activePage: this.state.activePage + 1,
        inputpayload: { ...tempPayload, ...Payload },
      },
      () => {
        this.updateScreen(
          <UploadProfilePhotoPage
            handleProPlanSelection={this.handleProPlanSelection}
            activePage={this.state.activePage}
            handleBackPrevScreen={this.handleBackPrevScreen}
            inputpayload={this.state.inputpayload}
          />
        );
      }
    );
  };

  handleProPlanSelection = (payload) => {
    let tempPayload = cloneDeep(this.state.inputpayload).__wrapped__;

    this.setState(
      {
        activePage: this.state.activePage + 1,
        inputpayload: { ...tempPayload, ...payload },
      },
      () => {
        this.updateScreen(
          <ProPlanSelectionPage
            handleFollowManufacturer={this.handleFollowManufacturer}
            activePage={this.state.activePage}
            handleBackPrevScreen={this.handleBackPrevScreen}
            inputpayload={this.state.inputpayload}
            handlePopularManufacturerList={this.handlePopularManufacturerList}
          />
        );
      }
    );
  };

  handlePopularManufacturerList = (
    limit = 10,
    page = 0,
    search,
    paginationLoader = false,
    searchLoader = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        if (this.state.hasMore || search) {
          let payload = {
            type: "4", //0-all manufactureres,1-boats and products,2-engines,3-trailers,4-popular manufactures
            limit: limit,
            offset: page * limit,
            searchKey: search,
          };
          this.setState(
            {
              paginationLoader,
              searchLoader,
            },
            () => {
              this.props.dispatch(
                setReduxState("paginationLoader", this.state.paginationLoader)
              );
              this.props.dispatch(
                setReduxState("searchLoader", this.state.searchLoader)
              );
            }
          );
          let result = await getManufactures(payload);
          let response = result.data;
          console.log("fkfok--res", response, search);
          let oldList = [...this.state.popularManufacturerList];
          let newList = response && response.data;
          if (response.code == 200) {
            this.setState(
              {
                popularManufacturerList:
                  search != undefined ? [...newList] : [...oldList, ...newList],
                paginationLoader: false,
                searchLoader: false,
                hasMore:
                  this.state.popularManufacturerList.length < response.count,
              },
              () => {
                this.props.dispatch(
                  setReduxState(
                    "popularManufacturerList",
                    this.state.popularManufacturerList
                  )
                );
                this.props.dispatch(
                  setReduxState("searchLoader", this.state.searchLoader)
                );
                this.props.dispatch(
                  setReduxState("paginationLoader", this.state.paginationLoader)
                );
              }
            );
          }
          if (response.code == 204) {
            this.setState(
              {
                paginationLoader: false,
                popularManufacturerList:
                  search != undefined ? [...newList] : [...oldList],
                searchLoader: false,
              },
              () => {
                this.props.dispatch(
                  setReduxState(
                    "popularManufacturerList",
                    this.state.popularManufacturerList
                  )
                );
                this.props.dispatch(
                  setReduxState("searchLoader", this.state.searchLoader)
                );
                this.props.dispatch(
                  setReduxState("paginationLoader", this.state.paginationLoader)
                );
              }
            );
            return rej();
          }
          res();
        }
      } catch (err) {
        console.log("fkfok--err", err);
        this.setState(
          {
            popularManufacturerList: [],
            paginationLoader: false,
            searchLoader: false,
          },
          () => {
            this.props.dispatch(
              setReduxState(
                "popularManufacturerList",
                this.state.popularManufacturerList
              )
            );
            this.props.dispatch(
              setReduxState("searchLoader", this.state.searchLoader)
            );
            this.props.dispatch(
              setReduxState("paginationLoader", this.state.paginationLoader)
            );
          }
        );
      }
    });
  };

  handleFollowManufacturer = (payload) => {
    let tempPayload = cloneDeep(this.state.inputpayload).__wrapped__;
    console.log("dndu--1", payload);
    this.setState(
      {
        activePage: this.state.activePage + 1,
        inputpayload: { ...tempPayload, ...payload },
      },
      () => {
        console.log("dndu--2", this.state.inputpayload);
        this.updateScreen(
          <FollowManufacturersPage
            handleSuccessfullSignup={this.handleSuccessfullSignup}
            activePage={this.state.activePage}
            handleBackPrevScreen={this.handleBackPrevScreen}
            inputpayload={this.state.inputpayload}
            popularManufacturerList={this.state.popularManufacturerList}
            handlePopularManufacturerList={this.handlePopularManufacturerList}
          />
        );
      }
    );
  };

  handleSuccessfullSignup = (payload) => {
    let tempPayload = cloneDeep(this.state.inputpayload).__wrapped__;
    this.setState(
      {
        inputpayload: { ...tempPayload, ...payload },
      },
      () => {
        console.log("inputpayload", this.state.inputpayload);
        this.handleUploadPic(this.state.inputpayload);
      }
    );
  };

  // used to upload image from local storage
  handleUploadPic = (finalpayload) => {
    this.setState(
      {
        signuploading: true,
      },
      () => {
        if (finalpayload.productImgs && !isEmpty(finalpayload.productImgs)) {
          __BlobUpload(finalpayload.productImgs.fileDetails)
            .then((res) => {
              console.log("feduf-->image upload success", res);
              this.setState(
                {
                  profilePicUrl: res.body.secure_url,
                },
                () => {
                  this.props.AuthPass
                    ? this.handleProRegisterAPI()
                    : this.handleUserRegisterAPI(this.state.inputpayload);
                }
              );
            })
            .catch((err) => {
              console.log("feduf-->err uploading image", err);
              this.setState({
                usermessage: "Err uploading image",
                variant: "error",
                open: true,
                vertical: "bottom",
                horizontal: "left",
              });
            });
        } else {
          this.props.AuthPass
            ? this.handleProRegisterAPI()
            : this.handleUserRegisterAPI(this.state.inputpayload);
        }
      }
    );
  };

  handleUserRegisterAPI = (finalpayload) => {
    console.log("nksf", finalpayload);
    const { profilePicUrl } = this.state;
    let apiPayload;
    if (finalpayload.signupType == Google_Signup_Type) {
      apiPayload = {
        signupType: finalpayload.signupType,
        deviceType: finalpayload.deviceType,
        googleId: finalpayload.googleId,
        email: finalpayload.Email,
        fullName: finalpayload.FullName,
        profilePicUrl: profilePicUrl
          ? profilePicUrl
          : finalpayload.profilePicUrl,
        accessToken: finalpayload.accessToken,
        googleToken: finalpayload.tokenId,
        password: finalpayload.Password,
        username: finalpayload.username,
        phoneNumber: finalpayload.countryCode + finalpayload.mobile,
        countryCode: finalpayload.countryCode,
        deviceId: getDeviceId(),

        longitude: this.state.inputpayload.logitude,
        latitude: this.state.inputpayload.latitude,
        city: this.state.inputpayload.city,
        countrySname: this.state.inputpayload.countryShortName,
        location: this.state.inputpayload.address_line,
      };
      delete apiPayload.mobile;
      delete apiPayload.ManufactureNamesList;
      delete apiPayload.Password;
    } else if (finalpayload.signupType == Facebook_Signup_Type) {
      apiPayload = {
        email: finalpayload.Email,
        fullName: finalpayload.FullName,
        signupType: finalpayload.signupType,
        deviceType: finalpayload.deviceType,
        profilePicUrl: profilePicUrl
          ? profilePicUrl
          : finalpayload.profilePicUrl
          ? finalpayload.profilePicUrl
          : "",
        facebookId: finalpayload.facebookId,
        accessToken: finalpayload.accessToken,
        password: finalpayload.Password,
        username: finalpayload.username,
        phoneNumber: finalpayload.countryCode + finalpayload.mobile,
        countryCode: finalpayload.countryCode,
        deviceId: getDeviceId(),
        longitude: this.state.inputpayload.logitude,
        latitude: this.state.inputpayload.latitude,
        city: this.state.inputpayload.city,
        countrySname: this.state.inputpayload.countryShortName,
        location: this.state.inputpayload.address_line,
      };
      delete apiPayload.ManufactureNamesList;
      delete apiPayload.Password;
    } else {
      apiPayload = {
        signupType: Noraml_Signup_Type,
        deviceType: Web_deviceType,
        username: finalpayload.username,
        password: finalpayload.Password,
        phoneNumber: finalpayload.countryCode + finalpayload.mobile,
        countryCode: finalpayload.countryCode,
        fullName: finalpayload.FullName,
        email: finalpayload.Email,
        deviceId: getDeviceId(),
        longitude: this.state.inputpayload.logitude,
        latitude: this.state.inputpayload.latitude,
        city: this.state.inputpayload.city,
        countrySname: this.state.inputpayload.countryShortName,
        location: this.state.inputpayload.address_line,
        profilePicUrl: profilePicUrl,
      };
    }
    registerUser(apiPayload)
      .then((res) => {
        console.log("res", res.data.response);
        let response = res.data.response;

        if (response) {
          this.setState({
            usermessage: "Registation Successfull",
            variant: "success",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          setupToken(response.authToken);
          setupUserId(response.userId);
          setupMongoId(response.mongoId);
          setupMqttId(response.mqttId);
          this.handleProRegisterAPI();
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: "Something Went Wrong",
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleProRegisterAPI = () => {
    let finalPayload = {
      email: this.state.inputpayload.Email,
      username: this.state.inputpayload.username,
      businessName: this.state.inputpayload.businessName,
      bussinessStatus:
        this.state.inputpayload.professionalType == "Professional" ? "2" : "1",
      websiteLink: this.state.inputpayload.Website,
      lat: this.state.inputpayload.latitude,
      long: this.state.inputpayload.logitude,
      address: this.state.inputpayload.address_line,
      country: this.state.inputpayload.country,
      state: this.state.inputpayload.stateName,
      city: this.state.inputpayload.city,
      zipcode: this.state.inputpayload.zipCode,
      aboutMe: this.state.inputpayload.about_proUSer,
      award: this.state.inputpayload.certificate_awards,
      businessLogo: this.state.inputpayload.businessLogo,
      speciality:
        this.state.inputpayload.speciality &&
        this.state.inputpayload.speciality
          .map((data) => {
            return data.name;
          })
          .toString(),
      subSpeciality:
        this.state.inputpayload.subSpeciality &&
        this.state.inputpayload.subSpeciality
          .map((data) => {
            return data.name;
          })
          .toString(),
      phonenumber: this.props.AuthPass
        ? this.props.userProfileData.phoneNumber
        : this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };

    console.log("finalPayload", finalPayload);

    proRegister(finalPayload)
      .then((res) => {
        console.log("res", res.data);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: this.handleResponseMsg(response),
            variant: "success",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            setupBusinessName(response.businessName);
            setupBusinessUserId(response.bussinessNodeId);
            this.updateScreen(<SuccessfullModel />);
            if (this.state.inputpayload.ManufactureNamesList.length > 0) {
              let manufactureIds = this.state.popularManufacturerList
                .filter((e) =>
                  this.state.inputpayload.ManufactureNamesList.includes(
                    e.manufactureName
                  )
                )
                .map((obj) => obj.manufactureId);
              let payload = {
                manufacture: manufactureIds.toString(),
              };
              postfollowManufactures(payload)
                .then((result) => {
                  console.log("result", result);
                  this.setState({
                    usermessage: result.data.message,
                    variant: this.handleSnackbar(result.data),
                    open: true,
                    vertical: "bottom",
                    horizontal: "left",
                  });
                })
                .catch((error) => {
                  console.log("error", error);
                  this.setState({
                    usermessage: error.message,
                    variant: "error",
                    open: true,
                    vertical: "bottom",
                    horizontal: "left",
                  });
                });
            }
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleResponseMsg = (response) => {
    switch (response.code) {
      case 200:
        return "Registation Successfull";
        break;
      default:
        return response.message;
        break;
    }
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    if (event.target.name == "businessName") {
      this.setState({
        prousernameChecked: false,
        prousernameErrMsg: "",
        prousernameErrMsgClr: "",
        prousernameValid: false,
      });
    }
    console.log("fejf-->", event.target.value, event.target.name);
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    console.log("fejf", inputControl);
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    console.log("fejf", tempsignUpPayload);
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
      },
      () => {
        // this.props.dispatch(setSignupData(this.state.inputpayload));
        console.log("inputpayload", this.state.inputpayload);
      }
    );
  };

  handleErrMsgColor = (response) => {
    switch (response.code) {
      case 200:
        return `${GREEN_COLOR}`;
        break;
      case 409:
        return `${Red_Color}`;
        break;
    }
  };

  handleProUserNameCheck = () => {
    if (
      this.state.inputpayload.username &&
      this.state.inputpayload.businessName
    ) {
      let userNamepayload = {
        username: this.state.inputpayload.username,
        businessName: this.state.inputpayload.businessName,
      };
      proUserNameVerfication(userNamepayload)
        .then((res) => {
          console.log("resfedfj--->", res);
          let response = res.data;
          if (response) {
            this.setState(
              {
                prousernameChecked: true,
                prousernameErrMsg: response.message,
                prousernameErrMsgClr: this.handleErrMsgColor(response),
                prousernameValid: response.code == 200 ? true : false,
              },
              () => {
                let tempPayload = { ...this.state.inputpayload };
                tempPayload[`prousernameValid`] = this.state.prousernameValid;
                this.setState(
                  {
                    inputpayload: { ...tempPayload },
                  },
                  () => {
                    this.checkIfFormValid();
                  }
                );
              }
            );
          }
        })
        .catch((err) => {
          console.log("resfedfj", err, err.message);
          this.setState(
            {
              prousernameErrMsg: err.message,
              prousernameErrMsgClr: `${Red_Color}`,
              prousernameValid: false,
            },
            () => {
              let tempPayload = { ...this.state.inputpayload };
              tempPayload[`prousernameValid`] = this.state.prousernameValid;
              this.setState(
                {
                  inputpayload: { ...tempPayload },
                },
                () => {
                  this.checkIfFormValid();
                }
              );
            }
          );
        });
    }
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl.value;
      this.setState(
        {
          inputpayload: { ...temppayload },
          [name]: {
            value: inputControl.value,
            label: inputControl.value,
          },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    console.log("onSelectFlag", num, country.dialCode, fullNum, status);
    this.handleOnchangePhone(status, num, country);
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log("dialInfo", dialInfo);
    console.log("countyName", this.state.inputpayload, dialInfo);
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
      },
      () => {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`isPhoneValid`] = this.state.isPhoneValid;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        tempPayload[`preferredCountries`] = this.state.preferredCountries;
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
        if (this.props.AuthPass) {
          this.setState(
            {
              mobileChecked: true,
              mobileValid: true,
            },
            () => {
              let tempPayload = { ...this.state.inputpayload };
              tempPayload[`mobileValid`] = this.state.mobileValid;
              this.setState(
                {
                  inputpayload: { ...tempPayload },
                },
                () => {
                  this.checkIfFormValid();
                }
              );
            }
          );
        } else {
          if (this.state.isPhoneValid) {
            this.handlePhoneNumberCheck();
          }
        }
        if (!this.state.isPhoneValid) {
          this.setState(
            {
              mobileChecked: false,
              mobileValid: false,
            },
            () => {
              let tempPayload = { ...this.state.inputpayload };
              tempPayload[`mobileValid`] = this.state.mobileValid;
              this.setState(
                {
                  inputpayload: { ...tempPayload },
                },
                () => {
                  this.checkIfFormValid();
                }
              );
            }
          );
        }
      }
    );
  };

  handlePhoneNumberCheck = () => {
    if (this.state.isPhoneValid) {
      let phoneNumberpayload = {
        phoneNumber:
          this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
      };
      PhoneNumberVerfication(phoneNumberpayload)
        .then((res) => {
          let response = res.data;
          if (response) {
            this.setState(
              {
                mobileChecked: true,
                mobileErrMsg: response.message,
                mobileErrMsgClr: this.handleErrMsgColor(response),
                mobileValid: response.code == 200 ? true : false,
              },
              () => {
                let tempPayload = { ...this.state.inputpayload };
                tempPayload[`mobileValid`] = this.state.mobileValid;
                this.setState(
                  {
                    inputpayload: { ...tempPayload },
                  },
                  () => {
                    this.checkIfFormValid();
                  }
                );
              }
            );
          }
        })
        .catch((err) => {
          console.log("err", err, err.message);
          this.setState(
            {
              mobileErrMsg: err.message,
              mobileErrMsgClr: `${Red_Color}`,
              mobileValid: false,
            },
            () => {
              let tempPayload = { ...this.state.inputpayload };
              tempPayload[`mobileValid`] = this.state.mobileValid;
              this.setState(
                {
                  inputpayload: { ...tempPayload },
                },
                () => {
                  this.checkIfFormValid();
                }
              );
            }
          );
        });
    }
  };

  handleVerifyNumberModel = () => {
    let payload = {
      deviceId: getDeviceId(),
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };
    requestForOTP(payload)
      .then((res) => {
        console.log("res", res);
      })
      .catch((err) => {
        console.log("err", err);
      });
    this.setState({
      verifyOtpModel: !this.state.verifyOtpModel,
    });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleVerifyOtp = (otp) => {
    let payload = {
      deviceId: getDeviceId(),
      otp: otp,
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };

    verifyOTP(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            // usermessage: response.message,
            // variant: this.handleSnackbar(response),
            usermessage: "otp verified!",
            variant: "success",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.setState({
              phoneNumVerified: true,
              verifyOtpModel: !this.state.verifyOtpModel,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.inputpayload.businessName &&
      this.state.inputpayload.prousernameValid &&
      this.state.professionalType.value &&
      (!this.props.AuthPass
        ? this.state.inputpayload.isPhoneValid &&
          this.state.inputpayload.mobileValid
        : true)
        ? true
        : false;

    this.setState({ isFormValid: isFormValid });
  };

  render() {
    const ProfessionalType = [
      { value: "Professional", label: "Professional" },
      { value: "Boat Dealer", label: "Boat Dealer" },
    ];

    const {
      activePage,
      isFormValid,
      phoneNumVerified,
      professionalType,
    } = this.state;
    const { businessName } = { ...this.state.inputpayload };
    return (
      <Wrapper>
        {!this.state.currentScreen ? (
          <Wrapper>
            <div className="proProfilePage">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div className="row m-0 align-items-center py-5">
                    <div className="col-10 mx-auto">
                      <h5 className="heading text-center">
                        Let’s get started by competing your profile
                      </h5>
                      <div className="profileContent">
                        <div className="row m-0">
                          <div className="col-6 contentForm px-4">
                            <div>
                              <h6 className="title">General Information</h6>
                              <div className="FormInput">
                                <div className="position-relative">
                                  <InputWithIcon
                                    autoFocus
                                    type="text"
                                    className="SignUpInputBox form-control"
                                    name="businessName"
                                    inputAdornment={
                                      <img
                                        src={Business_Profile_Icon}
                                        className="profileIcon"
                                      ></img>
                                    }
                                    value={businessName}
                                    placeholder="Business Name"
                                    onBlur={this.handleProUserNameCheck}
                                    onChange={this.handleOnchangeInput}
                                    autoComplete="off"
                                  />
                                  {this.state.prousernameChecked ? (
                                    this.state.prousernameValid &&
                                    this.state.businessName == 1 ? (
                                      <img
                                        src={Verified_Icon}
                                        className="verifiedIcon"
                                      ></img>
                                    ) : (
                                      <img
                                        src={NotVerified_Icon}
                                        className="notVerifiedIcon"
                                      ></img>
                                    )
                                  ) : (
                                    ""
                                  )}
                                </div>
                                {this.state.prousernameChecked ? (
                                  <p
                                    className="errMessage"
                                    style={{
                                      color: this.state.prousernameErrMsgClr,
                                    }}
                                  >
                                    {this.state.prousernameErrMsg}
                                  </p>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="SelectInput">
                                <SelectInput
                                  professionalType={true}
                                  placeholder="Professional Type"
                                  options={ProfessionalType}
                                  value={professionalType}
                                  onChange={this.handleOnSelectInput(
                                    `professionalType`
                                  )}
                                />
                              </div>
                              <div className="PhoneInput">
                                <div className="position-relative">
                                  <IntlTelInput
                                    key={this.state.load}
                                    preferredCountries={
                                      this.state.inputpayload.preferredCountries
                                    }
                                    containerClassName="intl-tel-input"
                                    defaultValue={
                                      this.state.inputpayload.mobile
                                    }
                                    onSelectFlag={this.handleOnchangeflag}
                                    onPhoneNumberChange={
                                      this.handleOnchangePhone
                                    }
                                    formatOnInit={false}
                                    separateDialCode={true}
                                    fieldId="phoneNum"
                                    autoComplete="off"
                                    disabled={this.props.AuthPass}
                                  />
                                  {this.state.mobileChecked ? (
                                    this.state.mobileValid &&
                                    this.state.isPhoneValid ? (
                                      <img
                                        src={Verified_Icon}
                                        className="verifiedIcon"
                                      ></img>
                                    ) : (
                                      <img
                                        src={NotVerified_Icon}
                                        className="notVerifiedIcon"
                                      ></img>
                                    )
                                  ) : (
                                    ""
                                  )}
                                </div>
                                {this.state.isPhoneValid === false ? (
                                  <p
                                    className="errMessage"
                                    style={{ color: `${Red_Color}` }}
                                  >
                                    Phone Number not valid
                                  </p>
                                ) : this.state.mobileChecked ? (
                                  <p
                                    className="errMessage"
                                    style={{
                                      color: this.state.mobileErrMsgClr,
                                    }}
                                  >
                                    {this.state.mobileErrMsg}
                                  </p>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div
                                className={
                                  !isFormValid ? "inactive_Btn" : "continue_btn"
                                }
                              >
                                <ButtonComp
                                  disabled={!isFormValid}
                                  onClick={
                                    this.props.AuthPass || phoneNumVerified
                                      ? this.handleBusinessAddress
                                      : this.handleVerifyNumberModel
                                  }
                                >
                                  Continue
                                </ButtonComp>
                              </div>
                            </div>
                          </div>
                          <div className="col-6 px-4">
                            <div>
                              <h6 className="profileTitle">
                                Complete your pro directory profile
                              </h6>
                              <div className="row mx-0 align-items-center my-2">
                                <div className="col-2 p-0">
                                  <img
                                    src={Profile_Img1}
                                    className="featureImg"
                                  ></img>
                                </div>
                                <div className="col-10 pr-0">
                                  <p className="featureDesc m-0">
                                    Your pro directory profile is how potential
                                    clients find you on boatzon.
                                  </p>
                                </div>
                              </div>
                              <div className="row mx-0 align-items-center my-2 mt-3">
                                <div className="col-2 p-0">
                                  <img
                                    src={Profile_Img2}
                                    className="featureImg"
                                  ></img>
                                </div>
                                <div className="col-10 pr-0">
                                  <p className="featureDesc m-0">
                                    Include your business name as it will appear
                                    in the boatzon pro directory search results.
                                  </p>
                                </div>
                              </div>
                              <div className="row mx-0 align-items-center my-2 mt-3">
                                <div className="col-2 p-0">
                                  <img
                                    src={Profile_Img3}
                                    className="featureImg"
                                  ></img>
                                </div>
                                <div className="col-10 pr-0">
                                  <p className="featureDesc m-0">
                                    Providу your phone number for clients to
                                    contact you from your pro directory profile.
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="slider-dots">
                        <img
                          src={activePage == 1 ? Active_Dot : Inactive_Dot}
                        ></img>
                        <img
                          src={activePage == 2 ? Active_Dot : Inactive_Dot}
                          className="mx-2"
                        ></img>
                        <img
                          src={activePage == 3 ? Active_Dot : Inactive_Dot}
                        ></img>
                        <img
                          src={activePage == 4 ? Active_Dot : Inactive_Dot}
                          className="mx-2"
                        ></img>
                        <img
                          src={activePage == 5 ? Active_Dot : Inactive_Dot}
                        ></img>
                        <img
                          src={activePage == 6 ? Active_Dot : Inactive_Dot}
                          className="mx-2"
                        ></img>
                        <img
                          src={activePage == 7 ? Active_Dot : Inactive_Dot}
                        ></img>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Wrapper>
        ) : (
          this.state.currentScreen
        )}

        {/* SignUp Model */}
        <Model
          open={this.state.verifyOtpModel}
          onClose={this.handleVerifyNumberModel}
        >
          <VerifyOtp
            proSignup={true}
            onClose={this.handleVerifyNumberModel}
            handleVerifyOtp={this.handleVerifyOtp}
            handlePopularManufacturerList={this.handlePopularManufacturerList}
          />
        </Model>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        <style jsx>
          {`
            .proProfilePage {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .profileContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 2.562vw 1.464vw;
              margin: 1.464vw 0;
            }
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .profileIcon {
              width: 1.024vw;
            }
            .FormInput {
              margin: 1.098vw 0;
            }
            .notVerifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.585vw 0 0 0;
            }
            :global(.proProfilePage .SignUpInputBox) {
              width: 100%;
              height: 2.489vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${BLACK_COLOR};
              padding: 0;
            }
            :global(.proProfilePage .SignUpInputBox input) {
              font-size: 0.878vw;
              padding: 0.439vw 0 0.439vw 0.292vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
            }
            :global(.proProfilePage .SignUpInputBox input::placeholder) {
              font-size: 0.878vw;
              text-transform: capitalize;
              color: ${GREY_VARIANT_2} !important;
              opacity: 0.8;
              font-family: "Open Sans" !important;
            }
            .SelectInput {
              margin-bottom: 1.098vw;
            }
            :global(.proProfilePage .SignUpInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.proProfilePage .SignUpInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }

            :global(.profileContent .PhoneInput) {
              position: relative;
            }
            :global(.profileContent .PhoneInput > div > div) {
              width: 100%;
              display: flex;
              border: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.489vw;
              padding: 0;
            }
            :global(.profileContent .PhoneInput > div > div > input) {
              padding: 0.366vw 0;
              width: 100%;
              height: 100%;
              line-height: 1 !important;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border-radius: none;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              padding: 0 0.146vw !important;
              margin-left: 25% !important;
            }
            :global(.profileContent .PhoneInput > div > div div .country-list) {
              width: 100%;
              margin: 0.219vw 0 0 0;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .country-list
                .country) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
            }
            :global(.profileContent .PhoneInput > div > div > input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container) {
              height: 2.342vw;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag) {
              background: ${WHITE_COLOR} !important;
              height: 100%;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus),
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag:active) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code) {
              font-size: 0.878vw;
              height: 2.342vw;
              align-items: center;
              display: flex;
              padding: 0;
              line-height: 1 !important;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code::before) {
              content: url(${Phone_Outline_Icon_Grey});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus) {
              box-shadow: none !important;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-flag) {
              display: none;
            }
            :global(.profileContent
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-arrow) {
              display: none;
            }
            :global(.profileContent .PhoneInput > div > div div:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            .continue_btn,
            .inactive_Btn {
              margin: 1.83vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .contentForm {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .profileTitle {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 1.098vw;
            }
            .featureImg {
              width: 85%;
              object-fit: cover;
            }
            .featureDesc {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
            }
            .featureDesc:first-letter,
            .profileTitle:first-letter {
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(ProfessionalProfilePage);
