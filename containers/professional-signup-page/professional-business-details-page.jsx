import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  WHITE_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_3,
  THEME_COLOR,
  GREY_VARIANT_1,
  GREEN_COLOR,
  Border_LightGREY_COLOR_2,
  Website_Icon,
  About_Icon,
  Award_Icon,
  Share_Details_Icon,
  Active_Dot,
  Inactive_Dot,
  Long_Arrow_Left_LightGrey,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_1,
  Red_Color,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import TextAreaBox from "../../components/input-box/text-area-input";
import { WEBSITE } from "../../lib/input-control-data/input-definitions";
import { requiredValidator } from "../../lib/validation/validation";

class ProfessionalBusinessDetailsPage extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
  };

  componentDidMount() {
    document.getElementById("about").value = this.props.inputpayload
      .about_proUSer
      ? this.props.inputpayload.about_proUSer
      : "";
    this.setState(
      {
        inputpayload: { ...this.props.inputpayload },
        Website: this.props.inputpayload.Website ? 1 : "",
      },
      () => {
        this.checkIfFormValid();
      }
    );
  }

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.Website === 1 &&
      this.state.inputpayload.Website &&
      this.state.inputpayload.about_proUSer &&
      this.state.inputpayload.certificate_awards
        ? true
        : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    console.log("fejf-->", event.target.value, event.target.name);
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    console.log("fejf", inputControl);
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    console.log("fejf", tempsignUpPayload);
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
      },
      () => {
        // this.props.dispatch(setSignupData(this.state.inputpayload));
        console.log("inputpayload", this.state.inputpayload);
      }
    );
  };

  render() {
    const { activePage } = this.props;
    const { isFormValid } = this.state;
    const { Website, about_proUSer, certificate_awards } = {
      ...this.state.inputpayload,
    };
    return (
      <Wrapper>
        <div className="proBusinessDetailsPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0 align-items-center py-5">
                <div className="col-10 mx-auto">
                  <h5 className="heading text-center">
                    Tell us a little about your business
                  </h5>
                  <div className="businessContent">
                    <div className="row m-0">
                      <div className="col-6 contentForm px-4">
                        <div>
                          <h6 className="title">Your Business Details</h6>
                          <div className="websiteFormInput">
                            <InputBox
                              autoFocus
                              type="text"
                              className="inputBox form-control"
                              name={WEBSITE}
                              value={Website}
                              placeholder="Business Website Address"
                              onChange={this.handleOnchangeInput}
                              autoComplete="off"
                            ></InputBox>
                          </div>
                          {this.state[WEBSITE] == 4 ? (
                            <p
                              className="errMessage"
                              style={{ color: `${Red_Color}` }}
                            >
                              Enter Valid Website Link
                            </p>
                          ) : (
                            ""
                          )}
                          <div className="aboutFormInput">
                            <TextAreaBox
                              type="textarea"
                              id="about"
                              className="textareaBox form-control"
                              placeholder="Tell your customers about yourself and what makes your business unique."
                              name="about_proUSer"
                              value={about_proUSer}
                              onChange={this.handleOnchangeInput}
                              autoComplete="off"
                            ></TextAreaBox>
                          </div>
                          <div className="certificateFormInput">
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              name="certificate_awards"
                              value={certificate_awards}
                              placeholder="Certifications and Awards"
                              onChange={this.handleOnchangeInput}
                              autoComplete="off"
                            ></InputBox>
                          </div>

                          <div
                            className={
                              !isFormValid ? "inactive_Btn" : "continue_btn"
                            }
                          >
                            <ButtonComp
                              disabled={!isFormValid}
                              onClick={this.props.handleServiceSelection.bind(
                                this,
                                this.state.inputpayload
                              )}
                            >
                              Continue
                            </ButtonComp>
                          </div>
                        </div>
                      </div>
                      <div className="col-6 px-4">
                        <div>
                          <h6 className="profileTitle">
                            A brief description of the business provides an
                            advantage.
                          </h6>
                          <div className="row mx-0 my-2">
                            <div className="col-2 p-0">
                              <img
                                src={Share_Details_Icon}
                                className="featureImg"
                              ></img>
                            </div>
                            <div className="col-10 pr-0">
                              <p className="featureDesc m-0">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Quisque iaculis tincidunt nisi,
                                id egestas quam placerat non. Aliquam quis
                                suscipit tellus.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slider-dots">
                    <div className="back_btn">
                      <ButtonComp
                        onClick={this.props.handleBackPrevScreen.bind(
                          this,
                          activePage
                        )}
                      >
                        <img
                          src={Long_Arrow_Left_LightGrey}
                          className="backIcon"
                        ></img>
                        Back
                      </ButtonComp>
                    </div>
                    <div>
                      <img
                        src={activePage == 1 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 2 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 3 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 4 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 5 ? Active_Dot : Inactive_Dot}
                      ></img>
                      <img
                        src={activePage == 6 ? Active_Dot : Inactive_Dot}
                        className="mx-2"
                      ></img>
                      <img
                        src={activePage == 7 ? Active_Dot : Inactive_Dot}
                      ></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0;
            }
            .proBusinessDetailsPage {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .businessContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              padding: 2.562vw 1.464vw;
              margin: 1.464vw 0;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .websiteFormInput,
            .certificateFormInput {
              margin: 1.098vw 0;
              display: flex;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .proBusinessDetailsPage .websiteFormInput::before {
              content: url(${Website_Icon});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
              top: 0.292vw;
            }
            .proBusinessDetailsPage .certificateFormInput::before {
              content: url(${Award_Icon});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
              top: 0.292vw;
            }
            :global(.proBusinessDetailsPage .inputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.proBusinessDetailsPage .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
            .contentForm {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .aboutFormInput {
              margin: 1.098vw 0;
              display: flex;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .proBusinessDetailsPage .aboutFormInput::before {
              content: url(${About_Icon});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
              top: 0.292vw;
            }
            :global(.businessContent .aboutFormInput .textareaBox) {
              display: block;
              width: 100%;
              height: 7.32vw;
              padding: 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.businessContent .aboutFormInput .textareaBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.textareaBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .profileTitle {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 15px;
            }
            .featureImg {
              width: 85%;
              object-fit: cover;
            }
            .featureDesc {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
            }
            .featureDesc:first-letter,
            .profileTitle:first-letter {
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default ProfessionalBusinessDetailsPage;
