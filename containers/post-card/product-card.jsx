// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR_Dark,
  THEME_COLOR,
  WHITE_COLOR,
  Shipping_Blue_Icon,
  PlaceHolder_Boat,
  GREY_VARIANT_1,
  GREY_VARIANT_2,
} from "../../lib/config";

class ProductCard extends Component {
  /** if image returns 404, a placeholder image would be replaced */
  onError = (e) => {
    e.target.src = PlaceHolder_Boat;
  };
  render() {
    const { Productdetails, wishlist, professionalpage, shipping } = this.props;
    return (
      <Wrapper>
        {window.location.pathname === "/products" ||
        window.location.pathname === "/" ? (
          <div
            className={
              wishlist ? "col-12 wishlistItem p-0" : "col-12 singleItem p-0"
            }
          >
            <div className="row m-0">
              <img
                src={Productdetails.mainUrl || Productdetails.img}
                width="100%"
                style={{
                  height: "15vw",
                  borderTopLeftRadius: "4px",
                  borderTopRightRadius: "4px",
                }}
                onError={this.onError}
                className={professionalpage ? "wishlistCardImg" : ""}
              />
            </div>
            <div className="row m-0">
              <div
                className={
                  wishlist
                    ? "col-12 p-0 wishlistCardContent"
                    : "col-12 p-0 cardContent"
                }
              >
                <div>
                  <div
                    className="row mx-0"
                    style={{ marginBottom: "2px", height: "3vw" }}
                  >
                    <div className="col-9 px-0">
                      <h6 className="itemTitle">
                        {Productdetails.productName || Productdetails.title}
                      </h6>
                    </div>
                    <div className="col-3 text-right px-0">
                      <p className="itemPrice m-0">
                        {Productdetails.currency == "INR" ? (
                          <sup className="c-sub">&#8377;</sup>
                        ) : Productdetails.currency == "USD" ? (
                          <sup className="c-sub">&#36;</sup>
                        ) : (
                          Productdetails.currency
                        )}
                        {Productdetails.price}
                      </p>
                    </div>
                  </div>
                  <span className="itemLoc">
                    {Productdetails.city
                      ? `${Productdetails.city}, ${Productdetails.countrySname}`
                      : `${Productdetails.place}`}
                  </span>

                  <p className="ships_forPrice">
                    <img
                      src={Shipping_Blue_Icon}
                      className="shippingIcon"
                    ></img>
                    Ships for &#36;{Productdetails.price}
                  </p>
                  {/* {shipping ? (
                  <p className="ships_forPrice">
                    <img src={Shipping_Blue_Icon} className="shippingIcon"></img>
                    Ships for {Productdetails.ships_for}
                  </p>
                ) : (
                  ""
                )} */}
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div
            className={
              wishlist ? "col-12 wishlistItem p-0" : "col-12 singleItem p-0"
            }
          >
            <div className="row m-0">
              <img
                src={Productdetails.mainUrl || Productdetails.img}
                width="100%"
                onError={this.onError}
                className={professionalpage ? "wishlistCardImg" : "productImg"}
              />
            </div>
            <div className="row m-0">
              <div
                className={
                  wishlist
                    ? "col-12 p-0 wishlistCardContent"
                    : "col-12 p-0 cardContent"
                }
              >
                <div className="row m-0 d-flex justify-content-between">
                  <div
                    className="col-12 p-0 mx-0"
                    style={{ marginBottom: "0.15625vw" }}
                  >
                    <h6 className="my-boats-title m-0">
                      {Productdetails.productName || Productdetails.title}
                    </h6>
                  </div>
                  <p className="m-0 my-boats-location">
                    {Productdetails.city
                      ? `${Productdetails.city}, ${Productdetails.countrySname}`
                      : `${Productdetails.place}`}
                  </p>
                  <div className="col-12 px-0">
                    <p className="my-boats-price m-0">
                      {Productdetails.currency == "INR" ? (
                        <span id="currancySymbol">&#8377;</span>
                      ) : Productdetails.currency == "USD" ? (
                        <span id="currancySymbol">&#36;</span>
                      ) : (
                        Productdetails.currency
                      )}
                      <span id="price">{Productdetails.price}</span>
                    </p>
                  </div>
                  <div className="col-12  px-0">
                    <p className="ships_forPrice">
                      <img
                        src={Shipping_Blue_Icon}
                        className="shippingIcon"
                      ></img>
                      Ships for &#36;{Productdetails.price}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

        <style jsx>
          {`
            #price {
              font-family: Museo-Sans !important;
              font-weight: 500;
              color: ${FONTGREY_COLOR_Dark};
              font-size: 0.856vw;
            }
            .my-boats-title {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 0.833vw;
              font-weight: 500;
              font-family: Museo-Sans !important;
              text-transform: capitalize;
            }
            .productImg {
              height: ${window.location.pathname === "/" ||
              window.location.pathname === "/my-boats"
                ? "auto"
                : "8.413vw"};
              min-height: ${window.location.pathname === "/my-boats"
                ? "10.417vw"
                : "unset"};
              object-fit: cover;
              border-top-left-radius: 4px;
              border-top-right-radius: 4px;
            }
            .my-boats-price {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 0.833vw;
              font-weight: 500;
              font-family: Museo-Sans !important;
              line-height: 1.4;
            }
            .my-boats-location {
              color: ${GREY_VARIANT_2};
              font-size: 0.749vw;
              text-transform: capitalize;
              font-family: Open Sans !important;
              font-style: normal;
              font-weight: normal;
              line-height: 1.4;
              margin-bottom: 0.417vw !important;
            }
            .c-sub {
              position: relative;
              font-size: 60%;
              line-height: 0;
              vertical-align: baseline;
              top: -0.4em;
            }
            .singleItem {
              width: 100%;
              background-color: ${WHITE_COLOR};
              border-radius: 3px;
              // box-shadow: 0 0px 3px 0 rgba(48, 56, 97, 0.1);
              position: relative;
            }
            .wishlistItem {
              width: 100%;
              background-color: ${WHITE_COLOR};
              border-radius: 3px;
              position: relative;
            }
            .wishlistCardImg {
              height: 9.516vw;
              border-top-left-radius: 4px;
              border-top-right-radius: 4px;
            }
            .wishlistCardContent {
              padding: 0.878vw 0.732vw !important;
              height: auto;
              // max-height: 24vw;
              align-items: center;
              display: flex;
              flex-direction: row-reverse;
            }
            .wishlistCardContent div {
              width: 100%;
              // margin-right: 0.878vw;
            }
            .cardContent {
              padding: 0.878vw 0.732vw !important;
            }
            .itemTitle {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 0.878vw;
              margin: 0;
              letter-spacing: 0.0219vw;
              font-family: "Museo-Sans" !important;
              text-transform: capitalize;
            }
            .itemLoc {
              color: ${GREY_VARIANT_1};
              font-size: 0.6688vw;
              font-family: "Open Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw;
              text-transform: capitalize;
            }
            .itemPrice {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 1vw;
              // margin: 0.366vw 0 0 0;
              font-family: "Museo-Sans" !important;
            }
            .itemPrice span {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 1vw;
              font-family: "Museo-Sans" !important;
            }
            .shippingIcon {
              width: 0.878vw;
              margin-right: 0.292vw;
              margin-bottom: 0.219vw;
            }
            .ships_forPrice {
              font-size: 0.7vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw;
              color: ${THEME_COLOR};
              font-style: normal;
              font-weight: normal;
              margin: 0.366vw 0 0 0;
            }
            #currancySymbol {
              // font-size: 0.585vw;
              margin-right: 2px;
              position: relative;
              bottom: ${window.location.pathname === "/boats"
                ? "0.293vw"
                : "0.05vw"};
              font-family: Museo-Sans !important;
              font-weight: 500 !important;
              color: ${window.location.pathname === "/boats"
                ? THEME_COLOR
                : FONTGREY_COLOR_Dark};
              font-size: ${window.location.pathname === "/boats"
                ? "0.512vw"
                : "0.856vw"};
            }
            .cardFooter {
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProductCard;
