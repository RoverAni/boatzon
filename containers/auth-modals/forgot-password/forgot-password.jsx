// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../../hoc/Wrapper";
import {
  Close_Icon,
  Email_Icon,
  BLACK_COLOR,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREY_VARIANT_2,
  GREEN_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_1,
  GREY_VARIANT_5,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  Facebook_Blue,
  GREY_VARIANT_1,
  FONTGREY_COLOR_Dark,
} from "../../../lib/config";
import { EMAIL } from "../../../lib/input-control-data/input-definitions";
import { requiredValidator } from "../../../lib/validation/validation";
import InputWithIcon from "../../../components/input-box/input-with-icon";
import ButtonComp from "../../../components/button/button";
import LoginPage from "../login/login-page";
import { forgotPassword } from "../../../services/auth";
import Snackbar from "../../../components/snackbar";

class ForgotPassword extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
    isRemembered: false,
  };

  // Function for User inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
      let tempsignUpPayload = this.state.inputpayload;
      tempsignUpPayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: tempsignUpPayload,
      });
    });
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state[EMAIL] == 1 ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Function to redirect to SignUp modal
  handleLoginModel = () => {
    this.updateScreen(<LoginPage onClose={this.props.onClose} />);
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleForgotPassword = () => {
    let payload = {
      type: "0",
      email: this.state.inputpayload.Email,
    };
    forgotPassword(payload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            setTimeout(() => {
              this.props.onClose();
            }, 1000);
          }
        }
      })
      .catch((err) => {
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const { isFormValid } = this.state;

    return (
      <Wrapper>
        {!this.state.currentScreen ? (
          <div className="col-12 FrgtPassModel p-0">
            <div className="modelContent">
              <img
                src={Close_Icon}
                className="closeIcon"
                onClick={this.props.onClose}
              ></img>
              <h5 className="heading">Forgot Password?</h5>
              <div className="FormInput">
                <InputWithIcon
                  type="email"
                  className="FrgtPassInputBox form-control"
                  name={EMAIL}
                  inputAdornment={
                    <img src={Email_Icon} className="emailIcon"></img>
                  }
                  placeholder="Enter Your Email"
                  onChange={this.handleOnchangeInput(`${EMAIL}`)}
                  autoComplete="off"
                />
                {this.state[EMAIL] == 0 || this.state[EMAIL] == 2 ? (
                  <p className="errMessage">Enter Valid Email</p>
                ) : (
                  ""
                )}
                <p className="recoveryMsg">
                  We will send the recovery link to the address
                </p>
              </div>

              <div className={!isFormValid ? "inactive_Btn" : "Login_Btn"}>
                <ButtonComp
                  disabled={!isFormValid}
                  onClick={this.handleForgotPassword}
                >
                  Send Recovery Link
                </ButtonComp>
              </div>
              <p className="LoginMsg">
                Return to{" "}
                <a target="_blank" onClick={this.handleLoginModel}>
                  Log in
                </a>
              </p>
            </div>
          </div>
        ) : (
          this.state.currentScreen
        )}

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .modelContent {
              background: ${WHITE_COLOR};
              overflow-y: auto;
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.292vw;
              padding: 1.464vw 1.830vw;
            }
            .FrgtPassModel {
              width: 23.426vw;
              position: relative;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              padding: 0.585vw 0 0 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .emailIcon {
              width: 1.024vw;
            }
            .passwordIcon {
              width: 0.878vw;
              margin-bottom: 0.292vw;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              margin: 0.366ve 0;
              font-size: 1.756vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .FormInput {
              margin: 1.098vw 0;
            }
            :global(.FrgtPassModel .FrgtPassInputBox) {
              width: 100%;
              height: 2.562vw;
              padding: 0.375rem 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${BLACK_COLOR};
            }
            :global(.FrgtPassModel .FrgtPassInputBox input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.FrgtPassModel .FrgtPassInputBox input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Open Sans" !important;
            }
            :global(.FrgtPassModel .FrgtPassInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.FrgtPassModel .FrgtPassInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.Login_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
            }
            :global(.Login_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.Login_Btn button:focus),
            :global(.Login_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.Login_Btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
            }
            .Frgt_Pass {
              margin: 0;
              font-size: 0.805vw;
              cursor: pointer;
              color: ${GREEN_COLOR};
              text-align: right;
              font-weight: lighter !important;
            }
            .text {
              margin: 0;
              text-align: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            .text:before {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              right: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5};
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .text:after {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              left: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            :global(.GoogleLogin_Btn button) {
              width: 100%;
              margin: 1.464vw 0 0.732vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR};
              color: ${FONTGREY_COLOR};
              font-size: 0.951vw;
              position: relative;
            }
            :global(.GoogleLogin_Btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.GoogleLogin_Btn button:focus),
            :global(.GoogleLogin_Btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
            }
            :global(.FacebookLogin_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${Facebook_Blue};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
              position: relative;
            }
            :global(.FacebookLogin_Btn button:hover) {
              background: ${Facebook_Blue};
            }
            :global(.FacebookLogin_Btn button:focus),
            :global(.FacebookLogin_Btn button:active) {
              background: ${Facebook_Blue};
              outline: none;
            }
            .googleIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 1.098vw;
            }
            .facebookIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 1.098vw;
            }
            .recoveryMsg {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing 0.3px !important;
              text-align: center;
              padding: 0.585vw 0 0 0;
              margin: 0;
            }
            .LoginMsg {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              text-align: center;
              padding: 1.098vw 0 0.585vw 0;
              font-family: "Open Sans" !important;
              letter-spacing 0.3px !important;
              margin: 0;
            }
            .LoginMsg a {
              text-decoration: none;
              color: ${GREEN_COLOR};
              cursor: pointer;
              font-family: "Open Sans" !important;
              letter-spacing 0.3px !important;
            }
            // // media query
            // @media only screen and (min-width: 768px) and (max-width: 992px) {
            //   .FrgtPassModel {
            //     width: 300px;
            //   }
            //   .errMessage {
            //     font-size: 9px;
            //     padding: 0.366vw 0 0 0;
            //   }
            //   .emailIcon {
            //     width: 13px;
            //   }
            //   .passwordIcon {
            //     width: 11px;
            //   }
            //   .closeIcon {
            //     width: 9px;
            //   }
            //   .heading {
            //     font-size: 1.3rem;
            //   }
            //   .FormInput {
            //     margin: 10px 0;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox) {
            //     height: 28px;
            //     font-size: 11px;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox ::placeholder) {
            //     font-size: 11px;
            //   }
            //   :global(.Login_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   :global(.inactive_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   .Frgt_Pass {
            //     font-size: 10px;
            //   }
            //   .text {
            //     font-size: 11px;
            //   }
            //   :global(.GoogleLogin_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   :global(.FacebookLogin_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   .googleIcon {
            //     top: 8px;
            //     width: 14px;
            //   }
            //   .facebookIcon {
            //     top: 8px;
            //     width: 14px;
            //   }
            //   .recoveryMsg {
            //     font-size: 10px;
            //   }
            //   .LoginMsg {
            //     font-size: 11px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 993px) and (max-width: 1199px) {
            //   .FrgtPassModel {
            //     width: 300px;
            //   }
            //   .errMessage {
            //     font-size: 9px;
            //     padding: 0.366vw 0 0 0;
            //   }
            //   .emailIcon {
            //     width: 13px;
            //   }
            //   .passwordIcon {
            //     width: 11px;
            //   }
            //   .closeIcon {
            //     width: 9px;
            //   }
            //   .heading {
            //     font-size: 1.3rem;
            //   }
            //   .FormInput {
            //     margin: 10px 0;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox) {
            //     height: 30px;
            //     font-size: 11px;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox ::placeholder) {
            //     font-size: 11px;
            //   }
            //   :global(.Login_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   :global(.inactive_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   .Frgt_Pass {
            //     font-size: 10px;
            //   }
            //   .text {
            //     font-size: 11px;
            //   }
            //   :global(.GoogleLogin_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   :global(.FacebookLogin_Btn button) {
            //     font-size: 12px;
            //     height: 30px;
            //   }
            //   .googleIcon {
            //     top: 8px;
            //     width: 14px;
            //   }
            //   .facebookIcon {
            //     top: 8px;
            //     width: 14px;
            //   }
            //   .recoveryMsg {
            //     font-size: 10px;
            //   }
            //   .LoginMsg {
            //     font-size: 11px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 1200px) and (max-width: 1400px) {
            //   .FrgtPassModel {
            //     width: 320px;
            //   }
            //   .errMessage {
            //     font-size: 10px;
            //     padding: 0.366vw 0 0 0;
            //   }
            //   .emailIcon {
            //     width: 14px;
            //   }
            //   .passwordIcon {
            //     width: 12px;
            //   }
            //   .closeIcon {
            //     width: 10px;
            //   }
            //   .heading {
            //     font-size: 1.5rem;
            //   }
            //   .FormInput {
            //     margin: 13px 0;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox) {
            //     height: 35px;
            //     font-size: 12px;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox ::placeholder) {
            //     font-size: 12px;
            //   }
            //   :global(.Login_Btn button) {
            //     font-size: 13px;
            //   }
            //   :global(.inactive_Btn button) {
            //     font-size: 13px;
            //   }
            //   .Frgt_Pass {
            //     font-size: 11px;
            //   }
            //   .text {
            //     font-size: 12px;
            //   }
            //   :global(.GoogleLogin_Btn button) {
            //     font-size: 13px;
            //   }
            //   :global(.FacebookLogin_Btn button) {
            //     font-size: 13px;
            //   }
            //   .googleIcon {
            //     top: 10px;
            //     width: 15px;
            //   }
            //   .facebookIcon {
            //     top: 10px;
            //     width: 15px;
            //   }
            //   .recoveryMsg {
            //     font-size: 11px;
            //   }
            //   .LoginMsg {
            //     font-size: 12px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 1920px) {
            //   .FrgtPassModel {
            //     width: 370px;
            //   }
            //   .errMessage {
            //     font-size: 13px;
            //     padding: 0.366vw 0 0 0;
            //   }
            //   .emailIcon {
            //     width: 16px;
            //   }
            //   .passwordIcon {
            //     width: 14px;
            //   }
            //   .closeIcon {
            //     width: 12px;
            //   }
            //   .heading {
            //     font-size: 2rem;
            //   }
            //   .FormInput {
            //     margin: 18px 0;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox) {
            //     height: 40px;
            //     font-size: 15px;
            //   }
            //   :global(.FrgtPassModel .FrgtPassInputBox ::placeholder) {
            //     font-size: 15px;
            //   }
            //   :global(.Login_Btn button) {
            //     font-size: 16px;
            //     height: 38px;
            //   }
            //   :global(.inactive_Btn button) {
            //     font-size: 16px;
            //     height: 38px;
            //   }
            //   .Frgt_Pass {
            //     font-size: 13px;
            //   }
            //   .text {
            //     font-size: 13px;
            //     margin: 10px 0;
            //   }
            //   :global(.GoogleLogin_Btn button) {
            //     font-size: 16px;
            //     height: 38px;
            //   }
            //   :global(.FacebookLogin_Btn button) {
            //     font-size: 16px;
            //     height: 38px;
            //   }
            //   .googleIcon {
            //     top: 13px;
            //     width: 15px;
            //   }
            //   .facebookIcon {
            //     top: 13px;
            //     width: 15px;
            //   }
            //   .recoveryMsg {
            //     font-size: 13px;
            //   }
            //   .LoginMsg {
            //     font-size: 14px;
            //   }
            // }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ForgotPassword;
