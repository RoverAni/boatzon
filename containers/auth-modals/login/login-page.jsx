// Main React Components
import React, { Component } from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../../../hoc/Wrapper";
import {
  Close_Icon,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREY_VARIANT_2,
  Password_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_1,
  GREY_VARIANT_5,
  Google_Icon,
  Facebook_Icon,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  Facebook_Blue,
  GREY_VARIANT_1,
  FONTGREY_COLOR_Dark,
  Professional_Type_Icon,
  FB_APP_ID,
  Noraml_Login_Type,
  Google_Login_Type,
  Facebook_Login_Type,
  BG_LightGREY_COLOR,
  View_Grey_UnFilled_Icon,
  View_Grey_Filled_Icon,
} from "../../../lib/config";
import { PASSWORD } from "../../../lib/input-control-data/input-definitions";
import InputWithIcon from "../../../components/input-box/input-with-icon";
import CheckBox from "../../../components/input-box/check-box";
import SignUpPage from "../sign-up/sign-up-page";
import ForgotPassword from "../forgot-password/forgot-password";
import { userLogin } from "../../../services/auth";
import Snackbar from "../../../components/snackbar";
import {
  setupToken,
  setupUserId,
  setupMqttId,
  setupUserCity,
  setupUserCountry,
  setupUserLocation,
  setupUserName,
} from "../../../lib/data-handler/data-handler";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login";
import { setUserData } from "../../../redux/actions/user-data/user-data";
import CircularProgressButton from "../../../components/button-loader/button-loader";
import Router, { withRouter } from "next/router";
import { setApiLoading } from "../../../redux/actions/auth";

class LoginPage extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
    isRemembered: false,
    passView: false,
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = this.state.inputpayload;
    tempsignUpPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: tempsignUpPayload,
        [inputControl.name]: inputControl.value,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  handleOnchangeInputV2 = (key, value) => {
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[key] = value;
    this.setState(
      {
        inputpayload: tempsignUpPayload,
        [key]: value,
      }
    );
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state.username && this.state[PASSWORD] ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  // Function to redirect to SignUp modal
  handleSignupModel = () => {
    this.updateScreen(<SignUpPage onClose={this.props.onClose} />);
  };

  // Function to redirect to ForgotPassword modal
  handleForgotPassModel = () => {
    this.updateScreen(<ForgotPassword onClose={this.props.onClose} />);
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleSnackbarMsg = (response) => {
    switch (response.code) {
      case 200:
        return response.message;
        break;
      case 204:
        return "User not found! User need to signup before login";
        break;
      default:
        return response.message;
        break;
    }
  };

  handleUserLogin = () => {
    this.props.dispatch(setApiLoading(true));
    let payload = {
      username: this.state.inputpayload.username,
      password: this.state.inputpayload.Password,
      loginType: Noraml_Login_Type,
      longitude: this.props.currentLocation.longitude,
      latitude: this.props.currentLocation.latitude,
      city: this.props.currentLocation.city,
      countrySname: this.props.currentLocation.country,
      location: this.props.currentLocation.address,
    };
    userLogin(payload)
      .then((res) => {
        let response = res.data;
        this.setState({
          usermessage: this.handleSnackbarMsg(response),
          variant: this.handleSnackbar(response),
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
        this.props.dispatch(setApiLoading(false));
        if (response.code == 200) {
          setupToken(response.token);
          setupUserId(response.userId);
          setupUserName(response.username);
          setupMqttId(response.mqttId);
          setupUserCity(response.city);
          setupUserCountry(response.countrySname);
          setupUserLocation(response.latitude, response.longitude);
          this.props.dispatch(setUserData(response));
          setTimeout(() => {
            this.props.onClose();
            this.props.router.pathname == "/verify-email"
              ? Router.push("/")
              : window.location.reload();

            // this.props.onClose();
          }, 500);
        }
      })
      .catch((err) => {
        this.props.dispatch(setApiLoading(false));
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  googleLoginResponseOnSuccess = (response) => {
    let googleResponse = response;

    let googleLoginPayload = {
      loginType: Google_Login_Type,
      googleId: googleResponse.googleId,
      accessToken: googleResponse.accessToken,
      longitude: this.props.currentLocation.longitude,
      latitude: this.props.currentLocation.latitude,
      city: this.props.currentLocation.city,
      countrySname: this.props.currentLocation.country,
      location: this.props.currentLocation.address,
    };

    userLogin(googleLoginPayload)
      .then((res) => {
        let response = res.data;
        this.setState({
          usermessage: this.handleSnackbarMsg(response),
          variant: this.handleSnackbar(response),
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
        if (response.code == 200) {
          setupToken(response.token);
          setupUserId(response.userId);
          setupMqttId(response.mqttId);
          setupUserName(response.username);
          setupUserCity(response.city);
          setupUserCountry(response.countrySname);
          setupUserLocation(response.latitude, response.longitude);
          this.props.dispatch(setUserData(response));
          setTimeout(() => {
            this.props.onClose();
            this.props.router.pathname == "/verify-email"
              ? Router.push("/")
              : window.location.reload();
          }, 500);
        }
        if (response.code == 204) {
          this.handleSignupModel();
        }
      })
      .catch((err) => {
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  googleLoginResponseOnFailure = (error) => {
    this.setState({
      usermessage: this.handleGoogleErrorMsg(error),
      variant: "error",
      open: true,
      vertical: "bottom",
      horizontal: "left",
    });
  };

  handleGoogleErrorMsg = (error) => {
    switch (error.error) {
      case "idpiframe_initialization_failed":
        return error.details;
        break;
      default:
        return error.error;
        break;
    }
  };

  responseFacebookLogin = (response) => {
    if (response.status !== "unknown") {
      let facebookResponse = response;
      let facebookLoginPayload = {
        loginType: Facebook_Login_Type,
        facebookId: facebookResponse.userID,
        longitude:
          this.props.currentLocation && this.props.currentLocation.longitude,
        latitude:
          this.props.currentLocation && this.props.currentLocation.latitude,
        city: this.props.currentLocation && this.props.currentLocation.city,
        countrySname:
          this.props.currentLocation && this.props.currentLocation.country,
        location:
          this.props.currentLocation && this.props.currentLocation.address,
      };
      userLogin(facebookLoginPayload)
        .then((res) => {
          let response = res.data;
          this.setState({
            usermessage: this.handleSnackbarMsg(response),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            setupToken(response.token);
            setupUserId(response.userId);
            setupMqttId(response.mqttId);
            setupUserName(response.username);
            setupUserCity(response.city);
            setupUserCountry(response.countrySname);
            setupUserLocation(response.latitude, response.longitude);
            this.props.dispatch(setUserData(response));
            setTimeout(() => {
              this.props.onClose();
              this.props.router.pathname == "/verify-email"
                ? Router.push("/")
                : window.location.reload();
            }, 500);
          }
          if (response.code == 204) {
            this.handleSignupModel();
          }
        })
        .catch((err) => {
          this.setState({
            usermessage: err.message,
            variant: "error",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        });
    }
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handlePasswordView = () => {
    this.setState({
      passView: !this.state.passView,
    });
  };

  componentDidMount() {
    this.props.dispatch(setApiLoading(false));
    try {
      if (this.props.uname) {
        this.handleOnchangeInputV2("username", this.props.uname);
      }
    } catch (e) {}
  }

  render() {
    const {
      currentScreen,
      isFormValid,
      isRemembered,
      variant,
      usermessage,
      open,
      vertical,
      horizontal,
      passView,
      Password,
    } = this.state;
    const { onClose, apiLoading } = this.props;
    return (
      <Wrapper>
        {!currentScreen ? (
          <div className="col-12 p-0 LoginModel">
            <div className="modelContent">
              <img
                src={Close_Icon}
                className="closeIcon"
                onClick={onClose}
              ></img>
              <h5 className="heading">Log in</h5>
              <div className="FormInput">
                <InputWithIcon
                  autoFocus
                  type="email"
                  value={this.state.inputpayload.username}
                  className="LoginInputBox form-control"
                  name="username"
                  inputAdornment={
                    <img
                      src={Professional_Type_Icon}
                      className="emailIcon"
                    ></img>
                  }
                  placeholder="Username"
                  onChange={this.handleOnchangeInput}
                  autoComplete="off"
                />
              </div>
              <div className="FormInput">
                <InputWithIcon
                  type={passView ? "text" : "password"}
                  className="LoginInputBox form-control"
                  name={PASSWORD}
                  autoFocus={this.props.uname ? true : false}
                  inputAdornment={
                    <img src={Password_Icon} className="passwordIcon"></img>
                  }
                  placeholder="Password"
                  onChange={this.handleOnchangeInput}
                  autoComplete="off"
                />
                {Password ? (
                  <img
                    src={
                      passView ? View_Grey_Filled_Icon : View_Grey_UnFilled_Icon
                    }
                    className="viewIcon"
                    onClick={this.handlePasswordView}
                  ></img>
                ) : (
                  ""
                )}
              </div>
              <div className={!isFormValid ? "inactive_Btn" : "Login_Btn"}>
                <CircularProgressButton
                  buttonText={"Log in"}
                  disabled={!isFormValid}
                  onClick={this.handleUserLogin}
                  loading={apiLoading}
                />
              </div>
              <div className="row m-0 align-items-center mb-2">
                <div className="col-6 p-0">
                  <CheckBox
                    checked={isRemembered}
                    onChange={this.handleOnchangeCheckbox("isRemembered")}
                    value="isRemembered"
                    label="Remember Me"
                  />
                </div>
                <div className="col-6 p-0">
                  <p className="Frgt_Pass" onClick={this.handleForgotPassModel}>
                    Forgot Your Password?
                  </p>
                </div>
              </div>
              <div className="position-relative">
                <p className="text">or</p>
              </div>
              <div className="GoogleLogin_Btn">
                <img src={Google_Icon} className="googleIcon"></img>
                <GoogleLogin
                  clientId="1029574855542-sqas8esrvk6ajj1fp2rpqo41raojvtkv.apps.googleusercontent.com"
                  buttonText={`Log in with Google`}
                  onSuccess={this.googleLoginResponseOnSuccess}
                  onFailure={this.googleLoginResponseOnFailure}
                  cookiePolicy={"single_host_origin"}
                  icon={false}
                />
              </div>
              <div className="FacebookLogin_Btn">
                <img src={Facebook_Icon} className="facebookIcon"></img>
                <FacebookLogin
                  appId={FB_APP_ID}
                  autoLoad={false}
                  textButton={`Log in with Facebook`}
                  fields="name,email,picture"
                  callback={this.responseFacebookLogin}
                />
              </div>
              <p className="LoginTerms_Sec">
                By signing up or logging in, you agree to the Boatzon{" "}
                <a>Terms of Service</a> and <a>Privacy Policy</a>.
              </p>
              <p className="signUpMsg">
                Dont have an account?{" "}
                <a target="_blank" onClick={this.handleSignupModel}>
                  Sign up
                </a>
              </p>
            </div>
          </div>
        ) : (
          currentScreen
        )}

        {/* Snakbar Components */}
        <Snackbar
          variant={variant}
          message={usermessage}
          open={open}
          onClose={this.handleSnackbarClose}
          vertical={vertical}
          horizontal={horizontal}
        />
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .modelContent {
              background: ${WHITE_COLOR};
              overflow-y: auto;
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.293vw;
              padding: 1.464vw 1.83vw;
            }
            .LoginModel {
              width: 25.622vw;
              position: relative;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              padding: 0.586vw 0 0 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
            }
            .emailIcon {
              width: 1.025vw;
            }
            .passwordIcon {
              width: 0.878vw;
              margin-bottom: 0.293vw;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.757vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .FormInput {
              margin: 1.098vw 0;
              position: relative;
            }
            .viewIcon {
              position: absolute;
              right: 10px;
              top: 50%;
              transform: translate(-50%, 0);
              cursor: pointer;
              width: 0.951vw;
            }
            :global(.LoginModel .LoginInputBox) {
              width: 100%;
              height: 2.562vw;
              padding: 0.439vw 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
            }
            :global(.LoginModel .LoginInputBox input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
            }
            :global(.LoginModel .LoginInputBox input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
            }
            :global(.LoginModel .LoginInputBox::before) {
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.LoginModel .LoginInputBox::after) {
              border-bottom: 0.073vw solid ${THEME_COLOR} !important;
            }
            :global(.Login_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              font-size: 0.952vw;
              border-radius: 0.146vw;
            }
            :global(.Login_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.Login_Btn button:focus),
            :global(.Login_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.Login_Btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR_1};
              color: ${BG_LightGREY_COLOR};
              font-size: 0.952vw;
              border-radius: 0.146vw;
            }
            .Frgt_Pass {
              margin: 0;
              font-size: 0.805vw;
              cursor: pointer;
              color: ${GREEN_COLOR};
              text-align: right;
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
            }
            .text {
              margin: 0;
              text-align: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.022vw !important;
            }
            .text:before {
              content: " ";
              position: absolute;
              bottom: 0.586vw;
              right: 55%;
              border-bottom: 0.073vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .text:after {
              content: " ";
              position: absolute;
              bottom: 0.586vw;
              left: 55%;
              border-bottom: 0.073vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .GoogleLogin_Btn,
            .FacebookLogin_Btn {
              position: relative;
            }
            :global(.GoogleLogin_Btn button) {
              width: 100%;
              margin: 0.732vw 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR} !important;
              color: ${FONTGREY_COLOR} !important;
              position: relative;
              box-shadow: none !important;
              border-radius: 0.146vw;
              opacity: 1 !important;
            }
            :global(.GoogleLogin_Btn button div) {
              padding: 0 !important;
              background: none !important;
            }
            :global(.GoogleLogin_Btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.037vw !important;
              font-size: 0.952vw !important;
              font-weight: 600 !important;
              padding: 0 !important;
              width: 100%;
            }
            :global(.GoogleLogin_Btn button:hover) {
              background: ${Border_LightGREY_COLOR} !important;
              opacity: 1 !important;
            }
            :global(.GoogleLogin_Btn button:focus),
            :global(.GoogleLogin_Btn button:active) {
              background: ${Border_LightGREY_COLOR} !important;
              outline: none !important;
              opacity: 1 !important;
            }

            :global(.FacebookLogin_Btn button) {
              width: 100%;
              margin: 0;
              padding: 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${Facebook_Blue};
              color: ${WHITE_COLOR};
              position: relative !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.037vw !important;
              font-size: 0.952vw;
              font-weight: 600;
              border-radius: 0.146vw;
            }
            :global(.FacebookLogin_Btn button:hover) {
              background: ${Facebook_Blue};
            }
            :global(.FacebookLogin_Btn button:focus),
            :global(.FacebookLogin_Btn button:active) {
              background: ${Facebook_Blue};
              outline: none;
            }
            .googleIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
            .facebookIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
            .LoginTerms_Sec {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
              text-align: center;
              padding: 0.586vw 0;
              margin: 0;
            }
            .LoginTerms_Sec a {
              text-decoration: none;
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
              cursor: pointer;
            }
            .signUpMsg {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              text-align: center;
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
              padding: 1.098vw 0 0.878vw 0;
              margin: 0;
            }
            .signUpMsg a {
              text-decoration: none;
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.022vw !important;
              cursor: pointer;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(withRouter(LoginPage));
