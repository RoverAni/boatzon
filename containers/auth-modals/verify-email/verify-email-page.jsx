import React, { Component } from "react";
import Wrapper from "../../../hoc/Wrapper";
import {
  WHITE_COLOR,
  GREEN_COLOR,
  FONTGREY_COLOR_Dark,
  Successful_SignUp,
  THEME_COLOR,
} from "../../../lib/config";
import ButtonComp from "../../../components/button/button";
import { saveNewEmail } from "../../../services/userProfile";

import Router, { withRouter } from "next/router";
import { getCookie } from "../../../lib/session";
import Model from "../../../components/model/model";
import LoginPage from "../login/login-page";
import PageLoader from "../../../components/loader/page-loader";

class VerifyEmailPage extends Component {
  state = {
    AuthPass: getCookie("authPass"),
    loginPage: false,
    emailVerified: 0,
    apiCallLoader: true,
  };

  componentDidMount() {
    this.handleVerifyNewEmail();
  }

  handleVerifyNewEmail = () => {
    const { token } = this.props.router && this.props.router.query;
    const queryArr = token.split("/");
    let payload = {
      verificationToken: queryArr[1],
      authToken: queryArr[0],
    };
    saveNewEmail(payload)
      .then((res) => {
        console.log("res", res);
        let response = res;
        if (
          response.code == 200 ||
          response.data.code == 200 ||
          response.status == 200
        ) {
          this.setState({
            emailVerified: response.data[0].emailVerified,
            apiCallLoader: false,
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          emailVerified: false,
          apiCallLoader: false,
        });
      });
  };

  handleGotoMyAccount = () => {
    this.state.AuthPass
      ? Router.push("/my-account/acc-settings")
      : this.handleLogin();
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  render() {
    const { emailVerified, apiCallLoader } = this.state;
    return (
      <Wrapper>
        <div className="col-12 p-0 emailVerifiesModel">
          {apiCallLoader ? (
            <div>
              <PageLoader loading={apiCallLoader} color={THEME_COLOR} />
            </div>
          ) : (
            <div className="row m-0">
              <div className="col-6 mx-auto my-4 p-0">
                <div className="modelContent">
                  {emailVerified ? (
                    <div>
                      <img
                        src={Successful_SignUp}
                        className="successfullLogo"
                      ></img>

                      <p className="heading">
                        Congratulations your email has been verified
                      </p>
                      <div className="Login_Btn">
                        <ButtonComp onClick={this.handleGotoMyAccount}>
                          go to My Account
                        </ButtonComp>
                      </div>
                    </div>
                  ) : (
                    <p className="heading">
                      Something went wrong please try again after sometime
                    </p>
                  )}
                </div>
              </div>
            </div>
          )}
        </div>

        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>

        <style jsx>
          {`
            .emailVerifiesModel {
              min-height: 23.426vw;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .successfullLogo {
              width: 38%;
            }
            .modelContent {
              background: ${WHITE_COLOR};
              overflow-y: auto;
              border-radius: 0.292vw;
              padding: 0 1.83vw;
              width: 25.622vw;
              position: relative;
              text-align: center;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.171vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
              text-align: center;
            }
            :global(.Login_Btn button) {
              width: 70%;
              margin: 1.464vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
              text-transform: capitalize;
            }
            :global(.Login_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.Login_Btn button:focus),
            :global(.Login_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.Login_Btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withRouter(VerifyEmailPage);
