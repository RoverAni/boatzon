import React, { Component } from "react";
import Wrapper from "../../../hoc/Wrapper";
import {
  Close_Icon,
  DummyProfilePic_Icon,
  FONTGREY_COLOR_Dark,
  Business_Profile_Icon,
  Professional_Type_Icon,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  BLACK_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_2,
  Phone_Outline_Icon_Grey,
  THEME_COLOR_Opacity,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  THEME_COLOR,
  Active_Dot,
  Inactive_Dot,
  Long_Arrow_Left_LightGrey,
  Border_LightGREY_COLOR_1,
  Verified_Icon,
  NotVerified_Icon,
  Red_Color,
  IMAGE_UPLOAD_SIZE,
  Camera_Icon,
} from "../../../lib/config";
import InputWithIcon from "../../../components/input-box/input-with-icon";
import {
  requiredValidator,
  validatePhoneNumber,
} from "../../../lib/validation/validation";
import IntlTelInput from "react-intl-tel-input";
import {
  MOBILE,
  COUNTRY_CODE,
} from "../../../lib/input-control-data/input-definitions";
import ButtonComp from "../../../components/button/button";
import {
  UserNameVerfication,
  PhoneNumberVerfication,
} from "../../../services/auth";

class AccountInfo extends Component {
  state = {
    inputpayload: {
      preferredCountries: ["in"],
    },
    isFormValid: false,
    preferredCountries: ["in"],
    phoneInput: false,
  };

  componentDidMount() {
    let tempPayload = {
      ...this.state.inputpayload,
      ...this.props.inputpayload,
    };
    this.setState(
      {
        inputpayload: { ...tempPayload },
        phoneInput: true,
        profilePicUrl: this.props.inputpayload.profilePicUrl
          ? this.props.inputpayload.profilePicUrl
          : "",
        preferredCountries: [
          `${
            this.props.currentLocation &&
            this.props.currentLocation.countryShortName
          }`,
        ],
      },
      () => {
        this.checkIfFormValid();
      }
    );
    this.validatePhoneInput();
  }

  // Number Validation function for phoneInput
  validatePhoneInput = () => {
    validatePhoneNumber("phoneNum");
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    this.setState({
      inputpayload: { ...tempsignUpPayload },
    });
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    this.handleOnchangePhone(status, num, country);
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log(
      "preferredCountries",
      this.state.preferredCountries,
      this.state.inputpayload
    );
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
        mobileValid: false,
      },
      () => {
        console.log(
          "preferredCountries",
          this.state.preferredCountries,
          this.state.inputpayload
        );
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        tempPayload[`preferredCountries`] = this.state.preferredCountries;
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
        if (this.state.isPhoneValid) {
          this.handlePhoneNumberCheck();
        }
        if (!this.state.isPhoneValid) {
          this.setState({
            mobileChecked: false,
            mobileValid: false,
          });
        }
      }
    );
  };

  handlePhoneNumberCheck = () => {
    if (this.state.isPhoneValid) {
      let phoneNumberpayload = {
        phoneNumber:
          this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
      };
      PhoneNumberVerfication(phoneNumberpayload)
        .then((res) => {
          let response = res.data;
          if (response) {
            this.setState(
              {
                mobileChecked: true,
                mobileErrMsg: response.message,
                mobileErrMsgClr: this.handleErrMsgColor(response),
                mobileValid: response.code == 200 ? true : false,
              },
              () => {
                this.checkIfFormValid();
              }
            );
          }
        })
        .catch((err) => {
          this.setState(
            {
              mobileErrMsg: err.message,
              mobileErrMsgClr: `${Red_Color}`,
              mobileValid: false,
            },
            () => {
              this.checkIfFormValid();
            }
          );
        });
    }
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.inputpayload.username &&
      this.state.inputpayload.usernameValid &&
      this.state.isPhoneValid &&
      this.state.mobileValid
        ? true
        : false;

    this.setState({ isFormValid: isFormValid });
  };

  handleErrMsgColor = (response) => {
    switch (response.code) {
      case 200:
        return `${GREEN_COLOR}`;
        break;
      case 409:
        return `${Red_Color}`;
        break;
    }
  };

  handleUserNameCheck = () => {
    if (this.state.username) {
      let userNamepayload = {
        username: this.state.inputpayload.username,
      };
      UserNameVerfication(userNamepayload)
        .then((res) => {
          let response = res.data;
          if (response) {
            let tempPayload = { ...this.state.inputpayload };
            tempPayload.usernameChecked = true;
            tempPayload.usernameErrMsg = response.message;
            tempPayload.usernameErrMsgClr = this.handleErrMsgColor(response);
            tempPayload.usernameValid = response.code == 200 ? true : false;
            this.setState(
              {
                inputpayload: { ...tempPayload },
              },
              () => {
                this.checkIfFormValid();
              }
            );
          }
        })
        .catch((err) => {
          let tempPayload = { ...this.state.inputpayload };
          tempPayload.usernameChecked = true;
          tempPayload.usernameErrMsg = err.message;
          tempPayload.usernameErrMsgClr = `${Red_Color}`;
          tempPayload.usernameValid = false;
          this.setState(
            {
              inputpayload: { ...tempPayload },
            },
            () => {
              this.checkIfFormValid();
            }
          );
        });
    }
  };

  // used to create image payload
  handleUploadFile = (event) => {
    let files = event.target.files;
    if (
      files &&
      files[0] &&
      (files[0].type == "image/jpeg" || files[0].type == "image/png")
    ) {
      if (this.isFileSizeValid(files[0])) {
        let imgPayload = {
          fileDetails: files[0],
          url: URL.createObjectURL(files[0]),
        };

        let tempInputPayload = { ...this.state.inputpayload };
        tempInputPayload.productImgs = { ...imgPayload };

        this.setState(
          {
            fileErr: null,
            isDocValid: true,
            productpic: true,
            ProductImg: { ...imgPayload },
            inputpayload: { ...tempInputPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      } else {
        this.setState(
          {
            fileErr: "File size should be less than 2MB!",
            isDocValid: false,
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    } else {
      this.setState(
        {
          fileErr:
            "File format is invalid! Image should be of .jpeg or .png format",
          isDocValid: false,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    switch (fileData.type.split("/")[0]) {
      case "video":
        return fileData.size <= VIDEO_UPLOAD_SIZE;
        break;
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };

  render() {
    const {
      isFormValid,
      phoneInput,
      profilePicUrl,
      ProductImg,
      productpic,
      mobileChecked,
      mobileValid,
      isPhoneValid,
      mobileErrMsgClr,
      mobileErrMsg,
      inputpayload,
    } = this.state;

    const {
      username,
      usernameChecked,
      usernameErrMsg,
      usernameErrMsgClr,
      usernameValid,
      preferredCountries,
      mobile,
    } = this.state.inputpayload;

    const {
      onClose,
      handleFollowManufacturers,
      handleBacktoPrevScreen,
      activePage,
    } = this.props;

    const { FullName } = this.props.inputpayload;
    return (
      <Wrapper>
        <div className="col-12 AccInfoModel p-0">
          <div className="modelContent">
            <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
            <div className="text-center">
              <h5 className="heading">Account Information</h5>

              <label for="cmpRegImage" className="uploadSec">
                <div className="position-relative">
                  {productpic && ProductImg ? (
                    <div className="ProfileImgSec">
                      <div className="content-overlay">
                        <img src={Camera_Icon} className="cameraIcon"></img>
                      </div>
                      <img src={ProductImg.url} className="newProfileImg"></img>
                    </div>
                  ) : (
                    <div className="dummyPicSec">
                      {profilePicUrl ? (
                        <div className="overlay">
                          <img src={Camera_Icon} className="cameraIcon"></img>
                        </div>
                      ) : (
                        ""
                      )}
                      <img
                        src={
                          profilePicUrl ? profilePicUrl : DummyProfilePic_Icon
                        }
                        className="dummyPic"
                      ></img>
                    </div>
                  )}
                </div>
                <input
                  id="cmpRegImage"
                  type="file"
                  style={{ display: "none" }}
                  onChange={this.handleUploadFile}
                  accept="image/png, image/jpeg"
                />
              </label>
            </div>
            <div className="FormInput">
              <InputWithIcon
                type="text"
                className="SignUpInputBox"
                name="accountName"
                inputAdornment={
                  <img
                    src={Business_Profile_Icon}
                    className="profileIcon"
                  ></img>
                }
                placeholder="Bryan"
                value={FullName}
                autoComplete="off"
              />
            </div>
            <div className="FormInput">
              <div className="position-relative">
                <InputWithIcon
                  type="text"
                  className="SignUpInputBox"
                  name="username"
                  inputAdornment={
                    <img
                      src={Professional_Type_Icon}
                      className="profileIcon"
                    ></img>
                  }
                  placeholder="Username"
                  value={username}
                  onBlur={this.handleUserNameCheck}
                  onChange={this.handleOnchangeInput}
                  autoComplete="off"
                />
                {usernameChecked ? (
                  usernameValid && username ? (
                    <img src={Verified_Icon} className="verifiedIcon"></img>
                  ) : (
                    <img
                      src={NotVerified_Icon}
                      className="notVerifiedIcon"
                    ></img>
                  )
                ) : (
                  ""
                )}
              </div>
              {usernameChecked ? (
                <p className="errMessage" style={{ color: usernameErrMsgClr }}>
                  {usernameErrMsg}
                </p>
              ) : (
                ""
              )}
            </div>
            <div className="PhoneInput">
              <div className="position-relative">
                <IntlTelInput
                  key={phoneInput}
                  preferredCountries={preferredCountries}
                  containerClassName="intl-tel-input"
                  value={mobile}
                  onSelectFlag={this.handleOnchangeflag}
                  onPhoneNumberChange={this.handleOnchangePhone}
                  formatOnInit={false}
                  separateDialCode={true}
                  fieldId="phoneNum"
                  autoComplete="off"
                />
                {mobileChecked ? (
                  mobileValid && isPhoneValid ? (
                    <img src={Verified_Icon} className="verifiedIcon"></img>
                  ) : (
                    <img
                      src={NotVerified_Icon}
                      className="notVerifiedIcon"
                    ></img>
                  )
                ) : (
                  ""
                )}
              </div>
              {isPhoneValid === false ? (
                <p className="errMessage" style={{ color: `${Red_Color}` }}>
                  Phone Number not valid
                </p>
              ) : mobileChecked ? (
                <p className="errMessage" style={{ color: mobileErrMsgClr }}>
                  {mobileErrMsg}
                </p>
              ) : (
                ""
              )}
            </div>
            <div className={!isFormValid ? "inactive_Btn " : "continue_btn"}>
              <ButtonComp
                disabled={!isFormValid}
                onClick={handleFollowManufacturers.bind(this, inputpayload)}
              >
                Continue
              </ButtonComp>
            </div>
          </div>
          <div className="slider-dots">
            <div className="back_btn">
              <ButtonComp
                onClick={handleBacktoPrevScreen.bind(this, activePage)}
              >
                <img src={Long_Arrow_Left_LightGrey} className="backIcon"></img>
                Back
              </ButtonComp>
            </div>
            <div>
              <img src={activePage == 1 ? Active_Dot : Inactive_Dot}></img>
              <img
                src={activePage == 2 ? Active_Dot : Inactive_Dot}
                className="mx-2"
              ></img>
              <img src={activePage == 3 ? Active_Dot : Inactive_Dot}></img>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .dummyPicSec {
              width: 6.954vw;
              height: 6.954vw;
              cursor: pointer;
              border-radius: 50% !important;
              position: relative;
              margin: 0 auto;
            }
            .ProfileImgSec {
              width: 6.954vw;
              height: 6.954vw;
              position: relative;
              margin: 0 auto;
              border-radius: 50% !important;
            }
            .cameraIcon {
              width: 1.098vw !important;
              object-fit: cover !important;
              margin-bottom: 0.219vw;
              height: unset !important;
              border-radius: none !important;
            }
            .content-overlay {
              background: rgba(255, 255, 255, 0.8);
              position: absolute;
              width: 100%;
              left: 0;
              top: 65%;
              bottom: 0;
              cursor: pointer;
              right: 0;
              opacity: 0;
              -webkit-transition: all 0.4s ease-in-out 0s;
              -moz-transition: all 0.4s ease-in-out 0s;
              transition: all 0.4s ease-in-out 0s;
              z-index: 1;
            }
            .overlay {
              background: rgba(255, 255, 255, 0.8);
              position: absolute;
              width: 100%;
              left: 0;
              top: 65%;
              bottom: 0;
              cursor: pointer;
              right: 0;
              opacity: 0;
              -webkit-transition: all 0.4s ease-in-out 0s;
              -moz-transition: all 0.4s ease-in-out 0s;
              transition: all 0.4s ease-in-out 0s;
              z-index: 1;
            }
            .ProfileImgSec:hover .content-overlay {
              opacity: 1;
            }
            .dummyPicSec:hover .overlay {
              opacity: 1;
            }
            .newProfileImg,
            .dummyPicSec .dummyPic {
              width: 100%;
              height: 100%;
              // cursor: pointer;
              object-fit: cover;
              object-position: top;
              border-radius: 50% !important;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .modelContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.292vw;
              padding: 1.464vw 1.83vw;
            }
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .AccInfoModel {
              width: 24.158vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .errMessage {
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.585vw 0 0 0;
              text-transform: capitalize;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.756vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .dummyDp {
              width: 7.32vw;
              object-fit: cover;
            }
            .FormInput {
              margin: 1.098vw 0;
            }
            .notVerifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            :global(.AccInfoModel .SignUpInputBox) {
              width: 100%;
              height: 2.489vw;
              padding: 0.439vw 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${BLACK_COLOR};
            }
            :global(.AccInfoModel .SignUpInputBox input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.AccInfoModel .SignUpInputBox input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            :global(.AccInfoModel .SignUpInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.AccInfoModel .SignUpInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.AccInfoModel .PhoneInput) {
              position: relative;
            }
            :global(.AccInfoModel .PhoneInput > div > div) {
              width: 100%;
              display: flex;
              border: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.489vw;
              padding: 0;
            }
            :global(.AccInfoModel .PhoneInput > div > div > input) {
              padding: 0.366vw 0;
              width: 100%;
              height: 100%;
              line-height: 1 !important;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border-radius: none;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              padding: 0 0.146vw !important;
              margin-left: 25% !important;
            }
            :global(.AccInfoModel .PhoneInput > div > div > input:focus) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.AccInfoModel .PhoneInput > div > div div .country-list) {
              width: 100%;
              margin: 0.219vw 0 0 0;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .country-list
                .country) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
            }
            :global(.AccInfoModel .PhoneInput > div > div > input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container) {
              height: 2.489vw;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.AccInfoModel .PhoneInput > div > div div .selected-flag) {
              background: ${WHITE_COLOR} !important;
              height: 100%;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus),
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag:active) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code) {
              font-size: 0.878vw;
              height: 2.342vw;
              align-items: center;
              display: flex;
              padding: 0;
              line-height: 1 !important;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code::before) {
              content: url(${Phone_Outline_Icon_Grey});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus) {
              box-shadow: none !important;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.AccInfoModel .PhoneInput > div > div div .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-flag) {
              display: none;
            }
            :global(.AccInfoModel
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-arrow) {
              display: none;
            }
            :global(.AccInfoModel .PhoneInput > div > div div:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            .continue_btn,
            .inactive_Btn {
              margin: 1.83vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              border-radius: 0.146vw;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default AccountInfo;
