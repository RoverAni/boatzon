import React, { Component } from "react";
import { connect } from "react-redux";
import Wrapper from "../../../hoc/Wrapper";
import {
  Close_Icon,
  Search_Light_Grey_Icon,
  Close_Grey_Thin_Icon,
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_1,
  THEME_COLOR,
  GREY_VARIANT_3,
  GREEN_COLOR,
  GREY_VARIANT_10,
  GREY_VARIANT_12,
  BG_LightGREY_COLOR,
  FONTGREY_COLOR_Dark,
  Long_Arrow_Left_LightGrey,
  Active_Dot,
  Inactive_Dot,
  GREY_VARIANT_2,
  Long_Arrow_Right_Green,
  Contender_Logo,
} from "../../../lib/config";
import InputBox from "../../../components/input-box/input-box";
import ButtonComp from "../../../components/button/button";
import CircularProgressButton from "../../../components/button-loader/button-loader";
import Pagination from "../../../hoc/divPagination";
import PageBeatLoader from "../../../components/loader/beat-loader";
import debounce from "lodash.debounce";
import PageLoader from "../../../components/loader/page-loader";

class FollowManufacturers extends Component {
  state = {
    inputpayload: {
      ManufactureNamesList: [],
    },
  };

  componentDidMount() {
    let payload = {
      ManufactureNamesList: [],
      ...this.props.inputpayload,
    };
    this.setState({
      inputpayload: { ...payload },
    });
  }

  handleSelectManufacturer = (data, index) => {
    let tempPayload = { ...this.state.inputpayload };
    if (tempPayload.ManufactureNamesList.includes(data.manufactureName)) {
      // tempPayload = tempPayload.filter((e) => e !== data.manufacturer_Name);
    } else {
      tempPayload.ManufactureNamesList.push(data.manufactureName);
    }
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  handleRemoveManufacturer = (data) => {
    let tempPayload = { ...this.state.inputpayload };
    tempPayload.ManufactureNamesList = tempPayload.ManufactureNamesList.filter(
      (e) => e != data
    );
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  handleSearchManufacturer = (event) => {
    let inputControl = event.target;
    this.setState(
      {
        searchKey: inputControl.value,
      },
      () => {
        this.searchAPICall(this.state.searchKey);
      }
    );
  };

  searchAPICall = debounce((value) => {
    this.props.handlePopularManufacturerList(5, 0, value, false, true);
  }, 500);

  getList = async (page = 0) => {
    return new Promise(async (res, rej) => {
      try {
        await this.props.handlePopularManufacturerList(
          5,
          page,
          this.state.searchKey == "" ? undefined : this.state.searchKey,
          true
        );
        res();
      } catch (e) {
        rej();
      }
    });
  };

  render() {
    const {
      activePage,
      apiLoading,
      handleBacktoPrevScreen,
      handleUploadPic,
      onClose,
      popularManufacturerList,
      paginationLoader,
      searchLoader,
    } = this.props;
    const { inputpayload, searchKey } = this.state;
    const { handleSearchManufacturer, getList } = this;
    return (
      <Wrapper>
        <div className="col-12 followManufacturerModel p-0">
          <div className="modelContent">
            <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
            <div className="text-center">
              <h5 className="heading">Follow Your Favorite Boats</h5>
              <p className="caption">
                Do you own a boat or would like to follow a boat manufacturer?
              </p>
              <p className="caption" style={{ maxWidth: "92%" }}>
                Every time a boat or product is posted for a boat manufacturer
                you follow, you can easily get alerts and view these posts in
                the My Boats sections.
              </p>
            </div>
            <div className="businessContent">
              <div className="FormInput">
                <ul className="list-unstyled SelectedList">
                  <li>
                    <img
                      src={Search_Light_Grey_Icon}
                      className="searchIcon"
                    ></img>
                  </li>
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="ManufactureNamesList"
                    placeholder="Search Manufacturer"
                    onChange={handleSearchManufacturer}
                    autoComplete="off"
                    value={searchKey}
                  ></InputBox>
                  {inputpayload.ManufactureNamesList &&
                    inputpayload.ManufactureNamesList.length > 0 &&
                    inputpayload.ManufactureNamesList.map((data) => (
                      <li className="Chip">
                        <span>{data}</span>
                        <img
                          src={Close_Grey_Thin_Icon}
                          onClick={this.handleRemoveManufacturer.bind(
                            this,
                            data
                          )}
                        ></img>
                      </li>
                    ))}
                  <li></li>
                </ul>
              </div>
              {searchLoader ? (
                <div className="text-center py-5">
                  <PageLoader size={10} loading={searchLoader} />
                </div>
              ) : popularManufacturerList &&
                popularManufacturerList.length > 0 ? (
                <>
                  <div
                    className="row m-0 manufacturerListSec"
                    id="manufacturerListSec"
                  >
                    <Pagination
                      id={"manufacturerListSec"}
                      // elementRef={this.divRef.current}
                      items={popularManufacturerList}
                      getItems={getList}
                    ></Pagination>
                    {popularManufacturerList.map((data, index) => (
                      <div className="col-4 p-0 logoColumn" key={index}>
                        <div className="text-center position-relative">
                          <div
                            className={
                              inputpayload &&
                              inputpayload.ManufactureNamesList.length > 0 &&
                              inputpayload.ManufactureNamesList.includes(
                                data.manufactureName
                              )
                                ? "logoDiv activeLogoDiv"
                                : "logoDiv"
                            }
                            onClick={this.handleSelectManufacturer.bind(
                              this,
                              data,
                              index
                            )}
                          >
                            <img
                              src={data.imageUrl || Contender_Logo}
                              className="logoImg"
                            ></img>
                          </div>
                          {inputpayload &&
                          inputpayload.ManufactureNamesList.length > 0 &&
                          inputpayload.ManufactureNamesList.includes(
                            data.manufactureName
                          ) ? (
                            <img
                              src={Close_Grey_Thin_Icon}
                              className="closeIcon"
                              onClick={this.handleRemoveManufacturer.bind(
                                this,
                                data.manufactureName
                              )}
                            ></img>
                          ) : (
                            ""
                          )}
                          <p className="manufacturerName">
                            {data.manufactureName}
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                  {paginationLoader && (
                    <div className="text-center">
                      <PageBeatLoader size={10} loading={paginationLoader} />
                    </div>
                  )}
                </>
              ) : (
                <div className="py-3 text-center">
                  <p className="noManufacturerMsg">No Manufacturers Found</p>
                </div>
              )}
              <div className="row m-0 justify-content-center">
                <div className="col-5 mx-aut0">
                  <div className="continue_btn">
                    <CircularProgressButton
                      buttonText={"Continue"}
                      onClick={handleUploadPic.bind(this, inputpayload)}
                      loading={apiLoading}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="slider-dots">
            <div className="back_btn">
              <ButtonComp
                onClick={handleBacktoPrevScreen.bind(this, activePage)}
              >
                <img src={Long_Arrow_Left_LightGrey} className="backIcon"></img>
                Back
              </ButtonComp>
            </div>
            <div>
              <img src={activePage == 1 ? Active_Dot : Inactive_Dot}></img>
              <img
                src={activePage == 2 ? Active_Dot : Inactive_Dot}
                className="mx-2"
              ></img>
              <img src={activePage == 3 ? Active_Dot : Inactive_Dot}></img>
            </div>
            <div className="skip_btn">
              <ButtonComp onClick={handleUploadPic.bind(this, inputpayload)}>
                Skip
                <img src={Long_Arrow_Right_Green} className="skipIcon"></img>
              </ButtonComp>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.noManufacturerMsg) {
              max-width: 80%;
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            :global(.MuiPaper-root) {
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .skip_btn {
              position: absolute;
              right: 0;
            }
            .skipIcon {
              width: 0.951vw !important;
              margin-left: 0.658vw !important;
              transform: rotate(180deg);
            }
            :global(.skip_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREEN_COLOR};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.skip_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.skip_btn button:hover) {
              background: transparent;
            }
            :global(.skip_btn button:focus),
            :global(.skip_btn button:active) {
              background: transparent;
              outline: none;
            }
            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .modelContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.292vw;
              padding: 1.464vw 1.83vw;
            }
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .followManufacturerModel {
              width: 34.407vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.756vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .caption {
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              margin: 0.585vw auto 0 auto;
              max-width: 85%;
            }
            .businessContent {
              background: ${WHITE_COLOR};
              //   box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              //   padding: 0.732vw;
              margin: 0.732vw 0 0 0;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            :global(.followManufacturerModel .inputBox) {
              width: fit-content;
              height: 2.489vw;
              padding: 0 0 0.292vw 0;
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.followManufacturerModel .inputBox:focus),
            :global(.followManufacturerModel .inputBox:active) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_3};
            }
            .logoColumn {
              padding-right: 0.951vw !important;
            }
            .searchIcon {
              width: 0.878vw;
              margin: 0 0.366vw 0.292vw 0;
            }
            .FormInput {
              margin: 0 0 1.098vw 0;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .logoDiv {
              background: #fff;
              border: 0.146vw solid ${GREY_VARIANT_1};
              display: flex;
              align-items: center;
              justify-content: center;
              height: 4.758vw;
              border-radius: 0.219vw;
              cursor: pointer;
              position: relative;
            }
            .activeLogoDiv {
              border: 0.146vw solid ${GREEN_COLOR};
            }
            .logoImg {
              width: 100%;
              height: 100%;
              background-blend-mode: multiply;
              object-fit: contain;
            }
            .manufacturerName {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin: 0.292vw 0 0.585vw 0;
            }
            .manufacturerListSec {
              height: 16.301vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .SelectedList {
              display: flex;
              align-items: center;
              flex-wrap: wrap;
              margin: 0;
              width: fit-content;
              max-height: 4.538vw;
              overflow: hidden;
            }
            .SelectedList .Chip {
              background: ${GREY_VARIANT_3};
              display: flex;
              align-items: center;
              width: fit-content;
              margin-right: 0.366vw;
              padding: 0.439vw 0.658vw;
              margin-bottom: 0.366vw;
              cursor: context-menu;
            }
            .SelectedList .Chip span {
              width: fit-content;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              font-size: 0.805vw;
              line-height: 1;
            }
            .SelectedList .Chip img {
              width: 0.951vw;
              padding: 0.146vw;
              margin-bottom: -0.146vw;
              margin-left: 0.366vw;
              cursor: pointer;
            }
            .SelectedList .Chip img:hover {
              background: ${GREY_VARIANT_10};
            }
            .closeIcon {
              position: absolute;
              top: 0.439vw;
              right: 0.439vw;
              width: 0.951vw;
              cursor: pointer;
              padding: 0.146vw;
            }
            .closeIcon:hover {
              background: ${GREY_VARIANT_12};
            }
            .continue_btn {
              margin: 15px 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.continue_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
    paginationLoader: state.paginationLoader,
    popularManufacturerList: state.popularManufacturerList,
    searchLoader: state.searchLoader,
  };
};

export default connect(mapStateToProps)(FollowManufacturers);
