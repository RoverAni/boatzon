import React, { Component } from "react";
import Wrapper from "../../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  Successful_SignUp,
  GREY_VARIANT_2,
  FONTGREY_COLOR_Dark,
  GREEN_COLOR,
  Close_Icon,
} from "../../../lib/config";
import ButtonComp from "../../../components/button/button";
import { withRouter } from "next/router";

class SuccessfullSignupModel extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.onClose();
      this.props.router.reload();
    }, 2500);
  }
  render() {
    return (
      <Wrapper>
        <div className="SignupModel p-0">
          <div className="modelContent">
            <img
              src={Close_Icon}
              className="closeIcon"
              onClick={this.props.onClose}
            ></img>
            <div className="addressContent">
              <div className="text-center">
                <img src={Successful_SignUp} className="successfullLogo"></img>
                <h6 className="heading">Congratulations!</h6>
                <p className="desc">You are all set!</p>
              </div>
            </div>
            <div className="redirect_btn">
              <ButtonComp>Complete</ButtonComp>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .modelContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.292vw;
              padding: 1.464vw 1.83vw;
            }
            .SignupModel {
              width: 24.158vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .addressContent {
              background: ${WHITE_COLOR};
              //   box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
              border-radius: 0.292vw;
              //   padding: 35px 20px;
              margin: 1.098vw 0 0 0;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.317vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0.732vw 0 0 0;
            }
            .desc {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              margin: 0 auto;
              max-width: 85%;
            }
            .successfullLogo {
              width: 38%;
            }
            .redirect_btn {
              margin: 3.294vw 0 0 0;
            }
            :global(.redirect_btn button) {
              width: 100%;
              padding: 0.292vw 1.098vw;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.redirect_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              font-weight: 600;
            }
            :global(.redirect_btn button:focus),
            :global(.redirect_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.redirect_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withRouter(SuccessfullSignupModel);
