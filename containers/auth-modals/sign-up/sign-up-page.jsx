// Main React Components
import React, { Component } from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../../../hoc/Wrapper";
import {
  Close_Icon,
  Email_Icon,
  BLACK_COLOR,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREY_VARIANT_2,
  Password_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_1,
  GREY_VARIANT_5,
  Google_Icon,
  Facebook_Icon,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  Active_Dot,
  Inactive_Dot,
  Facebook_Blue,
  GREY_VARIANT_1,
  ProfileName_Icon,
  FONTGREY_COLOR_Dark,
  FB_APP_ID,
  Web_deviceType,
  Noraml_Signup_Type,
  Google_Signup_Type,
  Facebook_Signup_Type,
  Red_Color,
  Verified_Icon,
  NotVerified_Icon,
  IMAGE_UPLOAD_SIZE,
  Info_Icon_Red,
  BG_LightGREY_COLOR,
  View_Grey_Filled_Icon,
  View_Grey_UnFilled_Icon,
} from "../../../lib/config";
import {
  EMAIL,
  PASSWORD,
  FULL_NAME,
} from "../../../lib/input-control-data/input-definitions";
import {
  requiredValidator,
  PureTextValidator,
} from "../../../lib/validation/validation";
import InputWithIcon from "../../../components/input-box/input-with-icon";
import ButtonComp from "../../../components/button/button";
import { FormControlLabel, Checkbox } from "@material-ui/core";
import CheckBox from "../../../components/input-box/check-box";
import LoginPage from "../login/login-page";
import AccountInfo from "./account-info";
import FollowManufacturers from "./follow-manufacturers";
import SuccessfullSignUpModel from "./successfull-signup-model";
import {
  EmailVerfication,
  PhoneNumberVerfication,
  UserNameVerfication,
  registerUser,
} from "../../../services/auth";
import Snackbar from "../../../components/snackbar";
import {
  setupToken,
  setupUserId,
  setupMongoId,
  setupMqttId,
} from "../../../lib/data-handler/data-handler";
import {
  getManufactures,
  postfollowManufactures,
} from "../../../services/manufacturer";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login";
import { getDeviceId } from "../../../lib/fingerprint/fingerprint";
import VerifyOtp from "./verify-otp";
import { requestForOTP, verifyOTP } from "../../../services/verify-otp";
import InfoTooltip from "../../../components/tooltip/info-tooltip";
import { __BlobUpload } from "../../../lib/image/cloudinaryUpload";
import { setApiLoading, setReduxState } from "../../../redux/actions/auth";
import { isEmpty } from "../../../lib/global";

class SignUpPage extends Component {
  state = {
    inputpayload: {
      preferredCountries: ["in"],
    },
    isFormValid: false,
    isRemembered: false,
    activePage: 1,
    open: false,
    verifyloading: false,
    passView: false,
    popularManufacturerList: [],
    hasMore: true,
  };

  setValues = () => {
    this.setState({
      email: this.props.signUpData && this.props.signUpData.Email,
      fullName: this.props.signUpData && this.props.signUpData.FullName,
      password: this.props.signUpData && this.props.signUpData.Password,
    });
  };

  componentDidMount = () => {
    this.props.dispatch(setApiLoading(false));
  };

  // Function for User inputpayload
  handleOnchangeInput = (event) => {
    if (event.target.name == "Email") {
      this.setState({
        emailChecked: false,
        emailErrMsg: "",
        emailErrMsgClr: "",
        emailValid: false,
      });
    }
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] =
      inputControl.value && inputControl.value.length ? inputControl.value : "";
    this.setState({
      inputpayload: { ...tempsignUpPayload },
    });
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.emailValid &&
      this.state[PASSWORD] === 1 &&
      this.state[FULL_NAME]
        ? true
        : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Function to redirect to login modal
  handleLoginModel = () => {
    this.updateScreen(<LoginPage onClose={this.props.onClose} />);
  };

  handleBacktoPrevScreen = (activePage) => {
    switch (activePage) {
      case 2:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen();
          }
        );
        break;
      case 3:
        this.setState(
          {
            activePage: this.state.activePage - 1,
          },
          () => {
            this.updateScreen(
              <AccountInfo
                onClose={this.props.onClose}
                activePage={this.state.activePage}
                inputpayload={this.state.inputpayload}
                handleBacktoPrevScreen={this.handleBacktoPrevScreen}
                handleFollowManufacturers={this.handleFollowManufacturers}
                currentLocation={this.props.currentLocation}
              />
            );
          }
        );
        break;
    }
  };

  handleBacktoAccountInfo = (activePage) => {
    this.updateScreen(
      <AccountInfo
        onClose={this.props.onClose}
        activePage={this.state.activePage}
        inputpayload={this.state.inputpayload}
        handleBacktoPrevScreen={this.handleBacktoPrevScreen}
        handleFollowManufacturers={this.handleFollowManufacturers}
        currentLocation={this.props.currentLocation}
      />
    );
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 409:
        return "warning";
        break;
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      case 422:
        return "error";
        break;
    }
  };

  handleErrMsgColor = (response) => {
    switch (response.code) {
      case 200:
        return `${GREEN_COLOR}`;
        break;
      case 409:
        return `${Red_Color}`;
        break;
    }
  };

  handleEmailCheck = () => {
    if (this.state[EMAIL] == 1) {
      let payload = {
        email: this.state.inputpayload.Email || this.state.inputpayload.email,
      };
      EmailVerfication(payload)
        .then((res) => {
          let response = res.data;
          if (response) {
            this.setState(
              {
                emailChecked: true,
                emailErrMsg: response.message,
                emailErrMsgClr: this.handleErrMsgColor(response),
                emailValid: response.code == 200 ? true : false,
              },
              () => {
                this.checkIfFormValid();
              }
            );
          }
        })
        .catch((err) => {
          this.setState(
            {
              emailErrMsg: err.message,
              emailErrMsgClr: `${Red_Color}`,
              emailValid: false,
            },
            () => {
              this.checkIfFormValid();
            }
          );
        });
    }
  };

  // Function to redirect to acc-info modal
  handleAccountInfo = () => {
    this.setState(
      {
        activePage: this.state.activePage + 1,
      },
      () => {
        this.updateScreen(
          <AccountInfo
            onClose={this.props.onClose}
            activePage={this.state.activePage}
            inputpayload={this.state.inputpayload}
            handleBacktoPrevScreen={this.handleBacktoPrevScreen}
            handleFollowManufacturers={this.handleFollowManufacturers}
            currentLocation={this.props.currentLocation}
          />
        );
      }
    );
  };

  handleFollowManufacturers = (inputpayload) => {
    this.setState(
      {
        inputpayload: { ...inputpayload },
      },
      () => {
        let payload = {
          deviceId: getDeviceId(),
          phoneNumber:
            this.state.inputpayload.countryCode +
            this.state.inputpayload.mobile,
        };
        requestForOTP(payload)
          .then((res) => {
            console.log("res", res);
          })
          .catch((err) => {
            console.log("err", err);
          });
        this.updateScreen(
          <VerifyOtp
            onClose={this.props.onClose}
            activePage={this.state.activePage}
            inputpayload={this.state.inputpayload}
            handleBacktoPrevScreen={this.handleBacktoPrevScreen}
            handleBacktoAccountInfo={this.handleBacktoAccountInfo}
            handleVerifyOtp={this.handleVerifyOtp}
            handleResendOtp={this.handleResendOtp}
            otpTimer={this.state.otpTimer}
            currentLocation={this.props.currentLocation}
            verifyloading={this.state.verifyloading}
            handlePopularManufacturerList={this.handlePopularManufacturerList}
          />
        );
      }
    );
  };

  handleVerifyOtp = (otp) => {
    this.setState({
      verifyloading: true,
    });
    let payload = {
      deviceId: getDeviceId(),
      otp: otp,
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };

    verifyOTP(payload)
      .then((res) => {
        console.log("res", res);
        this.setState(
          {
            activePage: this.state.activePage + 1,
            verifyloading: false,
          },
          () => {
            this.updateScreen(
              <FollowManufacturers
                onClose={this.props.onClose}
                activePage={this.state.activePage}
                inputpayload={this.state.inputpayload}
                handleBacktoPrevScreen={this.handleBacktoPrevScreen}
                handleUploadPic={this.handleUploadPic}
                handlePopularManufacturerList={
                  this.handlePopularManufacturerList
                }
                hasMore={this.state.hasMore}
              />
            );
          }
        );
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          verifyloading: false,
        });
      });
  };

  handlePopularManufacturerList = (
    limit = 5,
    page = 0,
    search,
    paginationLoader = false,
    searchLoader = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        if (this.state.hasMore || search) {
          let payload = {
            type: "4", //0-all manufactureres,1-boats and products,2-engines,3-trailers,4-popular manufactures
            limit: limit,
            offset: page * limit,
            searchKey: search,
          };
          this.setState(
            {
              paginationLoader,
              searchLoader,
            },
            () => {
              this.props.dispatch(
                setReduxState("paginationLoader", this.state.paginationLoader)
              );
              this.props.dispatch(
                setReduxState("searchLoader", this.state.searchLoader)
              );
            }
          );
          let result = await getManufactures(payload);
          let response = result.data;
          console.log("fkfok--res", response, search);
          let oldList = [...this.state.popularManufacturerList];
          let newList = response && response.data ? response.data : [];
          if (response.code == 200) {
            this.setState(
              {
                popularManufacturerList:
                  search != undefined ? [...newList] : [...oldList, ...newList],
                paginationLoader: false,
                searchLoader: false,
                hasMore:
                  this.state.popularManufacturerList.length < response.count,
              },
              () => {
                this.props.dispatch(
                  setReduxState(
                    "popularManufacturerList",
                    this.state.popularManufacturerList
                  )
                );
                this.props.dispatch(
                  setReduxState("searchLoader", this.state.searchLoader)
                );
                this.props.dispatch(
                  setReduxState("paginationLoader", this.state.paginationLoader)
                );
              }
            );
          }
          if (response.code == 204) {
            this.setState(
              {
                paginationLoader: false,
                popularManufacturerList:
                  search != undefined ? [...newList] : [...oldList],
                searchLoader: false,
              },
              () => {
                this.props.dispatch(
                  setReduxState(
                    "popularManufacturerList",
                    this.state.popularManufacturerList
                  )
                );
                this.props.dispatch(
                  setReduxState("searchLoader", this.state.searchLoader)
                );
                this.props.dispatch(
                  setReduxState("paginationLoader", this.state.paginationLoader)
                );
              }
            );
            return rej();
          }
          res();
        }
      } catch (err) {
        console.log("fkfok--err", err);
        this.setState(
          {
            popularManufacturerList: [],
            paginationLoader: false,
            searchLoader: false,
          },
          () => {
            this.props.dispatch(
              setReduxState(
                "popularManufacturerList",
                this.state.popularManufacturerList
              )
            );
            this.props.dispatch(
              setReduxState("searchLoader", this.state.searchLoader)
            );
            this.props.dispatch(
              setReduxState("paginationLoader", this.state.paginationLoader)
            );
          }
        );
      }
    });
  };

  // used to upload image from local storage
  handleUploadPic = (finalpayload) => {
    this.props.dispatch(setApiLoading(true));
    if (finalpayload.productImgs && !isEmpty(finalpayload.productImgs)) {
      __BlobUpload(finalpayload.productImgs.fileDetails)
        .then((res) => {
          console.log("feduf-->image upload success", res);
          this.setState(
            {
              profilePicUrl: res.body.secure_url,
            },
            () => {
              this.handleSuccessfullSignup(finalpayload);
            }
          );
        })
        .catch((err) => {
          console.log("feduf-->err uploading image", err);
          this.setState({
            usermessage: "Err uploading image",
            variant: "error",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        });
    } else {
      this.handleSuccessfullSignup(finalpayload);
    }
  };

  handleSuccessfullSignup = (finalpayload) => {
    const { profilePicUrl } = this.state;
    let apiPayload;
    if (finalpayload.signupType == Google_Signup_Type) {
      apiPayload = {
        signupType: finalpayload.signupType,
        deviceType: finalpayload.deviceType,
        googleId: finalpayload.googleId,
        email: finalpayload.Email,
        fullName: finalpayload.FullName,
        profilePicUrl: profilePicUrl
          ? profilePicUrl
          : finalpayload.profilePicUrl,
        accessToken: finalpayload.accessToken,
        googleToken: finalpayload.googleToken,
        password: finalpayload.Password,
        username: finalpayload.username,
        phoneNumber: finalpayload.countryCode + finalpayload.mobile,
        countryCode: finalpayload.countryCode,
        deviceId: getDeviceId(),
        longitude:
          this.props.currentLocation && this.props.currentLocation.longitude,
        latitude:
          this.props.currentLocation && this.props.currentLocation.latitude,
        city: this.props.currentLocation && this.props.currentLocation.city,
        countrySname:
          this.props.currentLocation &&
          this.props.currentLocation.countryShortName,
        location:
          this.props.currentLocation && this.props.currentLocation.address,
      };
      delete apiPayload.mobile;
      delete apiPayload.ManufactureNamesList;
      delete apiPayload.Password;
    } else if (finalpayload.signupType == Facebook_Signup_Type) {
      apiPayload = {
        email: finalpayload.Email,
        fullName: finalpayload.FullName,
        signupType: finalpayload.signupType,
        deviceType: finalpayload.deviceType,
        profilePicUrl: profilePicUrl
          ? profilePicUrl
          : finalpayload.profilePicUrl
          ? finalpayload.profilePicUrl
          : "",
        facebookId: finalpayload.facebookId,
        accessToken: finalpayload.accessToken,
        password: finalpayload.Password,
        username: finalpayload.username,
        phoneNumber: finalpayload.countryCode + finalpayload.mobile,
        countryCode: finalpayload.countryCode,
        deviceId: getDeviceId(),
        longitude:
          this.props.currentLocation && this.props.currentLocation.longitude,
        latitude:
          this.props.currentLocation && this.props.currentLocation.latitude,
        city: this.props.currentLocation && this.props.currentLocation.city,
        countrySname:
          this.props.currentLocation &&
          this.props.currentLocation.countryShortName,
        location:
          this.props.currentLocation && this.props.currentLocation.address,
      };
      delete apiPayload.ManufactureNamesList;
      delete apiPayload.Password;
    } else {
      apiPayload = {
        signupType: Noraml_Signup_Type,
        deviceType: Web_deviceType,
        username: finalpayload.username,
        password: finalpayload.Password,
        phoneNumber: finalpayload.countryCode + finalpayload.mobile,
        countryCode: finalpayload.countryCode,
        fullName: finalpayload.FullName,
        email: finalpayload.Email,
        deviceId: getDeviceId(),
        longitude:
          this.props.currentLocation && this.props.currentLocation.longitude,
        latitude:
          this.props.currentLocation && this.props.currentLocation.latitude,
        city: this.props.currentLocation && this.props.currentLocation.city,
        countrySname:
          this.props.currentLocation &&
          this.props.currentLocation.countryShortName,
        location:
          this.props.currentLocation && this.props.currentLocation.address,
        profilePicUrl: profilePicUrl,
      };
    }
    registerUser(apiPayload)
      .then((res) => {
        let response = res.data.response;
        console.log("fjwi", response);
        if (response) {
          this.setState({
            usermessage:
              response.code == 200
                ? "Registation Successfull"
                : response.message,
            variant:
              response.code == 200
                ? this.handleSnackbar(response)
                : res.data.code == 200
                ? "success"
                : "error",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          setupToken(response.authToken);
          setupUserId(response.userId);
          setupMongoId(response.mongoId);
          setupMqttId(response.mqttId);

          if (
            finalpayload.ManufactureNamesList &&
            finalpayload.ManufactureNamesList.length > 0
          ) {
            let manufactureIdsArr = this.state.popularManufacturerList
              .filter((e) =>
                finalpayload.ManufactureNamesList.includes(e.manufactureName)
              )
              .map((obj) => obj.manufactureId);
            let payload = {
              manufacture: manufactureIdsArr.toString(),
            };
            postfollowManufactures(payload)
              .then((result) => {
                this.setState({
                  usermessage: result.data.message,
                  variant: this.handleSnackbar(result.data),
                  open: true,
                  vertical: "bottom",
                  horizontal: "left",
                });
                this.props.dispatch(setApiLoading(false));
              })
              .catch((error) => {
                this.setState({
                  usermessage: error.response.data.message,
                  variant: "error",
                  open: true,
                  vertical: "bottom",
                  horizontal: "left",
                });
                this.props.dispatch(setApiLoading(false));
              });
          } else {
            this.props.dispatch(setApiLoading(false));
          }
          this.updateScreen(
            <SuccessfullSignUpModel onClose={this.props.onClose} />
          );
        }
      })
      .catch((err) => {
        this.props.dispatch(setApiLoading(false));
        this.setState({
          usermessage: "Something Went Wrong",
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };
  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  googleResponseOnSuccess = (response) => {
    let googleResponse = response;
    let googleLoginPayload = {
      signupType: Google_Signup_Type,
      deviceType: Web_deviceType,
      googleId: googleResponse.googleId,
      Email: googleResponse.profileObj.email,
      FullName: googleResponse.profileObj.name,
      profilePicUrl: googleResponse.profileObj.imageUrl,
      accessToken: googleResponse.accessToken,
      googleToken: googleResponse.tokenId,
    };
    this.setState(
      {
        inputpayload: googleLoginPayload,
        Email: 1,
        FullName: googleResponse.profileObj.name,
      },
      () => {
        this.handleEmailCheck();
      }
    );
  };

  googleResponseOnFailure = (error) => {
    this.setState({
      usermessage: error.error,
      variant: "error",
      open: true,
      vertical: "bottom",
      horizontal: "left",
    });
  };

  responseFacebook = (response) => {
    let facebookResponse = response;
    console.log("fns", facebookResponse);
    if (facebookResponse.status != "unknown") {
      let facebookLoginPayload = {
        signupType: Facebook_Signup_Type,
        deviceType: Web_deviceType,
        // profilePicUrl: facebookResponse.profilePicUrl,
        facebookId: facebookResponse.userID,
        Email: facebookResponse.email,
        FullName: facebookResponse.name,
        accessToken: facebookResponse.accessToken,
      };
      this.setState(
        {
          inputpayload: { ...facebookLoginPayload },
          Email: 1,
          FullName: facebookResponse.name,
        },
        () => {
          this.handleEmailCheck();
        }
      );
    } else {
      this.setState({
        usermessage: "Fb Login Model Error",
        variant: "error",
        open: true,
        vertical: "bottom",
        horizontal: "left",
      });
    }
  };

  handlePasswordView = () => {
    this.setState({
      passView: !this.state.passView,
    });
  };

  render() {
    const {
      isFormValid,
      isRemembered,
      activePage,
      emailChecked,
      emailValid,
      inputpayload,
      emailErrMsgClr,
      emailErrMsg,
      passView,
      currentScreen,
      variant,
      usermessage,
      open,
      vertical,
      horizontal,
    } = this.state;
    const { FullName, Email, Password } = { ...this.state.inputpayload };
    const { onClose } = this.props;
    const passHits = (
      <div className="passHitsContent">
        <ol className="m-0 pl-2">
          <li>Your password must be at least 8 characters</li>
          <li>Must have at least 1 number</li>
          <li>Must have at least 1 upper case and 1 lower case character</li>
          <li>Must have at least 1 special character</li>
        </ol>
      </div>
    );
    const {
      setValues,
      handleEmailCheck,
      handleOnchangeInput,
      handlePasswordView,
      handleAccountInfo,
      googleResponseOnSuccess,
      googleResponseOnFailure,
      responseFacebook,
      handleLoginModel,
      handleSnackbarClose,
    } = this;
    return (
      <Wrapper className="position-relative">
        {!currentScreen ? (
          <div onLoad={setValues} className="col-12 SignUpModel p-0">
            <div className="modelContent">
              <img
                src={Close_Icon}
                className="closeIcon"
                onClick={onClose}
              ></img>
              <h5 className="heading">Sign Up</h5>
              <div className="FormInput">
                <div className="position-relative">
                  <InputWithIcon
                    autoFocus
                    type="email"
                    className="SignUpInputBox form-control"
                    name={EMAIL}
                    inputAdornment={
                      <img src={Email_Icon} className="emailIcon"></img>
                    }
                    value={Email}
                    placeholder="Enter Your Email"
                    onBlur={handleEmailCheck}
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  />
                  {emailChecked ? (
                    emailValid && this.state.Email == 1 ? (
                      <img src={Verified_Icon} className="verifiedIcon"></img>
                    ) : (
                      <img
                        src={NotVerified_Icon}
                        className="notVerifiedIcon"
                      ></img>
                    )
                  ) : (
                    ""
                  )}
                </div>
                {inputpayload.hasOwnProperty("Email") &&
                !isEmpty(inputpayload.Email) ? (
                  this.state.Email == 0 || this.state.Email == 2 ? (
                    <p className="errMessage" style={{ color: `${Red_Color}` }}>
                      Enter Valid Email
                    </p>
                  ) : (
                    ""
                  )
                ) : (
                  ""
                )}
                {this.state.Email == 1 ? (
                  emailChecked ? (
                    <p className="errMessage" style={{ color: emailErrMsgClr }}>
                      {emailErrMsg}
                    </p>
                  ) : (
                    ""
                  )
                ) : (
                  ""
                )}
              </div>
              <div className="FormInput">
                <InputWithIcon
                  type="text"
                  className="SignUpInputBox form-control"
                  name={FULL_NAME}
                  value={FullName}
                  onKeyPress={PureTextValidator}
                  inputAdornment={
                    <img src={ProfileName_Icon} className="profileIcon"></img>
                  }
                  placeholder="Name"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                />

                {this.state.FullName == 0 ? (
                  <p className="errMessage" style={{ color: `${Red_Color}` }}>
                    Name field can't be empty
                  </p>
                ) : (
                  ""
                )}
              </div>
              <div className="FormInput">
                <div className="position-relative">
                  <InputWithIcon
                    type={passView ? "text" : "password"}
                    className="SignUpInputBox form-control"
                    name={PASSWORD}
                    inputAdornment={
                      <img src={Password_Icon} className="passwordIcon"></img>
                    }
                    value={Password}
                    placeholder="Password"
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  />
                  {Password ? (
                    <img
                      src={
                        passView
                          ? View_Grey_Filled_Icon
                          : View_Grey_UnFilled_Icon
                      }
                      className="viewIcon"
                      onClick={handlePasswordView}
                    ></img>
                  ) : (
                    ""
                  )}
                </div>
                {this.state.Password == 3 ? (
                  <p className="errMessage" style={{ color: `${Red_Color}` }}>
                    Not valid password{" "}
                    <InfoTooltip
                      tooltipContent={passHits}
                      placement="bottom"
                      whiteBg={true}
                      color={BG_LightGREY_COLOR}
                    >
                      <img src={Info_Icon_Red} className="passwordHits"></img>
                    </InfoTooltip>
                  </p>
                ) : (
                  ""
                )}
              </div>
              <p className="SignupTerms_Sec">
                This site is protected by reCAPTCHA and the Google{" "}
                <a>Privacy Policy</a> and <a>Terms of Service</a> apply.
              </p>
              <div
                className={
                  !isFormValid ? "inactive_Btn mb-2" : "Login_Btn mb-2"
                }
              >
                <ButtonComp disabled={!isFormValid} onClick={handleAccountInfo}>
                  Sign up
                </ButtonComp>
              </div>

              <div className="position-relative">
                <p className="text">or</p>
              </div>
              <div className="GoogleLogin_Btn">
                <img src={Google_Icon} className="googleIcon"></img>
                <GoogleLogin
                  clientId="1029574855542-sqas8esrvk6ajj1fp2rpqo41raojvtkv.apps.googleusercontent.com"
                  buttonText={`Sign up with Google`}
                  onSuccess={googleResponseOnSuccess}
                  onFailure={googleResponseOnFailure}
                  cookiePolicy={"single_host_origin"}
                  icon={false}
                />
              </div>
              <div className="FacebookLogin_Btn">
                <img src={Facebook_Icon} className="facebookIcon"></img>
                <FacebookLogin
                  appId={FB_APP_ID}
                  autoLoad={false}
                  textButton={`Sign up with Facebook`}
                  fields="name,email,picture"
                  callback={responseFacebook}
                />
              </div>
              <p className="SignupTerms_Sec">
                By signing up or logging in, you agree to the Boatzon{" "}
                <a>Terms of Service</a> and <a>Privacy Policy</a>.
              </p>
              <p className="LoginMsg">
                Already a user?{" "}
                <a target="_blank" onClick={handleLoginModel}>
                  Log in
                </a>
              </p>
            </div>
            <div className="slider-dots">
              <img src={activePage == 1 ? Active_Dot : Inactive_Dot}></img>
              <img
                src={activePage == 2 ? Active_Dot : Inactive_Dot}
                className="mx-2"
              ></img>
              <img src={activePage == 3 ? Active_Dot : Inactive_Dot}></img>
            </div>
          </div>
        ) : (
          currentScreen
        )}

        {/* Snakbar Components */}
        <Snackbar
          variant={variant}
          message={usermessage}
          open={open}
          onClose={handleSnackbarClose}
          vertical={vertical}
          horizontal={horizontal}
        />

        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.passHitsContent) {
              padding: 0.732vw;
            }
            :global(.passHitsContent li) {
              font-size: 0.732vw;
              color: ${Red_Color};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
            }
            .modelContent {
              background: ${WHITE_COLOR};
              overflow-y: auto;
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.292vw;
              padding: 1.464vw 1.83vw;
            }
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .SignUpModel {
              width: 24.158vw;
              position: relative;
            }
            .errMessage {
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.585vw 0 0 0;
              text-transform: capitalize;
            }
            .passwordHits {
              width: 0.658vw;
              margin-bottom: 0.146vw;
              cursor: pointer;
            }
            .emailIcon {
              width: 0.951vw;
            }
            .profileIcon {
              width: 0.878vw;
            }
            .passwordIcon {
              width: 0.878vw;
              margin-bottom: 0.292vw;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.756vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .FormInput {
              margin: 1.098vw 0;
              position: relative;
            }
            .viewIcon {
              position: absolute;
              right: 10px;
              top: 50%;
              transform: translate(-50%, 0);
              cursor: pointer;
              width: 0.951vw;
            }
            .notVerifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            :global(.SignUpModel .SignUpInputBox) {
              width: 100%;
              height: 2.562vw;
              padding: 0.375rem 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${BLACK_COLOR};
            }
            :global(.SignUpModel .SignUpInputBox input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.SignUpModel .SignUpInputBox input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            :global(.SignUpModel .SignUpInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.SignUpModel .SignUpInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.Login_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              border-radius: 0.146vw;
            }
            :global(.Login_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.Login_Btn button:focus),
            :global(.Login_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.Login_Btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              height: 2.342vw;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              border-radius: 0.146vw;
            }
            .Frgt_Pass {
              margin: 0;
              font-size: 0.805vw;
              cursor: pointer;
              color: ${GREEN_COLOR};
              text-align: right;
              font-weight: lighter !important;
            }
            .text {
              margin: 0;
              text-align: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .text:before {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              right: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .text:after {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              left: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .GoogleLogin_Btn,
            .FacebookLogin_Btn {
              position: relative;
            }
            :global(.GoogleLogin_Btn button) {
              width: 100%;
              margin: 0.732vw 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR} !important;
              color: ${FONTGREY_COLOR} !important;
              position: relative;
              box-shadow: none !important;
              border-radius: 0.146vw;
              opacity: 1 !important;
            }
            :global(.GoogleLogin_Btn button div) {
              padding: 0 !important;
              background: none !important;
            }
            :global(.GoogleLogin_Btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0366vw !important;
              font-size: 0.951vw !important;
              font-weight: 600 !important;
              padding: 0 !important;
              width: 100%;
            }
            :global(.GoogleLogin_Btn button:hover) {
              background: ${Border_LightGREY_COLOR} !important;
              opacity: 1 !important;
            }
            :global(.GoogleLogin_Btn button:focus),
            :global(.GoogleLogin_Btn button:active) {
              background: ${Border_LightGREY_COLOR} !important;
              outline: none !important;
              opacity: 1 !important;
            }

            :global(.FacebookLogin_Btn button) {
              width: 100%;
              margin: 0;
              padding: 0;
              height: 2.342vw;
              text-transform: initial;
              background: ${Facebook_Blue};
              color: ${WHITE_COLOR};
              position: relative !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0366vw !important;
              font-size: 0.951vw;
              font-weight: 600;
              border-radius: 0.146vw;
            }
            :global(.FacebookLogin_Btn button:hover) {
              background: ${Facebook_Blue};
            }
            :global(.FacebookLogin_Btn button:focus),
            :global(.FacebookLogin_Btn button:active) {
              background: ${Facebook_Blue};
              outline: none;
            }
            .googleIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
            .facebookIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
            .SignupTerms_Sec {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              text-align: center;
              padding: 0.585vw 0;
              margin: 0;
            }
            .SignupTerms_Sec a {
              text-decoration: none;
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              cursor: pointer;
            }
            .LoginMsg {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              text-align: center;
              padding: 0.732vw 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
            }
            .LoginMsg a {
              text-decoration: none;
              color: ${GREEN_COLOR};
              cursor: pointer;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    signUpData: state.signUpData,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(SignUpPage);
