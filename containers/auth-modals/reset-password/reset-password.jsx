// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../../hoc/Wrapper";
import {
  BLACK_COLOR,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREY_VARIANT_2,
  Password_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_1,
  GREY_VARIANT_5,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  Facebook_Blue,
  GREY_VARIANT_1,
  FONTGREY_COLOR_Dark,
} from "../../../lib/config";
import { requiredValidator } from "../../../lib/validation/validation";
import InputWithIcon from "../../../components/input-box/input-with-icon";
import ButtonComp from "../../../components/button/button";
import LoginPage from "../login/login-page";
import { resetPassword } from "../../../services/auth";
import { getUrlSlugs } from "../../../lib/url/getSlugs";
import Router from "next/router";
import { removeCookie } from "../../../lib/session";

class ResetPassword extends Component {
  state = {
    inputpayload: {},
    isFormValid: false,
    isRemembered: false,
  };

  componentDidMount() {
    let slugData = getUrlSlugs();
    setTimeout(() => {
      this.setState({
        ResetLink: slugData[slugData.length - 1],
      });
    }, 500);
  }
  // Function for User inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ valid: validate }, () => {
      let tempsignUpPayload = this.state.inputpayload;
      tempsignUpPayload[[name]] = inputControl.value;
      this.setState(
        {
          inputpayload: tempsignUpPayload,
          [name]: inputControl.value,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    });
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.newPassword == this.state.confirmPassword ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Function to redirect to SignUp modal
  handleLoginModel = () => {
    this.updateScreen(<LoginPage onClose={this.props.onClose} />);
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleResetPassword = () => {
    let payload = {
      password: this.state.inputpayload.newPassword,
      repeatPassword: this.state.inputpayload.confirmPassword,
      passwordResetLink: this.state.ResetLink,
    };
    resetPassword(payload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            removeCookie("token");
            removeCookie("authPass");
            removeCookie("mqttId");
            removeCookie("uid");
            removeCookie("userName");
            removeCookie("ucity");
            setTimeout(() => {
              Router.push("/");
            }, 1000);
          }
        }
      })
      .catch((err) => {
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };
  render() {
    const { isFormValid } = this.state;

    return (
      <Wrapper>
        <div className="col-12 p-0">
          <div className="row m-0">
            <div className="col-6 mx-auto FrgtPassModel my-4 p-0">
              <div className="modelContent">
                {/* <img
                  src={Close_Icon}
                  className="closeIcon"
                  // onClick={this.props.onClose}
                ></img> */}
                <h5 className="heading text-center my-5">Reset Password</h5>
                <div className="FormInput">
                  <InputWithIcon
                    type="password"
                    className="FrgtPassInputBox form-control"
                    name="newPassword"
                    inputAdornment={
                      <img src={Password_Icon} className="emailIcon"></img>
                    }
                    placeholder="New Password"
                    onChange={this.handleOnchangeInput(`newPassword`)}
                    autoComplete="off"
                  />
                  {/* {this.state.valid == 3 ? (
                  <p className="errMessage">Password not valid</p>
                ) : (
                  ""
                )} */}
                </div>
                <div className="FormInput">
                  <InputWithIcon
                    type="password"
                    className="FrgtPassInputBox form-control"
                    name="confirmPassword"
                    inputAdornment={
                      <img src={Password_Icon} className="emailIcon"></img>
                    }
                    placeholder="Confirm Password"
                    onChange={this.handleOnchangeInput(`confirmPassword`)}
                    autoComplete="off"
                  />
                  {this.state.confirmPassword ? (
                    this.state.newPassword != this.state.confirmPassword ? (
                      <p className="errMessage">
                        Confirm password should match
                      </p>
                    ) : (
                      ""
                    )
                  ) : (
                    ""
                  )}
                </div>

                <div className={!isFormValid ? "inactive_Btn" : "Login_Btn"}>
                  <ButtonComp
                    disabled={!isFormValid}
                    onClick={this.handleResetPassword}
                  >
                    Change Password
                  </ButtonComp>
                </div>
                {/* <p className="LoginMsg">
                Return to{" "}
                <a target="_blank" onClick={this.handleLoginModel}>
                  Log in
                </a>
              </p> */}
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .modelContent {
              background: ${WHITE_COLOR};
              overflow-y: auto;
              border-radius: 0.292vw;
              padding: 0 1.83vw;
              width: 23.426vw;
              position: relative;
            }
            .FrgtPassModel {
              min-height: 25.622vw;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              padding: 0.585vw 0 0 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .emailIcon {
              width: 1.024vw;
            }
            .passwordIcon {
              width: 0.878vw;
              margin-bottom: 0.292vw;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 1.098vw;
              cursor: pointer;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.756vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .FormInput {
              margin: 1.61vw 0;
            }
            :global(.FrgtPassModel .FrgtPassInputBox) {
              width: 100%;
              height: 2.562vw;
              padding: 0.375rem 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              color: ${BLACK_COLOR};
            }
            :global(.FrgtPassModel .FrgtPassInputBox input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.FrgtPassModel .FrgtPassInputBox input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Open Sans" !important;
            }
            :global(.FrgtPassModel .FrgtPassInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.FrgtPassModel .FrgtPassInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.Login_Btn button) {
              width: 100%;
              margin: 1.464vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
            }
            :global(.Login_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.Login_Btn button:focus),
            :global(.Login_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.Login_Btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              margin: 1.464vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
            }
            .Frgt_Pass {
              margin: 0;
              font-size: 0.805vw;
              cursor: pointer;
              color: ${GREEN_COLOR};
              text-align: right;
              font-weight: lighter !important;
            }
            .text {
              margin: 0;
              text-align: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            .text:before {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              right: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5};
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            .text:after {
              content: " ";
              position: absolute;
              bottom: 0.585vw;
              left: 55%;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_5}; /* border-size & color are now controllable */
              width: 45%;
              height: 1.171vw;
              display: inline;
            }
            :global(.GoogleLogin_Btn button) {
              width: 100%;
              margin: 1.464vw 0 0.732vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR};
              color: ${FONTGREY_COLOR};
              font-size: 0.951vw;
              position: relative;
            }
            :global(.GoogleLogin_Btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.GoogleLogin_Btn button:focus),
            :global(.GoogleLogin_Btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
            }
            :global(.FacebookLogin_Btn button) {
              width: 100%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${Facebook_Blue};
              color: ${WHITE_COLOR};
              font-size: 0.951vw;
              position: relative;
            }
            :global(.FacebookLogin_Btn button:hover) {
              background: ${Facebook_Blue};
            }
            :global(.FacebookLogin_Btn button:focus),
            :global(.FacebookLogin_Btn button:active) {
              background: ${Facebook_Blue};
              outline: none;
            }
            .googleIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 1.098vw;
            }
            .facebookIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 1.098vw;
            }
            .recoveryMsg {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              text-align: center;
              padding: 0.585vw 0 0 0;
              margin: 0;
            }
            .LoginMsg {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              text-align: center;
              padding: 1.098vw 0 0.585vw 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
            }
            .LoginMsg a {
              text-decoration: none;
              color: ${GREEN_COLOR};
              cursor: pointer;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ResetPassword;
