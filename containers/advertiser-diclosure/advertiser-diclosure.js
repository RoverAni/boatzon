import React from "react";
import Head from "next/head";
import { Close_Icon, WHITE_COLOR } from "../../lib/config";

export default function AdvertiserDiclosure(props) {
  const { onClose } = props;

  return (
    <div className="col-12 px-0 bgGreyPage mx-auto">
      <div className="bgWhite py-4">
        <Head>
          <link
            href="//db.onlinewebfonts.com/c/a78cfad3beb089a6ce86d4e280fa270b?family=Calibri"
            rel="stylesheet"
            type="text/css"
          />
        </Head>
        <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        <div className="px-4">
          <p className="font-weight-bold">How Does Boatzon Work?</p>
          <p>
            Boatzon allows you to receive personalized loan offers in seconds
            without affecting your credit score. Basically, when you apply for a
            loan for a boat, engine, trailer, or products you fill out an
            application form with Boatzon. Then, Boatzon shares that information
            with its network of marine lending partners. The Boatzon
            prequalification process uses what is called a soft pull to gather
            information from your credit report. This prequalification process
            does not affect or impact your credit score. Then, Boatzon partner
            marine lenders send their loan offers to Boatzon which displays them
            for you. Boatzon’s lenders offer loans in amounts ranging from
            $500-$10,000,000 to help you find the funds you need. After you pick
            an offer, apply with the lender that offers the best rate and terms
            for your situation to get the loan process started.
          </p>
          <p className="font-weight-bold">
            How much does Boatzon Cost?
          </p>
          <p>
            Boatzon’s prequalification and loan solutions are free to use for
            consumers. The personal loan offers that appear on Boatzon.com are
            from lending partners from which Boatzon receives compensation for
            its services, tools, and facilities. Boatzon does not include all
            lending partners, or all types of loan offers available in the
            marketplace. The underwriting criteria necessary for approval is
            determined by the lenders (not Boatzon) and it is imperative that
            you review each lender’s terms and conditions before proceeding with
            an application for a loan. You must determine which loan works for
            you and your personal financial situation. All rates, fees, and
            terms are presented without guarantee and are subject to change
            pursuant to each lender's discretion. Boatzon may receive
            compensation from companies listed on its platform.
          </p>
          <p className="font-weight-bold">Summary</p>
          <p>
            If you are considering borrowing money using a personal loan, there
            is no downside in giving Boatzon a shot for your boat, engine,
            trailer, or any product. In the worst-case scenario, if Boatzon does
            not have a personal loan offer you like, then no damage has been
            done. In the best-case scenario, you find a great personal loan
            offer and potentially save a ton of money over other higher priced
            personal loans in the marine industry.
          </p>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: 70vw !important;
            }
            :global(.MuiDialog-paper) {
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .mx-auto {
              width: 90vw;
              position: relative;
              background: ${WHITE_COLOR} !important;
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              right: 0.732vw;
              width: 0.732vw;
              cursor: pointer;
            }
            @font-face {
              font-family: Wingdings;
              panose-1: 5 0 0 0 0 0 0 0 0 0;
            }
            @font-face {
              font-family: "Cambria Math";
              panose-1: 2 4 5 3 5 4 6 3 2 4;
            }
            @font-face {
              font-family: Calibri;
              panose-1: 2 15 5 2 2 2 4 3 2 4;
            }
            html {
              overflow: auto;
              scroll-behavior: smooth !important;
              scrollbar-width: thin;
              letter-spacing: 0.0366vw !important;
            }
            body {
              margin: 0;
              overflow: auto;
              z-index: 0;
              position: relative;
              top: 0;
              left: 0;
              top: 0;
              bottom: 0;
            }
            p,
            u,
            li {
              font-size: 1.25vw;
              font-family: "Calibri", sans-serif !important;
            }
          `}
        </style>
      </div>
    </div>
  );
}
