import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";

import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  GREY_VARIANT_2,
  THEME_COLOR,
  GREY_VARIANT_6,
  Close_Icon,
  Clock_Filled_Grey_Icon,
  Video_Appointment_Icon,
  User_Filled_Icon_Grey,
  Phone_Dark_Grey_Icon,
  Hamburger_Menu_Icon_Grey,
  GREY_VARIANT_1,
  GREEN_COLOR,
  User_Blue_Icon,
} from "../../lib/config";
import { ViewState } from "@devexpress/dx-react-scheduler";
import {
  Scheduler,
  WeekView,
  DayView,
  Appointments,
  Toolbar,
  DateNavigator,
  ViewSwitcher,
  AppointmentTooltip,
  AppointmentForm,
} from "@devexpress/dx-react-scheduler-material-ui";
import ButtonComp from "../../components/button/button";
import SelectInput from "../../components/input-box/select-input";
import moment from "moment";
import { formatDate } from "../../lib/date-operation/date-operation";
const dayOfWeekNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const RenderDateCell = (cellData) => {
  return (
    <td className="text-center dateCell">
      <p className={cellData.today ? "currentDate m-0" : "date m-0"}>
        {cellData.startDate.getDate()}
      </p>
      <p className={cellData.today ? "currentDayName m-0" : "dayName m-0"}>
        {dayOfWeekNames[cellData.startDate.getDay()]}
      </p>
    </td>
  );
};

const RenderAppointmentContent = ({ data }) => {
  return (
    <div className="appointmentDiv">
      <div>
        <p className="m-0">{data.participant}</p>
        <p className="m-0 title">{data.title}</p>
        <p className="m-0">
          {moment(data.startDate).format("h:mm").valueOf()} -
          {moment(data.endDate).format("h:mm").valueOf()}
        </p>
      </div>
    </div>
  );
};

const TooltipContent = ({ appointmentData }) => {
  return (
    <div disableScrollLock={true}>
      <h5 className="appt_title">{appointmentData.title}</h5>
      <div className="col-12 p-0 appt_content">
        <div className="row m-0 align-items-center">
          <div className="col-1 p-0 ">
            <img src={Clock_Filled_Grey_Icon} className="clockIcon"></img>
          </div>
          <div className="col-11 p-0">
            <h6 className="appt_startDate">
              {moment(appointmentData.startDate)
                .format("dddd, MMMM D")
                .valueOf()}
            </h6>
          </div>
        </div>
        <div className="row m-0 align-items-center">
          <div className="col-1 p-0 "></div>
          <div className="col-11 p-0">
            <h6 className="appt_time">
              {moment(appointmentData.startDate).format("h:mm a").valueOf()} -
              {moment(appointmentData.endDate).format("h:mm a").valueOf()}
            </h6>
          </div>
        </div>
        <div className="row m-0 align-items-center">
          <div className="col-1 p-0 ">
            <img src={User_Filled_Icon_Grey} className="userIcon"></img>
          </div>
          <div className="col-11 p-0">
            <h6 className="appt_startDate">{appointmentData.participant}</h6>
          </div>
        </div>
        {appointmentData.contact_number ? (
          <div className="row m-0 align-items-center">
            <div className="col-1 p-0 ">
              <img src={Phone_Dark_Grey_Icon} className="userIcon"></img>
            </div>
            <div className="col-11 p-0">
              <h6 className="appt_startDate">
                {appointmentData.contact_number}
              </h6>
            </div>
          </div>
        ) : (
          ""
        )}
        {appointmentData.description ? (
          <div className="row m-0 pt-1">
            <div className="col-1 p-0 ">
              <img
                src={Hamburger_Menu_Icon_Grey}
                className="hamburgerIcon"
              ></img>
            </div>
            <div className="col-11 p-0">
              <h6 className="appt_desc">{appointmentData.description}</h6>
            </div>
          </div>
        ) : (
          ""
        )}
        {moment(appointmentData.startDate)
          .format("dddd, MMMM Do YYYY, h:mm:ss a")
          .valueOf() == moment().format("dddd, MMMM Do YYYY, h:mm:ss a") ? (
          <div className="launchVideo_btn">
            <ButtonComp>Launch Video Now</ButtonComp>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
const TooltipHeader = ({ appointmentData, onHide }) => {
  return (
    <div className="row m-0 align-items-center justify-content-between toolTipHeader">
      <div className="col-8 p-0">
        <p className="appointmentType d-flex align-items-center">
          <img
            src={
              appointmentData.meetingType == 1
                ? Video_Appointment_Icon
                : User_Blue_Icon
            }
            className="videoApptIcon"
          ></img>
          <span>
            {appointmentData.meetingType == 1
              ? "Video Appointment"
              : "In-Person Appointment"}
          </span>
        </p>
      </div>
      <div className="col-4 p-0">
        <div className="headerIcons d-flex align-items-center justify-content-end">
          {/* <img src={Delete_Grey_Icon} className="deleteIcon"></img>
          <img src={Email_Icon} className="emailIcon"></img> */}
          <img src={Close_Icon} onClick={onHide} className="closeIcon"></img>
        </div>
      </div>
    </div>
  );
};

class MyAppointmentsPage extends Component {
  state = {
    openPage: false,
    currentDate: new Date(),
    currentViewName: { value: "Week", label: "Week" },
  };

  currentDateChange = (currentDate) => {
    if (this.state.currentViewName.value == "Week") {
      let weekStartDate = formatDate(moment(currentDate).startOf("week"), "x");
      let weekEndDate = formatDate(moment(currentDate).endOf("week"), "x");
      this.props.handleAppointmentAPICall(weekStartDate, weekEndDate);
    } else if (this.state.currentViewName.value == "Day") {
      let Date = formatDate(moment(currentDate), "x");
      this.props.handleAppointmentAPICall(Date, Date);
    }
    this.setState({
      currentDate,
    });
  };

  componentDidMount() {
    this.setState({
      openPage: true,
    });
  }

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let selectPayload = { ...this.state[name] };
      selectPayload.label = inputControl.label;
      selectPayload.value = inputControl.value;
      this.setState({
        [name]: { ...selectPayload },
      });
    }
  };

  renderViewSwitcher = () => {
    const viewOptions = [
      { value: "Week", label: "Week" },
      { value: "Day", label: "Day" },
    ];
    const { currentViewName } = this.state;
    return (
      <SelectInput
        defaultValue={currentViewName}
        value={currentViewName}
        myAppointmentPage={true}
        options={viewOptions}
        onChange={this.handleOnSelectInput(`currentViewName`)}
      />
    );
  };

  render() {
    const { currentDate, currentViewName, openPage } = this.state;
    // const tomorrow = moment().add(8, "days").set({ h: 12, m: 45, s: 0 });
    // console.log("fsh-->timestamp", tomorrow, tomorrow.format("x"));
    const { appointmentsData } = this.props;
    return (
      <Wrapper>
        <div className="AppointmentPage">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 py-5">
              <div className="row mx-0 py-3 justify-content-center PageContent">
                <div className="col-12 paddingXThree">
                  <h6 className="heading">My Appointments</h6>
                  {openPage ? (
                    <div className="CalenderDiv">
                      <Scheduler data={appointmentsData} height={"inherit"}>
                        <ViewState
                          currentDate={currentDate}
                          currentViewName={currentViewName.value}
                          onCurrentDateChange={this.currentDateChange}
                        />
                        <DayView
                          startDayHour={8}
                          endDayHour={19}
                          cellDuration={60}
                          dayScaleCellComponent={RenderDateCell}
                        />
                        <WeekView
                          startDayHour={8}
                          endDayHour={19}
                          cellDuration={60}
                          dayScaleCellComponent={RenderDateCell}
                        />
                        <Appointments
                          appointmentContentComponent={RenderAppointmentContent}
                        />
                        <Toolbar />
                        <DateNavigator />
                        <ViewSwitcher
                          switcherComponent={this.renderViewSwitcher}
                        />
                        <AppointmentTooltip
                          headerComponent={TooltipHeader}
                          contentComponent={TooltipContent}
                          // showOpenButton
                          // showCloseButton
                          showDeleteButton
                        />
                        <AppointmentForm readOnly />
                      </Scheduler>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .AppointmentPage {
              background: ${BG_LightGREY_COLOR};
            }
            .PageContent {
              background: ${WHITE_COLOR};
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
            .CalenderDiv {
              height: 36.603vw;
              position: relative;
            }
            :global(.MuiToolbar-root) {
              padding: 0 !important;
            }
            :global(.MuiTable-root) {
              min-width: 100% !important;
            }
            :global(.MuiToolbar-root .MuiButtonBase-root) {
              padding: 0 0.219vw !important;
              border-radius: 0% !important;
            }
            :global(.MuiToolbar-root .MuiButtonBase-root:hover) {
              background: none;
            }
            :global(.MuiToolbar-root .MuiButtonBase-root:focus, .MuiToolbar-root
                .MuiButtonBase-root:active) {
              background: none;
              outline: none;
            }
            :global(.MuiToolbar-root button:nth-child(1)) {
              border: 0.0732vw solid ${GREY_VARIANT_3};
            }
            :global(.MuiToolbar-root button:nth-child(2)) {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-left: none;
            }
            :global(.MuiToolbar-root
                button:nth-child(1)
                .MuiSvgIcon-root, .MuiToolbar-root
                button:nth-child(2)
                .MuiSvgIcon-root) {
              width: 1.464vw;
              height: 1.464vw;
            }
            :global(.MuiToolbar-root button:nth-child(1) span, .MuiToolbar-root
                button:nth-child(2)
                span) {
              color: ${GREY_VARIANT_2};
            }
            :global(.MuiToolbar-root button:nth-child(3)) {
              margin-left: 0.732vw;
            }
            :global(.MuiToolbar-root button:nth-child(3) span) {
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.date) {
              font-size: 1.39vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_2};
              line-height: 1.4;
            }
            :global(.dayName) {
              font-size: 0.951vw;
              text-transform: uppercase;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_2};
              line-height: 1;
            }
            :global(.currentDate) {
              font-size: 1.39vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              line-height: 1.4;
            }
            :global(.currentDayName) {
              font-size: 0.951vw;
              text-transform: uppercase;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              line-height: 1;
            }
            :global(.dateCell) {
              padding: 0.951vw 0;
            }
            :global(.MuiTableCell-root div) {
              display: flex;
              align-items: center;
              justify-content: center;
              line-height: 3.221vw;
              padding: 0 !important;
            }
            :global(.MuiTableCell-root div span) {
              font-size: 0.951vw;
              text-transform: lowercase;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.021vw !important;
              color: ${FONTGREY_COLOR};
              margin-bottom: 3px;
              line-height: 1;
            }
            :global(.emptyCell) {
              width: 81px;
              display: flex;
              min-width: 81px;
              align-items: flex-end;
            }
            :global(.CalenderDiv
                div
                div:nth-child(3)
                > div
                div:nth-child(1)
                > div
                div:nth-child(1)) {
              border-right: none !important;
            }
            :global(.CalenderDiv
                div
                div:nth-child(3)
                > div
                div:nth-child(2)
                > div
                div:nth-child(1)
                > div
                table:nth-child(2)) {
              width: 0 !important;
            }
            :global(.CalenderDiv div div:nth-child(3)) {
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            :global(.CalenderDiv .greyChevronIcon) {
              width: 0.732vw;
              margin-bottom: 0.219vw;
            }
            :global(.CalenderDiv
                > div
                > div:nth-child(3)
                > div
                > div:nth-child(2)
                > div
                > div:nth-child(2)
                > div
                > div) {
              // width: 13.75% !important;
              z-index: 1 !important;
            }
            :global(.CalenderDiv
                > div
                > div:nth-child(3)
                > div
                > div:nth-child(2)
                > div
                > div:nth-child(2)
                > div
                > div
                > div) {
              background: ${THEME_COLOR};
              border-radius: 0.219vw;
            }
            :global(.appointmentDiv) {
              padding: 0.292vw;
              display: flex;
              flex-direction: column;
              line-height: 1.3;
            }
            :global(.appointmentDiv p) {
              color: ${GREY_VARIANT_6};
              font-family: "Open Sans";
              letter-spacing: 0.021vw !important;
              font-size: 0.805vw;
              text-transform: capitalize;
              line-height: 1;
            }
            :global(.appointmentDiv .title) {
              white-space: nowrap;
              overflow: hidden;
              text-overflow: ellipsis;
              max-width: 97%;
              display: inline-block;
            }
            :global(.MuiPopover-paper) {
              max-width: 19.033vw !important;
            }
            :global(.headerIcons) {
              line-height: 1;
            }
            :global(.closeIcon) {
              width: 0.805vw;
              cursor: pointer;
            }
            :global(.deleteIcon) {
              width: 0.878vw;
              cursor: pointer;
              margin-right: 0.732vw;
            }
            :global(.emailIcon) {
              width: 0.951vw;
              cursor: pointer;
              margin-right: 0.732vw;
            }
            :global(.toolTipHeader) {
              padding: 0.732vw;
              background: ${BG_LightGREY_COLOR};
            }
            :global(.appointmentType) {
              border: 0.0732vw solid ${THEME_COLOR};
              padding: 0.219vw 0.585vw;
              margin: 0;
              line-height: 1;
              width: fit-content;
              border-radius: 0.146vw;
            }
            :global(.appointmentType span) {
              color: ${THEME_COLOR};
              font-family: "Open Sans-SemiBold";
              letter-spacing: 0.021vw !important;
              font-size: 0.732vw;
              text-transform: capitalize;
            }
            :global(.videoApptIcon) {
              width: 0.878vw;
              margin-right: 0.585vw;
            }
            :global(.appt_title) {
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans";
              font-weight: 600;
              letter-spacing: 0.021vw !important;
              font-size: 1.171vw;
              text-transform: capitalize;
              padding: 0.219vw 0.732vw 0.512vw 0.732vw;
              background: ${BG_LightGREY_COLOR};
              margin: 0;
            }
            :global(.clockIcon) {
              width: 0.805vw;
              margin-bottom: 0.219vw;
            }
            :global(.userIcon) {
              width: 0.732vw;
              margin-bottom: 0.219vw;
            }
            :global(.hamburgerIcon) {
              width: 0.732vw;
              margin-bottom: 0.292vw;
              vertical-align: baseline;
            }
            :global(.appt_content) {
              padding: 10px 15px !important;
            }
            :global(.appt_startDate) {
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans";
              font-weight: 600;
              letter-spacing: 0.021vw !important;
              font-size: 1.024vw;
              line-height: 1;
              text-transform: capitalize;
              margin: 0;
            }
            :global(.appt_time) {
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans";
              letter-spacing: 0.021vw !important;
              font-size: 0.878vw;
              line-height: 1;
              text-transform: lowercase;
            }
            :global(.appt_desc) {
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans";
              letter-spacing: 0.021vw !important;
              font-size: 0.805vw;
              line-height: 1.2;
              text-transform: capitalize;
            }

            :global(.launchVideo_btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.366vw 0;
              border: none;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.launchVideo_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              line-height: 1;
              font-size: 0.951vw;
            }
            :global(.launchVideo_btn button:focus),
            :global(.launchVideo_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.launchVideo_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default MyAppointmentsPage;
