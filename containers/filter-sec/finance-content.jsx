import React, { Component } from "react";
import { connect } from "react-redux";

import InputBox from "../../components/input-box/input-box";
import Wrapper from "../../hoc/Wrapper";
import {
  Dark_Green_Color,
  Dollar_Icon,
  FONTGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  Orange_Color,
  Red_Color_1,
  THEME_COLOR,
  WHITE_COLOR,
  Yellow_Color,
} from "../../lib/config";
import { NumberValidator } from "../../lib/validation/validation";
import CustomizedSlider from "../../components/range-slider/custom-range-slider";
import ButtonComp from "../../components/button/button";

class FinanceContent extends Component {
  state = {
    inputpayload: {},
    Score: 650,
  };

  /** function to toggle price dropdown */
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
      [inputControl.name]:
        inputControl.value == "" ? 0 : parseInt(inputControl.value, 10),
    });
  };

  onChangeSlider = (name) => (event, value) => {
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[name] = value;
    this.setState({
      inputpayload: { ...tempPayload },
      [name]: value,
    });
  };

  render() {
    const { cashDown, monthlyPayment } = this.state.inputpayload;

    const cashMarks = [
      {
        value: 0,
        label: "$0",
      },
      {
        value: 3000,
      },
      {
        value: 6000,
      },
      {
        value: 9000,
      },
      {
        value: 12000,
      },
      {
        value: 15000,
        label: "$15000",
      },
    ];

    const monthlyPaymentMarks = [
      {
        value: 0,
        label: "$0",
      },
      {
        value: 2000,
      },
      {
        value: 4000,
      },
      {
        value: 6000,
      },
      {
        value: 8000,
      },
      {
        value: 10000,
        label: "$10000",
      },
    ];

    const { userProfileData } = this.props;

    return (
      <Wrapper>
        <div className="col-12 p-0 mt-3">
          <div className="row m-0 align-items-center justify-content-center">
            <div className="col-6 p-0 pr-3">
              <div>
                <div className="d-flex align-items-center justify-content-between">
                  <p className="label">Cash Down</p>
                  <div className="position-relative">
                    <InputBox
                      type="text"
                      className="inputBox"
                      name="cashDown"
                      value={cashDown}
                      onChange={this.handleOnchangeInput}
                      onKeyPress={NumberValidator}
                      autoComplete="off"
                      placeholder="0"
                    ></InputBox>
                    <img src={Dollar_Icon} className="dollarIcon"></img>
                  </div>
                </div>
                <CustomizedSlider
                  cashDown={true}
                  cashMarks={cashMarks}
                  onChange={this.onChangeSlider("cashDown")}
                  value={this.state.cashDown}
                />
              </div>
            </div>
            <div className="col-6 p-0 pl-2">
              <div>
                <div className="d-flex align-items-center justify-content-between">
                  <p className="label">Monthly Payment</p>
                  <div className="position-relative">
                    <InputBox
                      type="text"
                      className="inputBox"
                      name="monthlyPayment"
                      value={monthlyPayment}
                      onChange={this.handleOnchangeInput}
                      onKeyPress={NumberValidator}
                      autoComplete="off"
                      placeholder="0"
                    ></InputBox>
                    <img src={Dollar_Icon} className="dollarIcon"></img>
                  </div>
                </div>
                <CustomizedSlider
                  monthlyPayment={true}
                  cashMarks={monthlyPaymentMarks}
                  onChange={this.onChangeSlider("monthlyPayment")}
                  value={this.state.monthlyPayment}
                />
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center justify-content-center">
            <div className="col-12 p-0">
              <div className="offersFinancingDiv">
                <h5 className="heading">Boatzon Offers Financing</h5>
                <p className="scoreMsg">
                  Your pre-qualification terms expired. Get pre-qualified again
                  to see your current terms.
                </p>
                <div className="d-flex align-items-center justify-content-around scoreBar">
                  <div
                    className={
                      this.state.Score >= 300 && this.state.Score < 410
                        ? "firstScoreDiv currentScoreDiv"
                        : "firstScoreDiv"
                    }
                  >
                    <p className="cardLabel">
                      {this.state.Score >= 300 && this.state.Score < 410
                        ? "Poor"
                        : " "}
                    </p>
                  </div>
                  <div
                    className={
                      this.state.Score >= 410 && this.state.Score < 520
                        ? "secondScoreDiv currentScoreDiv"
                        : "secondScoreDiv"
                    }
                  >
                    <p className="cardLabel">
                      {this.state.Score >= 410 && this.state.Score < 520
                        ? "Fair"
                        : " "}
                    </p>
                  </div>
                  <div
                    className={
                      this.state.Score >= 520 && this.state.Score < 630
                        ? "thirdScoreDiv currentScoreDiv"
                        : "thirdScoreDiv"
                    }
                  >
                    <p className="cardLabel">
                      {this.state.Score >= 520 && this.state.Score < 630
                        ? "Good"
                        : " "}
                    </p>
                  </div>
                  <div
                    className={
                      this.state.Score >= 630 && this.state.Score < 740
                        ? "fourthScoreDiv currentScoreDiv"
                        : "fourthScoreDiv"
                    }
                  >
                    <p className="cardLabel">
                      {this.state.Score >= 630 && this.state.Score < 740
                        ? "Very Good"
                        : " "}
                    </p>
                  </div>
                  <div
                    className={
                      this.state.Score >= 740 && this.state.Score < 850
                        ? "lastScoreDiv currentScoreDiv"
                        : "lastScoreDiv"
                    }
                  >
                    <p className="cardLabel">
                      {this.state.Score >= 740 && this.state.Score < 850
                        ? "Exceptional"
                        : " "}
                    </p>
                  </div>
                </div>
                {!userProfileData?.creditScore && (
                  <div className="offersFinancing_btn">
                    <ButtonComp>Get Pre-Qualified</ButtonComp>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .label {
              margin: 0;
              font-size: 0.805vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_1};
            }
            :global(.priceInput
                .dropdownBtn
                .dropdownList
                > dropdownitem
                > div
                .inputBox) {
              display: block;
              max-width: 3.66vw !important;
              padding: 0.292vw 0.366vw 0.292vw 0.878vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              box-sizing: border-box;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.priceInput
                .dropdownBtn
                .dropdownList
                > dropdownitem
                > div
                .inputBox:focus) {
              color: ${GREY_VARIANT_2};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.priceInput
                .dropdownBtn
                .dropdownList
                > dropdownitem
                > div
                .inputBox::placeholder) {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .dollarIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              left: 0.292vw;
              height: 0.732vw;
            }
            .offersFinancingDiv {
              border: 0.0732vw solid #eaeaea;
              box-sizing: border-box;
              border-radius: 0.366vw;
              padding: 1.098vw 0;
              margin-bottom: 0.585vw;
            }
            .heading {
              margin: 0;
              font-size: 1.098vw;
              font-weight: 600;
              padding: 0 0.878vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_1};
            }
            .scoreMsg {
              margin: 0;
              font-size: 0.805vw;
              padding: 0.366vw 0.878vw 0.585vw 0.878vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            .scoreBar {
              overflow-x: hidden;
            }
            .cardLabel {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0;
              line-height: 1;
              position: relative;
              z-index: 1;
              bottom: 0.0732vw;
            }
            .firstScoreDiv {
              width: 15%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              background: ${Red_Color_1};
              position: relative;
            }
            .secondScoreDiv {
              width: 19%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              background: ${Orange_Color};
              position: relative;
            }
            .thirdScoreDiv {
              width: 19%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              background: ${Yellow_Color};
              position: relative;
            }
            .fourthScoreDiv {
              width: 19%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              opacity: 0.9;
              background: ${GREEN_COLOR};
              position: relative;
            }
            .lastScoreDiv {
              width: 15%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              opacity: 0.9;
              background: ${Dark_Green_Color};
              position: relative;
            }
            .currentScoreDiv {
              padding: 0.219vw 0.439vw;
              height: 1.61vw;
              width: 27%;
              text-align: center;
              align-items: center;
              display: flex;
              justify-content: center;
            }
            :global(.offersFinancing_btn) {
              width: 100%;
              text-align: center;
            }
            :global(.offersFinancing_btn button) {
              width: 85%;
              padding: 0.732vw 0;
              background: ${WHITE_COLOR};
              color: ${THEME_COLOR};
              margin: 0.732vw 0 0 0;
              border: 0.0732vw solid ${THEME_COLOR};
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.offersFinancing_btn button span) {
              font-family: "Open Sans-SemiBold" !important;
              line-height: 1;
              font-size: 0.951vw;
            }
            :global(.offersFinancing_btn button:focus),
            :global(.offersFinancing_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.offersFinancing_btn button:hover) {
              background: ${WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(FinanceContent);
