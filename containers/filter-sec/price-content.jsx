import React, { Component } from "react";
import InputBox from "../../components/input-box/input-box";
import Wrapper from "../../hoc/Wrapper";
import { NumberValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import {
  GREY_VARIANT_1,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  WHITE_COLOR,
} from "../../lib/config";

class PriceContent extends Component {
  render() {
    return (
      <Wrapper>
        <div className="mt-3 mb-1">
          <div className="d-flex align-items-center justify-content-between">
            <InputBox
              type="text"
              className="inputBox"
              name="minPrice"
              value={this.props.inputpayload.minPrice}
              onChange={this.props.handleOnchangeInput}
              onKeyPress={NumberValidator}
              autoComplete="off"
              placeholder="Min"
            ></InputBox>
            <p className="toSpan px-2">to</p>
            <InputBox
              type="text"
              className="inputBox"
              name="maxPrice"
              value={this.props.inputpayload.maxPrice}
              onChange={this.props.handleOnchangeInput}
              onKeyPress={NumberValidator}
              autoComplete="off"
              placeholder="Max"
            ></InputBox>
          </div>
          <div className="apply_btn" onClick={this.props.handlePriceRange}>
            <ButtonComp>Apply</ButtonComp>
          </div>
        </div>
        <style jsx>
          {`
            :global(.priceInput
                .dropdownBtn
                .dropdownList
                > dropdownitem
                > div
                .inputBox) {
              display: block;
              min-width: 3.66vw !important;
              max-width: fit-content !important;
              padding: 0.292vw 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              box-sizing: border-box;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.priceInput
                .dropdownBtn
                .dropdownList
                > dropdownitem
                > div
                .inputBox:focus) {
              color: ${GREY_VARIANT_2};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.priceInput
                .dropdownBtn
                .dropdownList
                > dropdownitem
                > div
                .inputBox::placeholder) {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .toSpan {
              margin: 0;
              font-size: 0.805vw;
              padding: 0 0.732vw 0 0;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
            }
            .apply_btn {
              text-align: center;
            }
            :global(.apply_btn button) {
              width: 50%;
              padding: 0.292vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.apply_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              font-size: 0.732vw;
            }
            :global(.apply_btn button:focus),
            :global(.apply_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.apply_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default PriceContent;
