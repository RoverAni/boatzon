// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// asstes and colors
import {
  FONTGREY_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  Plus_Icon,
  Close_Icon,
  GREEN_COLOR,
  GREY_VARIANT_3,
} from "../../lib/config";

// imported components
import ManufacturerSearchList from "./manufacturer-search-list";

// reusable component
import Model from "../../components/model/model";

class FilterSec extends Component {
  render() {
    const {
      myboat,
      heading,
      followingManufacturerList,
      activeLink,
    } = this.props;
    /**
     * myboat for myBoats page
     **/

    return (
      <Wrapper>
        <div className="container p-md-0">
          <div className="screenWidth mx-auto FilterPage">
            <h5 className="heading">{heading}</h5>

            {/* My-Boats Page Manufacturer Slider */}
            {myboat && (
              <Wrapper>
                <div className="myBoatFilter">
                  <ul className="list-unstyled mb-0 d-flex flex-wrap">
                    {followingManufacturerList &&
                    followingManufacturerList.length > 0 ? (
                      <li
                        className={
                          activeLink == "All"
                            ? "myboat-slide activeBox"
                            : "myboat-slide"
                        }
                        onClick={this.props.handleManufacturerClick.bind(
                          this,
                          "All"
                        )}
                      >
                        All
                      </li>
                    ) : (
                      ""
                    )}
                    {followingManufacturerList &&
                      followingManufacturerList.map((item, index) => (
                        <li
                          className={
                            activeLink.manufactureName ==
                            `${item.manufactureName}`
                              ? "myboat-slide activeBox "
                              : "myboat-slide"
                          }
                          key={index}
                        >
                          <div
                            onClick={this.props.handleManufacturerClick.bind(
                              this,
                              item
                            )}
                          >
                            {item.manufactureName}
                          </div>
                          <img
                            className="closeIcon"
                            src={Close_Icon}
                            onClick={this.props.handleUnfollowManufacturer.bind(
                              this,
                              `${item.manufactureId}`
                            )}
                          ></img>
                        </li>
                      ))}
                    <li
                      className="myboat-slide"
                      onClick={() => this.props.handleManufacturersList()}
                    >
                      <img src={Plus_Icon}></img>
                    </li>
                  </ul>
                </div>
              </Wrapper>
            )}
          </div>
        </div>

        {/* Add Follow Manufacturers Model */}
        <Model
          open={this.props.addManufaturer}
          onClose={this.props.handleManufacturersList}
        >
          <ManufacturerSearchList
            onClose={this.props.handleManufacturersList}
            notFollowingList={this.props.notFollowingList}
            handleFollowManufacturer={this.props.handleFollowManufacturer}
            handleAllGetManufacturerList={
              this.props.handleAllGetManufacturerList
            }
            allManufacturersList={this.props.allManufacturersList}
            total_count={this.props.total_count}
          />
        </Model>

        <style jsx>
          {`
            .myBoatFilter {
              margin-bottom: 0.732vw;
            }

            .FilterPage {
              padding: 1.756vw 0 0.585vw 0;
            }

            .heading {
              margin: 0 0 0.366vw 0;
              font-size: 1.244vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
            }

            .activeBox {
              background: ${GREEN_COLOR} !important;
              color: ${BG_LightGREY_COLOR} !important;
            }

            .myboat-slide {
              display: flex;
              width: fit-content;
              height: auto;
              padding: 0.439vw 1.464vw;
              background: ${BG_LightGREY_COLOR};
              margin-right: 0.732vw;
              margin-top: 0.732vw;
              color: ${GREY_VARIANT_1};
              font-size: 0.878vw;
              font-weight: lighter !important;
              cursor: pointer;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }

            .myboat-slide .closeIcon {
              width: 0.732vw;
              display: none;
              margin-left: 0.366vw;
            }

            .myboat-slide:hover {
              background: ${GREY_VARIANT_3};
            }

            .myboat-slide:hover .closeIcon {
              display: block;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default FilterSec;
