// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import { getManufactures } from "../../services/manufacturer";
// asstes and colors
import {
  FONTGREY_COLOR,
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  Chevron_Down,
  RightChevron,
} from "../../lib/config";

// redux component
import Context from "../../context/context";

// reusable component
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import CustomizedSlider from "../../components/range-slider/custom-range-slider";
import SelectInput from "../../components/input-box/select-input";

// validation fuction
import { NumberValidator } from "../../lib/validation/validation";

// Reactstrap Components
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";

class ProductsFilterSec extends Component {
  state = {
    priceRange: "$40,000",
    radiusRange: "200 Miles",
    location: "Location",
    boatManufacturerList: [],
    total_count: "",
    limit: 15,
  };

  // Async Boat Manufacturer List API call
  handleBoatManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "1", // 0-all manufactureres,1-boats and products,2,engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.boatManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName,
                };
              })
            : [];
        this.setState({
          boatManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.boatManufacturerList &&
      this.state.boatManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.boatManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    console.log("multiValue", this.state.multiValue);
    const { onlyFilters } = this.props;
    /**
     * onlyFilters for professional profile page
     **/

    const { _categoryArray, condition, handleChangeCondition } = this.props;
    return (
      <Context.Consumer>
        {(Context) => {
          return (
            <Wrapper>
              <div className={onlyFilters ? "" : "container p-md-0"}>
                <div
                  className={onlyFilters ? "" : "products-screenWidth mx-auto"}
                >
                  {onlyFilters ? (
                    ""
                  ) : (
                    <div className="filter-applied pt-3 pb-3 d-flex">
                      Products
                      {_categoryArray && _categoryArray.length > 0
                        ? _categoryArray.map((k, i) =>
                            k !== "" ? (
                              <div className="d-flex">
                                {i === 1 || i == 0 ? (
                                  <div
                                    className="d-flex"
                                    style={{
                                      alignItems: "center",
                                      padding: "0 0.364583vw 0 0.364583vw",
                                    }}
                                  >
                                    <img
                                      src={RightChevron}
                                      className="right-chevron"
                                      alt="right-chevron"
                                    />
                                  </div>
                                ) : (
                                  ""
                                )}{" "}
                                <div className="filter-applied" key={i}>
                                  {k}{" "}
                                </div>
                              </div>
                            ) : (
                              ""
                            )
                          )
                        : ""}
                    </div>
                  )}
                  {/* Products > Electronics & Navigation > FishFinder Only</div> */}
                  {onlyFilters ? (
                    ""
                  ) : (
                    <h5 className="heading">
                      {_categoryArray.length === 0
                        ? "All Products"
                        : _categoryArray.length === 2
                        ? _categoryArray[1]
                        : _categoryArray[0]}
                    </h5>
                  )}
                  {/* My-Boats Page Manufacturer Slider */}
                  <Wrapper>
                    <div // Products Page Filters
                      className={
                        onlyFilters
                          ? "row m-0 align-items-center FilterSelections p-0"
                          : "row m-0 align-items-center FilterSelections"
                      }
                    >
                      <div className="col-2 pl-0">
                        <label className="label">Condition</label>
                        <div className="d-flex align-items-center justify-content-center mb-2">
                          <div
                            className={
                              condition == "All"
                                ? "activeCondition leftBorderRadius norightBorder"
                                : "inactiveCondition leftBorderRadius norightBorder"
                            }
                            onClick={() => handleChangeCondition("All")}
                          >
                            <p>All</p>
                          </div>
                          <div
                            className={
                              condition == "New"
                                ? "activeCondition norightBorder"
                                : "inactiveCondition norightBorder"
                            }
                            onClick={() => handleChangeCondition("New")}
                          >
                            <p>New</p>
                          </div>
                          <div
                            className={
                              condition == "Used"
                                ? "activeCondition rightBorderRadius"
                                : "inactiveCondition rightBorderRadius"
                            }
                            onClick={() => handleChangeCondition("Used")}
                          >
                            <p>Used</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-3 pl-0">
                        <span className="new-filters-header">
                          Product Is For
                        </span>
                        <div className="MultiSelectCheckbox mb-2">
                          <SelectInput
                            asyncSelect={true}
                            productsPage={true}
                            placeholder="Manufacturer"
                            value={this.props.manufactureId}
                            loadOptions={this.handleBoatManufacturerList}
                            onChange={this.props.handleOnSelectInput(
                              `manufactureId`
                            )}
                            additional={{
                              page: 0,
                            }}
                            hideSelectedOptions={false}
                            isMulti={true}
                          />
                        </div>
                      </div>
                      <div className="col-2 pl-0">
                        <span className="new-filters-header">
                          Search Radius
                        </span>
                        <div className="RadiusInput mb-2">
                          <ButtonDropdown
                            isOpen={this.props.radiusDropdown}
                            toggle={this.props.radiusDropdownOpen}
                            className="dropdownBtn"
                          >
                            <DropdownToggle className="dropdownInput">
                              <p className="inputValue">
                                {this.state.radiusRange}
                              </p>
                            </DropdownToggle>
                            <DropdownMenu className="dropdownList">
                              <DropdownItem className="dropdownItem">
                                <CustomizedSlider
                                  onChange={this.props.onChangeRadius}
                                  value={this.props.valueRadius}
                                />
                              </DropdownItem>
                            </DropdownMenu>
                          </ButtonDropdown>
                        </div>
                      </div>
                      <div className="col-2 pl-0">
                        <span className="new-filters-header">Price</span>
                        <div className="PriceInput mb-2">
                          <ButtonDropdown
                            isOpen={this.props.priceDropdown}
                            toggle={this.props.priceDropdownOpen}
                            className="dropdownBtn"
                          >
                            <DropdownToggle className="dropdownInput">
                              <p className="inputValue">
                                {this.state.priceRange}
                              </p>
                            </DropdownToggle>
                            <DropdownMenu className="dropdownList">
                              <DropdownItem className="dropdownItem">
                                <div className=" d-flex align-items-center justify-content-between">
                                  <InputBox
                                    type="text"
                                    className="inputBox"
                                    name="minPrice"
                                    value={this.props.inputpayload.minPrice}
                                    onChange={this.props.handleOnchangeInput}
                                    onKeyPress={NumberValidator}
                                    autoComplete="off"
                                    placeholder="Min"
                                  ></InputBox>
                                  <p className="toSpan px-2">to</p>
                                  <InputBox
                                    type="text"
                                    className="inputBox"
                                    name="maxPrice"
                                    value={this.props.inputpayload.maxPrice}
                                    onChange={this.props.handleOnchangeInput}
                                    onKeyPress={NumberValidator}
                                    autoComplete="off"
                                    placeholder="Max"
                                  ></InputBox>
                                </div>
                                <div
                                  className="apply_btn"
                                  onClick={this.props.handlePriceRange}
                                >
                                  <ButtonComp>Apply</ButtonComp>
                                </div>
                              </DropdownItem>
                            </DropdownMenu>
                          </ButtonDropdown>
                        </div>
                      </div>
                    </div>
                    <hr className="product-sec-hr" />
                  </Wrapper>
                </div>
              </div>

              <style jsx>
                {`
                  .right-chevron {
                    height: 0.46875vw;
                    width: 0.46875vw;
                  }
                  .filter-applied {
                    font-size: 0.625vw;
                    font-weight: 100;
                    font-family: Open Sans !important;
                    text-transform: capitalize;
                    color: ${FONTGREY_COLOR};
                  }
                  .products-screenWidth {
                    width: 96% !important;
                  }
                  .product-sec-hr {
                    margin-top: 10px;
                    margin-bottom: 5px;
                    border: 0;
                    border-top: 1px solid rgba(0, 0, 0, 0.1);
                  }

                  .new-filters-header {
                    font-size: 0.659vw;
                    color: ${GREY_VARIANT_1};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    margin-bottom: 0.366vw;
                  }
                  .apply_btn {
                    text-align: center;
                  }
                  :global(.apply_btn button) {
                    width: 50%;
                    padding: 0.292vw 0;
                    background: ${THEME_COLOR};
                    color: ${WHITE_COLOR};
                    margin: 0.732vw 0 0 0;
                    text-transform: capitalize;
                    font-weight: 500;
                    position: relative;
                  }
                  :global(.apply_btn button span) {
                    font-family: "Museo-Sans-Cyrl-Light" !important;
                    font-weight: 600;
                    font-size: 0.732vw;
                  }
                  :global(.apply_btn button:focus),
                  :global(.apply_btn button:active) {
                    background: ${THEME_COLOR};
                    outline: none;
                    box-shadow: none;
                  }
                  :global(.apply_btn button:hover) {
                    background: ${THEME_COLOR};
                  }
                  :global(.PriceInput .dropdownBtn),
                  :global(.RadiusInput .dropdownBtn) {
                    border-radius: 0.219vw;
                    border: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
                    // margin: 0.366vw 0.732vw 0 0;
                    margin: 0 0.732vw 0 0;
                    width: 100%;
                    height: 2.188vw;
                    position: relative;
                  }
                  :global(.PriceInput .dropdownBtn .dropdownInput),
                  :global(.RadiusInput .dropdownBtn .dropdownInput) {
                    padding: 0.366vw 0.732vw !important;
                    width: 100%;
                    display: flex;
                    align-items: center;
                    background: ${WHITE_COLOR};
                    border: none !important;
                    border-top-right-radius: 0.25rem !important;
                    border-bottom-right-radius: 0.25rem !important;
                  }
                  :global(.PriceInput .dropdownBtn .dropdownInput:active),
                  :global(.PriceInput .dropdownBtn .dropdownInput:focus),
                  :global(.RadiusInput .dropdownBtn .dropdownInput:active),
                  :global(.RadiusInput .dropdownBtn .dropdownInput:focus) {
                    background: ${WHITE_COLOR};
                    outline: 0 !important;
                    box-shadow: none !important;
                    border: none !important;
                  }
                  :global(.PriceInput .dropdownBtn .dropdownInput::after),
                  :global(.RadiusInput .dropdownBtn .dropdownInput::after) {
                    content: url(${Chevron_Down});
                    border: none !important;
                    border-right: none !important;
                    border-left: none !important;
                    margin: 0 0.292vw !important;
                    position: relative;
                    // top: -0.146vw;
                    position: absolute;
                    // right: 0.146vw;
                    right: 0.5208vw;
                    height: 100%;
                    display: flex;
                    align-items: center;
                  }
                  .inputValue,
                  .toSpan {
                    margin: 0;
                    font-size: 0.805vw;
                    padding: 0 0.732vw 0 0;
                    font-family: "Open Sans" !important;
                    color: ${GREY_VARIANT_1};
                  }
                  :global(.PriceInput .dropdownBtn .dropdownList) {
                    padding: 0.732vw 1.098vw;
                    min-width: auto;
                    margin: 0.366vw 0 0 0;
                    width: 14.641vw;
                  }
                  :global(.RadiusInput .dropdownBtn .dropdownList) {
                    padding: 1.83vw 1.098vw 0.732vw 1.098vw;
                    min-width: auto;
                    margin: 0.366vw 0 0 0;
                  }
                  :global(.PriceInput
                      .dropdownBtn
                      .dropdownList
                      .dropdownItem:hover),
                  :global(.RadiusInput
                      .dropdownBtn
                      .dropdownList
                      .dropdownItem:hover) {
                    background: none !important;
                  }
                  :global(.PriceInput .dropdownBtn .dropdownList .dropdownItem),
                  :global(.RadiusInput
                      .dropdownBtn
                      .dropdownList
                      .dropdownItem) {
                    display: block !important;
                  }
                  :global(.PriceInput
                      .dropdownBtn
                      .dropdownList
                      .dropdownItem
                      > div
                      .inputBox) {
                    display: block;
                    min-width: 3.66vw !important;
                    max-width: fit-content !important;
                    padding: 0.292vw 0.732vw;
                    line-height: 1.5;
                    background-color: #fff;
                    background-clip: padding-box;
                    border: 0.0732vw solid ${GREY_VARIANT_3};
                    border-radius: 0.146vw;
                    box-sizing: border-box;
                    transition: border-color 0.15s ease-in-out,
                      box-shadow 0.15s ease-in-out;
                    font-size: 0.732vw;
                    color: ${GREY_VARIANT_2};
                    font-family: "Open Sans" !important;
                  }
                  :global(.PriceInput
                      .dropdownBtn
                      .dropdownList
                      .dropdownItem
                      > div
                      .inputBox:focus) {
                    color: ${GREY_VARIANT_2};
                    background-color: #fff;
                    outline: 0;
                    box-shadow: none;
                  }
                  :global(.PriceInput
                      .dropdownBtn
                      .dropdownList
                      .dropdownItem
                      > div
                      .inputBox::placeholder) {
                    font-size: 0.732vw;
                    color: ${GREY_VARIANT_2};
                    text-transform: capitalize;
                    font-family: "Open Sans" !important;
                  }

                  .FilterSelections {
                    padding-top: 0.585vw;
                    display: flex;
                  }
                  .heading {
                    margin: 0 0 0.366vw 0;
                    font-size: 1.244vw;
                    color: ${FONTGREY_COLOR};
                    text-transform: capitalize;
                    font-family: "Museo-Sans-Cyrl-Semibold" !important;
                    letter-spacing: 0.021vw !important;
                  }

                  .activeCondition {
                    background: ${THEME_COLOR};
                    padding: 0.219vw 0.366vw;
                    // min-width: 3.281vw;
                    width: 33%;
                    height: 2.187vw;
                    border: 1px solid ${THEME_COLOR};
                    cursor: pointer;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                  }
                  .activeCondition p {
                    color: ${WHITE_COLOR};
                    font-size: 0.729166vw;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    margin-bottom: 0;
                  }
                  .leftBorderRadius {
                    border-top-left-radius: 0.219vw;
                    border-bottom-left-radius: 0.219vw;
                  }
                  .rightBorderRadius {
                    border-top-right-radius: 0.219vw;
                    border-bottom-right-radius: 0.219vw;
                  }
                  .norightBorder {
                    border-right: none !important;
                  }
                  .inactiveCondition {
                    background: ${WHITE_COLOR};
                    padding: 0.219vw 0.366vw;
                    // min-width: 3.281vw;
                    width: 33%;
                    height: 2.187vw;
                    border: 0.0732vw solid ${Border_LightGREY_COLOR};
                    cursor: pointer;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                  }
                  .inactiveCondition p {
                    color: ${GREY_VARIANT_1};
                    font-size: 0.729166vw;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    margin-bottom: 0;
                  }
                  .FilterSelections .label {
                    font-size: 0.659vw;
                    color: ${GREY_VARIANT_1};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    margin-bottom: 0.15625vw;
                  }
                `}
              </style>
            </Wrapper>
          );
        }}
      </Context.Consumer>
    );
  }
}

export default ProductsFilterSec;
