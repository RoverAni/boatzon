import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR,
  THEME_COLOR_Opacity,
  THEME_COLOR,
  Search_Grey_Icon,
  WHITE_COLOR,
  Plus_Light_Icon,
  FONTGREY_COLOR,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import Pagination from "../../hoc/divPagination";
import PageLoader from "../../components/loader/page-loader";
import PageBeatLoader from "../../components/loader/beat-loader";
import debounce from "lodash.debounce";


class ManufacturerSearchList extends Component {
  state = {
    searchKey: "",
  };

  componentDidMount = () => {
    this.props.handleAllGetManufacturerList(10, 0, true);
  };

  handleSearchManufacturer = (event) => {
    let inputControl = event.target;
    this.setState(
      {
        searchKey: inputControl.value,
      },
      () => {
        this.handleSearchApiCall(this.state.searchKey);
      }
    );
  };

  handleSearchApiCall = debounce((value) => {
    this.props.handleAllGetManufacturerList(10, 0, true, value);
  }, 500);

  getList = (page = 0) => {
    return new Promise(async (res, rej) => {
      try {
        await this.props.handleAllGetManufacturerList(
          10,
          page,
          false,
          this.state.searchKey,
          true
        );
        res();
      } catch (e) {
        rej();
      }
    });
  };

  render() {
    const { searchKey } = this.state;
    const { getList } = this;
    const {
      allManufacturersList,
      total_count,
      notFollowingList,
      apiLoading,
      paginationLoader,
    } = this.props;
    return (
      <Wrapper>
        <div className="manufacturerSearchListModel">
          <div className="row searchBox">
            <div className="col-12">
              <img
                src={Close_Icon}
                className="closeIcon"
                onClick={this.props.onClose}
              ></img>
              <h6 className="heading">Add Manufacturers</h6>
              <div className="searchInput">
                <InputBox
                  placeholder="Search"
                  type="text"
                  className="inputBox form-control"
                  onChange={this.handleSearchManufacturer}
                  value={searchKey}
                  autoComplete="off"
                />
                <img src={Search_Grey_Icon} className="searchIcon"></img>
              </div>
            </div>
          </div>
          <div className="row manufacturerListSec" id="manufacturer-list">
            <Pagination
              id={"manufacturer-list"}
              items={allManufacturersList}
              getItems={getList}
            ></Pagination>
            <div className="col-12">
              <ul className="list-unstyled h-100">
                {!apiLoading ? (
                  notFollowingList && notFollowingList.length > 0 ? (
                    notFollowingList.map((data, index) => (
                      <li
                        className="manufacturerName"
                        key={index}
                        onClick={this.props.handleFollowManufacturer.bind(
                          this,
                          data
                        )}
                      >
                        {data.manufactureName}
                        <img src={Plus_Light_Icon} className="plusIcon"></img>
                      </li>
                    ))
                  ) : (
                    <div>No Manufacturer Found</div>
                  )
                ) : (
                  <div className="d-flex h-100 align-items-center justify-content-center">
                    <PageLoader size={30} loading={apiLoading} />
                  </div>
                )}
                {paginationLoader && (
                  <div className="text-center">
                    <PageBeatLoader size={8} loading={paginationLoader} />
                  </div>
                )}
              </ul>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .manufacturerSearchListModel {
              width: 25.622vw;
              padding: 0 1.098vw;
              position: relative;
              overflow: hidden;
            }
            .searchBox {
              background: ${BG_LightGREY_COLOR} !important;
              position: sticky;
              top: 0;
              z-index: 2;
            }
            .closeIcon {
              float: right;
              width: 0.658vw;
              cursor: pointer;
              padding: 1.098vw 0;
            }
            .heading {
              padding: 1.464vw 0 0 0;
              font-size: 1.245vw;
              color: ${GREY_VARIANT_1};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
            }
            :global(.manufacturerSearchListModel .searchInput) {
              position: relative;
              margin: 1.464vw 0;
            }
            :global(.manufacturerSearchListModel .searchInput .inputBox) {
              display: block;
              width: 100%;
              max-width: 100% !important;
              height: 2.635vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.manufacturerSearchListModel .searchInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.manufacturerSearchListModel
                .searchInput
                .inputBox::placeholder) {
              color: ${GREY_VARIANT_1};
              font-size: 0.878vw;
            }
            .searchIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.878vw;
            }
            .manufacturerListSec {
              background: ${WHITE_COLOR};
              height: 16.158vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .manufacturerListSec ul {
              margin: 0.366vw 0;
            }
            .manufacturerName {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              padding: 0.366vw 0.805vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              position: relative;
              align-items: center;
              cursor: pointer;
            }
            .manufacturerName:hover {
              background: ${BG_LightGREY_COLOR};
            }
            .manufacturerName:hover .plusIcon {
              display: block;
            }
            .plusIcon {
              position: absolute;
              right: 0.732vw;
              top: 0.659vw;
              width: 0.805vw;
              display: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
    paginationLoader: state.paginationLoader,
  };
};

export default connect(mapStateToProps)(ManufacturerSearchList);
