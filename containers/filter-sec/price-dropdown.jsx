import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import PriceContent from "./price-content";
import Tabs from "../../components/tabs/tabs";
import FinanceContent from "./finance-content";

class PriceDropdown extends Component {
  render() {
    const Finance = <FinanceContent />;
    const Price = (
      <PriceContent
        inputpayload={this.props.inputpayload}
        handleOnchangeInput={this.props.handleOnchangeInput}
        handlePriceRange={this.props.handlePriceRange}
      />
    );
    return (
      <Wrapper>
        <Tabs
          priceDropdown={true}
          tabs={[
            {
              label: `Price`,
            },
            {
              label: `Finance`,
            },
          ]}
          tabcontent={[{ content: Price }, { content: Finance }]}
        />
      </Wrapper>
    );
  }
}

export default PriceDropdown;
