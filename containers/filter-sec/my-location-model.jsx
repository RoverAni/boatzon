import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  THEME_COLOR_Opacity,
  GREY_VARIANT_1,
  GREEN_COLOR,
  WHITE_COLOR,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR,
} from "../../lib/config";
import LocationSearchInput from "../location/location-search-box";
// Google Map Components
import GoogleMapReact from "google-map-react";
import ButtonComp from "../../components/button/button";

const Marker = (props) => {
  const { color } = props;
  return <div className="marker" style={{ backgroundColor: color, cursor: "pointer" }} />;
};

let mapValue = "";

class MyLocationModel extends Component {
  state = {
    currentCenter: {},
    mapZoom: 11,
  };

  componentDidMount() {
    this.handleGetCurrentLocation();
  }

  handleGetCurrentLocation = () => {
    let currentCenter = this.state.currentCenter;
    currentCenter.lat = this.props.currentLocation && this.props.currentLocation.latitude;
    currentCenter.lng = this.props.currentLocation && this.props.currentLocation.longitude;
    this.setState({
      currentLat: currentCenter.lat,
      currentLng: currentCenter.lng,
      currentCenter,
      currentAddressData: this.props.currentLocation,
    });
  };

  updateUserAddress = (data) => {
    console.log("nvdkvn", data);
    if (data) {
      if (this.props && this.props.sendLocation) {
        this.props && this.props.setLocation(data);
      }
      let currentCenter = this.state.currentCenter;
      currentCenter.lat = data.lat;
      currentCenter.lng = data.lng;
      mapValue.setCenter(currentCenter);
      this.setState({
        currentLat: data.lat,
        currentLng: data.lng,
        currentCenter: currentCenter,
        currentAddressData: data,
      });
    } else {
      this.setState({ isAddressSelected: false });
    }
  };

  handleApiLoaded = (map, maps) => {
    mapValue = map;
  };

  render() {
    const {
      createPost,
      setCurrentLocation,
      onClose,
      handleCurrentAddAddress,
      handleAddAddress,
      updateUserAddress,
      sendLocation,
    } = this.props;
    const { currentAddressData, currentCenter, mapZoom, currentLat, currentLng } = this.state;
    return (
      <Wrapper>
        <div className="myLocationModel_Sec px-0">
          <div className="row searchBox mx-0">
            <div className="col-12 px-0">
              <div className="row mx-0">
                <div className="col-4 p-3">
                  <div className="locationsearchInput">
                    <LocationSearchInput
                      id="user-address-box"
                      updateLocation={this.updateUserAddress}
                      handleChange={this.updateUserAddress}
                      showOnlyAddress={true}
                      showAddressFromProps={true}
                      locationModel={true}
                    ></LocationSearchInput>
                  </div>
                  <div className="row map_btn_placements">
                    <div className="col-5">
                      <div className="cancel_btn">
                        <ButtonComp onClick={onClose}>Cancel</ButtonComp>
                      </div>
                    </div>
                    <div className="col-7 px-0">
                      <div className="submit_btn mr-3">
                        <ButtonComp
                          onClick={
                            setCurrentLocation
                              ? handleCurrentAddAddress.bind(this, currentAddressData)
                              : createPost
                              ? handleAddAddress.bind(this, currentAddressData)
                              : updateUserAddress.bind(this, currentAddressData)
                          }
                        >
                          {sendLocation ? "Send Location" : "Save"}
                        </ButtonComp>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-8 px-0">
                  <div className="MapSec">
                    <GoogleMapReact
                      bootstrapURLKeys={{
                        key: "AIzaSyAtrnJwdRbJXfbsH4fr28N1TJG64c7Lrc4",
                      }}
                      defaultCenter={currentCenter}
                      defaultZoom={mapZoom}
                      yesIWantToUseGoogleMapApiInternals
                      onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
                    >
                      <Marker lat={currentLat} lng={currentLng} color={THEME_COLOR_Opacity} />
                    </GoogleMapReact>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(0deg, rgba(17, 39, 56, 0.7), rgba(17, 39, 56, 0.7));
            }
            :global(.map_btn_placements) {
              margin-top: 22vw;
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: inherit !important;
            }
            .myLocationModel_Sec {
              width: 70vw;
              position: relative;
              background: ${WHITE_COLOR} !important;
            }
            .closeIcon {
              float: right;
              width: 0.732vw;
              cursor: pointer;
              padding: 1.098vw 0;
            }
            // .locationsearchInput{

            // }
            .heading {
              padding: 1.464vw 0 0 0;
              font-size: 1.245vw;
              color: ${GREY_VARIANT_1};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.022vw !important;
            }
            :global(.MapSec .gm-fullscreen-control) {
              height: 3.993vh !important;
              width: 1.83vw !important;
            }
            :global(.MapSec .gm-fullscreen-control img) {
              height: 2.236vh !important;
              width: 1.025vw !important;
            }
            :global(.MapSec .gm-bundled-control, .MapSec .gm-bundled-control-on-bottom) {
              bottom: 3.294vw !important;
              right: 2.196vw !important;
            }
            :global(.MapSec .gm-bundled-control > div > div button img, .MapSec .gm-bundled-control-on-bottom > div > div button img) {
              height: 2.076vh !important;
              width: 0.952vw !important;
            }
            :global(.MapSec .gm-bundled-control > div > div > div, .MapSec .gm-bundled-control-on-bottom > div > div > div) {
              margin: 0 !important;
            }
            :global(.MapSec .gm-bundled-control > div > div button, .MapSec .gm-bundled-control-on-bottom > div > div button) {
              width: 2.196vw !important;
              height: 3.993vh !important;
            }

            :global(.MapSec .gm-bundled-control > div > div, .MapSec .gm-bundled-control-on-bottom > div > div) {
              width: 2.196vw !important;
              height: 7.987vh !important;
            }
            .MapSec {
              width: 100%;
              height: 70vh;
              position: relative;
              // margin: 1.098vw 0;
            }
            :global(.marker) {
              position: absolute;
              top: 50%;
              left: 50%;
              width: 5.124vw;
              height: 11.182vh;
              background-color: #000;
              border-radius: 100%;
              user-select: none;
              transform: translate(-50%, -50%);
            }
            :global(.marker:hover) {
              z-index: 1;
            }
            .submit_btn .cancel_btn {
              width: 100%;
            }
            :global(.cancel_btn button) {
              width: 100%;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0 0 1.098vw 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.submit_btn button) {
              width: 100%;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0 0 1.098vw 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.submit_btn button span) {
              font-weight: 600;
              font-size: 1.098vw;
              font-family: "Museo-Sans" !important;
            }
            :global(.cancel_btn button span) {
              font-weight: 400;
              font-size: 1.098vw;
              font-family: "Museo-Sans" !important;
            }
            :global(.submit_btn button:focus),
            :global(.submit_btn button:active),
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.submit_btn button:hover),
            :global(.cancel_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(MyLocationModel);
