import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Banner_HandImg,
  FONTGREY_COLOR,
  GREY_VARIANT_1,
  App_Store,
  Google_Play,
} from "../../lib/config";

class EmptyMessenger extends Component {
  render() {
    return (
      <Wrapper>
        <div className="EmptyMessagePage">
          <div className="contentDiv">
            <h3>Get the app and post something today!</h3>
            <p>Download our top-rated iOS or Android app to get started </p>
            <div className="d-flex justify-content-between downloadBtnSec">
              <img src={App_Store}></img>
              <img src={Google_Play}></img>
            </div>
          </div>
        </div>
        <img src={Banner_HandImg} className="handImg"></img>

        <style jsx>
          {`
            .EmptyMessagePage {
              display: flex;
              align-items: center;
              justify-content: flex-end;
              height: 100%;
            }
            .handImg {
              width: 17.569vw;
              position: absolute;
              bottom: 0;
              left: 3.66vw;
            }
            .contentDiv {
              width: 50%;
              margin-right: 2.562vw;
            }
            .contentDiv h3 {
              font-size: 1.61vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin: 0 0 0.366vw 0;
              color: ${FONTGREY_COLOR};
            }
            .contentDiv p {
              font-size: 0.951vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              max-width: 85%;
            }
            .downloadBtnSec {
              width: 16.837vw;
              margin: 0.732vw 0 0 0;
            }
            .downloadBtnSec img {
              width: 8.052vw;
              cursor: pointer;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EmptyMessenger;
