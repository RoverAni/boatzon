import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { ArchiveChatListData } from "../../fixtures/my-offer/my-offer";
import { findDayAgo } from "../../lib/date-operation/date-operation";
import {
  Selling_Green_Icon,
  Buying_Blue_Icon,
  Dark_Blue_Color,
  THEME_COLOR,
  GREY_VARIANT_1,
  Archive_Blue_Icon,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";

class AllArchiveChatList extends Component {
  state = {
    activeMsgId: 0,
  };

  componentDidMount() {
    if (this.state.activeMsgId == 0) {
      //   this.props.handleSelectedMsg(ArchiveChatListData[0]);
    }
  }

  handleActiveMsg = (data) => {
    console.log("fsds", data);
    this.setState(
      {
        activeMsgId: data.id,
      },
      () => {
        // this.props.handleSelectedMsg(data);
      }
    );
  };

  render() {
    const { activeMsgId } = this.state;
    return (
      <Wrapper>
        {ArchiveChatListData &&
          ArchiveChatListData.map((data, index) => (
            <div
              className={
                activeMsgId == index
                  ? "col-12 ActiveChatListCard"
                  : "col-12 ChatListCard"
              }
              onClick={this.handleActiveMsg.bind(this, data)}
            >
              <div className="section">
                <div className="row m-0 align-items-center justify-content-between">
                  <div className="col-6 p-0">
                    <div className="d-flex align-items-center">
                      <img src={data.profile_pic} className="profilePic"></img>
                      <div>
                        <h6 className="messengerName">{data.name}</h6>
                        <p className="manufacturerName">
                          {data.manufacture_name}
                        </p>
                        <p className="chatDate">
                          {findDayAgo(data.msg_date)}{" "}
                          <img
                            src={
                              data.product_type == "selling"
                                ? `${Selling_Green_Icon}`
                                : `${Buying_Blue_Icon}`
                            }
                            className="productType"
                          ></img>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="d-flex align-items-center position-relative justify-content-end">
                      <div className="unarchive_btn">
                        <ButtonComp>
                          <img
                            src={Archive_Blue_Icon}
                            className="archiveIcon"
                          ></img>
                          Unarchive This Offer
                        </ButtonComp>
                      </div>
                      <img src={data.product_img} className="productImg"></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        <style jsx>
          {`
            .section {
              margin: 0 0.732vw;
              padding: 0.732vw;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .ChatListCard:last-child .section {
              border: none;
            }
            :global(.unarchive_btn button) {
              min-width: fit-content !important;
              width: 100%;
              padding: 0.366vw 1.098vw 0.366vw 0;
              background: transparent;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.unarchive_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.unarchive_btn button:focus),
            :global(.unarchive_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.unarchive_btn button:hover) {
              background: transparent;
            }
            .profilePic {
              width: 2.928vw;
              object-fit: cover;
              margin: 0 0.585vw;
            }
            .archiveIcon {
              width: 0.732vw;
              margin-right: 0.366vw;
            }
            .ChatListCard {
              padding: 0 0.732vw !important;
              cursor: pointer;
            }
            .ActiveChatListCard {
              padding: 0 0.732vw !important;
              cursor: pointer;
              background: ${BG_LightGREY_COLOR};
            }
            .ActiveChatListCard .section {
              border-bottom: none;
            }
            .messengerName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${Dark_Blue_Color};
              margin: 0;
            }
            .manufacturerName {
              font-size: 0.607vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              margin: 0;
            }
            .chatDate {
              font-size: 0.512vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .productType {
              width: 0.658vw;
              margin-bottom: 0.146vw;
              margin-left: 0.219vw;
            }
            .productImg {
              width: 3.66vw;
              height: 2.415vw;
              border-radius: 0.219vw;
              object-fit: cover;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default AllArchiveChatList;
