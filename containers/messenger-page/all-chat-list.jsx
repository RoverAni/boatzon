import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { findDayAgo } from "../../lib/date-operation/date-operation";
import {
  Dark_Blue_Color,
  THEME_COLOR,
  GREY_VARIANT_1,
  Archive_Grey_Icon,
  BG_LightGREY_COLOR,
  GREEN_COLOR,
  Archive_Blue_Filled_Icon,
  PlaceHolder_Boat,
  Placeholder_Image,
} from "../../lib/config";
import { userSelectedToChat, storeAllChatData, removeSelectedUserAllMessages, onScrollInit } from "../../redux/actions/chat/chat";
import { connect } from "react-redux";
import RequestVideoCallModal from "../boat-details/video-call-req";
import Model from "../../components/model/model";
import { getAllRequestForAVCalls, getStatusForAVCalls, getCallID } from "../../services/calling";

class AllChatList extends Component {
  state = {
    activeMsgId: 0,
    currentPageSize: 20,
    timestamp: "",
    promptForAcceptOrRejectRequest: false,
    requestStatus: "",
    showModal: false,
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  componentDidMount() {
    if (this.props.UserSelectedToChat && this.props.UserSelectedToChat.recipientId) {
      this.setState({ activeMsgId: this.props.UserSelectedToChat.recipientId });
      this.props.onScrollCallApi(new Date().getTime(), this.props.UserSelectedToChat.chatId, this.state.currentPageSize);
    }
    /** this function is to check whether, the object is present in array not. if length > 0 i.e data is available
     * else it will be added into chats
     */
    let prevChatData = [...this.props.allChats];
    if (Object.keys(this.props.boatData).length !== 0) {
      let checkIfUserExists = prevChatData.findIndex((k) => k.recipientId === this.props.boatData.memberMqttId);
      console.log("checkIfUserExists", checkIfUserExists);
      if (checkIfUserExists === -1) {
        let addNewChat = { ...this.props.boatData };
        addNewChat["userName"] = this.props.boatData.memberFullName;
        addNewChat["timestamp"] = this.props.boatData.postedOn;
        addNewChat["profilePic"] = this.props.boatData.memberProfilePicUrl;
        addNewChat["productImage"] = this.props.boatData.thumbnailImageUrl;
        addNewChat["recipientId"] = this.props.boatData.memberMqttId;
        addNewChat["secretId"] = this.props.boatData.postId.toString();
        this.props.updateOrAppendNewChat([...this.props.allChats, addNewChat]);
        this.handleActiveMsg(addNewChat);
        this.setState({ timestamp: this.props.boatData.postedOn });
      }
    }
  }

  GetAllRequestForAVCalls = (fromId) => {
    getAllRequestForAVCalls(fromId)
      .then((res) => {
        let guyWithRequest = { ...this.props.UserSelectedToChat };
        guyWithRequest["callStatus"] = res.data.data.status;
        guyWithRequest["callFrom"] = res.data.data.fromId;
        guyWithRequest["_id"] = res.data.data._id;
        this.getCallID(res.data.data._id);
        this.props.selectUserToChat(guyWithRequest);
        if (res.data.data.status === 0) {
          this.setState({ showModal: !this.state.showModal });
        }
      })
      .catch((err) => console.log("err", err));
  };

  getCallID = (callId) => {
    getCallID(callId)
      .then((res) => {
        console.log("callID res", res);
      })
      .catch((err) => console.log("callID err", err));
  };

  /** if status
   * 1. the buyer has requested so prompt is shown
   * 2. you have accepted the request
   * 3. rejected request
   */
  GetStatusForAVCalls = (toId) => {
    getStatusForAVCalls(toId)
      .then((res) => {
        let guyWithRequest = { ...this.props.UserSelectedToChat };
        guyWithRequest["meAsABuyerCallRequest"] = res.data.data.status;
        guyWithRequest["meAsABuyerCallWith"] = res.data.data.toId;
        guyWithRequest["meAsABuyerCallID"] = res.data.data._id;
        this.props.selectUserToChat(guyWithRequest);
        // this.getCallID(res.data.data._id);
      })
      .catch((err) => console.log("err", err));
  };

  /** this function stores the recipientId of the active user in order to keep it marked active */
  /**
   * this.state.timestamp !== data.timestamp prevents API to get called again and again on user click.
   */
  handleActiveMsg = (data) => {
    console.log("data", data);
    if (this.state.timestamp !== data.timestamp) {
      this.setState(
        {
          activeMsgId: data.timestamp,
          timestamp: data.timestamp,
        },
        () => {
          this.props.handleSelectedMsg(data);
          this.props.selectUserToChat(data);
          setTimeout(() => {}, 300);
          this.props.onScrollCallApi(new Date().getTime(), data.chatId, this.state.currentPageSize);
          this.GetAllRequestForAVCalls(data.recipientId);
          this.GetStatusForAVCalls(data.recipientId);
        }
      );
      console.log("call status", this.props.UserSelectedToChat.callStatus);
    }
  };

  /** inbuilt method triggers when user selects other 2nd or any other compared to the previous/first user chatting with
   * and wipes out all chat for selected user
   */
  componentDidUpdate(prevProps, prevState) {
    if (this.state.timestamp !== prevState.timestamp && this.props.allMessagesForSelectedUser.length > 0) {
      this.props.removeSelectedUserAllMessages();
      setTimeout(() => {}, 300);
    }
  }

  onErrorBoat = (e) => {
    e.target.src = PlaceHolder_Boat;
  };

  onErrorUser = (e) => {
    e.target.src = Placeholder_Image;
  };

  render() {
    const { activeMsgId } = this.state;
    console.log("activeMsgId", activeMsgId);
    return (
      <Wrapper>
        {this.props && this.props.allChats.length > 0 ? (
          this.props.allChats.map((data, index) => (
            <div
              key={index}
              className={activeMsgId == data.timestamp ? "col-12 p-0 ActiveChatListCard" : "col-12 p-0 ChatListCard"}
              onClick={this.handleActiveMsg.bind(this, data)}
            >
              <div className="row m-0 align-items-center">
                <div className="col-3 pr-2 pl-0 profilePicSec">
                  <img
                    src={data.profilePic || data.userImage || Placeholder_Image}
                    alt={data.userName || data.name}
                    onError={this.onErrorUser}
                    className="profilePic"
                  ></img>
                </div>
                <div className="col p-0">
                  {/* <h6 className="messengerName">{data.manufacturer_name}</h6> */}
                  <h6 className="messengerName">{data.productName}</h6>
                  <p className="manufacturerName">{data.userName || data.name}</p>
                  <p className="chatDate">{findDayAgo(data.timestamp)} </p>
                </div>
                <div className="col-3 p-0">
                  <div className="d-flex align-items-center position-relative">
                    <img
                      src={data.productImage}
                      alt={data.productName || data.name}
                      onError={this.onErrorBoat}
                      className="productImg"
                    ></img>
                    <div className="archiveDiv"></div>
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <div className="col-12 text-center pt-3"></div>
        )}
        {/* {this.props.UserSelectedToChat.callStatus === 0 && this.props.UserSelectedToChat.meAsABuyerCallRequest === 1 ? ( */}
        <Model open={this.state.showModal} onClose={this.toggleModal}>
          <RequestVideoCallModal type="AsSellerAcceptIncomingCallRequest" onClose={this.toggleModal} />
        </Model>
        <style jsx>
          {`
            .profilePicSec {
              max-width: 4.026vw;
              padding-left: 0.366vw !important;
            }
            .profilePic {
              width: 100%;
              object-fit: cover;
            }
            .ChatListCard {
              padding: 0.732vw 0 !important;
              cursor: pointer;
              border-left: 0.146vw solid ${THEME_COLOR};
            }
            .ActiveChatListCard {
              padding: 0.732vw 0 !important;
              cursor: pointer;
              background: ${BG_LightGREY_COLOR};
              border-left: 0.146vw solid ${GREEN_COLOR};
            }
            .messengerName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              color: ${Dark_Blue_Color};
              margin: 0 0 0.219vw 0;
              max-width: 90%;
            }
            .manufacturerName {
              font-size: 0.658vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              color: ${THEME_COLOR};
              margin: 0;
            }
            .chatDate {
              font-size: 0.549vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .chatDate:first-letter {
              text-transform: capitalize;
            }
            .productImg {
              width: 3.66vw;
              height: 2.415vw;
              border-radius: 0.219vw;
              object-fit: cover;
            }
            .archiveDiv {
              width: 1.024vw;
              height: 0.951vw;
              cursor: pointer;
              margin-left: 0.512vw;
              background: url(${Archive_Grey_Icon}) 0 0 / cover no-repeat;
              background-size: 75%;
            }
            .archiveDiv:hover {
              background: url(${Archive_Blue_Filled_Icon}) 0 0 / cover no-repeat;
              background-size: 75%;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allChats: state.allChats,
    boatData: state.boatData,
    allMessagesForSelectedUser: state.allMessagesForSelectedUser,
    UserSelectedToChat: state.userSelectedToChat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectUserToChat: (userObj) => dispatch(userSelectedToChat(userObj)),
    updateOrAppendNewChat: (chatObj) => dispatch(storeAllChatData(chatObj)),
    removeSelectedUserAllMessages: () => dispatch(removeSelectedUserAllMessages()),
    onScrollCallApi: (ts, cid, index) => dispatch(onScrollInit(ts, cid, index)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllChatList);
