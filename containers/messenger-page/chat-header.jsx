import React, { Component } from "react";
import { Border_LightGREY_COLOR, BG_LightGREY_COLOR, GREY_VARIANT_1, Menu_Icon_Grey } from "../../lib/config";
import { connect } from "react-redux";
import Wrapper from "../../hoc/Wrapper";
import Fade from "react-reveal/Fade";
import Button from "../../components/button/customButton";
import * as assets from "../../lib/config";
import Model from "../../components/model/model";
import MyLocationModel from "../filter-sec/my-location-model";
import { getCookie } from "../../lib/session";
import { messageSubject } from "../../lib/mqtt/subjects";
import { handleImageUpload } from "../../lib/image/generateThumbnail";
import { __BlobUpload } from "../../lib/image/cloudinaryUpload";
import RequestVideoCallModal from "../boat-details/video-call-req";

class ChatHeader extends Component {
  constructor(props) {
    super(props);
    this.node = React.createRef();
    this.myRef = React.createRef();
    this.state = {
      ChatData: {},
      dropdown: false,
      modal: false,
      locationObj: {},
      requestModal: false,
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  setLocation = (obj) => this.setState({ locationObj: obj });

  toggleModal = () => this.setState({ modal: !this.state.modal });

  toggleDropDown = () => this.setState({ dropdown: !this.state.dropdown });

  componentDidMount() {
    setInterval(() => {
      this.setState({
        ChatData: this.props.selectedMsgData,
      });
    }, 10);
    document.addEventListener("mousedown", this.handleDropDown, true);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleDropDown, true);
  }

  handleDropDown = (e) => {
    if (this.state.dropdown) {
      if (this.node.contains(e.target)) {
        return;
      }
      this.toggleDropDown();
    }
  };

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
      this.toggleDropDown();
    }
  }

  /** function to send image data over mqtt, type "1" */
  sendImage = () => {};

  /** function to convert message to location payload acceptable for app device  */
  createLocationToSend = (lat, lng, address, completeAddress) => {
    return `(${lat},${lng})@@${address}@@${completeAddress}`;
  };

  /** function to send location data over mqtt, type "3" */
  handleCurrentAddAddress = () => {
    let encodedText = btoa(
      this.createLocationToSend(
        this.state.locationObj.lat,
        this.state.locationObj.lng,
        this.state.locationObj.area,
        this.state.locationObj.lat
      )
    );
    let { secretId, recipientId, chatId } = this.props.userSelectedToChat;
    let { profilePicUrl, fullName } = this.props.userProfileData;
    let obj = {
      from: getCookie("mqttId"),
      id: new Date().getTime().toString(),
      name: fullName,
      payload: encodedText,
      receiverIdentifier: "",
      secretId: secretId,
      timestamp: new Date().getTime(),
      to: recipientId,
      toDocId: "",
      type: "3",
      userImage: profilePicUrl,
      chatId: chatId,
    };
    messageSubject.next(obj);
    this.setState({
      modal: false,
    });
  };

  /** function to send location data over mqtt, type "1" */
  uploadImage = async (file) => {
    let { secretId, recipientId, chatId } = this.props.userSelectedToChat;
    let { profilePicUrl, fullName } = this.props.userProfileData;
    let f = await handleImageUpload(file);
    __BlobUpload(file[0])
      .then((res) => {
        console.log("image upload success");
        let obj = {
          from: getCookie("mqttId"),
          id: new Date().getTime().toString(),
          name: fullName,
          payload: btoa(res.body.secure_url),
          receiverIdentifier: "",
          secretId: secretId,
          timestamp: new Date().getTime(),
          to: recipientId,
          dataSize: file[0].size,
          toDocId: "",
          thumbnail: f.thumb,
          type: "1",
          userImage: profilePicUrl,
          chatId: chatId,
        };
        console.log("sending image", obj);
        messageSubject.next(obj);
      })
      .catch((err) => {
        console.log("err uploading image", err);
      });
  };

  renderVideoCallButtonBasedOnProps = () => {
    if (
      (this.props.userSelectedToChat.callStatus === 1 && this.props.userSelectedToChat.meAsABuyerCallRequest === 1) ||
      (this.props.userSelectedToChat.offerType === "1" && this.props.userSelectedToChat.initiated)
    ) {
      return (
        <div className="video_call" onClick={this.toggleRequestModal}>
          <Button text={<img src={assets.VideoCall} alt="video-call" height={25} />} />
        </div>
      );
    }
  };

  toggleRequestModal = () => this.setState({ requestModal: !this.state.requestModal });

  render() {
    const { ChatData } = this.state;
    return (
      <Wrapper>
        <div className="col-12 Header">
          {/* <form encType="multipart/form-data">
            <label htmlFor="File"> */}
          <input
            type="file"
            hidden={true}
            id="File"
            accept="image/png, image/jpeg, video/mp4"
            ref={(fileInputEl) => (this.fileInputEl = fileInputEl)}
            onChange={(event) => this.uploadImage(event.target.files)}
          />
          {/* </label>
          </form> */}
          <div className="row m-0 align-items-center justify-content-between">
            <div className="d-flex align-items-center">
              <div className="profilePicSec">
                <img
                  src={this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.profilePic}
                  alt={this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.userName}
                  className="profilePic"
                ></img>
              </div>
              <div className="MessengerInfo">
                <h6 className="messengerName">{this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.userName}</h6>
                <p className="messengerLocation">{ChatData ? ChatData.location : ""}</p>
              </div>
            </div>
            <div className="d-flex align-items-center">
              {this.renderVideoCallButtonBasedOnProps()}

              {/** this is to check, if user selected for chat active, then only the dropdown is visible */}
              {!!this.props.userSelectedToChat.profilePic ? (
                <div className="enabledbutton">
                  <Button
                    text={
                      <>
                        <img src={assets.EnabledAttachment} alt="button" height={25} />
                        <div ref={(node) => (this.node = node)}>
                          <Fade right cascade when={this.state.dropdown} duration={200}>
                            <div>
                              {this.state.dropdown ? (
                                <div className="visibleDropdown">
                                  <Button
                                    className={"visibleDropdown_el"}
                                    handler={this.toggleModal}
                                    text={<img src={assets.ImageAttachment} alt="send-image" height={30} width={30} />}
                                  />
                                  <Button
                                    className={"visibleDropdown_el"}
                                    handler={(e) => {
                                      this.fileInputEl.click();
                                    }}
                                    text={<img src={assets.MapAttachment} alt="send-location" height={30} width={30} />}
                                  />
                                </div>
                              ) : (
                                <span />
                              )}
                            </div>
                          </Fade>
                        </div>
                      </>
                    }
                    handler={this.toggleDropDown}
                  />
                </div>
              ) : (
                <></>
              )}
              <section>
                <div>
                  <p className="offerProductPrice">
                    {this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.productPrice}
                  </p>
                </div>
                <div>
                  <p className="manufacturerName">
                    {this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.productName}
                  </p>
                </div>
              </section>

              <img
                src={this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.productImage}
                alt={this.props && this.props.userSelectedToChat && this.props.userSelectedToChat.productName}
                className="productImg"
              ></img>
              <img src={Menu_Icon_Grey} className="menuIcon" onClick={this.props.handleDrawerOpen}></img>
            </div>
          </div>
          <Model open={this.state.modal} onClose={this.toggleModal}>
            <MyLocationModel
              setCurrentLocation={true}
              onClose={this.toggleModal}
              setLocation={this.setLocation}
              handleCurrentAddAddress={this.handleCurrentAddAddress}
              sendLocation={true}
            />
          </Model>
          <Model open={this.state.requestModal} onClose={this.toggleRequestModal}>
            {(this.props.userSelectedToChat.offerType === "1" &&
              this.props.userSelectedToChat.initiated &&
              this.props.userSelectedToChat.callStatus === 1) ||
            (this.props.userSelectedToChat.offerType === "1" && this.props.userSelectedToChat.initiated) ? (
              <RequestVideoCallModal type="AcceptOrReject" onClose={this.toggleRequestModal} />
            ) : (
              <RequestVideoCallModal type="ChatRequest" onClose={this.toggleRequestModal} />
            )}
          </Model>
        </div>
        <style jsx>
          {`
            .Header {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
              padding: 0.732vw 0;
            }
            .profilePicSec {
              max-width: 2.562vw;
            }
            .profilePic {
              width: 100%;
              margin: 0;
            }
            .MessengerInfo {
              padding: 0.732vw 1.098vw 0.732vw 0.732vw;
              border-right: 0.0732vw solid ${BG_LightGREY_COLOR};
            }
            .messengerName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .messengerLocation {
              font-size: 0.658vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .manufacturerName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_1};
              margin: 0 0 0.219vw 0;
            }
            .offerProductPrice {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              color: #484848;
              margin: 0 0 0.219vw 0;
            }
            .productImg {
              width: 3.294vw;
              height: 100%;
              margin: 0 0.732vw;
              object-fit: cover;
            }
            .menuIcon {
              width: 1.098vw;
              padding: 0.732vw 0;
              cursor: pointer;
              position: relative;
            }
            :global(.enabledbutton button) {
              border: none !important;
              background: none !important;
            }
            :global(.enabledbutton button .visibleDropdown) {
              position: absolute;
              z-index: 1000;
              top: 1.45vw;
              right: 13vw;
              // top: 3.66vw;
              // display: flex !important;
              // flex-direction: column !important;
            }
            :global(.enabledbutton
                button
                .visibleDropdown
                button.visibleDropdown_el) {
              padding: 0 0.439vw;
              z-index: 999;
            }
            :global(.video_call button) {
              border: none !important;
              background: none !important;
            }
            // media query
            // @media only screen and (min-width: 768px) and (max-width: 992px) {
            //   .profilePicSec {
            //     max-width: 25px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 9px;
            //   }
            //   .messengerLocation {
            //     font-size: 7px;
            //   }
            //   .productImg {
            //     width: 38px;
            //   }
            //   .menuIcon {
            //     width: 13px;
            //   }
            // }
            // media query
            // @media only screen and (min-width: 993px) and (max-width: 1199px) {
            //   .profilePicSec {
            //     max-width: 30px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 10px;
            //   }
            //   .messengerLocation {
            //     font-size: 8px;
            //   }
            //   .productImg {
            //     width: 40px;
            //   }
            //   .menuIcon {
            //     width: 14px;
            //   }
            // }
            // media query
            // @media only screen and (min-width: 1200px) and (max-width: 1400px) {
            //   .profilePicSec {
            //     max-width: 35px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 11px;
            //   }
            //   .messengerLocation {
            //     font-size: 9px;
            //   }
            //   .productImg {
            //     width: 45px;
            //   }
            //   .menuIcon {
            //     width: 15px;
            //   }
            // }
            // media query
            // @media only screen and (min-width: 1920px) {
            //   .profilePicSec {
            //     max-width: 60px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 15px;
            //   }
            //   .messengerLocation {
            //     font-size: 13px;
            //   }
            //   .productImg {
            //     width: 70px;
            //   }
            //   .menuIcon {
            //     width: 22px;
            //   }
            }
            // // media query
            // @media only screen and (min-width: 768px) and (max-width: 992px) {
            //   .profilePicSec {
            //     max-width: 25px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 9px;
            //   }
            //   .messengerLocation {
            //     font-size: 7px;
            //   }
            //   .productImg {
            //     width: 38px;
            //   }
            //   .menuIcon {
            //     width: 13px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 993px) and (max-width: 1199px) {
            //   .profilePicSec {
            //     max-width: 30px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 10px;
            //   }
            //   .messengerLocation {
            //     font-size: 8px;
            //   }
            //   .productImg {
            //     width: 40px;
            //   }
            //   .menuIcon {
            //     width: 14px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 1200px) and (max-width: 1400px) {
            //   .profilePicSec {
            //     max-width: 35px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 11px;
            //   }
            //   .messengerLocation {
            //     font-size: 9px;
            //   }
            //   .productImg {
            //     width: 45px;
            //   }
            //   .menuIcon {
            //     width: 15px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 1920px) {
            //   .profilePicSec {
            //     max-width: 60px;
            //   }
            //   .messengerName,
            //   .manufacturerName {
            //     font-size: 15px;
            //   }
            //   .messengerLocation {
            //     font-size: 13px;
            //   }
            //   .productImg {
            //     width: 70px;
            //   }
            //   .menuIcon {
            //     width: 22px;
            //   }
            // }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userSelectedToChat: state.userSelectedToChat,
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps, null)(ChatHeader);
