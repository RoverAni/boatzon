import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  Close_Grey_Thin_Icon,
  GREEN_COLOR,
  Boat_3_IMG,
  BG_LightGREY_COLOR,
  Map_Marker_Green,
  Border_LightGREY_COLOR,
  WHITE_COLOR,
} from "../../lib/config";
import Router from "next/router";
import ButtonComp from "../../components/button/button";

class ProductInfo extends Component {
  navigateToDetailsPage = () => {
    let { messageId, productName, category, productId } = this.props.selectedMsgData;
    if (category === "Products") {
      Router.push(`/product-detail/${productName.replace(/ /g, "-")}?pid=${productId}`);
    } else {
      Router.push(`/boat-detail/${productName.replace(/ /g, "-")}?pid=${productId}`);
    }
  };
  render() {
    return (
      <Wrapper>
        <div className="col-12 p-0 ProductInforPage">
          <div className="d-flex">
            <h4 className="productName">2019 Ocean Alexander 120 Megayacht</h4>
            <img src={Close_Grey_Thin_Icon} className="closeIcon" onClick={this.props.onClose}></img>
          </div>
          <p className="postedDetails">
            Posted 2 months ago in <span>Boats / Pleasure-Boats</span>
          </p>
          <img src={Boat_3_IMG} className="productImg"></img>

          <div className="row m-0 align-items-center borderBottom">
            <div className="col-6 p-0 pr-1">
              <div className="price_btn">
                <ButtonComp>
                  <span id="priceCircle"></span>$130,000
                </ButtonComp>
              </div>
            </div>
            <div className="col-6 p-0 pl-1">
              <p className="pickupLocation">
                <img src={Map_Marker_Green}></img>
                <a target="_blank">Clearwater, FL</a>
              </p>
            </div>
          </div>
          <div className="position-relative">
            <h6 className="boatDescTitle">Boat Description</h6>
            <p className="subTitle">Fuel System:</p>
            <p className="desc">
              The fuel oil storage tanks are FRP integral, construction with aluminum baffling and hydro-tested to ABS requirements. The
              fuel day tanks and aft trim tanks are fabricated from aluminum; formed, welded and hydro-tested to ABS requirements.
            </p>
            <p className="subTitle">Gasoline System Main Components:</p>
            <p className="desc">The auxiliary ga</p>
            <div className="fadeSec"></div>
          </div>
          <div className="viewDetails_btn">
            <ButtonComp onClick={this.navigateToDetailsPage}>View Details</ButtonComp>
          </div>
        </div>
        <style jsx>
          {`
            .ProductInforPage {
              padding: 1.098vw 0.732vw !important;
            }
            .closeIcon {
              width: 0.732vw;
              cursor: pointer;
              position: absolute;
              top: 1.244vw;
              right: 0.732vw
            }
            .productName {
              font-size: 1.098vw;
              text-transform: capitalize;
              font-weight: 600;
              max-width: 90%
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0 0 0.366vw 0;
            }
            .postedDetails {
              margin: 0;
              font-size: 0.732vw;
              line-height: 1.2;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            .postedDetails span {
              color: ${GREEN_COLOR};
              font-size: 0.732vw;
            }
            .productImg {
              width: 100%;
              height: 8.784vw;
              object-fit: cover;
              margin: 0.732vw 0;
            }
            :global(.price_btn button) {
              width: 90%;
              padding:  0.146vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-top-left-radius: 1.464vw !important;
              border-bottom-left-radius: 1.464vw !important;
            }
            :global(.price_btn button #priceCircle) {
              width: 0.512vw;
              height: 0.512vw;
              border-radius: 50%;
              background: ${BG_LightGREY_COLOR};
              margin-right: 0.585vw;
            }
            :global(.price_btn button span) {
              font-family: "Open Sans-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.price_btn button:focus),
            :global(.price_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.price_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .borderBottom {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
              padding: 0 0 0.732vw 0 !important;
            }
            .pickupLocation {
              display: flex;
              align-items: center;
              justify-content: flex-end;
              margin: 0;
              padding:  0.146vw;
            }
            .pickupLocation img {
              width: 0.585vw;
              margin-right: 0.219vw;
            }
            .pickupLocation a {
              color: ${GREEN_COLOR};
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
            }
            .boatDescTitle {
              font-size: 1.024vw;
              font-family: "Open Sans-Semibold" !important;
              color: ${FONTGREY_COLOR};
              padding: 0.732vw 0;
              margin: 0;
            }
            .subTitle {
              font-size: 0.878vw;
              font-family: "Open Sans-Semibold" !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              line-height: 1.2;
            }
            .desc {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0.146vw 0 0.585vw 0;
            }
            .fadeSec {
              position: absolute;
              bottom: 0;
              width: 100%;
              height: 6.002vw;
              background: linear-gradient(
                360deg,
                rgba(255, 255, 255, 0.979167) 11.84%,
                rgba(255, 255, 255, 0) 151.32%
              );
            }
            :global(.viewDetails_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              background: ${WHITE_COLOR};
              color: ${GREEN_COLOR};
              margin: 0;
              border: 0.0732vw solid ${GREEN_COLOR};
            }
            :global(.viewDetails_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
              text-transform: capitalize;
            }
            :global(.viewDetails_btn button:focus),
            :global(.viewDetails_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.viewDetails_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProductInfo;
