// Main React Components
import React, { Component } from "react";
// materail-UI Components
import { withStyles } from "@material-ui/core/styles";

import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import ChatHeader from "./chat-header";
import ChatDetailsPage from "./chat-details-page";
import ProductInfo from "./product-info";

const styles = (theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  appBar: {
    position: "absolute !important",
    left: "unset !important",
    right: "unset !important",
    background: "transparent !important",
    zIndex: "0 !important",
    boxShadow: "none !important",
  },
  title: {
    flexGrow: 1,
  },
  hide: {
    display: "none",
  },
  drawer: {
    flexShrink: 0,
    width: "16.105vw",
  },
  drawerPaper: {
    position: "absolute !important",
    left: "unset !important",
    right: "unset !important",
    zIndex: "0 !important",
    transition: "unset !important",
    borderLeft: "none !important",
    boxShadow: "0px 0px 3px rgba(0, 0, 0, 0.1)",
    borderRadius: "0px 3px 3px 0px",
    width: "16.105vw",
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-start",
  },
  content: {
    flexGrow: 1,
    padding: "0.512vw",
    marginRight: "-16.105vw",
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginTop: "4.978vw",
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 0,
  },
});

class PersistentDrawerRight extends Component {
  state = {
    open: true,
  };

  handleDrawerToggle = () => {
    this.setState({
      open: !this.state.open,
    });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar className={clsx(classes.appBar)}>
          <Toolbar className="px-2">
            <ChatHeader selectedMsgData={this.props.selectedMsgData} handleDrawerOpen={this.handleDrawerToggle} />
          </Toolbar>
        </AppBar>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open,
          })}
        >
          <ChatDetailsPage selectedMsgData={this.props.selectedMsgData} />
        </main>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
          onClose={this.handleDrawerToggle}
        >
          <ProductInfo selectedMsgData={this.props.selectedMsgData} onClose={this.handleDrawerToggle} />
        </Drawer>
      </div>
    );
  }
}

export default withStyles(styles)(PersistentDrawerRight);
