import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  NoArchive_Img,
  GREEN_COLOR,
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";

class NoArchiveChat extends Component {
  render() {
    return (
      <Wrapper>
        <div className="col-12">
          <div className="noArchivePage">
            <div className="text-center">
              <img src={NoArchive_Img} className="noArchiveImg"></img>
              <p className="noArchiveMsg">No archived items</p>
              <div className="dismiss_btn">
                <ButtonComp>Dismiss</ButtonComp>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .noArchiveImg {
              width: 9.516vw;
              object-fit: cover;
            }
            .noArchivePage {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 31.112vw;
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .noArchiveMsg {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            :global(.dismiss_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0 0 0.585vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.dismiss_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.dismiss_btn button:focus),
            :global(.dismiss_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.dismiss_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default NoArchiveChat;
