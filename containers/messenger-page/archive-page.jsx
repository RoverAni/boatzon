import React, { Component } from "react";
import {
  Long_Arrow_Left,
  WHITE_COLOR,
  Selling_Grey_Icon,
  Cart_Grey_Icon,
  FONTGREY_COLOR,
  View_Blue_Filled_Icon,
  THEME_COLOR,
  GREY_VARIANT_2,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import Wrapper from "../../hoc/Wrapper";
import AllArchiveChatList from "./all-archive-chat-list";
import ButtonComp from "../../components/button/button";
import NoArchiveChat from "./no-archive-chat";

class ArchivePage extends Component {
  state = {
    noArchiveChat: false,
  };
  render() {
    const All = this.state.noArchiveChat ? (
      <NoArchiveChat />
    ) : (
      <AllArchiveChatList />
    );
    const Selling = this.state.noArchiveChat ? "" : <h1>Selling</h1>;
    const Buying = this.state.noArchiveChat ? "" : <h1>Buying</h1>;
    return (
      <Wrapper>
        <div className="col-11 mx-auto">
          <div className="back_btn">
            <ButtonComp onClick={this.props.handleBackScreen}>
              <img src={Long_Arrow_Left} className="backarrowIcon"></img> Back
              to Messenger
            </ButtonComp>
          </div>
          <div className="ArchivePage">
            <h6 className="heading">Archive</h6>
            <div className="position-relative">
              <div className="viewArchive_btn">
                <ButtonComp onClick={this.props.handleBackScreen}>
                  <img src={View_Blue_Filled_Icon} className="viewIcon"></img>
                  View Messenger
                </ButtonComp>
              </div>
              <Tabs
                chattabs={true}
                archivechat={true}
                tabs={[
                  {
                    label: <span>All</span>,
                  },
                  {
                    label: <span>Selling</span>,
                    activeIcon: (
                      <img
                        src={Selling_Grey_Icon}
                        className="sellingIcon"
                      ></img>
                    ),
                    inactiveIcon: (
                      <img
                        src={Selling_Grey_Icon}
                        className="sellingIcon"
                      ></img>
                    ),
                  },
                  {
                    label: <span>Buying</span>,
                    activeIcon: (
                      <img src={Cart_Grey_Icon} className="buyingIcon"></img>
                    ),
                    inactiveIcon: (
                      <img src={Cart_Grey_Icon} className="buyingIcon"></img>
                    ),
                  },
                ]}
                tabcontent={[
                  { content: All },
                  { content: Selling },
                  { content: Buying },
                ]}
              />
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .viewArchive_btn {
              position: absolute;
              top: 0;
              right: 1.464vw;
            }
            .viewIcon {
              width: 0.878vw;
              margin-right: 0.585vw;
            }
            :global(.viewArchive_btn button) {
              min-width: fit-content !important;
              width: 100%;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.viewArchive_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.viewArchive_btn button:focus),
            :global(.viewArchive_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.viewArchive_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            :global(.back_btn button) {
              min-width: fit-content !important;
              width: fit-content;
              padding: 0.732vw 0 1.464vw 0;
              background: transparent;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.back_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_2};
              line-height: 1;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            .backarrowIcon {
              width: 0.732vw;
              margin-right: 0.292vw;
            }
            .heading {
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 1.464vw;
            }
            .sellingIcon {
              width: 0.732vw;
              margin-bottom: 0 !important;
              margin-right: 0.292vw;
            }
            .buyingIcon {
              width: 0.658vw;
              margin-bottom: 0 !important;
              margin-right: 0.292vw;
            }
            .ArchivePage {
              background: ${WHITE_COLOR};
              height: 36.603vw;
              box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.1);
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ArchivePage;
