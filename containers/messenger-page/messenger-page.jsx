import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  Selling_Grey_Icon,
  Cart_Grey_Icon,
  WHITE_COLOR,
  Notification_Green_Icon,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import AllChatList from "./all-chat-list";
import ButtonComp from "../../components/button/button";
import ArchivePage from "./archive-page";
import RightDrawer from "./right-drawer";
import EmptyMessenger from "./empty-messenger";
import { storeAllChatDataInit } from "../../redux/actions/chat/chat";
import { connect } from "react-redux";
import BuyingChats from "./buying-chats";
import SellerChats from "./seller-chats";

class MessengerPage extends Component {
  state = {
    openPage: false,
    currentScreen: "",
    selectedMsgData: {},
    noMessages: false,
    allChats: [],
    currentPage: 0,
  };

  componentDidMount() {
    this.setState({
      openPage: true,
    });
    if (this.props.allChats.length === 0) {
      this.props.dispatch(storeAllChatDataInit(this.state.currentPage));
    }
  }

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleViewArchive = () => {
    this.updateScreen(<ArchivePage handleBackScreen={this.handleBackScreen} />);
  };
  handleBackScreen = () => {
    this.updateScreen();
  };
  handleSelectedMsg = (selectedMsg) => {
    // console.log("selectedMsg", selectedMsg);
    this.setState({
      selectedMsgData: selectedMsg,
    });
  };

  render() {
    // console.log("allChats", this.props.allChats);
    const All = this.state.noMessages ? "" : <AllChatList handleSelectedMsg={this.handleSelectedMsg} />;
    const Selling = this.state.noMessages ? "" : <BuyingChats handleSelectedMsg={this.handleSelectedMsg} />;
    const Buying = this.state.noMessages ? "" : <SellerChats handleSelectedMsg={this.handleSelectedMsg} />;

    return (
      <Wrapper>
        {this.state.openPage ? (
          <div className="MyOfferPage py-3">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0 py-3">
                {!this.state.currentScreen ? (
                  <div className="row m-0 MyOfferSec">
                    <div className="col-4 ChatList_Sec p-0">
                      <div className="d-flex align-items-center justify-content-between px-3 pt-3 pb-2">
                        <h6 className="heading">Messenger</h6>
                        <div className="viewArchive_btn">
                          <ButtonComp onClick={this.handleViewArchive}>View Archive</ButtonComp>
                        </div>
                      </div>

                      <Tabs
                        chattabs={true}
                        messengerTabs={true}
                        tabs={[
                          {
                            label: <span>All</span>,
                          },
                          {
                            label: <span>Selling</span>,
                            activeIcon: <img src={Selling_Grey_Icon} className="sellingIcon"></img>,
                            inactiveIcon: <img src={Selling_Grey_Icon} className="sellingIcon"></img>,
                          },
                          {
                            label: <span>Buying</span>,
                            activeIcon: <img src={Cart_Grey_Icon} className="buyingIcon"></img>,
                            inactiveIcon: <img src={Cart_Grey_Icon} className="buyingIcon"></img>,
                          },
                        ]}
                        tabcontent={[{ content: All }, { content: Selling }, { content: Buying }]}
                      />

                      <div className="col-12 notificationSec">
                        <div className="d-flex align-items-center justify-content-between">
                          <div className="d-flex">
                            <img src={Notification_Green_Icon} className="notifyIcon"></img>
                            <div>
                              <p className="msgTitle">Don't miss a reply!</p>
                              <p className="msgCaption">Allow notifications now</p>
                            </div>
                          </div>
                          <div className="allow_btn">
                            <ButtonComp>Allow</ButtonComp>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-8 px-0 position-relative">
                      {this.props.userSelectedToChat.recipientId || this.props.userSelectedToChat.to ? (
                        <RightDrawer selectedMsgData={this.state.selectedMsgData} />
                      ) : (
                        <EmptyMessenger />
                      )}
                    </div>
                  </div>
                ) : (
                  this.state.currentScreen
                )}
              </div>
            </div>
          </div>
        ) : (
          ""
        )}

        <style jsx>
          {`
            :global(.chat_tabs) {
            }
            :global(.viewArchive_btn button) {
              min-width: fit-content !important;
              width: fit-content;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border: none;
            }
            :global(.viewArchive_btn button span) {
              width: fit-content;
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.768vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.viewArchive_btn button:focus),
            :global(.viewArchive_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.viewArchive_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .notificationSec {
              padding: 0 0.732vw !important;
              position: absolute;
              bottom: 1.098vw;
              width: 100%;
            }
            .notifyIcon {
              width: 1.171vw;
              margin-right: 0.366vw;
            }
            .notificationSec > div {
              padding: 1.098vw;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.219vw;
            }
            .MyOfferSec {
              // height: 63vh;
              background: ${WHITE_COLOR};
            }
            .MyOfferPage {
              background: ${BG_LightGREY_COLOR};
              position: relative;
              box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
            }
            .ChatList_Sec {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR};
              min-height: 40.263vw;
              position: relative;
            }
            .heading {
              font-size: 0.951vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .sellingIcon {
              width: 0.732vw;
              margin-bottom: 0 !important;
              margin-right: 0.292vw;
            }
            .buyingIcon {
              width: 0.658vw;
              margin-bottom: 0 !important;
              margin-right: 0.292vw;
            }
            .msgTitle {
              font-size: 0.805vw;
              color: ${GREEN_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              margin: 0;
              line-height: 1;
            }
            .msgCaption {
              font-size: 0.658vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0;
            }
            :global(.allow_btn button) {
              min-width: fit-content !important;
              width: 3.66vw;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw !important;
              border: 0.0732vw solid ${GREEN_COLOR};
            }
            :global(.allow_btn button span) {
              width: fit-content;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREEN_COLOR};
              line-height: 1;
            }
            :global(.allow_btn button:focus),
            :global(.allow_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.allow_btn button:hover) {
              background: ${WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    boatData: state.boatData,
    allChats: state.allChats,
    userSelectedToChat: state.userSelectedToChat,
  };
};

export default connect(mapStateToProps, null)(MessengerPage);
