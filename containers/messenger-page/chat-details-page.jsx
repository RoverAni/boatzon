import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import ButtonComp from "../../components/button/button";
import {
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  WHITE_COLOR,
  THEME_COLOR,
  Sent_Icon_Grey,
  Sent_Icon_Green,
  My_Profile_Pic,
  FONTGREY_COLOR,
  LIGHT_THEME_COLOR,
  Orange_Color,
  Sent_Single_Tick,
  OFFER_ODD_COLOR,
} from "../../lib/config";
import TextAreaBox from "../../components/input-box/text-area-input";
import { connect } from "react-redux";
import { getCookie } from "../../lib/session";
import { messageSubject } from "../../lib/mqtt/subjects";
import GoogleMapReact from "google-map-react";
import {
  storeAllChatsOfSelectedUser,
  userSelectedToChat,
  onScrollInit,
  removeSelectedUserAllMessages,
} from "../../redux/actions/chat/chat";
import { storeWebrtcData } from "../../redux/actions/webrtc/webrtc";
import MakeOfferChatModel from "../product-details/make-offer-chat-model";
import { makeOffer } from "../../services/product-details";
import Model from "../../components/model/model";

const Marker = (props) => {
  const { color } = props;
  return (
    <div
      className="marker"
      style={{ backgroundColor: color, cursor: "pointer" }}
    />
  );
};

let extractLatAndLng = (str) => {
  let i = 0;
  let arr = "";
  while (str[i] !== "@") {
    arr += str[i++];
  }
  let _string = arr.substr(1, arr.length - 2);
  let s = _string.split(",");
  return { lat: s[0], lng: s[1] };
};

const helperText = [
  "Is it still available?",
  "What condition is it in?",
  "How can I see this boat?",
];

class ChatDetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputpayload: {},
      ChatData: {},
      oldestMessageTimestampIndex: 19,
      counterOffer: false,
    };
    this.chatRef = React.createRef();
  }

  toggleCounterOffer = () =>
    this.setState({ counterOffer: !this.state.counterOffer });

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        ChatData: this.props.selectedMsgData,
        textValue: "",
      });
    }, 300);
    this.scrollListener(this.chatRef.current);
    if (!!this.props.UserSelectedToChat.recipientId) {
      console.log("got user");
      setTimeout(() => {
        this.scrollToBot();
      }, 500);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props &&
      this.props.UserSelectedToChat &&
      this.props.UserSelectedToChat.recipientId !== prevProps &&
      prevProps.selectedMsgData &&
      prevProps.selectedMsgData.recipientId
    ) {
      this.scrollToBot();
    }
  }

  /** on selecting user, the by default the scroll would be on bottom to show latest messages */
  scrollToBot = () => {
    let el = document.getElementById("chatSection");
    el.scrollTop = el.scrollHeight;
  };

  scrollListener = (node) => {
    node.addEventListener("scroll", this.handleScroll, true);
  };

  removeScrollListener = (node) => {
    node.removeEventListener("scroll", this.handleScroll, true);
  };

  /** on scroll call API */
  handleScroll = (event) => {
    var node = event.target;
    if (node.scrollTop === 0 && node.scrollHeight > node.clientHeight) {
      if (
        this.props &&
        this.props.allMessagesForSelectedUser &&
        this.props.allMessagesForSelectedUser.length % 10 === 0
      ) {
        setTimeout(() => {
          this.props.onScrollCallApi(
            this.props.allMessagesForSelectedUser[
              this.state.oldestMessageTimestampIndex
            ].timestamp,
            this.props.UserSelectedToChat.chatId,
            10
          );
          this.setState({
            oldestMessageTimestampIndex:
              this.state.oldestMessageTimestampIndex + 10,
          });
        }, 300);
      }
    }
  };

  componentWillUnmount() {
    this.removeScrollListener(this.chatRef.current);
    window.removeEventListener("scroll", this.scrollListener);
    this.props.selectUserToChat("");
    /** this is to remove all stored messages for single user */
    this.props.removeAllStoredMessages();
  }

  /** this function decodes base64 to plain text */
  textdecode = (str) => {
    try {
      return decodeURIComponent(escape(atob(str)));
    } catch (e) {}
  };

  /** send message type "0" over mqtt */
  sendRegularMessage = () => {
    let {
      secretId,
      recipientId,
      chatId,
      postId,
    } = this.props.UserSelectedToChat;
    let { profilePicUrl, fullName } = this.props.userProfileData;
    let obj = {
      from: getCookie("mqttId"),
      id: new Date().getTime().toString(),
      name: fullName,
      payload: btoa(this.state.textValue),
      receiverIdentifier: "",
      secretId: secretId === undefined ? postId.toString() : secretId,
      timestamp: new Date().getTime(),
      to: recipientId,
      toDocId: "",
      type: "0",
      userImage: profilePicUrl,
      chatId: chatId === undefined ? "" : chatId,
    };
    console.log("sending normal text", obj);
    messageSubject.next(obj);
    this.setState({ textValue: "" });
  };

  /** function to copy selected helper text into text box */
  copyDataToTextBox = (i) => {
    let d = document.getElementById(`ht_${i}`);
    this.setState({ textValue: d.innerText });
  };

  wipeWebrtcData = () => {
    this.props.removeWebrtcData({});
  };

  /** renders single/double tick based on status */
  renderAckBasedOnStatus = (status) => {
    let icon;
    if (status === 1 || status === "1") {
      icon = (
        <img src={Sent_Single_Tick} height={13} className="sentIcon"></img>
      );
    } else if (status === 2 || status === "2") {
      icon = <img src={Sent_Icon_Green} height={13} className="sentIcon"></img>;
    } else if (status === 3 || status === "3") {
      icon = <img src={Sent_Icon_Grey} height={13} className="sentIcon"></img>;
    }
    return icon;
  };

  makeOffer = (newOfferType) => {
    let { username, profilePicUrl } = this.props.userProfileData;
    let existingUserSelectedToChat = { ...this.props.UserSelectedToChat };
    let {
      productId,
      name,
      recipientId,
      senderId,
      payload,
      productImage,
      productName,
      initiated,
      userName,
      to,
    } = this.props.UserSelectedToChat;
    let newPrice = atob(payload);
    let obj = {
      offerStatus: newOfferType,
      postId: productId,
      price: newPrice.toString(),
      type: "0",
      membername: initiated ? name || userName : username,
      buyer: initiated ? username : name || userName,
      sendchat: {
        name: initiated ? username : name || userName,
        from: getCookie("mqttId"),
        to: recipientId || senderId || to,
        payload: payload,
        type: "15",
        offerType: "2",
        id: new Date().getTime().toString(),
        isSold: "1",
        secretId: productId,
        thumbnail: "",
        userImage: profilePicUrl,
        toDocId: "xxx",
        dataSize: 1,
        productImage: productImage,
        productId: productId,
        productName: productName,
        productPrice: `${atob(payload)}`,
      },
    };
    makeOffer(obj)
      .then((res) => {
        existingUserSelectedToChat["offerType"] = "2";
        existingUserSelectedToChat["isSold"] = "1";
        this.props.selectUserToChat(existingUserSelectedToChat);
        console.log("offer accepted", res);
      })
      .catch((err) => console.log("err", err));
  };

  functionToRenderText = (obj) => {
    let toRender = "";
    if (obj.type === "1" || obj.messageType === "1") {
      toRender = (
        <>
          <img
            src={`${
              obj.thubmnail
                ? `data:image/png;base64,${obj.thubmnail}`
                : atob(obj.payload)
            }`}
            alt={obj.name || obj.userName}
            width={280}
            className="type_image"
          />
          {this.renderAckBasedOnStatus(obj.status)}
        </>
      );
    } else if (obj.type === "3" || obj.messageType === "3") {
      let encodeMsg = extractLatAndLng(atob(obj.payload));
      toRender = (
        <div className="type_map">
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyAtrnJwdRbJXfbsH4fr28N1TJG64c7Lrc4",
            }}
            zoom={6}
            center={{
              lat: parseInt(encodeMsg.lat),
              lng: parseInt(encodeMsg.lng),
            }}
            yesIWantToUseGoogleMapApiInternals
          >
            <Marker
              lat={parseInt(encodeMsg.lat)}
              lng={parseInt(encodeMsg.lng)}
              color={"#f53170"}
            />
          </GoogleMapReact>
          {this.renderAckBasedOnStatus(obj.status)}
        </div>
      );
    } else if (obj.type === "15" || obj.messageType === "15") {
      if (
        obj.senderId === getCookie("mqttId") &&
        obj.isSold === "0" &&
        obj.offerType !== "2"
      ) {
        toRender = (
          <div className="type_offer_text">
            <div>Offer Sent:</div>
            <div>US ${atob(obj.payload)}</div>
          </div>
        );
      } else if (
        obj.isSold === "0" &&
        obj.offerType === "3" &&
        this.props.UserSelectedToChat.isSold === "1"
      ) {
        toRender = (
          <div className="type_offer_text">
            <div>Offer Price:</div>
            <div>US ${atob(obj.payload)}</div>
          </div>
        );
      } else if (
        obj.isSold === "1" &&
        obj.offerType === "2" &&
        this.props.UserSelectedToChat.isSold === "1"
      ) {
        toRender = (
          <div className="type_offer_text">
            <div>Offer Accepted:</div>
            <div>US ${atob(obj.payload)}</div>
          </div>
        );
      } else if (obj.isSold === "0" && obj.offerType !== "2") {
        /**
         * makeOffer is accepting offer with offerType: 2
         * toggleCounterOffer is accepting offer with offerType: 3
         */
        toRender = (
          <div className="type_offer">
            <div>Offer Received:</div>
            <div>US ${atob(obj.payload)}</div>
            <div>
              {/* <button type="button" class="btn btn-danger" onClick={() => this.makeOffer(0)}>
                Reject
              </button> */}
              {this.props.UserSelectedToChat.isSold !== "1" ? (
                <>
                  <button
                    type="button"
                    className="offer_btn p-0"
                    onClick={() => this.makeOffer("2")}
                  >
                    Accept
                  </button>
                  <button
                    type="button"
                    className="offer_btn1 p-0"
                    onClick={this.toggleCounterOffer}
                  >
                    Counter Offer
                  </button>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        );
      }
    }
    return toRender;
  };

  render() {
    console.log("webrtc", this.props.webrtc);
    return (
      <Wrapper>
        <div className="col-12 p-0 position-relative Chats_SecPage">
          <div className="Chats_Sec">
            <div
              className="position-relative"
              ref={this.chatRef}
              id="chatSection"
            >
              {this.props &&
                this.props.allMessagesForSelectedUser &&
                this.props.allMessagesForSelectedUser
                  .map((k, i) =>
                    getCookie("mqttId") === k.receiverId ||
                    getCookie("mqttId") === k.to ? (
                      <div className="row m-0 justify-content-start" key={i}>
                        <div
                          className={
                            k.type === "15" || k.messageType === "15"
                              ? "type_offer_odd"
                              : "oddComment_Sec"
                          }
                        >
                          {k.type === "0" || k.messageType === "0" ? (
                            <>
                              <p>{this.textdecode(k.payload)}</p>
                              {this.renderAckBasedOnStatus(k.status)}
                            </>
                          ) : (
                            this.functionToRenderText(k)
                          )}
                        </div>
                      </div>
                    ) : (
                      <div className="row m-0 justify-content-end" key={i}>
                        <div
                          className={
                            k.type === "15" || k.messageType === "15"
                              ? "type_offer_even"
                              : "evenComment_Sec"
                          }
                        >
                          {k.type === "0" || k.messageType === "0" ? (
                            <>
                              <p>{this.textdecode(k.payload)}</p>
                              {this.renderAckBasedOnStatus(k.status)}
                            </>
                          ) : (
                            this.functionToRenderText(k)
                          )}
                        </div>
                      </div>
                    )
                  )
                  .reverse()}
            </div>
            <ul className="list-unstyled d-flex align-items-center flex-wrap predictionMsgList">
              {helperText.map((k, i) => (
                <li
                  key={i}
                  id={`ht_${i}`}
                  onClick={() => this.copyDataToTextBox(i)}
                >
                  {k}
                </li>
              ))}
            </ul>
            <div className="col-12 pl-0 pr-2">
              <div className="row m-0 commentInput_Sec">
                <div className="col-1 p-0">
                  <img src={My_Profile_Pic} className="myProfilePic"></img>
                </div>
                <div className="col-10 p-0">
                  <div className="FormInput">
                    <TextAreaBox
                      type="textarea"
                      className="textareaBox form-control"
                      placeholder="Write a reply"
                      name="even_comment"
                      value={this.state.textValue}
                      onChange={(e) =>
                        this.setState({ textValue: e.target.value })
                      }
                      autoComplete="off"
                    ></TextAreaBox>
                  </div>
                </div>
                <div className="col-1 p-0">
                  <div className="send_btn">
                    <ButtonComp onClick={this.sendRegularMessage}>
                      Send
                    </ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Model
            open={this.state.counterOffer}
            onClose={this.toggleCounterOffer}
          >
            <MakeOfferChatModel
              chatDetails={this.props.UserSelectedToChat}
              onClose={this.toggleCounterOffer}
            />
          </Model>
          {/* <MatUIModal isOpen={this.props.webrtc && this.props.webrtc.callId} toggle={this.wipeWebrtcData} width={"80%"}>
            <VideoComponent
              UserSelectedToChat={this.props.UserSelectedToChat}
              wipeWebrtcData={this.wipeWebrtcData}
              callId={this.props.webrtc && this.props.webrtc.callId}
              roomName={this.props.webrtc && this.props.webrtc.room}
              token={this.props.webrtc && this.props.webrtc.twilioToken}
              isIncomingCall={Object.keys(this.props.webrtc).length}
              webrtc={this.props.webrtc}
            />
          </MatUIModal> */}
        </div>

        <style jsx>
          {`
            .send_btn {
              float: right;
            }
            :global(.offer_btn) {
              width: 100%;
              background: white !important;
              color: #484848;
              font-weight: 600;
              border: none !important;
              font-family: "Open Sans" !important;
              font-size: 18px;
              padding: 2px !important;
            }
            :global(.offer_btn, .offer_btn1) {
              cursor: pointer;
            }
            :global(.offer_btn1) {
              width: 100%;
              background: white !important;
              color: #484848;
              font-weight: 600;
              border: none !important;
              font-family: "Open Sans" !important;
              font-size: 18px;
              padding: 2px !important;
              border-top: 2px solid ${OFFER_ODD_COLOR} !important;
            }
            .Chats_SecPage {
              height: 34.04vw;
              // border: 0.0732vw solid;
            }
            :global(.send_btn button) {
              min-width: fit-content !important;
              width: fit-content;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              margin: 0 0 0 auto;
              text-transform: capitalize;
              position: relative;
              border: none !important;
            }
            :global(.send_btn button span) {
              width: fit-content;
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              line-height: 1;
            }
            :global(.send_btn button:focus),
            :global(.send_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.send_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .commentInput_Sec {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR};
              padding: 0.732vw 0 0 0;
            }
            .myProfilePic {
              width: 70%;
              object-fit: cover;
            }
            :global(.commentInput_Sec .FormInput .textareaBox) {
              display: block;
              width: 100%;
              height: 2.928vw;
              padding: 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.commentInput_Sec .FormInput .textareaBox:focus) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.textareaBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .predictionMsgList {
              margin: 0.366vw 0 0.732vw 0;
            }
            .predictionMsgList li {
              background: ${Orange_Color};
              color: ${WHITE_COLOR};
              padding: 0.366vw 0.732vw;
              margin-right: 0.585vw;
              margin-top: 0.366vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.732vw;
            }
            .Chats_Sec {
              position: absolute;
              bottom: 0;
              width: 100%;
            }
            .Chats_Sec > div {
              max-height: 24.89vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .sentIcon {
              position: absolute;
              bottom: 0.585vw;
              right: 0.585vw;
              width: 0.732vw;
            }
            .evenComment_Sec {
              background: ${LIGHT_THEME_COLOR};
              padding: 0.732vw;
              width: fit-content;
              max-width: 75%;
              margin: 0.366vw 0;
              border-radius: 0.219vw;
              position: relative;
              padding-bottom: 1.464vw;
            }
            .evenComment_Sec p {
              font-size: 0.732vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              max-width: 98%;
            }
            .evenComment_Sec type_image {
              max-height: 20.497vw;
              height: auto;
              object-fit: contain;
            }
            .oddComment_Sec type_image {
              max-height: 280px;
              height: auto;
              object-fit: contain;
            }
            :global(.oddComment_Sec div.type_map) {
              height: 200px !important;
              width: 280px !important;
            }
            :global(.evenComment_Sec div.type_map) {
              height: 200px !important;
              width: 280px !important;
            }
            .oddComment_Sec {
              background: ${Border_LightGREY_COLOR};
              padding: 0.732vw;
              margin: 0.366vw 0;
              border-radius: 0.219vw;
              width: fit-content;
              max-width: 70%;
              position: relative;
              padding-bottom: 1.464vw;
            }
            :global(.type_offer_odd) {
              background: ${OFFER_ODD_COLOR} !important;;
              border-radius: 0.219vw; !important;
              width: fit-content;
              max-width: 70%;
              padding: 0px !important;
              position: relative;
              margin: 0.366vw 0;
              border: 2px solid ${OFFER_ODD_COLOR};
            }
            :global(.type_offer_even) {
              background: ${LIGHT_THEME_COLOR} !important;;
              border-radius: 0.219vw; !important;
              width: fit-content;
              max-width: 70%;
              padding: 0.532vw !important;
              position: relative;
              margin: 0.366vw 0;
              border: 2px solid ${LIGHT_THEME_COLOR};
            }
            :global(.type_offer_text > div:nth-child(1)) {
              color: #484848 !important;
              font-size: 0.732vw;
              text-align:center;
              font-weight: 100;
              padding: 2px;
              font-family: "Open Sans" !important;
            }
            :global(.type_offer_text > div:nth-child(2)) {
              color: #484848 !important;
              font-size: 1vw;
              text-align:center;
              font-weight: 500;
              padding: 2px;
              font-family: "Open Sans" !important;
            }
            :global(.type_offer > div:nth-child(1)) {
              color: #FFF !important;
              font-size: 13px;
              text-align:center;
              font-weight: 600;
              padding: 2px;
              font-family: "Open Sans" !important;
            }
            :global(.type_offer > div:nth-child(2)) {
              color: #FFF !important;
              font-size: 18px;
              text-align:center;
              font-weight: 600;
              padding: 2px;
              font-family: "Open Sans" !important;
            }
            .oddComment_Sec p {
              font-size: 0.732vw;
              color: ${FONTGREY_COLOR};
              max-width: 98%;
              letter-spacing: 0.0219vw !important;
              font-family: "Open Sans" !important;
              margin-bottom: 0;
            }
            .archive_btn {
              padding-left: 10px;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allMessagesForSelectedUser: state.allMessagesForSelectedUser,
    UserSelectedToChat: state.userSelectedToChat,
    userProfileData: state.userProfileData,
    webrtc: state.webrtc,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeAllStoredMessages: () => dispatch(removeSelectedUserAllMessages()),
    storeAllChatsOfSelectedUser: (obj) =>
      dispatch(storeAllChatsOfSelectedUser(obj)),
    selectUserToChat: (userObj) => dispatch(userSelectedToChat(userObj)),
    onScrollCallApi: (ts, cid, index) => dispatch(onScrollInit(ts, cid, index)),
    removeWebrtcData: (obj) => dispatch(storeWebrtcData(obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatDetailsPage);
