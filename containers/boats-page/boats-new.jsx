import React, { Component } from "react";
import CustomBreadCrumbs from "../../components/breadcrumbs/breadcrumbs";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
// reusable component
import ButtonComp from "../../components/button/button";
import MonthlyDealsBanner from "./monthlyDealsBanner";
import PopularBanner from "./popularBanner";
import BoatsFilter from "./boats-filter";
import { getCurrentLocation } from "../../redux/actions/location/location";
import { connect } from "react-redux";
import BoatList from "../landing-page/boat-list";
import PageBeatLoader from "../../components/loader/beat-loader";
import debounce from "lodash.debounce";
import { isEmpty } from "../../lib/global";
import PageLoader from "../../components/loader/page-loader";

class BoatsNew extends Component {
  state = {
    bannerOpen: true,
    tabName: "monthlyDeals",
    condition: "",
    lengthDropdown: false,
    lengthValue: [5, 100],
    minLength: 5,
    maxLength: 100,
    fromLength: 5,
    toLength: 100,
    LengthRange: [5, 100],
    radiusDropdown: false,
    inputpayload: {},
    engineHoursDropdown: false,
    engineHoursValue: [0, 4000],
    minEngineHours: 0,
    maxEngineHours: 4000,
    fromEngineHours: 0,
    toEngineHours: 4000,
    EngineHoursRange: [0, 4000],
    filterToggle: false,
    payload: {
      category: "Boats",
    },
    yearDropdown: false,
    yearsValue: [1970, 2017],
    minYears: 1970,
    maxYears: 2017,
    fromYear: 1970,
    toYear: 2017,
    yearsRange: [1970, 2017],
    selectedBoatsType: [],
    breadcrumbsData: ["All Boats"],
  };

  componentDidMount() {
    if (!isEmpty(this.props.urlString)) {
      const {
        condition,
        manufactor,
        modelName,
        subCategory,
        fromLength,
        toLength,
        hasWarranty,
        postedWithin,
        color,
        distanceMax,
        fromLoan,
        toLoan,
        fromPrice,
        toPrice,
        fromYear,
        toYear,
        fromEngineHours,
        toEngineHours,
        engineType,
        fuelType,
        saleBy,
      } = { ...this.props.urlString };

      // price payload
      let tempPayload = { ...this.state.inputpayload };
      tempPayload.minPrice = fromPrice;
      tempPayload.maxPrice = toPrice;

      // engine payload
      let tempEngineValue = [];
      fromEngineHours
        ? tempEngineValue.push(parseInt(fromEngineHours, 10))
        : this.state.minEngineHours;
      toEngineHours
        ? tempEngineValue.push(parseInt(toEngineHours, 10))
        : this.state.maxEngineHours;

      // year payload
      let tempYearValue = [];
      fromYear
        ? tempYearValue.push(parseInt(fromYear, 10))
        : this.state.minYears;
      toYear ? tempYearValue.push(parseInt(toYear, 10)) : this.state.maxYears;

      // length payload
      let templengthValue = [];
      fromLength
        ? templengthValue.push(parseInt(fromLength, 10))
        : this.state.minLength;
      toLength
        ? templengthValue.push(parseInt(toLength, 10))
        : this.state.maxLength;

      // Manufacturer payload
      let manufactureIdList = manufactor && manufactor.split(",");
      let tempPay =
        manufactureIdList?.length > 0
          ? manufactureIdList.map((data) => {
              return { label: data };
            })
          : "";

      // model payload
      let modelNameList = modelName?.split(",");

      let tempModelPayload =
        modelNameList?.length > 0
          ? modelNameList.map((data) => {
              return { label: data };
            })
          : "";

      // boats subCategory payload
      let subCategoryList = subCategory ? subCategory.split(",") : [];

      // postedWithin payload
      let tempPostedWithin =
        postedWithin == "24hr"
          ? { value: postedWithin, label: "last 24 hr" }
          : postedWithin == "15days"
          ? { value: postedWithin, label: "last 15 days" }
          : postedWithin == "30days"
          ? { value: postedWithin, label: "last 30 days" }
          : "";

      const FinanceTypeOptions = [
        { value: "0000000000", label: "All" },
        { value: 149, label: "Boats under $149/mo" },
        { value: 299, label: "Boats under $299/mo" },
        { value: 499, label: "Boats under $499/mo" },
        { value: 699, label: "Boats under $699/mo" },
        { value: 999, label: "Boats under $999/mo" },
        { value: 1500, label: "Boats under $1500/mo" },
        { value: 2000, label: "Boats under $2000/mo" },
        { value: 2500, label: "Boats under $2500/mo" },
      ];

      let tempBudget =
        FinanceTypeOptions?.length > 0 && toLoan
          ? FinanceTypeOptions.filter((data) => data.value == toLoan)
          : "";

      const ColorList = [
        {
          value: "#FFFFFD",
          label: "White",
        },
        {
          value: "#A4F8E1",
          label: "Turquoise",
        },
        {
          value: "#FFFFB5",
          label: "Soft Yellow",
        },
        {
          value: "#777D79",
          label: "Grey",
        },
        {
          value: "#0E1003",
          label: "Black",
        },
        {
          value: "#152F74",
          label: "Blue",
        },
        {
          value: "#1B253C",
          label: "Dark Blue",
        },
        {
          value: "#005E60",
          label: "Teal",
        },
        {
          value: "#024F19",
          label: "Green",
        },
        {
          value: "#EAC500",
          label: "Yellow",
        },
        {
          value: "#EA6500",
          label: "Orange",
        },
        {
          value:
            "linear-gradient(180deg, #80FF00 0%, #1FA3C0 50.52%, #FF003D 100%)",
          label: "Other",
        },
      ];

      let tempColor =
        ColorList?.length > 0 && color
          ? ColorList.filter((data) => data.label == color)
          : "";

      const SaleType = [
        { value: "Any Seller", label: "Any Seller" },
        { value: "0", label: "Private Seller" },
        { value: "1", label: "Dealer/Broker" },
      ];

      let tempSale =
        SaleType?.length > 0 && saleBy
          ? SaleType.filter((data) => data.value == saleBy)
          : "";

      let tempbreadcrumbsData = [...this.state.breadcrumbsData];

      let value =
        manufactureIdList?.length > 0
          ? manufactureIdList[manufactureIdList.length - 1]
          : null;

      tempbreadcrumbsData[1] = value;
      this.setState(
        {
          inputpayload: { ...tempPayload },
          condition: condition,
          manufactor: tempPay,
          boat_model: tempModelPayload,
          selectedBoatsType: subCategoryList,
          lengthValue: templengthValue,
          minLength: fromLength ? fromLength : this.state.minLength,
          maxLength: toLength ? toLength : this.state.maxLength,
          fromLength: fromLength
            ? parseInt(fromLength, 10)
            : this.state.minLength,
          toLength: toLength ? parseInt(toLength, 10) : this.state.maxLength,
          LengthRange: templengthValue,
          valueRadius: distanceMax ? distanceMax : 0,
          Budget: tempBudget,
          minPrice: fromPrice ? fromPrice : "",
          maxPrice: toPrice ? toPrice : "",
          yearsValue: tempYearValue,
          minYears: fromYear ? fromYear : this.state.minYears,
          maxYears: toYear ? toYear : this.state.maxYears,
          fromYear: fromYear ? parseInt(fromYear, 10) : this.state.minYears,
          toYear: toYear ? parseInt(toYear, 10) : this.state.maxYears,
          yearsRange: tempYearValue,
          hasWarranty: hasWarranty,
          engineHoursValue: tempEngineValue,
          minEngineHours: fromEngineHours
            ? fromEngineHours
            : this.state.minEngineHours,
          maxEngineHours: toEngineHours
            ? toEngineHours
            : this.state.maxEngineHours,
          fromEngineHours: fromEngineHours
            ? parseInt(fromEngineHours, 10)
            : this.state.minEngineHours,
          toEngineHours: toEngineHours
            ? parseInt(toEngineHours, 10)
            : this.state.maxEngineHours,
          EngineHoursRange: tempEngineValue,
          engineType: { value: engineType, label: engineType },
          fuelType: { value: fuelType, label: fuelType },
          color: tempColor,
          saleBy: tempSale,
          breadcrumbsData: [...tempbreadcrumbsData],
          postedWithin: { ...tempPostedWithin },
        },
        () => {
          let apiPayload = { ...this.state.payload, ...this.props.urlString };
          if (fromLength && toLength) {
            apiPayload.length = {
              fromLength,
              toLength,
            };
            delete apiPayload.fromLength;
            delete apiPayload.toLength;
          }
          if (fromEngineHours && toEngineHours) {
            apiPayload.engineHours = {
              fromEngineHours,
              toEngineHours,
            };
            delete apiPayload.fromEngineHours;
            delete apiPayload.toEngineHours;
          }
          if (fromLoan && toLoan) {
            apiPayload.loan = {
              fromLoan,
              toLoan,
            };
            delete apiPayload.fromLoan;
            delete apiPayload.toLoan;
          }
          if (fromYear && toYear) {
            apiPayload.year = {
              fromYear,
              toYear,
            };
            delete apiPayload.fromYear;
            delete apiPayload.toYear;
          }
          if (fromPrice && toPrice) {
            apiPayload.priceRange = {
              fromPrice,
              toPrice,
            };
            delete apiPayload.fromPrice;
            delete apiPayload.toPrice;
          }
          if (subCategory) {
            apiPayload.subCategory = subCategory.replace(/,/g, " OR ");
          }
          if (hasWarranty) {
            apiPayload.hasWarranty =
              hasWarranty == "All Options"
                ? ""
                : hasWarranty == "With a Warranty"
                ? "Yes"
                : hasWarranty == "No Warranty"
                ? "No"
                : null;
          }
          this.setState(
            {
              payload: apiPayload,
            },
            () => {
              this.props.handlePostProductsAPI(this.state.payload);
            }
          );
        }
      );
    }
  }

  handleToggleBanner = () => {
    this.setState({
      bannerOpen: !this.state.bannerOpen,
    });
  };

  handleChangeTab = (tabName) => {
    if (this.state.bannerOpen) {
      this.setState({
        tabName,
      });
    } else {
      this.setState({
        bannerOpen: true,
        tabName,
      });
    }
  };

  handleTypeOnSelect = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        if (name == "selectedBoatsType") {
          apiPayload[`subCategory`] = this.state[name]
            .toString()
            .replace(/,/g, " OR ");
          this.props.handleUrlParams(
            "subCategory",
            this.state[name].toString()
          ); // function to add filterdata in url params
        } else if (name == "hasWarranty") {
          apiPayload[`${name}`] =
            this.state.hasWarranty == "All Options"
              ? ""
              : this.state.hasWarranty == "With a Warranty"
              ? "Yes"
              : this.state.hasWarranty == "No Warranty"
              ? "No"
              : null;
          this.props.handleUrlParams(`${name}`, this.state[name]); // function to add filterdata in url params
        } else {
          apiPayload[`${name}`] = this.state[name];
          this.props.handleUrlParams(`${name}`, this.state[name]); // function to add filterdata in url params
        }
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  handleChangeCondition = (conditionName) => {
    this.setState(
      {
        condition: conditionName,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`condition`] =
          conditionName == "All" ? "" : this.state.condition;
        this.props.handleUrlParams(`condition`, this.state.condition); // function to add filterdata in url params

        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  /** function to toggle length dropdown */
  lengthDropdownOpen = () => {
    if (
      this.state.lengthDropdown == true &&
      (this.state.fromLength || this.state.toLength)
    ) {
      this.setState({
        minLength: this.state.fromLength,
        maxLength: this.state.toLength,
        lengthValue: this.state.LengthRange,
      });
    } else if (
      this.state.lengthDropdown == true &&
      (!this.state.fromLength || !this.state.toLength)
    ) {
      this.setState({
        minLength: 5,
        maxLength: 200,
        lengthValue: [5, 200],
      });
    }
    this.setState({
      lengthDropdown: !this.state.lengthDropdown,
    });
  };

  /** function to set min and max length */
  handleLengthRange = () => {
    this.setState(
      {
        lengthDropdown: !this.state.lengthDropdown,
        fromLength: this.state.minLength
          ? parseInt(this.state.minLength, 10)
          : 0,
        toLength: this.state.maxLength ? parseInt(this.state.maxLength, 10) : 0,
      },
      () => {
        this.setState(
          {
            LengthRange: [this.state.fromLength, this.state.toLength],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`length`] = {};
            apiPayload[`length`].fromLength = this.state.fromLength;
            apiPayload[`length`].toLength = this.state.toLength;
            this.props.handleUrlParams("length", apiPayload.length); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set length value on slide */
  handleSliderLegthRange = (event, newValue) => {
    this.setState(
      {
        lengthValue: newValue,
      },
      () => {
        this.handleMinMaxValues(this.state.lengthValue);
      }
    );
  };

  /** function to set min and max length value from slider value */
  handleMinMaxValues = (value) => {
    let minLength = value[0];
    let maxLength = value[1];
    this.setState({
      minLength,
      maxLength,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeLengthInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minLength") {
      this.setState(
        {
          maxLength: this.state.maxLength,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minLength, this.state.maxLength];
          this.setState({
            lengthValue: newValue,
          });
        }
      );
    } else if (name == "maxLength") {
      this.setState(
        {
          minLength: this.state.minLength,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minLength, this.state.maxLength];
          this.setState({
            lengthValue: newValue,
          });
        }
      );
    }
  };

  /** function to toggle engineHours dropdown */
  engineHoursDropdownOpen = () => {
    if (
      this.state.engineHoursDropdown == true &&
      (this.state.fromEngineHours || this.state.toEngineHours)
    ) {
      this.setState({
        minEngineHours: this.state.fromEngineHours,
        maxEngineHours: this.state.toEngineHours,
        engineHoursValue: this.state.EngineHoursRange,
      });
    } else if (
      this.state.engineHoursDropdown == true &&
      (!this.state.fromEngineHours || !this.state.toEngineHours)
    ) {
      this.setState({
        minEngineHours: 0,
        maxEngineHours: 4000,
        engineHoursValue: [0, 4000],
      });
    }
    this.setState({
      engineHoursDropdown: !this.state.engineHoursDropdown,
    });
  };

  /** function to set min and max engineHours */
  handleengineHoursRange = () => {
    this.setState(
      {
        engineHoursDropdown: !this.state.engineHoursDropdown,
        fromEngineHours: this.state.minEngineHours
          ? parseInt(this.state.minEngineHours, 10)
          : 0,
        toEngineHours: this.state.maxEngineHours
          ? parseInt(this.state.maxEngineHours, 10)
          : 0,
      },
      () => {
        this.setState(
          {
            EngineHoursRange: [
              this.state.fromEngineHours,
              this.state.toEngineHours,
            ],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`engineHours`] = {};
            apiPayload[
              `engineHours`
            ].fromEngineHours = this.state.fromEngineHours;
            apiPayload[`engineHours`].toEngineHours = this.state.toEngineHours;
            console.log("ndsjcfjdk", apiPayload);

            this.props.handleUrlParams("engineHours", apiPayload.engineHours); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set engineHours value on slide */
  handleSliderEngineHoursRange = (event, newValue) => {
    this.setState(
      {
        engineHoursValue: newValue,
      },
      () => {
        this.handleEngineMinMaxValues(this.state.engineHoursValue);
      }
    );
  };

  /** function to set min and max EngineHours value from slider value */
  handleEngineMinMaxValues = (value) => {
    let minEngineHours = value[0];
    let maxEngineHours = value[1];
    this.setState({
      minEngineHours,
      maxEngineHours,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeEngineHoursInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minEngineHours") {
      this.setState(
        {
          maxEngineHours: this.state.maxEngineHours,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minEngineHours, this.state.maxEngineHours];
          this.setState({
            engineHoursValue: newValue,
          });
        }
      );
    } else if (name == "maxEngineHours") {
      this.setState(
        {
          minEngineHours: this.state.minEngineHours,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minEngineHours, this.state.maxEngineHours];
          this.setState({
            engineHoursValue: newValue,
          });
        }
      );
    }
  };

  // ------?
  /** function to toggle Year dropdown */
  yearDropdownOpen = () => {
    if (
      this.state.yearDropdown == true &&
      (this.state.fromYear || this.state.toYear)
    ) {
      this.setState({
        minYears: this.state.fromYear,
        maxYears: this.state.toYear,
        yearsValue: this.state.yearsRange,
      });
    } else if (
      this.state.yearDropdown == true &&
      (!this.state.fromYear || !this.state.toYear)
    ) {
      this.setState({
        minYears: 1970,
        maxYears: 2017,
        yearsValue: [1970, 2017],
      });
    }
    this.setState({
      yearDropdown: !this.state.yearDropdown,
    });
  };

  /** function to set min and max Year */
  handleYearsRange = () => {
    this.setState(
      {
        yearDropdown: !this.state.yearDropdown,
        fromYear: this.state.minYears ? parseInt(this.state.minYears, 10) : 0,
        toYear: this.state.maxYears ? parseInt(this.state.maxYears, 10) : 0,
      },
      () => {
        this.setState(
          {
            yearsRange: [this.state.fromYear, this.state.toYear],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`year`] = {};
            apiPayload[`year`].fromYear = this.state.fromYear;
            apiPayload[`year`].toYear = this.state.toYear;
            this.props.handleUrlParams("year", apiPayload.year); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set Year value on slide */
  handleSliderYearsRange = (event, newValue) => {
    this.setState(
      {
        yearsValue: newValue,
      },
      () => {
        this.handleYearMinMaxValues(this.state.yearsValue);
      }
    );
  };

  /** function to set min and max Year value from slider value */
  handleYearMinMaxValues = (value) => {
    let minYears = value[0];
    let maxYears = value[1];
    this.setState({
      minYears,
      maxYears,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeYearInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minYears") {
      this.setState(
        {
          maxYears: this.state.maxYears,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minYears, this.state.maxYears];
          this.setState({
            yearsValue: newValue,
          });
        }
      );
    } else if (name == "maxYears") {
      this.setState(
        {
          minYears: this.state.minYears,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minYears, this.state.maxYears];
          this.setState({
            yearsValue: newValue,
          });
        }
      );
    }
  };

  /** function to toggle radius dropdown */
  radiusDropdownOpen = () => {
    this.props.currentLocation
      ? this.setState({
          radiusDropdown: !this.state.radiusDropdown,
        })
      : this.props.dispatch(getCurrentLocation());
  };

  /** function to set radius value on slide */
  onChangeRadius = (event, value) => {
    this.setState(
      {
        valueRadius: value,
      },
      () => {
        this.handleApiCallforRadius(this.state.valueRadius);
      }
    );
  };

  handleApiCallforRadius = debounce((value) => {
    let urlParams = {};
    urlParams[`distanceMax`] = value;
    urlParams[`latitude`] = this.props.currentLocation.latitude;
    urlParams[`longitude`] = this.props.currentLocation.longitude;
    let apiPayload = { ...this.state.payload, ...urlParams };

    this.props.handleUrlParams("distanceMax", urlParams); // function to add filterdata in url params
    this.setState(
      {
        payload: apiPayload,
      },
      () => {
        this.props.handlePostProductsAPI(this.state.payload); // API call
      }
    );
  }, 500);

  /** function to toggle price dropdown */
  priceDropdownOpen = () => {
    if (!this.state.minPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.minPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    if (!this.state.maxPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.maxPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    this.setState({
      priceDropdown: !this.state.priceDropdown,
    });
  };

  /** function to handle priceInput */
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  /** function to set min and max price */
  handlePriceRange = () => {
    let { minPrice, maxPrice } = this.state.inputpayload;
    this.setState(
      {
        minPrice,
        maxPrice,
        priceDropdown: !this.state.priceDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`priceRange`] = {};
        apiPayload[`priceRange`].fromPrice = this.state.minPrice;
        apiPayload[`priceRange`].toPrice = this.state.maxPrice;

        this.props.handleUrlParams("priceRange", apiPayload.priceRange); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API call
          }
        );
      }
    );
  };

  handleToggleFilters = () => {
    this.setState({
      filterToggle: !this.state.filterToggle,
    });
  };

  /** Function to inputpayload for selectInput */
  handleOnSelectInput = (name) => (event, { option }) => {
    if (
      event != null &&
      option != undefined &&
      (name == "manufactor" || name == "boat_model") &&
      this.state[name]?.length > 0
    ) {
      let tempPayload = this.state[name];
      let elementId = tempPayload.findIndex(
        (data) => data.label == option.label
      );
      if (elementId != -1) {
        tempPayload.splice(elementId, 1);
      } else {
        tempPayload = event;
      }
      this.setState(
        {
          [name]: tempPayload,
        },
        () => {
          this.handleAPIPayload(event, name);
        }
      );
    } else {
      this.setState(
        {
          [name]: event,
        },
        () => {
          this.handleAPIPayload(event, name);
        }
      );
    }
  };

  handleAPIPayload = (event, name) => {
    if (name == "manufactor") {
      let tempPayload = [...this.state.breadcrumbsData];
      if (this.state.manufactor != null) {
        let value =
          this.state.manufactor?.length > 0
            ? this.state.manufactor[this.state.manufactor.length - 1]?.label
            : null;
        tempPayload[1] = value;
      } else {
        tempPayload.pop();
      }

      this.setState(
        {
          boat_model: {},
          breadcrumbsData: [...tempPayload],
          bannerOpen: false,
        },
        () => {
          if (this.props.urlString && this.props.urlString.modelName) {
            this.props.handleUrlParams("modelName", ""); // function to add filterdata in url params
          }
        }
      );
    }
    if (event instanceof Array || this.state[name] == null) {
      let apiPayload = { ...this.state.payload };
      let labelArr =
        this.state[name] != null &&
        this.state[name] &&
        this.state[name].length > 0
          ? this.state[name].map((data) => data.label)
          : "";
      apiPayload[name] = labelArr.toString();
      this.props.handleUrlParams(`${name}`, labelArr.toString()); // function to add filterdata in url params
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    } else {
      let apiPayload = { ...this.state.payload };
      if (name == "Budget") {
        apiPayload[`loan`] = {};
        apiPayload[`loan`].fromLoan = 0;
        apiPayload[`loan`].toLoan = this.state[name].value;
        let urlPayload = this.state[name].label == "All" ? "" : apiPayload.loan;
        this.props.handleUrlParams("loan", urlPayload); // function to add filterdata in url params
      } else {
        let payloadName = name == "boat_model" ? "modelName" : name;
        apiPayload[payloadName] =
          name == "postedWithin"
            ? this.handlePostedWithin(this.state[name].value)
            : name == "boat_model" || name == "color"
            ? this.state[name].label
            : this.state[name].value;
        this.props.handleUrlParams(
          `${payloadName}`,
          name == "boat_model" || name == "color"
            ? this.state[name].label
            : this.state[name].value
        ); // function to add filterdata in url params
      }
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    }
  };

  handleLoanFilter = (data) => {
    this.setState(
      {
        Budget: data,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`loan`] = {};
        apiPayload[`loan`].fromLoan = 0;
        apiPayload[`loan`].toLoan = data.value;
        this.props.handleUrlParams("loan", apiPayload.loan); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  handleManufacturerFilter = (data) => {
    let apiPayload = { ...this.state.payload };
    apiPayload[`manufactor`] = data.value; // value is manufacturer name here
    this.props.handleUrlParams("manufactor", data.value); // function to add filterdata in url params
    this.setState(
      {
        payload: apiPayload,
      },
      () => {
        this.props.handlePostProductsAPI(this.state.payload);
      }
    );
  };

  /** function to set API postedWithin payload  */
  handlePostedWithin = (value) => {
    switch (value) {
      case "24hr":
        return "1";
        break;
      case "15days":
        return "2";
        break;
      case "30days":
        return "3";
        break;
    }
  };

  render() {
    const { bannerOpen, tabName, breadcrumbsData } = this.state;
    const {
      handleToggleBanner,
      handleChangeTab,
      handleLoanFilter,
      handleManufacturerFilter,
    } = this;
    const { paginationLoader } = this.props;
    return (
      <Wrapper>
        <div className="row m-0 align-items-center ProfessionalsPageSec">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 my-3 py-3">
              <div
                id="loderOverlay"
                className="row m-0"
                style={{
                  display: this.props.apiLoading ? "block" : "none",
                }}
              >
                <div className="loader">
                  <PageLoader loading={true} color={env.WHITE_COLOR} />
                </div>
              </div>
              <div className="row m-0">
                <CustomBreadCrumbs
                  boatsPage={true}
                  breadcrumbs={breadcrumbsData}
                  separator={
                    <img
                      src={env.Chevron_Right_Darkgrey}
                      width="4"
                      style={{ margin: "0 3px" }}
                    ></img>
                  }
                />
              </div>
              <div className="row m-0 align-items-center justify-content-between my-3">
                <div className="col p-0">
                  <h6 className="Heading">
                    Boats{" "}
                    <span>
                      {this.props.filterPostData &&
                      this.props.filterPostData.length > 0
                        ? `(${this.props.filterPostData.length})`
                        : ""}
                    </span>
                  </h6>
                </div>
                {bannerOpen ? (
                  ""
                ) : (
                  <div className="col-4 p-0">
                    <div className="row m-0 align-items-center justify-content-center">
                      <div className="col-5 p-0">
                        <div className="greyBg_btn noRightBorderRadius">
                          <ButtonComp
                            onClick={handleChangeTab.bind(this, "monthlyDeals")}
                          >
                            <img
                              src={env.Dollar_Circled_Grey}
                              style={{ width: "0.878vw", marginRight: "5px" }}
                            ></img>
                            Monthly Deals
                          </ButtonComp>
                        </div>
                      </div>
                      <div className="col-5 p-0">
                        <div className="greyBg_btn noLeftBorderRadius noRightBorderRadius">
                          <ButtonComp
                            onClick={handleChangeTab.bind(this, "popular")}
                          >
                            <img
                              src={env.Star_Grey}
                              style={{ width: "0.951vw", marginRight: "5px" }}
                            ></img>
                            Popular
                          </ButtonComp>
                        </div>
                      </div>
                      <div className="col-1 p-0">
                        <div className="greyBg_btn noLeftBorderRadius">
                          <ButtonComp onClick={handleToggleBanner}>
                            <img
                              src={env.Chevron_Left_Darkgrey}
                              style={{ width: "0.658vw" }}
                            ></img>
                          </ButtonComp>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              {bannerOpen ? (
                <div className="row m-0 align-items-center justify-content-center">
                  {tabName == "monthlyDeals" ? (
                    <MonthlyDealsBanner
                      handleToggleBanner={handleToggleBanner}
                      handleChangeTab={handleChangeTab}
                      handleLoanFilter={handleLoanFilter}
                    />
                  ) : tabName == "popular" ? (
                    <PopularBanner
                      handleToggleBanner={handleToggleBanner}
                      handleChangeTab={handleChangeTab}
                      handleManufacturerFilter={handleManufacturerFilter}
                    />
                  ) : (
                    ""
                  )}
                </div>
              ) : (
                ""
              )}
              <div className="row m-0 mt-3">
                <BoatsFilter
                  manufactor={this.state.manufactor}
                  condition={this.state.condition}
                  handleChangeCondition={this.handleChangeCondition}
                  handleTypeOnSelect={this.handleTypeOnSelect}
                  selectedYear={this.state.selectedYear}
                  clearSelection={this.clearSelection}
                  selectedBoatsType={this.state.selectedBoatsType}
                  lengthDropdownOpen={this.lengthDropdownOpen}
                  lengthDropdown={this.state.lengthDropdown}
                  handleLengthRange={this.handleLengthRange}
                  handleSliderLegthRange={this.handleSliderLegthRange}
                  lengthValue={this.state.lengthValue}
                  handleOnchangeLengthInput={this.handleOnchangeLengthInput}
                  maxLength={this.state.maxLength}
                  minLength={this.state.minLength}
                  radiusDropdownOpen={this.radiusDropdownOpen}
                  radiusDropdown={this.state.radiusDropdown}
                  onChangeRadius={this.onChangeRadius}
                  valueRadius={this.state.valueRadius}
                  priceDropdownOpen={this.priceDropdownOpen}
                  handleOnchangeInput={this.handleOnchangeInput}
                  handlePriceRange={this.handlePriceRange}
                  inputpayload={this.state.inputpayload}
                  priceDropdown={this.state.priceDropdown}
                  hasWarranty={this.state.hasWarranty}
                  engineHoursDropdown={this.state.engineHoursDropdown}
                  engineHoursDropdownOpen={this.engineHoursDropdownOpen}
                  handleengineHoursRange={this.handleengineHoursRange}
                  handleSliderEngineHoursRange={
                    this.handleSliderEngineHoursRange
                  }
                  engineHoursValue={this.state.engineHoursValue}
                  handleOnchangeEngineHoursInput={
                    this.handleOnchangeEngineHoursInput
                  }
                  maxEngineHours={this.state.maxEngineHours}
                  minEngineHours={this.state.minEngineHours}
                  handleToggleFilters={this.handleToggleFilters}
                  filterToggle={this.state.filterToggle}
                  handleOnSelectInput={this.handleOnSelectInput}
                  engineType={this.state.engineType}
                  fuelType={this.state.fuelType}
                  saleBy={this.state.saleBy}
                  Budget={this.state.Budget}
                  color={this.state.color}
                  boat_model={this.state.boat_model}
                  yearDropdown={this.state.yearDropdown}
                  yearDropdownOpen={this.yearDropdownOpen}
                  handleYearsRange={this.handleYearsRange}
                  handleSliderYearsRange={this.handleSliderYearsRange}
                  yearsValue={this.state.yearsValue}
                  handleOnchangeYearInput={this.handleOnchangeYearInput}
                  maxYears={this.state.maxYears}
                  minYears={this.state.minYears}
                />
              </div>
              <div className="row m-0 BoatListSec justify-content-center position-relative">
                {/* Boat List Module */}
                <BoatList
                  boatsPage={true}
                  BoatsList={this.props.filterPostData}
                  handleOnSelectInput={this.handleOnSelectInput}
                  postedWithin={this.state.postedWithin}
                />

                {paginationLoader && (
                  <div>
                    <PageBeatLoader size={10} loading={paginationLoader} />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .ProfessionalsPageSec {
              background: ${env.BG_LightGREY_COLOR};
            }

            .Heading {
              margin: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 1.171vw;
              color: ${env.FONTGREY_COLOR};
            }
            .Heading span {
              font-size: 0.878vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${env.GREY_VARIANT_3};
            }
            :global(.greyBg_btn button) {
              min-width: fit-content;
              width: 99.5%;
              height: 2.489vw;
              padding: 0.366vw 0.878vw;
              background: ${env.GREY_VARIANT_3};
              color: ${env.GREY_VARIANT_1};
              margin: 0;
              border-radius: 0.219vw;
              text-transform: capitalize;
            }
            :global(.noRightBorderRadius button) {
              border-top-right-radius: 0 !important;
              border-bottom-right-radius: 0 !important;
            }
            :global(.noLeftBorderRadius button) {
              border-top-left-radius: 0 !important;
              border-bottom-left-radius: 0 !important;
            }
            :global(.greyBg_btn button span) {
              font-size: 0.951vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.greyBg_btn button:focus),
            :global(.greyBg_btn button:active) {
              background: ${env.GREY_VARIANT_3};
              outline: none;
              box-shadow: none;
            }
            :global(.greyBg_btn button:hover) {
              background: ${env.GREY_VARIANT_3};
            }
            .BoatListSec {
              padding: 0.732vw 0;
              background: ${env.WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(BoatsNew);
