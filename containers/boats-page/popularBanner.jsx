import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";

class PopularBanner extends Component {
  render() {
    const { handleToggleBanner, handleChangeTab } = this.props;
    const ManufacturersImgs = [
      { img: env.Manufacturer_1, value: "SEA RAY BOATS" },
      { img: env.Manufacturer_2, value: "BAYLINER" },
      { img: env.Manufacturer_3, value: "YAMAHA" },
      { img: env.Manufacturer_4, value: "BENNINGTON MARINE" },
      { img: env.Manufacturer_5, value: "TRACKER BY TRACKER MARINE" },
      { img: env.Manufacturer_6, value: "WHALER" },
    ];
    return (
      <Wrapper>
        <div className="col-12 p-0 PopularTab position-relative">
          <div className="TabSec row m-0 align-items-center justify-content-end">
            <div className="col-4 p-0">
              <div className="row m-0 align-items-center justify-content-center">
                <div className="monthly-deal-button1 p-0">
                  <div className="transparentBg_btn noRightBorderRadius">
                    <ButtonComp
                      onClick={handleChangeTab.bind(this, "monthlyDeals")}
                    >
                      <img
                        src={env.Dollar_Circled_White}
                        style={{ width: "0.878vw", marginRight: "5px" }}
                      ></img>
                      Monthly Deals
                    </ButtonComp>
                  </div>
                </div>
                <div className="monthly-deal-button2 p-0">
                  <div className="orangeBg_btn noRightBorderRadius noLeftBorderRadius">
                    <ButtonComp onClick={handleChangeTab.bind(this, "popular")}>
                      <img
                        src={env.Star_White}
                        style={{ width: "0.951vw", marginRight: "8px" }}
                      ></img>
                      Popular
                    </ButtonComp>
                  </div>
                </div>
                <div className="col-1 p-0">
                  <div className="transparentBg_btn noLeftBorderRadius">
                    <ButtonComp onClick={handleToggleBanner}>
                      <img
                        src={env.Chevron_Down_White}
                        style={{ width: "0.878vw" }}
                      ></img>
                    </ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="HeadingSec">
            <h4 className="bannerHeading">
              Popular <br />
              <span>Boat Manufacturers</span>
            </h4>
          </div>
          <div className="bannerFooter row m-0 align-items-center justify-content-center">
            {ManufacturersImgs &&
              ManufacturersImgs.map((data, index) => (
                <div
                  className="col-3 p-0 manufacturerCard"
                  key={index}
                  onClick={this.props.handleManufacturerFilter.bind(this, data)}
                >
                  <img src={data.img}></img>
                </div>
              ))}
          </div>
        </div>
        <style jsx>
          {`
            .PopularTab {
              background: url(${env.PopularBg});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              position: relative;
              min-height: 24.158vw;
              border-radius: 0.219vw;
            }
            .monthly-deal-button1 {
              width: 10.2083vw;
            }
            .monthly-deal-button2 {
              width: 10vw;
            }
            .TabSec {
              width: 100%;
              position: absolute;
              top: 1.098vw;
            }
            :global(.orangeBg_btn button) {
              min-width: fit-content;
              width: 99.5%;
              height: 2.489vw;
              padding: 0.366vw 0.878vw;
              background: ${env.Orange_Color};
              color: ${env.WHITE_COLOR};
              margin: 0;
              border-radius: 0.219vw;
              text-transform: capitalize;
            }
            :global(.noRightBorderRadius button) {
              border-top-right-radius: 0 !important;
              border-bottom-right-radius: 0 !important;
            }
            :global(.noLeftBorderRadius button) {
              border-top-left-radius: 0 !important;
              border-bottom-left-radius: 0 !important;
            }
            :global(.orangeBg_btn button span) {
              font-size: 0.878vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.orangeBg_btn button:focus),
            :global(.orangeBg_btn button:active) {
              background: ${env.Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.orangeBg_btn button:hover) {
              background: ${env.Orange_Color};
            }
            :global(.transparentBg_btn button) {
              min-width: fit-content;
              width: 99.5%;
              height: 2.489vw;
              padding: 0.366vw 0.878vw;
              background: rgba(255, 255, 255, 0.3);
              color: ${env.WHITE_COLOR};
              margin: 0;
              border-radius: 0.219vw;
              text-transform: capitalize;
            }
            :global(.transparentBg_btn button span) {
              font-size: 0.878vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.transparentBg_btn button:focus),
            :global(.transparentBg_btn button:active) {
              background: rgba(255, 255, 255, 0.3);
              outline: none;
              box-shadow: none;
            }
            :global(.transparentBg_btn button:hover) {
              background: rgba(255, 255, 255, 0.3);
            }
            .HeadingSec {
              position: absolute;
              top: 25%;
              left: 1.098vw;
            }
            .bannerHeading {
              margin: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 2.049vw;
              color: ${env.WHITE_COLOR};
            }
            .bannerHeading span {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${env.Orange_Color};
            }
            .bannerFooter {
              position: absolute;
              // bottom: 1.098vw;
              bottom: 1.3vw;
              width: 100%;
            }
            .bannerFooter > div:first-child {
              margin-left: 1.016vw;
            }
            .manufacturerCard {
              max-width: 11.891vw;
              margin-right: 1.016vw;
            }
            @media (min-width: 1500px) and (max-width: 1920px) {
              .manufacturerCard {
                max-width: 11.4583vw !important;
                // margin-right: 0.9375vw !important;
                margin-right: 1.016vw !important;
              }
            }
            .manufacturerCard img {
              width: 100%;
              object-fit: conver;
              border-radius: 0.146vw;
            }
            .manufacturerCard:hover > img {
              border: 1.5px solid ${env.Orange_Color};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default PopularBanner;
