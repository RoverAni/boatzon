import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";

class MonthlyDealsBanner extends Component {
  render() {
    const { handleToggleBanner, handleChangeTab } = this.props;
    const monthlyPrices = [
      { value: 149, label: "Boats under $149/mo" },
      { value: 299, label: "Boats under $299/mo" },
      { value: 499, label: "Boats under $499/mo" },
      { value: 699, label: "Boats under $699/mo" },
      { value: 999, label: "Boats under $999/mo" },
    ];
    return (
      <Wrapper>
        <div className="col-12 p-0 MonthlyDealsTab position-relative">
          <div className="TabSec row m-0 align-items-center justify-content-end">
            <div className="col-4 p-0">
              <div className="row m-0 align-items-center justify-content-center">
                <div className="monthly-deal-button1 p-0">
                  <div className="greenBg_btn noRightBorderRadius">
                    <ButtonComp
                      onClick={handleChangeTab.bind(this, "monthlyDeals")}
                    >
                      <img
                        src={env.Dollar_Circled_White}
                        style={{ width: "0.878vw", marginRight: "5px" }}
                      ></img>
                      Monthly Deals
                    </ButtonComp>
                  </div>
                </div>
                <div className="monthly-deal-button2 p-0">
                  <div className="transparentBg_btn noRightBorderRadius noLeftBorderRadius">
                    <ButtonComp onClick={handleChangeTab.bind(this, "popular")}>
                      <img
                        src={env.Star_White}
                        style={{ width: "0.951vw", marginRight: "8px" }}
                      ></img>
                      Popular
                    </ButtonComp>
                  </div>
                </div>
                <div className="col-1 p-0">
                  <div className="transparentBg_btn noLeftBorderRadius">
                    <ButtonComp onClick={handleToggleBanner}>
                      <img
                        src={env.Chevron_Down_White}
                        style={{ width: "0.878vw" }}
                      ></img>
                    </ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="HeadingSec">
            <h4 className="bannerHeading">
              Boats That <br />
              <span>Fit Your Budget</span>
            </h4>
          </div>
          <div className="bannerFooter row m-0 align-items-center justify-content-center">
            {monthlyPrices &&
              monthlyPrices.map((data, index) => (
                <div className="col-3 p-0 priceCardWidth" key={index}>
                  <div className="priceCard">
                    <p>Boats for under</p>
                    <h6>
                      <img src={env.Dollar_White_Icon}></img>
                      {data.value} per month
                    </h6>
                    <div className="shopNow_btn">
                      <ButtonComp
                        onClick={this.props.handleLoanFilter.bind(this, data)}
                      >
                        Shop Now
                      </ButtonComp>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
        <style jsx>
          {`
            .MonthlyDealsTab {
              background: url(${env.MonthlyDealsBg});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              position: relative;
              min-height: 24.158vw;
              border-radius: 0.219vw;
            }
            .monthly-deal-button1 {
              width: 10.2083vw;
            }
            .monthly-deal-button2 {
              width: 10vw;
            }
            .TabSec {
              width: 100%;
              position: absolute;
              top: 1.098vw;
            }
            :global(.greenBg_btn button) {
              min-width: fit-content;
              width: 99.5%;
              height: 2.489vw;
              padding: 0.366vw 0.878vw;
              background: ${env.GREEN_COLOR};
              color: ${env.WHITE_COLOR};
              margin: 0;
              border-radius: 0.219vw;
              text-transform: capitalize;
            }
            :global(.noRightBorderRadius button) {
              border-top-right-radius: 0 !important;
              border-bottom-right-radius: 0 !important;
            }
            :global(.noLeftBorderRadius button) {
              border-top-left-radius: 0 !important;
              border-bottom-left-radius: 0 !important;
            }
            :global(.greenBg_btn button span) {
              font-size: 0.878vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.greenBg_btn button:focus),
            :global(.greenBg_btn button:active) {
              background: ${env.GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.greenBg_btn button:hover) {
              background: ${env.GREEN_COLOR};
            }
            :global(.transparentBg_btn button) {
              min-width: fit-content;
              width: 99.5%;
              height: 2.489vw;
              padding: 0.366vw 0.878vw;
              background: rgba(255, 255, 255, 0.3);
              color: ${env.WHITE_COLOR};
              margin: 0;
              border-radius: 0.219vw;
              text-transform: capitalize;
            }
            :global(.transparentBg_btn button span) {
              font-size: 0.878vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.transparentBg_btn button:focus),
            :global(.transparentBg_btn button:active) {
              background: rgba(255, 255, 255, 0.3);
              outline: none;
              box-shadow: none;
            }
            :global(.transparentBg_btn button:hover) {
              background: rgba(255, 255, 255, 0.3);
            }
            .HeadingSec {
              position: absolute;
              top: 25%;
              left: 1.098vw;
            }
            .bannerHeading {
              margin: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 2.049vw;
              color: ${env.WHITE_COLOR};
            }
            .bannerHeading span {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${env.Orange_Color};
            }
            .bannerFooter {
              position: absolute;
              bottom: 1.3vw;
              width: 100%;
            }
            .bannerFooter > div:first-child {
              margin-left: 1.146vw;
            }
            @media (min-width: 1500px) and (max-width: 1920px) {
              .priceCardWidth {
                max-width: 13.5416vw !important;
                margin-right: 0.9375vw !important;
              }
            }
            .priceCardWidth {
              max-width: 14.277vw;
              margin-right: 1.146vw;
            }
            .priceCard {
              border: 0.0732vw solid ${env.WHITE_COLOR};
              border-radius: 0.219vw;
              padding: 0.951vw 1.464vw;
              text-align: center;
              cursor: pointer;
            }
            .priceCard:hover {
              border: 2px solid ${env.WHITE_COLOR};
            }

            .priceCard p {
              margin-bottom: 0.156vw;
              font-family: "Open Sans" !important;
              font-size: 0.729vw;
              color: ${env.WHITE_COLOR};
            }
            .priceCard h6 {
              margin-bottom: 0.366vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 1.25vw;
              color: ${env.WHITE_COLOR};
            }
            .priceCard h6 img {
              width: 0.512vw;
              margin-bottom: 0.22vw;
              margin-right: 0.0732vw;
            }
            :global(.shopNow_btn button) {
              width: 10.469vw;
              height: 1.984vw;
              padding: 0;
              background: transparent;
              color: ${env.WHITE_COLOR};
              margin: 0;
              border: 1px solid ${env.WHITE_COLOR};
              border-radius: 0.219vw;
              text-transform: capitalize;
            }
            .priceCard:hover > :global(.shopNow_btn button) {
              color: ${env.GREEN_COLOR};
              background: ${env.WHITE_COLOR};
            }
            :global(.shopNow_btn button span) {
              font-size: 0.833vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.shopNow_btn button:focus),
            :global(.shopNow_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.shopNow_btn button:hover) {
              background: transparent;
            }
            // @media only screen and (min-width: 1550px) and (max-width: 1899px) {
            //   .bannerFooter > div:first-child {
            //     margin-left: 1.244vw;
            //   }
            //   .priceCardWidth {
            //     max-width: 13.753vw;
            //     margin-right: 1.244vw;
            //   }
            // }
            // @media only screen and (min-width: 1900px) {
            //   .bannerFooter > div:first-child {
            //     margin-left: 1.244vw;
            //   }
            //   .priceCardWidth {
            //     max-width: 14.265vw;
            //     margin-right: 1.244vw;
            //   }
            // }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default MonthlyDealsBanner;
