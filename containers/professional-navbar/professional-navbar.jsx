import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  APP_PRO_LOGO,
  Chat_Icon_Grey,
  GREY_VARIANT_2,
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  Seller_ProfilePic,
  Dark_Blue_Color,
  FONTGREY_COLOR,
  Logout_Grey_Icon,
  MyAcc_Grey_Icon,
  PublicProfile_Grey_Icon,
  Transaction_History_Grey,
  Saved_Grey_Icon,
  Selling_Grey_Icon,
  Buying_Grey_Icon,
  THEME_COLOR_Opacity,
  THEME_COLOR,
  Logout_Blue_Icon,
  MyAcc_Blue_Icon,
  Transaction_History_Blue,
  Saved_Blue_Icon,
  PublicProfile_Blue_Icon,
  Buying_Blue_Icon,
  Selling_Blue_Icon,
  Boatzon_ProfilePic,
  Profile_Logo,
  Professional_Profile_Dp,
  Chevron_Down,
  LogIn_Icon,
  GREY_VARIANT_1,
  LIGHT_THEME_BG,
  GREY_VARIANT_3,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
// Reactstrap Components
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import CustomLink from "../../components/Link/Link";
import { getCookie, removeCookie } from "../../lib/session";
import { getUserData } from "../../redux/actions/user-data/user-data";
import { userLogout } from "../../services/auth";
import Model from "../../components/model/model";
import LoginPage from "../auth-modals/login/login-page";
import { withRouter } from "next/router";
import Snackbar from "../../components/snackbar";

class ProfessionalNavbar extends Component {
  state = {
    signUpModel: false,
    dropdownOpen: false,
    openpage: false,
    loginPage: false,
    messageDropdown: false,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState(
      {
        AuthPass,
        openpage: true,
      },
      () => {
        if (AuthPass) {
          this.props.dispatch(getUserData());
        }
      }
    );
  }

  handleToggleMessageDropdown = () => {
    console.log("Clicked");
    this.setState({
      messageDropdown: !this.state.messageDropdown,
    });
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleLogout = () => {
    userLogout()
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        this.setState({
          usermessage: response.message,
          variant: this.handleSnackbar(response),
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });

        if (response.code == 200) {
          removeCookie("token");
          removeCookie("authPass");
          removeCookie("mqttId");
          removeCookie("uid");
          removeCookie("userName");
          removeCookie("ucity");
          window.location.replace("/");
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

   // Function for the Notification (Snakbar)
   handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const { pathname } = this.props.router;
    return (
      <Wrapper>
        <div className="proNavbar">
          {this.state.openpage ? (
            <div className="container p-md-0">
              <div className="screenWidth mx-auto">
                <div className="row m-0 align-items-center justify-content-between py-2">
                  <div className="col-6 p-0">
                    <CustomLink
                      href={
                        pathname == "/professional-signup"
                          ? "/"
                          : "/professional-signup"
                      }
                    >
                      <img
                        src={APP_PRO_LOGO}
                        className="professionalAppLogo"
                      ></img>
                    </CustomLink>
                  </div>
                  <div className="col-6 p-0">
                    <div className="d-flex align-items-center justify-content-end">
                      <div className="pricing_btn">
                        <CustomLink href="/pro-membership-plans">
                          <ButtonComp>Pricing</ButtonComp>
                        </CustomLink>
                      </div>
                      <div>
                        {this.state.AuthPass ? (
                          <div className="d-flex align-items-center">
                            <ButtonDropdown
                              isOpen={this.state.messageDropdown}
                              toggle={this.handleToggleMessageDropdown}
                              className="messageDropdownBtn"
                            >
                              <DropdownToggle>
                                <img
                                  src={Chat_Icon_Grey}
                                  className="chatIcon"
                                ></img>
                              </DropdownToggle>
                              <DropdownMenu className="messageDropdownList">
                                <p className="recentText">Recent</p>
                                <CustomLink href="#">
                                  <DropdownItem className="dropdownItem">
                                    <div className="row m-0 align-items-center">
                                      <div className="col-2 p-0">
                                        <img
                                          src={Seller_ProfilePic}
                                          className="messageProfilePic"
                                        ></img>
                                      </div>
                                      <div className="col-10 p-0 MessengerInfo">
                                        <p className="messengerName">Max</p>
                                        <p className="messengerManufacturer">
                                          Bayliner
                                        </p>
                                        <span className="messageDate">
                                          5 days ago
                                        </span>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                <DropdownItem className="dropdownItem">
                                  <div className="row m-0 align-items-center">
                                    <div className="col-2 p-0">
                                      <img
                                        src={Professional_Profile_Dp}
                                        className="messageProfilePic"
                                      ></img>
                                    </div>
                                    <div className="col-10 p-0 MessengerInfo">
                                      <p className="messengerName">Max</p>
                                      <p className="messengerManufacturer">
                                        Bayliner
                                      </p>
                                      <span className="messageDate">
                                        5 days ago
                                      </span>
                                    </div>
                                  </div>
                                </DropdownItem>
                              </DropdownMenu>
                            </ButtonDropdown>
                            <ButtonDropdown
                              isOpen={this.state.dropdownOpen}
                              toggle={this.toggle}
                              className="DropdownBtn"
                            >
                              <DropdownToggle>
                                <div className="d-flex align-items-center ProfileSec">
                                  <img
                                    src={
                                      (this.props.userProfileData &&
                                        this.props.userProfileData
                                          .profilePicUrl) ||
                                      Profile_Logo
                                    }
                                    className="profileLogo"
                                  ></img>
                                  <span>
                                    {this.props.userProfileData &&
                                      this.props.userProfileData.username}
                                  </span>
                                  <img
                                    src={Chevron_Down}
                                    className="chevronDown"
                                  ></img>
                                </div>
                              </DropdownToggle>
                              <DropdownMenu className="DropdownList">
                                <CustomLink href="/user-profile">
                                  <DropdownItem>
                                    <div className="row m-0 align-items-center userAcc">
                                      <div className="col-3 p-0 firstSec">
                                        <div className="dd_profileIcon"></div>
                                      </div>
                                      <div className="col-9 p-0">
                                        <div className="d-flex flex-column ml-2">
                                          <span className="userName">
                                            {this.props.userProfileData &&
                                              this.props.userProfileData
                                                .username}
                                          </span>
                                          <span className="userType">User</span>
                                        </div>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                {this.props.userProfileData &&
                                this.props.userProfileData.businessName ? (
                                  <CustomLink href="/professional-user-profile">
                                    <DropdownItem>
                                      <div className="row m-0 align-items-center profileAcc">
                                        <div className="col-3 p-0 firstSec">
                                          <div className="profilePic"></div>
                                        </div>
                                        <div className="col-9 p-0">
                                          <div className="d-flex flex-column ml-2">
                                            <span className="userName">
                                              {this.props.userProfileData &&
                                                this.props.userProfileData
                                                  .businessName}
                                            </span>
                                            <span className="userType">
                                              Professional
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    </DropdownItem>
                                  </CustomLink>
                                ) : (
                                  ""
                                )}
                                <CustomLink href="/selling">
                                  <DropdownItem>
                                    <div className="row m-0 align-items-center SellingLink_Sec ProfileLink_Sec">
                                      <div className="col-3 p-0 firstSec">
                                        <div className="text-center sellingIcon"></div>
                                      </div>
                                      <div className="col-9 p-0">
                                        <p className="linkName m-0 ml-2">
                                          Selling
                                        </p>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                <CustomLink href="/buying">
                                  <DropdownItem>
                                    <div className="row m-0 align-items-center BuyingLink_Sec ProfileLink_Sec">
                                      <div className="col-3 p-0 firstSec">
                                        <div className="text-center buyingIcon"></div>
                                      </div>
                                      <div className="col-9 p-0">
                                        <p className="linkName m-0 ml-2">
                                          Buying
                                        </p>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                <CustomLink href="/following">
                                  <DropdownItem>
                                    <div className="row m-0 align-items-center folloingLink_Sec ProfileLink_Sec">
                                      <div className="col-3 p-0 firstSec">
                                        <div className="text-center followingIcon"></div>
                                      </div>
                                      <div className="col-9 p-0">
                                        <p className="linkName m-0 ml-2">
                                          Following
                                        </p>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                <CustomLink href="/purchases-sales">
                                  <DropdownItem>
                                    <div className="row m-0 align-items-center salesLink_Sec ProfileLink_Sec">
                                      <div className="col-3 p-0 firstSec">
                                        <div className="text-center salesIcon"></div>
                                      </div>
                                      <div className="col-9 p-0">
                                        <p className="linkName m-0 ml-2">
                                          Purchases & Sales
                                        </p>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                <DropdownItem>
                                  <div className="row m-0 align-items-center publicProfileLink_Sec ProfileLink_Sec">
                                    <div className="col-3 p-0 firstSec">
                                      <div className="text-center publicProfileIcon"></div>
                                    </div>
                                    <div className="col-9 p-0">
                                      <p className="linkName m-0 ml-2">
                                        My Public Profile
                                      </p>
                                    </div>
                                  </div>
                                </DropdownItem>
                                <CustomLink href="/my-account/general-settings">
                                  <DropdownItem>
                                    <div className="row m-0 align-items-center myAccLink_Sec ProfileLink_Sec">
                                      <div className="col-3 p-0 firstSec">
                                        <div className="text-center myAccIcon"></div>
                                      </div>
                                      <div className="col-9 p-0">
                                        <p className="linkName m-0 ml-2">
                                          My Account
                                        </p>
                                      </div>
                                    </div>
                                  </DropdownItem>
                                </CustomLink>
                                <DropdownItem onClick={this.handleLogout}>
                                  <div className="row m-0 align-items-center logoutLink_Sec ProfileLink_Sec">
                                    <div className="col-3 p-0 firstSec">
                                      <div className="text-center logoutIcon"></div>
                                    </div>
                                    <div className="col-9 p-0">
                                      <p className="linkName m-0 ml-2">
                                        Logout
                                      </p>
                                    </div>
                                  </div>
                                </DropdownItem>
                              </DropdownMenu>
                            </ButtonDropdown>
                          </div>
                        ) : (
                          <div className="d-flex align-items-center">
                            <div>
                              <div
                                className="LoginSec"
                                onClick={this.handleLogin}
                              >
                                <ButtonComp>
                                  <img
                                    src={LogIn_Icon}
                                    className="loginIcon"
                                  ></img>{" "}
                                  Login
                                </ButtonComp>
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>

        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        <style jsx>
          {`
            .professionalAppLogo {
              width: 6.954vw;
              cursor: pointer;
            }
            .profileLogo {
              width: 1.171vw;
              height: 1.171vw;
              border-radius: 50%;
            }
            :global(.messageDropdownBtn > button) {
              background: ${WHITE_COLOR} !important;
              border: none;
              padding: 0.292vw 0.878vw !important;
              line-height: 0.5;
            }
            :global(.messageDropdownBtn > button:active),
            :global(.messageDropdownBtn > button:focus) {
              outline: none !important;
              background: none !important;
              box-shadow: none !important;
            }
            :global(.messageDropdownBtn
                > .messageDropdownList
                > .dropdownItem) {
              padding: 0.366vw 0.585vw !important;
            }
            :global(.messageDropdownList .dropdownItem:hover) {
              background: ${BG_LightGREY_COLOR} !important;
            }
            :global(.messageDropdownList) {
              padding: 0.146vw !important;
            }
            .chatIcon {
              width: 1.244vw;
              cursor: pointer;
            }
            .recentText {
              font-size: 0.732vw;
              margin: 0;
              padding: 0 0 0.219vw 0.219vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .loginIcon {
              width: 0.951vw;
              margin: 0 0.292vw 0 0.219vw;
            }
            :global(.LoginSec button),
            :global(.SignUpSec button) {
              color: ${GREY_VARIANT_1};
              padding: 0 0.585vw;
              text-transform: capitalize;
              cursor: pointer;
              border: none;
              line-height: 0;
            }
            :global(.LoginSec button:hover),
            :global(.SignUpSec button:hover) {
              background: none;
            }
            :global(.LoginSec button:focus),
            :global(.LoginSec button:active),
            :global(.SignUpSec button:focus),
            :global(.SignUpSec button:active) {
              border: none !important;
              box-shadow: none;
              outline: 0 !important;
            }
            :global(.LoginSec button > span),
            :global(.SignUpSec button > span) {
              font-size: 0.878vw;
              font-family: "Open Sans-SemiBold" !important;
              display: flex;
              align-items: center;
              line-height: 0;
            }
            .messageProfilePic {
              width: 100%;
              object-fit: cover;
              border-radius: 50%;
            }
            .MessengerInfo {
              positon: relative;
              padding: 0 0 0 0.585vw !important;
            }
            .messengerName {
              font-size: 0.732vw;
              text-transform: capitalize;
              font-weight: 600;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${Dark_Blue_Color};
              margin: 0;
            }
            .messengerManufacturer {
              font-size: 0.658vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
            }
            .messageDate {
              font-size: 0.585vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              position: absolute;
              top: 0;
              right: 0;
            }
            .recentText {
              font-size: 0.732vw;
              margin: 0;
              padding: 0 0 0.219vw 0.219vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.DropdownBtn > button) {
              background: ${WHITE_COLOR} !important;
              border: none;
              line-height: 0.5;
            }
            :global(.DropdownBtn > button:active),
            :global(.DropdownBtn > button:focus) {
              outline: none !important;
              background: none !important;
              box-shadow: none !important;
            }
            :global(.DropdownBtn > button:first-child) {
              padding-right: 0 !important;
              padding-left: 0 !important;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding-top: 0.292vw !important;
              padding-bottom: 0.292vw !important;
            }
            :global(.DropdownBtn > .dropdown-menu) {
              position: absolute;
              will-change: transform;
              right: 0;
              top: 100% !important;
              transform: unset !important;
              left: unset !important;
              padding: 0 !important;
              margin: 1.098vw 0 0 0;
              min-width: 11.713vw;
            }
            :global(.DropdownBtn > .dropdown-menu .dropdown-item) {
              width: 100%;
              cursor: pointer;
              padding: 0 !important;
            }
            :global(.DropdownBtn > .dropdown-menu .dropdown-item > div) {
              padding: 0.585vw 0.658vw !important;
            }
            .userAcc:hover .userName,
            .profileAcc:hover .userName {
              color: ${THEME_COLOR};
            }
            .profileAcc {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .firstSec {
              max-width: 1.976vw;
            }
            .userAcc:hover .dd_profileIcon {
              width: 100%;
              height: 1.903vw;
              background: url(${(this.props.userProfileData &&
                this.props.userProfileData.profilePicUrl) ||
              Profile_Logo});
              border-radius: 50%;
              background-repeat: no-repeat;
              background-size: cover;
            }
            :global(.DropdownBtn > .dropdown-menu > :nth-child(2)) {
              border-bottom: 0.0732vw solid ${BG_LightGREY_COLOR};
            }
            :global(.DropdownBtn > .dropdown-menu > :nth-last-child(1)) {
              border-top: 0.0732vw solid ${BG_LightGREY_COLOR};
            }
            .dd_profileIcon {
              width: 100%;
              height: 1.903vw;
              background: url(${(this.props.userProfileData &&
                this.props.userProfileData.profilePicUrl) ||
              Profile_Logo});
              border-radius: 50%;
              background-repeat: no-repeat;
              background-size: cover;
            }
            .profilePic {
              width: 100%;
              height: 1.903vw;
              background: url(${(this.props.userProfileData &&
                this.props.userProfileData.businessLogo) ||
              Boatzon_ProfilePic});
              border-radius: 50%;
              background-repeat: no-repeat;
              background-size: cover;
            }
            .userName {
              font-size: 0.805vw;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
            }
            .SellingLink_Sec:hover .sellingIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(3):hover
                .sellingIcon) {
              width: 100%;
              height: 0.805vw;
              background: url(${Selling_Blue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .BuyingLink_Sec:hover .buyingIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(4):hover
                .buyingIcon) {
              width: 100%;
              height: 0.805vw;
              background: url(${Buying_Blue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .publicProfileLink_Sec:hover .publicProfileIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(7):hover
                .publicProfileIcon) {
              width: 100%;
              height: 0.805vw;
              background: url(${PublicProfile_Blue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .folloingLink_Sec:hover .followingIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(5):hover
                .followingIcon) {
              width: 100%;
              height: 0.805vw;
              background: url(${Saved_Blue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .salesLink_Sec:hover .salesIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(6):hover
                .salesIcon) {
              width: 100%;
              height: 0.805vw;
              background: url(${Transaction_History_Blue});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .myAccLink_Sec:hover .myAccIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(8):hover
                .myAccIcon) {
              width: 100%;
              height: 0.805vw;
              background: url(${MyAcc_Blue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .logoutLink_Sec:hover .logoutIcon,
            :global(.DropdownBtn
                > .dropdown-menu
                :nth-child(9):hover
                .logoutIcon) {
              width: 100%;
              height: 0.732vw;
              background: url(${Logout_Blue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .ProfileLink_Sec:hover .linkName,
            :global(.DropdownBtn
                > .dropdown-menu
                > .dropdown-item:hover
                .linkName) {
              color: ${THEME_COLOR};
            }
            :global(.DropdownBtn > .dropdown-menu > .dropdown-item:hover) {
              background: ${THEME_COLOR_Opacity};
            }
            :global(.DropdownBtn > .dropdown-menu > .dropdown-item:focus),
            :global(.DropdownBtn > .dropdown-menu > .dropdown-item:active) {
              background: none !important;
              outline: none !important;
            }
            :global(.DropdownBtn > .dropdown-menu button:focus) {
              background: none !important;
              outline: none !important;
            }
            :global(.dropdown-item.active, .dropdown-item:active) {
              background: ${LIGHT_THEME_BG} !important;
            }
            .buyingIcon {
              width: 100%;
              height: 0.805vw;
              background: url(${Buying_Grey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .sellingIcon {
              width: 100%;
              height: 0.805vw;
              background: url(${Selling_Grey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .followingIcon {
              width: 100%;
              height: 0.805vw;
              background: url(${Saved_Grey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .salesIcon {
              width: 100%;
              height: 0.805vw;
              background: url(${Transaction_History_Grey});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .publicProfileIcon {
              width: 100%;
              height: 0.805vw;
              background: url(${PublicProfile_Grey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .myAccIcon {
              width: 100%;
              height: 0.805vw;
              background: url(${MyAcc_Grey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .logoutIcon {
              width: 100%;
              height: 0.732vw;
              background: url(${Logout_Grey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
            }
            .linkName {
              font-size: 0.805vw;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              margin: 0 !importnat;
              font-family: "Open Sans" !important;
            }
            .chevronDown {
              margin-top: 0.219vw;
              width: 0.732vw;
            }
            .ProfileSec {
              cursor: pointer;
              line-height: 0.5;
            }
            .ProfileSec span {
              font-size: 0.805vw;
              padding: 0 0.366vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .userType {
              font-size: 0.732vw;
              text-transform: capitalize;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.pricing_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              color: ${FONTGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.878vw;
            }
            :global(.pricing_btn button span) {
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.878vw;
            }
            :global(.pricing_btn button:focus),
            :global(.pricing_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.pricing_btn button:hover) {
              background: ${WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(withRouter(ProfessionalNavbar));
