import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { FONTGREY_COLOR } from "../../lib/config";

// Horizantal Scroll component
import Swiper from "swiper";
import { ProductsList } from "../../fixtures/grid-layout/grid-layout";
import CustomLink from "../../components/Link/Link";
import ProductCard from "../post-card/product-card";

class ProductSuggestions extends Component {
  state = {
    suggestionpage: false,
  };

  componentDidMount() {
    this.setState(
      {
        suggestionpage: true,
      },
      () => {
        this.sliderTranslate();
      }
    );
  }

  sliderTranslate = () => {
    const slider = document.querySelector(".swiper3");
    var swiper3 = new Swiper(slider, {
      slidesPerView: "auto",
      spaceBetween: 0,
      loop: false,
      freeMode: true,
      autoplay: 1,
      mousewheel: {
        releaseOnEdges: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
    swiper3.init();
  };

  render() {
    const { suggestionpage } = this.state;
    return (
      <Wrapper>
        {suggestionpage ? (
          <div className="col-12 p-0 position-relative SuggestionList_Sec py-3">
            <h6 className="heading">You may also like:</h6>
            <div className="swiper-container swiper3">
              <div className="swiper-wrapper">
                {ProductsList &&
                  ProductsList.map((data, index) => (
                    <div className="swiper-slide" key={index}>
                      <CustomLink href="/product-detail">
                        <a target="_blank">
                          <div className="productCard">
                            <ProductCard
                              Productdetails={data}
                              wishlist={true}
                              professionalpage={true}
                            />
                          </div>
                        </a>
                      </CustomLink>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .heading {
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0219vw !important;
              text-transform: initial;
              margin-bottom: 0;
              padding-bottom: 0.732vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .productCard {
              width: 10.98vw;
              height: 18.301vw;
              box-shadow: 0 0px 6px 0 rgba(48, 56, 97, 0.1);
            }
            :global(.SuggestionList_Sec .swiper-container),
            :global(.SuggestionList_Sec .swiper-wrapper) {
              z-index: 0 !important;
              height: 20.497vw;
            }
            :global(.SuggestionList_Sec .swiper-slide) {
              display: flex;
              flex-shrink: unset !important;
              height: auto;
              margin-right: 1.098vw !important;
              max-width: 24.158vw;
            }
            :global(.SuggestionList_Sec.swiper-button-next),
            :global(.SuggestionList_Sec .swiper-button-prev) {
              width: 0.951vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProductSuggestions;
