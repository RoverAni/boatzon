import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  GREY_VARIANT_1,
  THEME_COLOR,
  Save_White_Icon,
  BOX_SHADOW_GREY,
  Master_Card_DarkGrey_Icon,
  GREEN_COLOR,
  Shipping_Protection_Icon,
  Lock_Green_Icon,
  GREY_VARIANT_2,
  PowedBy_Icon,
} from "../../lib/config";
import Router from "next/router";
import InputBox from "../../components/input-box/input-box";
import { PureTextValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import StarRating from "../../components/rating/star-rating";
import { storeShippingAddress, getShippingCost, getAllShippingAddress, buyNow, getTransaction } from "../../services/contact";
import { getConnectedStripeAccount } from "../../services/transaction";
import { connect } from "react-redux";
import { getCookie } from "../../lib/session";
import { getCustomer } from "../../services/transaction";
import { makeOffer } from "../../services/product-details";
import Model from "../../components/model/centerModal";
import Snackbar from "../../components/snackbar";
import Select from "react-select";

class ReviewYourOffer extends Component {
  state = {
    inputpayload: {},
    getCustomerCards: [], // all cards connected to user account
    shippingCost: [], // all shipping methods for product to be shipped
    orderId: "", // orderID for product to be shipped recived from getShippingCost API
    modal: false,
    shippingMethod: "", // stores index
    shippingAddresses: [], // all shippo addresses
    options: [], // array of { value, label} extracted from [...shippingAddresses]
    selectedSingleAddress: "", // { value, label} for selected option
    revealAddress: "",
    isDefaultAddress: "", // stores default selected address (from shippo addresses)
    addCustomShippingAddress: false,
    addNewShippingAddress: false,
    open: false,
    variant: "success",
    usermessage: "",
    depositeAccLogin: false,
  };

  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  toggleModal = () => this.setState({ modal: !this.state.modal });

  selectShippingMethod = (index) => this.setState({ shippingMethod: index });

  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = this.state.inputpayload;
    tempsignUpPayload[[name]] = inputControl.value;
    this.setState({
      inputpayload: tempsignUpPayload,
    });
  };

  _getShippingCosts = () => {
    getShippingCost({ productId: this.props.productDetail && this.props.productDetail.postId, userId: getCookie("mqttId"), type: 0 })
      .then((res) => {
        this.setState({ shippingCost: res.data.data, orderId: res.data.orderId, shippingMethod: res.data.data.length > 0 ? 0 : "" });
        this._makeOffer();
      })
      .catch((err) => console.log("err getCustomer", err));
  };

  componentDidMount() {
    getCustomer()
      .then((res) => {
        this.setState({ getCustomerCards: res.data.data });
      })
      .catch((err) => console.log("err getCustomer", err));

    this._getShippingCosts();

    getAllShippingAddress(0, 20)
      .then((res) => {
        if (res.data.data && res.data.data.length > 0) {
          let options = [...this.state.options];
          let index = res.data.data.findIndex((k) => k.defaultAddress === true);

          res.data.data.map((k) => options.push({ value: k._id, label: k.street1 }));
          options.push({ value: "+Add New", label: "+Add New" });
          this.setState({
            shippingAddresses: res.data.data,
            options: options,
            revealAddress: res.data.data[index],
            selectedSingleAddress: { value: res.data.data[index]._id, label: res.data.data[index].street1 },
            isDefaultAddress: res.data.data[index],
            addCustomShippingAddress: index > -1 ? true : false,
          });
        } else {
          let options = [...this.state.options];
          options.push({ value: "+Add New", label: "+Add New" });
          this.setState({
            shippingAddresses: [],
            options: options,
            selectedSingleAddress: { value: "+Add New", label: "+Add New" },
            addCustomShippingAddress: false,
            open: true,
            usermessage: "No shipping addresses found.",
            variant: "warning",
          });
        }

        console.log("res getAllShippingAddress", res.data.data);
      })
      .catch((err) => {
        let options = [...this.state.options];
        options.push({ value: "+Add New", label: "+Add New" });
        this.setState({
          shippingAddresses: [],
          options: options,
          selectedSingleAddress: { value: "+Add New", label: "+Add New" },
          addCustomShippingAddress: false,
          open: true,
          usermessage: "No shipping addresses found.",
          variant: "warning",
        });
      });
    getConnectedStripeAccount(getCookie("mqttId"))
      .then((res) => {
        if (res.status === "200" && res.data.data === true) {
          this.setState({ depositeAccLogin: true });
        }
      })
      .catch((err) => this.setState({ depositeAccLogin: false }));
  }

  _makeOffer = async () => {
    let { price, memberFullName, postId, productName, mainUrl, memberMqttId, username, thumbnailImageUrl } =
      this.props && this.props.productDetail;
    let obj = {
      offerStatus: "4",
      postId: postId.toString(),
      price: price,
      type: "0",
      membername: memberFullName,
      sendchat: {
        name: username,
        from: getCookie("mqttId"),
        to: memberMqttId,
        payload: btoa(price),
        type: "15",
        offerType: "1",
        id: new Date().getTime().toString(),
        secretId: postId.toString(),
        thumbnail: thumbnailImageUrl,
        userImage: "",
        toDocId: "xxx",
        dataSize: 1,
        isSold: "0",
        productImage: mainUrl,
        productId: postId.toString(),
        productName: productName,
        productPrice: price,
      },
    };
    let res = await makeOffer(obj);
    console.log("[_makeOffer]", res);
  };

  _storeShippingAddress = async (isCustomShippingAddress) => {
    let { phoneNumber } = this.props.userProfileData;
    if (isCustomShippingAddress) {
      let { newStreetAddress, newSecondaryAddress, newCity, newState, newZipCode } = this.state.inputpayload;
      let obj = {
        userId: getCookie("mqttId"),
        countryCode: "US",
        city: newCity,
        postalCode: newZipCode,
        state: newState,
        address: newStreetAddress,
        defaultAddress: true,
        phone: phoneNumber,
        address1: newSecondaryAddress,
      };
      let res = await storeShippingAddress(obj, "");
      if (res != undefined) {
        let exAddresses = [...this.state.shippingAddresses];
        let options = [...this.state.options];
        let defaultAdrIndex = exAddresses.findIndex((k) => k.defaultAddress == true);
        exAddresses[defaultAdrIndex].defaultAddress = false;
        exAddresses.push({
          _id: res.data.data,
          city: obj.city,
          country: "US",
          defaultAddress: true,
          houseName: "",
          name: "mark",
          phone: 15553419393,
          state: obj.state,
          street1: obj.address,
          address1: obj.address1,
          street2: obj.address1,
          userId: getCookie("mqttId"),
          zip: obj.postalCode,
        });
        options.push({ value: res.data.data, label: obj.address });
        // if (exAddresses.length === 1) {
        this._getShippingCosts();
        // }
        this.setState({
          addCustomShippingAddress: true,
          shippingAddresses: exAddresses,
          options: options,
          selectedSingleAddress: { value: res.data.data, label: obj.address },
          usermessage: "New shipping address added.",
          variant: "success",
          open: true,
        });
      }
    } else {
      let { zip_code, state, city, address1, address2 } = this.state.inputpayload;
      let obj = {
        userId: getCookie("mqttId"),
        countryCode: "US",
        city: city,
        postalCode: zip_code,
        state: state,
        address: address1,
        address1: address2,
        defaultAddress: this.state.shippingAddresses.length === 0 ? true : false,
        phone: phoneNumber,
      };
      let res = await storeShippingAddress(obj, "");
      if (res != undefined) {
        let exAddresses = [...this.state.shippingAddresses];
        let options = [...this.state.options];
        exAddresses.push({
          _id: res.data.data,
          city: city,
          country: "US",
          defaultAddress: this.state.shippingAddresses.length === 0 ? true : false,
          houseName: "",
          name: "mark",
          phone: 15553419393,
          state: obj.state,
          street1: obj.address,
          street2: obj.address1,
          userId: getCookie("mqttId"),
          zip: obj.postalCode,
        });
        options.push({ value: res.data.data, label: obj.address1 });
        if (exAddresses.length === 1) {
          this._getShippingCosts();
        }
        this.setState({
          addNewShippingAddress: false,
          shippingAddresses: exAddresses,
          options: options,
          selectedSingleAddress: { value: res.data.data, label: address1 },
          usermessage: "New shipping address added.",
          variant: "success",
          open: true,
        });
      }
    }
  };

  _buyProductNow = async () => {
    this._getTransaction();
  };

  _getTransaction = async () => {
    if (this.state.shippingAddresses.length === 0) {
      this.setState({ open: true, usermessage: "Add shipping address before confirming offer", variant: "error" });
      return;
    } else if (this.state.getCustomerCards.length === 0) {
      this.setState({ open: true, usermessage: "Please add a card, for making transaction", variant: "error" });
      return;
    } else if (!this.state.depositeAccLogin) {
      this.setState({ open: true, usermessage: "Please add deposit accounts in My Accounts and try again", variant: "error" });
      return;
    } else if (this.state.depositeAccLogin && this.state.getCustomerCards.length > 0 && this.state.shippingAddresses.length > 0) {
      let productObj = {
        type: 0,
        userId: getCookie("mqttId"),
        toUserId: this.props.productDetail.memberMqttId,
        amount: this.props.productDetail.price.toString(),
        shippingCost: this.state.shippingCost[this.state.shippingMethod].amount,
        currency: "USD",
        // paymentMethod: "pm_1IZI5UK0rqExvmZhnzdpowL5",
        paymentMethod: this.state.getCustomerCards[0].id,
      };
      let response1 = await buyNow(productObj);
      console.log("_buyProductNow", response1);

      let { provider, object_id } = this.state.shippingCost[0];
      let { postId, memberMqttId, memberFullName } = this.props.productDetail;
      let obj = {
        productId: postId,
        buyerId: getCookie("mqttId"),
        rateId: object_id,
        providerName: provider,
        orderId: this.state.orderId,
        sellerId: memberMqttId,
      };
      let response = await getTransaction(obj);
      console.log("response", response);
      // let res = await markAsSold({
      //   postId: postId,
      //   type: "0",
      //   flag: "1",
      //   ratings: "5",
      //   trackingNumber: object_id,
      //   membername: memberFullName,
      // });
      // if (res != undefined) {
      //   this.setState({ open: true, usermessage: "Product sold", variant: "success" });
      // }
      // console.log("res", res);
    }
  };

  /** select dropdown handler, only one address inputs form will be active. */
  handleSelectChange = (e) => {
    let index = this.state.shippingAddresses.findIndex((k) => k._id === e.value);
    if (index > -1) {
      this.setState({ selectedSingleAddress: e, revealAddress: this.state.shippingAddresses[index], addNewShippingAddress: false });
    } else {
      if (this.state.addCustomShippingAddress) {
        this.setState({ addNewShippingAddress: true, selectedSingleAddress: e });
      }
    }
  };

  /** toggle checkbox, for toggling to add custom shipping address */
  checkboxHandler = (e) => {
    if (!this.state.addNewShippingAddress) {
      if (!e.target.checked || e.target.checked) {
        this.setState({ addCustomShippingAddress: !this.state.addCustomShippingAddress });
      }
    }
  };

  render() {
    let { productDetail } = this.props;
    return (
      <Wrapper>
        <div className="reviewYourOffer_Page">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 py-5">
              <div className="col-7 p-0 mx-auto">
                <h6 className="heading">Checkout</h6>
                <div className="row m-0">
                  <div className="col-6 p-0">
                    <div className="ShippingAddress_Sec">
                      <h6 className="secTitle p-0">Billing Address</h6>
                      <div className="row m-0" style={{ justifyContent: "space-between" }}>
                        <div className="p-0" style={{ width: "47.5%" }}>
                          <div className="FormInput overRide-basicInfo">
                            <label>First Name:</label>
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              placeholder="John"
                              name="firstName"
                              onChange={this.handleOnchangeInput(`firstName`)}
                              onKeyPress={PureTextValidator}
                              autoComplete="off"
                            ></InputBox>
                          </div>
                        </div>
                        <div className="p-0" style={{ width: "47.5%" }}>
                          <div className="FormInput overRide-basicInfo">
                            <label>Last Name:</label>
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              placeholder="Doe"
                              name="lastName"
                              onChange={this.handleOnchangeInput(`lastName`)}
                              onKeyPress={PureTextValidator}
                              autoComplete="off"
                            ></InputBox>
                          </div>
                        </div>
                      </div>
                      <div className="FormInput overRide-basicInfo">
                        <label>Address:</label>
                        <Select
                          className="shippo-address"
                          value={this.state.selectedSingleAddress}
                          onChange={this.handleSelectChange}
                          options={this.state.options}
                        />
                        {this.state.addNewShippingAddress ? (
                          <section>
                            <div className="FormInput">
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                placeholder="Street address"
                                name="address1"
                                onChange={this.handleOnchangeInput(`address1`)}
                                autoComplete="off"
                              ></InputBox>
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                placeholder="Street address 2 (Optional)"
                                name="address2"
                                onChange={this.handleOnchangeInput(`address2`)}
                                autoComplete="off"
                              ></InputBox>
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                placeholder="City"
                                name="city"
                                onChange={this.handleOnchangeInput(`city`)}
                                autoComplete="off"
                              ></InputBox>
                              <div className="row m-0">
                                <div className="col-8 p-0">
                                  <InputBox
                                    type="text"
                                    className="inputBox form-control"
                                    placeholder="State"
                                    name="state"
                                    onChange={this.handleOnchangeInput(`state`)}
                                    autoComplete="off"
                                  ></InputBox>
                                </div>
                                <div className="col-4 pr-0">
                                  <InputBox
                                    type="text"
                                    className="inputBox form-control"
                                    placeholder="Zip Code"
                                    name="zip_code"
                                    onChange={this.handleOnchangeInput(`zip_code`)}
                                    autoComplete="off"
                                  ></InputBox>
                                </div>
                              </div>
                              <div className="row m-0">
                                <div className="col-8 p-0 save_btn">
                                  <ButtonComp onClick={() => this._storeShippingAddress(false)}>
                                    <img src={Save_White_Icon} className="saveIcon"></img>
                                    Save
                                  </ButtonComp>
                                </div>
                              </div>
                            </div>
                          </section>
                        ) : (
                          <div className="row mx-0 reveal_address align-items-end">
                            <div className="col-8 px-0 selected_address">
                              {this.state.revealAddress && this.state.revealAddress.street1}{" "}
                              {this.state.revealAddress && this.state.revealAddress.street2}{" "}
                              {this.state.revealAddress && this.state.revealAddress.city}{" "}
                              {this.state.revealAddress && this.state.revealAddress.state}{" "}
                              {this.state.revealAddress && this.state.revealAddress.zip}{" "}
                              {this.state.revealAddress && this.state.revealAddress.country}
                            </div>
                            <div className="col-4 px-0 text-right change_address">Change ?</div>
                          </div>
                        )}

                        <div className="select_address_checkbox">
                          <input
                            type="checkbox"
                            // value={this.state.addCustomShippingAddress}
                            checked={this.state.addCustomShippingAddress}
                            onChange={this.checkboxHandler}
                          />{" "}
                          <label>Ship to Address</label>
                        </div>
                        {!this.state.addCustomShippingAddress ? (
                          <section className="newAddressForm">
                            <div className="FormInput">
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                placeholder="Street Address"
                                name="newStreetAddress"
                                onChange={this.handleOnchangeInput(`newStreetAddress`)}
                                onKeyPress={PureTextValidator}
                                autoComplete="off"
                              ></InputBox>
                            </div>
                            <div className="FormInput">
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                placeholder="Street Address 2(Optional)"
                                name="newSecondaryAddress"
                                onChange={this.handleOnchangeInput(`newSecondaryAddress`)}
                                onKeyPress={PureTextValidator}
                                autoComplete="off"
                              ></InputBox>
                            </div>
                            <div className="FormInput">
                              <InputBox
                                type="text"
                                className="inputBox form-control"
                                placeholder="City"
                                name="newCity"
                                onChange={this.handleOnchangeInput(`newCity`)}
                                onKeyPress={PureTextValidator}
                                autoComplete="off"
                              ></InputBox>
                            </div>
                            <div className="row m-0 justify-content-between FormInput overRide-basicInfo">
                              <div className="newState">
                                <InputBox
                                  type="text"
                                  className="inputBox form-control"
                                  placeholder="State"
                                  name="newState"
                                  onChange={this.handleOnchangeInput(`newState`)}
                                  onKeyPress={PureTextValidator}
                                  autoComplete="off"
                                ></InputBox>
                              </div>
                              <div className="newZip">
                                <InputBox
                                  type="text"
                                  className="inputBox form-control"
                                  placeholder="ZIP Code"
                                  name="newZipCode"
                                  onChange={this.handleOnchangeInput(`newZipCode`)}
                                  onKeyPress={PureTextValidator}
                                  autoComplete="off"
                                ></InputBox>
                              </div>
                            </div>
                            <div className="save_btn">
                              <ButtonComp onClick={() => this._storeShippingAddress(true)}>
                                <img src={Save_White_Icon} className="saveIcon"></img>
                                Save
                              </ButtonComp>
                            </div>
                          </section>
                        ) : (
                          ""
                        )}
                        <div className="select_method">
                          <h6>Select Method</h6>
                          {this.state.shippingCost &&
                            this.state.shippingCost.map((k, i) => (
                              <div className="col-12 px-0 shipOffer-single" key={i}>
                                <div className="row mx-0">
                                  <div className="shipOffer-radio" style={{ width: "10%" }}>
                                    <input
                                      type="radio"
                                      onClick={() => this.selectShippingMethod(i)}
                                      checked={i === this.state.shippingMethod}
                                    />
                                  </div>
                                  <div className="shipOffer-info">
                                    <div className="shipOffer-header">{k && k.servicelevel && k.servicelevel.name}</div>
                                    <div className="shipOffer-duration">{k && k.duration_terms}</div>
                                  </div>
                                  <div className="shipOffer-cost">${k.amount_local}</div>
                                </div>
                              </div>
                            ))}
                        </div>
                      </div>
                    </div>
                    {this.state.getCustomerCards && this.state.getCustomerCards.length > 0 ? (
                      <div className="PaymentMethod_Sec mt-4">
                        <h6 className="secTitle">Payment Method</h6>
                        <div className="d-flex cardDetails">
                          <img src={Master_Card_DarkGrey_Icon}></img>
                          <p>
                            {this.state.getCustomerCards[0] && this.state.getCustomerCards[0].brand}{" "}
                            <div className="mx-2 d-flex align-items-center">
                              <div></div>
                              <div></div>
                              <div></div>
                              <div></div>
                            </div>{" "}
                            {this.state.getCustomerCards[0] && this.state.getCustomerCards[0].last4}
                          </p>
                        </div>
                        <div className="change_btn">
                          <ButtonComp onClick={() => Router.push("/my-account/payment-methods")}>Change</ButtonComp>
                        </div>
                      </div>
                    ) : (
                      <div className="PaymentMethod_Sec mt-4">
                        <h6 className="secTitle">Payment Method</h6>
                        <div className="d-flex cardDetails">
                          <img src={Master_Card_DarkGrey_Icon}></img>
                          <p>
                            Please Add Card
                            {/* Master Card{" "}
                            <div className="mx-2 d-flex align-items-center">
                              <div></div>
                              <div></div>
                              <div></div>
                              <div></div>
                            </div>{" "}
                            5992 */}
                          </p>
                        </div>
                        <div className="change_btn">
                          <ButtonComp onClick={() => Router.push("/my-account/payment-methods")}>Add</ButtonComp>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="col-6 pr-0">
                    <div className="ProductInfo_Sec">
                      <div className="row m-0 d-flex section">
                        <div className="col-4 p-0">
                          <img src={productDetail && productDetail.mainUrl} className="productImg"></img>
                        </div>
                        <div className="col-8 pr-0">
                          <h6 className="productName border-0">{productDetail && productDetail.productName}</h6>
                        </div>
                      </div>
                      <div className="section">
                        <table className="paymentInfo_Sec">
                          <tr>
                            <td>Item price:</td>
                            <td>
                              <ButtonComp>Edit</ButtonComp>${productDetail && productDetail.price}
                            </td>
                          </tr>
                          <tr>
                            <td>Shipping:</td>
                            <td>
                              $
                              {this.state.shippingMethod > -1
                                ? parseFloat(
                                    this.state.shippingCost &&
                                      this.state.shippingCost[this.state.shippingMethod] &&
                                      this.state.shippingCost[this.state.shippingMethod].amount_local
                                  )
                                : 0.0}
                            </td>
                          </tr>
                          <tr>
                            <td>Sales tax (estimated):</td>
                            <td>$0.00</td>
                          </tr>
                          <tr>
                            <td id="totalLabel">You Pay:</td>
                            <td id="totalPrice">
                              $
                              {this.state.shippingMethod > -1
                                ? parseFloat(productDetail && productDetail.price) +
                                  parseFloat(
                                    this.state.shippingCost &&
                                      this.state.shippingCost[this.state.shippingMethod] &&
                                      this.state.shippingCost[this.state.shippingMethod].amount_local
                                  )
                                : productDetail && productDetail.price}
                            </td>
                          </tr>
                        </table>
                      </div>
                      <div className="section">
                        <div className="row m-0 py-2">
                          <div className="col-auto pl-0 pr-md-1">
                            <img src={productDetail && productDetail.memberProfilePicUrl} className="sellerProfilePic"></img>
                          </div>
                          <div className="col-5 p-0">
                            <h6 className="sellerName">{productDetail && productDetail.membername}</h6>
                            <div className="d-flex align-items-center">
                              <StarRating value={4} disableHover={true} readOnly={true} /> <span className="commentNum">(8)</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="section">
                        <div className="row m-0 py-2">
                          <div className="col-auto pl-0 pr-md-1">
                            <img src={Shipping_Protection_Icon} className="shippingProtectionImg"></img>
                          </div>
                          <div className="col p-0">
                            <h6 className="shippingProtectionTitle">Shipping purchase protection</h6>
                            <p className="shippingProtectionDesc">Items shipped through Boatzon come with a free mone-back guarantee.</p>
                            <a href="#" className="shippingLink">
                              Learn more
                            </a>
                          </div>
                        </div>
                      </div>
                      <div>
                        <p className="policyDesc">
                          By accepting this offer, you agree to follow our <a>shipping policy</a>, including applicable USPS shipping
                          restriction and accept the Boatzon <a>Terms of Service</a>
                        </p>
                        <div className="confirm_btn" onClick={this._buyProductNow}>
                          <ButtonComp>Confirm Offer</ButtonComp>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="d-flex align-items-center pt-3">
                  <img src={Lock_Green_Icon} className="lockIcon"></img>
                  <img src={PowedBy_Icon} className="poweredByIcon"></img>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Model isOpen={this.state.modal} toggle={this.toggleModal} width={"45vw"}>
          <section className="p-4">
            <div className="pb-3 heading text-center">Select Shipping Method</div>
            {this.state.shippingCost &&
              this.state.shippingCost.map((k, i) => (
                <div
                  className={
                    this.state.shippingMethod === i ? "row py-2 align-items-center selected_method" : "row py-2 align-items-center"
                  }
                  key={i}
                  onClick={() => this.selectShippingMethod(i)}
                >
                  <div className="modal_info_d1">
                    <img
                      src={k.provider_image_75}
                      alt={k.duration_terms}
                      height={50}
                      width={50}
                      style={{ objectFit: "contain", borderRadius: "4px" }}
                    />
                  </div>
                  <div className="modal_info_d2">
                    <div className="heading p-0">
                      {k.currency} {k.amount_local}{" "}
                    </div>
                    <div className="policyDesc pt-1 pb-0">{k.duration_terms}</div>
                  </div>
                </div>
              ))}
            <div className="row">
              <div className="cancel_btn col-6" onClick={this.toggleModal}>
                <ButtonComp>Cancel</ButtonComp>
              </div>
              <div className="confirm_btn col-6" onClick={this.toggleModal}>
                <ButtonComp>Continue</ButtonComp>
              </div>
            </div>
          </section>
        </Model>
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          // vertical={this.state.vertical}
          // horizontal={this.state.horizontal}
        />
        <style jsx>
          {`
            :global(.css-1uccc91-singleValue) {
              color: #717c99;
              font-size: 0.72916vw;
              font-family: Open Sans;
              font-weight: 600;
            }

            :global(.newAddressForm) {
              padding-bottom: 1.51vw;
              border-bottom: ${!this.state.addCustomShippingAddress ? "1px solid #d3d9e7" : "0"};
            }
            :global(.css-1okebmr-indicatorSeparator) {
              width: 0px !important;
            }
            :global(.css-6q0nyr-Svg) {
              transform: rotateZ(90deg) !important;
            }
            :global(.newState) {
              width: 61%;
            }
            :global(.newZip) {
              width: 35%;
            }
            :global(.shipOffer-single) {
              padding-bottom: 0.9375vw !important;
            }
            :global(.shipOffer-header) {
              color: #4b5777;
              font-weight: 600;
              font-size: 0.833vw;
              font-family: Open Sans;
            }
            :global(.shipOffer-radio) {
              width: 10% !important;
            }
            :global(.shipOffer-info) {
              width: 70%;
            }
            :global(.shipOffer-duration) {
              font-size: 0.625vw;
              font-family: Open Sans;
              color: #4b5777;
              font-weight: 400;
            }
            :global(.shipOffer-cost) {
              font-size: 0.72916vw;
              font-family: Open Sans;
              font-weight: 400;
              color: #4b5777;
              width: 20%;
              text-align: right;
            }
            :global(.select_address_checkbox) {
              display: flex;
              align-items: center;
              padding-top: 1.25vw;
              padding-bottom: ${!this.state.addCustomShippingAddress ? "0.26041vw" : "1.0416vw"} !important;
              border-bottom: ${this.state.addCustomShippingAddress ? "1px solid #d3d9e7" : "0"};
            }
            :global(.select_method h6) {
              color: #4b5777;
              font-size: 0.9375vw !important;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              margin-bottom: 1.19791vw !important;
            }
            :global(.select_method) {
              padding-top: 1.0416vw;
            }
            :global(.select_address_checkbox label) {
              font-size: 0.833vw !important;
              margin-left: 0.46875vw !important;
            }
            :global(.selected_address) {
              color: #c0c7d9;
              font-size: 0.72916vw;
              font-family: Open Sans;
              font-weight: 600;
            }
            :global(.reveal_address) {
              padding-top: 0.72916vw;
            }
            :global(.change_address) {
              color: #378bcb;
              font-size: 0.72916vw;
              font-family: Open Sans;
              font-weight: 400;
            }
            :global(.shippo-address) {
              padding-top: 0.585vw !important;
              padding-bottom: ${this.state.addNewShippingAddress ? "17px" : " 0 !important"};
            }
            :global(.shippo-address .css-yk16xz-control) {
              border: 0.0732vw solid #d3d9e7 !important;
              padding: 0.203vw;
              // padding-top: 0.3125vw;
            }
            :global(.selected_method) {
              border: 1px solid #27c397;
              border-radius: 4px;
            }
            :global(.modal_info_d1) {
              width: 80px;
              text-align: center;
            }
            :global(.modal_info_d2) {
              width: calc(45vw - 100px);
            }
            .poweredByIcon {
              width: 8.052vw;
            }
            .lockIcon {
              width: 0.878vw;
              margin-right: 0.366vw;
            }
            .change_btn {
              position: absolute;
              top: 0.732vw;
              right: 1.317vw;
            }
            :global(.change_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0;
              padding: 0;
              text-transform: initial;
              background: none;
              color: ${GREEN_COLOR};
              position: relative;
            }
            :global(.change_btn button:hover) {
              background: none;
            }
            :global(.change_btn button:focus),
            :global(.change_btn button:active) {
              background: none;
              outline: none;
            }
            :global(.change_btn button span) {
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
            }
            :global(.confirm_btn button) {
              width: 100%;
              margin: 0.439vw 0 0 0;
              padding: 0.366vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.confirm_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.confirm_btn button:focus),
            :global(.confirm_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.confirm_btn button span) {
              font-size: 1.024vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }

            :global(.cancel_btn button) {
              width: 100%;
              margin: 0.439vw 0 0 0;
              padding: 0.366vw 0;
              text-transform: initial;
              background: #378bcb;
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.cancel_btn button:hover) {
              background: #378bcb;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: #378bcb;
              outline: none;
            }
            :global(.cancel_btn button span) {
              font-size: 1.024vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }

            .policyDesc {
              font-size: 0.805vw;
              margin: 0;
              padding: 0.585vw 0;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .policyDesc a {
              color: ${GREEN_COLOR};
              text-decoration: none;
            }
            .shippingLink {
              color: ${GREEN_COLOR};
              font-size: 0.732vw;
              text-decoration: none;
              line-height: 1;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .shippingProtectionDesc {
              font-size: 0.732vw;
              margin: 0;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .sellerName,
            .shippingProtectionTitle {
              padding-top: 0.366vw;
              font-size: 1.024vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 0.219vw;
              font-family: "Open Sans-Semibold" !important;
            }
            .commentNum {
              font-size: 0.732vw;
              margin: 0 0.366vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            .paymentInfo_Sec {
              width: 100%;
              margin: 0.732vw 0;
            }
            .paymentInfo_Sec tr td {
              font-size: 0.878vw;
              margin: 0;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .paymentInfo_Sec tr td:last-child {
              text-align: right;
            }
            :global(.paymentInfo_Sec tr:first-child td button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0 0.366vw;
              padding: 0;
              text-transform: initial;
              background: none;
              color: ${GREEN_COLOR};
              position: relative;
            }
            :global(.paymentInfo_Sec tr:first-child td button:hover) {
              background: none;
            }
            :global(.paymentInfo_Sec tr:first-child td button:focus),
            :global(.paymentInfo_Sec tr:first-child td button:active) {
              background: none;
              outline: none;
            }
            :global(.paymentInfo_Sec tr:first-child td button span) {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
            }
            .paymentInfo_Sec tr:last-child td {
              padding: 0.366vw 0 0 0;
            }
            #totalLabel,
            #totalPrice {
              font-size: 1.098vw !important;
              font-family: "Open Sans-SemiBold" !important;
            }
            .sellerProfilePic,
            .shippingProtectionImg {
              width: 3.294vw !important;
              object-fit: cover;
              margin-right: 0.366vw;
            }
            .section {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .productImg {
              width: 100%;
              height: 80%;
              border-radius: 0.146vw;
              object-fit: cover;
            }
            .reviewYourOffer_Page {
              background: ${BG_LightGREY_COLOR};
            }
            .ProductInfo_Sec {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.219vw;
              padding: 1.098vw;
            }
            .ShippingAddress_Sec,
            .PaymentMethod_Sec {
              background: ${WHITE_COLOR};
              padding: 1.317vw;
              position: relative;
              border-radius: 0.219vw;
              box-shadow: 0px 0px 1.5px 0px ${BOX_SHADOW_GREY} !important;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0.732vw 0;
            }
            .secTitle,
            .productName {
              font-size: 1.024vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0.732vw 0;
              // border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .FormInput {
              margin: 1.098vw 0 0 0;
            }
            .overRide-basicInfo {
              margin: 0.78125vw 0 0 0 !important;
            }
            .ShippingAddress_Sec .FormInput label {
              font-size: 0.878vw;
              margin: 0;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }

            :global(.ShippingAddress_Sec .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.878vw;
              margin: 0.585vw 0 0 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.ShippingAddress_Sec .FormInput .inputBox:focus) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
            }
            .saveIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.951vw;
            }
            :global(.save_btn button) {
              width: 100%;
              margin: 1.25vw 0 0 0;
              text-transform: initial;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.save_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
            }
            :global(.save_btn button span) {
              font-size: 0.951vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .cardDetails {
              padding: 0.585vw 0;
            }
            .cardDetails img {
              width: 1.098vw;
              margin-right: 0.585vw;
            }
            .cardDetails p div div {
              width: 0.329vw;
              height: 0.329vw;
              margin: 0 0.219vw;
              border-radius: 50%;
              background: ${FONTGREY_COLOR};
            }
            .cardDetails p {
              display: flex;
              align-items: center;
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
              margin: 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps, null)(ReviewYourOffer);
