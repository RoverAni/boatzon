import React, { Component } from "react";
import {
  Close_Icon,
  Make_Offer_Grey_Icon,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR_2,
  GREEN_COLOR,
  Shipping_White_Icon,
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
} from "../../lib/config";
import Router from "next/router";
import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import { makeOffer } from "../../services/product-details";
import { connect } from "react-redux";
import { getCookie } from "../../lib/session";

class MakeOfferModel extends Component {
  state = {
    inputpayload: {},
  };

  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = this.state.inputpayload;
    tempsignUpPayload[[name]] = inputControl.value;
    this.setState({
      inputpayload: tempsignUpPayload,
    });
  };

  /** this for boat make offer */
  makeOffer2 = () => {
    let { fullName, profilePicUrl } = this.props.userProfileData;
    let {
      memberMqttId,
      postId,
      membername,
      mainUrl,
      productName,
    } = this.props.boatDetail;

    let obj = {
      offerStatus: "1",
      postId: postId.toString(),
      price: parseInt(this.state.inputpayload.offer_price),
      type: "0",
      membername: membername,
      sendchat: {
        name: fullName,
        from: getCookie("mqttId"),
        to: memberMqttId,
        payload: btoa(this.state.inputpayload.offer_price),
        type: "15",
        offerType: "1",
        id: new Date().getTime().toString(),
        secretId: postId.toString(),
        thumbnail: "",
        userImage: profilePicUrl,
        toDocId: "xyz",
        dataSize: 1,
        isSold: "0",
        productImage: mainUrl,
        productId: postId,
        productName: productName,
        productPrice: `$${btoa(this.state.inputpayload.offer_price)}`,
      },
    };
    console.log("obj", obj);
    makeOffer(obj)
      .then((res) => {
        console.log("success res", res);
        this.props.onClose();
        setTimeout(() => {
          Router.push("/messenger");
        }, 500);
      })
      .catch((err) => console.log("err", err));
  };

  /** this for product make offer */
  makeOffer1 = () => {
    let { fullName, profilePicUrl } = this.props.userProfileData;
    let {
      memberMqttId,
      postId,
      membername,
      mainUrl,
      productName,
    } = this.props.productDetail;

    let obj = {
      offerStatus: "1",
      postId: postId.toString(),
      price: parseInt(this.state.inputpayload.offer_price),
      type: "0",
      membername: membername,
      sendchat: {
        name: fullName,
        from: getCookie("mqttId"),
        to: memberMqttId,
        payload: btoa(this.state.inputpayload.offer_price),
        type: "15",
        offerType: "1",
        id: new Date().getTime().toString(),
        secretId: postId.toString(),
        thumbnail: "",
        userImage: profilePicUrl,
        toDocId: "xyz",
        dataSize: 1,
        isSold: "0",
        productImage: mainUrl,
        productId: postId,
        productName: productName,
        productPrice: `$${btoa(this.state.inputpayload.offer_price)}`,
      },
    };
    console.log("obj", obj);
    makeOffer(obj)
      .then((res) => {
        console.log("success res", res);
        this.props.onClose();
        setTimeout(() => {
          Router.push("/messenger");
        }, 500);
      })
      .catch((err) => console.log("err", err));
  };

  render() {
    // console.log("XXX", this.props.productDetail);
    // console.log("inputpayload", this.state.inputpayload);
    return (
      <Wrapper>
        <div className="col-12 MakeOfferModel text-center">
          <div className="px-3">
            <img
              src={Close_Icon}
              className="closeIcon"
              onClick={this.props.onClose}
            ></img>
            <h5 className="heading">
              <img src={Make_Offer_Grey_Icon}></img> Enter your offer
            </h5>
            <p className="productName">
              Galvanized-Manganese Delta Fast-Set Anchor
            </p>
            <div className="FormInput">
              <InputBox
                type="text"
                className="inputBox form-control"
                placeholder={`$${
                  this.props.productDetail
                    ? this.props.productDetail.price
                    : this.props.boatDetail.price
                }`}
                name="offer_price"
                onChange={this.handleOnchangeInput(`offer_price`)}
                autoComplete="off"
              ></InputBox>
            </div>
            <div className="shipToMe_btn">
              {/* <ButtonComp onClick={this.props.handleReviewOfferPage}> */}
              <ButtonComp
                onClick={
                  this.props.productDetail ? this.makeOffer1 : this.makeOffer2
                }
              >
                <img src={Shipping_White_Icon} className="shippingIcon"></img>
                Ship to me
              </ButtonComp>
            </div>
          </div>
          <p className="shippingFee">+$29.99 shipping fee</p>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .MakeOfferModel {
              width: 25.622vw;
              padding: 1.098vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .productName {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              line-height: 1;
            }
            .shippingFee {
              padding: 0.732vw 0 0 0;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              line-height: 1;
            }
            .heading {
              margin: 1.464vw 0 0.366vw 0;
              font-size: 1.405vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .heading img {
              width: 1.317vw;
              margin-right: 0.585vw;
              margin-bottom: 0.146vw;
            }
            .MakeOfferModel .FormInput {
              margin: 1.098vw 0;
            }
            :global(.MakeOfferModel .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: auto;
              padding: 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 2.196vw;
              text-align: center;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.MakeOfferModel .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR_Dark};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 2.196vw;
              text-align: center;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .shippingIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.317vw;
            }
            :global(.shipToMe_btn button) {
              width: 100%;
              margin: 0.366vw 0;
              padding: 0.366vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.shipToMe_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.shipToMe_btn button:focus),
            :global(.shipToMe_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.shipToMe_btn button span) {
              font-size: 1.098vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps, null)(MakeOfferModel);
