import React, { Component } from "react";
import {
  Close_Icon,
  Make_Offer_Grey_Icon,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR_2,
  GREEN_COLOR,
  Shipping_White_Icon,
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
} from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import { makeOffer } from "../../services/product-details";
import { connect } from "react-redux";
import { getCookie } from "../../lib/session";
import { userSelectedToChat } from "../../redux/actions/chat/chat";

class MakeOfferChatModel extends Component {
  state = {
    inputpayload: "",
  };

  componentDidMount() {
    this.setState({
      inputpayload: parseInt(atob(this.props.chatDetails.payload)),
    });
  }

  makeOffer1 = () => {
    let { profilePicUrl } = this.props.userProfileData;
    let existingUserToChat = { ...this.props.userProfileData };
    let {
      productId,
      recipientId,
      senderId,
      productImage,
      productName,
      userName,
    } = this.props.chatDetails;
    let { to, payload } = this.props.userSelectedToChat;
    let newPrice = this.state.inputpayload;
    let obj = {
      offerStatus: "3",
      postId: productId,
      price: newPrice,
      type: "0",
      membername: "niko",
      buyer: userName,
      sendchat: {
        name: "niko",
        from: getCookie("mqttId"),
        to: recipientId || senderId || to,
        payload: btoa(this.state.inputpayload),
        type: "15",
        offerType: "3",
        id: new Date().getTime().toString(),
        secretId: productId,
        thumbnail: "",
        userImage: profilePicUrl,
        toDocId: "xxx",
        dataSize: 1,
        isSold: "0",
        productImage: productImage,
        productId: productId,
        productName: productName,
        productPrice: `${atob(payload)}`,
      },
    };
    makeOffer(obj)
      .then((res) => {
        console.log("offer countered", res);
        existingUserToChat["offerType"] = "3";
        this.props._funcUserSelectedToChat(existingUserToChat);
        this.props.onClose();
      })
      .catch((err) => console.log("err", err));
  };

  render() {
    let { productName } = this.props && this.props.chatDetails;
    console.log("chatDetails", this.props.chatDetails);
    return (
      <Wrapper>
        <div className="col-12 MakeOfferModel text-center">
          <div className="px-3">
            <img
              src={Close_Icon}
              className="closeIcon"
              onClick={this.props.onClose}
            ></img>
            <h5 className="heading">
              <img src={Make_Offer_Grey_Icon}></img> Enter amount to counter
              offer
            </h5>
            <p className="productName">{productName}</p>
            <div className="FormInput">
              <InputBox
                type="text"
                className="inputBox form-control"
                value={this.state.inputpayload}
                placeholder={`$${this.state.inputpayload}`}
                name="offer_price"
                onChange={(e) =>
                  this.setState({ inputpayload: e.target.value })
                }
                autoComplete="off"
              ></InputBox>
            </div>
            <div className="shipToMe_btn">
              {/* <ButtonComp onClick={this.props.handleReviewOfferPage}> */}
              <ButtonComp onClick={this.makeOffer1}>
                <img src={Shipping_White_Icon} className="shippingIcon"></img>
                Counter Offer
              </ButtonComp>
            </div>
          </div>
          {/* <p className="shippingFee">+$29.99 shipping fee</p> */}
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .MakeOfferModel {
              width: 25.622vw;
              padding: 1.098vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .productName {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              line-height: 1;
            }
            .shippingFee {
              padding: 0.732vw 0 0 0;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              line-height: 1;
            }
            .heading {
              margin: 1.464vw 0 0.366vw 0;
              font-size: 1.405vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .heading img {
              width: 1.317vw;
              margin-right: 0.585vw;
              margin-bottom: 0.146vw;
            }
            .MakeOfferModel .FormInput {
              margin: 1.098vw 0;
            }
            :global(.MakeOfferModel .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: auto;
              padding: 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 2.196vw;
              text-align: center;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.MakeOfferModel .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR_Dark};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 2.196vw;
              text-align: center;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .shippingIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.317vw;
            }
            :global(.shipToMe_btn button) {
              width: 100%;
              margin: 0.366vw 0;
              padding: 0.366vw 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.shipToMe_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.shipToMe_btn button:focus),
            :global(.shipToMe_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.shipToMe_btn button span) {
              font-size: 1.098vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
    userSelectedToChat: state.userSelectedToChat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _funcUserSelectedToChat: (obj) => dispatch(userSelectedToChat(obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MakeOfferChatModel);
