import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_1,
  GREEN_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  BOX_SHADOW_GREY,
  GREY_VARIANT_3,
  GREY_VARIANT_11,
  Make_Offer_White_Icon,
  THEME_COLOR,
  Dark_Blue_Color,
  Video_Grey_Icon,
  Chat_White_Icon,
  Orange_Color,
  GREY_VARIANT_9,
  Inventory_Grey_Icon,
  Follow_Grey_Icon,
  Light_Blue_Color,
  Question_Mark_Green_Icon,
  Phone_Grey_Icon,
  Map_Marker_Filled,
  BG_LightGREY_COLOR,
  THEME_COLOR_Opacity,
  Shipping_Green_Icon,
  Share_Grey_Icon,
  Saved_Green_Icon,
  Wishlist_Icon,
  Profile_Logo,
  Cart_White_Icon,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomList from "../../components/custom-list/custom-list";
import StarRating from "../../components/rating/star-rating";
import MainDrawer from "../../components/Drawer/Drawer";

// Google Map Components
import GoogleMapReact from "google-map-react";
import MessageSellerModel from "./message-seller-model1";
import ProductSuggestions from "./product-suggestions";
import Model from "../../components/model/model";
import MakeOfferModel from "./make-offer-model";
import { unfollowPost, followPost, followPeople, unfollowPeople } from "../../services/following";
import currencies from "../../translations/currency.json";
import SharePost from "../boat-details/share-post";
import VideoCallAppointment from "../boat-details/video-call-appointment";
import AcceptedMeetingInformation from "../boat-details/accepted-meeting-info";

const Marker = (props) => {
  const { color } = props;
  return <div className="marker" style={{ backgroundColor: color, cursor: "pointer" }} />;
};

class ProductContent extends Component {
  state = {
    product_details: true,
    askSeller: false,
    makeOffer: false,
    sharePost: false,
    videoCallReq: false,
    videoCallModal: false,
    currentPage: 0,
    meetingTimestamp: "",
    isVideo: false,
  };

  isVideoMeeting = (bool) => this.setState({ isVideo: bool });

  setMeetingTimestamp = (time) => this.setState({ meetingTimestamp: time });

  toggleVideoCallDrawer = () => this.setState({ videoCallModal: !this.state.videoCallModal });

  /** function to toggle video call modal */
  toggleVideoCallReq = () => this.setState({ videoCallReq: !this.state.videoCallReq });

  directNextPage = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
  };

  directGoBackPage = () => {
    console.log("clicked back / executed");
    if (this.state.currentPage === 0) return;
    this.setState({ currentPage: this.state.currentPage - 1 });
  };

  directResetPage = () => {
    console.log("clicked back / executed");
    this.setState({ currentPage: 0 });
  };

  componentDidMount() {
    console.log("prevProps", this.props.prevProps);
    this.setState({
      Saved: this.props.productDetail.likeStatus,
      Follow: this.props.productDetail.followRequestStatus,
    });
  }
  componentDidUpdate = (prevProps) => {
    console.log("prevProps", prevProps.productDetail, this.props.prevProps);
    if (prevProps.productDetail != this.props.productDetail) {
      console.log("prevProps", prevProps.productDetail, this.props.prevProps);
      this.setState({
        Saved: this.props.productDetail.likeStatus,
      });
    }
  };

  handleListToggle = (name) => {
    this.setState({
      [name]: !this.state[name],
    });
  };

  handleAskSellerModel = () => {
    this.setState({
      askSeller: !this.state.askSeller,
    });
  };

  handleMakeOfferModel = () => {
    this.setState({
      makeOffer: !this.state.makeOffer,
    });
  };

  static defaultProps = {
    center: {
      lat: 28.000002,
      lng: -82.728244,
    },
    zoom: 11,
  };

  handleUnfollowPost = (postId) => {
    let payload = {
      postId: postId.toString(),
      label: "Photo",
    };
    unfollowPost(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              Saved: false,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  handleFollowPost = (postId) => {
    let payload = {
      postId: postId.toString(),
      label: "Photo",
    };
    followPost(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              Saved: true,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleUnfollowPeople = (personName) => {
    let payload = {
      membername: personName,
    };
    unfollowPeople(payload)
      .then((res) => {
        console.log("resFollow", res);
        let response = res.data;
        if (response.code == 200) {
          this.setState({
            Follow: false,
          });
        }
      })
      .catch((err) => {
        console.log("errFollow", err);
      });
  };

  handleFollowPeople = (personName) => {
    let payload = {
      membername: personName,
    };
    followPeople(payload)
      .then((res) => {
        console.log("resFollow", res);
        let response = res.data;
        if (response.code == 200) {
          this.setState({
            Follow: true,
          });
        }
      })
      .catch((err) => {
        console.log("errFollow", err);
      });
  };

  handleShareBoatPost = () => {
    this.setState({
      sharePost: !this.state.sharePost,
    });
  };

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };
  render() {
    const { productDetail, userProfileData } = this.props;
    console.log("productDetail", productDetail);

    return (
      <Wrapper>
        <div className="container p-md-0">
          <div className="screenWidth mx-auto py-4">
            <div className="row">
              <div className="col-11 mx-auto">
                <div className="row">
                  <div className="col-8">
                    <div className="positon-relative py-3">
                      <div className="row m-0 align-items-center justify-content-between">
                        <p className="publishedDate">
                          Posted 2 month ago in{" "}
                          <a target="_blank">
                            {productDetail && productDetail.category} / {productDetail && productDetail.subCategory}
                          </a>
                        </p>
                        <div className="d-flex align-items-center">
                          <div className="saved_btn">
                            <ButtonComp
                              onClick={
                                this.state.Saved
                                  ? this.handleUnfollowPost.bind(this, productDetail && productDetail.postId)
                                  : this.handleFollowPost.bind(this, productDetail && productDetail.postId)
                              }
                            >
                              <img src={this.state.Saved ? Saved_Green_Icon : Wishlist_Icon} className="savedIcon"></img>
                              {this.state.Saved ? "Saved" : "Save"}
                            </ButtonComp>
                          </div>
                          <div className="share_btn">
                            <ButtonComp onClick={this.handleShareBoatPost}>
                              <img src={Share_Grey_Icon} className="shareIcon"></img>
                              Share
                            </ButtonComp>
                          </div>
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className="col p-0">
                          <h4 className="postTitle">{productDetail && productDetail.title}</h4>
                        </div>
                        <div className="col-4 pl-0 p-md-0 pr-lg-3 text-center">
                          {/* <div className="price_btn">
                        <ButtonComp>
                          <span id="priceCircle"></span>
                          {this.handleCurrencySymbol(productDetail && productDetail.currency)} {productDetail && productDetail.price}
                        </ButtonComp>
                      </div> */}
                        </div>
                        <div className="col-3 p-0 price_btnSec text-right">
                          <div className="price_btnV2">
                            <ButtonComp>
                              {/* <span id="priceCircle"></span> */}
                              {this.handleCurrencySymbol(productDetail && productDetail.currency)}{" "}
                              {productDetail && new Number(productDetail.price).toLocaleString("en-US")}
                            </ButtonComp>
                          </div>
                        </div>
                      </div>
                      <p className="pickupLocation borderBottom1">
                        <img src={Shipping_Green_Icon} className="shippingIcon"></img> Shipping from{" "}
                        <a target="_blank" className="loation">
                          {productDetail && productDetail.city}, <span>{productDetail && productDetail.countrySname}</span>
                        </a>{" "}
                        for $29.99 <img src={Question_Mark_Green_Icon} className="questionMarkIcon"></img>
                      </p>

                      <div className="row m-0 mb-2">
                        <div className="productDetails">
                          <p>Condition: </p>
                          <span>{productDetail && productDetail.condition}</span>
                        </div>
                        <div className="productfrom">
                          <p>Product Came Off: </p>
                          <span>{productDetail && productDetail.manufactor}</span>
                        </div>
                      </div>
                      <CustomList
                        name="Product Details"
                        open={this.state.product_details}
                        onClick={this.handleListToggle.bind(this, "product_details")}
                      >
                        <table className="boatdetails_Sec">
                          <tr className="borderBottom">
                            <th>
                              <p>Sub-Category</p>
                            </th>
                            <td>{productDetail && productDetail.subCategory}</td>
                          </tr>
                          <tr>
                            <th>
                              <p>
                                {productDetail &&
                                  productDetail.postFilter &&
                                  productDetail.postFilter[0] &&
                                  productDetail.postFilter[0].fieldName}
                              </p>
                            </th>
                            <td>
                              {productDetail &&
                                productDetail.postFilter &&
                                productDetail.postFilter[0] &&
                                productDetail.postFilter[0].values}
                            </td>
                          </tr>
                        </table>
                      </CustomList>
                      <div className="boatdescription_Sec">
                        <div className="desc_Sec">
                          <p className="descHeading">Product Overview</p>
                          <p className="desc">{productDetail && productDetail.description}</p>
                        </div>
                      </div>
                      {/* <div className="buyBoat_Btn">
                    <ButtonComp>
                      <img src={Long_Arrow_Right} className="arrowRight"></img>{" "}
                      Buy this Product with boatzon
                    </ButtonComp>
                  </div> */}
                    </div>
                  </div>
                  <div className="col-4 rightSideSec">
                    <div className="row m-0 sellerDetails_Sec">
                      <div className="col-12 p-0">
                        <div className="sellerCard">
                          <div className="row m-0 pb-2 align-items-center">
                            <div className="col-3 pl-0 pr-md-1">
                              <img
                                src={productDetail && productDetail.memberProfilePicUrl ? productDetail.memberProfilePicUrl : Profile_Logo}
                                className="sellerProfilePic"
                              ></img>
                            </div>
                            <div className="col-9 p-0">
                              <div className="position-relative">
                                {/* {userProfileData && userProfileData.business ? (
                                  <span className="sellerType">
                                    <img src={Professionals_White_Icon}></img>
                                    dealer
                                  </span>
                                ) : (
                                  ""
                                )} */}
                                <h6 className="sellerName">{productDetail && productDetail.memberFullName}</h6>
                                <StarRating startFontSize={"0.938vw"} value={4} disableHover={true} readOnly={true} />
                              </div>
                              <div className="follow_btn">
                                <ButtonComp
                                  onClick={
                                    this.state.Follow
                                      ? this.handleUnfollowPeople.bind(this, productDetail.membername)
                                      : this.handleFollowPeople.bind(this, productDetail.membername)
                                  }
                                >
                                  <img src={this.state.Follow ? Follow_Grey_Icon : Follow_Grey_Icon} className="followIcon"></img>
                                  {this.state.Follow ? "following" : "follow"}
                                </ButtonComp>
                              </div>
                            </div>
                          </div>
                          <div className="chat_btn">
                            <ButtonComp onClick={this.handleAskSellerModel}>
                              <img src={Chat_White_Icon} className="phoneIcon"></img>
                              Ask
                            </ButtonComp>
                          </div>

                          <div className="offer_btn">
                            <ButtonComp onClick={this.handleMakeOfferModel}>
                              <img src={Make_Offer_White_Icon} className="phoneIcon"></img>
                              Make Offer
                            </ButtonComp>
                          </div>
                          {(productDetail && productDetail.nationWide) || (productDetail && productDetail.nationWide == "true") ? (
                            <div className="buyNow">
                              <ButtonComp onClick={this.props.handleReviewOfferPage}>
                                <img src={Cart_White_Icon} className="phoneIcon"></img>
                                Buy Now
                              </ButtonComp>
                            </div>
                          ) : (
                            ""
                          )}

                          <div className="video_btn">
                            <ButtonComp onClick={this.toggleVideoCallDrawer}>
                              <img src={Video_Grey_Icon} className="phoneIcon"></img>
                              Video Appoinment
                            </ButtonComp>
                          </div>
                          <div className="inventory_btn">
                            <ButtonComp>
                              <img src={Inventory_Grey_Icon} className="phoneIcon"></img>
                              View All Inventory
                            </ButtonComp>
                          </div>
                        </div>
                        <div className="MapSec">
                          <GoogleMapReact
                            bootstrapURLKeys={{
                              key: "AIzaSyAtrnJwdRbJXfbsH4fr28N1TJG64c7Lrc4",
                            }}
                            center={{
                              lat: (productDetail && productDetail.latitude) || 28.000002,
                              lng: (productDetail && productDetail.longitude) || -82.728244,
                            }}
                            // zoom={this.props.zoom}
                            defaultZoom={this.props.zoom}
                            yesIWantToUseGoogleMapApiInternals
                          >
                            <Marker
                              lat={(productDetail && productDetail.latitude) || 28.000002}
                              lng={(productDetail && productDetail.longitude) || -82.728244}
                              color={THEME_COLOR_Opacity}
                            />
                          </GoogleMapReact>
                          <div className="loacationCard p-3 mx-2">
                            <div className="col-12 p-0">
                              <div className="row m-0 borderBottom align-items-center">
                                <div className="col-1 p-0">
                                  <img src={Phone_Grey_Icon} className="contactIcon"></img>
                                </div>
                                <div className="col-11 p-0">
                                  <p className="sellerContactNum">
                                    {(productDetail && productDetail.memberPhoneNumber) || "(888) 719-3246"}
                                  </p>
                                </div>
                              </div>
                              <div className="row m-0 pt-3">
                                <div className="col-1 p-0">
                                  <img src={Map_Marker_Filled} className="mapmarkerIcon"></img>
                                </div>
                                <div className="col-11 p-0">
                                  <div className="addressDetails">
                                    {/* <h6>Marine Trader, Inc</h6> */}
                                    <p>{productDetail && productDetail.place}</p>
                                    <a target="_blank">View on map</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <ProductSuggestions />
          </div>
        </div>

        {/* MessageSeller Drawer */}
        <MainDrawer open={this.state.askSeller} onClose={this.handleAskSellerModel} anchor="right">
          <MessageSellerModel
            productDetail={this.props.productDetail}
            onClose={this.handleAskSellerModel}
            setMeetingTimestamp={this.setMeetingTimestamp}
            meetingTimestamp={this.state.meetingTimestamp}
            isVideoMeeting={this.isVideoMeeting}
          />
        </MainDrawer>

        {/* MakeOffer Model */}
        <Model open={this.state.makeOffer} onClose={this.handleMakeOfferModel}>
          <MakeOfferModel
            onClose={this.handleMakeOfferModel}
            productDetail={this.props.productDetail}
            handleReviewOfferPage={this.props.handleReviewOfferPage}
          />
        </Model>

        <MainDrawer open={this.state.videoCallModal} onClose={this.toggleVideoCallDrawer} anchor="right">
          {this.state.currentPage === 0 ? (
            <VideoCallAppointment
              boatDetail={this.props.productDetail}
              onClose={this.toggleVideoCallDrawer}
              isDirect={true}
              directGoBackPage={this.directGoBackPage}
              directNextPage={this.directNextPage}
              directResetPage={this.directResetPage}
              setMeetingTimestamp={this.setMeetingTimestamp}
              isVideoMeeting={this.isVideoMeeting}
              isInPersonMeetingScreen={this.state.isInPersonMeetingScreen}
            />
          ) : this.state.currentPage === 1 ? (
            <AcceptedMeetingInformation
              isDirect={true}
              boatDetail={this.props.productDetail}
              onClose={this.toggleVideoCallDrawer}
              resetPage={this.directResetPage}
              meetingTimestamp={this.state.meetingTimestamp}
              isVideo={true}
            />
          ) : (
            ""
          )}
        </MainDrawer>

        {/**
         * request for video call
         * the prop is same for boat page and product page
         */}
        {/* <Model open={this.state.videoCallReq} onClose={this.toggleVideoCallReq}>
          <RequestVideoCallModal
            type="Request"
            onClose={this.toggleVideoCallReq}
            boatDetail={this.props.productDetail}
          />
        </Model> */}

        {/* Share Model */}
        <Model open={this.state.sharePost} onClose={this.handleShareBoatPost}>
          <SharePost productDetail={this.props.productDetail} onClose={this.handleShareBoatPost} />
        </Model>

        <style jsx>
          {`
            .questionMarkIcon {
              width: 0.658vw;
              margin-left: 0.219vw;
            }
            .loation {
              text-transform: capitalize;
            }
            .rightSideSec {
              max-width: 21.51vw;
            }
            .loation span {
              text-transform: uppercase;
            }
            :global(.share_btn button) {
              width: 4.026vw;
              min-width: 4.026vw;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_2};
              margin: 0;
              text-align: right;
              text-transform: capitalize;
              position: relative;
              box-shadow: none;
            }
            :global(.share_btn button span) {
              width: fit-content;
              margin: 0 0 0 auto;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              text-align: right;
              float: right;
              justify-content: flex-end;
            }
            :global(.share_btn button:focus),
            :global(.share_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.share_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .savedIcon {
              width: 0.732vw;
              margin-right: 0.366vw;
              margin-top: 0.0732vw;
            }
            .shareIcon {
              width: 0.732vw;
              margin-right: 0.366vw;
            }
            :global(.saved_btn button) {
              width: 4.026vw;
              min-width: 4.026vw;
              text-align: right !important;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              box-shadow: none;
            }
            :global(.saved_btn button span) {
              width: fit-content;
              margin: 0 0 0 auto;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              text-align: right;
              float: right;
              justify-content: flex-end;
            }
            :global(.saved_btn button:focus),
            :global(.saved_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.saved_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .productDetails,
            .productfrom {
              display: flex;
              align-items: center;
              margin-right: 0.732vw;
              padding: 0 0 0.732vw 0;
            }
            .productDetails p,
            .productfrom p {
              color: ${GREY_VARIANT_1};
              margin-bottom: 0;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .productDetails span {
              color: ${WHITE_COLOR};
              background: ${GREEN_COLOR};
              padding: 0.366vw 0.585vw;
              line-height: 1;
              margin-bottom: 0;
              margin-left: 0.366vw;
              font-size: 0.732vw;
              border-radius: 0.219vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .productfrom span {
              color: ${GREY_VARIANT_1};
              background: ${Border_LightGREY_COLOR};
              padding: 0.366vw 0.585vw;
              line-height: 1;
              margin-bottom: 0;
              margin-left: 0.366vw;
              border-radius: 0.219vw;
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .insuranceIcon,
            .financingIcon,
            .boatIcon {
              width: 1.024vw;
              margin-bottom: 0.146vw;
            }
            .historyIcon {
              width: 0.732vw;
              margin-bottom: 0.146vw;
            }
            :global(.MapSec > div > div > div) {
              top: -3.66vw !important;
            }
            :global(.gm-control-active, .gm-fullscreen-control) {
              top: 3.66vw !important;
            }
            :global(.marker) {
              position: absolute;
              top: 50%;
              left: 50%;
              width: 5.124vw;
              height: 5.124vw;
              background-color: #000;
              border-radius: 100%;
              user-select: none;
              transform: translate(-50%, -50%);
            }
            :global(.marker:hover) {
              z-index: 1;
            }
            .serviceTitle {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-weight: normal;
              color: ${FONTGREY_COLOR};
            }
            .followIcon {
              width: 0.512vw;
              margin-right: 0.366vw;
            }
            .loacationCard {
              background: ${WHITE_COLOR};
              position: absolute;
              bottom: 0.732vw;
              width: 95%;
            }
            .arrowRight {
              width: 1.024vw;
              margin-right: 0.732vw;
            }
            .MapSec {
              width: 100%;
              height: 24.158vw;
              margin-top: 1.464vw;
              position: relative;
            }
            .contactIcon {
              width: 0.878vw;
              margin-bottom: 0.658vw;
            }
            .mapmarkerIcon {
              width: 0.732vw;
              margin-bottom: 0.658vw;
            }
            .addressDetails h6 {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.658vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .addressDetails p {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.366vw;
              max-width: 14.641vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .addressDetails a {
              font-size: 0.805vw;
              text-decoration: none;
              color: ${THEME_COLOR};
              cursor: pointer;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .sellerContactNum {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.658vw;
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
            }
            .publishedDate {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 10px;
              font-family: "Open Sans" !important;
              letter-spacing: 0.00732vw !important;
            }
            .publishedDate:first-letter {
              text-transform: capitalize;
            }
            .publishedDate a {
              font-size: 0.732vw;
              color: ${GREEN_COLOR};
              text-transform: capitalize;
              text-decoration: none;
              font-family: "Open Sans" !important;
              letter-spacing: 0.1px !important;
            }
            .buyBoat_Btn {
              position: fixed;
              bottom: 0;
              left: 50%;
              transform: translate(-43%);
              z-index: 1;
              // width: 45%;
            }
            :global(.buyBoat_Btn button) {
              width: 100%;
              padding: 0.439vw 1.098vw;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              box-shadow: 2px 2px 3px #999;
            }
            :global(.buyBoat_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
            }
            :global(.buyBoat_Btn button:focus),
            :global(.buyBoat_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.buyBoat_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .postTitle {
              font-size: 1.405vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              margin-bottom: 0.732vw;
            }
            .price_btnSec {
              max-width: 28%;
            }
            :global(.price_btnV2 button) {
              // width: 75%;
              padding: 0.146vw 0.78125vw;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-weight: 500;
              // border-top-left-radius: 1.464vw !important;
              // border-bottom-left-radius: 1.464vw !important;
            }
            :global(.price_btnV2 button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 1.024vw;
            }
            :global(.price_btnV2 button:focus),
            :global(.price_btnV2 button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.price_btnV2 button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.price_btn button) {
              width: 75%;
              padding: 0.146vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-weight: 500;
              border-top-left-radius: 1.464vw !important;
              border-bottom-left-radius: 1.464vw !important;
            }
            :global(.price_btn button #priceCircle) {
              width: 0.585vw;
              height: 0.585vw;
              border-radius: 50%;
              background: ${BG_LightGREY_COLOR};
              margin-right: 1.098vw;
            }
            :global(.price_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 1.024vw;
            }
            :global(.price_btn button:focus),
            :global(.price_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.price_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .emiOptions {
              font-size: 0.732vw;
              color: ${GREEN_COLOR};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .insuranceOption {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .pickupLocation {
              align-items: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }
            .pickupLocation img {
              width: 0.878vw;
              margin-right: 0.219vw;
              margin-bottom: 0.0732vw;
            }
            .pickupLocation a {
              color: ${GREEN_COLOR};
              font-size: 0.878vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
            }
            .boatdetails_Sec {
              width: 100%;
            }
            .boatdetails_Sec th {
              width: 30%;
              font-weight: normal;
            }
            .boatdetails_Sec th p {
              font-size: 0.841vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-bottom: 0.219vw;
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .boatdetails_Sec td {
              width: 70%;
              color: ${GREY_VARIANT_2};
              font-size: 0.841vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              padding: 0.366vw 1.171vw;
            }
            .borderBottom {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .borderBottom1 {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .desc_Sec {
              padding: 1.098vw;
              border: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .descHeading {
              font-size: 0.915vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-bottom: 0.732vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0366vw !important;
            }
            .desc {
              font-size: 0.768vw;
              color: ${FONTGREY_COLOR};
              text-transform: initial;
              margin-bottom: 0;
              opacity: 0.8;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .sellerDetails_Sec {
              position: relative;
              top: -5.124vw;
              z-index: 0;
              width: 100%;
            }
            .sellerCard {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              box-shadow: 0px 0px 0.5px 0px ${BOX_SHADOW_GREY} !important;
              padding: 1.464vw 1.83vw;
              background: ${WHITE_COLOR};
            }
            .sellerProfilePic {
              width: 3.294vw;
              height: 3.294vw;
              border-radius: 50%;
              object-fit: cover;
            }
            .sellerType {
              background: ${THEME_COLOR};
              font-size: 0.439vw;
              color: ${BG_LightGREY_COLOR};
              text-transform: uppercase;
              padding: 0.146vw 0.292vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              border-radius: 0.219vw;
            }
            .sellerType img {
              width: 0.439vw;
              margin-bottom: 0.146vw;
              margin-right: 0.219vw;
            }
            .sellerName {
              font-size: 1.024vw;
              color: ${Dark_Blue_Color};
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .follow_btn {
              position: absolute;
              bottom: 3px;
              right: 0;
            }
            :global(.follow_btn button) {
              width: fit-content;
              padding: 0.219vw 0.292vw;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              text-transform: capitalize;
              font-size: 0.625vw;
              font-family: "Open Sans" !important;
            }
            :global(.follow_btn button span) {
              font-size: 0.625vw;
              font-family: "Open Sans" !important;
            }
            :global(.follow_btn button:focus),
            :global(.follow_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.follow_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .phoneIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 0.951vw;
            }
            :global(.callDetails_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.callDetails_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.callDetails_btn button:focus),
            :global(.callDetails_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.callDetails_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.chat_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Orange_Color};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.chat_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.chat_btn button:focus),
            :global(.chat_btn button:active) {
              background: ${Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.chat_btn button:hover) {
              background: ${Orange_Color};
            }
            :global(.offer_btn button:hover) {
              background: #378bcb;
            }
            :global(.offer_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.offer_btn button:focus),
            :global(.offer_btn button:active) {
              background: #378bcb;
              outline: none;
              box-shadow: none;
            }
            :global(.offer_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: #378bcb;
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }

            :global(.buyNow button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.buyNow button:focus),
            :global(.buyNow button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.buyNow button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }

            :global(.video_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREY_VARIANT_9};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.video_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.video_btn button:focus),
            :global(.video_btn button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.video_btn button:hover) {
              background: ${GREY_VARIANT_9};
            }
            :global(.inventory_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0 0.732vw 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              border: 0.146vw solid ${GREY_VARIANT_2};
            }
            :global(.inventory_btn button span) {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.805vw;
            }
            :global(.inventory_btn button:focus),
            :global(.inventory_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.inventory_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .serviceHeading {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              padding: 1.098vw 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              margin: 0;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .boatzonInsurance_Sec p,
            .boatzonFinancing_Sec p {
              font-size: 0.768vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-botton: 0.366vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.boatzonInsurance_Sec button) {
              width: 70%;
              padding: 0.585vw 0;
              background: ${GREEN_COLOR};
              color: ${GREY_VARIANT_11};
              margin: 0.732vw 0 0.732vw 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.boatzonInsurance_Sec button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.boatzonInsurance_Sec button:focus),
            :global(.boatzonInsurance_Sec button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.boatzonInsurance_Sec button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.boatzonFinancing_Sec button) {
              width: 70%;
              padding: 0.585vw 0;
              background: ${Light_Blue_Color};
              color: ${GREY_VARIANT_11};
              margin: 0.732vw 0 0.732vw 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.boatzonFinancing_Sec button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.boatzonFinancing_Sec button:focus),
            :global(.boatzonFinancing_Sec button:active) {
              background: ${Light_Blue_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.boatzonFinancing_Sec button:hover) {
              background: ${Light_Blue_Color};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(ProductContent);
