import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { Chevron_Right_White, Chevron_Left_White, Img_Placeholder } from "../../lib/config";

// Horizantal Scroll component
import Swiper from "swiper";
import ProductContent from "./product-content";
import ReviewYourOffer from "./review-your-offer";

class ProductDetails extends Component {
  state = {
    currentScreen: "",
  };

  componentDidMount() {
    const { productDetail } = this.props;
    let otherImageLinks = [];
    if (productDetail && productDetail.imageCount > 1) {
      for (let i = 1; i < productDetail.imageCount; i++) {
        let imageUrl = this.props.productDetail[`imageUrl${i}`];
        otherImageLinks.push(imageUrl);
      }
    }
    this.setState(
      {
        otherImageLinks,
      },
      () => {
        this.sliderTranslate();
      }
    );
  }

  sliderTranslate = () => {
    const slider = document.querySelector(".swiper1");
    var swiper1 = new Swiper(slider, {
      slidesPerView: "auto",
      spaceBetween: 0,
      loop: true,
      freeMode: true,
      autoplay: 1,
      mousewheel: {
        releaseOnEdges: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
    swiper1.init();
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleReviewOfferPage = () => {
    this.updateScreen(<ReviewYourOffer productDetail={this.props.productDetail} />);
  };
  render() {
    const { openpage, currentScreen, otherImageLinks } = this.state;
    const { productDetail } = this.props;
    return (
      <Wrapper>
        {!currentScreen ? (
          <Wrapper>
            <div className="col-12 p-0 position-relative productDetails_Banner">
              <div className="swiper-container swiper1">
                <div className="swiper-wrapper">
                  <div
                    className="swiper-slide"
                    style={{
                      backgroundImage: `url(${productDetail && productDetail.mainUrl})`,
                    }}
                  ></div>

                  {otherImageLinks &&
                    otherImageLinks.length > 0 &&
                    otherImageLinks.map((data) => (
                      <div
                        className="swiper-slide"
                        style={{
                          backgroundImage: `url(${data})`,
                        }}
                      ></div>
                    ))}

                  {productDetail && productDetail.imageCount < 3 && productDetail.imageCount == 2 && (
                    <div
                      className="swiper-slide"
                      style={{
                        backgroundImage: `url(${Img_Placeholder})`,
                      }}
                    ></div>
                  )}

                  {productDetail && productDetail.imageCount < 3 && productDetail.imageCount == 1 && (
                    <>
                      <div
                        className="swiper-slide"
                        style={{
                          backgroundImage: `url(${Img_Placeholder})`,
                        }}
                      ></div>
                      <div
                        className="swiper-slide"
                        style={{
                          backgroundImage: `url(${Img_Placeholder})`,
                        }}
                      ></div>
                    </>
                  )}
                </div>
                <img className="swiper-button-next" src={Chevron_Right_White}></img>
                <img className="swiper-button-prev" src={Chevron_Left_White}></img>
              </div>
            </div>
            <ProductContent handleReviewOfferPage={this.handleReviewOfferPage} productDetail={productDetail} />
          </Wrapper>
        ) : (
          currentScreen
        )}

        <style jsx>
          {`
            :global(.productDetails_Banner .swiper-container),
            :global(.swiper-wrapper) {
              z-index: 0 !important;
            }
            :global(.productDetails_Banner .swiper-slide) {
              height: 20vw;
              width: 33vw;
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
            }
            :global(.productDetails_Banner .swiper-button-next),
            :global(.productDetails_Banner .swiper-button-prev) {
              width: 0.951vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProductDetails;
