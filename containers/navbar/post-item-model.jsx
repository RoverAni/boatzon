import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Banner_BG,
  WHITE_COLOR,
  Vector_img,
  GREY_VARIANT_2,
  THEME_COLOR,
  Banner_HandImg,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
} from "../../lib/config";

// materail-UI Components
import { withStyles } from "@material-ui/core/styles";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import { requiredValidator } from "../../lib/validation/validation";
import IntlTelInput from "react-intl-tel-input";
import {
  COUNTRY_CODE,
  MOBILE,
} from "../../lib/input-control-data/input-definitions";

const styles = (theme) => ({
  radioGroupRoot: {
    flexDirection: "row",
  },
  radioLabelRoot: {
    width: "50%",
    margin: "0 !important",
  },
  radioRoot: {
    color: `${WHITE_COLOR} !important`,
    padding: "0.658vw 0.366vw 0.658vw 0 !important",
    "& $span": {
      "& $div": {
        "& $svg": {
          width: "0.781vw !important",
          height: "0.781vw !important",
        },
      },
    },
  },
  radioChecked: {
    color: `${WHITE_COLOR} !important`,
    "&:focus, &:active": {
      color: `${WHITE_COLOR}  !important`,
    },
  },
  radioLabelSpan: {
    fontSize: "0.833vw",
    fontFamily: `"Open Sans" !important`,
    color: `${WHITE_COLOR}`,
  },
});

class PostItemModel extends Component {
  state = {
    inputpayload: {},
  };

  componentDidMount() {
    this.setState({
      value: this.props && this.props.value,
    });
  }

  handleRadioChange = (event) => {
    this.setState({
      value: event.target.value,
    });
  };

  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate });
    let tempsignUpPayload = this.state.inputpayload;
    tempsignUpPayload[[name]] = inputControl.value;
    this.setState({
      inputpayload: tempsignUpPayload,
    });
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    this.handleOnchangePhone(status, num, country);
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log(
      "preferredCountries",
      this.state.preferredCountries,
      this.state.inputpayload
    );
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
        mobileValid: false,
      },
      () => {
        console.log(
          "preferredCountries",
          this.state.preferredCountries,
          this.state.inputpayload
        );
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        tempPayload[`preferredCountries`] = this.state.preferredCountries;
        this.setState({
          inputpayload: { ...tempPayload },
        });
        if (!this.state.isPhoneValid) {
          this.setState({
            mobileChecked: false,
            mobileValid: false,
          });
        }
      }
    );
  };

  render() {
    const { value, phoneInput } = this.state;
    const { classes } = this.props;
    const { mobile, preferredCountries, email } = this.state.inputpayload;
    console.log("djwi", this.props);
    return (
      <Wrapper>
        <div className="col-12 p-0 postItemPage">
          <div className="row m-0">
            <div className="col-7 p-0">
              <div>
                <h5 className="heading">
                  Post your boat or any marine product for sale in 30 seconds.
                </h5>
                <p className="desc">
                  Or download Boatzon to search and view 1000s of boats and
                  marine products for sale. Talk to local sellers, boat dealers,
                  and professionals, and more.
                </p>
                <img src={Vector_img} className="vectorImg"></img>
                <div className="row m-0">
                  <div className="col-9 p-0">
                    <RadioGroup
                      aria-label="gender"
                      name="postThrough"
                      value={value}
                      onChange={this.handleRadioChange}
                      classes={{ root: classes.radioGroupRoot }}
                    >
                      <FormControlLabel
                        classes={{
                          root: classes.radioLabelRoot,
                          label: classes.radioLabelSpan,
                        }}
                        value="phone"
                        control={
                          <Radio
                            classes={{
                              root: classes.radioRoot,
                              checked: classes.radioChecked,
                            }}
                          />
                        }
                        label="Phone Number"
                      />
                      <FormControlLabel
                        classes={{
                          root: classes.radioLabelRoot,
                          label: classes.radioLabelSpan,
                        }}
                        value="email"
                        control={
                          <Radio
                            classes={{
                              root: classes.radioRoot,
                              checked: classes.radioChecked,
                            }}
                          />
                        }
                        label="Email Address"
                      />
                    </RadioGroup>
                  </div>
                </div>
                {/* <div className="col-12 p-0"> */}
                {value == "phone" ? (
                  <Wrapper>
                    <div className="row m-0 align-items-center d-flex">
                      <div className="col-9 p-0">
                        <div className="PhoneInput">
                          <div className="position-relative">
                            <IntlTelInput
                              key={phoneInput}
                              preferredCountries={preferredCountries}
                              containerClassName="intl-tel-input"
                              value={mobile}
                              onSelectFlag={this.handleOnchangeflag}
                              onPhoneNumberChange={this.handleOnchangePhone}
                              formatOnInit={false}
                              separateDialCode={true}
                              fieldId="phoneNum"
                              autoComplete="off"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-3 p-0 Send_btn">
                        <ButtonComp>Send</ButtonComp>
                      </div>
                    </div>
                    {this.state.email == 0 || this.state.email == 2 ? (
                      <p className="errMessage">enter valid email</p>
                    ) : (
                      ""
                    )}
                  </Wrapper>
                ) : (
                  <Wrapper>
                    <div className="row m-0 align-items-center d-flex">
                      <div className="col-9 p-0">
                        <InputBox
                          type="email"
                          className="inputBox form-control"
                          placeholder="Your Email Address"
                          name="email"
                          value={email}
                          onChange={this.handleOnchangeInput("email")}
                          autoComplete="off"
                        ></InputBox>
                      </div>
                      <div className="col-3 p-0 Send_btn">
                        <ButtonComp>Send</ButtonComp>
                      </div>
                    </div>
                    {this.state.email == 0 || this.state.email == 2 ? (
                      <p className="errMessage">enter valid email</p>
                    ) : (
                      ""
                    )}
                  </Wrapper>
                )}
                {/* </div> */}
              </div>
            </div>
            <div className="col-5 p-0">
              <div className="position-relative pl-4">
                <img src={Banner_HandImg} className="handImg"></img>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .postItemPage {
              width: 51.979vw;
              background-image: url(${Banner_BG});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center;
              position: relative;
              padding: 2.562vw 2.928vw !important;
              overflow: hidden;
            }
            .heading {
              color: ${WHITE_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 1.875vw;
              max-width: 93%;
            }
            .desc {
              font-family: "Open Sans" !important;
              font-size: 0.729vw;
              color: ${WHITE_COLOR};
              max-width: 100%;
              margin-bottom: 0.366vw;
            }
            .vectorImg {
              width: 5.856vw;
              padding: 0.732vw 0 0.366vw 0;
            }
            :global(.postItemPage .inputBox) {
              display: block;
              width: 100%;
              height: 2.196vw;
              padding: 0.292vw 12px;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0.366vw 0;
            }
            :global(.postItemPage .inputBox:focus) {
              color: ${GREY_VARIANT_2};
              background-color: #fff;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.postItemPage .inputBox::placeholder) {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            .errMessage {
              font-size: 0.732vw;
              text-align: left;
              color: red;
              font-family: "Open Sans" !important;
              padding-top: 0.732vw;
              margin-bottom: 0;
              text-transform: capitalize;
            }
            :global(.Send_btn button) {
              width: 100%;
              height: 2.196vw;
              padding: 0.292vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.366vw 0;
              border-radius: 0;
              text-transform: capitalize;
            }
            :global(.Send_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              font-weight: 600;
            }
            :global(.Send_btn button:focus),
            :global(.Send_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.Send_btn button:hover) {
              background: ${THEME_COLOR};
            }
            .handImg {
              width: 95%;
              position: absolute;
              top: 0;
            }

            :global(.postItemPage .PhoneInput) {
              position: relative;
              width: 100%;
            }
            :global(.postItemPage .PhoneInput > div > .intl-tel-input) {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
              border: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.342vw;
              padding: 0;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                > #phoneNum) {
              width: 81%;
              height: 100%;
              line-height: 1 !important;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              padding: 0 0.366vw !important;
              margin-left: 0.366vw !important;
              flex-grow: 1;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .country-list) {
              width: 14.641vw !important;
              margin: 0.219vw 0 0 0;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .country-list
                .country) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                > #phoneNum:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container) {
              height: 2.342vw;
              position: relative !important;
              width: fit-content !important;
              display: block ruby !important;
              padding: 0 !important;
              flex-grow: 2;
              border-radius: 0;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag) {
              width: 100% !important;
              background: ${WHITE_COLOR} !important;
              height: 100%;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag:focus),
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag:active) {
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag
                .selected-dial-code) {
              font-size: 0.805vw;
              height: 2.122vw;
              align-items: center;
              display: flex;
              padding: 0 0.366vw;
              line-height: 1 !important;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag:focus) {
              box-shadow: none !important;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag
                .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag
                .iti-flag) {
              display: none;
            }
            :global(.postItemPage
                .PhoneInput
                > div
                > .intl-tel-input
                .flag-container
                .selected-flag
                .iti-arrow) {
              display: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(PostItemModel);
