import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
  GREY_VARIANT_7,
  Border_LightGREY_COLOR,
  Professionals_Blue_Icon,
  GREY_VARIANT_1,
  Discussion_Blue_Icon,
  Product_Active,
} from "../../lib/config";
import Router from "next/router";

class SearchModel extends Component {
  state = {
    productsList: [],
    discussionList: [],
    professionalList: [],
  };

  componentDidMount() {
    const { searchData } = this.props;
    let productsList =
      searchData &&
      searchData.length > 0 &&
      searchData.filter((data) => data._source.searchType === "Products");

    let boatsList =
      searchData &&
      searchData.length > 0 &&
      searchData.filter((data) => data._source.searchType === "Boats");

    let usersList =
      searchData &&
      searchData.length > 0 &&
      searchData.filter((data) => data._type === "users");

    let professionalList =
      searchData &&
      searchData.length > 0 &&
      searchData.filter((data) => data._type === "professional");

    let discussionList =
      searchData &&
      searchData.length > 0 &&
      searchData.filter((data) => data._source.searchType === "discussion");

    this.setState({
      boatsList,
      productsList,
      discussionList,
      professionalList,
      usersList,
    });
  }

  handleBoatDetails = (item) => {
    Router.push(
      `/boat-detail/${item.productName.replace(/ /g, "-")}?pid=${item.postId}`
    );
  };

  handleProductDetails = (item) => {
    Router.push(
      `/product-detail/${item.productName.replace(/ /g, "-")}?pid=${
        item.postId
      }`
    );
  };

  render() {
    const {
      boatsList,
      productsList,
      discussionList,
      professionalList,
      usersList,
    } = this.state;
    const { searchInputValue, searchData } = this.props;
    console.log("searchData", searchData);
    return (
      <Wrapper>
        <div className="searchModel">
          {" "}
          {boatsList && boatsList.length ? (
            <div className="row m-0 align-items-start bottomBorder">
              <div className="col-1 p-0 IconSec">
                <img src={Product_Active} className="boatIcon"></img>
              </div>
              <div className="col-11 p-0">
                <ul className="list-unstyled mb-0">
                  {boatsList.map((data, index) => (
                    <li
                      className="searchItem"
                      onClick={this.handleBoatDetails.bind(this, data._source)}
                    >
                      <p>
                        {/* {searchInputValue}{" "} */}
                        <span className="itemName">
                          {data._source.productName
                            ? data._source.productName
                            : ""}{" "}
                        </span>
                        <span className="in">in </span>
                        <a target="_blank" className="itemLink">
                          {data._source.searchType}
                        </a>
                      </p>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ) : (
            ""
          )}
          {productsList && productsList.length ? (
            <div className="row m-0 align-items-start bottomBorder">
              <div className="col-1 p-0 IconSec">
                <img src={Product_Active} className="boatIcon"></img>
              </div>
              <div className="col-11 p-0">
                <ul className="list-unstyled mb-0">
                  {productsList.map((data, index) => (
                    <li
                      className="searchItem"
                      onClick={this.handleProductDetails.bind(
                        this,
                        data._source
                      )}
                    >
                      <p>
                        {/* {searchInputValue}{" "} */}
                        <span className="itemName">
                          {data._source.productName
                            ? data._source.productName
                            : ""}{" "}
                        </span>
                        <span className="in">in </span>
                        <a target="_blank" className="itemLink">
                          {data._source.searchType}
                        </a>
                      </p>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ) : (
            ""
          )}
          {usersList && usersList.length ? (
            <div className="row m-0 align-items-start bottomBorder">
              <div className="col-1 p-0 IconSec">
                <img
                  src={Professionals_Blue_Icon}
                  className="professionalsIcon"
                ></img>
              </div>
              <div className="col-11 p-0">
                <ul className="list-unstyled mb-0">
                  {usersList.map((data, index) => (
                    <li className="searchItem">
                      <p>
                        {data._source.username} <span className="in">in</span>{" "}
                        <a target="_blank" className="itemLink">
                          {data._type}
                        </a>
                      </p>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ) : (
            ""
          )}
          {professionalList && professionalList.length ? (
            <div className="row m-0 align-items-start bottomBorder">
              <div className="col-1 p-0 IconSec">
                <img
                  src={Professionals_Blue_Icon}
                  className="professionalsIcon"
                ></img>
              </div>
              <div className="col-11 p-0">
                <ul className="list-unstyled mb-0">
                  {professionalList.map((data, index) => (
                    <li className="searchItem">
                      <p>
                        {data._source.username} <span className="in">in</span>{" "}
                        <a target="_blank" className="itemLink">
                          {data._type}
                        </a>
                      </p>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ) : (
            ""
          )}
          {discussionList && discussionList.length ? (
            <div className="row m-0 align-items-start bottomBorder">
              <div className="col-1 p-0 IconSec">
                <img src={Discussion_Blue_Icon} className="projectIcon"></img>
              </div>
              <div className="col-11 p-0">
                <ul className="list-unstyled mb-0">
                  {discussionList.map((data, index) => (
                    <li className="searchItem">
                      <p>
                        {data._source.title} <span className="in">in</span>{" "}
                        <a target="_blank" className="itemLink">
                          {data._type}
                        </a>
                      </p>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>

        <style jsx>
          {`
            .searchModel {
              padding: 0.732vw 1.098vw;
            }
            .boatIcon,
            .professionalsIcon,
            .projectIcon {
              width: 1.024vw;
              // margin: 0.366vw 0;
              padding: 0.366vw 0;
            }
            .IconSec {
              max-width: 1.464vw;
            }
            .searchItem {
              padding: 0.366vw;
              cursor: pointer;
            }
            .bottomBorder {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .searchItem:hover {
              background: ${GREY_VARIANT_7};
            }
            .searchItem p {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1} !important;
              font-family: "Open Sans-SemiBold" !important;
              // font-weight: 600;
              margin: 0;
              text-transform: capitalize;
            }
            .itemName {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              font-weight: normal;
              text-transform: capitalize;
            }
            .in {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              font-weight: normal;
              text-transform: lowercase;
            }
            .itemLink {
              font-size: 0.878vw;
              color: ${THEME_COLOR} !important;
              font-family: "Open Sans" !important;
              font-weight: normal;
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SearchModel;
