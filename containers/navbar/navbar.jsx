// Main React Components
import React, { Component } from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// asstes and colors
import {
  APP_LOGO,
  Search_White_Icon,
  Chat_Icon_Grey,
  Profile_Logo,
  BG_LightGREY_COLOR,
  THEME_COLOR,
  FONTGREY_COLOR,
  Dark_Blue_Color,
  GREY_VARIANT_2,
  Map_Marker,
  Buying_Blue_Icon,
  Buying_Grey_Icon,
  My_Appointment_Blue_Icon,
  My_Appointment_Grey_Icon,
  Chevron_Left,
  Chevron_Down,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR,
  WHITE_COLOR,
  BOX_SHADOW_GREY,
  Boatzon_ProfilePic,
  Logout_Grey_Icon,
  Logout_Blue_Icon,
  MyAcc_Grey_Icon,
  MyAcc_Blue_Icon,
  PublicProfile_Grey_Icon,
  PublicProfile_Blue_Icon,
  LogIn_Icon,
  THEME_COLOR_Opacity,
  Selling_Filled_Blue_Icon,
  Selling_Filled_Grey_Icon,
  Transaction_History_Blue,
  Transaction_History_Grey,
  Saved_Grey_Icon,
  Saved_Blue_Icon,
  Suitecase_Grey_Icon,
  LIGHT_THEME_BG,
  Placeholder_Image,
  GREY_VARIANT_3,
  My_Location_Icon_Blue,
  My_Location_Icon_Grey,
  THEME_COLOR_Opacity1,
  Flag_Usa_Icon_Blue,
  Flag_Usa_Icon_Grey,
  Globe_Icon_Blue,
  Globe_Icon_Grey,
  Map_Marked_Icon_Blue,
  Map_Marked_Icon_Grey,
  Hide_Icon_Grey,
  Light_Blue_Color,
} from "../../lib/config";

// Reactstrap Components
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

// reusable component
import ButtonComp from "../../components/button/button";
import Model from "../../components/model/model";
import MainDrawer from "../../components/Drawer/Drawer";
import CustomLink from "../../components/Link/Link";
import InputBox from "../../components/input-box/input-box";
import OutsideAlerter from "../outside-alerter/outside-alerter";
import { findDayAgo } from "../../lib/date-operation/date-operation";
// redux context component
import Context from "../../context/context";

// imported components
import LoginPage from "../auth-modals/login/login-page";
import CreatePost from "../create-post/create-post";
import SearchModel from "./search-model";
import SignUpPage from "../auth-modals/sign-up/sign-up-page";
import PostItemModel from "./post-item-model";
import Snackbar from "../../components/snackbar";
import { getCookie } from "../../lib/session";
import { getUserData } from "../../redux/actions/user-data/user-data";
import {
  getCurrentLocation,
  setCurrentLocation,
} from "../../redux/actions/location/location";
import { userLogout } from "../../services/auth";
import { removeCookie } from "../../lib/session";
import Router from "next/router";
import MyLocationModel from "../filter-sec/my-location-model";
import { userSelectedToChat } from "../../redux/actions/chat/chat";
import InfoTooltip from "../../components/tooltip/info-tooltip";
import { postSearchData } from "../../services/search";
import debounce from "lodash.debounce";


class Navbar extends Component {
  state = {
    loginPage: false,
    createPost: false,
    searchPopover: false,
    signUpModel: false,
    dropdownOpen: false,
    messageDropdown: false,
    locationDropdown: false,
    locationModel: false,
    openPage: false,
    location: "nearby",
    locationType: 1,
    searchData: [],
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState(
      {
        openPage: true,
        AuthPass,
      },
      () => {
        if (AuthPass) {
          this.props.dispatch(getUserData());
        }
      }
    );
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.currentLocation != this.props.currentLocation) {
      this.setState({
        location:
          this.props.currentLocation &&
          this.props.currentLocation.city &&
          this.props.currentLocation.city != "" &&
          this.props.currentLocation.countryShortName &&
          this.props.currentLocation.countryShortName != ""
            ? `${this.props.currentLocation.city}, ${this.props.currentLocation.countryShortName}`
            : "nearby",
      });
    }
  };

  handleGlobalLocation = () => {
    this.setState({
      locationType: 3,
      location: "Global",
    });
  };

  handleNationWideLocation = () => {
    this.setState({
      locationType: 2,
      location:
        this.props.currentLocation &&
        this.props.currentLocation.city &&
        this.props.currentLocation.city != "" &&
        this.props.currentLocation.country &&
        this.props.currentLocation.country != ""
          ? `${this.props.currentLocation.country}`
          : "nearby",
    });
  };

  handleMyLocation = () => {
    this.props.dispatch(getCurrentLocation());
    this.setState({
      locationType: 1,
      location:
        this.props.currentLocation &&
        this.props.currentLocation.city &&
        this.props.currentLocation.city != "" &&
        this.props.currentLocation.countryShortName &&
        this.props.currentLocation.countryShortName != ""
          ? `${this.props.currentLocation.city}, ${this.props.currentLocation.countryShortName}`
          : "nearby",
    });
  };

  onError = (e) => {
    e.target.scr = Placeholder_Image;
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  handleSignupModel = () => {
    this.setState({
      signUpModel: !this.state.signUpModel,
    });
  };

  handleProSignupModel = () => {
    Router.push("/professional-signup");
  };

  // Function to redirect to createPost drawer
  handleCreatePost = () => {
    this.setState({
      createPost: !this.state.createPost,
    });
  };

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };

  handleSearchPopover = (event) => {
    let inputValue = event.target.value;
    this.setState(
      {
        searchInputValue: inputValue,
        searchData: [],
        searchPopover: false,
      },
      () => {
        if (
          (this.state.searchInputValue &&
            this.state.searchInputValue.length > 2) ||
          this.state.searchInputValue == ""
        ) {
          this.handleSearchAPI(this.state.searchInputValue);
        }
      }
    );
  };

  handleSearchAPI = debounce((value) => {
    let payload = {
      limit: "20",
      skip: "0",
      search: value,
      type: this.state.locationType,
      countrySname: this.props.currentLocation.countryShortName,
      lat: this.props.currentLocation.latitude,
      lon: this.props.currentLocation.longitude,
    };
    postSearchData(payload)
      .then((res) => {
        console.log("fjiefj-->res", res.data);
        if (res && res.status == 200) {
          this.setState({
            searchData: res.data.data.length > 0 ? res.data.data : [],
            searchPopover: res.data.data.length > 0 ? true : false,
          });
        }
      })
      .catch((err) => {
        console.log("fjiefj-->err", err);
        this.setState({
          searchData: [],
          searchPopover: false,
        });
      });
  }, 500);

  handleToggleMessageDropdown = () => {
    this.setState({
      messageDropdown: !this.state.messageDropdown,
    });
  };

  handleToggleLocationDropdown = () => {
    this.setState({
      locationDropdown: !this.state.locationDropdown,
    });
  };

  handleCloseMessageDropdown = () => {
    this.setState({
      messageDropdown: false,
    });
  };

  handleCloseSearch = () => {
    this.setState({
      searchPopover: false,
    });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      default:
        return "error";
        break;
    }
  };

  handleLogout = () => {
    userLogout()
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        this.setState({
          usermessage: response.message,
          variant: this.handleSnackbar(response),
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });

        if (response.code == 200) {
          removeCookie("token");
          removeCookie("authPass");
          removeCookie("mqttId");
          removeCookie("uid");
          removeCookie("userName");
          removeCookie("ucity");
          window.location.replace("/");
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleLocationModel = () => {
    this.setState({
      locationModel: !this.state.locationModel,
    });
  };

  handleCurrentAddAddress = (AddressData) => {
    console.log("AddressData", AddressData);
    let payload = {
      address: AddressData.address,
      latitude: AddressData.lat,
      longitude: AddressData.lng,
      city: AddressData.city,
      country: AddressData.country,
      countryShortName: AddressData.countryShortName,
      zipCode: AddressData.zipCode,
    };
    this.props.dispatch(setCurrentLocation(payload));
    this.setState({
      locationType: 4,
      locationModel: false,
    });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const SearchBtnTooltip = (
      <div className="SearchBtnTooltip_Sec">
        <p className="Msg">
          Got a question? <span>Search</span> for the answer. Chances are,
          someone has your back.
        </p>
        <div className="d-flex align-items-center justify-content-between footerSec">
          <p className="hideMsg">
            <img src={Hide_Icon_Grey} className="hideIcon"></img>
            <span>Hide these tips</span>
          </p>
          <div className="gotIt_btn">
            <ButtonComp>Got it</ButtonComp>
          </div>
        </div>
      </div>
    );
    return (
      <Context.Consumer>
        {(Context) => {
          return (
            <Wrapper>
              {this.state.openPage ? (
                <div className="container p-md-0">
                  <div className="screenWidth mx-auto">
                    <div className="row m-0 align-items-center justify-content-between py-2 border-bottom Navbar">
                      {/* App Logo */}
                      <div className="col-md-3 col-lg-3 col-xl-3 p-0">
                        <div className="text-left">
                          <CustomLink href="/">
                            <img src={APP_LOGO} className="appLogo" />
                          </CustomLink>
                        </div>
                      </div>
                      {/* Search Bar */}
                      <div className="col p-0 searchBar">
                        <div className="d-flex align-items-center justify-content-between searchBar-height">
                          <div className="searchInput">
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              placeholder="Search..."
                              onChange={this.handleSearchPopover}
                            />
                          </div>
                          {this.state.searchPopover ? (
                            <OutsideAlerter onClose={this.handleCloseSearch}>
                              <div className="searchPopover">
                                <SearchModel
                                  searchData={this.state.searchData}
                                  searchInputValue={this.state.searchInputValue}
                                />
                              </div>
                            </OutsideAlerter>
                          ) : (
                            ""
                          )}

                          <ButtonDropdown
                            isOpen={this.state.locationDropdown}
                            toggle={this.handleToggleLocationDropdown}
                            className="locationDropdownBtn"
                            title={
                              this.state.location == "nearby"
                                ? ""
                                : this.state.location
                            }
                          >
                            <DropdownToggle>
                              <div className="row m-0 d-flex align-items-center justify-content-between locationSec">
                                <div className="d-flex align-items-center">
                                  <img
                                    src={Map_Marker}
                                    className="mapMarker"
                                  ></img>
                                  <p className="nearByLocation">
                                    {this.state.location}
                                  </p>
                                </div>
                                <img
                                  src={Chevron_Left}
                                  className="leftChevron"
                                ></img>
                              </div>
                            </DropdownToggle>
                            <DropdownMenu className="locationDropdownList">
                              <DropdownItem
                                className="dropdownItem"
                                onClick={this.handleMyLocation}
                                title=""
                              >
                                <div className="row m-0 align-items-center ProfileLink_Sec myLocation_Sec">
                                  <div className="col-3 p-0 firstSec">
                                    <div className="text-center myLocationIcon"></div>
                                  </div>
                                  <div className="col-9 p-0">
                                    <p className="locationOption m-0">
                                      My Location
                                    </p>
                                  </div>
                                </div>
                              </DropdownItem>
                              <DropdownItem
                                className="dropdownItem"
                                onClick={this.handleNationWideLocation}
                                title=""
                              >
                                <div className="row m-0 align-items-center ProfileLink_Sec nationWide_Sec">
                                  <div className="col-3 p-0 firstSec">
                                    <div className="text-center nationWideIcon"></div>
                                  </div>
                                  <div className="col-9 p-0">
                                    <p className="locationOption m-0">
                                      Nationwide
                                    </p>
                                  </div>
                                </div>
                              </DropdownItem>
                              <DropdownItem
                                className="dropdownItem"
                                onClick={this.handleGlobalLocation}
                                title=""
                              >
                                <div className="row m-0 align-items-center ProfileLink_Sec global_Sec">
                                  <div className="col-3 p-0 firstSec">
                                    <div className="text-center globalIcon"></div>
                                  </div>
                                  <div className="col-9 p-0">
                                    <p className="locationOption m-0">Global</p>
                                  </div>
                                </div>
                              </DropdownItem>
                              <DropdownItem
                                className="dropdownItem"
                                onClick={this.handleLocationModel}
                                title=""
                              >
                                <div className="row m-0 align-items-center ProfileLink_Sec selectOnMap_Sec">
                                  <div className="col-3 p-0 firstSec">
                                    <div className="text-center selectMapIcon"></div>
                                  </div>
                                  <div className="col-9 p-0">
                                    <p className="locationOption m-0">
                                      Select On Map
                                    </p>
                                  </div>
                                </div>
                              </DropdownItem>
                            </DropdownMenu>
                          </ButtonDropdown>

                          <InfoTooltip
                            tooltipContent={SearchBtnTooltip}
                            placement="bottom"
                            color={Light_Blue_Color}
                          >
                            <div className="Search_btn">
                              <ButtonComp>
                                <img
                                  src={Search_White_Icon}
                                  className="searchIcon"
                                />
                              </ButtonComp>
                            </div>
                          </InfoTooltip>
                        </div>
                      </div>

                      <div className="col px-0 rightSec">
                        <ul className="row m-0 list-unstyled align-items-center mainNavUL">
                          <li
                            className="sell-li"
                            onClick={
                              this.state.AuthPass
                                ? this.handleCreatePost
                                : this.handleLogin
                            }
                          >
                            Sell
                            {/* <img
                              src={Sell_Icon}
                              className="sellIcon"
                              onClick={
                                this.state.AuthPass
                                  ? this.handleCreatePost
                                  : this.handleLogin
                              }
                            ></img> */}
                          </li>
                          {this.state.AuthPass ? (
                            <Wrapper>
                              <li className="d-flex align-items-center">
                                <div className="m-0">
                                  <ButtonDropdown
                                    isOpen={this.state.messageDropdown}
                                    toggle={this.handleToggleMessageDropdown}
                                    className="messageDropdownBtn"
                                  >
                                    <DropdownToggle>
                                      <img
                                        src={Chat_Icon_Grey}
                                        className="chatIcon"
                                      ></img>
                                    </DropdownToggle>
                                    <DropdownMenu className="messageDropdownList">
                                      <p className="recentText">Recent</p>
                                      {this.props &&
                                        this.props.allChats
                                          .slice(0, 4)
                                          .map((k, i) => (
                                            <CustomLink
                                              href="/messenger"
                                              index={i}
                                            >
                                              <DropdownItem
                                                className="dropdownItem"
                                                onClick={() =>
                                                  this.props.dispatch(
                                                    userSelectedToChat(k)
                                                  )
                                                }
                                              >
                                                <div className="row m-0 align-items-center">
                                                  <div className="col-2 p-0">
                                                    <img
                                                      src={
                                                        k.profilePic ||
                                                        Placeholder_Image
                                                      }
                                                      width={25}
                                                      height={25}
                                                      onError={this.onError}
                                                      className="messageProfilePic"
                                                    ></img>
                                                  </div>
                                                  <div className="col-10 p-0 MessengerInfo">
                                                    <p className="messengerName">
                                                      {k.userName}
                                                    </p>
                                                    <p className="messengerManufacturer">
                                                      {k.productName}
                                                    </p>
                                                    <span className="messageDate">
                                                      {findDayAgo(k.timestamp)}
                                                    </span>
                                                  </div>
                                                </div>
                                              </DropdownItem>
                                            </CustomLink>
                                          ))}
                                    </DropdownMenu>
                                  </ButtonDropdown>
                                </div>
                                <ButtonDropdown
                                  isOpen={this.state.dropdownOpen}
                                  toggle={this.toggle}
                                  className="DropdownBtn"
                                >
                                  <DropdownToggle>
                                    <div className="d-flex align-items-center ProfileSec">
                                      <img
                                        src={
                                          (this.props.userProfileData &&
                                            this.props.userProfileData
                                              .profilePicUrl) ||
                                          Profile_Logo
                                        }
                                        className="profileLogo"
                                      ></img>
                                      <span>
                                        {this.props.userProfileData &&
                                          this.props.userProfileData.username}
                                      </span>
                                      <img
                                        src={Chevron_Down}
                                        className="chevronDown"
                                      ></img>
                                    </div>
                                  </DropdownToggle>
                                  <DropdownMenu className="DropdownList">
                                    <CustomLink href="/user-profile">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center userAcc">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="dd_profileIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <div className="d-flex flex-column ml-2">
                                              <span className="userName">
                                                {this.props.userProfileData &&
                                                  this.props.userProfileData
                                                    .username}
                                              </span>
                                              <span className="userType">
                                                User
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>
                                    {this.props.userProfileData &&
                                    this.props.userProfileData.businessName ? (
                                      <CustomLink href="/professional-user-profile">
                                        <DropdownItem>
                                          <div className="row m-0 align-items-center profileAcc">
                                            <div className="col-3 p-0 firstSec">
                                              <div className="profilePic"></div>
                                            </div>
                                            <div className="col-9 p-0">
                                              <div className="d-flex flex-column ml-2">
                                                <span className="userName">
                                                  {this.props.userProfileData &&
                                                    this.props.userProfileData
                                                      .businessName}
                                                </span>
                                                <span className="userType">
                                                  Professional
                                                </span>
                                              </div>
                                            </div>
                                          </div>
                                        </DropdownItem>
                                      </CustomLink>
                                    ) : (
                                      ""
                                    )}
                                    <CustomLink href="/selling">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center SellingLink_Sec ProfileLink_Sec">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="text-center sellingIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <p className="linkName m-0 ml-2">
                                              Selling
                                            </p>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>
                                    <CustomLink href="/buying">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center BuyingLink_Sec ProfileLink_Sec">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="text-center buyingIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <p className="linkName m-0 ml-2">
                                              Buying
                                            </p>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>
                                    <CustomLink href="/my-appointments">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center appointmentLink_Sec ProfileLink_Sec">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="text-center appointmentIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <p className="linkName m-0 ml-2">
                                              My Appointments
                                            </p>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>

                                    <CustomLink href="/following">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center folloingLink_Sec ProfileLink_Sec">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="text-center followingIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <p className="linkName m-0 ml-2">
                                              Following
                                            </p>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>
                                    <CustomLink href="/purchases-sales">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center salesLink_Sec ProfileLink_Sec">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="text-center salesIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <p className="linkName m-0 ml-2">
                                              Purchases & Sales
                                            </p>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>
                                    <DropdownItem>
                                      <div className="row m-0 align-items-center publicProfileLink_Sec ProfileLink_Sec">
                                        <div className="col-3 p-0 firstSec">
                                          <div className="text-center publicProfileIcon"></div>
                                        </div>
                                        <div className="col-9 p-0">
                                          <p className="linkName m-0 ml-2">
                                            My Public Profile
                                          </p>
                                        </div>
                                      </div>
                                    </DropdownItem>
                                    <CustomLink href="/my-account/general-settings">
                                      <DropdownItem>
                                        <div className="row m-0 align-items-center myAccLink_Sec ProfileLink_Sec">
                                          <div className="col-3 p-0 firstSec">
                                            <div className="text-center myAccIcon"></div>
                                          </div>
                                          <div className="col-9 p-0">
                                            <p className="linkName m-0 ml-2">
                                              My Account
                                            </p>
                                          </div>
                                        </div>
                                      </DropdownItem>
                                    </CustomLink>
                                    <DropdownItem onClick={this.handleLogout}>
                                      <div className="row m-0 align-items-center logoutLink_Sec ProfileLink_Sec">
                                        <div className="col-3 p-0 firstSec">
                                          <div className="text-center logoutIcon"></div>
                                        </div>
                                        <div className="col-9 p-0">
                                          <p className="linkName m-0 ml-2">
                                            Logout
                                          </p>
                                        </div>
                                      </div>
                                    </DropdownItem>
                                  </DropdownMenu>
                                </ButtonDropdown>
                              </li>
                            </Wrapper>
                          ) : (
                            <li className="d-flex align-items-center">
                              {/* Login/SignOut */}

                              <div
                                className="LoginSec"
                                onClick={this.handleLogin}
                              >
                                <ButtonComp>
                                  <img
                                    src={LogIn_Icon}
                                    className="loginIcon"
                                  ></img>{" "}
                                  {Context.locale.login}
                                </ButtonComp>
                              </div>

                              {/* <div
                              className="SignUpSec"
                              onClick={this.handleSignupModel}
                            >
                              <ButtonComp className="p-0">
                                <img
                                  src={SignUp_Icon}
                                  className="signupIcon"
                                ></img>
                                {Context.locale.signup}
                              </ButtonComp>
                            </div> */}
                            </li>
                          )}
                          {this.props.userProfileData &&
                          this.props.userProfileData.businessName ? (
                            ""
                          ) : (
                            <li>
                              <div
                                className="SignUpSec"
                                onClick={this.handleProSignupModel}
                              >
                                <ButtonComp className="p-0">
                                  <img
                                    src={Suitecase_Grey_Icon}
                                    className="proIcon"
                                  ></img>
                                  Join as a Pro
                                </ButtonComp>
                              </div>
                            </li>
                          )}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
              {/* Login Model */}
              <Model
                open={this.state.loginPage}
                onClose={this.handleLogin}
                authmodals={true}
              >
                <LoginPage onClose={this.handleLogin} />
              </Model>

              {/* SignUp Model */}
              <Model
                open={this.state.signUpModel}
                onClose={this.handleSignupModel}
                authmodals={true}
              >
                <SignUpPage onClose={this.handleSignupModel} />
              </Model>

              {/* Post Item Model */}
              <Model
                open={this.state.postItemModel}
                onClose={this.handlePostItemModel}
              >
                <PostItemModel onClose={this.handlePostItemModel} />
              </Model>

              {/* Create Post Drawer */}
              <MainDrawer
                open={this.state.createPost}
                onClose={this.handleCreatePost}
                anchor="right"
              >
                <CreatePost onClose={this.handleCreatePost} />
              </MainDrawer>

              {/* Snakbar Components */}
              <Snackbar
                variant={this.state.variant}
                message={this.state.usermessage}
                open={this.state.open}
                onClose={this.handleSnackbarClose}
                vertical={this.state.vertical}
                horizontal={this.state.horizontal}
              />

              {/* Location Model */}
              <Model
                open={this.state.locationModel}
                onClose={this.handleLocationModel}
              >
                <MyLocationModel
                  setCurrentLocation={true}
                  onClose={this.handleLocationModel}
                  handleCurrentAddAddress={this.handleCurrentAddAddress}
                />
              </Model>

              <style jsx>
                {`
                  .sell-li {
                    font-size: 0.878vw;
                    color: #98a1b9;
                    padding-top: 0.078125vw;
                    font-family: "Open Sans" !important;
                    font-weight: 600;
                    cursor: pointer;
                  }
                  :global(.SearchBtnTooltip_Sec) {
                    width: fit-content !important;
                  }
                  :global(.SearchBtnTooltip_Sec .Msg) {
                    padding: 0.585vw 0.585vw 0 0.585vw;
                    font-size: 0.658vw;
                    color: ${WHITE_COLOR};
                    font-family: "Open Sans" !important;
                    margin: 0;
                  }
                  :global(.SearchBtnTooltip_Sec .Msg span) {
                    font-weight: 600;
                  }
                  :global(.gotIt_btn button) {
                    color: ${THEME_COLOR};
                    padding: 0.219vw 0.366vw;
                    cursor: pointer;
                    border: none;
                    background: ${BG_LightGREY_COLOR};
                    border-radius: 0.146vw;
                  }
                  :global(.gotIt_btn button:hover) {
                    background: ${BG_LightGREY_COLOR};
                  }
                  :global(.gotIt_btn button:focus),
                  :global(.gotIt_btn button:active) {
                    border: none !important;
                    box-shadow: none;
                    background: ${BG_LightGREY_COLOR};
                    outline: 0 !important;
                  }
                  :global(.gotIt_btn button > span) {
                    font-size: 0.512vw;
                    font-family: "Open Sans-SemiBold" !important;
                    color: ${THEME_COLOR};
                    width: fit-content;
                  }
                  :global(.gotIt_btn button > span::first-letter) {
                    text-transform: capitalize;
                  }
                  :global(.footerSec) {
                    padding: 0.585vw;
                  }
                  :global(.hideMsg) {
                    width: fit-content;
                    margin: 0;
                    display: flex;
                    align-items: center;
                  }
                  :global(.hideMsg span) {
                    font-size: 0.585vw;
                    font-family: "Open Sans" !important;
                    color: ${Border_LightGREY_COLOR};
                  }
                  :global(.hideIcon) {
                    width: 8px;
                    margin-right: 3px;
                  }
                  :global(.locationDropdownBtn) {
                    width: 100%;
                  }
                  :global(.locationDropdownBtn > button) {
                    background: transparent !important;
                    border: none;
                    padding: 0 !important;
                    line-height: 0.5;
                    width: 100%;
                  }
                  :global(.locationDropdownBtn > button:active),
                  :global(.locationDropdownBtn > button:focus) {
                    outline: none !important;
                    background: none !important;
                    box-shadow: none !important;
                  }
                  :global(.locationDropdownBtn > .locationDropdownList) {
                    padding: 0 !important;
                    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
                    border-radius: 2px;
                    border: none;
                    margin-top: 0.366vw !important;
                  }
                  :global(.locationDropdownBtn
                      > .locationDropdownList
                      > .dropdownItem) {
                    padding: 0.512vw 0 !important;
                    cursor: pointer;
                  }
                  :global(.locationDropdownList .dropdownItem:hover) {
                    background: ${THEME_COLOR_Opacity1} !important;
                  }
                  :global(.messageDropdownBtn > button) {
                    background: ${WHITE_COLOR} !important;
                    border: none;
                    padding: 0.292vw 1.09375vw !important;
                    line-height: 0.5;
                  }
                  :global(.messageDropdownBtn > button:active),
                  :global(.messageDropdownBtn > button:focus) {
                    outline: none !important;
                    background: none !important;
                    box-shadow: none !important;
                  }
                  :global(.messageDropdownBtn
                      > .messageDropdownList
                      > .dropdownItem) {
                    padding: 0.366vw 0.585vw !important;
                  }
                  :global(.dropdownItem) {
                    padding: 0.146vw 0.366vw !important;
                  }
                  :global(.messageDropdownList .dropdownItem:hover) {
                    background: ${BG_LightGREY_COLOR} !important;
                  }
                  :global(.messageDropdownList) {
                    padding: 0.146vw !important;
                  }
                  .messageDate {
                    font-size: 0.585vw;
                    color: ${GREY_VARIANT_2};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    position: absolute;
                    top: 0;
                    right: 0;
                  }
                  .MessengerInfo {
                    positon: relative;
                    padding: 0 0 0 0.585vw !important;
                  }
                  .messengerManufacturer {
                    font-size: 0.658vw;
                    text-transform: capitalize;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    color: ${GREY_VARIANT_2};
                    white-space: nowrap;
                    overflow: hidden !important;
                    text-overflow: ellipsis;
                    margin: 0;
                  }
                  .messengerName {
                    font-size: 0.732vw;
                    text-transform: capitalize;
                    font-weight: 600;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    color: ${Dark_Blue_Color};
                    margin: 0;
                  }
                  .messageProfilePic {
                    // width: 100%;
                    object-fit: cover !important;
                    border-radius: 50%;
                    height: 2.083vw;
                    width: 2.083vw;
                  }
                  .recentText {
                    font-size: 0.732vw;
                    margin: 0;
                    padding: 0 0 0.219vw 0.219vw;
                    color: ${GREY_VARIANT_2};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                  }
                  :global(.DropdownBtn > button) {
                    background: ${WHITE_COLOR} !important;
                    border: none;
                    line-height: 0.5;
                    padding: 0 0.366vw;
                  }
                  :global(.DropdownBtn > button:active),
                  :global(.DropdownBtn > button:focus) {
                    outline: none !important;
                    background: none !important;
                    box-shadow: none !important;
                  }
                  :global(.DropdownBtn > button:first-child) {
                    padding-right: 0 !important;
                    padding-left: 0 !important;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                  }
                  .mapMarker {
                    width: 0.585vw;
                    margin-bottom: 0.0732vw;
                  }
                  :global(.DropdownBtn > .dropdown-menu) {
                    position: absolute;
                    will-change: transform;
                    right: 0;
                    top: 100% !important;
                    transform: unset !important;
                    left: unset !important;
                    padding: 0 !important;
                    margin: 1.098vw 0 0 0;
                    min-width: 11.713vw;
                  }
                  :global(.DropdownBtn > .dropdown-menu .dropdown-item) {
                    width: 100%;
                    cursor: pointer;
                    padding: 0 !important;
                  }
                  :global(.DropdownBtn > .dropdown-menu .dropdown-item > div) {
                    padding: 0.585vw 0.658vw !important;
                  }
                  .userAcc:hover .userName,
                  .profileAcc:hover .userName {
                    color: ${THEME_COLOR};
                  }
                  .profileAcc {
                    border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
                  }
                  .firstSec {
                    max-width: 1.976vw;
                  }
                  .userAcc:hover .dd_profileIcon {
                    width: 100%;
                    height: 1.903vw;
                    background: url(${(this.props.userProfileData &&
                      this.props.userProfileData.profilePicUrl) ||
                    Profile_Logo});
                    border-radius: 50%;
                    background-repeat: no-repeat;
                    background-size: cover;
                  }
                  :global(.DropdownBtn > .dropdown-menu > :nth-child(2)) {
                    border-bottom: 0.0732vw solid ${BG_LightGREY_COLOR};
                  }
                  :global(.DropdownBtn > .dropdown-menu > :nth-last-child(1)) {
                    border-top: 0.0732vw solid ${BG_LightGREY_COLOR};
                  }
                  // .user_dd_profileIcon{
                  //   width: 100%;
                  //   height: 1.83vw;
                  //   border-radius: 50%;
                  //   object-fit: cover;
                  // }
                  .dd_profileIcon {
                    width: 100%;
                    height: 1.903vw;
                    background: url(${(this.props.userProfileData &&
                      this.props.userProfileData.profilePicUrl) ||
                    Profile_Logo});
                    border-radius: 50%;
                    background-repeat: no-repeat;
                    background-size: cover;
                  }
                  .profilePic {
                    width: 100%;
                    height: 1.903vw;
                    background: url(${(this.props.userProfileData &&
                      this.props.userProfileData.businessLogo) ||
                    Boatzon_ProfilePic});
                    border-radius: 50%;
                    background-repeat: no-repeat;
                    background-size: cover;
                  }
                  .userName {
                    font-size: 0.805vw;
                    text-transform: capitalize;
                    color: ${FONTGREY_COLOR};
                    font-family: "Open Sans-SemiBold" !important;
                    margin: 0;
                  }
                  .myLocation_Sec:hover .myLocationIcon,
                  :global(.locationDropdownBtn
                      > .locationDropdownList
                      :nth-child(1):hover
                      .myLocationIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${My_Location_Icon_Blue});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .nationWide_Sec:hover .nationWideIcon,
                  :global(.locationDropdownBtn
                      > .locationDropdownList
                      :nth-child(2):hover
                      .nationWideIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Flag_Usa_Icon_Blue});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .global_Sec:hover .globalIcon,
                  :global(.locationDropdownBtn
                      > .locationDropdownList
                      :nth-child(3):hover
                      .globalIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Globe_Icon_Blue});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .selectOnMap_Sec:hover .selectMapIcon,
                  :global(.locationDropdownBtn
                      > .locationDropdownList
                      :nth-child(4):hover
                      .selectMapIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Map_Marked_Icon_Blue});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .SellingLink_Sec:hover .sellingIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(3):hover
                      .sellingIcon) {
                    width: 100%;
                    height: 0.622vw;
                    background: url(${Selling_Filled_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .BuyingLink_Sec:hover .buyingIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(4):hover
                      .buyingIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Buying_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .appointmentLink_Sec:hover .appointmentIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(5):hover
                      .appointmentIcon) {
                    width: 100%;
                    height: 0.878vw;
                    background: url(${My_Appointment_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .publicProfileLink_Sec:hover .publicProfileIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(8):hover
                      .publicProfileIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${PublicProfile_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .folloingLink_Sec:hover .followingIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(6):hover
                      .followingIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Saved_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .salesLink_Sec:hover .salesIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(7):hover
                      .salesIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Transaction_History_Blue});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .myAccLink_Sec:hover .myAccIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(9):hover
                      .myAccIcon) {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${MyAcc_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .logoutLink_Sec:hover .logoutIcon,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      :nth-child(10):hover
                      .logoutIcon) {
                    width: 100%;
                    height: 0.732vw;
                    background: url(${Logout_Blue_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .ProfileLink_Sec:hover .linkName,
                  :global(.DropdownBtn
                      > .dropdown-menu
                      > .dropdown-item:hover
                      .linkName) {
                    color: ${THEME_COLOR};
                  }
                  .ProfileLink_Sec:hover .locationOption,
                  :global(.locationDropdownBtn
                      > .locationDropdownList
                      > .dropdown-item:hover
                      .locationOption) {
                    color: ${FONTGREY_COLOR};
                  }
                  :global(.DropdownBtn
                      > .dropdown-menu
                      > .dropdown-item:hover) {
                    background: ${THEME_COLOR_Opacity};
                  }
                  :global(.DropdownBtn > .dropdown-menu > .dropdown-item:focus),
                  :global(.DropdownBtn
                      > .dropdown-menu
                      > .dropdown-item:active) {
                    background: none !important;
                    outline: none !important;
                  }
                  :global(.DropdownBtn > .dropdown-menu button:focus) {
                    background: none !important;
                    outline: none !important;
                  }
                  :global(.dropdown-item.active, .dropdown-item:active) {
                    background: ${LIGHT_THEME_BG} !important;
                  }
                  .buyingIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Buying_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .appointmentIcon {
                    width: 100%;
                    height: 0.878vw;
                    background: url(${My_Appointment_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .myLocationIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${My_Location_Icon_Grey});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .nationWideIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Flag_Usa_Icon_Grey});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .globalIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Globe_Icon_Grey});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .selectMapIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Map_Marked_Icon_Grey});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .sellingIcon {
                    width: 100%;
                    height: 0.622vw;
                    background: url(${Selling_Filled_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .followingIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Saved_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .salesIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${Transaction_History_Grey});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .publicProfileIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${PublicProfile_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .myAccIcon {
                    width: 100%;
                    height: 0.805vw;
                    background: url(${MyAcc_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .logoutIcon {
                    width: 100%;
                    height: 0.732vw;
                    background: url(${Logout_Grey_Icon});
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;
                  }
                  .linkName {
                    font-size: 0.805vw;
                    text-transform: capitalize;
                    color: ${FONTGREY_COLOR};
                    margin: 0 !importnat;
                    font-family: "Open Sans" !important;
                  }
                  .locationOption {
                    font-size: 0.805vw;
                    text-transform: capitalize;
                    color: ${GREY_VARIANT_1};
                    margin: 0 !importnat;
                    font-family: "Open Sans" !important;
                  }
                  .userType {
                    font-size: 0.732vw;
                    text-transform: capitalize;
                    color: ${GREY_VARIANT_2};
                    font-family: "Open Sans" !important;
                  }
                  .messagesPopover {
                    position: absolute;
                    width: 100%;
                    left: 0;
                    margin-top: 0.732vw;
                    align-item: center;
                    background: ${WHITE_COLOR};
                    box-shadow: 0px 0px 2px 0px ${BOX_SHADOW_GREY} !important;
                  }
                  .searchPopover {
                    position: absolute;
                    width: 25.622vw;
                    left: 0;
                    margin-top: 1.366vw;
                    align-item: center;
                    background: ${WHITE_COLOR};
                    box-shadow: 0px 0px 2px 0px ${BOX_SHADOW_GREY} !important;
                  }
                  .appLogo {
                    width: 7.32vw;
                    cursor: pointer;
                  }
                  .searchIcon {
                    width: 1.024vw;
                    object-fit: cover;
                    margin: 0 0.366vw;
                  }
                  .sellIcon {
                    width: 3.001vw;
                    cursor: pointer;
                    margin-bottom: 0.146vw;
                  }
                  .Navbar .chatIcon {
                    width: 1.244vw;
                    cursor: pointer;
                  }
                  .profileLogo {
                    width: 1.244vw;
                    height: 1.317vw;
                    border-radius: 50%;
                    object-fit: cover;
                  }
                  .chevronDown {
                    margin-top: 0.219vw;
                    width: 0.732vw;
                  }
                  .loginIcon {
                    width: 0.951vw;
                    margin: 0 0.292vw 0 0.219vw;
                  }
                  .signupIcon {
                    width: 0.951vw;
                    margin: 0 0.292vw 0 0.219vw;
                  }
                  .proIcon {
                    width: 0.951vw;
                    margin: 0 0.292vw 0 0;
                  }
                  :global(.LoginSec button),
                  :global(.SignUpSec button) {
                    color: #98a1b9 !important;
                    padding: 0 0.585vw;
                    text-transform: capitalize;
                    cursor: pointer;
                    border: none;
                    line-height: 0;
                  }
                  :global(.LoginSec button:hover),
                  :global(.SignUpSec button:hover) {
                    background: none;
                  }
                  :global(.LoginSec button:focus),
                  :global(.LoginSec button:active),
                  :global(.SignUpSec button:focus),
                  :global(.SignUpSec button:active) {
                    border: none !important;
                    box-shadow: none;
                    outline: 0 !important;
                  }
                  :global(.LoginSec button > span),
                  :global(.SignUpSec button > span) {
                    font-size: 0.878vw;
                    font-family: "Open Sans-SemiBold" !important;
                    display: flex;
                    align-items: center;
                    line-height: 0;
                  }
                  .rightSec {
                    max-width: max-content;
                  }
                  .searchBar {
                    margin: 0 9.144vw 0 2.195vw;
                    max-width: 28.53vw;
                    padding-left: 0.732vw;
                    background: ${BG_LightGREY_COLOR};
                    border-top-left-radius: 1.464vw !important;
                    border-bottom-left-radius: 1.464vw !important;
                    border-top-right-radius: 1.464vw !important;
                    border-bottom-right-radius: 1.464vw !important;
                  }
                  .searchBar-height {
                    height: 2.061vw;
                  }
                  .Search_btn {
                    height: inherit;
                    max-width: 3.294vw;
                  }
                  :global(.Search_btn button) {
                    background: ${THEME_COLOR};
                    border: none;
                    width: 100%;
                    min-width: 100%;
                    margin: 0 0 0 auto !important;
                    // height: 2.562vw;
                    height: inherit;
                    padding: 0 0.732vw;
                    border-top-left-radius: 0 !important;
                    border-bottom-left-radius: 0 !important;
                    border-top-right-radius: 1.464vw !important;
                    border-bottom-right-radius: 1.464vw !important;
                  }
                  :global(.Search_btn button:focus),
                  :global(.Search_btn button:active) {
                    background: ${THEME_COLOR};
                    outline: none;
                  }
                  :global(.Search_btn button:hover) {
                    background: ${THEME_COLOR};
                  }
                  .searchInput {
                    width: fit-content !important;
                    margin-left: 0.732vw;
                  }
                  :global(.Navbar .searchInput .inputBox) {
                    display: block;
                    width: 12.811vw;
                    padding: 0 !important;
                    line-height: 1.5;
                    height: 1.83vw;
                    background: ${BG_LightGREY_COLOR};
                    font-size: 0.878vw;
                    border: none;
                    border-right: 0.0732vw solid ${GREY_VARIANT_1};
                    color: ${GREY_VARIANT_1};
                    border-radius: 0;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                  }
                  :global(.Navbar .searchInput .inputBox:focus),
                  :global(.Navbar .searchInput .inputBox:active) {
                    outline: none;
                    box-shadow: none;
                    background: ${BG_LightGREY_COLOR};
                  }
                  :global(.Navbar .searchInput .inputBox::placeholder) {
                    font-size: 0.878vw;
                    font-family: "Open Sans" !important;
                  }
                  .searchInput {
                    padding: 0.366vw 0;
                    width: 100%;
                  }
                  .locationSec {
                    cursor: pointer;
                    padding: 0.366vw 0.732vw;
                    width: 100%;
                  }
                  .nearByLocation {
                    font-size: 0.878vw;
                    margin: 0 0.585vw;
                    color: ${GREY_VARIANT_2};
                    text-transform: capitalize;
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    max-width: 6.222vw;
                    white-space: nowrap;
                    overflow-x: hidden;
                    text-overflow: ellipsis;
                    line-height: 1.5;
                  }
                  .leftChevron {
                    width: 0.439vw;
                    text-align: right;
                    margin-top: 0.146vw;
                  }
                  .mainNavUL {
                    float: right;
                  }
                  .ProfileSec {
                    cursor: pointer;
                    line-height: 0.5;
                    margin-right: 0.732vw;
                  }
                  .ProfileSec span {
                    font-size: 0.805vw;
                    padding: 0 0.366vw;
                    color: ${GREY_VARIANT_2};
                    font-family: "Open Sans-SemiBold" !important;
                    letter-spacing: 0.0219vw !important;
                  }
                `}
              </style>
            </Wrapper>
          );
        }}
      </Context.Consumer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    userProfileData: state.userProfileData,
    allChats: state.allChats,
  };
};

export default connect(mapStateToProps)(Navbar);
