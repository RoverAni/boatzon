import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import NestedList from "../../components/custom-list/nested-list";
import {
  FONTGREY_COLOR,
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  THEME_COLOR,
  Info_Icon_Grey,
  Financing_Grey_Icon,
  Loan_Financing_Icon_White,
  Insurance_White_Icon,
  Insurance_Grey_Icon,
  Loan_Financing_Video,
  Insurance_Video,
  Play_Video_Icon,
} from "../../lib/config";
import LoanFinancingServices from "./loan-financing-services";
import { getUrlSlugs } from "../../lib/url/getSlugs";
import VideoModel from "./video-model";
import Model from "../../components/model/model";
import Router from "next/router";
import InsuranceServices from "./insurance-services";

class QuoteServices extends Component {
  state = {
    activeLink: "",
    loans_financing: "",
    loanfinancingVideo: false,
  };

  // On Component load this called
  componentDidMount = () => {
    this.handleActiveLink();
  };

  handleActiveLink = () => {
    let slugData = getUrlSlugs();
    console.log("asasdasdasd", slugData);

    this.setState(
      {
        activeLink: `${slugData[0]}`,
        openPage: true,
      },
      () => {
        this.setState({
          loans_financing: this.state.activeLink == "insurance" ? false : true,
        });
      }
    );
  };

  // Used to get Current Slug
  handelTabClick = (slug) => {
    console.log("asasdasdasd--->", slug);
    Router.push(`/${slug}`);

    this.handleActiveLink();
  };

  handleListToggle = (name) => {
    let Name = name.replace("_", "-");
    this.handelTabClick(Name);
  };

  handleLoanFinancingVideo = (VideoLabel) => {
    let videoSrc = VideoLabel !== "Loan_Financing" ? `${Insurance_Video}` : `${Loan_Financing_Video}`;
    this.setState({
      loanfinancingVideo: !this.state.loanfinancingVideo,
      videoSrc,
    });
  };

  render() {
    const { buyBoatDetailsData, postBoatDetailsData, userProfileData, userId, productId } = this.props;

    const loan_Financing = (
      <LoanFinancingServices
        buyBoatDetailsData={buyBoatDetailsData}
        userProfileData={userProfileData}
        postBoatDetailsData={postBoatDetailsData}
        userId={userId}
        productId={productId}
      />
    );

    let pageContent = loan_Financing;

    switch (this.state.activeLink) {
      case "loan_financing":
        pageContent = loan_Financing;
        break;
      case "insurance":
        pageContent = (
          <InsuranceServices
            buyBoatDetailsData={buyBoatDetailsData}
            userProfileData={userProfileData}
            postBoatDetailsData={postBoatDetailsData}
            userId={userId}
            productId={productId}
          />
        );
        break;
      default:
        pageContent = loan_Financing;
    }
    return (
      <Wrapper>
        {this.state.openPage ? (
          <div className="QuoteServicesPage">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto py-3">
                <div className="row m-0">
                  <div className="col-3 Tabs pl-0">
                    <div className="position-relative">
                      <h6 className="heading">Boatzon Services </h6>
                      <img src={Info_Icon_Grey} className="infoIcon"></img>
                    </div>
                    <ul className="list-unstyled ServicesTabs">
                      <li>
                        <NestedList
                          serviceslist={true}
                          activeLabel={
                            <p className="m-0">
                              <img src={Loan_Financing_Icon_White} className="loanIcon"></img>
                              <span className="mx-2 activelistLabel">Loans & Financing </span>
                            </p>
                          }
                          inactiveLabel={
                            <p className="m-0">
                              <img src={Financing_Grey_Icon} className="loanIcon"></img>
                              <span className="mx-2 inactivelistLabel">Loans & Financing </span>
                            </p>
                          }
                          open={this.state.loans_financing}
                          onClick={this.handleListToggle.bind(this, "loan_financing")}
                        >
                          <Wrapper className="loan_financingSec">
                            <p className="loanMsg">
                              Would you like a Boatzon <span>Loan Quote</span> to finance or refinance this boat. We offer wholesale loan
                              rates to Boatzon members and will beat any rate.
                            </p>
                            <div className="videoDiv">
                              <div className="videoBlock">
                                <video className="video">
                                  <source src={Loan_Financing_Video} type="video/mp4"></source>
                                </video>
                              </div>
                              <div className="videoIconDiv">
                                <img
                                  src={Play_Video_Icon}
                                  className="videoIcon"
                                  onClick={this.handleLoanFinancingVideo.bind(this, "Loan_Financing")}
                                ></img>
                              </div>
                            </div>
                          </Wrapper>
                        </NestedList>
                        <NestedList
                          serviceslist={true}
                          activeLabel={
                            <p className="m-0">
                              <img src={Insurance_White_Icon} className="loanIcon"></img>
                              <span className="mx-2 activelistLabel">Insurance </span>
                            </p>
                          }
                          inactiveLabel={
                            <p className="m-0">
                              <img src={Insurance_Grey_Icon} className="loanIcon"></img>
                              <span className="mx-2 inactivelistLabel">Insurance </span>
                            </p>
                          }
                          open={!this.state.loans_financing}
                          onClick={this.handleListToggle.bind(this, "insurance")}
                        >
                          <Wrapper className="loan_financingSec">
                            <p className="loanMsg">
                              Would you like a Boatzon <span>Insurance Quote</span> to finance or refinance this boat. We offer wholesale
                              loan rates to Boatzon members and will beat any rate.
                            </p>
                            <div className="videoDiv">
                              <div className="videoBlock">
                                <video className="video">
                                  <source src={Insurance_Video} type="video/mp4"></source>
                                </video>
                              </div>
                              <div className="videoIconDiv">
                                <img
                                  src={Play_Video_Icon}
                                  className="videoIcon"
                                  onClick={this.handleLoanFinancingVideo.bind(this, "Insurance")}
                                ></img>
                              </div>
                            </div>
                          </Wrapper>
                        </NestedList>
                      </li>
                    </ul>
                  </div>
                  <div className="col-9 TabPanel">{pageContent}</div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        {/* Login Model */}
        <Model open={this.state.loanfinancingVideo} onClose={this.handleLoanFinancingVideo}>
          <VideoModel onClose={this.handleLoanFinancingVideo} videoSrc={this.state.videoSrc} />
        </Model>
        <style jsx>
          {`
            .activelistLabel {
              font-size: 0.951vw;
              opacity: 1;
              padding: 0 0.732vw;
              cursor: pointer;
              margin: 0 !important;
              color: ${WHITE_COLOR} !important;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letterspacing: 0.2px !important;
            }
            .loanIcon {
              width: 1.171vw;
              margin-left: 0.585vw;
              margin-bottom: 0.219vw;
            }
            .inactivelistLabel {
              font-size: 0.951vw;
              opacity: 1;
              margin: 0 !important;
              padding: 0 0.732vw;
              cursor: pointer;
              color: ${FONTGREY_COLOR} !important;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letterspacing: 0.2px !important;
            }
            .TabPanel {
              background: ${WHITE_COLOR};
              padding: 1.098vw;
            }
            .infoIcon {
              width: 0.878vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .heading {
              font-size: 1.317vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
            .loan_financingSec {
              border-bottom-right-radius: 0.219vw;
              border-bottom-left-radius: 0.219vw;
            }
            .loanMsg {
              font-size: 0.732vw;
              color: ${BG_LightGREY_COLOR};
              background: ${THEME_COLOR};
              padding: 0.732vw;
              font-family: "Open Sans" !important;
              margin: 0;
              letter-spacing: 0.0219vw !important;
              font-weight: lighter;
            }
            .loanMsg span {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              font-weight: 700;
              color: ${BG_LightGREY_COLOR};
            }
            .Tabs {
              background: ${BG_LightGREY_COLOR};
            }
            .QuoteServicesPage {
              background: ${BG_LightGREY_COLOR};
            }
            .videoDiv {
              cursor: pointer;
              padding: 0.732vw;
              background: ${THEME_COLOR};
              position: relative;
            }
            .videoBlock {
              width: 100%;
              height: 100%;
              display: flex;
              justify-content: center;
              align-items: center;
            }
            .videoBlock .video {
              width: 100%;
            }
            .videoIconDiv {
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            .videoIcon {
              width: 2.928vw;
              object-fit: cover;
              display: block;
              z-index: 1;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default QuoteServices;
