import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import ButtonComp from "../../components/button/button";
import {
  GREY_VARIANT_10,
  WHITE_COLOR,
  Long_Arrow_Left_White_Icon,
  THEME_COLOR,
  GREY_VARIANT_3,
  Quote_Sale_Plan_Label,
  APP_LOGO,
  FONTGREY_COLOR_Dark,
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  GREEN_COLOR,
  Plan_Card_Bg,
  Financing_Dark_Grey_Icon,
  Insurance_Dark_Grey_Icon,
} from "../../lib/config";
import ReactToPrint from "react-to-print";

const printBtn = (
  <div className="print_btn">
    <ButtonComp>Print</ButtonComp>
  </div>
);

class QuotePage extends Component {
  render() {
    const {
      loan_type,
      loan_amount,
      terms_year,
      replacement_cost,
      hull_deduction,
      liability_limit,
      loanQuote,
      insuranceQuote,
    } = this.props.inputpayload;
    return (
      <Wrapper>
        <div className="position-relative">
          <ReactToPrint
            trigger={() => printBtn}
            content={() => this.componentRef}
            pageStyle={{ width: "713px" }}
          />
          <div className="pt-3 viewQuotesPage">
            <div
              className="col-12 termsSec p-0 pt-3"
              ref={(el) => (this.componentRef = el)}
            >
              <div className="row m-0">
                <div className="col-6 p-0">
                  <h5 className="title">
                    <img src={Financing_Dark_Grey_Icon}></img> Finance Quote
                  </h5>
                  <div className="planCard text-center mr-2">
                    <img src={APP_LOGO} className="appLogo"></img>
                    <div className="quoteDiv">
                      <p className="discountMsg">
                        Your Discounted Boatzon Quote:
                      </p>
                      <div className="d-flex justify-content-center align-items-end">
                        <h3 className="currentPlanPrice">${loanQuote}</h3>
                        <span className="planDuration">/mo</span>
                      </div>
                      <p className="beforePriceMsg">
                        Before discounts:{" "}
                        <span className="beforePlanPrice">
                          $3500<span>/Mo</span>
                        </span>
                      </p>
                    </div>
                    <p className="planMsg">
                      This your Boatzon estimated loan quote. One of our agents
                      will contact you to see if we can obtain further
                      discounts.
                    </p>
                    <img src={Quote_Sale_Plan_Label} className="labelImg"></img>
                  </div>
                  <div className="planSummary mr-2">
                    <div className="pb-3">
                      <h6 className="heading">Loan Information</h6>
                      <table>
                        <tr>
                          <td className="label">Loan Type</td>
                          <td className="value">
                            {loan_type && loan_type.value}
                          </td>
                        </tr>
                        <tr>
                          <td className="label">Loan Amount</td>
                          <td className="value">${loan_amount}</td>
                        </tr>
                        <tr>
                          <td className="label">Term Requested</td>
                          <td className="value">
                            {terms_year && terms_year.value}
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="col-6 p-0">
                  <h5 className="title ml-2">
                    <img src={Insurance_Dark_Grey_Icon}></img> Insurance Quote
                  </h5>

                  <div className="planCard text-center ml-2">
                    <img src={APP_LOGO} className="appLogo"></img>
                    <div className="quoteDiv">
                      <p className="discountMsg">
                        Your Discounted Boatzon Quote:
                      </p>
                      <div className="d-flex justify-content-center align-items-end">
                        <h3 className="currentPlanPrice">${insuranceQuote}</h3>
                        <span className="planDuration">/mo</span>
                      </div>
                      <p className="beforePriceMsg">
                        Before discounts:{" "}
                        <span className="beforePlanPrice">
                          $275<span>/Mo</span>
                        </span>
                      </p>
                    </div>

                    <p className="planMsg">
                      This your Boatzon estimated insurance quote. One of our
                      agents will contact you to see if we can obtain further
                      discounts.
                    </p>
                    <img src={Quote_Sale_Plan_Label} className="labelImg"></img>
                  </div>
                  <div className="planSummary ml-2">
                    <div className="pb-3">
                      <h6 className="heading">Insurance Information</h6>
                      <table>
                        <tr>
                          <td className="label">Replacement Cost</td>
                          <td className="value">${replacement_cost}</td>
                        </tr>
                        <tr>
                          <td className="label">Deductible Requested</td>
                          <td className="value">
                            {hull_deduction && hull_deduction.value}
                          </td>
                        </tr>
                        <tr>
                          <td className="label">Liability Limit Requested</td>
                          <td className="value">
                            ${liability_limit && liability_limit.value}
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 p-0 pt-3">
              <div className="d-flex align-items-center justify-content-between">
                <div className="back_btn">
                  <ButtonComp onClick={this.props.handleBack}>
                    <img
                      src={Long_Arrow_Left_White_Icon}
                      className="backArrow"
                    ></img>{" "}
                    Back
                  </ButtonComp>
                </div>
                {/* <div className="next_btn">
                <ButtonComp onClick={this.props.handleNext}>
                  Next
                  <img
                    src={Long_Arrow_Right_White_Icon}
                    className="nextArrow"
                  ></img>{" "}
                </ButtonComp>
              </div> */}
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .termsSec {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              margin: 0;
              font-weight: 600;
              padding: 0 0 0.732vw 0;
            }
            .title img {
              height: 1.171vw;
              object-fit: cover;
              margin-right: 0.366vw;
            }
            .planCard {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              box-sizing: border-box;
              border-radius: 0.325vw;
              position: relative;
              padding: 1.098vw 0;
            }
            .labelImg {
              position: absolute;
              top: -1.098vw;
              left: -1.098vw;
              width: 7.32vw;
            }
            .appLogo {
              width: 6.588vw;
              object-fit: cover;
            }
            .quoteDiv {
              background: url(${Plan_Card_Bg});
              background-repeat: no-repeat;
              background-size: cover;
              width: 100%;
              padding: 0.732vw;
              margin: 0.732vw 0;
            }
            .discountMsg {
              font-size: 0.878vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${GREEN_COLOR};
              margin: 0;
            }
            .currentPlanPrice {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 2.049vw;
              color: ${GREEN_COLOR};
              margin: 0;
            }
            .planDuration {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.317vw;
              color: ${GREEN_COLOR};
            }
            .beforePriceMsg {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0;
            }
            .beforePlanPrice {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR_Dark};
            }
            .beforePlanPrice span {
              font-size: 0.805vw;
            }
            .planMsg {
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              margin: 0 auto;
              max-width: 80%;
              line-height: 1;
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0;
              font-weight: 600;
              padding: 0 0 0.366vw 0;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .planSummary {
              padding: 1.464vw 0 2.928vw 0;
            }
            .planSummary table {
              width: 100%;
            }
            .planSummary table tr .label {
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              padding-top: 0.366vw;
            }
            .planSummary table tr .value {
              font-family: "Open Sans-Semibold" !important;
              font-size: 0.732vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              padding-top: 0.366vw;
              text-align: right;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.print_btn) {
              position: absolute;
              top: 10px;
              right: 10px;
            }
            :global(.print_btn button) {
              width: fit-content;
              padding: 0;
              background: none;
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.print_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
            }
            :global(.print_btn button:focus),
            :global(.print_btn button:active) {
              background: none;
              outline: none;
              box-shadow: none;
            }
            :global(.print_btn button:hover) {
              background: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default QuotePage;
