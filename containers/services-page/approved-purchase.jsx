import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Successful_SignUp,
  GREY_VARIANT_2,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_3,
  FONTGREY_COLOR,
  GREY_VARIANT_10,
  GREY_VARIANT_1,
  WHITE_COLOR,
  THEME_COLOR,
  Long_Arrow_Right_White_Icon,
  Long_Arrow_Left_White_Icon,
  Orange_Color,
  Dark_Green_Color,
  GREEN_COLOR,
  Yellow_Color,
  Red_Color_1,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";

class ApprovedPurchase extends Component {
  state = {
    Score: 450,
  };

  render() {
    const { insuranceTab } = this.props;
    return (
      <Wrapper>
        <div className="py-3 approvedPurchasePage">
          <div className="col-12 termsSec">
            <div className="row m-0">
              <div className="col-8 p-0 mx-auto">
                <div className="text-center">
                  <img
                    src={Successful_SignUp}
                    className="getApprovalIcon"
                  ></img>
                  <h5 className="heading">Congratulations!</h5>
                  <h6 className="caption">You’ve been prequalified!</h6>
                </div>
                <div className="section">
                  <div className="d-flex align-items-center justify-content-between">
                    <p className="label">Powered by: FicoScore8</p>
                    <p className="label text-right">
                      Experian Data: Feb 4, 2020
                    </p>
                  </div>

                  <div className="row m-0 mt-4 mb-3 d-flex align-items-center justify-content-evenly">
                    <div className="col-1 p-0 initialScoreDiv">
                      <p className="initialScore">300</p>
                    </div>
                    <div className="col p-0">
                      <div className="d-flex align-items-center justify-content-around">
                        <div
                          className={
                            this.state.Score >= 300 && this.state.Score < 410
                              ? "firstScoreDiv currentScoreDiv"
                              : "firstScoreDiv"
                          }
                        >
                          <p className="cardLabel">
                            {this.state.Score >= 300 && this.state.Score < 410
                              ? "Poor"
                              : " "}
                          </p>
                        </div>
                        <div
                          className={
                            this.state.Score >= 410 && this.state.Score < 520
                              ? "secondScoreDiv currentScoreDiv"
                              : "secondScoreDiv"
                          }
                        >
                          <p className="cardLabel">
                            {this.state.Score >= 410 && this.state.Score < 520
                              ? "Fair"
                              : " "}
                          </p>
                        </div>
                        <div
                          className={
                            this.state.Score >= 520 && this.state.Score < 630
                              ? "thirdScoreDiv currentScoreDiv"
                              : "thirdScoreDiv"
                          }
                        >
                          <p className="cardLabel">
                            {this.state.Score >= 520 && this.state.Score < 630
                              ? "Good"
                              : " "}
                          </p>
                        </div>
                        <div
                          className={
                            this.state.Score >= 630 && this.state.Score < 740
                              ? "fourthScoreDiv currentScoreDiv"
                              : "fourthScoreDiv"
                          }
                        >
                          <p className="cardLabel">
                            {this.state.Score >= 630 && this.state.Score < 740
                              ? "Very Good"
                              : " "}
                          </p>
                        </div>
                        <div
                          className={
                            this.state.Score >= 740 && this.state.Score < 850
                              ? "lastScoreDiv currentScoreDiv"
                              : "lastScoreDiv"
                          }
                        >
                          <p className="cardLabel">
                            {this.state.Score >= 740 && this.state.Score < 850
                              ? "Exceptional"
                              : " "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-1 p-0 finalScoreDiv">
                      <p className="finalScore text-right">850</p>
                    </div>
                  </div>

                  <div className="progressSec">
                    <div className="progessTable">
                      <table className="w-100">
                        <tr className="tableRow">
                          <td className="key">Interest Rate (APR)</td>
                          <td className="value text-right">7.40%</td>
                        </tr>
                        <tr className="tableRow">
                          <td className="key">Cash Down</td>
                          <td className="value text-right">$0</td>
                        </tr>
                        <tr className="tableRow">
                          <td className="key">Monthly Payment</td>
                          <td className="value text-right">$369</td>
                        </tr>
                        <tr className="tableRow">
                          <td className="key">Term (Months)</td>
                          <td className="value text-right">48</td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 p-0 pt-3">
            <div className="d-flex align-items-center justify-content-between">
              <div className="back_btn">
                <ButtonComp onClick={this.props.handleBackGetPreApproval}>
                  <img
                    src={Long_Arrow_Left_White_Icon}
                    className="backArrow"
                  ></img>{" "}
                  Back
                </ButtonComp>
              </div>
              <div className="next_btn">
                <ButtonComp
                  onClick={
                    insuranceTab
                      ? this.props.handleNext
                      : this.props.handleAddInsuranceTab
                  }
                >
                  Next
                  <img
                    src={Long_Arrow_Right_White_Icon}
                    className="nextArrow"
                  ></img>{" "}
                </ButtonComp>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .getApprovalIcon {
              width: 4.392vw;
              object-fit: contain;
              margin-top: 1.098vw;
            }
            .termsSec {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.366vw;
              padding: 0.732vw 0 0 0;
            }
            .caption {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              line-height: 1.6;
              letter-spacing: 0.0219vw !important;
              max-width: 80%;
              margin: 0 auto 0.585vw auto;
            }
            .section {
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 0.732vw 0 4.464vw 0;
            }
            .label {
              font-size: 0.658vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0 0 0.585vw 0;
              line-height: 1.2;
            }
            .progressSec {
              margin: 1.098vw 0;
            }
            .progessTable {
              background: linear-gradient(0deg, #f1f3f5, #f1f3f5), #c4c4c4;
              margin: 1.098vw 0;
              padding: 0.585vw;
            }
            .key {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0.732vw 0.878vw;
              line-height: 1;
            }
            .value {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              line-height: 1;
              padding: 0.732vw 0.878vw;
              font-weight: 600;
            }
            .initialScoreDiv,
            .finalScoreDiv {
              max-width: fit-content;
            }

            .initialScore,
            .finalScore {
              font-size: 0.951vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              line-height: 1;
            }
            .initialScore {
              padding: 0 5px 0 0;
            }
            .finalScore {
              padding: 0 0 0 5px;
            }
            .cardLabel {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0;
              line-height: 1;
              position: relative;
              z-index: 1;
              bottom: 1px;
            }
            .firstScoreDiv {
              width: 19%;
              height: 12px;
              border-radius: 1px;
              background: ${Red_Color_1};
              position: relative;
            }
            .secondScoreDiv {
              width: 19%;
              height: 12px;
              border-radius: 1px;
              background: ${Orange_Color};
              position: relative;
            }
            .thirdScoreDiv {
              width: 19%;
              height: 12px;
              border-radius: 1px;
              background: ${Yellow_Color};
              position: relative;
            }
            .fourthScoreDiv {
              width: 19%;
              height: 12px;
              border-radius: 1px;
              opacity: 0.9;
              background: ${GREEN_COLOR};
              position: relative;
            }
            .lastScoreDiv {
              width: 19%;
              height: 12px;
              border-radius: 1px;
              opacity: 0.9;
              background: ${Dark_Green_Color};
              position: relative;
            }
            .currentScoreDiv {
              padding: 3px 6px;
              height: 18px;
              width: 20%;
              text-align: center;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default ApprovedPurchase;
