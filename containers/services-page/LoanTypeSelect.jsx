import React, { useState } from "react";
import {
  FONTGREY_COLOR,
  WHITE_COLOR,
  Card1Active,
  Card1NotActive,
  Card2Active,
  Card2NotActive,
  Card3Active,
  Card3NotActive,
  Card4Active,
  Card4NotActive,
  THEME_COLOR,
  Long_Arrow_Right_White_Icon,
} from "../../lib/config";

const LoanTypeSelect = ({ handleNext, saveTypeOfLabel }) => {
  const [cards] = useState([
    {
      header: "Boat Loan",
      subHeader: "Boat loans up to 5 million From 4.29% APR with AutoPay Pre-Qualified in 2 minutes",
      active: Card1Active,
      notActive: Card1NotActive,
      keyword: "Boat",
    },
    {
      header: "Engine or Repower Loan",
      subHeader: "Engines loans up to $100,000 Funding as fast as 24 hours Pre-Qualified in 2 minutes",
      active: Card2Active,
      notActive: Card2NotActive,
      keyword: "Engine",
    },
    {
      header: "Trailer Loan",
      subHeader: "Trailer loans up to $100,000 Funding as fast as 24 hours Pre-Qualified in 2 minutes",
      active: Card3Active,
      notActive: Card3NotActive,
      keyword: "Trailer",
    },
    {
      header: "Loans to Products, Electronics, Service and Other",
      subHeader: "Electronic, Product and Other loans up to $100,000 Funding as fast as 24 hours Pre-Qualified in 2 minutes",
      active: Card4Active,
      notActive: Card4NotActive,
      keyword: "Product",
    },
  ]);
  const [activeCard, setActiveCard] = useState(0);
  return (
    <section>
      <div className="header1">What type of loan do you need ?</div>
      <div className="subHeader">Get Pre-Qualified in 2 minutes and received funding as fast as 24 hours.</div>
      <div className="col-12 px-0">
        <div className="row mx-0" style={{ justifyContent: "space-evenly" }}>
          {cards.map((k, i) => (
            <div
              key={k.header}
              className={activeCard == i ? "activeCard-color" : "notActiveCard-color"}
              onClick={() => {
                setActiveCard(i);
                saveTypeOfLabel(k.keyword);
              }}
            >
              <div>
                <div className={activeCard == i ? "activeCard-circle" : "notActiveCard-circle"}>
                  <img src={activeCard == i ? k.active : k.notActive} />
                </div>
              </div>
              <div className={activeCard == i ? "activeCard-header" : "notActiveCard-header"}>{k.header}</div>
              <div className={activeCard == i ? "activeCard-subHeader" : "notActiveCard-subHeader"}>{k.subHeader}</div>
            </div>
          ))}
        </div>
      </div>
      <div className="button-section">
        <button className="cancel-btn">Cancel</button>
        <button className="next-btn" onClick={handleNext}>
          <span>
            Next <img src={Long_Arrow_Right_White_Icon} className="nextArrow"></img>
          </span>
        </button>
      </div>
      <style>
        {`
        .button-section {
            margin-top: 1.0416vw;
            padding: 0.9375vw;
            display: flex;
            justify-content: space-between;
        }
        .next-btn {
            position: relative;
            background: ${THEME_COLOR};
            padding: 0.3645vw 2.0833vw;
            border-radius: 2px;
            border: none;
            outline: none;
        }
        .next-btn span {
            font-size: 0.833vw;
            font-weight: 600;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            color: ${WHITE_COLOR};
        }
        .next-btn span img {
            position: absolute;
            right: 0.625vw;
            top: 0.625vw;
        }
        .cancel-btn {
            background: #C0C7D9;
            font-size: 0.833vw;
            font-weight: 600;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            color: ${WHITE_COLOR};
            padding: 0.3645vw 2.0833vw;
            border-radius: 2px;
            border: none;
            outline: none;
        }

        .activeCard-circle {
            height:  4.21875vw;
            width:  4.21875vw;
            border-radius: 50%;
            background: #87b9df;
            margin-bottom: 0.625vw;
            display: flex;
            justify-content:center;
            align-items:center;
        }
        .notActiveCard-circle {
            height:  4.21875vw;
            width:  4.21875vw;
            border-radius: 50%;
            background: #EAEAEA;
            margin-bottom: 0.625vw;
            display: flex;
            justify-content:center;
            align-items:center;
        }
        .activeCard-color {
            border-radius: 2px;
            padding: 1.0416vw;
            background: #378BCB;
            display: flex;
            align-items: center;
            cursor: pointer;
            flex-direction: column;
            border: 2px solid  #378BCB;
            width: 23%;
        }
        .notActiveCard-color {
            border-radius: 2px;
            padding: 1.0416vw;
            background: #FBFBFB;  
            display: flex;
            align-items: center;
            cursor: pointer;
            flex-direction: column;
            border: 2px solid #D3D9E7;
            width: 23%;
        }
        .activeCard-header {
            color: ${WHITE_COLOR};
            font-weight: 600;
            font-family: "Museo-Sans-Cyrl-Semibold";
            font-size: 0.833vw;
            margin-bottom: 0.625vw;
            text-align:center
        }
        .notActiveCard-subHeader {
            color: #98A1B9;
            // font-weight: 600;
            font-family: "Open Sans";
            font-size: 0.625vw;
            text-align:center
        }
        .notActiveCard-header {
            color: ${FONTGREY_COLOR};
            font-weight: 600;
            font-family: "Museo-Sans-Cyrl-Semibold";
            font-size: 0.833vw;
            margin-bottom: 0.625vw;
            text-align:center
        }
        .activeCard-subHeader {
            color: ${WHITE_COLOR};
            // font-weight: 600;
            font-family: "Open Sans";
            font-size: 0.625vw;
            text-align:center
        }
          .header1 {
              color: ${FONTGREY_COLOR};
              font-weight: 600;
              font-family: "Museo-Sans-Cyrl-Semibold";
              font-size: 0.9375vw;
              padding-top: 2.34375vw;
          }
          .subHeader {
              color: #98A1B9;
              font-family: "Open Sans";
              font-size:0.7291vw;
              padding-top: 0.5208vw;
              padding-bottom: 1.302vw;
          }
          `}
      </style>
    </section>
  );
};

export default LoanTypeSelect;
