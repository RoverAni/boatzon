import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { Close_White_Icon } from "../../lib/config";

class VideoModel extends Component {
  componentDidMount() {
    var vid = document.getElementById("myVideo");
    vid.autoplay = true;
    vid.load();
  }
  render() {
    return (
      <Wrapper>
        <div className="d-flex position-relative videoModel">
          <video className="video" id="myVideo" controls autoPlay>
            <source src={this.props.videoSrc} type="video/mp4"></source>
          </video>
          <img
            src={Close_White_Icon}
            className="closeIcon"
            onClick={this.props.onClose}
          ></img>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .video {
              width: 100%;
            }
            .videoModel {
              width: 43.923vw;
              cursor: pointer;
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              right: 0.732vw;
              width: 0.805vw;
              cursor: pointer;
              display: none;
              z-index: 1;
            }
            .videoModel:hover .closeIcon {
              display: block;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default VideoModel;
