import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Services_Banner,
  WHITE_COLOR,
  Problem_Icon,
  FONTGREY_COLOR,
  Solution_Icon,
  Red_Color,
  GREEN_COLOR,
  Insurance_Bg,
  Tick_Bulletpoint_Icon,
  Services_Footer_Banner,
  Loan_Financing_Video,
  Play_Video_Icon,
  Insurance_Video,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import QuoteServices from "./quote-services";
import Model from "../../components/model/model";
import VideoModel from "./video-model";

class ServicesPage extends Component {
  state = {
    currentScreen: "",
    loanfinancingVideo: false,
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleQuoteServices = () => {
    this.updateScreen(<QuoteServices />);
  };

  handleLoanFinancingVideo = (VideoLabel) => {
    let videoSrc =
      VideoLabel !== "Loan_Financing"
        ? `${Insurance_Video}`
        : `${Loan_Financing_Video}`;
    this.setState({
      loanfinancingVideo: !this.state.loanfinancingVideo,
      videoSrc,
    });
  };
  render() {
    return (
      <Wrapper>
        {!this.state.currentScreen ? (
          <Wrapper>
            <div className="ServicesBanner_Sec">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div>
                    <h2 className="bannerHeading">Boatzon Services</h2>
                    <h6 className="bannerCaption">
                      Changing the way we conduct and process boat and marine
                      transactions
                    </h6>
                  </div>
                </div>
              </div>
            </div>
            <div className="container p-md-0">
              <div className="screenWidth mx-auto">
                <div className="my-5 py-3">
                  <h4 className="heading">
                    Boatzon aims to not only deliver a new experience to the
                    marine and boating industry, but we wanted to change how the
                    industry transacted.
                  </h4>
                  <div className="m-0 row py-4">
                    <div className="col-5 p-0">
                      <div className="d-flex">
                        <img src={Problem_Icon} className="problemIcon"></img>
                        <div>
                          <h6 className="problemTitle py-2">
                            Oops! This is Problem
                          </h6>
                          <p className="problemDesc">
                            Getting a boat loan or even insurance is still a
                            cumbersome process and many boaters are in the dark
                            on how to inquire or obtain these services from
                            credible companies and at competitive rates.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-7">
                      <div className="d-flex">
                        <img src={Solution_Icon} className="solutionIcon"></img>
                        <div>
                          <h6 className="solutionTitle py-2">
                            One Great Solution
                          </h6>
                          <p className="solutionDesc">
                            Boatzon is its own insurance and finance agency,
                            providing wholesale rates to boat buyers. Boatzon
                            allows you to purchase a boat online, much like
                            purchasing any product on the Internet in a few easy
                            steps. Combined with services like live video
                            appointments, we enhance your boat or product buying
                            experience to new levels.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="boatzonInsurance_Sec row mx-0 align-items-center my-4 py-5">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto py-4">
                  <div className="row m-0 align-items-center">
                    <div className="col-5 p-0">
                      <div className="videoDiv">
                        <div className="videoBlock">
                          <video className="video">
                            <source
                              src={Loan_Financing_Video}
                              type="video/mp4"
                            ></source>
                          </video>
                        </div>
                        <div className="videoIconDiv">
                          <img
                            src={Play_Video_Icon}
                            className="videoIcon"
                            onClick={this.handleLoanFinancingVideo.bind(
                              this,
                              "Insurance"
                            )}
                          ></img>
                        </div>
                      </div>
                    </div>
                    <div className="col-7 content_Sec">
                      <h4 className="heading">Boatzon Insurance</h4>
                      <p className="insuranceDesc">
                        Progressive, AllState, etc….they are all great companies
                        and offer an umbrella of insurance services. However,
                        their boat insurance rates are not competitive. There
                        are also a ton of resellers out there, but most are just
                        reselling these companies as well.
                      </p>
                      <p className="insuranceDesc">
                        At Boatzon, we established our own insurance company to
                        specialize in only boats to deliver wholesale rates to
                        our members. Whether your looking for a policy on a new
                        boat or would like to save money on your existing boat
                        insurance policy, we will offer you the lowest rate
                        possible.
                      </p>
                      <div className="insurance_btn">
                        <ButtonComp>Get an Insurance quote now</ButtonComp>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container p-md-0">
              <div className="screenWidth mx-auto py-5 my-4">
                <div className="row m-0 align-items-center my-3">
                  <div className="col-7 p-0 financingContent_Sec">
                    <h4 className="heading">Boatzon Loans & Financing</h4>
                    <p className="financingDesc">
                      Boat financing is still cumbersome to many boat owners and
                      sometimes not achievable because of lack of knowledge.
                      Secondly, boat owners are paying very high loan rates.
                      Potential boat owners combat options of:
                    </p>
                    <ul className="list-unstyled financingOptions_List">
                      <li>
                        <div className="row m-0 align-items-center">
                          <div className="col-1 p-0 tickIconSec">
                            <img
                              src={Tick_Bulletpoint_Icon}
                              className="tickIcon"
                            ></img>
                          </div>
                          <div className="col-11 p-0">
                            <p className="financingPoints">
                              Should I contact my local bank for a loan?
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="row m-0 align-items-center">
                          <div className="col-1 p-0 tickIconSec">
                            <img
                              src={Tick_Bulletpoint_Icon}
                              className="tickIcon"
                            ></img>
                          </div>
                          <div className="col-11 p-0">
                            <p className="financingPoints">
                              Should I search for a specialty bank online? Is
                              this really a bank or broker?
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="row m-0 align-items-center">
                          <div className="col-1 p-0 tickIconSec">
                            <img
                              src={Tick_Bulletpoint_Icon}
                              className="tickIcon"
                            ></img>
                          </div>
                          <div className="col-11 p-0">
                            <p className="financingPoints">
                              Should I fill out another form online?
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="row m-0 align-items-center">
                          <div className="col-1 p-0 tickIconSec">
                            <img
                              src={Tick_Bulletpoint_Icon}
                              className="tickIcon"
                            ></img>
                          </div>
                          <div className="col-11 p-0">
                            <p className="financingPoints">
                              Are all these sites checking my credit and hurting
                              my credit score?
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="row m-0 align-items-center">
                          <div className="col-1 p-0 tickIconSec">
                            <img
                              src={Tick_Bulletpoint_Icon}
                              className="tickIcon"
                            ></img>
                          </div>
                          <div className="col-11 p-0">
                            <p className="financingPoints">
                              Should I get financing through a boat dealer? Who
                              is the lender via the dealer? Is the dealer adding
                              points to my loan?
                            </p>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <p className="financingDesc">
                      Boatzon is the first integrated and simple process for
                      securing a loan on a boat or a marine product such as an
                      engine. As the finance institution, we offer full
                      transparency and wholesale loan rates, and we can get you
                      preapproved for a boat or product you are looking at. Yep,
                      you can buy a boat directly on Boatzon.
                    </p>
                    <div className="financing_btn">
                      {/* <CustomLink href="/services/loans-financing"> */}
                      <ButtonComp>Get a Boat loan quote now</ButtonComp>
                      {/* </CustomLink> */}
                    </div>
                  </div>
                  <div className="col-5 p-0">
                    <div className="videoDiv">
                      <div className="videoBlock">
                        <video className="video">
                          <source
                            src={Insurance_Video}
                            type="video/mp4"
                          ></source>
                        </video>
                      </div>
                      <div className="videoIconDiv">
                        <img
                          src={Play_Video_Icon}
                          className="videoIcon"
                          onClick={this.handleLoanFinancingVideo.bind(
                            this,
                            "Loan_Financing"
                          )}
                        ></img>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="ServicesFooterBanner_Sec">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div className="row m-0 my-4 py-4">
                    <div className="col-7 p-0 py-3">
                      <h2 className="bannerFooterHeading">
                        More services to come
                      </h2>
                      <h6 className="bannerFooterCaption">
                        We don’t want to leak any news, but we have more
                        innovative and exciting services coming to Boatzon soon.
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Wrapper>
        ) : (
          this.state.currentScreen
        )}

        {/* Login Model */}
        <Model
          open={this.state.loanfinancingVideo}
          onClose={this.handleLoanFinancingVideo}
        >
          <VideoModel
            onClose={this.handleLoanFinancingVideo}
            videoSrc={this.state.videoSrc}
          />
        </Model>

        <style jsx>
          {`
            .videoDiv {
              width: 100%;
              height: 100%;
              cursor: pointer;
              position: relative;
            }
            .videoBlock {
              width: 100%;
              height: 100%;
              display: flex;
              justify-content: center;
              align-items: center;
            }
            .videoBlock .video {
              width: 100%;
            }
            .videoIconDiv {
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            .videoIcon {
              width: 2.928vw;
              object-fit: cover;
              display: block;
              z-index: 1;
            }
            .ServicesFooterBanner_Sec {
              background-image: url(${Services_Footer_Banner});
              min-height: 23.06vw;
              background-repeat: no-repeat;
              background-size: cover;
              position: relative;
              display: flex;
              flex-direction: row-reverse;
              align-items: flex-end;
            }
            .ServicesBanner_Sec {
              background-image: url(${Services_Banner});
              min-height: 25.622vw;
              background-repeat: no-repeat;
              background-size: cover;
              position: relative;
              display: flex;
              flex-direction: row-reverse;
              align-items: center;
            }
            .boatzonInsurance_Sec {
              background-image: url(${Insurance_Bg});
              min-height: 28.55vw;
              background-repeat: no-repeat;
              background-size: cover;
              position: relative;
            }
            .bannerFooterHeading {
              font-size: 2.342vw;
              color: ${WHITE_COLOR};
              text-transform: uppercase;
              font-family: "Museo-Sans-Cyrl-Bolder" !important;
            }
            .bannerHeading {
              font-size: 2.928vw;
              color: ${WHITE_COLOR};
              max-width: 14.641vw;
              text-transform: uppercase;
              font-family: "Museo-Sans-Cyrl-Bolder" !important;
            }
            .bannerFooterCaption {
              font-size: 1.024vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .bannerCaption {
              font-size: 1.171vw;
              color: ${WHITE_COLOR};
              max-width: 27.086vw;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0219vw !important;
            }
            .heading {
              font-size: 1.317vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 1.098vw;
              font-family: "Museo-Sans" !important;
            }
            .problemIcon {
              width: 2.342vw !important;
              height: 2.342vw !important;
              margin-right: 0.878vw;
              margin-top: 0;
            }
            .problemTitle {
              font-size: 1.244vw;
              color: ${Red_Color};
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
            }
            .problemDesc,
            .solutionDesc,
            .insuranceDesc {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
            }
            .problemDesc:first-letter,
            .solutionDesc:first-letter,
            .insuranceDesc:first-letter {
              text-transform: capitalize;
            }
            .solutionIcon {
              width: 2.342vw !important;
              height: 2.342vw !important;
              margin-right: 0.878vw;
              margin-top: 0;
            }
            .solutionTitle {
              font-size: 1.244vw;
              color: ${GREEN_COLOR};
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
            }
            .insuranceImg,
            .loanFinancing_Img {
              width: 100%;
              object-fit: cover;
            }
            :global(.insurance_btn button),
            :global(.financing_btn button) {
              width: 45%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.585vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.insurance_btn button span),
            :global(.financing_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.insurance_btn button:focus),
            :global(.insurance_btn button:active),
            :global(.financing_btn button:focus),
            :global(.financing_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.insurance_btn button:hover),
            :global(.financing_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .content_Sec {
              padding-left: 3.733vw;
            }
            .financingOptions_List li {
              padding: 0.366vw 0;
            }
            .financingPoints {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              margin: 0;
            }
            .financingDesc {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              opacity: 0.85;
              font-family: "Museo-Sans" !important;
            }
            .financingDesc:first-letter,
            .financingPoints:first-letter {
              text-transform: capitalize;
            }
            .tickIcon {
              width: 0.732vw;
              margin-right: 0.732vw;
            }
            .tickIconSec {
              max-width: 2.196vw;
            }
            .financingContent_Sec {
              padding-right: 2.781vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ServicesPage;
