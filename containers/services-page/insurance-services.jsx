import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  Personal_Info_Active_Icon,
  Personal_Info_Inactive_Icon,
  Boat_Info_Active_Icon,
  Boat_Info_Inactive_Icon,
  My_Insurance_Active_Icon,
  My_Insurance_Inactive_Icon,
  View_Quote_Active_Icon,
  View_Quote_Inactive_Icon,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import BoatInfoPage from "./boat-info-page";
import PersonalInfoPage from "./personal-info-page";
import InsurancePage from "./insurance-page";
import InsuranceQuote from "./insurance-quote";
import { requiredValidator } from "../../lib/validation/validation";
import moment from "moment";
import currencies from "../../translations/currency.json";
import { MOBILE, COUNTRY_CODE } from "../../lib/input-control-data/input-definitions";
import { handlePreferredCountries } from "../../lib/phone-num/phone-num";
import { getLocationfromLatLng, getLocationfromStateZipcode } from "../../lib/location/geo-location";
import { getCountries, getStates } from "country-state-picker";
import { handleFirstName, handleLastName, handleMiddleName } from "../../lib/name-handlers/handlers";
import { timestampTomomentizedDate } from "../../lib/date-operation/date-operation";
import { getStoreBuyingInfo, getStoreBuyingInfoUser, storeBuyingInfo, storeBuyingInfoUser } from "../../services/buy-with-boatzon";
import Snackbar from "../../components/snackbar";

class InsuranceServices extends Component {
  state = {
    value: 0,
    isPersonalInfoFormValid: false,
    isBoatInfoFormValid: false,
    isInsuranceFormValid: false,
    isQuoteFormValid: false,
    isAppointmentFormValid: false,
    interested_parties_list: false,
    timeSelectedForCall: "",
    selectedDate: moment(),
    inputpayload: {
      loanTermsApproved: false,
      meeting_type: "In-Person",
      loan_type: { value: "Purchase", label: "Purchase" },
    },
    countryShortName: "us",
    apiloading: false,
    preferredCountries: ["us"],
  };

  componentDidMount() {
    console.log("insurance-services.jsx");
    this.handleOnLoad();
  }

  // On Component load this called
  handleOnLoad = async () => {
    const { fullName, phoneNumber, countryCode, email, latitude, longitude, location, creditScore } = {
      ...this.props.userProfileData,
    };
    const DateOfBirth = this.props.userProfileData.dob;
    const {
      price,
      condition,
      subCategory,
      manufactor,
      manufactorId,
      year,
      length,
      engineType1,
      engineMake1,
      engineYear1,
      engineCount,
      engineHorsePower1,
      engineFuelType1,
      boatModel,
      place,
    } = {
      ...this.props.postBoatDetailsData,
    };

    const {
      first_name,
      last_name,
      Email,
      dob,
      mobile,
      address,
      country,
      city,
      stateName,
      zipCode,
      maritalStatus,
      boat_type,
      manufacturer_type,
      boat_make,
      boat_year,
      hull_idNum,
      boat_usage,
      boat_speed,
      engine_type,
      engine_make,
      engine_year,
      num_of_engines,
      hourse_power,
      fuel_type,
      residence_type,
      monthly_rent,
      rent_year,
      rent_month,
      employment_status,
      employer_name,
      employer_year,
      employer_month,
      gross_income,
      applicant,
      loan_type,
      terms_year,
      purchase_price,
      down_payment,
      loan_amount,
      tradeIn_allowance,
      trade_payoff,
      purchase_date,
      insurance_purchase_price,
      years_owned,
      replacement_cost,
      hull_deduction,
      liability_limit,
      medical,
      mooring_locations,
      mooring_types,
      mooring_zip_code,
      fire_fighting_system,
      burglary_detection,
      trailer_avaiable,
      years_boat_ownership,
      years_boat_experience,
      boat_courses,
      boating_loss,
      premium_up_front,
      loanTermsApproved,
      CustomerCreditScore,
      CustomerToken,
      CustomerTransid,
    } = {
      ...this.props.buyBoatDetailsData,
    };

    const dialCode = this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.countryCode;
    const PhoneNumber = phoneNumber.startsWith("+") ? phoneNumber.replace(countryCode, "") : phoneNumber;

    const boatCondition = this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.condition;
    const Length = this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.length;
    let tempPreferredCountries =
      this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.countryCode
        ? handlePreferredCountries(this.props.buyBoatDetailsData.countryCode, mobile)
        : countryCode != "undefined"
        ? handlePreferredCountries(countryCode, PhoneNumber)
        : this.state.preferredCountries;
    let userLocation;

    try {
      if (latitude && longitude) {
        userLocation = await getLocationfromLatLng(latitude, longitude);
      }
      const StateList = userLocation
        ? getStates(userLocation.countryShortName.toLowerCase()).map((data) => ({
            value: data,
            label: data,
          }))
        : "";
      let countryShortName = userLocation ? userLocation.countryShortName.toLowerCase() : "us";
      const StateName =
        StateList &&
        StateList.filter((data) => {
          return data.value.includes(userLocation.stateName);
        });

      let tempPayload = { ...this.state.inputpayload };
      tempPayload.userId = this.props.userId;
      tempPayload.productId = this.props.productId;
      let fullNameArr = fullName.split(" ");
      tempPayload.first_name = first_name ? first_name : `${handleFirstName(fullNameArr)}${handleMiddleName(fullNameArr)}`;
      tempPayload.last_name = last_name ? last_names : handleLastName(fullNameArr);
      tempPayload.mobile = mobile ? mobile : PhoneNumber;
      tempPayload.countryCode = dialCode ? dialCode : countryCode != "undefined" ? countryCode : "+91";
      tempPayload.Email = Email ? Email : email ? email : "";
      tempPayload.dob = dob ? moment(dob) : DateOfBirth ? timestampTomomentizedDate(DateOfBirth) : "";
      tempPayload.address = address ? address : userLocation && userLocation.address ? userLocation.address : location ? location : "";
      tempPayload.country = country
        ? country
        : userLocation && userLocation.country
        ? { value: userLocation.countryShortName, label: userLocation.country }
        : {};
      tempPayload.city = city ? city : userLocation && userLocation.city;
      tempPayload.zipCode = zipCode ? zipCode : userLocation && userLocation.zipCode;
      tempPayload.stateName = stateName ? stateName : StateName ? StateName[0] : "";
      tempPayload.maritalStatus = maritalStatus ? maritalStatus : "";
      tempPayload.condition = boatCondition ? boatCondition : condition ? condition : "";
      tempPayload.boat_type = boat_type ? boat_type : subCategory ? { value: subCategory, label: subCategory } : {};
      tempPayload.manufacturer_type = manufacturer_type ? manufacturer_type : manufactor ? { value: manufactorId, label: manufactor } : {};
      tempPayload.boat_make = boatModel ? boatModel : boat_make ? boat_make : "";
      tempPayload.boat_year = boat_year ? boat_year : year ? { value: year, label: year } : {};
      tempPayload.hull_idNum = hull_idNum ? hull_idNum : "";
      tempPayload.length = Length ? Length : length ? length : "";
      tempPayload.boat_usage = boat_usage ? boat_usage : {};
      tempPayload.boat_speed = boat_speed ? boat_speed : "";
      tempPayload.engine_type = engine_type ? engine_type : engineType1 ? { value: engineType1, label: engineType1 } : {};
      tempPayload.engine_make = engine_make ? engine_make : engineMake1 ? { value: engineMake1, label: engineMake1 } : {};
      tempPayload.engine_year = engine_year ? engine_year : engineYear1 ? { value: engineYear1, label: engineYear1 } : {};
      tempPayload.num_of_engines = num_of_engines ? num_of_engines : engineCount ? { value: engineCount, label: engineCount } : {};
      tempPayload.hourse_power = hourse_power ? hourse_power : engineHorsePower1 ? engineHorsePower1 : "";
      tempPayload.fuel_type = fuel_type ? fuel_type : engineFuelType1 ? { value: engineFuelType1, label: engineFuelType1 } : {};

      tempPayload.residence_type = residence_type ? residence_type : {};
      tempPayload.monthly_rent = monthly_rent ? monthly_rent : "";
      tempPayload.rent_year = rent_year ? rent_year : {};
      tempPayload.rent_month = rent_month ? rent_month : {};
      tempPayload.employment_status = employment_status ? employment_status : {};
      tempPayload.employer_name = employer_name ? employer_name : "";
      tempPayload.employer_year = employer_year ? employer_year : {};
      tempPayload.employer_month = employer_month ? employer_month : {};
      tempPayload.gross_income = gross_income ? gross_income : "";
      tempPayload.applicant = applicant ? applicant : "";
      tempPayload.loan_type = loan_type ? loan_type : { value: "Purchase", label: "Purchase" };
      tempPayload.terms_year = terms_year ? terms_year : {};
      tempPayload.purchase_price = purchase_price ? purchase_price : price ? price : "";
      tempPayload.down_payment = down_payment ? down_payment : "";
      tempPayload.loan_amount = loan_amount ? loan_amount : price ? price : "";
      tempPayload.tradeIn_allowance = tradeIn_allowance ? tradeIn_allowance : "";
      tempPayload.trade_payoff = trade_payoff ? trade_payoff : "";

      tempPayload.purchase_date = purchase_date ? moment(purchase_date) : moment();
      tempPayload.insurance_purchase_price = insurance_purchase_price ? insurance_purchase_price : "";
      tempPayload.years_owned = years_owned ? years_owned : {};
      tempPayload.replacement_cost = replacement_cost ? replacement_cost : price ? price : "";
      tempPayload.hull_deduction = hull_deduction ? hull_deduction : {};
      tempPayload.liability_limit = liability_limit ? liability_limit : {};
      tempPayload.medical = medical ? medical : {};
      tempPayload.mooring_locations = mooring_locations ? mooring_locations : {};
      tempPayload.mooring_types = mooring_types ? mooring_types : {};
      tempPayload.mooring_zip_code = mooring_zip_code ? mooring_zip_code : userLocation && userLocation.zipCode;
      tempPayload.fire_fighting_system = fire_fighting_system ? fire_fighting_system : {};
      tempPayload.burglary_detection = burglary_detection ? burglary_detection : {};
      tempPayload.trailer_avaiable = trailer_avaiable ? trailer_avaiable : {};
      tempPayload.years_boat_ownership = years_boat_ownership ? years_boat_ownership : "";
      tempPayload.years_boat_experience = years_boat_experience ? years_boat_experience : "";
      tempPayload.boat_courses = boat_courses ? boat_courses : {};
      tempPayload.boating_loss = boating_loss ? boating_loss : {};
      tempPayload.premium_up_front = premium_up_front ? premium_up_front : {};
      tempPayload.loanTermsApproved = loanTermsApproved ? loanTermsApproved : false;
      tempPayload.loanTermsApproved = loanTermsApproved ? loanTermsApproved : false;
      tempPayload.CustomerCreditScore = CustomerCreditScore ? CustomerCreditScore : creditScore ? parseInt(creditScore, 10) : 0;
      tempPayload.CustomerToken = CustomerToken ? CustomerToken : "";
      tempPayload.CustomerTransid = CustomerTransid ? CustomerTransid : "";
      tempPayload.boatAddress = place ? place : "";
      this.setState(
        {
          inputpayload: { ...tempPayload },
          EmailValid: tempPayload.Email ? 1 : 2,
          isPhoneValid: phoneNumber ? true : false,
          preferredCountries: [...tempPreferredCountries],
          countryShortName: countryShortName,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } catch (err) {
      console.log("err---->", err);
    }
  };

  // Function to Store form values
  handleStoreInfoAPICall = () => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          apiloading: true,
        },
        () => {
          this.props.productId
            ? storeBuyingInfo(this.state.inputpayload)
            : storeBuyingInfoUser(this.state.inputpayload)
                .then((res) => {
                  this.setState({
                    usermessage: res.data.message,
                    variant: this.handleSnackbar(res.data),
                    open: true,
                    vertical: "bottom",
                    horizontal: "left",
                    apiloading: res.data.code == 200 ? false : true,
                  });
                  let apiResponse = res.data.code == 200 ? true : false;
                  return resolve(apiResponse);
                })
                .catch((err) => {
                  reject(err);
                });
        }
      );
    });
  };

  // Function to get Latitude and Longitude from stateName and zipCode
  handleGetLatLng = async () => {
    this.setState({
      latLngAPI: true,
    });
    const { address, country, stateName, city, zipCode } = this.state.inputpayload;
    let location;
    try {
      if (address && country && stateName && city && zipCode) {
        location = await getLocationfromStateZipcode(address, country, stateName, city, zipCode);
      }
      let tempPayload = { ...this.state.inputpayload };
      tempPayload["latitude"] = location.lat || location.latitude;
      tempPayload["longitude"] = location.lng || location.longitude;
      this.setState({
        inputpayload: { ...tempPayload },
        locationUpdate: true,
        latLngAPI: false,
      });
    } catch (err) {
      console.log("err---->", err);
      this.setState({
        locationUpdate: false,
      });
    }
    return this.state.locationUpdate;
  };

  // Function to get loanQuote and insuranceQuote
  handleGetStoreBuyingInfo = () => {
    this.handleStoreInfoAPICall();
    return new Promise((resolve, reject) => {
      this.setState(
        {
          apiResponse: true,
        },
        () => {
          let payload = {
            mqttId: this.props.userId,
            postId: this.props.productId,
          };
          this.props.productId
            ? getStoreBuyingInfo(payload)
            : getStoreBuyingInfoUser(payload)
                .then((res) => {
                  console.log("fbeuf", res);
                  let tempPayload = { ...this.state.inputpayload };
                  tempPayload.loanQuote = Math.round(res.data.data[0].loanQuote);
                  tempPayload.insuranceQuote = Math.round(res.data.data[0].insuranceQuote);
                  this.setState({
                    inputpayload: { ...tempPayload },
                    apiloading: res.data.code == 200 ? false : true,
                  });
                  let apiResponse = res.data.code == 200 ? true : false;
                  return resolve(apiResponse);
                })
                .catch((err) => {
                  reject(err);
                });
        }
      );
    });
  };

  handleNext = async () => {
    try {
      let lanLanUpdate = this.state.value == 0 ? await this.handleGetLatLng() : true;
      let apiResponse = this.state.value == 2 ? await this.handleGetStoreBuyingInfo() : await this.handleStoreInfoAPICall();
      this.setState(
        {
          apiResponse: apiResponse && lanLanUpdate,
        },
        () => {
          if (this.state.apiResponse) {
            this.setState({
              value: this.state.value + 1,
            });
          }
        }
      );
    } catch (err) {
      console.log("err", err);
      this.setState({
        usermessage: err.message,
        variant: "error",
        open: true,
        vertical: "bottom",
        horizontal: "left",
      });
    }
  };

  handleBack = () => {
    this.setState({
      value: this.state.value - 1,
    });
  };

  // Function for form validation
  checkIfFormValid = () => {
    const { inputpayload, isPhoneValid, EmailValid, SSNValid } = this.state;
    let isPersonalInfoFormValid = false;
    isPersonalInfoFormValid =
      inputpayload.first_name &&
      inputpayload.last_name &&
      isPhoneValid &&
      EmailValid == 1 &&
      inputpayload.maritalStatus &&
      inputpayload.dob &&
      inputpayload.address &&
      inputpayload.city &&
      inputpayload.stateName &&
      inputpayload.zipCode
        ? true
        : false;

    let SSNFormValid = false;
    SSNFormValid = SSNValid == 1 ? true : false;

    let isBoatInfoFormValid = false;
    isBoatInfoFormValid =
      inputpayload.condition &&
      inputpayload.boat_type &&
      inputpayload.manufacturer_type &&
      inputpayload.boat_make &&
      inputpayload.boat_year &&
      inputpayload.length &&
      inputpayload.boat_usage &&
      inputpayload.boat_speed &&
      inputpayload.engine_type &&
      inputpayload.engine_make &&
      inputpayload.engine_year &&
      inputpayload.num_of_engines &&
      inputpayload.hourse_power &&
      inputpayload.fuel_type
        ? true
        : false;

    let isLoanFormValid = false;
    isLoanFormValid =
      inputpayload.residence_type &&
      inputpayload.monthly_rent &&
      inputpayload.rent_year &&
      inputpayload.rent_month &&
      inputpayload.employment_status &&
      inputpayload.employer_name &&
      inputpayload.employer_year &&
      inputpayload.employer_month &&
      inputpayload.gross_income &&
      inputpayload.applicant &&
      inputpayload.loan_type &&
      inputpayload.terms_year &&
      inputpayload.purchase_price &&
      inputpayload.down_payment &&
      inputpayload.loan_amount
        ? true
        : false;

    let isInsuranceFormValid = false;
    isInsuranceFormValid =
      inputpayload.purchase_date &&
      inputpayload.insurance_purchase_price &&
      inputpayload.years_owned != {} &&
      inputpayload.replacement_cost &&
      inputpayload.hull_deduction != {} &&
      inputpayload.liability_limit != {} &&
      inputpayload.medical != {} &&
      inputpayload.mooring_locations != {} &&
      inputpayload.mooring_types != {} &&
      inputpayload.mooring_zip_code &&
      inputpayload.years_boat_ownership &&
      inputpayload.years_boat_experience &&
      inputpayload.fire_fighting_system &&
      inputpayload.burglary_detection != {} &&
      inputpayload.trailer_avaiable != {} &&
      inputpayload.boat_courses != {} &&
      inputpayload.boating_loss != {} &&
      inputpayload.premium_up_front != {}
        ? true
        : false;

    let isAppointmentFormValid = false;
    isAppointmentFormValid = this.state.timeSelectedForCall !== "" && this.state.selectedDate ? true : false;

    this.setState({
      isPersonalInfoFormValid: isPersonalInfoFormValid,
      // isPersonalInfoFormValid: true,
      // isBoatInfoFormValid: true,
      isBoatInfoFormValid: isBoatInfoFormValid,
      isLoanFormValid: isLoanFormValid,
      isInsuranceFormValid: isInsuranceFormValid,
      isAppointmentFormValid: isAppointmentFormValid,
      SSNFormValid: SSNFormValid,
    });
  };

  // handleServiceChange = (event, value) => {
  //   this.setState({ value });
  // };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log("valuenumber", dialInfo, valuenumber);
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
      },
      () => {
        console.log("valuenumber", this.state.isPhoneValid);
        this.checkIfFormValid();
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        tempPayload[`preferredCountries`] = this.state.preferredCountries;
        this.setState({
          inputpayload: { ...tempPayload },
          [`${MOBILE}`]: this.state.UserNumber,
          [`${COUNTRY_CODE}`]: "+" + this.state.cCode,
        });
      }
    );
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    console.log("onSelectFlag", num, country.dialCode, fullNum, status);
    this.handleOnchangePhone(status, num, country);
  };

  handleDOBChange = (date, dateString) => {
    if (date) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`dob`] = moment(date);
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`dob`] = "";
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  disabledDate = (current) => {
    // Can not select days after today and today
    return current && current >= moment().subtract(18, "year");
  };

  handleRadioInputChange = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[[inputControl.name]] = inputControl.value;
    this.setState(
      {
        [inputControl.name]: inputControl.value,
        inputpayload: { ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    let temppayload = { ...this.state.inputpayload };
    temppayload[[name]] = {
      value: inputControl.value,
      label: inputControl.label,
    };
    if (name == "country") {
      this.setState({
        countryShortName: event.value,
      });
    }
    this.setState(
      {
        inputpayload: { ...temppayload },
        [name]: {
          value: inputControl.value,
          label: inputControl.value,
        },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  updateUserAddress = (data) => {
    console.log("nficf", data);
    if (data) {
      this.setState(
        {
          address: data.address,
          city: data.city,
          zipCode: data.zipCode,
          country: data.country,
          isAddressSelected: true,
          latitude: data.lat,
          longitude: data.lng,
          stateName: "",
        },
        () => {
          let tempPayload = { ...this.state.inputpayload };
          tempPayload[`address`] = this.state.address;
          tempPayload[`zipCode`] = this.state.zipCode;
          tempPayload[`city`] = this.state.city;

          this.checkIfFormValid();
          let countryShortName = getCountries()
            .filter((data) => {
              return data.name.includes(this.state.country);
            })
            .map((item) => {
              return item.code;
            });
          this.setState({
            countryShortName: countryShortName && countryShortName[0].toLowerCase(),
            inputpayload: { ...tempPayload },
          });
        }
      );
    } else {
      this.setState({ isAddressSelected: false });
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [`${inputControl.name}Valid`]: validate });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[[inputControl.name]] = inputControl.value;
    if (inputControl.name === "purchase_price") {
      tempPayload.loan_amount =
        inputControl.value - (tempPayload.down_payment !== "" && tempPayload.down_payment ? tempPayload.down_payment : 0);
      tempPayload.insurance_purchase_price =
        inputControl.value - (tempPayload.down_payment !== "" && tempPayload.down_payment ? tempPayload.down_payment : 0);
      tempPayload.replacement_cost = inputControl.value;
      this.setState({
        loan_amount: tempPayload.loan_amount,
        insurance_purchase_price: tempPayload.insurance_purchase_price,
      });
    } else if (inputControl.name === "down_payment") {
      tempPayload.loan_amount = tempPayload.purchase_price - inputControl.value;
      tempPayload.insurance_purchase_price = tempPayload.purchase_price - inputControl.value;
      this.setState({
        loan_amount: tempPayload.loan_amount,
        insurance_purchase_price: tempPayload.insurance_purchase_price,
      });
    }
    this.setState(
      {
        inputpayload: { ...tempPayload },
        [inputControl.name]: inputControl.value,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  handlePurchaseDateChange = (date, dateString) => {
    if (date) {
      let dob = moment(date);
      let temppayload = { ...this.state.inputpayload };
      temppayload[`purchase_date`] = moment(date);
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`purchase_date`] = "";
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleSnackbar = (response) => {
    switch (response.code || response.status) {
      case 200:
        return "success";
        break;
      case 204 || 422:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      default:
        return "error";
        break;
    }
  };

  render() {
    const Personal_Info = (
      <PersonalInfoPage
        handleNext={this.handleNext}
        handleOnchangePhone={this.handleOnchangePhone}
        handleOnchangeflag={this.handleOnchangeflag}
        handleDOBChange={this.handleDOBChange}
        disabledDate={this.disabledDate}
        handleRadioInputChange={this.handleRadioInputChange}
        handleOnSelectInput={this.handleOnSelectInput}
        updateUserAddress={this.updateUserAddress}
        handleOnchangeInput={this.handleOnchangeInput}
        isPersonalInfoFormValid={this.state.isPersonalInfoFormValid}
        inputpayload={this.state.inputpayload}
        countryShortName={this.state.countryShortName}
        isPhoneValid={this.state.isPhoneValid}
        EmailValid={this.state.EmailValid}
        apiloading={this.state.apiloading}
        preferredCountries={this.state.preferredCountries}
      />
    );
    const Boat_Info = (
      <BoatInfoPage
        handleNext={this.handleNext}
        handleBack={this.handleBack}
        inputpayload={this.state.inputpayload}
        handleOnSelectInput={this.handleOnSelectInput}
        handleOnchangeInput={this.handleOnchangeInput}
        handleRadioInputChange={this.handleRadioInputChange}
        isBoatInfoFormValid={this.state.isBoatInfoFormValid}
        apiloading={this.state.apiloading}
      />
    );
    const Insurance = (
      <InsurancePage
        handleNext={this.handleNext}
        handleBack={this.handleBack}
        inputpayload={this.state.inputpayload}
        handlePurchaseDateChange={this.handlePurchaseDateChange}
        handleOnchangeInput={this.handleOnchangeInput}
        handleOnSelectInput={this.handleOnSelectInput}
        isInsuranceFormValid={this.state.isInsuranceFormValid}
        apiloading={this.state.apiloading}
      />
    );
    const Quotes = <InsuranceQuote handleBack={this.handleBack} inputpayload={this.state.inputpayload} />;
    const { value } = this.state;
    const Loan_InsuranceTabArr = [
      {
        label: `Personal Info`,
        activeIcon: <img src={Personal_Info_Active_Icon} className="tabIcon"></img>,
        inactiveIcon: <img src={Personal_Info_Inactive_Icon} className="tabIcon"></img>,
      },
      {
        label: `Boat Info`,
        activeIcon: <img src={Boat_Info_Active_Icon} className="tabIcon"></img>,
        inactiveIcon: <img src={Boat_Info_Inactive_Icon} className="tabIcon"></img>,
      },
      {
        label: `My Insurance`,
        activeIcon: <img src={My_Insurance_Active_Icon} className="tabIcon"></img>,
        inactiveIcon: <img src={My_Insurance_Inactive_Icon} className="tabIcon"></img>,
      },
      {
        label: `View Quotes`,
        activeIcon: <img src={View_Quote_Active_Icon} className="tabIcon"></img>,
        inactiveIcon: <img src={View_Quote_Inactive_Icon} className="tabIcon"></img>,
      },
    ];
    const Loan_InsuranceTabContentArr = [{ content: Personal_Info }, { content: Boat_Info }, { content: Insurance }, { content: Quotes }];
    return (
      <Wrapper>
        <div className="col-12 p-0 LoanServicesPage">
          <div className="row m-0 py-3">
            <Tabs
              loanfinancingservices={true}
              servicesValue={value}
              // handleServiceChange={this.handleServiceChange}
              tabs={Loan_InsuranceTabArr}
              tabcontent={Loan_InsuranceTabContentArr}
            />
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        <style jsx>{`
          .tabIcon {
            width: fit-content;
            height: 1.317vw;
          }
          .boatPic {
            width: 100%;
            height: 8.052vw;
            object-fit: cover;
          }
          .postTitle {
            font-size: 1.317vw;
            color: ${FONTGREY_COLOR};
            font-family: "Museo-Sans" !important;
            font-weight: 600;
            margin-bottom: 0.585vw;
            max-width: 75%;
          }
          .emiOptions,
          .insuranceOption {
            font-size: 0.732vw;
            margin: 0;
            color: ${GREY_VARIANT_2};
            text-transform: capitalize;
            font-family: "Open Sans-SemiBold" !important;
          }
        `}</style>
      </Wrapper>
    );
  }
}

export default InsuranceServices;
