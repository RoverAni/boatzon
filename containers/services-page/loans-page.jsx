import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import {
  NumberValidator,
  PureTextValidator,
} from "../../lib/validation/validation";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  WHITE_COLOR,
  Question_LightGrey_Icon,
  Question_LightBlue_Icon,
  Long_Arrow_Left_White_Icon,
  Long_Arrow_Right_White_Icon,
  GREY_VARIANT_10,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { withStyles } from "@material-ui/core";
const YearsList = getYearsList();
import { getYearsList } from "../../lib/date-operation/date-operation";
import moment from "moment";
import CircularProgressButton from "../../components/button-loader/button-loader";

const styles = (theme) => ({
  radiogrouRoot: {
    flexDirection: "row",
  },
  radioLabelRoot: {
    margin: "0",
  },
  radioLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    top: "0.219vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Museo-Sans !important",
    fontSize: "0.805vw",
  },
  root: {
    padding: "0.658vw 0.658vw 0.219vw 0",
    color: `${GREY_VARIANT_3}`,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
    "& span": {
      "& div": {
        "& svg": {
          width: "0.951vw",
        },
      },
    },
  },
  checked: {
    color: `${THEME_COLOR} !important`,
  },
});

class LoansPage extends Component {
  render() {
    const ResidenceType = [
      { value: "Own", label: "Own" },
      { value: "Rent", label: "Rent" },
      { value: "With Relatives", label: "With Relatives" },
    ];

    const ManufacturerType = [
      { value: "Tracker", label: "Tracker" },
      { value: "Sea Ray", label: "Sea Ray" },
      { value: "Ranger", label: "Ranger" },
    ];

    const EmploymentStatus = [
      { value: "Employed", label: "Employed" },
      { value: "Self-Employed", label: "Self-Employed" },
      { value: "Unemployed", label: "Unemployed" },
      { value: "Retired", label: "Retired" },
      { value: "Military", label: "Military" },
      { value: "Student", label: "Student" },
    ];

    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const Emp_YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const Emp_MonthOptions = moment.monthsShort().map((data) => ({
      value: data,
      label: data,
    }));

    const MonthOptions = moment.monthsShort().map((data) => ({
      value: data,
      label: data,
    }));

    const Terms_YearOptions = [
      { value: "8 years", label: "8 years" },
      { value: "10 years", label: "10 years" },
      { value: "12 years", label: "12 years" },
      { value: "15 years", label: "15 years" },
      { value: "20 years", label: "20 years" },
      { value: "30 years", label: "30 years" },
    ];

    const Loan_Type = [
      { value: "Purchase", label: "Purchase" },
      { value: "Refinance", label: "Refinance" },
    ];

    const HoursePowerTypes = [
      { value: "115", label: "115" },
      { value: "200", label: "200" },
      { value: "300", label: "300" },
    ];
    const FuelTypes = [
      { value: "Gas", label: "Gas" },
      { value: "Diesel", label: "Diesel" },
      { value: "Electric", label: "Electric" },
    ];
    const { classes, apiloading } = this.props;
    const {
      residence_type,
      monthly_rent,
      rent_year,
      rent_month,
      employment_status,
      employer_name,
      employer_year,
      employer_month,
      gross_income,
      applicant,
      loan_type,
      terms_year,
      purchase_price,
      down_payment,
      loan_amount,
      tradeIn_allowance,
      trade_payoff,
    } = this.props.inputpayload;
    return (
      <Wrapper>
        <div className="py-3 BoatInfoPage">
          <div className="col-12 section">
            <h6 className="title">
              Your Residence{" "}
              <span className="helperText">
                (<span className="mandatory">*</span> Required Field)
              </span>
            </h6>
            <div className="row m-0">
              <div className="col-7 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Current Residence: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={ResidenceType}
                            value={residence_type}
                            onChange={this.props.handleOnSelectInput(
                              `residence_type`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Monthly Mortgage/Rent:{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="monthly_rent"
                            value={monthly_rent}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Time At This Address: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <div className="row m-0">
                            <div className="col-6 p-0 pr-1">
                              <SelectInput
                                servicesSelect={true}
                                placeholder="Years"
                                options={YearOptions}
                                value={rent_year}
                                onChange={this.props.handleOnSelectInput(
                                  `rent_year`
                                )}
                              />
                            </div>
                            <div className="col-6 p-0">
                              <SelectInput
                                servicesSelect={true}
                                placeholder="Month"
                                options={MonthOptions}
                                value={rent_month}
                                onChange={this.props.handleOnSelectInput(
                                  `rent_month`
                                )}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-4 p-0"></div>
            </div>
          </div>
          <div className="col-12 section">
            <h6 className="title">Employer & Financial Information</h6>
            <div className="row m-0">
              <div className="col-7 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Employment Status: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={EmploymentStatus}
                            value={employment_status}
                            onChange={this.props.handleOnSelectInput(
                              `employment_status`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Employer Name: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="employer_name"
                            value={employer_name}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={PureTextValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Time With Employer: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <div className="row m-0">
                            <div className="col-6 p-0 pr-1">
                              <SelectInput
                                servicesSelect={true}
                                placeholder="Years"
                                options={Emp_YearOptions}
                                value={employer_year}
                                onChange={this.props.handleOnSelectInput(
                                  `employer_year`
                                )}
                              />
                            </div>
                            <div className="col-6 p-0">
                              <SelectInput
                                servicesSelect={true}
                                placeholder="Month"
                                options={Emp_MonthOptions}
                                value={employer_month}
                                onChange={this.props.handleOnSelectInput(
                                  `employer_month`
                                )}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Gross Income: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="gross_income"
                            placeholder="Monthly gross income"
                            value={gross_income}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-4 p-0"></div>
            </div>
          </div>

          <div className="col-12 section">
            <h6 className="title">Loan Information</h6>
            <div className="row m-0">
              <div className="col-6 p-0">
                <table className="contentTabel contentTabel1">
                  <tr>
                    <th>
                      Applicant: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <FormControl component="fieldset">
                        <RadioGroup
                          classes={{
                            root: classes.radiogrouRoot,
                          }}
                          aria-label="applicant"
                          name="applicant"
                          value={applicant}
                          onChange={this.props.handleRadioInputChange}
                        >
                          <FormControlLabel
                            classes={{
                              root: classes.radioLabelRoot,
                              label: classes.radioLabel,
                            }}
                            value="Joint"
                            control={
                              <Radio
                                classes={{
                                  root: classes.root,
                                  checked: classes.checked,
                                }}
                              />
                            }
                            label="Joint"
                          />
                          <FormControlLabel
                            classes={{
                              root: classes.radioLabelRoot,
                              label: classes.radioLabel,
                            }}
                            value="Individual"
                            control={
                              <Radio
                                classes={{
                                  root: classes.root,
                                  checked: classes.checked,
                                }}
                              />
                            }
                            label="Individual"
                          />
                        </RadioGroup>
                      </FormControl>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Loan Type: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            defaultValue={Loan_Type[0]}
                            options={Loan_Type}
                            value={loan_type}
                            onChange={this.props.handleOnSelectInput(
                              `loan_type`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Term Requested: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            placeholder="Years"
                            options={Terms_YearOptions}
                            value={terms_year}
                            onChange={this.props.handleOnSelectInput(
                              `terms_year`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Purchase Price: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="purchase_price"
                            value={purchase_price}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Down Payment: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="down_payment"
                            value={down_payment}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Loan Amount: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="loan_amount"
                            value={loan_amount}
                            // onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-6 p-0">
                <table className="contentTabel contentTabel1">
                  <tr>
                    <th>Trade-In Allowance:</th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="tradeIn_allowance"
                            value={tradeIn_allowance}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>Trade Payoff:</th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="trade_payoff"
                            value={trade_payoff}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <div className="col-12 p-0 py-2">
            <div className="d-flex align-items-center justify-content-between">
              <div className="back_btn">
                <ButtonComp onClick={this.props.handleBack}>
                  <img
                    src={Long_Arrow_Left_White_Icon}
                    className="backArrow"
                  ></img>{" "}
                  Back
                </ButtonComp>
              </div>
              <div
                className={this.props.isLoanFormValid ? "next_btn" : "back_btn"}
              >
                <CircularProgressButton
                  buttonText={
                    <span>
                      Next{" "}
                      <img
                        src={Long_Arrow_Right_White_Icon}
                        className="nextArrow"
                      ></img>
                    </span>
                  }
                  disabled={!this.props.isLoanFormValid}
                  onClick={this.props.handleNext}
                  loading={apiloading}
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .helperText {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-weight: 500;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            .divWidth {
              width: 95%;
            }
            .title {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
              padding-bottom: 0.585vw;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .section {
              padding: 1.098vw 0;
            }
            .contentTabel {
              width: 96%;
            }
            .contentTabel1 {
              width: 90%;
            }
            .contentTabel th {
              width: 40%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
              line-height: 1.2;
            }
            .questionIcon {
              background: url(${Question_LightGrey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
              cursor: pointer;
            }
            .questionIcon:hover {
              background: url(${Question_LightBlue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
            }
            .contentHeading {
              font-size: 0.915vw;
              color: ${FONTGREY_COLOR};
              font-weight: 600;
              margin-bottom: 0.366vw;
              font-family: "Open Sans" !important;
            }
            .contentTabel td {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
            }
            :global(.contentTabel td .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              margin-top: 0.366vw;
              font-family: "Open Sans" !important;
            }
            :global(.contentTabel td .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.contentTabel td .inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(LoansPage);
