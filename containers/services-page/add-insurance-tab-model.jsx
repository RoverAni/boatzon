import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon_Dark_Grey,
  FONTGREY_COLOR,
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_1,
  Add_Insurance_Tab_Icon,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";

class AddInsuranceTabModel extends Component {
  render() {
    const {
      onClose,
      handleProceedFurther,
      handleAddInsuranceQuote,
    } = this.props;
    return (
      <Wrapper>
        <div className="addInsuranceTabModel_Sec">
          <img
            src={Close_Icon_Dark_Grey}
            className="closeIcon"
            onClick={onClose}
          ></img>
          <div className="py-4 HeadingSec">
            <h5 className="heading">
              Would you like to add an insurance quote?
            </h5>
            <p className="caption">
              View your wholesale insurance quote for this boat
            </p>
          </div>
          <img
            src={Add_Insurance_Tab_Icon}
            className="AddInsuranceTabIcon"
          ></img>
          <div className="py-4 footerDiv">
            <div className="addInsuranceQuote_btn text-center">
              <ButtonComp onClick={handleAddInsuranceQuote}>
                Add Insurance Quote
              </ButtonComp>
            </div>
            <p className="footerCaption" onClick={handleProceedFurther}>
              No thanks link to proceed
            </p>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: inherit !important;
            }
            .addInsuranceTabModel_Sec {
              width: 43.923vw;
              position: relative;
            }
            .closeIcon {
              width: 0.732vw;
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
            }
            .heading {
              font-size: 1.317vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.366vw;
              text-align: center;
            }
            .caption {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              margin-bottom: 0.366vw;
              text-align: center;
            }
            .AddInsuranceTabIcon {
              width: 100%;
            }
            :global(.addInsuranceQuote_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              text-transform: capitalize;
              position: relative;
              margin: 0.732vw auto;
              border-radius: 0.219vw;
            }
            :global(.addInsuranceQuote_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.878vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.addInsuranceQuote_btn button:focus),
            :global(.addInsuranceQuote_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.addInsuranceQuote_btn button:hover) {
              background: ${THEME_COLOR};
            }
            .footerCaption {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Museo-Sans" !important;
              margin-bottom: 0;
              text-align: center;
              cursor: pointer;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default AddInsuranceTabModel;
