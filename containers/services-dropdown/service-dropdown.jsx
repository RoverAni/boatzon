import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_16,
  Financing_Grey_Icon,
  Insurance_Grey_Icon,
  THEME_COLOR,
  GREEN_COLOR,
  WHITE_COLOR,
  Service_Overview_Bg,
  Yellow_Color,
  Orange_Color,
  Dark_Green_Color,
  Red_Color_1,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import { getCookie } from "../../lib/session";
import Router from "next/router";
import LoginPage from "../../containers/auth-modals/login/login-page";
import Model from "../../components/model/model";
import CustomLink from "../../components/Link/Link";

class ServiceDropdown extends Component {
  state = {
    Score: 650,
    loginPage: false,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      AuthPass,
    });
  }

  handleLoanFinancing = () => {
    this.state.AuthPass ? Router.push(`/loan-financing`) : this.handleLogin();
  };

  handleInsurance = () => {
    this.state.AuthPass ? Router.push(`/insurance`) : this.handleLogin();
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  render() {
    const { userProfileData } = this.props;

    return (
      <Wrapper>
        <div className="screenWidth mx-auto">
          <div className="col-12 px-0 py-3">
            <div className="d-flex align-item-center justify-content-between">
              <div>
                <div className="d-flex align-item-center">
                  <div className="loan_financing_Sec">
                    <h5 className="title">
                      <img src={Financing_Grey_Icon}></img> Loans & Financing
                    </h5>
                    <ul className="list-unstyled m-0 my-2 loan_financing_list">
                      <CustomLink href={`/get-pre-qualified`}>
                        <li className="mb-2">Get Pre-Qualified in 2 minutes</li>{" "}
                      </CustomLink>
                      <li onClick={this.handleLoanFinancing}>
                        Boat Loan Application
                      </li>
                      <li>Engine Loan Application</li>
                      <li>Products/Electronics Loan Application</li>
                      <li>Trailer Loan Application</li>
                    </ul>
                  </div>
                  <div className="insurance_Sec">
                    <h5 className="title">
                      <img src={Insurance_Grey_Icon}></img> Insurance
                    </h5>
                    <ul className="list-unstyled m-0 my-2 loan_financing_list">
                      <li onClick={this.handleInsurance}>
                        Get Insurance Quote
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div>
                <div className="d-flex align-item-center justify-content-end">
                  <div className="getApproval_Sec">
                    <h4 className="heading">Get Pre-Qualified Now</h4>
                    <p className="getApproval_desc">
                      in 2 minutes to view personolized monthly payments for any
                      boat, engine, trailer, and even electronics.
                    </p>
                    <div className="d-flex align-items-center justify-content-around scoreBar">
                      <div
                        className={
                          this.state.Score >= 300 && this.state.Score < 410
                            ? "firstScoreDiv currentScoreDiv"
                            : "firstScoreDiv"
                        }
                      >
                        <p className="cardLabel">
                          {this.state.Score >= 300 && this.state.Score < 410
                            ? "Poor"
                            : " "}
                        </p>
                      </div>
                      <div
                        className={
                          this.state.Score >= 410 && this.state.Score < 520
                            ? "secondScoreDiv currentScoreDiv"
                            : "secondScoreDiv"
                        }
                      >
                        <p className="cardLabel">
                          {this.state.Score >= 410 && this.state.Score < 520
                            ? "Fair"
                            : " "}
                        </p>
                      </div>
                      <div
                        className={
                          this.state.Score >= 520 && this.state.Score < 630
                            ? "thirdScoreDiv currentScoreDiv"
                            : "thirdScoreDiv"
                        }
                      >
                        <p className="cardLabel">
                          {this.state.Score >= 520 && this.state.Score < 630
                            ? "Good"
                            : " "}
                        </p>
                      </div>
                      <div
                        className={
                          this.state.Score >= 630 && this.state.Score < 740
                            ? "fourthScoreDiv currentScoreDiv"
                            : "fourthScoreDiv"
                        }
                      >
                        <p className="cardLabel">
                          {this.state.Score >= 630 && this.state.Score < 740
                            ? "Very Good"
                            : " "}
                        </p>
                      </div>
                      <div
                        className={
                          this.state.Score >= 740 && this.state.Score < 850
                            ? "lastScoreDiv currentScoreDiv"
                            : "lastScoreDiv"
                        }
                      >
                        <p className="cardLabel">
                          {this.state.Score >= 740 && this.state.Score < 850
                            ? "Exceptional"
                            : " "}
                        </p>
                      </div>
                    </div>

                    {!userProfileData?.creditScore && (
                      <CustomLink href={`/get-pre-qualified`}>
                        <div className="getPreApproved_btn">
                          <ButtonComp>Get Pre-Qualified</ButtonComp>
                        </div>
                      </CustomLink>
                    )}
                  </div>
                  <div className="serviceOverview_Sec">
                    <h4 className="heading">Boatzon Services Overview</h4>
                    <div className="readMore_btn">
                      <ButtonComp>Read More</ButtonComp>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>
        <style jsx>
          {`
            .loan_financing_Sec {
              padding: 0 1.83vw 0 1.098vw;
              border-right: 0.0732vw solid ${GREY_VARIANT_16};
            }
            .insurance_Sec {
              padding: 0 1.83vw 0 1.464vw;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              margin: 0;
              font-weight: 600;
              padding: 0 0 0.732vw 0;
              cursor: pointer;
            }
            .title img {
              height: 1.024vw;
              object-fit: cover;
              margin-right: 0.366vw;
            }
            .loan_financing_list li {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0.366vw 0;
            }
            .loan_financing_list li:hover {
              cursor: pointer;
              color: ${THEME_COLOR};
            }
            .getApproval_Sec {
              background: ${GREEN_COLOR};
              margin: 0 0.585vw;
              max-width: 16.471vw;
              position: relative;
              padding: 0.732vw 0;
              height: 13.177vw;
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-size: 1.171vw;
              color: ${WHITE_COLOR};
              margin: 0;
              font-weight: 600;
              padding: 0 0.732vw 0.732vw 0.732vw;
            }
            .getApproval_desc {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              margin: 0 0 0.439vw 0;
              padding: 0 0.732vw 0.366vw 0.732vw;
            }
            .serviceOverview_Sec {
              background: url(${Service_Overview_Bg});
              background-repeat: no-repeat;
              background-size: cover;
              width: 100%;
              margin: 0 0.585vw;
              padding: 0.732vw;
              max-width: 16.471vw;
              position: relative;
              height: 13.177vw;
            }
            :global(.readMore_btn),
            :global(.getApproval_Sec .getPreApproved_btn) {
              position: absolute;
              width: 100%;
              left: 55%;
              transform: translate(-50%, 0);
              bottom: 15px;
            }
            :global(.readMore_btn button) {
              width: 90%;
              padding: 0.732vw 0;
              background: transparent;
              color: ${WHITE_COLOR};
              margin: 3.587vw 0 0 0;
              border: 0.0732vw solid ${WHITE_COLOR};
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.readMore_btn button span) {
              font-family: "Open Sans" !important;
              line-height: 1;
              font-size: 0.951vw;
            }
            :global(.readMore_btn button:focus),
            :global(.readMore_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.readMore_btn button:hover) {
              background: transparent;
            }
            :global(.getApproval_Sec .getPreApproved_btn button) {
              width: 90%;
              padding: 0.732vw 0;
              background: transparent;
              color: ${WHITE_COLOR};
              margin: 0;
              border: 0.0732vw solid ${WHITE_COLOR};
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.getApproval_Sec .getPreApproved_btn button span) {
              font-family: "Open Sans" !important;
              line-height: 1;
              font-size: 0.951vw;
            }
            :global(.getApproval_Sec .getPreApproved_btn button:focus),
            :global(.getApproval_Sec .getPreApproved_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.getApproval_Sec .getPreApproved_btn button:hover) {
              background: transparent;
            }
            .scoreBar {
              overflow-x: hidden;
            }
            .cardLabel {
              font-size: 0.805vw;
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0;
              line-height: 1;
              position: relative;
              z-index: 1;
              bottom: 0.0732vw;
            }
            .firstScoreDiv {
              width: 13%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              background: ${Red_Color_1};
              position: relative;
            }
            .secondScoreDiv {
              width: 18%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              background: ${Orange_Color};
              position: relative;
            }
            .thirdScoreDiv {
              width: 18%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              background: ${Yellow_Color};
              position: relative;
            }
            .fourthScoreDiv {
              width: 18%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              opacity: 0.9;
              background: ${WHITE_COLOR};
              position: relative;
            }
            .lastScoreDiv {
              width: 13%;
              height: 0.585vw;
              border-radius: 0.0732vw;
              opacity: 0.9;
              background: ${Dark_Green_Color};
              position: relative;
            }
            .currentScoreDiv {
              padding: 0.219vw 0.439vw;
              height: 1.317vw;
              width: fit-content;
              text-align: center;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(ServiceDropdown);
