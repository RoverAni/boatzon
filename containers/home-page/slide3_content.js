import React from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";

export default function Slide3Content() {
  return (
    <Wrapper>
      <div
        className="row m-0 align-items-center justify-content-end"
        style={{
          backgroundImage: `url(${env.Banner_Img_3})`,
          width: "100vw",
          backgroundSize: "cover",
        }}
      >
        <div className="col-5">
          <p className="bannerCaption">
            The free, simple and new way to buy and sell boats, engines, or any product for the marine industry.
          </p>
          <h1 className="bannerHeading">Install the boatzon App today!</h1>
          <div className="d-flex justify-content-between my-3 downloadBtnSec">
            <img src={env.App_Store}></img>
            <img src={env.Google_Play}></img>
          </div>
        </div>
        <div className="col-1"></div>
      </div>
      <style jsx>
        {`
          .bannerHeading {
            color: ${env.WHITE_COLOR};
            text-transform: uppercase;
            font-family: "Museo-Sans-Cyrl-Bolder" !important;
            text-align: right;
            max-width: 93%;
            margin: 0 0 0 auto;
            line-height: 1;
            font-size: 3.229vw;
            text-shadow: 0px 4px 10px rgba(0, 0, 0, 0.3);
          }

          .bannerCaption {
            font-size: 1.25vw;
            color: ${env.WHITE_COLOR};
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            max-width: 90%;
            text-align: right;
            margin: 0 0 0.732vw auto;
            line-height: 1.2;
          }

          .downloadBtnSec {
            width: 16vw;
            margin: 0.732vw 0 0 auto;
          }

          .downloadBtnSec img {
            width: 7.901vw;
          }
        `}
      </style>
    </Wrapper>
  );
}
