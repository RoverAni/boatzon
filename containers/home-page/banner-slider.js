import React, { useEffect } from "react";
import Wrapper from "../../hoc/Wrapper";
// Horizantal Scroll component
import Swiper from "swiper";
import * as env from "../../lib/config";
import Slide2Content from "./slide2_content";
import Slide1Content from "./slide1_content";
import Slide3Content from "./slide3_content";
import Slide4Content from "./slide4_content";

export default function BannerSlider(props) {
  useEffect(() => {
    sliderTranslate();
  }, []);

  const sliderTranslate = () => {
    const slider = document.querySelector(".swiper2");
    var swiper2 = new Swiper(slider, {
      slidesPerView: 1,
      spaceBetween: 30,
      loop: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
    swiper2.init();
  };

  return (
    <Wrapper>
      <div className="swiper-container swiper2">
        <div className="swiper-wrapper">
          <div className="swiper-slide">
            <Slide1Content />
          </div>
          <div className="swiper-slide">
            <Slide2Content />
          </div>
          <div className="swiper-slide">
            <Slide3Content />
          </div>
          <div className="swiper-slide">
            <Slide4Content />
          </div>
        </div>
        <div className="swiper-pagination"></div>
        <img className="swiper-button-next" src={env.Banner_Chevron_Right}></img>
        <img className="swiper-button-prev" src={env.Banner_Chevron_Left}></img>
      </div>
      <style jsx>
        {`
          :global(.swiper-container),
          :global(.swiper-wrapper) {
            z-index: 0 !important;
          }
          :global(.swiper-slide) {
            display: flex;
            flex-shrink: unset !important;
            height: 30.208vw;
            width: 100%;
            object-fit: cover !important;
          }
          :global(.swiper-button-next) {
            right: 2.08333vw;
            width: 1.042vw !important;
            top: 50%;
          }
          :global(.swiper-button-prev) {
            width: 1.042vw !important;
            left: 2.08333vw;
            // margin: 0 23px;
            top: 50%;
          }
          :global(.swiper-pagination-bullet-active) {
            background: #98a1b9 !important;
          }
          :global(.swiper-pagination-bullet) {
            background: #ffffff !important;
          }
        `}
      </style>
    </Wrapper>
  );
}
