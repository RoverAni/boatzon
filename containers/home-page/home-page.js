import React from "react";
import Wrapper from "../../hoc/Wrapper";
import BannerSlider from "./banner-slider";
import SecondBanner from "./second-banner";
import ServicesBanner from "./services-banner";
import WholesaleBanner from "./wholesale-banner";

export default function HomePage(props) {
  return (
    <Wrapper>
      <BannerSlider />
      <SecondBanner />
      <WholesaleBanner />
      <ServicesBanner />
    </Wrapper>
  );
}
