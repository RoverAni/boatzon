import React from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomLink from "../../components/Link/Link";
import { useSelector } from "react-redux";

export default function Slide1Content() {
  const userProfileData = useSelector((state) => state.userProfileData);

  return (
    <Wrapper>
      <div className="slideContent1">
        <section className="showcase">
          <div className="videoContainer">
            <video autoPlay={true} muted={true} loop={true}>
              <source src={env.Banner_Video}></source>
            </video>
          </div>
          <div className="content row m-0">
            <div className="col-7 p-0 mx-auto">
              <img src={env.Vector_Img_White} className="vectorImg"></img>
              <h2 className="bannerHeading mb-1">The New Way</h2>
              <h3 className="bannerSubHeading">
                To Buy A Boat and Marine Products
              </h3>
              <p className="bannerCaption">
                Get pre-qualified to see personalized monthly payments on
                thousands of boats, engines, trailers, and products
              </p>
              <div className="row m-0 my-3">
                <div className="col-9 mx-auto p-0 btnSec">
                  <div
                    className={
                      !userProfileData?.creditScore
                        ? "d-flex align-items-center justify-content-between"
                        : "text-center"
                    }
                  >
                    {!userProfileData?.creditScore && (
                      <>
                        <CustomLink href={`/get-pre-qualified`}>
                          <div className="greenBg_btn">
                            <ButtonComp>Get Pre-Qualified</ButtonComp>
                          </div>
                        </CustomLink>
                        <p className="orTxt m-0">or</p>
                      </>
                    )}
                    <CustomLink href={`/boats`}>
                      <div className="whiteBg__greenTxt_btn">
                        <ButtonComp>Search All Boats</ButtonComp>
                      </div>
                    </CustomLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

      <style jsx>
        {`
          .showcase {
            width: 100vw;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
            color: #fff;
            padding: 0 20px;
            position: relative;
          }
          .videoContainer {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
          }

          .videoContainer video {
            width: 100vw;
            height: 100%;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            object-fit: cover;
          }

          .videoContainer:after {
            content: "";
            z-index: 1;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.5);
            position: absolute;
          }

          .content {
            z-index: 2;
          }

          .vectorImg {
            width: 6.193vw;
            margin-bottom: 0.585vw;
          }

          .bannerHeading {
            font-size: 2.708vw;
            color: ${env.WHITE_COLOR};
            font-family: "Museo-Sans-Cyrl-Bolder" !important;
          }

          .bannerSubHeading {
            font-size: 2.188vw;
            color: ${env.WHITE_COLOR};
            font-family: "Museo-Sans-Cyrl-Bolder" !important;
          }

          .bannerCaption {
            font-size: 1.25vw;
            color: ${env.WHITE_COLOR};
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            max-width: 93%;
            margin: 0 auto 0 auto;
          }

          .btnSec {
            max-width: 24.667vw;
          }

          :global(.slideContent1 .greenBg_btn button) {
            width: 10.709vw;
            min-width: 10.709vw;
            height: 2.708vw;
            text-align: right !important;
            padding: 0;
            background: ${env.GREEN_COLOR};
            color: ${env.WHITE_COLOR};
            border-radius: 0;
            margin: 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.937vw;
          }

          :global(.slideContent1 .greenBg_btn button span) {
            width: fit-content;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.937vw;
          }

          :global(.slideContent1 .greenBg_btn button:focus),
          :global(.slideContent1 .greenBg_btn button:active) {
            background: ${env.GREEN_COLOR};
            outline: none;
            box-shadow: none;
          }

          :global(.slideContent1 .greenBg_btn button:hover) {
            background: ${env.GREEN_COLOR};
          }

          :global(.slideContent1 .whiteBg__greenTxt_btn button) {
            width: 10.709vw;
            min-width: 10.709vw;
            height: 2.708vw;
            text-align: right !important;
            padding: 0;
            background: ${env.WHITE_COLOR};
            color: ${env.GREEN_COLOR};
            border-radius: 0;
            margin: 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.937vw;
          }

          :global(.slideContent1 .whiteBg__greenTxt_btn button span) {
            width: fit-content;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.937vw;
          }

          :global(.slideContent1 .whiteBg__greenTxt_btn button:focus),
          :global(.slideContent1 .whiteBg__greenTxt_btn button:active) {
            background: ${env.WHITE_COLOR};
            outline: none;
            box-shadow: none;
          }

          :global(.slideContent1 .whiteBg__greenTxt_btn button:hover) {
            background: ${env.WHITE_COLOR};
          }

          .orTxt {
            color: ${env.WHITE_COLOR};
            font-family: "Museo-Sans-Cyrl-Bolder" !important;
            font-size: 0.937vw;
          }
        `}
      </style>
    </Wrapper>
  );
}
