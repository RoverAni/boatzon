import React from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomLink from "../../components/Link/Link";

export default function WholesaleBanner() {
  return (
    <Wrapper>
      <div className="wholesaleBanner row m-0 d-flex align-items-center justify-content-center">
        <div className="col-3 mx-auto p-0 text-center">
          <img src={env.Vector_Img_Grey} className="vectorImg"></img>
          <h5 className="bannerHeading my-2">
            Wholesale Pricing On New & Used Products
          </h5>
          <p className="bannerCaption">
            Shop deals on thousands of new and used boating and marine products
          </p>
          <CustomLink href="/products">
            <div className="blueBg_btn">
              <ButtonComp>
                <img
                  src={env.Long_Arrow_Right_White_Icon}
                  className="arrowIcon"
                ></img>
                Shop New and Used Products
              </ButtonComp>
            </div>
          </CustomLink>
        </div>
      </div>
      <style jsx>
        {`
          .wholesaleBanner {
            background-image: url(${env.Wholesale_Bg});
            width: 99.8%;
            background-size: cover;
            height: 22.677vw;
            margin-right: 0.732vw;
            border-radius: 0.219vw;
            margin: 0 0.732vw;
          }
          .vectorImg {
            width: 4.129vw;
          }
          .bannerHeading {
            font-size: 1.667vw;
            color: ${env.FONTGREY_COLOR_Dark};
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            line-height: 1;
          }
          .bannerCaption {
            font-size: 0.938vw;
            color: ${env.FONTGREY_COLOR_Dark};
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            line-height: 1.2;
          }
          .arrowIcon {
            width: 0.99vw;
            height: 0.781vw;
            margin-right: 0.878vw;
          }
          :global(.wholesaleBanner .blueBg_btn button) {
            width: 16.927vw;
            min-width: 16.927vw;
            height: 2.5vw;
            text-align: right !important;
            padding: 0.366vw 1.17vw;
            background: ${env.THEME_COLOR};
            color: ${env.WHITE_COLOR};
            border-radius: 0.146vw;
            margin: 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.833vw;
          }

          :global(.wholesaleBanner .blueBg_btn button span) {
            width: fit-content;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.833vw;
            text-align: center;
          }

          :global(.wholesaleBanner .blueBg_btn button:focus),
          :global(.wholesaleBanner .blueBg_btn button:active) {
            background: ${env.THEME_COLOR};
            outline: none;
            box-shadow: none;
          }

          :global(.wholesaleBanner .blueBg_btn button:hover) {
            background: ${env.THEME_COLOR};
          }
        `}
      </style>
    </Wrapper>
  );
}
