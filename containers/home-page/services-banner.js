import React, { useState } from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";
import VideoModel from "../services-page/video-model";
import Model from "../../components/model/model";
import CustomLink from "../../components/Link/Link";
import { getCookie } from "../../lib/session";
import LoginPage from "../../containers/auth-modals/login/login-page";
import { useRouter } from "next/router";
import TaskRouterCapability from "twilio/lib/jwt/taskrouter/TaskRouterCapability";
import { handleSlidetoTop } from "../../lib/global";

export default function ServicesBanner() {
  const [videoSrc, setVideoSrc] = useState("");
  const [openModel, toggleModel] = useState(false);
  const AuthPass = getCookie("authPass");
  const [loginPage, toggleLoginPage] = useState(false);
  const router = useRouter();

  const handleLoanFinancingVideo = (VideoLabel) => {
    let videoSrc =
      VideoLabel !== "Loan_Financing"
        ? `${env.Insurance_Video}`
        : `${env.Loan_Financing_Video}`;
    setVideoSrc(videoSrc);
    toggleModel(true);
  };

  const handleLoanFinancing = () => {
    AuthPass
      ? router.push(`/loan-financing`).then(() => handleSlidetoTop())
      : toggleLoginPage(true);
  };

  const handleInsurance = () => {
    AuthPass
      ? router.push(`/insurance`).then(() => handleSlidetoTop())
      : toggleLoginPage(true);
  };

  return (
    <Wrapper>
      <div className="container p-md-0 servicesBanner">
        <div className="screenWidth my-5 text-center">
          <h4 className="heading">Boatzon Services</h4>
          <p className="caption">
            No middleman, wholesale pricing and services for the boating
            community.
          </p>
          <div className="row m-0 mt-5 align-items-start justify-content-center">
            <div
              className="col-3"
              style={{
                borderRight: `0.0732vw solid ${env.GREY_VARIANT_10}`,
              }}
            >
              <div className="servicesCard">
                <div className="iconDiv">
                  <img
                    src={env.Boatzon_Insurance_Icon}
                    className="insuranceIcon"
                  ></img>
                </div>
                <h6 className="cardHeading my-3">Boatzon Insurance</h6>
                <p className="cardCaption mb-2">
                  We are the insurance agency, no middleman. We offer wholesale
                  insurnce polices that are often up to 50% cheaper than Geico,
                  AllState, Progressive, and more.
                </p>
                <p className="cardCaption mb-2">
                  Save hundreds, even thousands today with Boazon insurance.
                </p>
                <div className="transGrey_btn" onClick={handleInsurance}>
                  <ButtonComp>Get Quote</ButtonComp>
                </div>
                <span
                  className="watchVideo"
                  onClick={() => handleLoanFinancingVideo("Insurance")}
                >
                  <img src={env.Video_Play_Icon} className="watchIcon"></img>
                  Watch The Video
                </span>
              </div>
            </div>
            <div
              className="col-3"
              style={{
                borderRight: `0.0732vw solid ${env.GREY_VARIANT_10}`,
              }}
            >
              <div className="servicesCard">
                <div className="iconDiv">
                  <img
                    src={env.Boatzon_Financing_Icon}
                    className="financingIcon"
                  ></img>
                </div>
                <h6 className="cardHeading my-3">Boatzon Financing</h6>
                <p className="cardCaption mb-2">
                  Looking to buy or refinance a boat, engine, or trailer? Save
                  money today with our wholesale finacing rates and programs.
                </p>
                <p className="cardCaption mb-2">
                  Get pre-approved today with no impact on your credit score.
                </p>
                <div className="transGrey_btn" onClick={handleLoanFinancing}>
                  <ButtonComp>Get Quote</ButtonComp>
                </div>

                <span
                  className="watchVideo"
                  onClick={() => handleLoanFinancingVideo("Loan_Financing")}
                >
                  <img src={env.Video_Play_Icon} className="watchIcon"></img>
                  Watch The Video
                </span>
              </div>
            </div>
            <div
              className="col-3"
              style={{
                borderRight: `0.0732vw solid ${env.GREY_VARIANT_10}`,
              }}
            >
              <div className="servicesCard">
                <div className="iconDiv">
                  <img
                    src={env.Boatzon_Warranties_Icon}
                    className="warrantiesIcon"
                  ></img>
                </div>
                <h6 className="cardHeading my-3">Boatzon Warranties</h6>
                <p className="cardCaption mb-2">
                  Cover your boat, engine, trailer, jetski, or RV with our
                  wholesale warranty programs.
                </p>
                <p className="cardCaption mb-2">
                  Our turn-key warranty programs are low cost and deliver peace
                  of mind on the water.
                </p>
                <div className="transGrey_btn">
                  <ButtonComp disabled={true}>Get Quote</ButtonComp>
                </div>
              </div>
            </div>
            <div className="col-3">
              <div className="servicesCard">
                <div className="iconDiv">
                  <img
                    src={env.Service_Maintenance_Icon}
                    className="maintenanceIcon"
                  ></img>
                </div>

                <h6 className="cardHeading my-3">Service & Maintenance</h6>
                <p className="cardCaption mb-2">
                  Talk to Boatzon Pros for engine repair, repowering,
                  upholstery, detailing, and other services.
                </p>
                <p className="cardCaption mb-2">
                  Even ask advice from the community or post your project for
                  others to see.
                </p>
                <CustomLink href="/professionals">
                  <div className="transGray_talkToProfessionals">
                    <ButtonComp>Talk to Professionals</ButtonComp>
                  </div>
                </CustomLink>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Video Model */}
      <Model open={openModel} onClose={() => toggleModel(false)}>
        <VideoModel onClose={() => toggleModel(false)} videoSrc={videoSrc} />
      </Model>

      {/* Login Model */}
      <Model
        open={loginPage}
        onClose={() => toggleLoginPage(false)}
        authmodals={true}
      >
        <LoginPage onClose={() => toggleLoginPage(false)} />
      </Model>
      <style jsx>
        {`
          .heading {
            font-size: 1.667vw;
            color: ${env.FONTGREY_COLOR_Dark};
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            line-height: 1;
          }
          .caption {
            font-size: 0.729vw;
            color: ${env.FONTGREY_COLOR_Dark};
            font-family: "Open Sans" !important;
            line-height: 1.2;
          }
          .servicesCard {
            height: 19.751vw;
          }
          .servicesCard .cardHeading {
            font-size: 1.25vw;
            color: ${env.FONTGREY_COLOR_Dark};
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            line-height: 1.2;
          }
          .iconDiv {
            display: flex;
            justify-content: center;
            align-items: baseline;
          }
          .insuranceIcon {
            width: 4.135vw;
            object-fit: contain;
            height: 4.195vw;
          }
          .financingIcon {
            width: 4.186vw;
            object-fit: contain;
            height: 4.195vw;
          }
          .warrantiesIcon {
            width: 3.814vw;
            object-fit: contain;
            height: 4.195vw;
          }
          .maintenanceIcon {
            width: 4.195vw;
            object-fit: contain;
            height: 4.195vw;
          }
          .cardCaption {
            font-size: 0.729vw;
            color: ${env.GREY_VARIANT_1};
            font-family: "Open Sans" !important;
            line-height: 1.2;
          }
          .servicesBanner .transGrey_btn,
          .transGray_talkToProfessionals {
            width: 100%;
            position: absolute;
            bottom: 0.878vw;
            left: 50%;
            transform: translate(-50%, 0);
          }
          :global(.servicesBanner .transGrey_btn button) {
            width: 9.635vw;
            min-width: 9.635vw;
            height: 2.188vw;
            text-align: right !important;
            padding: 0;
            background: transparent;
            color: ${env.GREY_VARIANT_1};
            border-radius: 0.146vw;
            border: 0.073vw solid ${env.GREY_VARIANT_2};
            margin: 0.732vw 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans" !important;
            font-size: 0.833vw;
          }
          :global(.transGray_talkToProfessionals button) {
            width: 11.302vw;
            min-width: 11.302vw;
            height: 2.188vw;
            text-align: right !important;
            padding: 0;
            background: transparent;
            color: ${env.GREY_VARIANT_1};
            border-radius: 0.146vw;
            border: 0.073vw solid ${env.GREY_VARIANT_2};
            margin: 0.732vw 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans" !important;
            font-size: 0.833vw;
          }
          :global(.servicesBanner
              .transGrey_btn
              button
              span, .transGray_talkToProfessionals button span) {
            width: fit-content;
            font-family: "Museo-Sans" !important;
            font-size: 0.833vw;
          }

          :global(.servicesBanner .transGrey_btn button:focus),
          :global(.servicesBanner .transGrey_btn button:active),
          :global(.transGray_talkToProfessionals button:focus),
          :global(.transGray_talkToProfessionals button:active) {
            background: transparent;
            outline: none;
            box-shadow: none;
          }

          :global(.servicesBanner
              .transGrey_btn
              button:hover, .transGray_talkToProfessionals button:hover) {
            background: transparent;
          }
          .watchIcon {
            width: 0.805vw;
            margin-right: 0.366vw;
          }

          .watchVideo {
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            bottom: 0;
            left: 50%;
            transform: translate(-50%, 0);
            color: ${env.Light_Blue_Color};
            font-family: "Museo-Sans" !important;
            font-size: 0.833vw;
            cursor: pointer;
          }
        `}
      </style>
    </Wrapper>
  );
}
