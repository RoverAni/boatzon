import React from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomLink from "../../components/Link/Link";

export default function Slide2Content() {
  return (
    <Wrapper>
      <div
        style={{
          backgroundImage: `url(${env.Banner_Img_2})`,
          width: "100vw",
          backgroundSize: "cover",
        }}
        className="slideContent2"
      >
        <div className="row m-0 h-100">
          <div className="col-9 mx-auto">
            <div className="row m-0 d-flex align-items-center justify-content-around h-100">
              <div
                className="col-6 text-center"
                style={{ borderRight: "1px solid #ffffff" }}
              >
                <h3 className="bannerHeading" style={{ width: "60%" }}>
                  Thousands of new and used boats from $99 a month
                </h3>
                <CustomLink href={`/boats`}>
                  <div className="greenBg_btn">
                    <ButtonComp>Shop Boats</ButtonComp>
                  </div>
                </CustomLink>
              </div>
              <div className="col-6 text-center">
                <h3 className="bannerHeading" style={{ width: "66%" }}>
                  Shop deals on new and used boating and marine products
                </h3>
                <CustomLink href={`/products`}>
                  <div className="greenBg_btn">
                    <ButtonComp>Shop Products</ButtonComp>
                  </div>
                </CustomLink>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style jsx>
        {`
          .bannerHeading {
            font-size: 2.188vw;
            color: ${env.WHITE_COLOR};
            margin: 0 auto 1.317vw auto;
            text-shadow: 0px 4px 10px rgba(0, 0, 0, 0.3);
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
          }

          :global(.slideContent2 .greenBg_btn button) {
            width: fit-content;
            min-width: fit-content;
            height: 2.708vw;
            text-align: right !important;
            padding: 0.585vw 2.56vw;
            background: ${env.GREEN_COLOR};
            color: ${env.WHITE_COLOR};
            border-radius: 0;
            margin: 0.732vw 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.938vw;
          }

          :global(.slideContent2 .greenBg_btn button span) {
            width: fit-content;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.938vw;
          }

          :global(.slideContent2 .greenBg_btn button:focus),
          :global(.slideContent2 .greenBg_btn button:active) {
            background: ${env.GREEN_COLOR};
            outline: none;
            box-shadow: none;
          }

          :global(.slideContent2 .greenBg_btn button:hover) {
            background: ${env.GREEN_COLOR};
          }
        `}
      </style>
    </Wrapper>
  );
}
