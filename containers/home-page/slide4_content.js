import React from "react";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import ButtonComp from "../../components/button/button";

export default function Slide4Content() {
  return (
    <Wrapper>
      <div
        className="row m-0 align-items-center justify-content-end slideContent4"
        style={{
          backgroundImage: `url(${env.Banner_Img_4})`,
          width: "100vw",
          backgroundSize: "cover",
        }}
      >
        <div className="col-5">
          <h4 className="bannerCaption">When you post your boat or products on Boatzon, we also</h4>
          <h1 className="bannerHeading">Distribute it everywhere!</h1>
          <div className="greenBg_btn my-2">
            <ButtonComp>Sell Fast Today</ButtonComp>
          </div>
        </div>
        <div className="col-1"></div>
      </div>
      <style jsx>
        {`
          .bannerHeading {
            color: ${env.WHITE_COLOR};
            text-transform: uppercase;
            font-family: "Museo-Sans-Cyrl-Bolder" !important;
            text-align: right;
            max-width: 93%;
            margin: 0 0 0 auto;
            font-size: 3.75vw;
            line-height: 1;
            text-shadow: 0px 4px 10px rgba(0, 0, 0, 0.3);
          }

          .bannerCaption {
            font-size: 1.979vw;
            color: ${env.WHITE_COLOR};
            font-family: "Museo-Sans-Cyrl-Bolder" !important;
            max-width: 90%;
            text-align: right;
            margin: 0 0 0.732vw auto;
          }

          :global(.slideContent4 .greenBg_btn) {
            text-align: right;
          }

          :global(.slideContent4 .greenBg_btn button) {
            width: fit-content;
            min-width: fit-content;
            height: 2.708vw;
            text-align: right !important;
            padding: 0.585vw 2.048vw;
            background: ${env.GREEN_COLOR};
            color: ${env.WHITE_COLOR};
            border-radius: 0;
            margin: 0.732vw 0;
            text-transform: capitalize;
            position: relative;
            box-shadow: none;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.938vw;
          }

          :global(.slideContent4 .greenBg_btn button span) {
            width: fit-content;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.938vw;
          }

          :global(.slideContent4 .greenBg_btn button:focus),
          :global(.slideContent4 .greenBg_btn button:active) {
            background: ${env.GREEN_COLOR};
            outline: none;
            box-shadow: none;
          }

          :global(.slideContent4 .greenBg_btn button:hover) {
            background: ${env.GREEN_COLOR};
          }
        `}
      </style>
    </Wrapper>
  );
}
