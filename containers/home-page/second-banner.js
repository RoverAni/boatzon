import React, { Component } from "react";
import SelectInput from "../../components/input-box/select-input";
import Wrapper from "../../hoc/Wrapper";
import {
  Appstore_Trsp,
  Border_LightGREY_COLOR,
  Google_Play_Trsp,
  GREEN_COLOR,
  GREY_VARIANT_1,
  Inventory_Bg,
  Long_Arrow_Right_White_Icon,
  Search_Grey_Icon,
  Selling_Bg,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  Vector_Img_Orange,
  WHITE_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import Model from "../../components/model/model";
import PostItemModel from "../navbar/post-item-model";
import { getManufactures } from "../../services/manufacturer";
import Router, { withRouter } from "next/router";

class SecondBanner extends Component {
  state = {
    limit: 15,
    boatManufacturerList: [],
    total_count: "",
    postItemModel: false,
    value: "",
  };

  handleOnSelectInput = (name) => (event) => {
    console.log("dkwod", name, event);
    this.setState({
      [name]: event,
    });
  };

  handleBoatsFilter = () => {
    let queryPayload = {};
    if (
      this.state.manufactureId != null &&
      this.state.manufactureId &&
      this.state.manufactureId.length > 0
    ) {
      queryPayload.manufactureId = this.state.manufactureId
        .map((data) => data.value)
        .toString();
    }
    if (
      this.state.Budget != null &&
      this.state.Budget &&
      this.state.Budget.value
    ) {
      queryPayload[`loan`] = {
        fromLoan: 0,
        toLoan: this.state.Budget.value,
      };
    }
    Router.push(
      {
        pathname: "/boats",
        query: { ...queryPayload },
      },
      {
        pathname: "/boats",
        query: { ...queryPayload },
      },
      { scroll: false, shallow: true }
    );
  };

  handleOpenPostItemModel = (tabValue) => {
    console.log("dnwjd", tabValue);
    this.setState({
      value: tabValue,
      postItemModel: true,
    });
  };

  handleClosePostItemModel = () => {
    this.setState({
      postItemModel: false,
    });
  };

  // Async Boat Manufacturer List API call
  handleBoatManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "1", // 0-all manufactureres,1-boats and products,2,engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.boatManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName.toLowerCase(),
                };
              })
            : [];
        this.setState({
          boatManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.boatManufacturerList &&
      this.state.boatManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.boatManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const FinanceTypeOptions = [
      { value: "All", label: "All" },
      { value: 149, label: "Boats under $149/mo" },
      { value: 299, label: "Boats under $299/mo" },
      { value: 499, label: "Boats under $499/mo" },
      { value: 699, label: "Boats under $699/mo" },
      { value: 999, label: "Boats under $999/mo" },
      { value: 1500, label: "Boats under $1500/mo" },
      { value: 2000, label: "Boats under $2000/mo" },
      { value: 2500, label: "Boats under $2500/mo" },
    ];
    const { postItemModel, manufactureId, Budget, value } = this.state;
    const {
      handleBoatManufacturerList,
      handleOpenPostItemModel,
      handleClosePostItemModel,
    } = this;
    return (
      <Wrapper>
        <div className="secondBanner">
          <div className="row m-0 align-items-center justify-content-center">
            <div className="col-6 p-0">
              <div className="leftSec">
                <div className="row m-0 d-flex align-items-center justify-content-center h-100">
                  <div className="col-8">
                    <img src={Vector_Img_Orange} className="vectorImg"></img>
                    <h4 className="bannerHeading mb-1">Largest inventory</h4>
                    <h4 className="bannerHeading mb-1">
                      of new and used boats
                    </h4>
                    <p
                      className="bannerCaption my-3"
                      style={{ maxWidth: "85%" }}
                    >
                      Search our inventory of over 100,000 boats, see monthly
                      payments, and even purchase online.
                    </p>
                    <div className="row m-0 flex-nowrap d-flex align-items-center justify-content-between">
                      <div className="col-6 p-0 searchInput">
                        <img
                          src={Search_Grey_Icon}
                          className="searchIcon"
                        ></img>
                        <SelectInput
                          asyncSelect={true}
                          boatsPage={true}
                          placeholder="Manufacturer"
                          value={manufactureId}
                          loadOptions={handleBoatManufacturerList}
                          onChange={this.handleOnSelectInput(`manufactureId`)}
                          additional={{
                            page: 0,
                          }}
                          hideSelectedOptions={false}
                          isMulti={true}
                        />
                      </div>
                      <div className="col-6 px-1 budgetSelectInput">
                        <div
                          className={
                            Budget ? "FinanceSelectInput" : "SelectInput"
                          }
                        >
                          <SelectInput
                            financeBoat={true}
                            placeholder="My Budget"
                            value={Budget}
                            options={FinanceTypeOptions}
                            onChange={this.handleOnSelectInput(`Budget`)}
                          />
                        </div>
                      </div>
                      <div className="col p-0 greenBg_btn">
                        <ButtonComp onClick={this.handleBoatsFilter}>
                          Go!
                        </ButtonComp>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6 p-0">
              <div className="rightSec">
                <div className="row m-0 d-flex align-items-center justify-content-start h-100">
                  <div className="col-8 p-0 ml-5">
                    <img src={Vector_Img_Orange} className="vectorImg"></img>
                    <h4 className="bannerHeading mb-1">
                      Sell Your Boat or any product fast...
                    </h4>
                    <h4 className="bannerHeading mb-1">and for free!</h4>
                    <p className="bannerCaption my-3">
                      Your boat and products will also be posted on Facebook
                      Marketplace, OfferUp, Google Products, Pinterest,
                      Craiglist, and more.
                    </p>
                    <div className="d-flex align-items-center justify-content-between btnSec">
                      <div
                        className="greenBg_btn"
                        style={{
                          width: "8.802vw",
                        }}
                      >
                        <ButtonComp
                          style={{
                            width: "100%",
                            textTransform: "capitalize",
                            fontSize: "0.833vw",
                          }}
                          onClick={() => this.handleOpenPostItemModel("email")}
                        >
                          <img
                            src={Long_Arrow_Right_White_Icon}
                            className="arrowIcon"
                          ></img>
                          Sell Now
                        </ButtonComp>
                      </div>
                      <div className="social_media_imgs">
                        <img
                          src={Google_Play_Trsp}
                          onClick={() => this.handleOpenPostItemModel("phone")}
                        ></img>
                      </div>
                      <div className="social_media_imgs">
                        <img
                          src={Appstore_Trsp}
                          onClick={() => this.handleOpenPostItemModel("phone")}
                        ></img>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Post Item Model */}
        <Model open={postItemModel} onClose={handleClosePostItemModel}>
          <PostItemModel onClose={handleClosePostItemModel} value={value} />
        </Model>

        <style jsx>
          {`
            .secondBanner {
              margin: 0.732vw;
            }
            .vectorImg {
              width: 4.129vw;
              margin-bottom: 0.585vw;
            }
            .leftSec {
              background-image: url(${Inventory_Bg});
              width: 99.8%;
              background-size: cover;
              height: 19.167vw;
              margin-right: 0.732vw;
              border-radius: 0.219vw;
            }
            .rightSec {
              background-image: url(${Selling_Bg});
              width: 99.8%;
              background-size: cover;
              height: 19.167vw;
              margin-left: 0.732vw;
              border-radius: 0.219vw;
            }

            .bannerHeading {
              font-size: 1.667vw;
              color: ${WHITE_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              line-height: 1;
            }
            .bannerCaption {
              font-size: 0.938vw;
              color: ${WHITE_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              line-height: 1.2;
            }
            :global(.searchInput) {
              position: relative;
              max-width: 13.125vw;
            }

            :global(.searchInput .inputBox) {
              display: block;
              width: 98%;
              max-width: 98% !important;
              height: 2.5vw;
              padding: 0.439vw 0.878vw 0.439vw 1.975vw !important;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.729vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.searchInput .inputBox:focus) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.searchInput .inputBox::placeholder) {
              color: ${GREY_VARIANT_1};
              font-size: 0.729vw;
              font-family: "Open Sans" !important;
            }
            .searchIcon {
              position: absolute;
              transform: translate(0, -50%);
              top: 50%;
              left: 0.732vw;
              width: 1.042vw;
              height: 0.833vw;
            }
            .budgetSelectInput {
              max-width: 13.125vw;
            }
            .btnSec {
              width: 24.189vw;
            }
            :global(.secondBanner .greenBg_btn button) {
              width: 4.063vw;
              min-width: 4.063vw;
              height: 2.5vw;
              text-align: right !important;
              padding: 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              border-radius: 0.146vw;
              margin: 0.732vw 0;
              text-transform: uppercase;
              position: relative;
              box-shadow: none;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.938vw;
            }

            :global(.secondBanner .greenBg_btn button span) {
              width: fit-content;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: inherit;
            }

            :global(.secondBanner .greenBg_btn button:focus),
            :global(.secondBanner .greenBg_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }

            :global(.secondBanner .greenBg_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .arrowIcon {
              width: 0.643vw;
              height: 0.976vw;
              position: absolute;
              top: 50%;
              left: 0.878vw;
              transform: translate(0, -50%);
            }
            .social_media_imgs {
              width: 7.105vw;
              height: 2.5vw;
            }
            .social_media_imgs img {
              width: 100%;
              height: 100%;
              cursor: pointer;
            }

            :global(.FinanceSelectInput .react-select__indicators) {
              background: ${GREEN_COLOR};
              border-radius: 4px;
            }
            :global(.FinanceSelectInput
                .react-select__value-container--has-value) {
              background: ${GREEN_COLOR};
            }
            :global(.FinanceSelectInput .react-select__control) {
              background: ${GREEN_COLOR};
              width: 100% !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SecondBanner;
