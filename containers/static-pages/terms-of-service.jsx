import React, { Component } from "react";

class TermsOfService extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-3">
              <div className="WordSection1 px-4">
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "8.05pt",
                    marginRight: "0in",
                    marginBottom: "8.05pt",
                    marginLeft: "0in",
                    lineHeight: "14.4pt",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "#4A4A4A" }}>
                    Terms of Service
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    LAST UPDATED: February 03, 2021
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      PLEASE READ THESE TERMS OF SERVICE CAREFULLY. THESE TERMS
                      INCLUDE A MANDATORY ARBITRATION PROVISION AND CLASS ACTION
                      WAIVER PROVISION, WHICH AFFECT YOUR RIGHTS ABOUT HOW TO
                      RESOLVE ANY DISPUTE WITH BOATZON HOLDINGS L.L.C.BY
                      ACCESSING OR USING OUR WEBSITE, APPLICATIONS OR OTHER
                      PRODUCTS OR SERVICES (TOGETHER, THE “BOATZON SERVICE”),
                      YOU AGREE TO BE BOUND BY ALL TERMS DESCRIBED HEREIN AND
                      ALL TERMS INCORPORATED BY REFERENCE (“TERMS”). IF YOU DO
                      NOT AGREE TO ALL OF THESE TERMS, DO NOT ACCESS OR USE THE
                      BOATZON SERVICE.
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      YOU ACKNOWLEDGE AND AGREE THAT THERE ARE RISKS ASSOCIATED
                      WITH UTILIZING AN INTERNET-BASED MARKETPLACE AND
                      INTERACTING WITH OTHER USERS IN PERSON AS OUTLINED IN
                      SECTION 17.
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    These Terms apply to your access to and use of the Boatzon
                    Service provided by Boatzon Holdings L.L.C.(“Boatzon,” “we”
                    and “us”). Additional terms (including, but not limited to,
                    the terms of social media services, third-party payment
                    processors, and third-party fulfillment providers) may apply
                    to particular functionalities and features related to the
                    Boatzon Service.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    These Terms governs each website, mobile site, application,
                    and/or other service, regardless of how distributed,
                    transmitted, published, or broadcast (each, a "Service")
                    provided by Boatzon Holdings, L.L.C., and/or subsidiaries
                    ("we," "us," or "our") that links to this Terms of Service,
                    which is binding on all those who access, visit and/or use
                    the Service, whether acting as an individual or on behalf of
                    an entity, including you and all persons, entities, or
                    digital engines of any kind that harvest, crawl, index,
                    scrape, spider, or mine digital content by an automated or
                    manual process or otherwise (collectively, "you" or "your").
                    Any financial product or service that may obtain from us
                    through the Service is also subject to our&nbsp;
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#007BFF" }}>
                      Financial Privacy Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#007BFF" }}>
                    .
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    Please read this Terms of Service carefully. You can access
                    this Terms of Service any time in the footer of the
                    Service's home page or initial screen. Your access,
                    visitation and/or use of the Service, including without
                    limitation any registration on any aspect of the Service,
                    will constitute your agreement to this Terms of Service. If
                    you do not agree with the terms and conditions of this Terms
                    of Service, you may not access, visit and/or use the
                    Service.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    The Terms of Service may be modified from time to time; the
                    date of the most recent revisions will appear on this page,
                    so check back often. Continued access of the Service by you
                    will constitute your acceptance of any changes or revisions
                    to the Terms of Service. If you breach, violate, fail to
                    follow, or act inconsistently with the rules, restrictions,
                    limitations, terms and/or conditions that apply to the
                    Service, whether listed in this Terms of Service, posted at
                    various points in the Service, or otherwise communicated to
                    users of the Service (collectively, the "Agreement"), we may
                    terminate, discontinue, suspend, and/or restrict your
                    account/profile, your ability to access, visit, and/or use
                    the Service or any portion thereof, and/or the Agreement,
                    including without limitation any of our purported
                    obligations hereunder, with or without notice, in addition
                    to our other remedies. In addition, we may curtail,
                    restrict, or refuse to provide you with any future access,
                    visitation, and/or use of the Service. We reserve the right,
                    in addition to our other remedies, to take any technical,
                    legal, and/or other action(s) that we deem necessary and/or
                    appropriate, with or without notice, to prevent violations
                    and enforce the Agreement and remediate any purported
                    violations. You acknowledge and agree that we have the right
                    hereunder to an injunction without posting a bond to stop or
                    prevent a breach or violation of your obligations under the
                    Agreement.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    In the event of any conflict or inconsistency between the
                    terms and conditions of this Terms of Service and the terms
                    of the Financial Privacy Policy, the terms of the Financial
                    Privacy Policy shall control. In the event of any other
                    conflict between the terms and conditions of this Terms of
                    Service, and any rules, restrictions, limitations, terms
                    and/or conditions that may be posted at various points in
                    the Service or otherwise communicated to users of the
                    Service, the terms of this Terms of Service shall control.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    Among other things, the Agreement governs all text,
                    articles, photographs, images, graphics, illustrations,
                    creative, copy, artwork, video, audio, music, podcasts,
                    ringtones, games, trademarks, trade names, service marks,
                    and other brand identifiers, designs, plans, software,
                    source and object code, algorithms, data, statistics,
                    analysis, formulas, indexes, registries, repositories, and
                    all other content, information, and materials (collectively,
                    "Content") available on or through the Service, whether
                    posted, uploaded, transmitted, sent or otherwise made
                    available by us, our licensors, vendors, and/or service
                    providers.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Note: From time to time Boatzon introduces new features that
                    may only be available to certain users. Provisions of these
                    Terms of Service relating to such new features, including,
                    at the present, the electronic payments and fulfillment
                    solutions, may not apply to all users.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      1. Eligibility
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      ONLY USERS WHO ARE THIRTEEN (14) YEARS OF AGE OR OLDER MAY
                      REGISTER FOR OR USE THE BOATZON SERVICE.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;The Boatzon Service is not targeted towards, nor
                    intended for use by, anyone under the age of thirteen (14).
                    If you are between the ages of foureen (14) and eighteen
                    (18), you may use the Boatzon Service only under the
                    supervision of a parent or legal guardian who agrees to be
                    bound by these Terms and any applicable additional terms.
                    You further represent and warrant that you: (a) have not
                    previously been suspended or removed from using the Boatzon
                    Service; (b) are legally permitted to, and do, live in the
                    United States or one of its territories, and (c) may enter
                    into this agreement without violating any other agreement to
                    which you are a party. If you are registering to use the
                    Boatzon Service on behalf of a legal entity, you further
                    represent and warrant that (i) such legal entity is duly
                    organized and validly existing under the applicable laws of
                    the jurisdiction of its organization, and (ii) you are duly
                    authorized by such legal entity to act on its behalf.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      2. Account Registration and Security Responsibilities
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    In order to access certain parts of the Boatzon Service, you
                    may be required to create an Boatzon account (an “Account”)
                    or a seller payment account (“Stripe Account”) with Stripe,
                    Inc. (“Stripe”). In connection with creating an Account or a
                    Stripe Account, you must provide certain information
                    (“Registration Data”) and answer all questions or fields
                    marked “required.” You agree to: (a) provide true, accurate,
                    current and complete Registration Data; (b) maintain and
                    update such Registration Data to keep it true, accurate,
                    current and complete; (c) maintain the security of your
                    Account and the Stripe Account, including by maintaining the
                    security and confidentiality of your login credentials; and
                    (d) consent to allow Boatzon to contact you for the purpose
                    of confirming some or all of your Registration Data, to
                    conduct research and to resolve disputes, as Boatzon may
                    elect to do from time to time.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      3. Purchases From Other Users.
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      A. Cash and Other Payments Determined by Users.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;A buyer may, by agreement with the seller, elect to
                    make payment by cash, check or other payment method accepted
                    by the seller. Such payments are made directly between the
                    buyer and the seller when they meet in person to complete
                    their purchase and sale transaction, pursuant to terms they
                    determine. Boatzon is not a party to such transactions, and
                    does not facilitate such transactions, refunds or returns in
                    any manner.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      B. Boatzon Payment Solution.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;A buyer and seller may instead pay with and accept
                    credit cards and other electronic payment methods (each an
                    “Electronic Payment Method”) through the Boatzon Service.
                    Boatzon has integrated with Stripe, a payment processing
                    service, to allow sellers to accept certain Electronic
                    Payment Methods from buyers using Stripe’s payment
                    processing services (“Boatzon Payment Solution”). Buyers who
                    wish to use the Boatzon Payment Solution must register an
                    Electronic Payment Method with Boatzon, and sellers must
                    enroll for the service through Stripe. Enrollment in and
                    usage of the Boatzon Payment Solution is voluntary, so
                    buyers should note that some sellers may not accept
                    Electronic Payment Methods, or may only accept Electronic
                    Payment Methods for certain transactions. Boatzon, in its
                    sole discretion, may from time to time impose limits on your
                    ability to make and/or receive payments through the Boatzon
                    Payment Solution. Additionally, Stripe may impose its own
                    limits and limitations on a seller’s use of the Boatzon
                    Payment Solution. For instance, a seller’s enrollment in the
                    Boatzon Payment Solution is subject to Stripe’s confirmation
                    that the seller meets Stripe’s enrollment criteria. Sellers
                    should refer to Section 2(d) below for information about the
                    impact of failing to meet Stripe’s enrollment criteria.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      i. Buyers.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;As with any purchases made using cash, all purchases
                    made using the Boatzon Payment Solution are made directly
                    between the buyer and the seller when they complete their
                    purchase and sale transaction, pursuant to the terms they
                    determine. When you initiate a payment through the Boatzon
                    Payment Solution, the seller processes your Electronic
                    Payment Method using Stripe’s payment processing service.
                    Boatzon is not a party to purchase and sale transactions
                    completed using the Boatzon Payment Solution, and disclaims
                    any and all responsibility to facilitate such transactions,
                    except to provide an interface through which you can provide
                    your Electronic Payment Method to Stripe to process on
                    behalf of the seller. Boatzon further disclaims any and all
                    responsibility to facilitate or provide refunds or returns
                    in any manner, other than as expressly provided in the&nbsp;
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                      Buyer Protection Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    .
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      ii. Sellers.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;By using the Boatzon Payment Solution to accept
                    Electronic Payment Methods, you are entering into an
                    agreement with Stripe subject to the terms of the&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a
                      href="https://stripe.com/connect/account-terms"
                      target="_blank"
                    >
                      <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                        Stripe Connected Account Agreement
                      </span>
                    </a>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    , which includes the&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="https://stripe.com/terms" target="_blank">
                      <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                        Stripe Terms of Service
                      </span>
                    </a>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;(collectively, the “Stripe Services Agreement”).
                    Notwithstanding anything to the contrary in the Stripe
                    Services Agreement, you will not have the right to have
                    Stripe, and will not request that Stripe, transfer any buyer
                    data Stripe collects via the Boatzon Payment Solution to an
                    alternative payment processor. Except for the foregoing
                    restriction that supersedes any rights you may have in the
                    Stripe Services Agreement, the Stripe Services Agreement is
                    separate from these Terms. Boatzon is not a party to the
                    Stripe Services Agreement and will not be liable or
                    responsible for the payment services provided by Stripe. If
                    Stripe discontinues providing services in connection with
                    the Boatzon Payment Solution, you authorize Stripe to share
                    your payment method information with an alternative
                    third-party payment processor that is or will be integrated
                    into the Boatzon Payment Solution.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      iii. Seller Fees.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You agree to pay the service fees (“Service Fees”) for
                    the sales transactions you make using the Boatzon Payment
                    Solution. The Service Fees include Electronic Payment Method
                    processing fees to Stripe and service fees to Boatzon.
                    Boatzon reserves the right to change the Service Fees from
                    time to time.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      iv. Receiving Sales Proceeds.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Upon completion of a sale in which the buyer uses the
                    Boatzon Payment Solution, if you have not previously set up
                    a Stripe Account, then you must set up a Stripe Account in
                    accordance with the requirements specified by Stripe to
                    receive the payment via the Boatzon Payment Solution. STRIPE
                    MUST ACCEPT YOUR APPLICATION TO USE THE BOATZON PAYMENT
                    SOLUTION BEFORE YOU CAN RECEIVE SALES PROCEEDS VIA THE
                    BOATZON PAYMENT SOLUTION. IF STRIPE REJECTS YOUR APPLICATION
                    OR YOU FAIL TO SET UP A STRIPE ACCOUNT WITHIN 90 DAYS AFTER
                    COMPLETION OF A SALE PROCESSED BY STRIPE THROUGH THE BOATZON
                    PAYMENT SOLUTION, THEN STRIPE MAY DISABLE OR LIMIT YOUR
                    ABILITY TO RECEIVE SALES PROCEEDS VIA THE BOATZON PAYMENT
                    SOLUTION.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      v. Acceptable Use Violations.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;YOUR RIGHT AND/OR ABILITY TO RECEIVE SALES PROCEEDS
                    VIA THE BOATZON PAYMENT SOLUTION MAY BE REVOKED, DISABLED OR
                    LIMITED IF THE PURCHASE OR SALE VIOLATES SECTION 7
                    (ACCEPTABLE USE) OF THESE TERMS, INCLUDING FOR SALES THAT
                    VIOLATE THE PROHIBITED ITEMS GUIDELINES.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      vi. Transactions Final.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Regardless of the payment method chosen, ALL PURCHASES
                    ARE FINAL, AND THERE ARE NO REFUNDS, UNLESS YOU AND THE
                    SELLER OTHERWISE AGREE AND MAKE ARRANGEMENTS FOR A REFUND.
                    Boatzon will not be responsible for facilitating
                    refunds,&nbsp;
                    <u>
                      other than as expressly provided with respect to the&nbsp;
                    </u>
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                      Buyer Protection Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    .
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      vii. Taxes.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;It is your responsibility to determine what, if any,
                    taxes apply to each transaction you complete via the Boatzon
                    Service, including, for example, sales, use, value added,
                    and similar taxes. It is also your responsibility to
                    withhold, collect, report and remit the correct taxes to the
                    appropriate tax authorities. Boatzon is not responsible for
                    withholding, collecting, reporting, or remitting any sales,
                    use, value added, or similar tax arising from any
                    transaction you complete via the Boatzon Service.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      C. Shipping.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;From time to time, Boatzon may, in its sole discretion
                    and pursuant to its&nbsp;
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                      Shipping Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    , recommend a third party to allow sellers to ship purchased
                    items to buyers. &nbsp;Boatzon is not a party to
                    transactions conducted between buyers and sellers, or to the
                    shipping of items from sellers to buyers, and, other than as
                    expressly provided in the&nbsp;
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                      Shipping Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    , Boatzon will not be liable for, and you release us from
                    any liability to you for, any losses, damages, or delays
                    related to shipping.&nbsp;&nbsp;You agree not to use the
                    Boatzon shipping service to mail or cause to be mailed, or
                    ship or cause to be shipped, any item purchased and sold
                    through Boatzon in a manner that violates the law and/or
                    United States Postal Service or shipping regulations,
                    including but not limited to&nbsp;U.S. Postal Service
                    Publication 52.&nbsp; You further warrant that any item you
                    mail or ship contains no weapons, ammunition, explosives,
                    living or infectious biological matter, human remains,
                    pornography, alcohol, prescription drugs, illegal drugs,
                    currency, dangerous goods, hazardous goods, or other goods
                    that may not be shipped or mailed by law. As a seller, you
                    assume full responsibility for compliance with all
                    applicable laws and regulations, including those regarding
                    mailing and shipping.&nbsp; Anyone who sends, or causes to
                    be sent, a prohibited, illegal, or improperly packaged or
                    labeled material can be subject to legal penalties such as
                    civil penalties, fines and/or imprisonment, including but
                    not limited to those specified in 18 U.S.C. § 1716 and 39
                    U.S.C. § 3018.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      i. Sellers.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;From time to time, Boatzon may, in its sole
                    discretion, permit certain sellers to post their items for
                    sale nationwide. Sellers will be charged a posting fee for
                    any sale that results in an item being shipped. Sellers who
                    choose to post nationwide must ship their items in
                    accordance with Boatzon’s Shipping Policy. When a seller
                    accepts a buyer’s offer to purchase an item to be shipped,
                    the seller must print the pre-paid shipping label and mail
                    the item within 3 business days of accepting the offer. The
                    seller may cancel an accepted offer up until the package is
                    initially scanned for mailing. The buyer’s payment, minus
                    Boatzon’s nationwide posting fee, will be released to the
                    seller typically 3 business days after delivery, provided
                    that no Buyer Protection claims are made by the buyer. Buyer
                    Protection claims may result in delay and/or cancellation of
                    payment being released to the seller.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    textIndent: "41.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      ii. Buyers.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;From time to time, Buyers will be able to view items
                    posted nationwide by sellers who have agreed to use the
                    Boatzon shipping feature. For these items, the buyer is
                    solely responsible to pay the cost of shipping the item from
                    the seller. Buyers can cancel an offer to purchase an item
                    for shipping up until the seller has accepted the offer.
                    When a seller accepts a buyer’s offer to purchase an item
                    for shipping, Boatzon will charge or put a hold on the
                    buyer’s method of payment, pending delivery of the item. If
                    there are any problems with the delivery or the item itself,
                    please consult the Boatzon&nbsp;
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                      Buyer Protection Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    . For any undisclosed damage, incorrect or missing items, or
                    items that are not as described, buyers must contact Boatzon
                    within 2 days of delivery, otherwise Boatzon will deem the
                    transaction closed and release the buyer’s payment to the
                    seller. Please review the Boatzon&nbsp;
                  </span>
                  <u>
                    <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                      Buyer Protection Policy
                    </span>
                  </u>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;to learn more. Buyers may also be given the option
                    within the Boatzon Service to confirm the item purchased is
                    as described after receiving and inspecting it. When done,
                    Boatzon will also deem the transaction closed and release
                    the buyer’s payment to the seller.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      4. Content and Listings
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span
                      style={{
                        fontFamily: '"Segoe UI",sans-serif',
                        color: "dimgray",
                      }}
                    >
                      A.
                    </span>
                  </b>
                  <span
                    style={{
                      fontFamily: '"Segoe UI",sans-serif',
                      color: "dimgray",
                    }}
                  >
                    {" "}
                    Content that is provided by us, our licensors, vendors
                    and/or service providers, including without limitation
                    photos, images, text, music, audio, videos, podcasts,
                    trademarks, trade names, service marks and other brand
                    identifiers, the organization, design, compilation, and
                    "look and feel" of the Service, and all advertising thereon,
                    is protected by local, state, federal, provincial, national,
                    international, and foreign copyright, trademark and other
                    intellectual property laws, rules, and regulations, and is
                    the property of us or our licensors, vendors and/or service
                    providers.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span
                      style={{
                        fontFamily: '"Segoe UI",sans-serif',
                        color: "dimgray",
                      }}
                    >
                      B.
                    </span>
                  </b>
                  <span
                    style={{
                      fontFamily: '"Segoe UI",sans-serif',
                      color: "dimgray",
                    }}
                  >
                    {" "}
                    Certain Content may be made available to you on or through
                    the Service for download, installation, and/or streaming on
                    your computer, mobile or other device, and via Real Simple
                    Syndication (RSS), such as photos, images, text, music,
                    audio, videos, podcasts, ringtones, games, graphics, or
                    software. Such Content is subject to the same terms,
                    conditions, limitations and restrictions applicable to all
                    Content provided by us, our licensors, vendors and/or
                    service providers. You must, in addition to all of your
                    other obligations, use such Content only to the extent
                    expressly authorized for the particular Content, and you may
                    not use such Content in a manner that exceeds such
                    authorization. Certain Content on the Service may be
                    provided by third parties and Carvana may not have editorial
                    control over the content. The views or opinions expressed by
                    those third parties do not necessarily represent the views
                    of Carvana.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span
                      style={{
                        fontFamily: '"Segoe UI",sans-serif',
                        color: "dimgray",
                      }}
                    >
                      C.
                    </span>
                  </b>
                  <span
                    style={{
                      fontFamily: '"Segoe UI",sans-serif',
                      color: "dimgray",
                    }}
                  >
                    {" "}
                    Vessel listings, engine listings, trailer listings, and all
                    product listings and descriptions on the Service are for
                    informational purposes only and Boatzon does not guarantee
                    the accuracy of such information. Boatzon may obtain listing
                    information, including listing descriptions from third
                    parties, so there is a possibility that unintentional errors
                    can occur. All listings are subject to prior sale and may
                    not be available in your area when you are ready to
                    purchase. You agree that any reliance on the listing and
                    descriptions on the Service is at your own risk. All
                    listings are subject to the{" "}
                    <b>
                      Disclaimer of Warranty and Limitation of Liability
                      sections
                    </b>{" "}
                    in these Terms of Use. Boatzon is under no obligation to
                    finance, sell, or lease a vessel, engine, trailer, or
                    product to you. In addition, despite our best efforts, it is
                    possible that listings or other items on our website may be
                    mispriced. If the correct price of a vessel, engine, trailer
                    or other item is higher than our stated price, we will, at
                    our discretion, either contact you for instructions before
                    delivery or pickup of the listing or cancel your purchase
                    and notify you of such cancellation.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      5. Terms of Sale for Boatzon’s Paid Services.
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    From time to time, Boatzon may make certain services
                    available for a fee in connection with the Boatzon Service
                    (“Paid Services”). The following terms of sale apply solely
                    to your purchase of Paid Services from Boatzon.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      A. Fees.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Unless otherwise agreed upon by Boatzon in writing,
                    the fees payable in connection with any Paid Services
                    (“Fees”) will be specified via the Boatzon Service. All Fees
                    are denominated in U.S. dollars and are exclusive of any
                    applicable taxes.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      B. Payment Method.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You may only pay Fees using valid payment methods
                    acceptable to us, as specified via the Boatzon Service. You
                    represent and warrant that you are authorized to use the
                    payment method you designate via the Boatzon Service. You
                    authorize us to charge your designated payment method for
                    the total amount of your purchase, including any applicable
                    taxes and other charges. You authorize us to use a
                    third-party service to update your designated payment
                    information if it is cancelled or expires. If the payment
                    method cannot be verified, is invalid or is otherwise not
                    acceptable to us, your order may be suspended or cancelled.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      C. Subscriptions.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;CERTAIN OF THE PAID SERVICES PROVIDED BY BOATZON MAY
                    BE OFFERED ON A SUBSCRIPTION BASIS WITH AUTO-RENEWING
                    PAYMENTS (“SUBSCRIPTION SERVICES”). THE BILLING PERIOD FOR
                    EACH TYPE OF SUBSCRIPTION SERVICE WILL BE AS SPECIFIED VIA
                    THE BOATZON SERVICE AT THE TIME OF REGISTRATION. WHEN YOU
                    REGISTER FOR ANY SUBSCRIPTION SERVICE, YOU EXPRESSLY
                    ACKNOWLEDGE AND AGREE THAT (I) BOATZON (OR ITS DESIGNATED
                    THIRD-PARTY PAYMENT PROCESSOR) IS AUTHORIZED TO CHARGE YOU
                    ON A RECURRING BASIS FOR THE SUBSCRIPTION SERVICE (IN
                    ADDITION TO ANY APPLICABLE TAXES AND OTHER CHARGES) AT THE
                    THEN-CURRENT RATES FOR AS LONG AS THE SUBSCRIPTION SERVICE
                    CONTINUES, AND (II) THE SUBSCRIPTION SERVICE WILL CONTINUE
                    UNTIL YOU CANCEL IT OR WE SUSPEND OR STOP PROVIDING ACCESS
                    TO THE BOATZON SERVICE IN ACCORDANCE WITH THESE TERMS.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      D. Cancellation Policy For Subscription Services.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;TO CANCEL ANY SUBSCRIPTION SERVICE, YOU MUST CONTACT
                    US THROUGH OUR HELP CENTER VIA OUR MOBILE APP OR WEBSITE
                    (WWW.BOATZON.COM) AND FOLLOW THE INSTRUCTIONS IN THE EMAIL
                    WE SEND YOU IN RESPONSE TO YOUR CANCELLATION REQUEST. YOU
                    MUST CANCEL A SUBSCRIPTION SERVICE BEFORE THE START OF THE
                    NEXT BILLING PERIOD IN ORDER TO AVOID CHARGES FOR THE NEXT
                    BILLING PERIOD’S FEES. FOLLOWING ANY CANCELLATION, YOU WILL
                    CONTINUE TO HAVE ACCESS TO THE SUBSCRIPTION SERVICES
                    (SUBJECT TO THESE TERMS) THROUGH THE END OF YOUR CURRENT
                    BILLING PERIOD.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      E. Price Changes.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;BOATZON RESERVES THE RIGHT TO MODIFY THE FEES FOR ANY
                    PAID SERVICES, INCLUDING ANY SUBSCRIPTION SERVICES, FROM
                    TIME TO TIME IN ITS SOLE DISCRETION. FOR SUBSCRIPTION
                    SERVICES, PRICE CHANGES WILL APPLY TO THE NEXT BILLING
                    PERIOD.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      F. Taxes.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You are responsible for any sales, duty or other
                    governmental taxes or fees due with respect to your purchase
                    of Paid Services. We will collect applicable sales tax if we
                    determine that we have a duty to collect sales tax, and will
                    provide notice of such taxes at the time you place your
                    order.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      G. No Refunds.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Except as provided in Section 5(H), or as otherwise
                    expressly agreed upon by Boatzon, all sales of Paid Services
                    (including any Subscription Services) are final and there
                    are no refunds. THERE ARE NO REFUNDS OR CREDITS FOR
                    PARTIALLY USED SUBSCRIPTION SERVICES PERIODS.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      H. Errors.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;In the event of an error in connection with the
                    pricing or charging of Paid Services, we reserve the right
                    to correct such error and revise your order accordingly
                    (including charging the correct price) or to cancel the
                    purchase and refund any amount charged. Your sole remedy in
                    the event of a billing error is to obtain a refund for the
                    excess amount charged. To be eligible for such refund, you
                    must provide notice of any such error within 120 days of the
                    date of the billing statement in which such error first
                    appeared.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      6. Discontinuance of the Boatzon Service
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Boatzon may, in its sole discretion and without liability to
                    you, modify, discontinue, terminate, suspend or shut-down
                    (temporarily or permanently) all or any portion of the
                    Boatzon Service at any time, without prior notice. Upon any
                    such action by Boatzon, you must immediately stop using the
                    Boatzon Service. You may also cancel your Account at any
                    time, as described in Section 15 below.{" "}
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      7. Acceptable Use
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    When accessing or using the Boatzon Service, you agree that
                    you will not violate any law, contract, intellectual
                    property or other third-party right or commit a tort.
                    Without limiting the generality of the foregoing, you agree
                    that you will not do, and will not permit any third party to
                    do, any of the following:
                  </span>
                </p>
                <ul type="disc">
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Engage in any unauthorized use of the Boatzon Service
                      (including, without limitation, political campaigning,
                      advertising, or marketing);
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Transmit or otherwise make available any content that: (1)
                      you do not have the right to provide or transmit using the
                      Boatzon Service, (2) may expose Boatzon or its affiliates,
                      licensors, or users to any harm or liability, or (3) is
                      harmful, fraudulent, deceptive, threatening, harassing,
                      defamatory, obscene, unlawful, untrue, or otherwise
                      objectionable;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Upload to, transmit, distribute, store, create, or
                      otherwise sell or offer for sale anything that violates
                      our&nbsp;
                    </span>
                    <u>
                      <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                        Prohibited Items Guidelines
                      </span>
                    </u>
                    <span style={{ fontSize: "12.0pt" }}>.</span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Transmit or otherwise make available any content that
                      contains software viruses or any other computer code,
                      files or programs designed to interrupt, destroy or limit
                      the functionality of any computer software or hardware or
                      telecommunications equipment;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Originate, send, deliver, relay or otherwise transmit
                      unsolicited commercial email or other messages through the
                      Boatzon Service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Copy any portion of the Boatzon Service or any underlying
                      content or source code;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Reverse engineer, disassemble or decompile any portion of
                      the Boatzon Service or otherwise attempt to discover or
                      re-create the source code to any software;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Distribute the software or source code behind the Boatzon
                      Service to any third party;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Make any modification, adaptation, improvement,
                      enhancement, translation or derivative work of or to any
                      portion of the Boatzon Service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Remove, alter, or obscure any copyright or other
                      proprietary notices of Boatzon or its affiliates or
                      licensors in any portion of the Boatzon Service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Obscure or disable any advertisements that appear on or
                      through the Boatzon Service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use any type of automated means, including without
                      limitation any harvesting bot, robot, spider, script,
                      crawler, scraper or other automated means or interface not
                      provided by Boatzon, to utilize the Boatzon Service or to
                      collect or extract data;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "dimgray",
                      marginBottom: "11.25pt",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      copy, harvest, crawl, index, scrape, spider, mine, gather,
                      extract, compile, obtain, aggregate, capture, or store any
                      Content, including without limitation photos, images,
                      text, music, audio, videos, podcasts, data, software,
                      source or object code, algorithms, statistics, analysis,
                      formulas, indexes, registries, repositories, or any other
                      information available on or through the Service, including
                      by an automated or manual process or otherwise, if we have
                      taken steps to forbid, prohibit, or prevent you from doing
                      so;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "dimgray",
                      marginBottom: "11.25pt",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      copy, reproduce, modify, change, edit, crop, alter,
                      revise, adapt, translate, enhance, reformat, remix,
                      rearrange, resize, create derivative works of, move,
                      remove, delete, or erase any copyright, trademark, or
                      other proprietary legends, symbols, marks, or notices on
                      the Service, or attempt to circumvent any mechanisms for
                      preventing the unauthorized reproduction or distribution
                      of Content;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "dimgray",
                      marginBottom: "11.25pt",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      Access without authorization any networks, systems, or
                      databases used in providing the Boatzon Service or any
                      accounts associated with Boatzon Service, or to access or
                      use any information therein for any purpose;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Attempt to probe, test, hack, or otherwise circumvent any
                      security measures;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Violate any requirements, policies, procedures or
                      regulations of any network connected to the Boatzon
                      Service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use the Boatzon Service in any manner that could damage,
                      disable, overburden, or otherwise impair the Boatzon
                      Service (or the networks connected to the Boatzon
                      Service);
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Interfere with or disrupt the use and enjoyment by others
                      of the Boatzon Service, including without limitation
                      attempting, in any manner, to obtain the password,
                      account, or other security information of any other user;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Falsely state, impersonate, or otherwise misrepresent your
                      identity;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Provide false information during Account creation or when
                      using the TruYou Badge or the Boatzon Payment Solution, or
                      otherwise provide false, inaccurate or misleading
                      information;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Create more than one Account or create an Account on
                      behalf of anyone other than yourself without permission;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use or attempt to use another user’s Account without
                      authorization;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Attempt to pay for an item using the Boatzon Payment
                      Solution with an Electronic Payment Method that you either
                      do not own or are not validly authorized to use;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use the Boatzon Service in any manner to stalk, harass,
                      invade the privacy of, or otherwise cause harm to, any
                      person;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use the Boatzon Service in any manner that exposes Boatzon
                      to any harm or liability of any nature;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use the Boatzon Service to infringe or violate the
                      intellectual property rights or any other rights of anyone
                      else (including Boatzon);
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Develop any third-party applications that interact with
                      the Boatzon Service without Boatzon’s prior written
                      consent;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Use the Boatzon Service to engage in any illegal or
                      unauthorized purpose or to engage in, encourage, or
                      promote activities that are unlawful, misleading,
                      malicious or discriminatory, including, but not limited to
                      violations of these Terms, illegal gambling, fraud,
                      money-laundering, or terrorist activities;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Transfer any rights granted to you under these Terms; or
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Encourage or induce any third party to engage in any of
                      the activities prohibited under this section.
                    </span>
                  </li>
                </ul>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    CAUTION: ANY ATTEMPT TO DO ANY OF THE FOREGOING PROHIBITED
                    ACTS, OR TO OTHERWISE UNDERMINE THE OPERATION OF THE
                    SERVICE, MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAW.
                    SHOULD SUCH AN ATTEMPT BE MADE, WE RESERVE THE RIGHT, IN
                    ADDITION TO OUR OTHER REMEDIES, TO SEEK DAMAGES (INCLUDING
                    WITHOUT LIMITATION ATTORNEYS' FEES) FROM ANY SUCH INDIVIDUAL
                    OR ENTITY TO THE FULLEST EXTENT PERMITTED BY LAW, INCLUDING
                    CRIMINAL PROSECUTION.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    If you violate any of the foregoing, Boatzon reserves the
                    right to suspend or terminate your right to access and use
                    the Boatzon Service immediately without notice, and you will
                    have infringed Boatzon’s intellectual property and other
                    rights, which may subject you to prosecution and damages.
                    Boatzon also reserves the right to take any remedies it
                    deems appropriate under the circumstances if you have
                    purchased or sold items that are in violation of this
                    Section 7. Boatzon reserves the right at all times to
                    monitor, review, retain and disclose any information
                    regarding your use of the Boatzon Service as necessary to
                    satisfy any applicable law, regulation, legal process or
                    governmental request. You also acknowledge and agree that
                    Boatzon is not responsible or liable for the conduct of, or
                    your interactions with, any users of the Boatzon Service
                    (whether online or offline). Your interactions with other
                    users are solely between you and such users and we are not
                    responsible or liable for any loss, damage, injury or harm
                    which results from these interactions. In addition,
                    enforcement of these Terms is solely in our discretion, and
                    the absence of enforcement in some instances does not
                    constitute a waiver of our right to enforce these Terms in
                    other instances. These Terms do not create any private right
                    of action on the part of any third party or any reasonable
                    expectation or promise that the Boatzon Service will not
                    contain any content that is prohibited by these Terms.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      8. User Content
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    In the course of using the Boatzon Service, you may transmit
                    or otherwise make available certain content, including
                    information about yourself, content, messages, materials,
                    data, information, text, photos, graphics, code or other
                    items or materials (“User Content”) through interactive
                    areas or services, such as posting items for sale, making an
                    offer, private messaging, or other areas or services. User
                    Content may be publicly viewable in some instances. Boatzon
                    reserves the right, but does not have the obligation, to
                    remove, screen or edit any User Content posted, transmitted,
                    or stored on the Boatzon Service at any time and for any
                    reason without notice. You will not (and will not allow or
                    authorize any third-party to) post, upload to, transmit,
                    distribute, store, create, solicit, disclose, or otherwise,
                    publish through the Boatzon Service any of the following:
                  </span>
                </p>
                <ul type="disc">
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      User Content that is, in Boatzon’s judgment, disrespectful
                      or may expose Boatzon, Users or others to harm or
                      liability;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      User Content that may infringe the patent, trademark,
                      trade secret, copyright, intellectual, privacy or
                      proprietary right of any party;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Private information of any third parties, including
                      addresses, phone numbers and payment card information;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Viruses, corrupted data or other harmful, disruptive, or
                      destructive files; or
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{
                      color: "#6F6F6F",
                      lineHeight: "normal",
                    }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      User Content that inhibits any other person from using or
                      enjoying the Boatzon Service.
                    </span>
                  </li>
                </ul>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    You are solely responsible for creating backup copies of and
                    replacing your User Content at your sole cost and expense.
                    You acknowledge and agree that Boatzon is not responsible
                    for any errors or omissions that you make in connection with
                    the Boatzon Service. By submitting or posting User Content,
                    you hereby grant to Boatzon a non-exclusive, transferable,
                    royalty-free, perpetual, irrevocable, sublicensable right to
                    use, reproduce, modify, adapt, publish, translate, sell,
                    create derivative works from, distribute, perform, and
                    display the User Content, and your name, company name,
                    location and any other information you submit with the User
                    Content, in connection with the Boatzon Service. The use of
                    your or any other User’s name, likeness, or identity in
                    connection with the Boatzon Service does not imply any
                    endorsement thereof unless explicitly stated otherwise. We
                    also have the right to disclose your identity to any third
                    party who is claiming that any content posted by you
                    constitutes a violation of their intellectual property
                    rights, or of their right to privacy.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      9. Moderation
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    You agree that Boatzon may moderate access to and use of the
                    Boatzon Service in our sole discretion through any means
                    (including, for example, blocking, filtering, deletion,
                    delay, omission, verification, and/or termination of your
                    access. Furthermore, we have the right to remove any posting
                    you make on the Boatzon Service if, in our opinion, your
                    post does not comply with the content standards set out in
                    Sections 7 and 8 above, and any other Boatzon Service rules,
                    including without limitation the&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a
                      href="https://offerup.com/support/?return_to=/360032329711"
                      target="_self"
                    >
                      <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                        Prohibited Items Guidelines
                      </span>
                    </a>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    . You agree not to bypass or attempt to bypass such
                    moderation. You further agree that Boatzon is not liable for
                    moderating, not moderating or making any representations
                    regarding moderating.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      10. Third Party Services and Content
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    In using the Boatzon Service, you may view content, utilize
                    services, or otherwise interact with content and services
                    provided by third parties, including, but not limited to,
                    the Boatzon Payment Solution provided by a third-party
                    payment processor, the Boatzon fulfillment solution through
                    third-party logistics providers, links and/or connections to
                    websites, applications or services of such parties
                    (“Third-Party Content”). Boatzon does not control, endorse
                    or adopt any Third-Party Content and you acknowledge and
                    agree that Boatzon will have no responsibility for any Third
                    Party Content, including without limitation, material that
                    may be misleading, incomplete, erroneous, offensive,
                    indecent or otherwise objectionable. In addition, your
                    business or other dealings or correspondence with such third
                    parties are solely between you and the third parties.
                    Boatzon is not responsible or liable for any damage or loss
                    of any sort caused, or alleged to be caused, by or in
                    connection with any such dealings, including the delivery,
                    quality, safety, legality or any other aspect of any good or
                    services that you may purchase or sell to or from a third
                    party.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      11. Feedback
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Any comments or materials sent to us, including, but not
                    limited to, ideas, questions, comments, suggestions,
                    feedback or the like regarding the Boatzon Service or any
                    other products or services of Boatzon (collectively,
                    "Feedback"), is non-confidential and will become our sole
                    property. We will have no obligation to you of any kind,
                    monetary or non-monetary, with respect to such Feedback and
                    will be free to reproduce, use, disclose, exhibit, display,
                    transform, create derivative works from and distribute the
                    Feedback to others without limitation or obligation. You
                    waive any rights you may have to the Feedback (including any
                    copyrights or moral rights). Further, you agree not to
                    submit any feedback that is defamatory, illegal, offensive
                    or otherwise violates any right of any third party, or
                    breaches any agreement between you and any third party.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      12. Copyright Policy
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      A. Repeat Infringer Policy.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;In accordance with the Digital Millennium Copyright
                    Act (“DMCA”) and other applicable law, Boatzon has adopted a
                    policy of terminating, in appropriate circumstances and at
                    Boatzon’s discretion, users who are deemed to be repeat
                    infringers. We also may, at Boatzon’s discretion, limit
                    access to the Boatzon Service and terminate access of any
                    users who infringe any intellectual property rights of
                    others, whether or not there is any repeat infringement.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      B. Copyright Complaints.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;If you believe that anything on the Boatzon Service
                    infringes upon any copyright that you own or control, you
                    may file a notification with Boatzon’s Designated Agent as
                    set forth below:
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Designated Agent:
                    <br />
                    <b>Boatzon Copyright Agent</b>
                    <br />
                    Address of Designated Agent:
                    <br />
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="javascript:void(0)">
                      <b>
                        <span
                          style={{
                            fontSize: "12.0pt",
                            color: "#6F6F6F",
                            textDecoration: "none",
                          }}
                        >
                          20801
                        </span>
                      </b>
                    </a>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    {" "}
                    Biscayne Blvd Aventura, FL 33180
                    <br />
                    Email Address of Designated Agent:
                    <br />
                    <b>Legal@Boatzon.com</b>
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Please see 17 U.S.C. § 512(c)(3) for the requirements of a
                    proper notification. If you knowingly misrepresent in your
                    notification that the material or activity is infringing,
                    you may be liable for any damages, including costs and
                    attorneys’ fees, incurred by Boatzon or the alleged
                    infringer as the result of Boatzon’s reliance upon such
                    misrepresentation in removing or disabling access to the
                    material or activity claimed to be infringing.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      13. Intellectual Property Rights
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Unless otherwise indicated, the Boatzon Service and all
                    content, materials, information, functionality and other
                    materials displayed, performed, contained or available on or
                    through the Boatzon Service, including, without limitation,
                    the Boatzon logo, and all designs, text, graphics, pictures,
                    information, data, sound files, images, illustrations,
                    software, other files, and the selection and arrangement
                    thereof (collectively, the “Materials”) are the proprietary
                    property of Boatzon or its affiliates or licensors, and are
                    protected by U.S. and international copyright laws and other
                    intellectual property rights laws.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Except as otherwise provided, subject to your compliance
                    with all of the terms and conditions of these Terms, and in
                    consideration of your promises reflected herein (and with
                    respect to any services requiring payment of fees, your
                    payment of such fees), we grant to you a revocable,
                    personal, non-exclusive, non-assignable and non-transferable
                    license for personal, non-commercial purposes, except where
                    explicitly provided otherwise, to (i) access and use the
                    Boatzon Service, (ii) cause the Materials to be displayed
                    from a computer and/or mobile device and (iii) use the
                    Materials, solely as permitted under these Terms (the
                    “License”). Boatzon and its affiliates and licensors reserve
                    all rights not expressly granted to you in these Terms. You
                    agree that these Terms do not grant you any rights in or
                    licenses to the Boatzon Service or the Materials, except for
                    this express, limited License. You will not otherwise copy,
                    transmit, distribute, sell, resell, license, de-compile,
                    reverse engineer, disassemble, modify, publish, participate
                    in the transfer or sale of, create derivative works from,
                    perform, display, incorporate into another website, or in
                    any other way exploit any of the Materials or any other part
                    of the Boatzon Service or any derivative works thereof, in
                    whole or in part for commercial or non-commercial purposes.
                    Without limiting the foregoing, you agree not to frame or
                    display the Boatzon Service or Materials (or any portion
                    thereof) as part of any other website or any other work of
                    authorship without our prior written permission. The License
                    granted under this Section will automatically terminate if
                    we suspend or terminate your access to the Boatzon Service.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      14. Trademarks
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Boatzon, Boatzon.com, and other Boatzon graphics, logos,
                    page headers, buttons, icons, scripts, and service names are
                    trademarks, registered trademarks, or trade dress of Boatzon
                    or its affiliates in the U.S. and/or other countries, and
                    may not be copied, imitated, or used, in whole or in part,
                    without Boatzon’s prior written consent. You will not use
                    any trademark, product or service name of Boatzon without
                    our prior written permission, including without limitation
                    any metatags or other “hidden text” utilizing any trademark,
                    product of service name of Boatzon. All other registered
                    trademarks and service marks are used for reference purposes
                    only, and remain the property of their respective owners.
                    Reference to any products, services, processes or other
                    information, by name, trademark, manufacturer, supplier or
                    otherwise does not constitute or imply an endorsement,
                    sponsorship or recommendation by Boatzon.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      15. Suspension; Termination.
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    Boatzon may revoke or terminate your License to access or
                    use the Boatzon Service for any reason without notice at
                    Boatzon’s sole discretion. Without limiting the generality
                    of the foregoing, we may revoke or terminate the License if
                    you: (i) breach any obligation in these Terms or in any
                    other agreement between you and us, (ii) violate any policy
                    or guideline applicable to the Boatzon Service or Materials,
                    or any other Boatzon product or service, or (iii) use the
                    Boatzon Service or the Materials other than as specifically
                    authorized in these Terms, without our prior written
                    permission. You will stop accessing or using the Boatzon
                    Service immediately if Boatzon suspends or terminates your
                    License to access or use the Boatzon Service. Boatzon
                    reserves the right, but does not undertake any duty, to take
                    appropriate legal action including the pursuit of civil,
                    criminal, or injunctive redress against you for continuing
                    to use the Boatzon Service during suspension or after
                    termination. Boatzon may recover its reasonable attorneys’
                    fees and court costs from you for such action. These Terms
                    will remain enforceable against you while your License to
                    access or use the Boatzon Service is suspended and after it
                    is terminated.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    You may also terminate your License to access or use the
                    Boatzon Service by closing your Account at any time. For
                    more details on how to close your Account, please refer to
                    our&nbsp;FAQs.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      16. Disclaimer of Warranty
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                      A.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    {" "}
                    CERTAIN FEATURES, FUNCTIONALITY, AND/OR CONTENT OFFERED ON
                    OR THROUGH THE SERVICE MAY BE HOSTED, ADMINISTERED, RUN OR
                    OTHERWISE PARTICIPATED IN BY THIRD PARTIES. THESE SERVICE
                    PROVIDERS MAY REQUIRE THAT YOU AGREE TO THEIR ADDITIONAL
                    TERMS, CONDITIONS, CONTRACTS, AGREEMENTS AND/OR RULES. YOUR
                    COMPLIANCE WITH ANY SUCH ADDITIONAL TERMS, CONDITIONS,
                    CONTRACTS, AGREEMENTS AND/OR RULES IS SOLELY YOUR
                    RESPONSIBILITY AND WILL HAVE NO EFFECT ON YOUR CONTINUING
                    OBLIGATION TO COMPLY WITH THE AGREEMENT WHEN USING THE
                    SERVICE. WE AND OUR INDEMNITEES SPECIFICALLY DISCLAIM ANY
                    AND ALL LIABILITY IN CONNECTION WITH THE ACTS OR OMISSIONS
                    OF SUCH THIRD PARTIES.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                      B.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    {" "}
                    YOU ACKNOWLEDGE THAT YOU ARE USING THE SERVICE AT YOUR OWN
                    RISK. THE SERVICE IS PROVIDED "AS IS", "WITH ALL FAULTS" AND
                    ON AN "AS AVAILABLE" BASIS, AND WE AND OUR INDEMNITEES
                    HEREBY EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS,
                    WARRANTIES, AND GUARANTEES, EXPRESS AND IMPLIED, INCLUDING
                    BUT NOT LIMITED TO ANY WARRANTIES OF ACCURACY, RELIABILITY,
                    TITLE, MERCHANTABILITY, NON-INFRINGEMENT, FITNESS FOR A
                    PARTICULAR PURPOSE OR ANY OTHER WARRANTY, CONDITION,
                    GUARANTEE OR REPRESENTATION, WHETHER ORAL, IN WRITING OR IN
                    ELECTRONIC FORM, INCLUDING BUT NOT LIMITED TO THE ACCURACY
                    OR COMPLETENESS OF ANY CONTENT CONTAINED THEREIN OR PROVIDED
                    BY US OR THE SERVICE. WE AND OUR INDEMNITEES DO NOT
                    REPRESENT, WARRANT OR GUARANTEE THAT ACCESS TO THE SERVICE
                    AND/OR COMMUNICATIONS OR MESSAGING FROM OR TO US OR YOU WILL
                    BE UNINTERRUPTED, TIMELY, OR ERROR FREE, OR THAT THERE WILL
                    BE NO FAILURES, DELAYS, INACCURACIES, ERRORS OR OMISSIONS OR
                    LOSS OF TRANSMITTED CONTENT, OR THAT NO SOFTWARE DISABLING
                    DEVICES, TIME BOMBS, VIRUSES, WORMS, BUGS, OR DEVICES OR
                    DEFECTS OF SIMILAR NATURE WILL BE TRANSMITTED ON OR THROUGH
                    THE SERVICE, AND WE AND OUR INDEMNITEES WILL NOT BE LIABLE
                    IN THE EVENT OF ANY SUCH OCCURRENCE.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                      C.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    {" "}
                    WE AND OUR INDEMNITEES ARE NOT RESPONSIBLE FOR INCOMPLETE,
                    INCORRECT, LOST, DELAYED, LATE, MISDIRECTED, GARBLED,
                    DAMAGED, ILLEGIBLE, UNDELIVERABLE, OR INCOMPLETELY RECEIVED
                    COMMUNICATIONS OR MESSAGING FROM OR TO US OR YOU FOR ANY
                    REASON, INCLUDING BY REASON OF HARDWARE, SOFTWARE, BROWSER,
                    NETWORK, COMMUNICATIONS SYSTEM FAILURE, MALFUNCTION, DELAY,
                    OR CONGESTION, OR ANY INCOMPATIBILITY AT OUR SERVERS OR
                    ELSEWHERE, OR FOR ANY OTHER TECHNICAL PROBLEMS, ANY FORM OF
                    ACTIVE OR PASSIVE FILTERING BY A USER'S COMPUTER, MOBILE OR
                    OTHER DEVICE OR ACCESS PROVIDER, INSUFFICIENT SPACE ON
                    USER'S COMPUTER, MOBILE OR OTHER DEVICE OR ACCOUNT/PROFILE,
                    OR ANY OTHER CAUSE OR COMBINATION THEREOF.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                      D.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    {" "}
                    WE AND OUR INDEMNITEES SHALL NOT BE LIABLE TO YOU OR ANY
                    THIRD PARTIES FOR ANY DIRECT, INDIRECT, SPECIAL,
                    CONSEQUENTIAL OR PUNITIVE DAMAGES ALLEGEDLY SUSTAINED
                    ARISING OUT OF THE AGREEMENT, THE SERVICE, YOUR ABILITY OR
                    INABILITY TO ACCESS, VISIT AND/OR USE THE SERVICE, INCLUDING
                    DAMAGE TO YOUR COMPUTER, MOBILE OR OTHER DEVICE, OR FOR
                    SOFTWARE DISABLING DEVICES, TIME BOMBS, VIRUSES, WORMS,
                    BUGS, OR DEVICES OR DEFECTS OF SIMILAR NATURE ALLEGED TO
                    HAVE BEEN OBTAINED FROM THE SERVICE, YOUR ACCESS,
                    VISITATION, AND/OR USE OF, OR RELIANCE ON, THE SERVICE
                    AND/OR CONTENT AVAILABLE ON OR THROUGH THE SERVICE,
                    REGARDLESS OF THE TYPE OF CLAIM OR THE NATURE OF THE CAUSE
                    OF ACTION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
                    DAMAGES. IN NO EVENT SHALL OUR LIABILITY TO YOU EXCEED THE
                    TOTAL AMOUNT OF FEES PAID BY YOU DURING THE ONE MONTH PERIOD
                    IN WHICH THE CLAIM AROSE. SOME STATES DO NOT ALLOW THE
                    EXCLUSION OF IMPLIED WARRANTIES OR THE LIMITATION OR
                    EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL
                    DAMAGES. THE AGREEMENT IS NOT SUBJECT TO THE LAWS OF SUCH
                    STATES, BUT TO THE EXTENT A CLAIM IS BROUGHT THEREIN, OUR
                    LIABILITY AND WARRANTIES ARE LIMITED TO THE EXTENT PERMITTED
                    BY LAW. IF YOU ARE A CALIFORNIA RESIDENT, YOU WAIVE
                    CALIFORNIA CIVIL CODE SECTION 1542, WHICH STATES, IN PART:
                    "A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE
                    CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS OR HER
                    FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN
                    BY HIM OR HER MUST HAVE MATERIALLY AFFECTED HIS OR HER
                    SETTLEMENT WITH THE DEBTOR".
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      17. Assumption of Risk
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    A. YOU ACKNOWLEDGE AND AGREE THAT THERE ARE RISKS ASSOCIATED
                    WITH UTILIZING AN INTERNET-BASED MARKETPLACE AND INTERACTING
                    WITH OTHER USERS IN PERSON. WE DO NOT INVESTIGATE OR VERIFY
                    ANY USER’S REPUTATION, CONDUCT, MORALITY, CRIMINAL
                    BACKGROUND, OR ANY INFORMATION USERS MAY SUBMIT TO THE
                    SERVICES (OTHER THAN IN CONNECTION WITH THE TRUYOU FEATURE
                    DESCRIBED IN SECTION 4). YOU ARE SOLELY RESPONSIBLE FOR
                    TAKING ALL NECESSARY PRECAUTIONS WHEN INTERACTING WITH OTHER
                    USERS, PARTICULARLY WHEN MEETING A STRANGER IN PERSON FOR
                    THE FIRST TIME. IT IS POSSIBLE THAT OTHER USERS MAY ATTEMPT
                    TO PHYSICALLY HARM OR DEFRAUD YOU OR OBTAIN INFORMATION FROM
                    YOU FOR FRAUDULENT PURPOSES. YOU ARE SOLELY RESPONSIBLE FOR,
                    AND ASSUME ALL RISKS RELATED TO, SELLING AND BUYING THROUGH
                    BOATZON’S SERVICES (INCLUDING ALL ONLINE AND OFFLINE
                    INTERACTIONS WITH OTHER USERS).
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    B.&nbsp;<b>COMMUNITY MEETUP SPOTS.</b>&nbsp;COMMUNITY MEETUP
                    SPOTS ARE LOCATIONS IN WHICH A THIRD PARTY (SUCH AS A POLICE
                    DEPARTMENT OR LOCAL STORE) HAS AGREED TO POST A COMMUNITY
                    MEETUP SPOT SIGN. WE ENCOURAGE THIRD PARTIES TO PLACE
                    COMMUNITY MEETUP SPOTS IN WELL-LIT LOCATIONS, WITH
                    SURVEILLANCE AND IN GENERALLY WELL-TRAFFICKED AREAS;
                    HOWEVER, BOATZON DOES NOT INDEPENDENTLY VERIFY THE
                    CONDITIONS AT ANY COMMUNITY MEETUP SPOT, DOES NOT MONITOR
                    COMMUNITY MEETUP SPOTS AND DOES NOT WARRANTY THEIR SAFETY OR
                    CONDITION. YOUR USAGE OF COMMUNITY MEETUP SPOTS, AND ANY
                    DISPUTE ARISING OUT OF THAT USAGE, INCLUDING AGAINST ANY
                    THIRD PARTY POSTING A COMMUNITY MEETUP SPOT SIGN, REMAINS
                    SUBJECT TO THE EXPRESS PROVISIONS IN SECTIONS 16-22 OF THESE
                    TERMS OF SERVICE.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      18. Limitation of Liability
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    A.&nbsp;TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW,
                    IN NO EVENT SHALL BOATZON OR THE BOATZON PROVIDERS BE LIABLE
                    FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL
                    DAMAGES, INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, LOSS OF
                    PROFITS OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT,
                    TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE), OR
                    OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH: (I)
                    USE OF THE BOATZON SERVICE OR MATERIALS, INCLUDING, BUT NOT
                    LIMITED TO, ANY DAMAGE CAUSED BY ANY RELIANCE ON, OR ANY
                    DELAYS, INACCURACIES, ERRORS OR OMISSIONS IN, ANY OF THE
                    BOATZON SERVICE OR MATERIALS, (II) ANY INABILITY TO USE THE
                    BOATZON SERVICE OR MATERIALS FOR WHATEVER REASON, OR (III)
                    ANY GOODS OR SERVICES DISCUSSED, PURCHASED OR OBTAINED,
                    DIRECTLY OR INDIRECTLY, THROUGH THE BOATZON SERVICE, EVEN IF
                    BOATZON OR THE BOATZON PROVIDERS ARE ADVISED OF THE
                    POSSIBILITY OF SUCH DAMAGES, INCLUDING WITHOUT LIMITATION
                    ANY DAMAGES CAUSED BY OR RESULTING FROM (Y) RELIANCE BY ANY
                    USER ON ANY INFORMATION OBTAINED FROM COMPANY, OR (Z) THAT
                    RESULT FROM EVENTS BEYOND BOATZON’S OR THE BOATZON
                    PROVIDERS' REASONABLE CONTROL, SUCH AS SITE INTERRUPTIONS,
                    DELETION OF FILES OR EMAIL, ERRORS, DEFECTS, VIRUSES, DELAYS
                    IN OPERATION OR TRANSMISSION OR ANY FAILURE OF PERFORMANCE,
                    WHETHER OR NOT RESULTING FROM A FORCE MAJEURE EVENT,
                    COMMUNICATIONS FAILURE, THEFT, DESTRUCTION OR UNAUTHORIZED
                    ACCESS TO BOATZON’S RECORDS, PROGRAMS OR SERVICES.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    B.&nbsp;TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW,
                    IN NO EVENT SHALL THE AGGREGATE LIABILITY OF BOATZON OR THE
                    BOATZON PROVIDERS (JOINTLY), ARISING OUT OF OR RELATING TO
                    THE USE OF, OR INABILITY TO USE THE OFFER UP SERVICE OR
                    OTHERWISE RELATING TO THESE TERMS EXCEED THE GREATER OF (I)
                    ANY COMPENSATION YOU PAY, IF ANY, TO BOATZON FOR ACCESS TO
                    OR USE OF THE BOATZON SERVICE OR MATERIALS DURING THE 12
                    MONTHS PRECEDING THE EVENT GIVING RISE TO THE LIABILITY; OR
                    (II) $100 U.S. DOLLARS.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    C. THE LIMITATIONS SET FORTH IN THIS SECTION 18 WILL NOT
                    LIMIT OR EXCLUDE LIABILITY FOR BOATZON OR THE BOATZON
                    PROVIDERS' GROSS NEGLIGENCE, INTENTIONAL, WILLFUL, MALICIOUS
                    OR RECKLESS MISCONDUCT OR FRAUD.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      19. Indemnity
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    You agree to hold harmless, defend and indemnify Boatzon and
                    the Boatzon Providers from all liabilities, losses, damages,
                    deficiencies, claims, causes of action, demands and
                    expenses, (including, but not limited to, reasonable
                    attorneys' fees), that are due to, arise from or otherwise
                    relate to your conduct or your use or misuse of the Boatzon
                    Service or Materials, including, without limitation, any
                    actual or threatened suit, demand or claim made against
                    Boatzon or any Boatzon Provider that arises out of or
                    relates to: (i) any intellectual property rights or other
                    proprietary rights of any third party, (ii) your breach of
                    these Terms including without limitation your breach of any
                    of your representations and warranties; (iii) your use of
                    any of the Boatzon Service or Materials; (iv) any content
                    that you store on or transmit through the Boatzon Service;
                    or (v) any items that you mail or ship in connection with
                    the Boatzon Service, including items sold to other Boatzon
                    users. Boatzon may assume exclusive control of any defense
                    of any matter subject to indemnification by you, and you
                    agree to cooperate with Boatzon in such event.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      20. Arbitration
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    PLEASE READ THE FOLLOWING SECTION CAREFULLY BECAUSE IT
                    REQUIRES YOU TO SUBMIT TO BINDING ARBITRATION (INCLUDING A
                    JURY TRIAL WAIVER) ANY AND ALL DISPUTES (OTHER THAN
                    SPECIFIED INTELLECTUAL PROPERTY CLAIMS AND SMALL CLAIMS)
                    WITH BOATZON AND IT LIMITS THE MANNER IN WHICH YOU CAN SEEK
                    RELIEF FROM BOATZON (NO CLASS ARBITRATIONS, CLASS ACTIONS OR
                    REPRESENTATIVE ACTIONS OR ARBITRATIONS).
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      A. Binding Arbitration.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Except for any disputes, claims, suits, actions,
                    causes of action, demands or proceedings (collectively,
                    “Disputes”) arising out of or related to a violation of
                    Section 7 or Disputes in which either party seeks to bring
                    an individual action in small claims court or seeks
                    injunctive or other equitable relief for the alleged
                    unlawful use of intellectual property, including, without
                    limitation, copyrights, trademarks, trade names, logos,
                    trade secrets or patents, you and Boatzon agree (1) to waive
                    your and Boatzon’s respective rights to have any and all
                    Disputes arising from or related to these Terms, the Boatzon
                    Service or the Materials, resolved in a court, and (2) to
                    waive your and Boatzon’s respective rights to a jury trial.
                    Instead, you and Boatzon agree to arbitrate Disputes through
                    binding arbitration (which is the referral of a Dispute to
                    one or more persons charged with reviewing the Dispute and
                    making a final and binding determination to resolve it
                    instead of having the Dispute decided by a judge or jury in
                    court).
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      B.&nbsp;No Class Arbitrations, Class Actions or
                      Representative Actions.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You and Boatzon agree that any Dispute arising out of
                    or related to these Terms, the Boatzon Service or the
                    Materials is personal to you and Boatzon and that such
                    Dispute will be resolved solely through individual
                    arbitration and will not be brought as a class arbitration,
                    class action or any other type of representative proceeding.
                    You and Boatzon agree that there will be no class
                    arbitration or arbitration in which an individual attempts
                    to resolve a Dispute as a representative of another
                    individual or group of individuals. Further, you and Boatzon
                    agree that a Dispute cannot be brought as a class or other
                    type of representative action, whether within or outside of
                    arbitration, or on behalf of any other individual or group
                    of individuals. The limitations imposed by this Section
                    20(B) shall apply to, but are in no way limited to, class
                    action refund claims brought by a class of taxpayers against
                    Boatzon related to taxes collected and remitted in good
                    faith efforts at compliance with state and local marketplace
                    facilitator or marketplace provider laws. Nothing in this
                    Section 20(B) shall be construed to prohibit a buyer from
                    filing a refund claim with the seller or applicable state
                    and local tax authority as permitted by law.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      C.&nbsp;Federal Arbitration Act.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You and Boatzon agree that these Terms affect
                    interstate commerce and that the enforceability of this
                    Section 20 shall be both substantively and procedurally
                    governed by and construed and enforced in accordance with
                    the Federal Arbitration Act, 9 U.S.C. § 1 et seq. (the
                    “FAA”), to the maximum extent permitted by applicable law.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      D.&nbsp;Notice; Informal Dispute Resolution.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You and Boatzon agree that each party will notify the
                    other party in writing of any arbitrable or small claims
                    Dispute within thirty (30) days of the date it arises, so
                    that the parties can attempt in good faith to resolve the
                    Dispute informally. Notice to Boatzon shall be sent by
                    certified mail or courier to Boatzon, Inc., Attn: Boatzon
                    Designated Agent, 20801 Biscayne Blvd Aventura Florida
                    33180. Your notice must include (1) your name, postal
                    address, telephone number, the email address you use or used
                    for your Account, and, if different, an email address at
                    which you can be contacted, (2) a description in reasonable
                    detail of the nature or basis of the Dispute, and (3) the
                    specific relief that you are seeking. Our notice to you will
                    be sent to the email address you used to register for your
                    Account, and will include (a) our name, postal address,
                    telephone number and an email address at which we can be
                    contacted with respect to the Dispute, (b) a description in
                    reasonable detail of the nature or basis of the Dispute, and
                    (c) the specific relief that we are seeking. If you and
                    Boatzon cannot agree how to resolve the Dispute within
                    thirty (30) days after the date notice is received by the
                    applicable party, then either you or Boatzon may, as
                    appropriate and in accordance with this Section 20, commence
                    an arbitration proceeding, or to the extent specifically
                    provided for in section 20(A), file a claim in court.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      E.&nbsp;Process.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Except for Disputes arising out of or related to a
                    violation of Section 7 or Disputes in which either party
                    seeks to bring an individual action in small claims court or
                    seeks injunctive or other equitable relief for the alleged
                    unlawful use of intellectual property, including without
                    limitation, copyrights, trademarks, trade names, logos,
                    trade secrets or patents, you and Boatzon agree that any
                    Dispute must be commenced or filed by you or Boatzon within
                    one (1) year of the date the Dispute arose, otherwise the
                    underlying claim is permanently barred (which means that you
                    and Boatzon will no longer have the right to assert such
                    claim regarding the Dispute). You and Boatzon agree that (1)
                    any arbitration will occur in Delaware, (2) arbitration will
                    be conducted confidentially by a single arbitrator in
                    accordance with the rules of the Judicial Arbitration and
                    mediation Services (“JAMS”), which are hereby incorporated
                    by reference, and (3) that the state or federal courts of
                    the State of Delaware and the United States have exclusive
                    jurisdiction over any appeals and the enforcement of an
                    arbitration award. You may also litigate a Dispute in the
                    small claims court located in the county of your billing
                    address if the Dispute meets the requirements to be heard in
                    small claims court.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      F.&nbsp;Authority of the Arbitrator.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;As limited by the FAA, these Terms and the applicable
                    JAMS rules, the arbitrator will have (1) the exclusive
                    authority and jurisdiction to make all procedural and
                    substantive decisions regarding a Dispute, including the
                    determination of whether a Dispute is arbitrable, and (2)
                    the authority to grant any remedy that would otherwise be
                    available in court; provided, however, that the arbitrator
                    does not have the authority to conduct a class arbitration
                    or a representative action, which is prohibited by these
                    Terms. The arbitrator may only conduct an individual
                    arbitration and may not consolidate more than one
                    individual’s claims, preside over any type of class or
                    representative proceeding or preside over any proceeding
                    involving more than one individual.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      G.&nbsp;Rules of JAMS.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;The rules of JAMS and additional information about
                    JAMS are available on the&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="http://www.jamsadr.com/" target="_blank">
                      <span style={{ fontSize: "12.0pt", color: "#1E9182" }}>
                        JAMS website
                      </span>
                    </a>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    . By agreeing to be bound by these Terms, you either (1)
                    acknowledge and agree that you have read and understand the
                    rules of JAMS, or (2) waive your opportunity to read the
                    rules of JAMS and any claim that the rules of JAMS are
                    unfair or should not apply for any reason.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      H.&nbsp;Severability.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;If any term, clause or provision of this Section 20 is
                    held invalid or unenforceable, it will be so held to the
                    minimum extent required by law, and all other terms, clauses
                    and provisions of this Section 20 will remain valid and
                    enforceable. Further, the waivers set forth in Section 20(B)
                    are severable from the other provisions of these Terms and
                    will remain valid and enforceable, except as prohibited by
                    applicable law.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      I.&nbsp;Opt-Out Right.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You have the right to opt out of binding arbitration
                    within thirty (30) days of the date you first accepted the
                    terms of this Section 20 by writing to: Boatzon, Inc., Attn:
                    Boatzon Designated Agent, 20801 Biscayne Blvd Aventura FL
                    33180. In order to be effective, the opt-out notice must
                    include your full name and clearly indicate your intent to
                    opt-out of binding arbitration. By opting out of binding
                    arbitration, you are agreeing to resolve Disputes in
                    accordance with Section 21.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      21. Governing Law; Venue.
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    These Terms, your access to and use of the Boatzon Service
                    and Materials shall be governed by and construed and
                    enforced in accordance with the laws of the State of
                    Delaware without regard to conflict of law rules or
                    principles (whether of the State of Delaware or any other
                    jurisdiction) that would cause the application of the laws
                    of any other jurisdiction. Any Dispute between the parties
                    that is not subject to arbitration or cannot be heard in
                    small claims court, shall be resolved in the state or
                    federal courts of the State of Delaware and the United
                    States.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      22. Miscellaneous
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      A.&nbsp;Entire Agreement; Order of Precedence.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;These Terms contain the entire agreement, and
                    supersede all prior and contemporaneous understandings
                    between the parties regarding the Boatzon Service. These
                    Terms do not alter the terms or conditions of any other
                    electronic or written agreement you may have with Boatzon
                    for the Boatzon Service or for any other Boatzon product,
                    feature, service or otherwise. In the event of any conflict
                    between these Terms and any other agreement you may have
                    with Boatzon, the terms of that other agreement will control
                    only if these Terms are specifically identified and declared
                    to be overridden by such other agreement.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      B.&nbsp;Amendments.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;We reserve the right to make changes or modifications
                    to these Terms from time to time, in our sole discretion. If
                    we make changes to these Terms, we will provide you with
                    notice of such changes, such as by sending you an email
                    and/or by posting the amended Terms via the Boatzon Service
                    and updating the “Last Updated” date at the top of these
                    Terms. All amended Terms will become effective immediately
                    on the date they are posted to the Boatzon Service unless we
                    state otherwise via our notice of such amended Terms. Any
                    amended Terms will apply prospectively to use of the Boatzon
                    Service after such changes become effective. Your continued
                    use of the Boatzon Service following the effective date of
                    such changes will constitute your acceptance of such
                    changes. If you do not agree to any amended Terms, you must
                    discontinue using the Boatzon Service.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      C.&nbsp;Severability.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;If any provision of these Terms is held to be
                    unenforceable for any reason, such provision will be
                    reformed only to the extent necessary to make it
                    enforceable, and such decision will not affect the
                    enforceability of such provision under other circumstances,
                    or of the remaining provisions hereof under all
                    circumstances.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      D.&nbsp;Waiver.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Our failure or delay in exercising any right, power or
                    privilege under these Terms will not operate as a waiver
                    thereof.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      E.&nbsp;Relationship.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Boatzon is an independent contractor for all purposes,
                    and is not your agent or trustee. You are not an agent of
                    Boatzon.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      F.&nbsp;Assignment.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;You may not assign or transfer any of your rights or
                    obligations under these Terms without prior written consent
                    from Boatzon, including by operation of law or in connection
                    with any change of control. Boatzon may assign or transfer
                    any or all of its rights under these Terms, in whole or in
                    part, without obtaining your consent or approval.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      G.&nbsp;Headings.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Headings of sections are for convenience only and will
                    not be used to limit or construe such sections.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      H.&nbsp;Survival.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;Sections 16 (Disclaimer of Warranties), 18 (Limitation
                    of Liability), 19 (Indemnity), 20 (Arbitration), 21
                    (Governing Law; Venue), this Section 22 (Miscellaneous), and
                    any other term that, by its nature, should survive, will
                    survive any termination or expiration of these Terms.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                      23.
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    {" "}
                  </span>
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                      Communications to You:
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    1. The communications between you and us usually use
                    electronic means, whether you access, visit or use the
                    Service, send us messages, or whether we post notices on the
                    Service or communicate with you via messaging. For
                    contractual purposes, you (a) consent to receive
                    communications from us in electronic form; and (b) agree
                    that all notices, documents, disclosures, and other
                    communications that we provide to you electronically satisfy
                    any legal requirement that such communications would satisfy
                    if they were in writing. Your consent to receive
                    communications and do business electronically, and your
                    agreement to do so applies to all of your interactions and
                    transactions with us.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    2. You understand and agree that joining the Service may
                    include receiving certain communications from us, such as
                    transactional or relationship messages, and/or messages
                    about your account/profile, and that these communications
                    are considered part of your account/profile and you may not
                    be able to opt out of receiving them without ceasing to be a
                    registered user of the Service.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "dimgray" }}>
                    3. You agree that by providing your phone number, Boatzon,
                    or Boatzon's&nbsp;
                    <b>
                      <i>authorized representatives*</i>
                    </b>
                    , may call and/or send text messages (including by using
                    equipment to automatically dial telephone numbers) about
                    your interest in a purchase, for marketing/sales purposes,
                    or for any other servicing or informational purpose related
                    to your account. You do not have to consent to receiving
                    calls or texts to purchase from Boatzon.
                    <br />
                    <b>
                      <i>
                        *Including, but not limited to Boatzon Insurance L.L.C,
                        Boatzon Finance L.L.C, partner banks, financial
                        institutions, partner insurance carriers, and partner
                        warranty carriers.
                      </i>
                    </b>
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt", color: "#6F6F6F" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "12.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  @font-face {
                    font-family: "Segoe UI";
                    panose-1: 2 11 5 2 4 2 4 2 2 3;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                  /* List Definitions */
                  ol {
                    margin-bottom: 0in;
                  }
                  ul {
                    margin-bottom: 0in;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default TermsOfService;
