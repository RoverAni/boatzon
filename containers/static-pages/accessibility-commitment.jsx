import React, { Component } from "react";

class AccessibilityCommitment extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "7.5pt",
                    marginRight: "0in",
                    marginBottom: "0in",
                    marginLeft: "0in",
                    lineHeight: "18.0pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                      Our Policy &amp; Commitment
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "7.5pt",
                    lineHeight: "15.75pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                    At Boatzon Holdings, L.L.C. (the “Company”) we are committed
                    to accessibility, diversity and inclusion. We believe all of
                    our customers, consumers and staff should be able to have
                    equal and effective access to our core websites and other
                    public-facing digital communication. In doing so, the
                    Company has a goal to comply with applicable accessibility
                    laws and regulations including the Americans with
                    Disabilities Act (ADA).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "7.5pt",
                    marginRight: "0in",
                    marginBottom: "0in",
                    marginLeft: "0in",
                    lineHeight: "18.0pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                      Digital Accessibility Statement
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "7.5pt",
                    lineHeight: "15.75pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                    To meet our goal, we have dedicated resources to our digital
                    accessibility compliance effort, including those who
                    coordinate the effort, and those responsible for designing,
                    developing and testing our core website, smartphone apps,
                    websites, chat and digital content. We also work to ensure
                    that our support documentation and services meet
                    accessibility requirements. While we do not control our
                    industry partners and vendors, we encourage them to provide
                    accessible content and assist them in identifying areas that
                    need improvement.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "7.5pt",
                    marginRight: "0in",
                    marginBottom: "0in",
                    marginLeft: "0in",
                    lineHeight: "18.0pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                      Contact
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "7.5pt",
                    lineHeight: "15.75pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                    If you would like further information about the Company’s
                    digital accessibility efforts, have a question, or would
                    like to request auxiliary aids or services, please contact
                    us in one of the following ways:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "0in",
                    lineHeight: "15.75pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        color: "#404041",
                        border: "none windowtext 1.0pt",
                        padding: "0in",
                      }}
                    >
                      Phone:
                    </span>
                  </b>
                  <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                    &nbsp;Contact our team by phone by calling (833) BOATZON or
                    email us at{" "}
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="mailto:Support@Boatzon.com">
                      <span style={{ fontSize: "12.0pt" }}>
                        Support@Boatzon.com
                      </span>
                    </a>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                    .
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "0in",
                    lineHeight: "15.75pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "#404041" }}>
                    <br />
                    For deaf or hard-of-hearing telephone, dial 711 to connect
                    to the Telecommunications Relay Service (TRS).
                  </span>
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif !important;
                  }
                  a:link,
                  span.MsoHyperlink {
                    color: #0563c1;
                    text-decoration: underline;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AccessibilityCommitment;
