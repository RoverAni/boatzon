import React, { Component } from "react";
import PrivacyPolicyTerms from "../buy-with-boatzon-page/privacy-policy-terms";

class FinancialPrivacyPolicy extends Component {
  render() {
    return <PrivacyPolicyTerms pageOpen={true}></PrivacyPolicyTerms>;
  }
}
export default FinancialPrivacyPolicy;
