import React, { Component } from "react";

class PostingRules extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="px-4">
                <p className="text-center">
                  <b>Posting rules</b>
                </p>
                <p>
                  To help make the buying and selling experience enjoyable for
                  everyone on Boatzon, please follow these guidelines when
                  posting items.
                </p>
                <p className="mb-2">
                  <b>Images</b>
                </p>
                <p className="mb-2">
                  Allow buyers to have a good viewing experience for the items
                  you’re selling, please:
                </p>
                <ul>
                  <li>
                    Always include an image of the item. Posts with no image,
                    including those with a photo containing only text, aren't
                    allowed.
                  </li>
                  <li>Include high-quality, well-lit photos.</li>
                  <li>
                    Keep photo clean by not adding extra stickers or text.
                  </li>
                  <li>
                    Post an image of the actual item, not a catalog or stock
                    photo.
                  </li>
                  <li>
                    Use your own photos. Don't use photos that infringe on the
                    intellectual property rights of others.
                  </li>
                </ul>
                <p className="mb-2">
                  <b>Duplicate posts</b>
                </p>
                <p>
                  Multiple posts showing identical items from the same seller
                  can be confusing to buyers, and difficult to manage for
                  sellers. Even if you have more than one of the same item,
                  please list one at a time. Don't post the same item in more
                  than one listing or in multiple categories.
                </p>
                <p className="mb-2">
                  <b>Posting location</b>
                </p>
                <p>
                  Please list an accurate geographic area so your local buyers
                  can arrange to meet you for pickup. If you want to sell
                  something outside your area, list it for selling and shipping
                  nationwide.
                </p>
                <p className="mb-2">
                  <b>Titles, descriptions, &amp; prices</b>
                </p>
                <p>
                  Titles and descriptions should give buyers a clear, honest,
                  and accurate description so they can tell if the item is what
                  they're looking for. Some things to avoid:
                </p>
                <ul>
                  <li>
                    Don't add keywords or price points intended to manipulate
                    search results.
                  </li>
                  <li>Don't link to websites or post URLs.</li>
                  <li>
                    Don't provide personal information, such as a phone number
                    or email address.
                  </li>
                  <li>
                    Don't solicit risky payment methods such as Western Union or
                    Moneygram.
                  </li>
                </ul>
                <p className="mb-2">
                  <b>Messaging &amp; communication</b>
                </p>
                <p>
                  Boatzon messaging lets you communicate with others without
                  sharing personal information or leaving the app. Remember, use
                  of in-app messaging to circumvent prohibited items or posting
                  rules isn't allowed. {" "}
                </p>
                <p className="mb-2">
                  <b>Prohibited items</b>
                </p>
                <p>
                  Most items are allowed on Boatzon, but there are a few types
                  of items that aren’t permitted. Some may present legal issues
                  or be dangerous, while others may be offensive or not in line
                  with the spirit of our community. Read the 
                  <u>prohibited items guidelines</u> for details.
                </p>
                <p className="mb-2">
                  <b>Fee avoidance</b>
                </p>
                <p>
                  Sometimes fees may be incurred when posting certain kinds of
                  items or making cashless transactions. Any action by a seller
                  to avoid paying a fee is strictly prohibited. It's important
                  to remember that if you don't pay these fees, whether
                  intentionally or by mistake, your listings may be removed. 
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  p,
                  u,
                  li {
                    font-size: 1.25vw;
                    font-family: "Calibri", sans-serif !important;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default PostingRules;
