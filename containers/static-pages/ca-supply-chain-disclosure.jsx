import React, { Component } from "react";

class CaSupplyChainDisclosure extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "22.5pt",
                    marginRight: "0in",
                    marginBottom: "22.5pt",
                    marginLeft: "0in",
                    textAlign: "center",
                    lineHeight: "37.5pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                  align="center"
                >
                  <b>
                    <span style={{ fontSize: "12.0pt", color: "black" }}>
                      CA Supply Chain Disclosure
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "12.0pt",
                    marginRight: "0in",
                    marginBottom: "12.0pt",
                    marginLeft: "0in",
                    lineHeight: "19.2pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    At Boatzon, we have worked hard to become the nation's first
                    boat and marine retailer and attribute our success to
                    remaining true to our core values, which focus on ethical
                    conduct, supporting our local community, and constantly
                    improving. We expect that our associates and business
                    partners share these core values and understand that our
                    local community extends to those who work hard each day to
                    create products that end up in our homes and on our roads.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "12.0pt",
                    marginRight: "0in",
                    marginBottom: "12.0pt",
                    marginLeft: "0in",
                    lineHeight: "19.2pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    While this expectation informs how we do business, the State
                    of California asks that we tell you a little more about how
                    Boatzon works with the companies that bring us the products
                    we offer to you:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "12.0pt",
                    marginRight: "0in",
                    marginBottom: "12.0pt",
                    marginLeft: "0in",
                    lineHeight: "19.2pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    First and foremost, it is Boatzon's policy not to purchase
                    any products made or acquired in any way through the use of
                    forced labor or human trafficking. Our direct importer
                    commits to following this policy and performs routine audits
                    of factories where our products are made and takes prompt
                    action to maintain compliance with local laws. Similarly,
                    Boatzon's vendors commit to us that the products we purchase
                    comply with all applicable laws.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "12.0pt",
                    marginRight: "0in",
                    marginBottom: "12.0pt",
                    marginLeft: "0in",
                    lineHeight: "19.2pt",
                    background: "white",
                    verticalAlign: "baseline",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Because of this expectation, Boatzon does not currently
                    verify, internally or through a third party, their
                    respective product supply chains to evaluate and address the
                    risks of human trafficking and forced labor. Boatzon does
                    not independently audit suppliers to evaluate compliance
                    with our expectation that the supply chains are free from
                    human trafficking and forced labor. Boatzon does not require
                    suppliers to certify that materials incorporated into the
                    final products comply with local anti-trafficking and forced
                    labor laws. While Boatzon expects our associates to share
                    our core values, we do not maintain separate internal
                    accountability standards and procedures for associates or
                    contractors who fail to meet our standards regarding human
                    trafficking and forced labor. Finally, Boatzon does not
                    provide associates or management who have direct
                    responsibility for supply chain management training on human
                    trafficking and forced labor.
                  </span>
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default CaSupplyChainDisclosure;
