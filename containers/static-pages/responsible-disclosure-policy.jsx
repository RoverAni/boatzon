import React, { Component } from "react";

class ResponsibleDisclosurePolicy extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <p
                  className="MsoNormal"
                  style={{
                    marginTop: "37.5pt",
                    marginRight: "0in",
                    marginBottom: "37.5pt",
                    marginLeft: "0in",
                    lineHeight: "27.0pt",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "27.0pt", color: "black" }}>
                      Boatzon’s Responsible Disclosure Policy
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <b>
                    <u>
                      <span style={{ fontSize: "12.0pt", color: "black" }}>
                        Responsible Disclosure
                      </span>
                    </u>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Boatzon’s code of ethics, foundation of trust, and its
                    constant efforts to ensure that we are always acting
                    prudently as a company is built upon the confidence that our
                    customers place in us. As a result of these core values, the
                    security of our online platforms - and the data housed
                    within these platforms - is of paramount importance. If you
                    are a security researcher and believe that you have
                    discovered a security vulnerability involving Boatzon
                    services or sites, we encourage you to securely disclose it
                    to us in a responsible manner, as directed by this
                    Responsible Disclosure Policy (the “Policy”). Boatzon will
                    engage with security researchers when vulnerabilities are
                    reported to us in accordance with this Policy. We will also
                    validate and fix confirmed vulnerabilities affecting our
                    services or sites in accordance with our commitment to
                    security and privacy. We will not take legal action against,
                    or suspend or terminate the accounts of, researchers who
                    discover and report security vulnerabilities in accordance
                    with this Policy. Boatzon reserves all legal rights in the
                    event of any non-compliance with this Policy.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <b>
                    <u>
                      <span style={{ fontSize: "12.0pt", color: "black" }}>
                        Reporting
                      </span>
                    </u>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    We encourage security researchers to share the details of
                    any suspected vulnerabilities with the Boatzon Information
                    Security Team by emailing Security@Boatzon.com. Boatzon will
                    review each submission to determine if the finding: (a) is
                    valid and (b) has not previously been reported. Boatzon and
                    this Policy require security researchers to include detailed
                    information with steps for Boatzon’s Information Security
                    Team to reproduce the vulnerability in the submission in
                    order for a security researcher to be considered for
                    monetary compensation.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <b>
                    <u>
                      <span style={{ fontSize: "12.0pt", color: "black" }}>
                        Boatzon’s Commitment
                      </span>
                    </u>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    If you identify a novel and valid suspected security
                    vulnerability in compliance with this Policy, Boatzon
                    commits to:
                  </span>
                </p>
                <ul type="disc">
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Work with the security researcher(s) to understand and
                      validate the suspected vulnerability; and
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Address any valid vulnerability or risk (as deemed
                      necessary and/or appropriate by Boatzon).
                    </span>
                  </li>
                </ul>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <b>
                    <u>
                      <span style={{ fontSize: "12.0pt", color: "black" }}>
                        Noncompliance With this Policy
                      </span>
                    </u>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Public disclosure - by a security researcher or otherwise -
                    of the details of any identified suspected vulnerability
                    without express written consent from Boatzon’s InfoSec Team
                    will deem any Form submission under this Policy as
                    noncompliant with this Policy.
                    <br />
                    The Form is not intended to be used by, and this Policy is
                    not directed to:
                  </span>
                </p>
                <ul type="disc">
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Employees of Boatzon;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Boatzon’s subsidiaries, affiliates, or partners;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Vendors currently working with or for Boatzon or Boatzon’s
                      subsidiaries, affiliates, or partners; or
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Residents of countries on the United States Office of
                      Foreign Assets Control’s (OFAC) Sanctions List.
                    </span>
                  </li>
                </ul>
                <p
                  className="MsoNormal"
                  style={{ marginBottom: "0in", lineHeight: "normal" }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    In addition, to remain compliant with this Policy, security
                    researcher(s) are prohibited from:
                  </span>
                </p>
                <ul type="disc">
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Accessing, downloading, or modifying data residing in an
                      account that does not belong to the security
                      researcher(s);
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Executing or attempting to execute any “Denial of Service”
                      or related attack against any Boatzon system or service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Posting, transmitting, uploading, linking to, sending, or
                      storing any malicious software on or to any Boatzon system
                      or service;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Testing any suspected vulnerability in a manner that would
                      result in the sending of unsolicited or unauthorized junk
                      mail, spam, pyramid schemes, or any other form of
                      unsolicited message;
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Testing any suspected vulnerability in a manner that would
                      degrade or negatively impact the operation of any Boatzon
                      service or system; and/or
                    </span>
                  </li>
                  <li
                    className="MsoNormal"
                    style={{ color: "black", lineHeight: "normal" }}
                  >
                    <span style={{ fontSize: "12.0pt" }}>
                      Testing third-party applications, websites, or services
                      that integrate with or link to any Boatzon service or
                      system.
                    </span>
                  </li>
                </ul>
                <p className="MsoNormal">
                  <span style={{ color: "black" }}>&nbsp;</span>
                </p>
              </div>

              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                  /* List Definitions */
                  ol {
                    margin-bottom: 0in;
                  }
                  ul {
                    margin-bottom: 0in;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ResponsibleDisclosurePolicy;
