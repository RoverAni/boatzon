import React, { Component } from "react";

class CommunityGuidelines extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="px-4">
                <p className="text-center font-weight-bold">
                  Community guidelines
                </p>
                <p>
                  Boatzon is committed to maintaining a courteous and respectful
                  community. Here are a few tips to help everyone experience an
                  enjoyable experience:
                </p>
                <ul>
                  <li>
                    Be courteous, these are you peers in the marine and boating
                    community
                  </li>
                  <li>
                    If you change your mind on an offer, politely let the seller
                    know so they can work with other buyers.
                  </li>
                  <li>
                    If you're running late to meet someone or shipping your
                    product is delayed, keep the other person updated.
                  </li>
                </ul>
                <p>
                  In addition, when using Boatzon and interacting with buyers,
                  sellers, dealers, or employees, please refrain from any of the
                  behaviors below, in your post or in messages.
                </p>
                <p>The following types behaviors are not allowed:</p>
                <ul>
                  <li>
                    <u>Disrespectful behavior</u>
                  </li>
                  <li>
                    <u>Harassment</u>
                  </li>
                  <li>
                    <u>Spam or unsolicited offers</u>
                  </li>
                  <li>
                    <u>Displaying prohibited profile content</u>
                  </li>
                  <li>
                    <u>Fraudulent or illegal activity</u>
                  </li>
                </ul>
                <p className="font-weight-bold">Disrespectfulness, such as:</p>
                <ul>
                  <li>Profanity or hate speech</li>
                  <li>Threats of violence or actual violence</li>
                  <li>Inciting violence</li>
                  <li>Offensive or vulgar posts or messages</li>
                </ul>
                <p className="font-weight-bold">Harassment, such as:</p>
                <ul>
                  <li>
                    Targeting another person with complaints about items that
                    were not purchased or expressing opinions about items with
                    no intent to purchase
                  </li>
                  <li>
                    Pressuring a seller to make a sale or a buyer to make a
                    purchase
                  </li>
                  <li>Other comments not related to the item</li>
                  <li>
                    Comments related to another person’s looks, relationship
                    status, etc.
                  </li>
                  <li>
                    Any form of harassment including but not limited to hateful,
                    obscene, offensive, profane, racist, sexual or sexually
                    suggestive, defamatory, or violent language
                  </li>
                </ul>
                <p className="font-weight-bold">
                  Spam, unsolicited offers, or abusing our system, such as:
                </p>
                <ul>
                  <li>
                    Unsolicited messages sent to potential buyers, including
                    those of a commercial nature
                  </li>
                  <li>
                    Impersonation of another buyer, seller, Boatzon employee, or
                    other entity in communications
                  </li>
                  <li>
                    Any other misleading, fake, or malicious communication
                  </li>
                  <li>
                    Misrepresenting yourself, or the products you’re selling
                  </li>
                  <li>
                    Harvesting information about our users or directing our
                    users to external websites or apps to collect their
                    information
                  </li>
                  <li>
                    Abusing our reporting system, or otherwise exploiting tools
                    meant to protect the community
                  </li>
                </ul>
                <p className="font-weight-bold">
                  Use of disallowed content in your public profile or custom
                  profile link, such as:
                </p>
                <ul>
                  <li>
                    Offensive or vulgar profile names, links, or photos, or
                    those that infringe on the intellectual property rights of
                    others. Check out our complete list of 
                    <u>username guidelines.</u>
                  </li>
                  <li>Use of the Boatzon name or image</li>
                  <li>Inclusion of personal contact details</li>
                  <li>
                    Posting another person's private information, including
                    contact information
                  </li>
                </ul>
                <p className="font-weight-bold">
                  Fraudulent or illegal activity, such as:
                </p>
                <ul>
                  <li>Falsifying information</li>
                  <li>Using Boatzon for any fraudulent purpose </li>
                  <li>Violating any law or regulation while using Boatzon</li>
                </ul>
                <p>
                  We may remove content that violates these guidelines. In
                  severe cases or where there are  repeated violations, we may
                  suspend or cancel an account or take other action. For more
                  information, please see our Terms of service.
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  p,
                  u,
                  li {
                    font-size: 1.25vw;
                    font-family: "Calibri", sans-serif !important;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CommunityGuidelines;
