import React, { Component } from "react";
import { About_boatzon } from "../../lib/config";

class AboutUs extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    Design DReference:
                  </span>
                  <a href="https://moz.com/about">
                    <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                      https://moz.com/about
                    </span>
                  </a>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "18.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  align="center"
                  style={{ textAlign: "center" }}
                >
                  <span style={{ fontSize: "18.0pt", lineHeight: "107%" }}>
                    Top Header: About Boatzon
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  align="center"
                  style={{ textAlign: "center" }}
                >
                  <span style={{ fontSize: "18.0pt", lineHeight: "107%" }}>
                    Small Text: Honored To Bring A Change
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  align="center"
                  style={{ textAlign: "center" }}
                >
                  <span style={{ fontSize: "18.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <b>
                    <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                      Boatzon Mission
                    </span>
                  </b>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    Boatzon is the first 100% online boat and marine retailer.
                    We are the pioneer in buying a new or used boat, engine,
                    trailer, or any marine product online. We thrive to provide
                    a new and better boat and marine shopping experience using
                    cutting-edge technology, FinTech, and InsureTech solutions.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p
                  className="sc-jlyjg"
                  style={{
                    margin: "0in",
                    lineHeight: "15.0pt",
                    background: "white",
                  }}
                >
                  <b>
                    <span
                      style={{
                        fontSize: "14.0pt",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Boatzon + Online means savings
                    </span>
                  </b>
                </p>
                <p
                  className="sc-csuqgl"
                  style={{
                    marginTop: "6.0pt",
                    marginRight: "0in",
                    marginBottom: "12.0pt",
                    marginLeft: "0in",
                    lineHeight: "18.0pt",
                    background: "white",
                  }}
                >
                  <span
                    style={{
                      fontSize: "14.0pt",
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Since we are the insurance and finance agency, our 100%
                    online process cuts out the middlemen, so you get a lower
                    price from the start. No commissions, no pressure, and no
                    hidden fees. We give you our wholesale rates for all of our
                    services.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <b>
                    <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                      About Boatzon
                    </span>
                  </b>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    We feel the boating and marine industry has been partially
                    ignored on the Internet. The current leading boating and
                    marine sites have not gotten up to speed on technology, they
                    are a bit outdated, and expensive to the market. Then there
                    are sites like Craigslist, OfferUp, and eBay that are great
                    universal product and community selling platforms…but they
                    are not tailored for the marine industry and only provide a
                    “listing” solution for buyers and sellers.{" "}
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    Furthermore, boating services like loans, insurance, and
                    warranties are so confusing and fragmented to consumers.
                    Even experienced boaters pay way too much for these services
                    because of lack of knowledge or going through traditional,
                    expensive options (for example Geico, Allstate,
                    Progressive). We built our <b>own insurance agency</b> and
                    partnered with leading marine banks to provide the lowest
                    and wholesale rates to the boating community.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    So when we set out to develop Boatzon, we put all the
                    players on the whiteboard in this marine ecosystem. We
                    examined the boat listing or classified companies, the
                    social and community selling sites like OfferUp and
                    Craigslist, the Yelp type services for directories, and the
                    boating insurance and finance companies. We said to
                    ourselves, we want to build something special and{" "}
                    <b>
                      <i>integrated</i>
                    </b>{" "}
                    for the marine industry.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    Boatzon today is the first 100% boat and marine retail
                    platform introducing integrated real-time preapprovals for
                    financing, wholesale insurance, and a whole new buying
                    experience for the boating and marine industry.{" "}
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    The platform also embraces all the modern and easy
                    boat/product posting features, an International marketplace,
                    worldwide shipping, integrated FinTech and InsureTech
                    solutions, a marine business directory, an interactive
                    Projects &amp; Advice module, live video between sellers and
                    buyers, virtual reality technology, and many other features
                    tailored for the boating community.{" "}
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <div className="row">
                  <div className="col-10 mx-auto">
                    <img src={About_boatzon} width="100%" />
                  </div>
                </div>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <b>
                    <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                      From The CEO
                    </span>
                  </b>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    First and foremost, I hope you enjoy the Boatzon experience{" "}
                    <b>and I am honored</b> to provide a service for an industry
                    and community that I love so much.{" "}
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    Before my journey as a technology entrepreneur, most of
                    childhood pictures are growing up on water and fishing. When
                    I say I am honored to provided Boatzon to the marine
                    community, that statement comes from a history of passion
                    and love for the community.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    I promise we will always put our community first and provide
                    an authentic experience. I will always share my personal
                    email{" "}
                  </span>
                  <a href="mailto:Bryan@Boatzon.com">
                    <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                      Bryan@Boatzon.com
                    </span>
                  </a>
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    {" "}
                    with the Boatzon community to be engaged with you, get
                    feedback and ideas from you, and offer any level of help.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "14.0pt", lineHeight: "107%" }}>
                    Bryan &amp; The Boatzon Team
                  </span>
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  a:link,
                  span.MsoHyperlink {
                    color: #0563c1;
                    text-decoration: underline;
                  }
                  p.sc-jlyjg,
                  li.sc-jlyjg,
                  div.sc-jlyjg {
                    mso-style-name: sc-jlyjg;
                    margin-right: 0in;
                    margin-left: 0in;
                    font-size: 12pt;
                    font-family: "Times New Roman", serif;
                  }
                  p.sc-csuqgl,
                  li.sc-csuqgl,
                  div.sc-csuqgl {
                    mso-style-name: sc-csuqgl;
                    margin-right: 0in;
                    margin-left: 0in;
                    font-size: 12pt;
                    font-family: "Times New Roman", serif;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                  /* List Definitions */
                  ol {
                    margin-bottom: 0in;
                  }
                  ul {
                    margin-bottom: 0in;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AboutUs;
