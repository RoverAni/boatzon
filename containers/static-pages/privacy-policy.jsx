import React, { Component } from "react";

class PrivacyPolicy extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <p
                  className="MsoNormal"
                  align="center"
                  style={{
                    textAlign: "center",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "18.0pt", color: "black" }}>
                    Boatzon Privacy Policy
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  align="center"
                  style={{
                    textAlign: "center",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Last Revised and Effective: 1/11/2021
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>A. Introduction</span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Boatzon Holdings, L.L.C. and/or subsidiaries (“us,” “we,” or
                    “our”) collects and processes data and information about you
                    as described in this Privacy Policy (“Policy”). Boatzon is
                    committed to protecting the privacy of those with whom we
                    interact with online and offline. This Policy contains
                    details about how Boatzon collects, uses, and shares
                    personal information that we obtain from and about you when
                    you interact with us via our website(s), email, mobile
                    application(s), and in other online and offline
                    interactions. Please read this Policy carefully.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We may change this Privacy Policy from time to time. If we
                    make changes, we will notify you by revising the date at the
                    top of the policy and, in some cases, we may provide you
                    with more prominent notice (such as adding a statement to
                    our homepage or sending you an email notification). We
                    encourage you to review the Privacy Policy whenever you
                    access the Boatzon website and service to stay informed
                    about our information practices and the ways you can help
                    protect your privacy.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>Applicability:</span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We collect information in several contexts as
                    described below. However, this Policy does not apply to the
                    following information:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Information we collect in connection with providing
                    individual consumers with a financial product or service.
                    Such information is subject instead to our&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="https://www.carvana.com/privacy-policy">
                      <span style={{ color: "black", textDecoration: "none" }}>
                        Financial Privacy Policy
                      </span>
                    </a>
                  </span>
                  <span style={{ color: "black" }}>.</span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Information we collect from individuals with whom we engage
                    in solely business-to-business communications and
                    transactions, such as information about the employees of our
                    business partners and customers.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      B. Sources of Personal Information
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We collect data and information about you and how you
                    interact with us in several ways, including:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Information you provide to us directly.
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We collect the information you provide to us directly.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Payment Information Collected by Stripe
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    . Boatzon is integrated with Stripe Inc.’s (“Stripe”)
                    services to allow sellers to accept credit card and other
                    electronic payment methods for goods they list for sale on
                    the Boatzon Service. Stripe provides these services directly
                    to sellers pursuant to its own terms and privacy policy.
                    When signing up to receive funds electronically, Stripe, and
                    not Boatzon, collects some of the information provided by
                    sellers via the Boatzon Service, including debit card
                    numbers, Social Security numbers, government IDs, and bank
                    account information.{" "}
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    3.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Information automatically collected from your online
                      interaction with Boatzon.&nbsp;
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    We automatically collect technical information about your
                    online interactions with us (such as IP address and browsing
                    preferences).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    4.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Information from public sources,
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;including government entities from which public
                    records are obtained and information you submit in public
                    forums
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    5.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Information from third parties.
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We receive information about you and your interactions
                    with us from third parties, such as credit reporting
                    entities, marketing and advertising providers, social
                    network services, data brokers, and companies that provide
                    or sell lists of potential purchasers.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We may combine information that we receive from the various
                    sources described in this Policy, including third party
                    sources and public sources, and use or disclose it for the
                    purposes identified below.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      C. Types of Personal Information Boatzon Collects
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    The types of information that we collect about you
                    (“Personal Information”) include:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Personal data and identifiers&nbsp;such as your name, alias,
                    postal address, country of residence, unique personal
                    identifier, online identifier, internet protocol address,
                    email address, account name, phone number, social media
                    identifiers (e.g., Twitter handle, Instagram name, etc.)
                    social security number, driver’s license number, passport
                    number, or other similar identifiers.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Demographic information,&nbsp;such as age, race, gender,
                    gender identity, national origin, religion, sex,
                    citizenship, medical condition, military or veteran status,
                    education, or date of birth.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    3.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Commercial information&nbsp;including records of personal
                    property, creditworthiness, products or services purchased,
                    obtained, or considered, or other purchasing or consuming
                    histories or tendencies.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    4.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Geolocation information and approximate location based on
                    your IP address or precise location with your express
                    consent.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    5.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Audio, electronic, visual, such as call center recordings,
                    or customer support chat logs.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    6.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Professional, employment, or education information such as
                    job title, employer, business address and contact
                    information, employment history, other professional
                    information, or education history.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    7.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Education information&nbsp;that is not publicly available
                    personally identifiable information as defined in the Family
                    Educational Rights and Privacy Act.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      D. How We Use Your Personal Information
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We may use each category of your information described above
                    in the following ways:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    To enable interactions between you and Boatzon such as to
                    facilitate online purchases of boats, engines, trailers,
                    products; posting boats and products for sale; process
                    shipping and returns; register and administer your account,
                    provide you with and support your interactions with us;
                    facilitate an order, download, return, or exchange; provide
                    requested boat, engine, trailer, product information;
                    communicate with you about your account or our data
                    practices; install and configure changes and updates to
                    programs and technologies related to interactions with us;
                    authenticate those who interact with us.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    For legal or security reasons,&nbsp;such as to comply with
                    legal requirements; protect our safety, our property or
                    rights of those who interact with us, or others; and detect,
                    prevent, and respond to security incidents or other
                    malicious, deceptive, fraudulent, or illegal activity.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    3.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    For marketing.&nbsp;We may use your Personal Information to
                    market our products or services or those of third parties,
                    such as our business partners. We may use your Personal
                    Information to invite you to participate in marketing
                    surveys, questionnaires, promotions, events or contests. We
                    may use your Personal Information to enhance our content and
                    to deliver advertisements to you, including commercial
                    emails.{" "}
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    4.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    For any other purposes for which you provide consent.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      E. With Who Do We Share Your Personal Information
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We may share your Personal Information with the categories
                    of recipients described below:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Affiliates and subsidiaries:
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We may share your Personal Information within the
                    Boatzon group of companies, which includes corporate
                    affiliates, subsidiaries, business units and other companies
                    that share common ownership for the purposes described
                    above.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>Service providers:</span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We may share your Personal Information with service
                    providers working on our behalf in order to facilitate our
                    interactions with you or request or support our relationship
                    with you, such as hosting service providers, IT providers,
                    operating systems and platforms, internet service providers,
                    analytics companies, and marketing providers (e.g., we may
                    share your email address with our outbound email marketing
                    provider). We may contract with other companies to provide
                    certain services, including identity verification, email
                    distribution, market research, promotions management, and
                    payment processing. We take reasonable efforts to provide
                    these companies with only the information they need to
                    perform services on our behalf.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    3.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      Third parties necessary to complete a transaction or
                      request.
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We may disclose information to third parties to
                    provide you with services or benefits you may request, such
                    as shipping vendors and consumer credit reporting companies.
                    We may also share information with third parties at your
                    direction, such as if you direct us to send or receive
                    information from third parties using Plaid.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    4.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>Business partners:</span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We may also provide your Personal Information or
                    provide access to your Personal Information to our business
                    partners who may use it for their own purposes, such as
                    supplemental product providers, including those who offer
                    marine accessories, marine warranties etc., social network
                    services, and lead generators.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    5.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      For legal, security and safety purposes:
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We may share your Personal Information with third
                    parties such as law enforcement or other government agencies
                    to comply with law or legal requirements; to enforce or
                    apply our Terms of Use and other agreements; and to protect
                    our rights and our property or the safety of our users or
                    third parties.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    6.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>
                      In connection with a corporate transaction:
                    </span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;We may transfer any information we have about you in
                    connection with a change in corporate control, including but
                    not limited to a merger or sale (including transfers made as
                    part of insolvency or bankruptcy proceedings) involving all
                    or part of Boatzon or as part of a corporate reorganization
                    or stock sale.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We may also de-identity, anonymize, or aggregate Personal
                    Information to share with third parties for any purpose.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      F. How We Use Cookies and Automatic Data Collection Tools
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We also collect information, via cookies, web beacons,
                    pixels, tags, or other tracking technologies (“Tracking
                    Technologies”), such as your Internet Service Provider and
                    IP address, the date and time you access our website(s), the
                    pages you accessed while visiting our website(s), and the
                    Internet address from which you accessed our website(s). We
                    may place cookies on your device, which may last only during
                    a single session or may be persistent over multiple sessions
                    over time. We use these technologies to remember user
                    preferences, maximize the performance of our website(s) and
                    services, provide you with offers that may be of interest to
                    you, measure the effectiveness of our email and other
                    advertising campaigns, and to personalize online content.
                    These Tracking Technologies may be used to track you across
                    devices and websites over time.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Additionally, we may permit third parties to place Tracking
                    Technologies on our website(s) to allow these third parties
                    to directly collect personal information from you. These
                    third parties may also use Tracking Technologies to serve
                    you advertisements tailored to interests you have shown by
                    browsing on this website and other sites, applications,
                    destinations, and services you have visited, and for other
                    lawful business purposes. In doing so, these third parties
                    may collect your Personal Information, including for example
                    the make, model, settings, specifications (e.g., CPU speed,
                    connection speed, browser type, operating system, device
                    identifier) and geographic location of your computer, mobile
                    or other device, as well as date/time stamp, IP address,
                    pages visited, time of visits, content viewed, ads viewed,
                    the site(s), application(s), destination(s), and/or
                    service(s) you arrived from, and other clickstream data.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    To provide you with more relevant and interesting
                    experiences, we may work with third party companies to
                    display ads or customize the content on this website and on
                    other digital properties and websites. For example:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    We use Google Analytics to analyze traffic to our Web
                    site(s) in order to help us understand our customers' and
                    visitors' needs and to continually improve our site for
                    them. The data they collect on our behalf may include, but
                    is not limited to, search engine referral, affiliate
                    referrals, traffic driven by banner ads or other online
                    promotions, how visitors navigate around the site, and the
                    most popular pages. We also collect certain technical
                    information, such as the browser version and operating
                    system. We reserve the right to use, transfer, sell, and
                    share data about our users for any lawful business purpose,
                    such as analyzing usage trends and seeking compatible
                    advertisers, sponsors, clients, and customers. For more
                    information related to our use of Google Analytics, please
                    visit&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="http://www.google.com/policies/privacy/partners/">
                      <span style={{ color: "black", textDecoration: "none" }}>
                        www.google.com/policies/privacy/partners/
                      </span>
                    </a>
                  </span>
                  <span style={{ color: "black" }}>.</span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    We use Facebook Pixel and/or related Facebook business tools
                    that may allow third parties, including Facebook, to use
                    cookies, web beacons, and other storage technologies to
                    collect or receive information from our website(s), app(s),
                    and/or elsewhere on the Internet and use that information to
                    provide measurement services and target ads.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    These companies may use cookies and similar tracking
                    technologies as described in this Policy to gather
                    information about your visits to our website(s), as well as
                    your visits elsewhere on the Internet. These companies use
                    this information to provide you with more relevant
                    advertising known as interest-based advertising. For more
                    information about third-party advertisers, please
                    visit&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="http://www.networkadvertising.org/choices/">
                      <span style={{ color: "black", textDecoration: "none" }}>
                        http://www.networkadvertising.org/choices/
                      </span>
                    </a>
                  </span>
                  <span style={{ color: "black" }}>
                    . This is a site offered by the Network Advertising
                    Initiative ("NAI") that includes information on how
                    consumers can opt-out from receiving interest-based
                    advertising from some or all of NAI's members. The Digital
                    Advertising Alliance (“DAA”) also offers a choice mechanism
                    with respect to certain types of data collection and use by
                    third parties available at&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="http://www.aboutads.info/">
                      <span style={{ color: "black", textDecoration: "none" }}>
                        www.aboutads.info
                      </span>
                    </a>
                  </span>
                  <span style={{ color: "black" }}>
                    . Opting out of interest-based advertising will not opt you
                    out of all advertising, but rather only interest-based
                    advertising from us or our agents or representatives.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Some browsers have incorporated Do Not Track (“DNT”)
                    preferences. Most of these features, when turned on, send
                    signals to the website you are visiting that you do not wish
                    to have information about your online searching and browsing
                    activities collected and used. As there is not yet a common
                    agreement about how to interpret DNT signals, we do not
                    honor DNT signals from website browsers at this time.
                    However, you may refuse or delete cookies. If you refuse or
                    delete cookies, some of our website(s) functionality may be
                    impaired. If you change computers, devices, or browsers, or
                    use multiple computers, devices, or browsers, and delete
                    your cookies, you may need to repeat this process for each
                    computer, device, or browser. Please refer to your browser’s
                    Help instructions to learn more about how to manage cookies
                    and the use of other tracking technologies.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      G. Security and Retention
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    We maintain reasonable security procedures and technical and
                    organizational measures to protect your Personal Information
                    against accidental or unlawful destruction, loss,
                    disclosure, alteration, or use. However, because no data
                    transmission is completely secure, and no system of physical
                    or electronic security is impenetrable, we cannot guarantee
                    the security of the information you send to us or the
                    security of our servers, networks or databases, and by using
                    our Service you agree to assume all risk in connection with
                    the information sent to us or collected by us when you
                    access, visit and/or use our Service, including without
                    limitation your personally identifiable information or other
                    Registration Information, and we are not responsible for any
                    loss of such information or the consequences thereof.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      H. Children’s Privacy
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Interactions with us are intended for individuals 14 years
                    of age and older. Our interactions are not directed at,
                    marketed to, nor intended for, children under 14 years of
                    age. We do not knowingly collect any information, including
                    Personal Information, from children under 16 years of age.
                    If you believe that we have inadvertently collected Personal
                    Information from a child under the age of 14, please contact
                    us at the address below and we will use reasonable efforts
                    to delete the child’s information from our databases.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>I. External Links</span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    When interacting with us you may encounter links to external
                    sites or other online services, including those embedded in
                    third party advertisements or sponsor information, that we
                    do not control. We are not responsible for the privacy
                    practices and data collection policies for such third party
                    services. You should consult the privacy statements of those
                    third party services for details.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>K. Contact Info</span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ color: "black" }}>
                    If you have questions regarding this Policy, please contact
                    us at:
                  </span>
                </p>
                <table
                  className="MsoNormalTable"
                  border={1}
                  cellSpacing={0}
                  cellPadding={0}
                  style={{ borderCollapse: "collapse", border: "none" }}
                >
                  <tbody>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            EMAIL:
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderLeft: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            privacy@Boatzon.com
                          </span>
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            MAIL:
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            Boatzon, LLC
                            <br />
                            20801 Biscayne Blvd
                          </span>
                        </p>
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            Aventura, Florida 33180
                          </span>
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>&nbsp;</span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Note: Only inquiries involving this Privacy Policy or
                    Boatzon’s collection, use, and/or management of your
                    personal information should be sent to this email and/or
                    mailing address. No other communication will be accepted or
                    addressed.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    To opt-out of receiving promotional email messages from us,
                    please click on the "Unsubscribe" link contained at the
                    bottom of each email or by contacting us using the
                    information above.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ color: "black" }}>
                      L. Your California Privacy Rights
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    This section applies to certain California residents and
                    supplements (but does not replace) the Policy above. This
                    section does not apply to the following information:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Information about individuals who are not California
                    residents.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Information we collect in connection with providing
                    individual consumers with a financial product or service.
                    Such information is subject instead to our&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="https://www.carvana.com/privacy-policy">
                      <span style={{ color: "black", textDecoration: "none" }}>
                        Financial Privacy Policy
                      </span>
                    </a>
                  </span>
                  <span style={{ color: "black" }}>..</span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Information we collect from individuals with whom we engage
                    in solely business-to-business communications and
                    transactions, including due diligence transactions, such as
                    information about the employees of our business clients.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ color: "black" }}>
                      We do not “sell” the personal information we collect (and
                      will not sell it in the future without providing a right
                      to opt out).
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ color: "black" }}>
                    We collect personal information for the business and
                    commercial purposes listed in the grid below. We share the
                    personal information we collect with the categories of third
                    parties listed in the grid below.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ color: "black" }}>
                    Subject to certain limitations, the CCPA provides California
                    residents with the right to: request to know about the
                    personal information that we have collected about you in the
                    12 months preceding your request (including the categories
                    of information collected, the source of that information,
                    the business purpose of that collection, the categories of
                    third parties with whom that information is shared, and the
                    specific pieces of personal information collected about
                    you); request to delete any personal information about you
                    that we have collected; opt-out of “sales” of personal
                    information to third parties, if applicable; and not to be
                    discriminated against for exercising these rights.
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ color: "black" }}>&nbsp;</span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ color: "black" }}>
                    To submit a request to know or a request to delete, please
                    send an email to Privacy@Boatzon.com. In both cases, we will
                    verify your request by asking you to provide information
                    that matches information we have on file about you. You may
                    designate another person as your authorized agent to act on
                    your behalf with regard to CCPA requests. If you have
                    designated an authorized agent to submit requests on your
                    behalf, we will require proof that the person is authorized
                    to act on your behalf and may also still ask you to verify
                    your identity with us directly.
                  </span>
                </p>
                <table
                  className="MsoNormalTable"
                  border={1}
                  cellSpacing={0}
                  cellPadding={0}
                  style={{ borderCollapse: "collapse", border: "none" }}
                >
                  <tbody>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          align="center"
                          style={{
                            marginBottom: "0in",
                            textAlign: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <b>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Categories of Personal Information Boatzon
                              Collects
                            </span>
                          </b>
                        </p>
                      </td>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderLeft: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          align="center"
                          style={{
                            marginBottom: "0in",
                            textAlign: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <b>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Categories of Internal &amp; Third Parties With
                              Whom We Disclose Personal Information for a
                              Business Purpose
                            </span>
                          </b>
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Identifiers
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , such as name, alias, postal address, unique
                            personal identifier, online identifier, IP address,
                            email address, account name, social security number,
                            driver’s license number, passport number, or other
                            similar identifiers.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as those who
                            offer marine accessories, insurance, warranties,
                            etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Personal information subject to the California
                              Customer Records Act
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , such as signatures.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Characteristics of protected classifications under
                              California or Federal Law
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , such as age, sex (including gender, gender
                            identity, gender expression), citizenship, religion
                            or creed, marital status, medical condition,
                            pregnancy or childbirth, veteran or military status.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Commercial information
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , including records of personal property, product or
                            services purchased, obtained or considered, or other
                            purchasing or consuming histories or tendencies.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Internet or other electronic network activity
                              information
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , including, but not limited to, browsing history,
                            search history, and information regarding a
                            consumer’s interaction with an internet website,
                            application or advertisement.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Geolocation data
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , such as approximate location of a device or
                            equipment based on geographical coordinates and
                            measurements, including on company issued devices,
                            company-controlled phones, etc.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Audio, electronic, visual, thermal, olfactory, or
                              similar information
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            &nbsp;such as CCTV footage, call recordings, web
                            activity recordings, etc.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style={{
                          border: "solid black 1.0pt",
                          borderTop: "none",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <p
                          className="MsoNormal"
                          style={{
                            marginBottom: "0in",
                            lineHeight: "normal",
                          }}
                        >
                          <u>
                            <span style={{ fontSize: "9.0pt", color: "black" }}>
                              Professional, employment, or education information
                            </span>
                          </u>
                          <span style={{ fontSize: "9.0pt", color: "black" }}>
                            , such as job title, employer, business address and
                            contact information, employment history, other
                            professional information, or education history.
                          </span>
                        </p>
                      </td>
                      <td
                        style={{
                          borderTop: "none",
                          borderLeft: "none",
                          borderBottom: "solid black 1.0pt",
                          borderRight: "solid black 1.0pt",
                          padding: "11.25pt 11.25pt 11.25pt 11.25pt",
                        }}
                      >
                        <ul type="circle">
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Service providers, such as identity verification
                            services, financial services, technical support,
                            research and analytics providers, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Third party social network services
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Our subsidiaries, company divisions, and affiliates
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Marketing and advertising partners or providers
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Public or law enforcement officials, as well as
                            private parties involved in other legal process
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Consumer credit reporting companies
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Companies to whom your data is shared at your
                            direction
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Supplemental product providers, such as marine
                            accessories, insurance, warranties, etc.
                          </li>
                          <li
                            className="MsoNormal"
                            style={{
                              color: "black",
                              marginBottom: "11.25pt",
                              lineHeight: "normal",
                            }}
                          >
                            Entities involved in a corporate transaction
                          </li>
                        </ul>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>&nbsp;</span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Collection of the categories of personal information
                    identified above may occur through one or more of the
                    following sources:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    From you, when you engage in certain activities such as:
                    registering and/or setting up an account/profile to access
                    Boatzon.com, Boatzon’s mobile applications, Boatzon’s
                    Insurance platform(s), Boatzon’s Insurance platform(s),
                    Boatzon’s Warranty platform(s) or related Boatzon platforms
                    and/or Service(s), and navigating and/or utilizing Boatzon’s
                    website(s), mobile, and/or related Service(s).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    From third party social networking service(s) that you may
                    have accessed, visited, and/or used that may be integrated
                    with or linked to the Service(s).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    From lead generation partners or sources.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    From identity verification partners.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    From entities that sell or share your personal information,
                    including entities registered in California as data brokers.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    From service providers who gather personal information from
                    you or external parties who possess and/or process your
                    personal information, once you grant the requisite right,
                    power, and/or authority to those service providers to act on
                    your behalf and gather and/or provide such data to us.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Through Local Device Storage (placing and/or storing code or
                    other types of information and/or devices (e.g., "cookies")
                    on your computer, mobile or other device) or Tracking
                    Technologies (web beacons, web bugs, clear gifs, and similar
                    technologies).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    The business and/or or commercial purpose(s) for which we
                    collect the data identified above may include one or more of
                    the following:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Auditing, such as auditing related to a current interaction
                    with you and concurrent transactions, including, but not
                    limited to, counting ad impressions to unique visitors,
                    verifying positioning and quality of ad impressions, and
                    auditing compliance with this specification and other
                    standards;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Debugging, such as debugging to identify and repair errors
                    that impair existing intended functionality;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    The occurrence of mergers and acquisitions, including in the
                    event Boatzon undergoes a corporate reorganization such as a
                    merger, acquisition, sale of part or all of its business, or
                    other related event, in which data may be collected, used,
                    or shared for the purpose of negotiating or completing such
                    transactions;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Performing or improving services, such as maintaining or
                    servicing accounts, providing customer service, providing
                    consumers with requested or personalized content,
                    communicating with you about new features, products, or
                    services, processing or fulfilling orders and transactions,
                    tailoring the services that we offer to your needs;
                    verifying your information, processing payments, providing
                    financing, providing analytic services, or providing similar
                    services on behalf of Boatzon;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Quality assurance, including undertaking activities to
                    verify or maintain the quality or safety of a service or
                    device that is owned, manufactured, manufactured for, or
                    controlled by Boatzon, and to improve, upgrade, or enhance
                    the Service or a device that is owned, manufactured,
                    manufactured for, or controlled by Boatzon;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Security, such as detecting security incidents, protecting
                    against malicious, deceptive, fraudulent, or illegal
                    activity, and/or prosecuting those responsible for that
                    activity;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Short-term, transient use;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    To communicate, advertise, and/or market Boatzon’s goods,
                    products, and/or services, to show you content and sponsored
                    messaging, to engage in interest-based advertising, to gauge
                    the effectiveness of marketing campaigns, and/or for other
                    marketing or advertising purposes;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    To comply with legal or compliance requirements, such as
                    compliance training, investigating and responding to legal
                    claims, responding to law enforcement requests, or as
                    otherwise required by law;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    To conduct internal research, including undertaking internal
                    research for technological development and/or demonstration
                    and analyzing trends;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    To help diagnose problems with our servers, to gather broad
                    demographic information, track users' movements around the
                    Service, and/or to otherwise administer the Service; and/or
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    To track geographic location, usage patterns, broad
                    demographic information, and/or the movements of individual
                    users.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <u>
                    <span style={{ color: "black" }}>Your Rights</span>
                  </u>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    California residents may exercise the following rights,
                    subject to legal limitations and applicable exceptions by
                    calling 833-BOATZON.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>Right to Know.</span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;You have the right to request information about the
                    categories of personal information we have collected about
                    you, the categories of sources from which we collected the
                    personal information, the purposes for collecting the
                    personal information, and the categories of third parties
                    with whom we have shared or sold your personal information
                    (“Categories Report”). You may also request information
                    about the specific pieces of personal information we have
                    collected about you (“Specific Pieces Report”).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>Right to Delete.</span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;You have the right to request that we delete personal
                    information that we have collected from you.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <b>
                    <span style={{ color: "black" }}>Right to Opt Out.</span>
                  </b>
                  <span style={{ color: "black" }}>
                    &nbsp;You have the right to opt out of the sale of your
                    personal information.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    In accordance with applicable law, we will not discriminate
                    against you for exercising these rights.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <u>
                    <span style={{ color: "black" }}>Verification:</span>
                  </u>
                  <span style={{ color: "black" }}>
                    &nbsp;In order to exercise your rights, we may need to
                    obtain information to locate you in our records, verify that
                    you are a California resident, and/or verify your identity
                    depending on the nature of the request. If you are
                    submitting a request on behalf of a household, we may need
                    to verify each member of the household in the manner set
                    forth in this section.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    Please be aware there might be circumstances where we will
                    not honor you request, as permitted under the CCPA. For
                    example, if we are not able to verify your identity and/or
                    that you are a California resident, we may not honor your
                    access, deletion, or opt-out request. We will send a
                    verification email that you must respond to in order for us
                    to proceed with a Right to Delete or Right to Know request
                    as part of the verification process. Additionally, we may
                    not delete your personal information if an exception under
                    the CCPA applies to your deletion request.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    For a Specific Pieces Report, we may request data such as
                    your name, email address, home address, and date of birth to
                    verify your identity, along with a signed declaration, under
                    penalty of perjury, that you are who you say you are. We may
                    also request data such as the last four digits of your
                    Social Security Number if you use Boatzon’s financing
                    services.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    For a Categories Report, we may request data such as your
                    name, email address, and home address to verify your
                    identity.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    For a Right to Delete request, we may request data such as
                    your name, email address, home address, and date of birth to
                    verify your identity, along with a signed declaration, under
                    penalty of perjury, that you are who you say you are. We may
                    also request the last four digits of your Social Security
                    Number if you use Boatzon’s financing services.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    The verification information you provide must match the
                    information currently associated with your Boatzon.com
                    account. We may request alternative information under
                    certain circumstances and will inform you if such
                    information is needed in the verification process.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <u>
                    <span style={{ color: "black" }}>Authorized Agents</span>
                  </u>
                  <span style={{ color: "black" }}>
                    : Authorized agents may exercise rights on behalf of
                    consumers, but we reserve the right to also verify the
                    consumer directly as described above. Authorized agents must
                    use the&nbsp;
                  </span>
                  <span style={{ color: "black" }}>
                    <a href="https://privacyportal.carvana.com/">
                      <span style={{ color: "black", textDecoration: "none" }}>
                        Privacy Portal
                      </span>
                    </a>
                  </span>
                  <span style={{ color: "black" }}>
                    &nbsp;and upload documentation demonstrating the agent has
                    authority to exercise rights on the consumer’s behalf. At a
                    minimum, we will require evidence of the agent’s identity,
                    proof of registration with the California Secretary of
                    State, and at least one of the following evidencing proof of
                    your legal authority to act on the behalf of the individual
                    consumer:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Written authorization signed by the Consumer; or
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    marginLeft: "54.75pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: '"Courier New"',
                      color: "black",
                    }}
                  >
                    o
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ color: "black" }}>
                    Certified copy of a Power of Attorney granted under Probate
                    Code.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <u>
                    <span style={{ color: "black" }}>Timing</span>
                  </u>
                  <span style={{ color: "black" }}>
                    : We will process a Request to Opt Out within 15 business
                    days. We will respond to Requests to Delete and Requests to
                    Know within 45 calendar days, unless we need more time (in
                    which case we will notify you), and may take up to 90
                    calendar days in total to respond to your request(s).
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <u>
                    <span style={{ color: "black" }}>
                      California Shine the Light
                    </span>
                  </u>
                  <span style={{ color: "black" }}>
                    : If you are a California resident, you may opt out of
                    sharing your Personal Information with third parties for the
                    third parties’ direct marketing purposes. Please contact us
                    at privacy@Boatzon.com if you would like to do so.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginBottom: "11.25pt",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ color: "black" }}>
                    If you don't agree to the terms contained in this Privacy
                    Policy, you must immediately exit the Service.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ color: "black" }}>&nbsp;</span>
                </p>
              </div>

              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                  /* List Definitions */
                  ol {
                    margin-bottom: 0in;
                  }
                  ul {
                    margin-bottom: 0in;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default PrivacyPolicy;
