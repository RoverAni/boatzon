import React, { Component } from "react";

class ShippingPolicies extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <p
                  className="MsoNormal"
                  align="center"
                  style={{
                    marginBottom: "8.05pt",
                    textAlign: "center",
                    lineHeight: "normal",
                  }}
                >
                  <b>
                    <span style={{ fontSize: "12.0pt" }}>
                      Shipping Policies
                    </span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt" }}>
                    In order to make Boatzon shipping transactions as smooth as
                    possible, follow the policies below.&nbsp;In cases of severe
                    or repeated violations, we may suspend or cancel an account
                    or take other action.&nbsp;Shipping is subject to the&nbsp;
                  </span>
                  <a href="https://offerup.com/terms/">
                    <span style={{ fontSize: "12.0pt", color: "#050505" }}>
                      Boatzon Terms of Service
                    </span>
                  </a>
                  <span style={{ fontSize: "12.0pt" }}>.</span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt" }}>Eligible items</span>
                  </b>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <span style={{ fontSize: "12.0pt" }}>
                    To be eligible for shipping, items must meet these criteria.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Sellers must use Boatzon’s provided label. That's how we
                    track shipments and sellers get
                    paid.&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Event tickets are not allowed to be shipped.&nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt" }}>
                    3.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Items must be available for immediate shipping. Drop-shipped
                    items are ineligible. Drop-shipping is when a seller
                    purchases items from another retailer and directly ships to
                    a buyer. This type of product sourcing is not allowed on
                    Boatzon. Sellers must send the actual item, not a letter or
                    promise of delivery in the shipment.&nbsp;
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt" }}>Restrictions</span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Sellers are responsible for complying with all applicable
                    laws and regulations regarding mailing. Please check
                    the&nbsp;
                  </span>
                  <a href="https://www.usps.com/ship/shipping-restrictions.htm">
                    <span style={{ fontSize: "12.0pt", color: "#050505" }}>
                      US Postal Service shipping restrictions
                    </span>
                  </a>
                  <span style={{ fontSize: "12.0pt" }}>&nbsp;</span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Certain things are not allowed to be sold on Boatzon. Check
                    the&nbsp;
                    <u>
                      <span style={{ color: "#050505" }}>
                        Prohibited Item guidelines
                      </span>
                    </u>
                    &nbsp;to ensure that they are OK to sell on Boatzon. Also
                    please review our&nbsp;
                    <u>
                      <span style={{ color: "#050505" }}>Posting rules</span>
                    </u>
                    &nbsp;and&nbsp;
                    <u>
                      <span style={{ color: "#050505" }}>
                        Community guidelines
                      </span>
                    </u>
                    .
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt" }}>Fees</span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Sellers are responsible for paying a service fee. Shipping
                    costs are typically paid by the buyer but can be paid by the
                    seller. Shipping fees can vary, so the amounts are shown for
                    each item. You'll know how much you'll pay before you buy.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Boatzon will not reimburse postage if a package is held by
                    USPS with the reason “postage due.”&nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    As a seller, if you didn’t select the correct label weight
                    and more postage is due, do not ship the item. You will need
                    to cancel the transaction and edit your package weight in
                    your posting. When the buyer makes a new offer, print the
                    updated label.&nbsp;
                  </span>
                </p>
                <p className="MsoNormal" style={{ lineHeight: "normal" }}>
                  <b>
                    <span style={{ fontSize: "12.0pt" }}>
                      Shipped Items and Buyer Protection Claims
                    </span>
                  </b>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Only use Boatzon to transact. Sellers should not take
                    payments outside of Boatzon.&nbsp;When an item is&nbsp;
                  </span>
                  <a href="https://help.offerup.com/hc/en-us/articles/360031987532">
                    <span style={{ fontSize: "12.0pt", color: "#050505" }}>
                      paid for and shipped
                    </span>
                  </a>
                  <span style={{ fontSize: "12.0pt" }}>
                    &nbsp;through Boatzon, the buyer may be&nbsp;
                  </span>
                  <a href="https://help.offerup.com/hc/en-us/articles/360032329891-Buyer-protection-on-OfferUp">
                    <span style={{ fontSize: "12.0pt", color: "#050505" }}>
                      protected
                    </span>
                  </a>
                  <span style={{ fontSize: "12.0pt" }}>
                    &nbsp;if certain problems occur, but only if the transaction
                    and payment were completed through Boatzon.
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    Sellers are responsible for selling the item that was
                    purchased as described in the post.&nbsp;
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "10.0pt", fontFamily: "Symbol" }}>
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt" }}>
                    We will not honor any situations that indicate false claims,
                    abuse, or return fraud.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span style={{ fontSize: "12.0pt", lineHeight: "107%" }}>
                    &nbsp;
                  </span>
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                  /* List Definitions */
                  ol {
                    margin-bottom: 0in;
                  }
                  ul {
                    margin-bottom: 0in;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShippingPolicies;
