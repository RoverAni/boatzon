import React, { Component } from "react";

class ProhibitedItemsGuidelines extends Component {
  render() {
    return (
      <div className="bgGreyPage">
        <div className="container p-md-0">
          <div className="screenWidth mx-auto p-0">
            <div className="bgWhite py-5">
              <div className="WordSection1 px-4">
                <h1
                  align="center"
                  style={{
                    marginTop: "0in",
                    marginRight: "0in",
                    marginBottom: "8.05pt",
                    marginLeft: "0in",
                    textAlign: "center",
                  }}
                >
                  <span
                    style={{
                      fontSize: "12.0pt",
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Prohibited Items Guidelines
                  </span>
                </h1>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Although most marine products can be sold on Boatzon,
                    certain items aren't allowed. Some present legal risks or
                    may pose health and safety issues. We've also chosen not to
                    allow items that we feel are inappropriate for our community
                    or could be considered offensive.
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Before posting your items, please read the guidelines below
                    to ensure that they are allowed to sell on Boatzon. Also
                    please review our&nbsp;
                  </span>
                  <a href="https://help.offerup.com/hc/en-us/articles/360032334871">
                    <span
                      style={{
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Posting rules
                    </span>
                  </a>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;and&nbsp;
                  </span>
                  <a href="https://help.offerup.com/hc/en-us/articles/360031988352">
                    <span
                      style={{
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Community guidelines
                    </span>
                  </a>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    .&nbsp;Boatzon may make exceptions to these rules for
                    certain verified sellers in its sole discretion.
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    The following items are not allowed for sale:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    1.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Alcohol, drugs&nbsp;&amp; tobacco
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    2.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Adult &amp; mature content
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    3.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Animals
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    4.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Wildlife products
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    5.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Counterfeit and replica items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    6.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Dangerous items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    7.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Recalled items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    8.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Food items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    9.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Gift cards
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    10.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Illegal items or encouraging illegal activity
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    11.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Intangible items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    12.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Medical &amp; healthcare items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    13.
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Offensive materials
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Alcohol, drugs&nbsp;&amp; tobacco
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Alcohol, drugs, tobacco, and related products are often
                    subject to regulatory restrictions and are illegal for sale
                    in some cases. Because we can't enforce these laws and
                    regulations, we don't allow these items for sale, including:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Alcohol
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Prescription drugs
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Drugs, narcotics, or controlled substances (even those that
                    are legal in some states)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items marketed for the purpose of intoxication
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Drug paraphernalia such as bongs, vaporizers, and pipes
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Tobacco and related products, including e-cigarettes,
                    e-juice, vaporizers, mods, hookahs, and hookah accessories
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Cannabis and cannabis-derived products, such as CBD
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Growing or cultivation equipment for&nbsp;cannabis and
                    cannabis-derived products
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Adult &amp; mature content
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Boatzon strives to be an open marketplace but understands
                    that not all people want to see explicit sexual content.
                    Items intended for adult use or that contain explicit sexual
                    content are not allowed on Boatzon, such as:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Explicit sexual content or nudity
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Pornography
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Adult toys
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Animals
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Our policy around animals and wildlife products represents
                    Boatzon’s commitment to improving animal welfare in our own
                    communities and in helping to protect threatened and
                    endangered animals around the world. &nbsp;&nbsp;
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    You can certainly post many types of animal products.
                    However, live animals are never allowed, including
                  </span>
                  <strong>
                    <span
                      style={{
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      :
                    </span>
                  </strong>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Pets
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Farm animals and livestock
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Insects
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Marine life
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Wildlife &amp; wildlife products
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    You can certainly post many types of animal products.
                    However, wildlife products that were sourced illegally or
                    are being traded in contravention of any law are prohibited.
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <span
                    style={{
                      fontSize: "12.0pt",
                      lineHeight: "107%",
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Counterfeit or replica items
                  </span>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    The sale of bootlegs, counterfeits, fakes, and unauthorized
                    or pirated copies of items is illegal and not allowed on
                    Boatzon, including:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Counterfeits, fakes, and replicas of brand-name items,
                    including items “inspired” by a brand without permission of
                    the owner
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Bootlegged or unauthorized recordings
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Pirated copies of any copyrighted materials
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Use of a trademark or other implied affiliation with a brand
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Dangerous items
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Items that pose health and safety concerns or that are
                    regulated or illegal are not allowed on Boatzon. These
                    include but are not limited to:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Firearms, no exceptions
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Knives primarily used as weapons or for
                    self-defense.&nbsp;Note: Cutlery, kitchen, and utility
                    knives are generally permitted
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Mace, pepper spray, bear spray, tasers, and other weapons
                    used to subdue humans or animals
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Batons, billyclubs, or similar weapons used for striking a
                    person or animal
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Paintball guns, BB guns, guns that fire blanks, Airsoft
                    Guns, and all related ammunition and accessories
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Sporting weapons such as bows and arrows, crossbows, and
                    archery items
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Swords, if sharpened or functional.&nbsp;Note: Decorative
                    blunt swords are generally permitted
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Weapon parts and accessories, including&nbsp;any item that
                    attaches to a firearm,&nbsp;ammunition, bullets, clips,
                    arrows, and bolts
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Body armor, including Kevlar and other tactical and
                    protective vests, except protective clothing for sporting
                    activities
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Regulated chemicals, poisons, or substances
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    New or used airbags&nbsp;sold separately from a vehicle
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Hazardous materials such as:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Fireworks
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Explosives
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items deemed to pose significant health or safety risks to
                    others
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items used to incite violence
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Recalled items (see below)
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Recalled items
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Items that are subject to a recall, typically because they
                    are deemed unsafe or defective by federal regulators, are
                    not allowed on Boatzon. These items present a unique risk
                    because the harm they present is often not visibly
                    apparent.&nbsp; Items recalled in the past include:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Samsung Galaxy Note 7 cell phones
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Fisher-Price Rock N’ Play infant sleepers
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Drop-side cribs
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Other items listed&nbsp;
                  </span>
                  <a href="https://www.recalls.gov/cpsc.html">
                    <span style={{ fontSize: "12.0pt", color: "black" }}>
                      on the U.S. CPSC list of recalls
                    </span>
                  </a>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    You should avoid selling or buying any recalled item or
                    product. If you end up selling or buying a recalled item,
                    you should contact the manufacturer as they may offer a
                    refund, replacement, or voucher in exchange for surrendering
                    the recalled item. For more information on recalled consumer
                    products generally, visit the&nbsp;
                  </span>
                  <a href="https://www.cpsc.gov/">
                    <span
                      style={{
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      U.S. Consumer Product Safety Commission’s website
                    </span>
                  </a>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    .
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Food items
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    The sale of homemade prepared food is subject to regulatory
                    guidelines and often requires a permit. Because we can't
                    enforce these restrictions, prepared food is not allowed.
                    Examples:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Most perishable goods, including meat, seafood, dairy, and
                    cut fruits or vegetables
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Homemade shelf products, such as jams, jellies, and pickles
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Homemade meals or baked goods
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Eggs sold outside of the seller's farm
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Exceptions to this policy include:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Whole, uncut fruits and vegetables
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Eggs sold directly on the seller's farm
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Unopened and packaged commercially sold food goods (canned
                    soup, packages of cookies, etc.) as long as the packaging
                    has not been tampered with and the items are sold before
                    their expiration date
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Gift cards
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Gift cards and reloadable cards are not allowed due to the
                    risk for buyers. The balance can't always be verified, and
                    sometimes the card balance can be used or cancelled by the
                    original purchaser.
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Illegal items or encouraging illegal activity
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Any item that is illegal or that encourages illegal activity
                    is not allowed, including but not limited to:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items with sensitive information such as IDs, passports, and
                    social security cards
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Fake or forged documents (such as licenses, IDs, diplomas,
                    and government-issued documents)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items received through Government Assistance
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Offers to trade for illegal goods
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Police badges or security uniforms, as these can be used to
                    impersonate an officer or gain access to secure areas
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Locked phones or devices, for example blacklisted,
                    activation, iCloud or similar locks
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Current vehicle license plates or plates that were valid in
                    the last 5 years
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Licence plate tags
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    TV streaming devices
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Cars without titles
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Canisters or cylinders of pressurized gas (such as CO2 or
                    nitrous oxide)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Stolen or found goods
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Rideshare signs
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Intangible&nbsp;Items
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Boatzon is intended for the sale of individual, tangible
                    items. Items must exist and be available for immediate sale.
                    Examples of things that are not allowed include:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Services&nbsp;for intangible items such as dog walking,
                    babysitting, and IT support
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Advertisements
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Links to or information about flea markets, yard sales, and
                    garage sales
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    URLs, links, or redirecting to other websites
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Rentals and timeshares
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Multi-level marketing schemes and related promotional
                    materials
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Job postings
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "87.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Lost pet ads
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Wanted ads
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Solicitations
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Digital account transfers
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Invites, referral codes, or discount codes for apps or
                    websites
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items planned for future purchase from another retailer,
                    wholesaler, distributor, or manufacturer
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items of unknown contents such as “mystery boxes” or “grab
                    bags”
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items where the photo does not match the title or
                    description
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Medical &amp; healthcare items
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Due to health and safety, privacy, and regulatory concerns,
                    certain medical and healthcare items are not allowed for
                    sale on Boatzon, including but not limited to:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Devices, drugs, and medications that require a prescription
                    from a licensed medical practitioner (such as a dentist,
                    doctor, optometrist, or veterinarian)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Listings that claim the item can diagnose, cure, mitigate,
                    treat, or prevent a disease or viruses, including COVID-19
                    (the novel coronavirus)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Ventilators and other respiratory equipment
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Cannabis and cannabis-derived products, such as CBD
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Pills, vitamins, and supplements
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Contact lenses
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Used cosmetics
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Used makeup sponges and applicators
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Needles and items containing needles (such as syringes or
                    tattoo equipment)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Any post related to administering vaccinations, including
                    access to, proof of, or a physical item
                  </span>
                </p>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Offensive materials
                    </span>
                  </b>
                </h2>
                <p>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Boatzon is an inclusive community that welcomes users from
                    all backgrounds. To build a respectful mobile marketplace,
                    posts that contain foul language or that promote hatred,
                    violence, or discrimination are not allowed. This includes
                    posts that support hate or discrimination toward others
                    based on age, race, ethnicity, national origin, religion,
                    gender, gender identity, disability, or sexual orientation,
                    or items that promote or glorify organizations with such
                    views. Examples:
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Posts containing racial slurs
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Posts promoting intolerance or discrimination
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items that promote, support, or commemorate groups with
                    views of hatred or intolerance (for example Nazi, Neo-Nazi,
                    KKK groups, Proud Boys, QAnon, Antifa, or the Boogaloo
                    movement)
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items that depict or glorify violence against people or
                    animals
                  </span>
                </p>
                <p
                  className="MsoNormal"
                  style={{
                    marginLeft: "51.0pt",
                    textIndent: "-.25in",
                    lineHeight: "normal",
                  }}
                >
                  <span
                    style={{
                      fontSize: "10.0pt",
                      fontFamily: "Symbol",
                      color: "black",
                    }}
                  >
                    ·
                    <span style={{ font: '7.0pt "Times New Roman"' }}>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                  </span>
                  <span style={{ fontSize: "12.0pt", color: "black" }}>
                    Items that reflect people of color through offensive or
                    dehumanizing caricatures, including historic, antique, or
                    memorabilia items
                  </span>
                </p>
                <h2 style={{ marginTop: "0in" }}>
                  <span
                    style={{
                      fontSize: "12.0pt",
                      lineHeight: "107%",
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                  <span
                    style={{
                      fontSize: "12.0pt",
                      lineHeight: "107%",
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    .
                  </span>
                </h2>
                <h2 style={{ marginTop: "0in" }}>
                  <b>
                    <span
                      style={{
                        fontSize: "12.0pt",
                        lineHeight: "107%",
                        fontFamily: '"Calibri",sans-serif',
                        color: "black",
                      }}
                    >
                      Non-compliance
                    </span>
                  </b>
                </h2>
                <p style={{ marginBottom: "0in" }}>
                  <span
                    style={{
                      fontFamily: '"Calibri",sans-serif',
                      color: "black",
                    }}
                  >
                    Posts that violate any of our guidelines, policies, or terms
                    may be removed at our discretion. Additional actions may be
                    taken for severe or repeat offenses.
                  </span>
                </p>
                <p className="MsoNormal">
                  <span
                    style={{
                      fontSize: "12.0pt",
                      lineHeight: "107%",
                      color: "black",
                    }}
                  >
                    &nbsp;
                  </span>
                </p>
              </div>
              <style jsx>
                {`
                  @font-face {
                    font-family: Wingdings;
                    panose-1: 5 0 0 0 0 0 0 0 0 0;
                  }
                  @font-face {
                    font-family: "Cambria Math";
                    panose-1: 2 4 5 3 5 4 6 3 2 4;
                  }
                  @font-face {
                    font-family: Calibri;
                    panose-1: 2 15 5 2 2 2 4 3 2 4;
                  }
                  html {
                    overflow: auto;
                    scroll-behavior: smooth !important;
                    scrollbar-width: thin;
                    letter-spacing: 0.0366vw !important;
                  }
                  body {
                    margin: 0;
                    overflow: auto;
                    z-index: 0;
                    position: relative;
                    top: 0;
                    left: 0;
                    top: 0;
                    bottom: 0;
                  }
                  /* Style Definitions */
                  p.MsoNormal,
                  li.MsoNormal,
                  div.MsoNormal {
                    margin-top: 0in;
                    margin-right: 0in;
                    margin-bottom: 8pt;
                    margin-left: 0in;
                    line-height: 107%;
                    font-size: 11pt;
                    font-family: "Calibri", sans-serif;
                  }
                  h1 {
                    mso-style-link: "Heading 1 Char";
                    margin-right: 0in;
                    margin-left: 0in;
                    font-size: 24pt;
                    font-family: "Times New Roman", serif;
                    font-weight: bold;
                  }
                  h2 {
                    mso-style-link: "Heading 2 Char";
                    margin-top: 2pt;
                    margin-right: 0in;
                    margin-bottom: 0in;
                    margin-left: 0in;
                    line-height: 107%;
                    page-break-after: avoid;
                    font-size: 13pt;
                    font-family: "Calibri Light", sans-serif;
                    color: #2f5496;
                    font-weight: normal;
                  }
                  a:link,
                  span.MsoHyperlink {
                    color: blue;
                    text-decoration: underline;
                  }
                  p {
                    margin-right: 0in;
                    margin-left: 0in;
                    font-size: 12pt;
                    font-family: "Times New Roman", serif;
                  }
                  span.Heading1Char {
                    mso-style-name: "Heading 1 Char";
                    mso-style-link: "Heading 1";
                    font-family: "Times New Roman", serif;
                    font-weight: bold;
                  }
                  span.Heading2Char {
                    mso-style-name: "Heading 2 Char";
                    mso-style-link: "Heading 2";
                    font-family: "Calibri Light", sans-serif;
                    color: #2f5496;
                  }
                  .MsoChpDefault {
                    font-family: "Calibri", sans-serif;
                  }
                  span {
                    font-family: "Calibri", sans-serif !important;
                  }
                  .MsoPapDefault {
                    margin-bottom: 8pt;
                    line-height: 107%;
                  }
                  @page WordSection1 {
                    size: 8.5in 11in;
                    margin: 1in 1in 1in 1in;
                  }
                  div.WordSection1 {
                    page: WordSection1;
                  }
                  /* List Definitions */
                  ol {
                    margin-bottom: 0in;
                  }
                  ul {
                    margin-bottom: 0in;
                  }
                `}
              </style>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ProhibitedItemsGuidelines;
