// Main React Components
import React, { Component } from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// reusable component
import MasonaryLayout from "../../components/masonry-layout/masonry-layout";

// imported components
import ProductCard from "../post-card/product-card";

// fixtures data
import SelectInput from "../../components/input-box/select-input";
import {
  GREY_VARIANT_2,
  Wishlist_Icon,
  Saved_Blue_Icon,
} from "../../lib/config";
import { followPost, unfollowPost } from "../../services/following";
import Model from "../../components/model/model";
import LoginPage from "../auth-modals/login/login-page";
import { getCookie } from "../../lib/session";
import PageClipLoader from "../../components/loader/clip-loader";
import Router from "next/router";

class ProductList extends Component {
  state = {
    loginPage: false,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      AuthPass,
    });
  }

  handleWishlistPost = (event, item) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.state.AuthPass) {
      item.isLiked
        ? this.handleUnfollowPost(item.postId)
        : this.handleFollowPost(item.postId);
    } else {
      this.handleLogin();
    }
  };

  handleUnfollowPost = (postId) => {
    this.setState(
      {
        clipLoader: true,
        [postId]: true,
      },
      () => {
        let payload = {
          postId: postId.toString(),
          label: "Photo",
        };
        unfollowPost(payload)
          .then((res) => {
            console.log("res", res);
            let response = res.data;
            if (response) {
              if (response.code == 200) {
                this.setState(
                  {
                    clipLoader: false,
                    [postId]: false,
                  },
                  () => {
                    document.getElementById(postId).src = `${Wishlist_Icon}`;
                  }
                );
              }
            }
          })
          .catch((err) => {
            console.log("err", err);
          });
      }
    );
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  handleFollowPost = (postId) => {
    this.setState(
      {
        clipLoader: true,
        [postId]: true,
      },
      () => {
        let payload = {
          postId: postId.toString(),
          label: "Photo",
        };
        followPost(payload)
          .then((res) => {
            console.log("res", res);
            let response = res.data;
            if (response) {
              if (response.code == 200) {
                this.setState(
                  {
                    clipLoader: false,
                    [postId]: false,
                  },
                  () => {
                    document.getElementById(postId).src = `${Saved_Blue_Icon}`;
                  }
                );
              }
            }
          })
          .catch((err) => {
            console.log("err", err);
          });
      }
    );
  };

  handleProductDetails = (item) => {
    Router.push(
      `/product-detail/${item.productName.replace(/ /g, "-")}?pid=${
        item.postId
      }`
    );
  };

  render() {
    const BoatsTypes = [
      { value: "Recommended", label: "Recommended" },
      { value: "Popular", label: "Popular" },
      { value: "Recent", label: "Recent" },
    ];

    const PostedTypes = [
      { value: "24hr", label: "last 24 hr" },
      { value: "15days", label: "last 15 days" },
      { value: "30days", label: "last 30 days" },
    ];

    const { ProductsList, landingPage, myboats } = this.props;
    console.log("ProductsList", ProductsList);
    return (
      <Wrapper>
        <div className="container p-md-0">
          <div
            className={
              landingPage || myboats
                ? "screenWidth mx-auto"
                : "products-screenWidth mx-auto"
            }
          >
            {this.props.landingPage ? (
              ""
            ) : (
              <div className="d-flex justify-content-end align-items-center sortFilter_Sec mb-1">
                <div className="d-flex align-items-center">
                  <p className="postedLabel">Posted within</p>
                  <div className="SelectInput">
                    <SelectInput
                      noboxstyle={true}
                      placeholder="select"
                      value={this.props.postedWithin}
                      options={PostedTypes}
                      onChange={this.props.handleOnSelectInput(`postedWithin`)}
                    />
                  </div>
                </div>
              </div>
            )}

            <MasonaryLayout breakpointCols={this.props.breakpointCols}>
              {ProductsList &&
                ProductsList.map((item, index) => (
                  <div key={index} className="boatCard">
                    <div onClick={this.handleProductDetails.bind(this, item)}>
                      <ProductCard
                        Productdetails={item}
                        wishlist={this.props.productpage ? true : false}
                        shipping={this.props.myboats ? true : false}
                      />
                    </div>
                    <div>
                      {this.props.productpage ? (
                        this.state[`${item.postId}`] ? (
                          <div className="clipLoader">
                            <PageClipLoader
                              size={10}
                              loading={this.state.clipLoader}
                            />
                          </div>
                        ) : window.location.pathname === "/" ? (
                          ""
                        ) : (
                          <img
                            src={item.isLiked ? Saved_Blue_Icon : Wishlist_Icon}
                            id={item.postId}
                            className="wishlistIcon"
                            onClick={(e) => this.handleWishlistPost(e, item)}
                          ></img>
                        )
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                ))}
            </MasonaryLayout>
          </div>
        </div>
        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>
        <style jsx>
          {`
            .boatCard {
              cursor: pointer;
              box-shadow: 0 2px 4px 0px rgba(48, 56, 97, 0.1);
              // border-bottom-left-radius: 4px;
              // border-bottom-right-radius: 4px;
              border-radius: 3px;
              position: relative;
              height: ${this.props.myboats ? "auto" : "23.3vw"};
              max-height: ${this.props.myboats ? "auto" : "23.3vw"};
            }
            .wishlistIcon {
              position: absolute;
              // bottom: 0.732vw;
              // bottom: 20px;
              bottom: 0.938vw;
              right: 0.732vw;
              width: 0.951vw;
            }
            .clipLoader {
              position: absolute;
              bottom: 0.732vw;
              right: 0.732vw;
            }
            .SelectInput {
              margin: 0 1.464vw 0 0.366vw;
              height: auto;
            }
            .products-screenWidth {
              width: 98%;
            }
            .sortLabel,
            .postedLabel {
              font-size: 0.768vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0;
              letter-spacing: 0.0219vw !important;
            }
            :global(.sortFilter_Sec .SelectInput .blueChevronIcon) {
              width: 0.586vw;
              margin-top: 0.207vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(ProductList);
