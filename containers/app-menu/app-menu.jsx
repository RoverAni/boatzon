// Main React Components
import React, { Component } from "react";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// reusable component
import NormalTabs from "../../components/tabs/normal-tabs";

// imported components
const MyBoatsPage = dynamic(() => import("../my-boats-page/my-boats-page"), {ssr: false,});
const BoatsNew = dynamic(() => import("../boats-page/boats-new"), {ssr: false,});
const LoginPage = dynamic(() => import("../auth-modals/login/login-page"), {ssr: false,});
const EnginesPage = dynamic(() => import("../engines-page/engines-page"), {ssr: false,});
const TrailersPage = dynamic(() => import("../trailers-page/trailers-page"), {ssr: false,});
const ProductsPage = dynamic(() => import("../product-page/product-page"), {ssr: false,});
const ProfessionalPage = dynamic(() => import("../professional-page/professional-page"), {ssr: false,});
const DiscussionsPage = dynamic(() => import("../discussions-page/discussions-page"), {ssr: false,});
const ServicesPage = dynamic(() => import("../services-page/services-page"), {ssr: false,});
import Router from "next/router";

// Reactstrap Components
import { getCookie } from "../../lib/session";
import Model from "../../components/model/model";

class AppMenu extends Component {
  state = {
    materialUI: false,
    value: "",
    loginPage: false,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      materialUI: true,
      AuthPass,
    });
    try {
      let baseurl = window.location.href;
      if (baseurl.includes("?uname")) {
        const urlParams = new URLSearchParams(window.location.search);
        const myParam = urlParams.get("uname");
        this.setState({ uname: myParam });
        if (getCookie("token") == null && myParam.length > 0 && !this.state.loginPage) {
          this.handleLogin();
        }
      }
    } catch (e) {
      console.log("baseurl fail err", err);
    }
  }

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  onChangeTab = (event, value) => {
    switch (value) {
      case 0:
        this.state.AuthPass ? Router.push("/my-boats") : this.handleLogin();
        break;
      case 1:
        Router.push("/boats");
        break;
      case 2:
        Router.push("/engines");
        break;
      case 3:
        Router.push("/trailers");
        break;
      case 4:
        Router.push("/products");
        break;
      case 5:
        Router.push("/professionals");
        break;
      case 6:
        Router.push("/discussions");
        break;
      case 7:
        Router.push("/services");
        break;
    }
    this.setState({
      value,
    });
  };

  render() {
    const My_Boat = (
      <MyBoatsPage
        getManufacturerData={this.props.getManufacturerData}
        currentLocation={this.props.currentLocation}
        allManufacturersList={this.props.allManufacturersList}
        total_count={this.props.total_count}
        handleAllGetManufacturerList={this.props.handleAllGetManufacturerList}
      />
    );
    const Boats = (
      <BoatsNew
        handlePostProductsAPI={this.props.handlePostProductsAPI}
        handleUrlParams={this.props.handleUrlParams}
        urlString={this.props.urlString}
        preventAPICallOnFilterFunction={this.props.preventAPICallOnFilterFunction}
        filterPostData={this.props.filterPostData}
        paginationLoader={this.props.paginationLoader}
      />
    );
    const Engines = (
      <EnginesPage
        handlePostProductsAPI={this.props.handlePostProductsAPI}
        handleUrlParams={this.props.handleUrlParams}
        urlString={this.props.urlString}
        preventAPICallOnFilterFunction={this.props.preventAPICallOnFilterFunction}
        engineData={this.props.engineData}
        filterPostData={this.props.filterPostData}
        paginationLoader={this.props.paginationLoader}
      />
    );
    const Trailers = (
      <TrailersPage
        handlePostProductsAPI={this.props.handlePostProductsAPI}
        handleUrlParams={this.props.handleUrlParams}
        urlString={this.props.urlString}
        preventAPICallOnFilterFunction={this.props.preventAPICallOnFilterFunction}
        filterPostData={this.props.filterPostData}
        TrailerData={this.props.TrailerData}
      />
    );

    const Products = (
      <ProductsPage
      handlePostProductsAPI={this.props.handlePostProductsAPI}
      handleUrlParams={this.props.handleUrlParams}
      isRealProductsPage={this.props.isRealProductsPage}
      urlString={this.props.urlString}
      ProfessionalsList={this.props.ProfessionalsList} /** temporary data */
      preventAPICallOnFilterFunction={this.props.preventAPICallOnFilterFunction}
      /** updated */
      _productCategories={this.props._productCategories}
      _getProductSubSubCategoriesFunc={this.props._getProductSubSubCategoriesFunc}
      _categoryArray={this.props._categoryArray}
      _appendToCategory={this.props._appendToCategory}
    />
    );
    const Professionals = (
      <ProfessionalPage
        ProfessionalsList={this.props.ProfessionalsList}
        ProfessionalsData={this.props.ProfessionalsData}
        ProfessionalsDataAPI={this.props.ProfessionalsDataAPI}
        resetData={this.props.resetData}
        professionalSearchByInputText={this.props.professionalSearchByInputText}
        setUpdatedProfessionalData={this.props.setUpdatedProfessionalData}
        handler={this.props.handler}
        obj={this.props.obj}
      />
    );
    const Discussions = <DiscussionsPage />;
    const Services = <ServicesPage />;
    return (
      <Wrapper>
        {this.state.materialUI ? (
          <NormalTabs
            value={this.props.value}
            onChangeTab={this.onChangeTab}
            ladingpagetabs={true}
            tabs={[
              { label: `My Feed` },
              { label: `Boats` },
              { label: `Engines` },
              { label: `Trailers` },
              { label: `Products` },
              { label: `Professionals` },
              { label: `Discussions` },
              { label: `Services` },
            ]}
            tabcontent={[
              { content: My_Boat },
              { content: Boats },
              { content: Engines },
              { content: Trailers },
              { content: Products },
              { content: Professionals },
              { content: Discussions },
              { content: Services },
            ]}
          />
        ) : (
          ""
        )}

        {/* Login Model */}
        <Model open={this.state.loginPage} onClose={this.handleLogin} authmodals={true}>
          <LoginPage uname={this.state.uname} onClose={this.handleLogin} />
        </Model>
      </Wrapper>
    );
  }
}

export default AppMenu;

// import React, { Component } from "react";

// class AappMenu extends Component {
//   render() {
//     return <div>demo</div>;
//   }
// }
// export default AappMenu;
