import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  ITEM_6_IMG,
  ITEM_7_IMG,
  ITEM_8_IMG,
  Img_Placeholder,
  Chevron_Right_White,
  Chevron_Left_White,
} from "../../lib/config";
// Horizantal Scroll component
import Swiper from "swiper";
import ReviewYourOffer from "../product-details/review-your-offer";
import TrailerContent from "./trailer-content";

class TrailerDetails extends Component {
  state = {
    openpage: false,
  };

  componentDidMount() {
    const { trailerDetail } = this.props;
    let otherImageLinks = [];
    if (trailerDetail && trailerDetail.imageCount > 1) {
      for (let i = 1; i < trailerDetail.imageCount; i++) {
        let imageUrl = this.props.trailerDetail[`imageUrl${i}`];
        otherImageLinks.push(imageUrl);
      }
    }
    this.setState(
      {
        openpage: true,
        otherImageLinks,
      },
      () => {
        this.sliderTranslate();
      }
    );
  }

  sliderTranslate = () => {
    const slider = document.querySelector(".swiper1");
    var swiper1 = new Swiper(slider, {
      slidesPerView: "auto",
      spaceBetween: 0,
      loop: true,
      freeMode: true,
      autoplay: 1,
      mousewheel: {
        releaseOnEdges: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
    swiper1.init();
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleReviewOfferPage = () => {
    this.updateScreen(<ReviewYourOffer />);
  };

  render() {
    const { openpage, currentScreen, otherImageLinks } = this.state;
    const { trailerDetail } = this.props;

    return (
      <Wrapper>
        {!currentScreen ? (
          <Wrapper>
            <div className="col-12 p-0 position-relative">
              <div className="swiper-container swiper1">
                <div className="swiper-wrapper">
                  <div
                    className="swiper-slide"
                    style={{
                      backgroundImage: `url(${
                        trailerDetail && trailerDetail.mainUrl
                      })`,
                    }}
                  ></div>

                  {otherImageLinks &&
                    otherImageLinks.length > 0 &&
                    otherImageLinks.map((data) => (
                      <div
                        className="swiper-slide"
                        style={{
                          backgroundImage: `url(${data})`,
                        }}
                      ></div>
                    ))}

                  {trailerDetail &&
                    trailerDetail.imageCount < 3 &&
                    trailerDetail.imageCount == 2 && (
                      <div
                        className="swiper-slide"
                        style={{
                          backgroundImage: `url(${Img_Placeholder})`,
                        }}
                      ></div>
                    )}

                  {trailerDetail &&
                    trailerDetail.imageCount < 3 &&
                    trailerDetail.imageCount == 1 && (
                      <>
                        <div
                          className="swiper-slide"
                          style={{
                            backgroundImage: `url(${Img_Placeholder})`,
                          }}
                        ></div>
                        <div
                          className="swiper-slide"
                          style={{
                            backgroundImage: `url(${Img_Placeholder})`,
                          }}
                        ></div>
                      </>
                    )}
                </div>
                <img
                  className="swiper-button-next"
                  src={Chevron_Right_White}
                ></img>
                <img
                  className="swiper-button-prev"
                  src={Chevron_Left_White}
                ></img>
              </div>
            </div>
            <TrailerContent
              trailerDetail={trailerDetail}
              handleReviewOfferPage={this.handleReviewOfferPage}
            />
          </Wrapper>
        ) : (
          currentScreen
        )}

        <style jsx>
          {`
            :global(.swiper-container),
            :global(.swiper-wrapper) {
              z-index: 0 !important;
            }
            :global(.swiper-slide) {
              height: 20vw;
              width: 33vw;
              background-repeat: no-repeat;
              background-size: cover;
              background-position: top center;
            }
            :global(.swiper-button-next),
            :global(.swiper-button-prev) {
              width: 0.951vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default TrailerDetails;
