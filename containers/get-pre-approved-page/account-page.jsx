import React, { Component } from "react";
import InputBox from "../../components/input-box/input-box";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR_2,
  FONTGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_2,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  WHITE_COLOR,
} from "../../lib/config";
import IntlTelInput from "react-intl-tel-input";
import CheckBox from "../../components/input-box/check-box";
import SsnModel from "../buy-with-boatzon-page/ssn-model";
import PrivacyPolicyTerms from "../buy-with-boatzon-page/privacy-policy-terms";
import EleSignTerms from "../buy-with-boatzon-page/ele-sign-terms";
import Model from "../../components/model/model";
import CircularProgressButton from "../../components/button-loader/button-loader";
import VerifyOtp from "../my-account-page/verify-otp";

class AccountPage extends Component {
  state = {
    eleSignModel: false,
    privacyModel: false,
  };

  handleEleSignModel = () => {
    this.setState({
      eleSignModel: !this.state.eleSignModel,
    });
  };

  handlePrivacyModel = () => {
    this.setState({
      privacyModel: !this.state.privacyModel,
    });
  };

  render() {
    const { Email, Password, mobile, loanApproved } = {
      ...this.props.inputpayload,
    };
    const {
      AuthPass,
      handleOnchangeCheckbox,
      handleOnchangeInput,
      handleOnchangePhone,
      handleOnchangeflag,
      preferredCountries,
      accFormValid,
      handleApprovedPurchasePage,
      apiloading,
      ssnModel,
      handleSSNModel,
      inputpayload,
      ssnapiloading,
      SSNFormValid,
      handleGetCreditWithSSN,
      handleNewEmailCheck,
      EmailVerified,
      EmailValid,
      otpModel,
      PhoneVerified,
      isPhoneValid,
      handlePhoneNumberVerify,
      handleVerifyOtp,
      handleCloseOtpModel,
    } = this.props;
    return (
      <Wrapper>
        <div className="py-4 text-center AccountPage">
          <h6 className="heading">
            {AuthPass ? "Your Account" : "Create an account"}
          </h6>
          <p className="caption">
            Your account will allow you to see your monthly payments on boats
            and products, view and post marine products for sale, talk to
            professionals, and explore all the features of Boatzon
          </p>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <div className="position-relative">
                  <InputBox
                    type="email"
                    className="inputBox form-control"
                    name="Email"
                    value={Email}
                    placeholder="Email"
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                  {/* {EmailVerified ? (
                    <img src={Verified_Icon} className="verifiedIcon"></img>
                  ) : (
                    <img
                      src={NotVerified_Icon}
                      className="notVerifiedIcon"
                    ></img>
                  )}
                  {EmailVerified ? (
                    " "
                  ) : (
                    <p className="verify_btn" onClick={handleNewEmailCheck}>
                      Verify
                    </p>
                  )} */}
                </div>
                {EmailValid == 0 || EmailValid == 2 ? (
                  <p className="errMessage">Enter Valid Email</p>
                ) : (
                  ""
                )}
                {/* <p className="emailChangeMsg">
                  Changing your email will require re-verefication.
                </p> */}
              </div>
            </div>
          </div>
          {AuthPass ? (
            ""
          ) : (
            <div className="row m-0 align-items-center">
              <div className="col-12 p-0">
                <div className="FormInput">
                  <InputBox
                    type="password"
                    className="inputBox form-control"
                    name="Password"
                    value={Password}
                    placeholder="Password"
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                </div>
              </div>
            </div>
          )}
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="PhoneInput">
                <div className="position-relative">
                  <IntlTelInput
                    key={preferredCountries}
                    preferredCountries={preferredCountries}
                    containerClassName="intl-tel-input"
                    value={mobile}
                    onSelectFlag={handleOnchangeflag}
                    onPhoneNumberChange={handleOnchangePhone}
                    formatOnInit={false}
                    separateDialCode={true}
                    fieldId="phoneNum"
                    autoComplete="off"
                  />
                  {/* {isPhoneValid ? (
                    PhoneVerified ? (
                      <img src={Verified_Icon} className="verifiedIcon"></img>
                    ) : (
                      <img
                        src={NotVerified_Icon}
                        className="notVerifiedIcon"
                      ></img>
                    )
                  ) : (
                    ""
                  )}
                  {PhoneVerified ? (
                    " "
                  ) : (
                    <p className="verify_btn" onClick={handlePhoneNumberVerify}>
                      Verify
                    </p>
                  )} */}
                </div>
              </div>
            </div>
          </div>
          <div className="py-3 text-left">
            <CheckBox
              getLoanApproval={true}
              loanText={true}
              checked={loanApproved}
              plaincheckbox={true}
              onChange={handleOnchangeCheckbox("loanApproved")}
              value="loanApproved"
              label="Yes, I Agree"
            />
            <div className="row m-0 d-flex justify-content-start">
              <div className="col-1 p-0 point">
                <span className="bullet-point"></span>
              </div>
              <div className="col-11 p-0 ">
                <p className="terms">
                  I have read and agree to the{" "}
                  <span className="links" onClick={this.handleEleSignModel}>
                    E-SIGN Consent
                  </span>{" "}
                  that enables all transactions and disclosure delivery to occur
                  electronically.
                </p>
              </div>
            </div>
            <div className="row m-0 d-flex justify-content-start">
              <div className="col-1 p-0 point">
                <span className="bullet-point"></span>
              </div>
              <div className="col-11 p-0">
                <p className="terms">
                  I have received and read{" "}
                  <span className="links" onClick={this.handlePrivacyModel}>
                    Boatzon's Financial Privacy Policy.
                  </span>
                </p>
              </div>
            </div>
            <div className="row m-0 d-flex justify-content-start">
              <div className="col-1 p-0 point">
                <span className="bullet-point"></span>
              </div>
              <div className="col-11 p-0">
                <p className="terms">
                  By clicking on the <b>I Agree</b> checkbox and typing in your
                  name, you are confirming that you have read and understand the{" "}
                  <span className="links" onClick={this.handlePrivacyModel}>
                    Privacy Policy
                  </span>{" "}
                  and{" "}
                  <span className="links" onClick={this.handleEleSignModel}>
                    Electronic Signature Disclosure
                  </span>
                  , and you are authorizing <b>Boatzon</b> under all applicable
                  federal and state laws, including the Fair Credit Reporting
                  Act, to obtain information from your personal credit profile.
                  Also, you are confirming you understand that any pre-approval
                  is subject to review and acceptance of credit information.
                </p>
              </div>
            </div>
            <div className="col-12 p-0 pt-2">
              <div className="row m-0 d-flex align-items-center justify-content-between">
                <div className="col-4 p-0 pr-2">
                  <div className="back_btn">
                    <CircularProgressButton
                      buttonText={<span>Back</span>}
                      onClick={this.props.handleBack}
                    />
                  </div>
                </div>
                <div className="col-8 p-0 pl-1">
                  {/* <ButtonComp onClick={this.props.handleApprovedPurchasePage}>
                    Get Pre-Qualified
                  </ButtonComp> */}
                  <div className={accFormValid ? "next_btn" : "inactive_btn"}>
                    <CircularProgressButton
                      buttonText={<span>Get Pre-Qualified</span>}
                      disabled={!accFormValid}
                      onClick={handleApprovedPurchasePage}
                      loading={apiloading}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row m-0 d-flex justify-content-start reportSec">
              <div className="col-12 p-0">
                <p className="reportMsg">
                  Consumer Report: By clicking "GET MY TERMS", I give Boatzon
                  written consent to obtain consumer reports from one or more
                  consumer reporting agencies to show me credit options I
                  prequalify for when financing with Boatzon. Retrieving my
                  pre-qualification credit terms generates a soft credit
                  inquiry, which is visible only to me and does not affect my
                  credit score. This prequalification is a soft pull of your
                  credit, and does not impact your credit score.
                </p>
              </div>
            </div>
          </div>
        </div>

        {/* EleSignTerms Model */}
        <Model open={this.state.eleSignModel} onClose={this.handleEleSignModel}>
          <EleSignTerms onClose={this.handleEleSignModel} />
        </Model>

        {/* Privacy Policy Model */}
        <Model open={this.state.privacyModel} onClose={this.handlePrivacyModel}>
          <PrivacyPolicyTerms onClose={this.handlePrivacyModel} />
        </Model>

        {/* SSN Model */}
        <Model open={ssnModel} onClose={handleSSNModel}>
          <SsnModel
            handleOnchangeInput={handleOnchangeInput}
            onClose={handleSSNModel}
            inputpayload={inputpayload}
            ssnapiloading={ssnapiloading}
            SSNFormValid={SSNFormValid}
            handleGetCreditWithSSN={handleGetCreditWithSSN}
          />
        </Model>

        {/* Login Model */}
        <Model open={otpModel} onClose={handleCloseOtpModel} authmodals={true}>
          <VerifyOtp
            onClose={handleCloseOtpModel}
            handleVerifyOtp={handleVerifyOtp}
            inputpayload={inputpayload}
          />
        </Model>
        <style jsx>
          {`
            .heading {
              font-family: "Museo-Sans" !important;
              font-style: normal;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              font-size: 0.951vw;
              letter-spacing: 0.0292vw !important;
            }
            .caption {
              font-family: "Open Sans" !important;
              font-style: normal;
              color: ${FONTGREY_COLOR};
              font-size: 0.732vw;
              letter-spacing: 0.0292vw !important;
              line-height: 1.2;
            }
            .FormInput {
              margin: 0.292vw 0;
              position: relative;
            }
            :global(.AccountPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.585vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.AccountPage .FormInput .inputBox:focus),
            :global(.AccountPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.AccountPage .PhoneInput) {
              position: relative;
              margin: 0.292vw 0;
            }
            :global(.AccountPage .PhoneInput > div > div) {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
              border: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.342vw;
              padding: 0;
            }
            :global(.AccountPage .PhoneInput > div > div > input) {
              width: 100%;
              height: 100%;
              line-height: 1 !important;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR_2} !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              padding: 0 0.366vw !important;
              margin-left: 0.366vw !important;
            }
            :global(.AccountPage .PhoneInput > div > div div .country-list) {
              width: 14.641vw !important;
              margin: 0.219vw 0 0 0;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .country-list
                .country) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.AccountPage .PhoneInput > div > div > input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container) {
              height: 2.342vw;
              position: relative !important;
              width: fit-content !important;
              display: block ruby !important;
              padding: 0 !important;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.AccountPage .PhoneInput > div > div div .selected-flag) {
              width: 100% !important;
              background: ${WHITE_COLOR} !important;
              height: 100%;
              border: none;
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR_2} !important;
              border-radius: 0.146vw;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus),
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:active) {
              border: none;
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR_2} !important;
              border-radius: 0.146vw;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code) {
              font-size: 0.805vw;
              height: 2.122vw;
              align-items: center;
              display: flex;
              padding: 0 0 0 0.219vw;
              line-height: 1 !important;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code::after) {
              content: "${this.props.preferredCountries[0]} ";
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.219vw !important;
              position: relative;
              text-transform: uppercase;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus) {
              box-shadow: none !important;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.AccountPage .PhoneInput > div > div div .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-flag) {
              display: none;
            }
            :global(.AccountPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-arrow) {
              display: none;
            }
            :global(.AccountPage .PhoneInput > div > div div:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            .point {
              max-width: 1.464vw;
            }
            .bullet-point {
              display: block;
              margin: 0.366vw 0.732vw 0 0.366vw;
              width: 0.366vw;
              height: 0.366vw;
              border-radius: 50%;
              background: ${GREEN_COLOR};
            }
            .terms {
              font-size: 0.658vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0;
              line-height: 1.2;
            }
            .terms span {
              color: ${GREEN_COLOR};
              font-family: "Open Sans-SemiBold" !important;
            }
            .terms .links {
              cursor: pointer;
            }
            .reportSec {
              margin-top: 1.024vw !important;
            }
            .reportMsg {
              font-size: 0.658vw;
              font-style: italic;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0.219vw;
              line-height: 1.4;
            }
            :global(.back_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              background: ${WHITE_COLOR};
              margin: 0 !important;
              text-transform: capitalize;
              border: 0.0732vw solid ${GREY_VARIANT_2};
              border-radius: 0.146vw;
              position: relative;
              height: 2.342vw;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            :global(.inactive_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              margin: 0;
              text-transform: capitalize;
              border-radius: 0.146vw;
              position: relative;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              height: 2.489vw;
            }
            :global(.inactive_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              background: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
              height: 2.489vw;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${BG_LightGREY_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .verify_btn {
              position: absolute;
              right: -2.928vw;
              top: 50%;
              transform: translate(0, -50%);
              background: transparent;
              width: fit-content;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              cursor: pointer;
              margin: 0;
            }
            .errMessage {
              font-size: 0.732vw;
              color: red;
              padding: 0.585vw 0 0 0;
              margin: 0;
            }
            .notVerifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .emailChangeMsg {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              padding: 0.366vw 0 0 0;
              opacity: 0.8;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              text-align: left;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default AccountPage;
