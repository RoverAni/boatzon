import React, { Component } from "react";
import Tabs from "../../components/tabs/tabs";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  Score_Meter_Icon,
  Border_LightGREY_COLOR,
  Get_Pre_Approved_Bg,
  WHITE_COLOR,
} from "../../lib/config";
import {
  handleFirstName,
  handleLastName,
  handleMiddleName,
} from "../../lib/name-handlers/handlers";
import AccountPage from "./account-page";
import InformationPage from "./information-page";
import SuccessfullLoanApprovalPage from "./successfull-loan-approval-page";
import moment from "moment";
import { getCookie } from "../../lib/session";
import { handlePreferredCountries } from "../../lib/phone-num/phone-num";
import {
  COUNTRY_CODE,
  MOBILE,
} from "../../lib/input-control-data/input-definitions";
import {
  getLocationfromLatLng,
  getLocationfromStateZipcode,
} from "../../lib/location/geo-location";
import { postCreditScore } from "../../services/buy-with-boatzon";
import { requiredValidator } from "../../lib/validation/validation";
import Snackbar from "../../components/snackbar";
import {
  newEmailCheck,
  newPhoneNumber,
  newPhoneNumberVerifyOtp,
  saveEditProfileData,
} from "../../services/userProfile";
import {
  formatDate,
  timestampTomomentizedDate,
} from "../../lib/date-operation/date-operation";

class GetPreApprovedPage extends Component {
  state = {
    value: 0,
    inputpayload: {},
    InfoFormValid: false,
    accFormValid: false,
    preferredCountries: ["us"],
    loanApproved: false,
    apiloading: false,
    ssnModel: false,
    ssnapiloading: false,
    SSNFormValid: false,
  };

  componentDidMount = () => {
    this.handleOnLoad();
  };

  handleOnLoad = async () => {
    let AuthPass = getCookie("authPass");
    this.setState({
      AuthPass,
    });
    const {
      username,
      fullName,
      location,
      longitude,
      latitude,
      email,
      countryCode,
      phoneNumber,
      annualIncome,
      dob,
      employeeStatus,
      emailVerified,
    } = this.props.userProfileData;
    const {
      locationUpdate,
      inputpayload,
      firstNameEdit,
      middleNameEdit,
      lastNameEdit,
      annualIncomeEdit,
      dobEdit,
      employmentStatusEdit,
    } = this.state;

    let fullNameArr = fullName.split(" ");
    let userLocation;
    try {
      if (latitude && longitude) {
        userLocation = await getLocationfromLatLng(latitude, longitude);
      }
      const PhoneNumber = phoneNumber.startsWith("+")
        ? phoneNumber.replace(countryCode, "")
        : phoneNumber;

      let tempPreferredCountries =
        countryCode != "undefined"
          ? handlePreferredCountries(countryCode, PhoneNumber)
          : this.state.preferredCountries;

      let tempPayload = { ...this.state.inputpayload };
      tempPayload.firstName = firstNameEdit
        ? inputpayload.firstName
        : fullName
        ? handleFirstName(fullNameArr)
        : "";
      tempPayload.middleName = middleNameEdit
        ? inputpayload.middleName
        : fullName
        ? handleMiddleName(fullNameArr)
        : "";
      tempPayload.lastName = lastNameEdit
        ? inputpayload.lastName
        : fullName
        ? handleLastName(fullNameArr)
        : "";
      tempPayload.username = username ? username : "";
      tempPayload.address_line = locationUpdate
        ? inputpayload.location
        : location
        ? location
        : "";
      tempPayload.longitude = locationUpdate
        ? inputpayload.longitude
        : longitude
        ? longitude
        : "";
      tempPayload.latitude = locationUpdate
        ? inputpayload.latitude
        : latitude
        ? latitude
        : "";
      tempPayload.city = locationUpdate
        ? inputpayload.city
        : userLocation
        ? userLocation.city
        : "";
      tempPayload.stateName = locationUpdate
        ? inputpayload.stateName
        : userLocation
        ? userLocation.stateName
        : "";
      tempPayload.country = locationUpdate
        ? inputpayload.country
        : userLocation
        ? userLocation.country
        : "";
      tempPayload.zipCode = locationUpdate
        ? inputpayload.zipCode
        : userLocation
        ? userLocation.zipCode
        : "";
      tempPayload.Email = email ? email : "";
      tempPayload.mobile = phoneNumber ? PhoneNumber : "";
      tempPayload.countryCode = countryCode ? countryCode : "";
      tempPayload.annualIncome = annualIncomeEdit
        ? inputpayload.annualIncome
        : annualIncome
        ? annualIncome
        : "";
      tempPayload.dob = dobEdit
        ? timestampTomomentizedDate(inputpayload.dob)
        : dob
        ? timestampTomomentizedDate(dob)
        : "";
      tempPayload.employmentStatus = employmentStatusEdit
        ? inputpayload.employmentStatus
        : employeeStatus
        ? { value: employeeStatus, label: employeeStatus }
        : "";
      this.setState(
        {
          inputpayload: { ...tempPayload },
          preferredCountries: [...tempPreferredCountries],
          EmailValid: email ? 1 : 0,
          isPhoneValid: PhoneNumber && countryCode ? true : false,
          // EmailVerified: emailVerified ? emailVerified : false,
          // PhoneVerified: PhoneNumber && countryCode ? true : false,
          EmailVerified: true,
          PhoneVerified: true,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } catch (err) {
      console.log("err---->", err);
    }
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  // Function to Save Profile Details
  handleSaveChanges = () => {
    const { inputpayload } = this.state;
    const { userProfileData } = this.props;
    let payload = {
      fullName: `${inputpayload.firstName} ${inputpayload.middleName} ${inputpayload.lastName}`,
      profilePicUrl: userProfileData.profilePicUrl,
      thumbnailImageUrl: userProfileData.profilePicUrl,
      location: inputpayload.address_line,
      latitude: inputpayload.latitude.toString(),
      longitude: inputpayload.longitude.toString(),
      city: inputpayload.city,
      stateName: inputpayload.stateName,
      countryShortName: inputpayload.countryShortName,
      creditScore: inputpayload.CustomerCreditScore,
      employeeStatus:
        inputpayload.employmentStatus && inputpayload.employmentStatus.value,
      dob: parseInt(formatDate(inputpayload.dob, "X"), 10),
      annualIncome: inputpayload.annualIncome,
    };
    saveEditProfileData(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: this.handleResponseMsg(response),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            // window.location.reload();
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleResponseMsg = (response) => {
    switch (response.code) {
      case 200:
        return "Successfully Updated the Profile";
        break;
      default:
        response.message;
    }
  };

  // Function to handle snackbar variant color
  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 404:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  // Function to get creditScore
  handleApprovedPurchasePage = () => {
    const {
      productId,
      firstName,
      middleName,
      lastName,
      address_line,
      city,
      stateName,
      zipCode,
      mobile,
      countryCode,
    } = this.state.inputpayload;

    let Address = address_line.replaceAll(",", "");
    let payload = {
      userId: getCookie("uid"),
      // productId: productId,
      first_name: `${firstName} ${middleName}`,
      last_name: lastName,
      address: Address,
      city: city,
      stateName: stateName,
      zipCode: zipCode,
      mobile: countryCode + mobile,
    };
    console.log("payload", payload);
    this.setState({
      apiloading: true,
    });
    // checkCreditScore(this.state.inputpayload)
    postCreditScore(payload)
      .then((res) => {
        // var xml = convert.xml2json(res, { compact: true, spaces: 2 });
        // let response = JSON.parse(xml);
        // let creditScoreData = response && response.Results;
        if (res && res.data.code == 200) {
          let creditScoreData =
            res.data && res.data.message && res.data.message.Results;
          console.log("fbfbh", res, creditScoreData);
          this.setState(
            {
              apiloading: false,
              creditScoreData,
              usermessage: this.handleCreditScoreResponseMsg(creditScoreData),
              variant: this.handleCreditScoreResponseClr(creditScoreData),
              open: true,
              vertical: "bottom",
              horizontal: "left",
            },
            () => {
              if (
                this.handleCreditScoreResponseClr(creditScoreData) == "success"
              ) {
                let tempPayload = { ...this.state.inputpayload };
                tempPayload.CustomerCreditScore =
                  creditScoreData.XML_Report.Prescreen_Report.Score &&
                  creditScoreData.XML_Report.Prescreen_Report.Score._text
                    ? creditScoreData.XML_Report.Prescreen_Report.Score._text
                    : 0;
                tempPayload.CustomerTransid =
                  creditScoreData.XML_Report.Transid &&
                  creditScoreData.XML_Report.Transid._text
                    ? creditScoreData.XML_Report.Transid._text
                    : "";
                tempPayload.CustomerToken =
                  creditScoreData.XML_Report.Token &&
                  creditScoreData.XML_Report.Token._text
                    ? creditScoreData.XML_Report.Token._text
                    : "";
                this.setState(
                  {
                    inputpayload: { ...tempPayload },
                  },
                  () => {
                    this.updateScreen(<SuccessfullLoanApprovalPage />);
                    this.handleSaveChanges();
                  }
                );
              } else if (
                this.handleCreditScoreResponseClr(creditScoreData) == "warning"
              ) {
                setTimeout(() => {
                  this.handleSSNModel();
                }, 1200);
              }
            }
          );
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          apiloading: true,
        });
      });
  };

  // Function to handle snackbar msg
  handleCreditScoreResponseMsg = (data) => {
    if (data && data.Creditsystem_Error) {
      return data.Creditsystem_Error._attributes.message;
    } else if (data && data.XML_Report && data.XML_Report.Prescreen_Report) {
      return data.XML_Report.Prescreen_Report.ResultDescription._text;
    }
  };

  // Function to handle snackbar color
  handleCreditScoreResponseClr = (data) => {
    if (data && data.Creditsystem_Error) {
      return "error";
    } else if (data && data.XML_Report && data.XML_Report.Prescreen_Report) {
      switch (data.XML_Report.Prescreen_Report.ResultDescription._text) {
        case "Consumer Found and Score Returned":
          return "success";
          break;
        case "No Hit, Consumers file not found":
          return "warning";
          break;
      }
    }
  };

  // Function to Toggle SSN Model
  handleSSNModel = () => {
    this.setState({
      ssnModel: !this.state.ssnModel,
    });
  };

  // Function to get creditScore with SSN
  handleGetCreditWithSSN = () => {
    const {
      productId,
      firstName,
      middleName,
      lastName,
      address_line,
      city,
      stateName,
      zipCode,
      mobile,
      countryCode,
      SSN,
    } = this.state.inputpayload;
    let Address = address_line.replaceAll(",", "");
    let payload = {
      userId: getCookie("uid"),
      // productId: productId,
      first_name: `${firstName} ${middleName}`,
      last_name: lastName,
      address: Address,
      city: city,
      stateName: stateName,
      zipCode: zipCode,
      mobile: countryCode + mobile,
      ssn: SSN,
    };
    this.setState({
      ssnapiloading: true,
    });
    postCreditScore(payload)
      .then((res) => {
        // var xml = convert.xml2json(res, { compact: true, spaces: 2 });
        // let response = JSON.parse(xml);
        // let creditScoreData = response && response.Results;
        let creditScoreData =
          res.data && res.data.message && res.data.message.Results;
        this.setState(
          {
            ssnapiloading: false,
            creditScoreData,
            usermessage: this.handleCreditScoreResponseMsg(creditScoreData),
            variant: this.handleCreditScoreResponseClr(creditScoreData),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          },
          () => {
            if (
              this.handleCreditScoreResponseClr(creditScoreData) == "success"
            ) {
              let tempPayload = { ...this.state.inputpayload };
              tempPayload.CustomerCreditScore =
                creditScoreData.XML_Report.Prescreen_Report.Score &&
                creditScoreData.XML_Report.Prescreen_Report.Score._text
                  ? creditScoreData.XML_Report.Prescreen_Report.Score._text
                  : 0;
              tempPayload.CustomerTransid =
                creditScoreData.XML_Report.Transid &&
                creditScoreData.XML_Report.Transid._text
                  ? creditScoreData.XML_Report.Transid._text
                  : "";
              tempPayload.CustomerToken =
                creditScoreData.XML_Report.Token &&
                creditScoreData.XML_Report.Token._text
                  ? creditScoreData.XML_Report.Token._text
                  : "";
              this.setState(
                {
                  inputpayload: { ...tempPayload },
                  ssnModel: false,
                },
                () => {
                  this.updateScreen(<SuccessfullLoanApprovalPage />);
                  this.handleSaveChanges();
                }
              );
            }
          }
        );
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          ssnapiloading: true,
          ssnModel: false,
        });
      });
  };

  // Function to navigate to previous Tab
  handleBack = () => {
    this.setState({
      value: 0,
    });
  };

  // Function to navigate to next Tab
  handleNext = async () => {
    try {
      let lanLanUpdate = await this.handleGetLatLng();
      if (lanLanUpdate) {
        this.setState({
          value: 1,
        });
      }
    } catch (err) {
      console.log("err", err);
      this.setState({
        usermessage: err.message,
        variant: "error",
        open: true,
        vertical: "bottom",
        horizontal: "left",
      });
    }
  };

  // Fuction to Validate forms
  checkIfFormValid = () => {
    const {
      firstName,
      lastName,
      username,
      address_line,
      dob,
      annualIncome,
      employmentStatus,
      initialPlacement,
    } = this.state.inputpayload;
    const {
      isPhoneValid,
      EmailValid,
      SSNValid,
      PhoneVerified,
      loanApproved,
    } = this.state;

    let InfoFormValid = false;
    InfoFormValid =
      firstName &&
      lastName &&
      username &&
      address_line &&
      dob &&
      annualIncome &&
      employmentStatus
        ? true
        : false;

    let accFormValid = false;
    accFormValid =
      EmailValid && isPhoneValid && loanApproved
        ? true
        : false;

    let SSNFormValid = false;
    SSNFormValid = SSNValid == 1 ? true : false;

    this.setState({
      InfoFormValid,
      accFormValid,
      SSNFormValid,
    });
  };

  // Function to get Latitude and Longitude from stateName and zipCode
  handleGetLatLng = async () => {
    const {
      address_line,
      country,
      stateName,
      city,
      zipCode,
    } = this.state.inputpayload;
    let location;
    try {
      if (address_line && country && stateName && city && zipCode) {
        location = await getLocationfromStateZipcode(
          address_line,
          country,
          stateName,
          city,
          zipCode
        );
      }
      let tempPayload = { ...this.state.inputpayload };
      tempPayload["latitude"] = location.lat || location.latitude;
      tempPayload["longitude"] = location.lng || location.longitude;
      this.setState({
        inputpayload: { ...tempPayload },
        locationUpdate: true,
      });
    } catch (err) {
      console.log("err---->", err);
    }
    return this.state.locationUpdate;
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [`${inputControl.name}Valid`]: validate });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
        [`${inputControl.name}Edit`]: true,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = event;
      this.setState(
        {
          inputpayload: { ...tempPayload },
          [`${name}Edit`]: true,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  handleDOBChange = (date) => {
    if (date) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`dob`] = moment(date);
      this.setState(
        {
          inputpayload: { ...temppayload },
          dobEdit: true,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`dob`] = "";
      this.setState(
        {
          inputpayload: { ...temppayload },
          dobEdit: true,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  disabledDate = (current) => {
    // Can not select days after today and today
    return current && current >= moment().subtract(18, "year");
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    this.handleOnchangePhone(status, num, country);
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    const PhoneNumber = this.props.userProfileData.phoneNumber.startsWith("+")
      ? this.props.userProfileData.phoneNumber.replace(
          this.props.userProfileData.countryCode,
          ""
        )
      : this.props.userProfileData.phoneNumber;

    let tempPayload = { ...this.state.inputpayload };
    tempPayload[`${MOBILE}`] = valuenumber;
    tempPayload[`${COUNTRY_CODE}`] = "+" + dialInfo.dialCode;
    tempPayload[`preferredCountries`] = [`${dialInfo.iso2}`];
    this.setState(
      {
        inputpayload: { ...tempPayload },
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
        PhoneVerified: PhoneNumber != valuenumber && status ? false : true,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    console.log("ndisw", event.target.checked, name);
    let tempInputPayload = { ...this.state.inputpayload };
    tempInputPayload[name] = event.target.checked;
    this.setState(
      {
        inputpayload: { ...tempInputPayload },
        [name]: event.target.checked,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function to handle snackbar variant color
  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 404:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  // Function to send Email Varification link
  handleNewEmailCheck = () => {
    let payload = {
      email: this.state.inputpayload.Email,
    };
    newEmailCheck(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function to send PhoneNum Verification Otp
  handlePhoneNumberVerify = () => {
    let payload = {
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };
    newPhoneNumber(payload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message.toString(),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.setState({
              otpModel: true,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // Function to Toggle otp Model
  handleCloseOtpModel = () => {
    this.setState({
      otpModel: false,
    });
  };

  // Function to verify otp
  handleVerifyOtp = (otp) => {
    let payload = {
      otp: otp,
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
      countryCode: this.state.inputpayload.countryCode,
    };

    newPhoneNumberVerifyOtp(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response.code == 200) {
          this.setState(
            {
              PhoneVerified: true,
            },
            () => {
              this.handleCloseOtpModel();
            }
          );
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };
  render() {
    const {
      currentScreen,
      value,
      inputpayload,
      InfoFormValid,
      AuthPass,
      preferredCountries,
      accFormValid,
      apiloading,
      ssnModel,
      ssnapiloading,
      SSNFormValid,
      EmailVerified,
      EmailValid,
      otpModel,
      PhoneVerified,
      isPhoneValid,
    } = this.state;
    const {
      handleNext,
      handleBack,
      handleApprovedPurchasePage,
      handleOnchangeInput,
      handleOnSelectInput,
      handleDOBChange,
      disabledDate,
      handleOnchangePhone,
      handleOnchangeflag,
      handleOnchangeCheckbox,
      handleSSNModel,
      handleGetCreditWithSSN,
      handleNewEmailCheck,
      handleCloseOtpModel,
      handlePhoneNumberVerify,
      handleVerifyOtp,
    } = this;
    const { userProfileData } = this.props;

    const Information = (
      <InformationPage
        handleNext={handleNext}
        userProfileData={userProfileData}
        inputpayload={inputpayload}
        handleOnchangeInput={handleOnchangeInput}
        handleOnSelectInput={handleOnSelectInput}
        handleDOBChange={handleDOBChange}
        disabledDate={disabledDate}
        InfoFormValid={InfoFormValid}
      />
    );
    const Account = (
      <AccountPage
        handleApprovedPurchasePage={handleApprovedPurchasePage}
        handleBack={handleBack}
        AuthPass={AuthPass}
        inputpayload={inputpayload}
        handleOnchangePhone={handleOnchangePhone}
        handleOnchangeflag={handleOnchangeflag}
        handleOnchangeCheckbox={handleOnchangeCheckbox}
        handleOnchangeInput={handleOnchangeInput}
        preferredCountries={preferredCountries}
        accFormValid={accFormValid}
        apiloading={apiloading}
        ssnModel={ssnModel}
        handleSSNModel={handleSSNModel}
        ssnapiloading={ssnapiloading}
        SSNFormValid={SSNFormValid}
        handleGetCreditWithSSN={handleGetCreditWithSSN}
        handleNewEmailCheck={handleNewEmailCheck}
        EmailVerified={EmailVerified}
        EmailValid={EmailValid}
        otpModel={otpModel}
        handlePhoneNumberVerify={handlePhoneNumberVerify}
        handleCloseOtpModel={handleCloseOtpModel}
        PhoneVerified={PhoneVerified}
        isPhoneValid={isPhoneValid}
        handleVerifyOtp={handleVerifyOtp}
      />
    );

    return (
      <Wrapper>
        <div className="getPreApprovedPage_Heaing">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 py-4">
              <div className="col-9 mx-auto text-center">
                <h3 className="heading">
                  Get Pre-Qualified for any boat, engine, trailer, and products
                  in 2 minutes
                </h3>
                <p className="caption">
                  Pre-Qualified shoppers see real terms and actual monthly
                  payments on thousands of boats and products
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          className={
            currentScreen
              ? "successPage_Content PreApprovedPage_Content"
              : value == 1
              ? "accPage_Content PreApprovedPage_Content"
              : "infoPage_Content PreApprovedPage_Content"
          }
        >
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0">
              <div className="col-6 p-0 mx-auto ">
                <div className="getPreApprovedSteps_Sec">
                  <div className="ScoreInfoDiv">
                    <p className="Msg">
                      <img
                        src={Score_Meter_Icon}
                        className="scoreMeterIcon"
                      ></img>
                      <span>No Impact To Your Credit Score</span>
                    </p>
                  </div>
                  {!currentScreen ? (
                    <div className="StepsDiv">
                      <Tabs
                        preApprovedPage={true}
                        loanValue={value}
                        tabs={[
                          {
                            label: `1. Information`,
                          },
                          {
                            label: `2. Account`,
                          },
                        ]}
                        tabcontent={[
                          { content: Information },
                          { content: Account },
                        ]}
                      />
                    </div>
                  ) : (
                    <div className="StepsDiv p-0">{currentScreen}</div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        <style jsx>
          {`
            .getPreApprovedPage_Heaing {
              background: ${BG_LightGREY_COLOR};
              min-height: 14.641vw;
            }
            .heading {
              font-family: "Museo-Sans" !important;
              font-style: normal;
              font-weight: 600;
              font-size: 1.464vw;
              color: ${FONTGREY_COLOR};
              max-width: 80%;
              margin: 0 auto 0.366vw auto;
              letter-spacing: 0.0292vw !important;
            }
            .caption {
              font-family: "Open Sans" !important;
              font-style: normal;
              font-weight: normal;
              text-align: center;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0292vw !important;
            }
            .PreApprovedPage_Content {
              background: url(${Get_Pre_Approved_Bg});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              position: relative;
            }
            .successPage_Content {
              min-height: 25.988vw;
            }
            .accPage_Content {
              min-height: 52.342vw;
            }
            .infoPage_Content {
              min-height: 60.029vw;
            }
            .getPreApprovedSteps_Sec {
              position: absolute;
              width: 100%;
              top: -6.881vw;
            }
            .ScoreInfoDiv {
              background: ${Border_LightGREY_COLOR};
              padding: 0.292vw;
              text-align: center;
            }
            .ScoreInfoDiv .Msg {
              margin: 0;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .ScoreInfoDiv .Msg span {
              font-family: "Open Sans-SemiBold" !important;
              font-style: normal;
              font-weight: normal;
              text-align: center;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0292vw !important;
            }
            .scoreMeterIcon {
              width: 1.464vw;
              margin-right: 0.878vw;
            }
            .StepsDiv {
              background: ${WHITE_COLOR};
              margin: 1.098vw auto 1.83vw auto;
              padding: 1.098vw 0;
              border-radius: 0.219vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default GetPreApprovedPage;
