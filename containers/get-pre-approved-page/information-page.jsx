import React, { Component } from "react";
import InputBox from "../../components/input-box/input-box";
import SelectTopLabel from "../../components/input-box/select-top-label";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  Info_Icon_Light_Grey,
  Border_LightGREY_COLOR_2,
  GREY_VARIANT_2,
  THEME_COLOR,
  Question_Grey_Icon,
  BG_LightGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR_1,
  WHITE_COLOR,
} from "../../lib/config";
import { NumberValidator } from "../../lib/validation/validation";
import { DatePicker } from "antd";
import ButtonComp from "../../components/button/button";
import { getCookie } from "../../lib/session";

let AuthPass = getCookie("authPass");

class InformationPage extends Component {
  render() {
    const {
      firstName,
      middleName,
      lastName,
      username,
      addressLine2,
      dob,
      annualIncome,
      address_line,
      employmentStatus,
      initialPlacement,
      city,
      stateName,
      zipCode,
      country,
    } = {
      ...this.props.inputpayload,
    };
    const suffixType = [
      { value: "Suffix", label: "Suffix" },
      { value: "Preffix", label: "Preffix" },
    ];
    const employmentStatusType = [
      { value: "Employed", label: "Employed" },
      { value: "Unemployed", label: "Unemployed" },
      { value: "Student", label: "Student" },
    ];
    const {
      handleNext,
      handleOnchangeInput,
      handleOnSelectInput,
      InfoFormValid,
    } = this.props;
    return (
      <Wrapper>
        <div className="py-4 text-center InformationPage">
          <h6 className="heading">Personal information</h6>
          <p className="caption">
            This information will help determine your identity and is never
            shared without your consent.
          </p>
          <div className="d-flex align-items-center pt-2 pb-1">
            <img src={Info_Icon_Light_Grey} className="infoIcon"></img>
            <p className="infoMsg">
              To ensure accurate terms, your name must match your driver's
              license (ex. John M. Smith Jr.)
            </p>
          </div>

          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="firstName"
                  value={firstName}
                  placeholder="First Name"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="middleName"
                  value={middleName}
                  placeholder="Middle Name"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-7 p-0 pr-1">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="lastName"
                  value={lastName}
                  placeholder="Last Name"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
            <div className="col-5 p-0 pl-2">
              <div className="FormInput">
                <SelectTopLabel
                  getPreApprovedBorder={true}
                  value={initialPlacement}
                  label=""
                  placeholder="Placement"
                  options={suffixType}
                  onChange={handleOnSelectInput(`initialPlacement`)}
                />
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="username"
                  value={username}
                  placeholder="username"
                  readOnly={true}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="address_line"
                  value={address_line}
                  placeholder="address_line"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="addressLine2"
                  value={addressLine2}
                  placeholder="Home Address Line 2"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-6 p-0 pr-2">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="city"
                  value={city}
                  placeholder="city"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
            <div className="col-6 p-0 pl-2">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="stateName"
                  value={stateName}
                  placeholder="stateName"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-6 p-0 pr-2">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="country"
                  value={country}
                  placeholder="country"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
            <div className="col-6 p-0 pl-2">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="zipCode"
                  value={zipCode}
                  placeholder="zipCode"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="dobInput">
                <DatePicker
                  onChange={this.props.handleDOBChange}
                  format="MM-DD-YYYY"
                  value={dob}
                  placeholder="MM-DD-YYYY"
                  disabledDate={this.props.disabledDate}
                />
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <SelectTopLabel
                  getPreApprovedBorder={true}
                  value={employmentStatus}
                  label=""
                  placeholder="Current Employment Status"
                  options={employmentStatusType}
                  onChange={handleOnSelectInput(`employmentStatus`)}
                />
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-12 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="annualIncome"
                  value={annualIncome}
                  placeholder="Your Annual Income"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
                <img src={Question_Grey_Icon} className="questionIcon"></img>
              </div>
            </div>
          </div>
          <div className="py-3">
            <p className="caption m-0">
              Thank you for your honesty and understanding as we ask what we
              know can be invasive questions during this difficult time.
            </p>
          </div>
          <div className={InfoFormValid ? "next_btn" : "inactive_btn"}>
            <ButtonComp disabled={!InfoFormValid} onClick={handleNext}>
              Next
            </ButtonComp>
          </div>
          {AuthPass ? (
            ""
          ) : (
            <p className="footerMsg">
              Already have an account? <span className="loginText">Log in</span>
            </p>
          )}
        </div>
        <style jsx>
          {`
            .heading {
              font-family: "Museo-Sans" !important;
              font-style: normal;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              font-size: 0.951vw;
              letter-spacing: 0.0292vw !important;
            }
            .caption {
              font-family: "Open Sans" !important;
              font-style: normal;
              color: ${FONTGREY_COLOR};
              font-size: 0.732vw;
              letter-spacing: 0.0292vw !important;
              line-height: 1.2;
            }
            .infoIcon {
              width: 1.098vw;
              margin-right: 0.366vw;
            }
            .infoMsg {
              border-left: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              margin: 0;
              font-family: "Open Sans" !important;
              font-style: normal;
              color: ${GREY_VARIANT_2};
              font-size: 0.658vw;
              letter-spacing: 0.0292vw !important;
              line-height: 1.2;
              padding: 0 0.512vw;
              max-width: 80%;
              text-align: left;
            }
            .FormInput {
              margin: 0.292vw 0;
              position: relative;
            }
            :global(.InformationPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.585vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.InformationPage .FormInput .inputBox:focus),
            :global(.InformationPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .questionIcon {
              width: 0.878vw;
              position: absolute;
              top: 55%;
              transform: translate(0, -50%);
              right: 0.951vw;
            }
            :global(.next_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              background: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              border-radius: 0.146vw;
              position: relative;
              height: 2.342vw;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${BG_LightGREY_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            :global(.inactive_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
            }
            :global(.inactive_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              margin: 0;
              text-transform: capitalize;
              border-radius: 0.146vw;
              position: relative;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              height: 2.342vw;
            }

            .footerMsg {
              font-family: "Open Sans" !important;
              font-style: normal;
              color: ${GREY_VARIANT_1};
              font-size: 0.878vw;
              letter-spacing: 0.0292vw !important;
              line-height: 1.2;
              text-align: center;
              padding: 1.098vw 0 0 0;
              margin: 0;
            }
            .footerMsg .loginText {
              color: ${GREEN_COLOR};
              font-family: "Open Sans-SemiBold" !important;
            }
            .InformationPage .dobInput {
              width: 100% !important;
              margin: 0.292vw 0;
              position: relative;
            }
            :global(.InformationPage .dobInput .ant-picker) {
              margin-top: 0.366vw;
              height: 2.342vw;
              width: 100%;
              padding: 0.439vw 0.585vw;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
            }
            :global(.InformationPage .dobInput .ant-picker-focused) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.InformationPage .dobInput .ant-picker-input input) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.InformationPage
                .dobInput
                .ant-picker-input
                input::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.ant-picker-dropdown) {
              font-size: 0.805vw !important;
            }
            :global(.ant-picker-date-panel) {
              width: 16.837vw !important;
            }
            :global(.ant-picker-date-panel .ant-picker-content) {
              width: 15.373vw !important;
              table-layout: auto;
            }
            :global(.ant-picker-panel .ant-picker-footer) {
              display: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default InformationPage;
