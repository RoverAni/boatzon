import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_2,
  Successful_SignUp,
  THEME_COLOR,
  Loan_Approved_Bg,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomLink from "../../components/Link/Link";

class SuccessfullLoanApprovalPage extends Component {
  render() {
    return (
      <Wrapper>
        <div className="confirmedPage">
          <div className="col-9 px-2 pt-3 mx-auto">
            <div className="text-center">
              <img src={Successful_SignUp} className="loaderIcon"></img>
              <h5 className="heading">Congratulations!</h5>
              <p className="caption">You’ve Been Prequalified!</p>
              <p className="Msg">
                You can now see your estimated Monthly Payment and Interest Rate
                on thousands of boats and selected products.
              </p>
              <CustomLink href="/boats">
                <div className="next_btn">
                  <ButtonComp> View Boats </ButtonComp>
                </div>
              </CustomLink>
            </div>
          </div>
          <div className="serviceBg"></div>
        </div>
        <style jsx>
          {`
            .serviceBg {
              min-height: 9.516vw;
              background: url(${Loan_Approved_Bg});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              position: relative;
            }
            .Msg {
              font-family: "Open Sans" !important;
              font-style: normal;
              color: ${FONTGREY_COLOR};
              font-size: 0.732vw;
              letter-spacing: 0.0292vw !important;
              line-height: 1.2;
            }
            .loaderIcon {
              width: 4.026vw;
              margin: 0 auto;
              object-fit: cover;
              margin-bottom: 1.098vw;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.366vw;
              padding: 0.732vw 0 0 0;
            }
            .caption {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              line-height: 1.6;
              letter-spacing: 0.0219vw !important;
              max-width: 68%;
              margin: 0 auto 0.585vw auto;
            }
            :global(.next_btn button) {
              width: 100%;
              padding: 0.366vw 3.294vw;
              background: ${THEME_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              border-radius: 0.146vw;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${BG_LightGREY_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SuccessfullLoanApprovalPage;
