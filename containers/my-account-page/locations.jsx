import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREEN_COLOR,
  Plus_Icon_White,
  GREY_VARIANT_6,
  BG_LightGREY_COLOR,
  Review_Grey_Icon,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
  THEME_COLOR,
  LogIn_Icon_White,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import SwitchInput from "../../components/input-box/switch-input";
import AddNewLocation from "./add-new-location";
import { getAllInvitedUsers, updateUserSubAccount } from "../../services/auth";

class Locations extends Component {
  state = {
    inputpayload: {},
    skip: 0,
    limit: 10,
    allLocations: [],
    addressToEdit: {},
    userData: {},
  };

  storeUserInfo = (data) => {
    this.setState({ userData: data });
  };

  // used to store the switch checked status
  handleSwitchChange = (e, obj) => {
    let allInvites = [...this.state.allLocations];
    let index = allInvites.findIndex((k) => k._id === obj._id);
    allInvites[index].subAccount = e.target.checked;
    this.setState({ allLocations: allInvites }, () => {
      this.updateUserSubAccountFunc(allInvites[index]);
    });
  };

  loginOnNewTab = (data) => {
    window.open(`http://localhost:7122?uname=${data.userName}`, "_blank");
  };

  componentDidMount() {
    getAllInvitedUsers().then((res) => {
      console.log("all invite", res);
      if (res.data.code === 200) {
        this.setState({ allLocations: res.data.data });
      }
    });
  }

  updateUserSubAccountFunc = (obj) => {
    let data = {
      userId: obj._id,
      parentId: obj.parentAccountId,
      subAccount: obj.subAccount,
    };
    updateUserSubAccount(data)
      .then((res) => {
        console.log("success", res);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  // Function to redirect to Add-New-Location Page
  handleAddNewLocation = (mode, index) => {
    // console.log("func", mode, index, this.state.allLocations);
    let obj = this.state.allLocations.filter((k, i) => i === index);
    if (index !== null) this.setState({ addressToEdit: obj });
    this.updateScreen(<AddNewLocation updateScreen={this.updateScreen} mode={mode} addressToEdit={obj} />);
  };

  render() {
    const { location1, currentScreen, allLocations } = this.state;
    return (
      <Wrapper>
        <div className="row mx-0 py-4 justify-content-center tabContent">
          {!currentScreen ? (
            <div className="col-12 px-0">
              <div>
                <div className="mx-4 borderBottom">
                  <h6 className="heading">Locations</h6>
                  <div className="addLocation_btn">
                    <ButtonComp onClick={() => this.handleAddNewLocation("addMode", null)}>
                      <img src={Plus_Icon_White} className="plusIcon"></img>Add Location
                    </ButtonComp>
                  </div>
                </div>
                <div className="locationTabel">
                  <table className={allLocations && allLocations.length > 0 ? "" : "w-100"}>
                    <tr>
                      <th className="nameColumn">
                        <p className="pl-4">Name</p>
                      </th>
                      <th className="locationColumn">
                        <p>Location</p>
                      </th>
                      <th className="adminColumn">
                        <p>Admin</p>
                      </th>
                      <th className="statusColumn">
                        <p>Status</p>
                      </th>
                      <th className="loginColumn"></th>
                      <th className="editColumn">
                        <p className="pr-4"></p>
                      </th>
                    </tr>
                    {allLocations && allLocations.length > 0
                      ? allLocations.map((k, i) => (
                          <tr key={i}>
                            <td>
                              <p className="data pl-4">{k.locationName ? k.locationName : "No House Mentioned"}</p>
                            </td>
                            <td>
                              <p className="data location">{k.location}</p>
                            </td>
                            <td>
                              <p className="data">{k.fullName}</p>
                            </td>
                            <td>
                              <div className="toggleInput" onClick={() => this.storeUserInfo(k)}>
                                <SwitchInput
                                  triggerOnChange={true}
                                  locationToggle={true}
                                  onChange={(e) => this.handleSwitchChange(e, k)}
                                  value={k.subAccount}
                                  checked={k.subAccount}
                                />
                              </div>
                            </td>
                            <td>
                              <div className="login_btn" onClick={() => this.loginOnNewTab(k)}>
                                <ButtonComp>
                                  {" "}
                                  <img src={LogIn_Icon_White} className="loginIcon"></img> Login
                                </ButtonComp>
                              </div>
                            </td>
                            <td>
                              <div className="edit_btn pr-4" onClick={() => this.handleAddNewLocation("editMode", i)}>
                                <ButtonComp>
                                  <img src={Review_Grey_Icon} className="editIcon"></img>
                                  Edit
                                </ButtonComp>
                              </div>
                            </td>
                          </tr>
                        ))
                      : ""}
                  </table>
                </div>
              </div>
            </div>
          ) : (
            currentScreen
          )}
        </div>
        <style jsx>
          {`
            .nameColumn {
              width: 21%;
            }
            .locationColumn {
              width: 27%;
            }
            .adminColumn {
              width: 15%;
            }
            .statusColumn {
              width: 11%;
            }
            .loginColumn {
              width: 13%;
            }
            .editColumn {
              width: 14%;
            }
            .tabContent {
              background: ${WHITE_COLOR};
              position: relative;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              margin: 0;
            }
            .heading:first-letter {
              text-transform: capitalize;
            }
            .plusIcon {
              width: 0.658vw;
              margin-right: 0.366vw;
            }
            .addLocation_btn {
              position: absolute;
              top: -0.3125rem;
              right: 1.188rem;
            }
            :global(.addLocation_btn button) {
              width: 100%;
              margin: 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${GREY_VARIANT_6};
              font-size: 0.805vw;
              font-family: "Open Sans-SemiBold" !important;
              position: relative;
            }
            :global(.addLocation_btn button span) {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.805vw;
            }
            :global(.addLocation_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.addLocation_btn button:focus),
            :global(.addLocation_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.login_btn button) {
              width: 80%;
              padding: 0.658vw;
              margin: 0;
              text-transform: initial;
              background: ${THEME_COLOR};
              color: ${BG_LightGREY_COLOR};
              font-size: 0.805vw;
              font-family: "Museo-Sans" !important;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.login_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.login_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.login_btn button:focus),
            :global(.login_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
            }
            .loginIcon {
              width: 0.805vw;
              margin-right: 0.512vw;
            }
            .editIcon {
              width: 0.805vw;
              margin-right: 0.366vw;
            }
            :global(.edit_btn button) {
              width: 80%;
              min-width: 100%;
              padding: 0.658vw;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.edit_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.edit_btn button:focus),
            :global(.edit_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.edit_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .borderBottom {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 0 0 1.317vw 0;
            }
            .locationTabel {
              padding: 1.317vw 0;
            }
            .locationTabel table tr {
              cursor: pointer;
            }
            .locationTabel table tr:hover {
              background: ${GREY_VARIANT_6};
            }
            .locationTabel table tr:hover .data {
              font-size: 0.805vw;
              font-family: "Open Sans-SemiBold" !important;
            }
            .locationTabel table tr:first-child {
              border-top: none;
              background: none;
            }
            .locationTabel table tr th p {
              font-size: 0.805vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              padding: 0.366vw 0;
              margin: 0;
              font-weight: 500 !important;
            }
            .locationTabel table tr td:first-child,
            .locationTabel table tr th:first-child {
              padding-left: 0 !important;
            }
            .locationTabel table tr td {
              padding: 15px 0;
            }
            .locationTabel table tr td p {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin: 0;
              padding: 0;
            }
            .location {
              max-width: 73.5%;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default Locations;
