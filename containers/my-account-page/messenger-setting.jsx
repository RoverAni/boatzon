import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  GREEN_COLOR,
  GREY_VARIANT_2,
} from "../../lib/config";
import SwitchInput from "../../components/input-box/switch-input";
import { requiredValidator } from "../../lib/validation/validation";
import InputBox from "../../components/input-box/input-box";
import Button from "../../components/button/customButton";
import { submitLeadsForwarding, getLeadsForwarding } from "../../services/contact";
import Snackbar from "../../components/snackbar";

class MessengerSetting extends Component {
  state = {
    inputpayload: {},
    leadForwarding: true,
    cEmail: "",
    fEmail: "",
    open: false,
    usermessage: "",
    variant: "Success",
  };

  // used to store the switch checked status
  handleSwitchChange = (name) => (event) => {
    let inputControl = event;
    this.setState({ [name]: inputControl.target.checked }, () => {
      let tempPayload = this.state.inputpayload;
      tempPayload[[name]] = this.state[name];
      this.setState(
        {
          inputpayload: tempPayload,
        },
        () => {
          console.log("inputpayload", this.state.inputpayload);
        }
      );
    });
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
      [inputControl.name]: inputControl.value,
    });
  };

  componentDidMount() {
    getLeadsForwarding()
      .then((res) => {
        this.setState({ cEmail: res.data.message.cEmail, fEmail: res.data.message.fEmail });
      })
      .catch((err) => console.log("err getLeadsForwarding", err));
  }

  submitLeadsForwardingFunc = () => {
    console.log("clicked");
    submitLeadsForwarding({ fEmail: this.state.fEmail, cEmail: this.state.cEmail })
      .then((res) => {
        console.log("succ submitLeadsForwarding", res);
        this.setState({ open: true, usermessage: "Forward email and Copy email updated successfully", variant: "success" });
      })
      .catch((err) => {
        this.setState({ open: true, usermessage: "Please check your forward and copy email", variant: "error" });
      });
  };

  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const { leadForwarding } = this.state;
    return (
      <Wrapper>
        <div className="row mx-0 py-4 justify-content-center tabContent">
          <div className="col-md-6 col-lg-6 col-xl-6 MessengerSettings_Sec">
            <div>
              <h6 className="heading">Messenger Settings</h6>
              <div className="toggleInput">
                <SwitchInput
                  triggerOnChange={true}
                  messengerSetting={true}
                  label="Messenger & Leads Forwarding "
                  onChange={this.handleSwitchChange("leadForwarding")}
                  value={leadForwarding}
                  checked={leadForwarding}
                  name="leadForwarding"
                />
              </div>
              <div className="FormInput">
                <label>Forward Emails to:</label>
                <div className="position-relative">
                  <InputBox
                    type="email"
                    className="inputBox form-control"
                    name="forwardEmails"
                    value={this.state.fEmail}
                    placeholder="Bryan677@gmail.com"
                    onChange={(e) => this.setState({ fEmail: e.target.value })}
                    // onChange={this.handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                  {/* {this.state.EmailVerified ? (
                  <img src={Verified_Icon} className="verifiedIcon"></img>
                ) : (
                  <img src={NotVerified_Icon} className="notVerifiedIcon"></img>
                )}
                {this.state.EmailVerified ? (
                  " "
                ) : (
                  <p className="verify_btn" onClick={this.handleNewEmailCheck}>
                    Verify
                  </p>
                )} */}
                </div>

                {/* {this.state[EMAIL] == 0 || this.state[EMAIL] == 2 ? (
                <p className="errMessage">Enter Valid Email</p>
              ) : (
                ""
              )}
              <p className="emailChangeMsg">
                Changing your email will require re-verefication.
              </p> */}
              </div>
              <div className="FormInput">
                <label>Copy Emails to:</label>
                <div className="position-relative">
                  <InputBox
                    type="email"
                    className="inputBox form-control"
                    name="copyEmail"
                    value={this.state.cEmail}
                    placeholder="Enter an email"
                    onChange={(e) => this.setState({ cEmail: e.target.value })}
                    // onChange={this.handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                  {/* {this.state.EmailVerified ? (
                  <img src={Verified_Icon} className="verifiedIcon"></img>
                ) : (
                  <img src={NotVerified_Icon} className="notVerifiedIcon"></img>
                )}
                {this.state.EmailVerified ? (
                  " "
                ) : (
                  <p className="verify_btn" onClick={this.handleNewEmailCheck}>
                    Verify
                  </p>
                )} */}
                </div>

                {/* {this.state[EMAIL] == 0 || this.state[EMAIL] == 2 ? (
                <p className="errMessage">Enter Valid Email</p>
              ) : (
                ""
              )}
              <p className="emailChangeMsg">
                Changing your email will require re-verefication.
              </p> */}
              </div>
              <div className="save_btn">
                <Button handler={this.submitLeadsForwardingFunc} text="Save" disabled={false} />
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            .tabContent {
              background: ${WHITE_COLOR};
            }
            .heading {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.732vw;
            }
            .heading:first-letter {
              text-transform: capitalize;
            }
            .toggleInput {
              border-bottom: 1px solid ${GREY_VARIANT_3} !important;
              padding-bottom: 0.366vw;
            }
            .MessengerSettings_Sec .FormInput {
              padding-top: 0.732vw;
              position: relative;
            }
            .MessengerSettings_Sec .FormInput label {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin-bottom: 0.292vw;
            }
            .MessengerSettings_Sec .FormInput label:first-letter {
              text-transform: capitalize;
            }
            :global(.MessengerSettings_Sec .FormInput .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.MessengerSettings_Sec .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              font-size: 0.805vw;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            .save_btn {
              float: right;
              width: 40%;
              margin: 0.732vw 0;
              z-index: 999 !important;
            }
            :global(.save_btn button) {
              width: 100%;
              margin: 0.732vw 0 0 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
              position: relative;
              border: none;
            }
            :global(.save_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
          `}
        </style>
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={"bottom"}
          horizontal={"left"}
        />
      </Wrapper>
    );
  }
}
export default MessengerSetting;
