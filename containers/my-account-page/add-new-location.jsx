import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  THEME_COLOR,
  Border_LightGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  GREY_VARIANT_1,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import { requiredValidator } from "../../lib/validation/validation";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import LocationSearchInput from "../location/location-search-box";
import Locations from "./locations";
import { connect } from "react-redux";
import { getCookie } from "../../lib/session";
import { editAndUpdateSubUserAccount, inviteNewUser } from "../../services/auth";
import Snackbar from "../../components/snackbar";

let checkForMissingFields = (obj) => {
  return Object.values(obj).every((k, i) => {
    try {
      if (k.length > 0 || k !== "") return true;
      return false;
    } catch (e) {}
  });
};

class AddNewLocation extends Component {
  state = {
    country: "",
    locationName: "",
    city: "",
    zipCode: "",
    stateName: "",
    adminNumber: "",
    addressLine1: "",
    adminPhoneNumber: "",
    defaultAddress: false,
    phone: "",
    vertical: "bottom",
    horizontal: "left",
    inputpayload: {},
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
      [inputControl.name]: inputControl.value,
    });
  };

  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  editInvitiedUserDetails = () => {
    let { addressToEdit } = this.props;
    const { locationName, addressLine1, city, stateName, zipCode, adminEmail, adminName, adminPhoneNumber } = this.state;
    let data = {
      locationName: locationName, // ghar ka naam
      city: addressLine1.city || city,
      zip: addressLine1.zipCode || zipCode,
      stateName: addressLine1.area || stateName,
      location: addressLine1.address, // long address
      phone: adminPhoneNumber,
      fullName: adminName,
      email: adminEmail,
      userId: addressToEdit && addressToEdit[0] && addressToEdit[0]._id,
      parentId: getCookie("mqttId"),
    };
    editAndUpdateSubUserAccount(data)
      .then((res) => {
        if (res.data.code === 200) {
          this.setState({
            open: true,
            usermessage: "User Invite Update successfull...",
            variant: "success",
          });
          this.props.updateScreen(<Locations updateScreen={this.props.updateScreen} />);
        } else {
          this.setState({
            open: true,
            usermessage: "Something went wrong..",
            variant: "error",
          });
        }
      })
      .catch((err) =>
        this.setState({
          open: true,
          usermessage: "Something went wrong..",
          variant: "error",
        })
      );
  };

  updateUserAddress = (data) => {
    this.setState({ addressLine1: data });
  };

  componentDidMount() {
    let { addressToEdit } = this.props;
    if (this.props.mode === "editMode") {
      this.setState({
        locationName: addressToEdit && addressToEdit[0] && addressToEdit[0].locationName, // ghar ka naam
        country: addressToEdit && addressToEdit[0] && addressToEdit[0].country,
        city: addressToEdit && addressToEdit[0] && addressToEdit[0].city,
        zipCode: addressToEdit && addressToEdit[0] && addressToEdit[0].zip,
        stateName: addressToEdit && addressToEdit[0] && addressToEdit[0].state,
        addressLine1: addressToEdit && addressToEdit[0] && addressToEdit[0].location, // long address
        defaultAddress: false,
        adminName: addressToEdit && addressToEdit[0] && addressToEdit[0].fullName,
        adminPhoneNumber: addressToEdit && addressToEdit[0] && addressToEdit[0].phone,
        adminEmail: addressToEdit && addressToEdit[0] && addressToEdit[0].email,
      });
    }
  }

  sendInvitation = () => {
    const { locationName, addressLine1, city, stateName, zipCode, adminEmail, adminName, adminPhoneNumber } = this.state;
    let data = {
      location: addressLine1.address, // long address
      locationName: locationName, // ghar ka naam
      longitude: addressLine1.lat,
      latitude: addressLine1.lng,
      phoneNumber: adminPhoneNumber,
      countryCode: "+1",
      city: city || addressLine1.city,
      fullName: adminName,
      email: adminEmail,
      state: addressLine1.area || stateName,
      zipCode: addressLine1.zipCode || zipCode,
      userId: getCookie("mqttId"),
    };

    let result = checkForMissingFields(data);
    if (result) {
      inviteNewUser(data)
        .then((res) => {
          if (res.data.code === 200) {
            this.setState({
              open: true,
              usermessage: "User Invite send successfull...",
              variant: "success",
            });
            this.props.updateScreen(<Locations updateScreen={this.props.updateScreen} />);
            console.log("user invite send successfull");
          } else if (res.data.code === 409) {
            this.setState({
              open: true,
              usermessage: res.data.message,
              variant: "error",
            });
          } else {
            this.setState({
              open: true,
              usermessage: res.data.message || "Something went wrong",
              variant: "error",
            });
          }
        })
        .catch((err) => {
          console.log("err", err);
          this.setState({
            open: true,
            usermessage: (err && err.message) || "Something went wrong",
            variant: "error",
          });
        });
    } else {
      this.setState({
        open: true,
        usermessage: "Please check your inputs and try again...",
        variant: "warning",
      });
    }
  };

  render() {
    console.log("addressLine1", this.state.addressLine1);
    const { locationName, addressLine1, addressLine2, city, stateName, zipCode, adminEmail, adminName, adminPhoneNumber } = this.state;

    const { handleOnchangeInput } = this;
    return (
      <Wrapper>
        <div className="col-12 px-5 addNewLocation_Sec">
          <div>
            <h6 className="heading">Add New Location</h6>
            <div className="row m-0">
              <div className="col-6 p-0 pr-2">
                <div>
                  <div className="FormInput">
                    <label>Location Name</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="locationName"
                      placeholder="Location Name"
                      value={locationName}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
              </div>
              <div className="col-6 p-0 pl-2"></div>
            </div>
            <div className="row m-0 my-2">
              <div className="col-6 p-0 pr-2">
                <div>
                  <div className="FormInput">
                    <label>Address Line 1</label>
                    <LocationSearchInput
                      id="user-address-box"
                      address={addressLine1}
                      placeHolder="Enter Address 1"
                      updateLocation={this.updateUserAddress}
                      handleChange={this.updateUserAddress}
                      showOnlyAddress={true}
                      placeholder="Enter Address 1"
                      showAddressFromProps={true}
                      addlocationScreen={true}
                    ></LocationSearchInput>
                  </div>
                </div>
              </div>
              <div className="col-6 p-0 pl-2">
                <div>
                  <div className="FormInput">
                    <label>Address Line 2</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="addressLine2"
                      placeholder=""
                      value={addressLine2}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
              </div>
            </div>
            <div className="row m-0 borderBottom mb-3">
              <div className="col-6 p-0 pr-2">
                <div>
                  <div className="FormInput">
                    <label>City</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="city"
                      placeholder="Enter a city"
                      value={(addressLine1 && addressLine1.city) || city}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
              </div>
              <div className="col-6 p-0 pl-2">
                <div className="row m-0">
                  <div className="col-7 p-0 pr-1">
                    <div>
                      <div className="FormInput">
                        <label>State</label>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="stateName"
                          placeholder="Enter a state"
                          value={(addressLine1 && addressLine1.area) || stateName}
                          onChange={handleOnchangeInput}
                          autoComplete="off"
                        ></InputBox>
                      </div>
                    </div>
                  </div>
                  <div className="col-5 p-0 pl-1">
                    <div>
                      <div className="FormInput">
                        <label>Zip</label>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="zipCode"
                          placeholder="Enter Zip Code"
                          value={(addressLine1 && addressLine1.zipCode) || zipCode}
                          onChange={handleOnchangeInput}
                          autoComplete="off"
                        ></InputBox>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <h6 className="heading">Invite Admin</h6>
            <div className="row m-0">
              <div className="col-6 p-0 pr-2">
                <div>
                  <div className="FormInput">
                    <label>Email</label>
                    <InputBox
                      type="email"
                      className="inputBox form-control"
                      name="adminEmail"
                      placeholder="Name@example.com"
                      value={adminEmail}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
              </div>
              <div className="col-6 p-0 pl-2">
                <div>
                  <div className="FormInput">
                    <label>Name (optional)</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="adminName"
                      placeholder="Optional"
                      value={adminName}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
              </div>
            </div>
            <div className="row m-0 BorderBottom mb-3">
              <div className="col-6 p-0 pr-2">
                <div className="FormInput">
                  <label>Phone Number</label>
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="adminPhoneNumber"
                    placeholder="Enter Phone Number"
                    value={adminPhoneNumber}
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                  ></InputBox>
                </div>
              </div>
              {this.props.mode === "addMode" ? (
                <div className="col-6 p-0 pl-2 d-flex align-items-end">
                  <div className="sendInvitation_btn w-100" onClick={this.sendInvitation}>
                    <ButtonComp>Send Invitation and Save</ButtonComp>
                  </div>
                </div>
              ) : (
                <></>
              )}
            </div>

            <div className="row m-0">
              <div className="col-6 p-0 pr-2"></div>
              <div className="col-6 p-0 pl-2">
                {this.props.mode === "editMode" ? (
                  <div className="save_btn">
                    <ButtonComp onClick={this.editInvitiedUserDetails}>Save Changes</ButtonComp>
                  </div>
                ) : (
                  <div className="save_btn">
                    <ButtonComp
                      onClick={() => {
                        this.props.updateScreen(<Locations updateScreen={this.props.updateScreen} />);
                      }}
                    >
                      Cancel
                    </ButtonComp>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        <style jsx>
          {`
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              margin: 0;
            }
            .heading:first-letter {
              text-transform: capitalize;
            }
            .addNewLocation_Sec .FormInput {
              padding-top: 0.732vw;
              position: relative;
            }
            .addNewLocation_Sec .FormInput label {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin-bottom: 0.292vw;
            }
            .addNewLocation_Sec .FormInput label:first-letter {
              text-transform: capitalize;
            }
            :global(.addNewLocation_Sec .FormInput .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.addNewLocation_Sec .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              font-size: 0.805vw;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            .borderBottom {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
              padding-bottom: 2.562vw;
            }
            .BorderBottom {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
              padding-bottom: 2.196vw;
            }
            :global(.sendInvitation_btn button) {
              width: 100%;
              margin: 1.098vw 0 0 0;
              padding: 0.366vw 0;
              text-transform: initial;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              font-size: 0.951vw;
              font-family: "Museo-Sans" !important;
              position: relative;
            }
            :global(.sendInvitation_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
            }
            :global(.sendInvitation_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.sendInvitation_btn button:focus),
            :global(.sendInvitation_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
            }
            :global(.save_btn button) {
              width: 100%;
              margin: 0.585vw 0 0 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
              position: relative;
            }
            :global(.save_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userPhoneNumber: state.userProfileData,
  };
};

export default connect(mapStateToProps, null)(AddNewLocation);
