import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_1,
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  WHITE_COLOR,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR,
  Visa_Card_Grey_Icon,
  Master_Card_Grey_Icon,
  American_Express_Card_Grey_Icon,
  Discover_Card_Grey_Icon,
} from "../../lib/config";
// import InputBox from "../../components/input-box/input-box";
import { loadStripe } from "@stripe/stripe-js";
import _SplitFieldsForm from "./stripe-folders/split-form";

class AddPaymentMethod extends Component {
  state = {
    inputpayload: {},
  };
  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = this.state.inputpayload;
    tempsignUpPayload[[name]] = inputControl.value;
    this.setState({
      inputpayload: tempsignUpPayload,
    });
  };
  render() {
    return (
      <Wrapper>
        <div className="row m-0 ">
          <div className="col-10 addCardTabContent">
            <section>
              <div className="d-flex align-items-center justify-content-between">
                <h6 className="heading">Add new credit card</h6>
                <div>
                  <div className="d-flex align-items-center justify-content-evenly">
                    <img src={Visa_Card_Grey_Icon} className="cardIcon"></img>
                    <img src={Master_Card_Grey_Icon} className="cardIcon"></img>
                    <img src={American_Express_Card_Grey_Icon} className="cardIcon"></img>
                    <img src={Discover_Card_Grey_Icon} className="cardIcon"></img>
                  </div>
                </div>
              </div>
              <p className="paymentMsg">Safe money transfer using your bank account Visa, Maestro, Discover, American Express.</p>

              <_SplitFieldsForm updateScreen={this.props.updateScreen} />

              {/* <div className="row m-0">
                <div className="col-5"></div>
                <div className="col-7 p-0">
                  <div className="row m-0">
                    <div className="col-5 p-0">
                      <div className="cancel_btn">
                        <ButtonComp onClick={() => this.props.updateScreen(<PaymentMethods updateScreen={this.props.updateScreen} />)}>Cancel</ButtonComp>
                      </div>
                    </div>
                    <div className="col-7 pr-0">
                      <div className="save_btn">
                        <ButtonComp onClick={() => this.props.updateScreen(<PaymentMethods updateScreen={this.props.updateScreen} />)}>
                          Save and Continue
                        </ButtonComp>
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
            </section>
          </div>
        </div>
        <style jsx>
          {`
            .cardIcon {
              width: 100%;
              margin: 0 0 0 0.732vw;
            }
            :global(section) {
              width: 76%;
              margin: auto;
              padding: 1.5625vw 0;
            }
            :global(.cancel_btn button) {
              width: 100%;
              padding: 0.585vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
            }
            :global(.cancel_btn button span) {
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_1};
              font-size: 0.805vw;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.save_btn button) {
              width: 100%;
              padding: 0.585vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
            }
            :global(.save_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${BG_LightGREY_COLOR};
              font-size: 0.805vw;
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .addCardTabContent {
              background: ${WHITE_COLOR};
              position: relative;
            }
            :global(.inputWidth) {
              width: 93% !important;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.732vw;
            }
            .heading:first-letter {
              text-transform: capitalize;
            }
            .paymentMsg {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.219vw;
              text-transform: capitalize;
              max-width: 70%;
            }
            .cardInput_Sec {
              padding: 0.732vw 1.464vw;
              margin: 0.732vw 0;
              border: 0.0732vw dashed ${GREY_VARIANT_3};
            }
            .FormInput {
              margin: 1.098vw 0;
            }
            .cardInput_Sec .FormInput label {
              font-size: 0.878vw;
              margin: 0;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }

            :global(.cardInput_Sec .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.196vw;
              padding: 0.439vw 0.878vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.cardInput_Sec .FormInput .inputBox:focus) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default AddPaymentMethod;
