import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  GREY_VARIANT_2,
  GREEN_COLOR,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  BG_LightGREY_COLOR,
  Password_Green,
  Stripe_Logo,
  BOX_SHADOW_GREY,
  GREEN_COLOR_Opacity,
  View_Transacrtion_Blue_Icon,
  GREY_VARIANT_1,
  GREY_VARIANT_10,
  Stripe_Color,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import { F_NAME, L_NAME, DOB } from "../../lib/input-control-data/input-definitions";
import ButtonComp from "../../components/button/button";
import { requiredValidator, PureTextValidator } from "../../lib/validation/validation";
import SwitchInput from "../../components/input-box/switch-input";
import { getCookie } from "../../lib/session";
import { getConnectedStripeAccount, getConnectedBalance, getConnectLink } from "../../services/transaction";

class DepositAccount extends Component {
  state = {
    inputpayload: {},
    isfastDeposit: true,
    depositeAccLogin: false,
    myMoney: "",
    isConnectLink: "",
  };

  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate }, () => {
      let tempPayload = this.state.inputpayload;
      tempPayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: tempPayload,
      });
    });
  };

  // used to store the switch checked status
  handleSwitchChange = (name) => (event) => {
    let inputControl = event;
    this.setState({ [name]: inputControl.target.checked }, () => {
      let tempPayload = this.state.inputpayload;
      tempPayload[[name]] = this.state[name];
      this.setState(
        {
          inputpayload: tempPayload,
        },
        () => {
          console.log("inputpayload", this.state.inputpayload);
        }
      );
    });
  };

  componentDidMount() {
    getConnectedStripeAccount(getCookie("mqttId"))
      .then((res) => {
        if (res.status === "200" && res.data.data === true) {
          this.setState({ depositeAccLogin: true });
        }
      })
      .catch((err) => this.setState({ depositeAccLogin: false }));
    getConnectedBalance(getCookie("mqttId"))
      .then((res) => {
        if (res.status === "200") {
          this.setState({ myMoney: res.data.data });
        }
      })
      .catch((err) => this.setState({ depositeAccLogin: false }));
    getConnectLink(getCookie("mqttId")).then((res) => {
      if (res.status === "200") {
        this.setState({ isConnectLink: res.data.data.url });
      }
      console.log("getConnectLink", res);
    });
  }

  addDepositAccount = () => {
    if (this.state.isConnectLink.length == 0) {
      window.open(
        `https://connect.stripe.com/express/oauth/authorize?client_id=ca_GBTx9U1n8dizvKwFEhu07pJhWBKZX4Ge&state=${getCookie(
          "mqttId"
        )}&scope=read_write&response_type=code&suggested_capabilities[]=card_payments&suggested_capabilities[]=transfers&redirect_uri=https://stripe.boatzon.com/v1/connectAuth`,
        "_blank"
      );
    } else {
      window.open(this.state.isConnectLink, "_blank");
    }
  };

  render() {
    console.log("myMoney", this.state.myMoney);
    const fastDepositLabel = (
      <div>
        <h6 className="labelTitle">Fast Deposit</h6>
        <p className="labelDesc">
          Debit card is required. Transaction fee will be applied after your trial ends. <a target="_blank">Learn more</a>
        </p>
      </div>
    );
    const { isfastDeposit, depositeAccLogin } = this.state;
    return (
      <Wrapper>
        <div className="row m-0">
          <div className="col-11">
            <div className="col-12 tabContent">
              <h6 className="Heading">Tell us a little bit of yourself</h6>
              <div className="section pb-0">
                <p className="verificationMsg">
                  Boatzone partners with Stripe to securely process each payment. To promote transparency and prevent fraud. Stripe needs to verify your legal
                  name and date of birth. <a>Learn more</a>
                </p>
                <div className="row m-0 my-3 userDetails justify-content-between align-items-center">
                  <div className="col-6 p-0">
                    <div className="FormInput">
                      <label>first name</label>
                      <InputBox
                        type="text"
                        className="inputBox form-control"
                        name={F_NAME}
                        placeholder="First Name"
                        onChange={this.handleOnchangeInput(`${F_NAME}`)}
                        autoComplete="off"
                        onKeyPress={PureTextValidator}
                      ></InputBox>
                    </div>
                  </div>
                  <div className="col-6 p-0">
                    <div className="FormInput">
                      <label>last name</label>
                      <InputBox
                        type="text"
                        className="inputBox form-control"
                        name={L_NAME}
                        placeholder="Last Name"
                        onChange={this.handleOnchangeInput(`${L_NAME}`)}
                        autoComplete="off"
                        onKeyPress={PureTextValidator}
                      ></InputBox>
                    </div>
                  </div>
                </div>
                <div className="row m-0 my-3 userDetails justify-content-between align-items-center">
                  <div className="col-6 p-0">
                    <div className="FormInput">
                      <label>date of Birth</label>
                      <InputBox
                        type="text"
                        className="inputBox form-control"
                        name={DOB}
                        placeholder="MM/DD/YYYY"
                        onChange={this.handleOnchangeInput(`${DOB}`)}
                        autoComplete="off"
                      ></InputBox>
                    </div>
                  </div>
                </div>
                <p className="securityMsg">
                  <img src={Password_Green} className="securityIcon"></img>We take privacy and security very seriously.
                </p>
                <div className="verify_btn">
                  <ButtonComp>Verify</ButtonComp>
                </div>
              </div>
            </div>
            <div className="col-12 tabContent ">
              <h6 className="heading">Deposit Account</h6>
              <div className="section">
                {depositeAccLogin ? (
                  <div className="row m-0 align-items-center d-flex">
                    <div className="col-5 LoginUserInfo p-0">
                      <div className="d-flex align-items-center">
                        <div className="stripe_btn">
                          <ButtonComp>
                            <img src={Stripe_Logo} className="stripeLogo"></img>
                          </ButtonComp>
                        </div>
                        <p className="loginUserName">Bob Jones</p>
                        <img src={View_Transacrtion_Blue_Icon} className="viewIcon"></img>
                      </div>
                    </div>
                    <div className="col p-0">
                      <div className="row m-0 align-items-center d-flex">
                        <div className="col-6 summaryContent p-0">
                          <span className="label">This Week</span>
                          <p className="price">
                            ${this.state.myMoney && this.state.myMoney.available && this.state.myMoney.available[0] && this.state.myMoney.available[0].amount}
                          </p>
                          <p>2 trades</p>
                        </div>
                        <div className="col-6 summaryContent p-0">
                          <span className="label">Your Balance</span>
                          <p className="price">
                            ${this.state.myMoney && this.state.myMoney.available && this.state.myMoney.available[0] && this.state.myMoney.available[0].amount}
                          </p>
                          <p>
                            ${this.state.myMoney && this.state.myMoney.available && this.state.myMoney.available[0] && this.state.myMoney.available[0].amount}{" "}
                            avaiable
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-3 paymentBtn_Sec p-0">
                      <div className="payment_btn">
                        <ButtonComp>Payment now</ButtonComp>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="addDeposit_btn" onClick={this.addDepositAccount}>
                    <ButtonComp>Add deposit account</ButtonComp>
                  </div>
                )}
              </div>
              {depositeAccLogin ? (
                ""
              ) : (
                <div className="section pb-0">
                  <div className="changeDeposit_btn" onClick={this.addDepositAccount}>
                    <ButtonComp>Change deposit account</ButtonComp>
                  </div>
                </div>
              )}
            </div>
            <div className="col-12 tabContent noMargin">
              <h6 className="heading">Deposit Account</h6>
              <div className="section pb-0">
                <SwitchInput
                  triggerOnChange={true}
                  label={fastDepositLabel}
                  onChange={this.handleSwitchChange("isfastDeposit")}
                  value={isfastDeposit}
                  checked={isfastDeposit}
                  name="isfastDeposit"
                />
                <div className="fastDeposit_Sec">
                  <h5 className="Heading">Get your money sooner with Fast Deposit</h5>
                  <p>With Fast Deposit, your bank will typically receive your money as soon as released.</p>
                  <p>
                    Fast Deposit is free until. After, the standard 1% fee for Fast Deposit will be charged (minimum $0.50). Fast Deposit fee is in addition to
                    standard payment fees.
                  </p>
                  <a>How Fast Deposit works</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            .stripeLogo {
              width: 2.562vw;
            }
            .LoginUserInfo {
              max-width: 11.713vw;
            }
            :global(.stripe_btn button) {
              width: 100%;
              margin: 0;
              padding: 0.292vw 0.878vw;
              border-radius: 0.219vw;
              border: 0.0732vw solid ${Stripe_Color};
              position: relative;
            }
            :global(.stripe_btn button:hover) {
              background: none;
            }
            :global(.stripe_btn button:focus),
            :global(.stripe_btn button:active) {
              background: none;
              outline: none;
            }
            .loginUserName {
              font-size: 0.805vw;
              margin: 0 0.366vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans-Semibold" !important;
              margin-bottom: 0.219vw;
              text-transform: capitalize;
            }
            .viewIcon {
              width: 0.951vw;
              object-fit: cover;
            }
            .paymentBtn_Sec {
              max-width: 7.32vw;
            }
            :global(.payment_btn button) {
              width: 100%;
              margin: 0;
              padding: 0.292vw;
              text-transform: initial;
              background: ${WHITE_COLOR};
              color: ${THEME_COLOR};
              border: 0.0732vw solid ${THEME_COLOR};
              position: relative;
            }
            :global(.payment_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.payment_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            :global(.payment_btn button:focus),
            :global(.payment_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
            }
            .summaryContent {
              max-width: 6.588vw;
            }
            .summaryContent .label {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans-Semibold" !important;
              margin-bottom: 0.219vw;
              text-transform: capitalize;
            }
            .summaryContent .price {
              font-size: 0.951vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0;
            }
            .summaryContent p {
              font-size: 0.658vw;
              color: ${GREY_VARIANT_10};
              font-family: "Open Sans" !important;
              margin-bottom: 0.585vw;
              text-transform: lowercase;
              margin-bottom: 0;
            }
            .tabContent {
              background: ${WHITE_COLOR};
              padding: 2.489vw 2.196vw;
              border-radius: 0.146vw;
              margin-bottom: 1.317vw !important;
              box-shadow: 0px 1px 2px 0px ${BOX_SHADOW_GREY} !important;
            }
            .heading {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
            }
            .section {
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 1.098vw 0;
            }
            .verificationMsg {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              margin-bottom: 0;
            }
            .verificationMsg a {
              font-family: "Open Sans" !important;
              color: ${GREEN_COLOR};
            }
            .FormInput {
              padding-top: 0.732vw;
            }
            .userDetails .FormInput label {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin-bottom: 0.292vw;
            }
            .userDetails .FormInput label:first-letter {
              text-transform: capitalize;
            }
            :global(.userDetails .FormInput .inputBox) {
              width: 95%;
              height: 2.342vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans-SemiBold" !important;
            }
            :global(.userDetails .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
            }
            .securityMsg {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_2};
              margin-bottom: 0;
            }
            .securityIcon {
              width: 0.585vw;
              margin-right: 0.292vw;
              margin-bottom: 0.219vw;
            }
            :global(.verify_btn button) {
              width: 30%;
              margin: 1.171vw 0 0 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              position: relative;
            }
            :global(.verify_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              font-weight: 600;
            }
            :global(.verify_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.verify_btn button:focus),
            :global(.verify_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.addDeposit_btn button) {
              width: 35%;
              margin: 0.366vw 0;
              text-transform: initial;
              background: ${WHITE_COLOR};
              color: ${THEME_COLOR};
              border: 0.0732vw solid ${THEME_COLOR};
              position: relative;
            }
            :global(.addDeposit_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
            }
            :global(.addDeposit_btn button:hover) {
              background: ${WHITE_COLOR};
              border: 0.0732vw solid ${THEME_COLOR};
            }
            :global(.addDeposit_btn button:focus),
            :global(.addDeposit_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
            }
            :global(.changeDeposit_btn button) {
              width: fill-content;
              padding: 0;
              margin: 0.732vw 0 0 0;
              text-transform: initial;
              background: ${WHITE_COLOR};
              color: ${THEME_COLOR};
              border: none;
              position: relative;
            }
            :global(.changeDeposit_btn button span) {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.951vw;
            }
            :global(.changeDeposit_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            :global(.changeDeposit_btn button:focus),
            :global(.changeDeposit_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
            }
            :global(.labelTitle) {
              font-size: 0.951vw;
              margin-bottom: 0.366vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              text-transform: capitalize;
            }
            :global(.labelDesc) {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              margin-bottom: 0;
            }
            :global(.labelDesc a) {
              color: ${GREEN_COLOR} !important;
              text-decoration: none;
              font-family: "Open Sans" !important;
            }
            .fastDeposit_Sec {
              background: ${GREEN_COLOR_Opacity};
              padding: 1.098vw;
              margin: 1.098vw 0 0 0;
            }
            .Heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
            }
            .fastDeposit_Sec p {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              margin-bottom: 0.585vw;
            }
            .fastDeposit_Sec a {
              color: ${GREEN_COLOR} !important;
              text-decoration: none;
              font-size: 0.878vw;
              font-family: "Open Sans-SemiBold" !important;
              cursor: pointer;
            }
            .noMargin {
              margin: 0 !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default DepositAccount;
