import React, { useEffect, useState } from "react";
import Wrapper from "../../hoc/Wrapper";
import { getAllShippingAddress, storeShippingAddress } from "../../services/contact";
import InputBox from "../../components/input-box/input-box";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  GREY_VARIANT_2,
  Red_Color_1,
  GREEN_COLOR,
  Selected_Checkbox_Green_Icon,
  Edit_Icon,
  Close_Light_Grey_Icon,
  Add_Card_Green_Icon,
  THEME_COLOR,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import { withStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import ButtonComp from "../../components/button/button";
import LocationSearchInput from "../location/location-search-box";
import { getCookie } from "../../lib/session";
import Snackbar from "../../components/snackbar";

const GreenCheckbox = withStyles({
  root: {
    padding: "0px",
    color: "#c0c7d9",
    "&$checked": {
      color: "#c0c7d9",
    },
  },
  checked: {
    padding: "0px",
    color: "#c0c7d9",
  },
})((props) => {
  console.log("props", props);
  return <Checkbox disableRipple={true} color="default" {...props} />;
});

const ShippingSettings = () => {
  const [state, setState] = useState({
    shippingAddresses: [],
    activeCard: 0,
    isChecked: false,
    open: false,
    usermessage: "",
    variant: "success",
  });
  const [changeView, setChangeView] = useState(false);
  const handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    setState({ ...state, open: false });
  };

  /** toggle checkbox to make a address default */
  const toggleCheckbox = (e) => setState({ ...state, isChecked: e.target.checked });

  /** change view to add shipping view */
  const toggleView = () => setChangeView(!changeView);

  /** select shipping address by index */
  const setActiveCard = (index) => setState({ ...state, activeCard: index });

  /** API to save new shippo address */
  const saveNewLocation = async () => {
    let { _state, address, city, zip, country, isChecked, phoneNumber, cc } = state;
    let obj = {
      userId: getCookie("mqttId"),
      countryCode: country,
      country: country,
      city: city,
      postalCode: zip,
      state: _state,
      address: address,
      defaultAddress: isChecked,
      phone: "+" + cc + phoneNumber,
    };
    let res = await storeShippingAddress(obj, "");
    console.log("res", res);
    if (res.status == "200") {
      setState({
        ...state,
        open: true,
        usermessage: "Address added successfully",
        variant: "success",
        country: "",
        city: "",
        zip: "",
        _state: "",
        address: "",
        isChecked: false,
        phoneNumber: "",
        cc: "",
      });
      toggleView();
      _getAllShippingAddress();
    } else {
      setState({ ...state, open: true, usermessage: res.data.message || "Please check the fields", variant: "error" });
    }
  };

  /** to fetch address from geolocation API */
  const updateUserAddress = (data) => {
    setState({
      ...state,
      address: data.address,
      city: data.city,
      country: data.countryShortName,
      _state: data.stateName,
      zip: data.zipCode,
    });
  };

  /** text handler */
  const handleOnchangeInput = (e) => setState({ ...state, [e.target.name]: e.target.value });

  useEffect(() => {
    (async () => {
      _getAllShippingAddress();
    })();
  }, []);

  const _getAllShippingAddress = async () => {
    let shippingAddresses = await getAllShippingAddress(0, 20);
    if (shippingAddresses.status == "200") {
      setState({ ...state, shippingAddresses: shippingAddresses.data.data });
      console.log("shippingAddresses", shippingAddresses.data.data);
    }
  };

  return (
    <Wrapper>
      {changeView ? (
        <div className="row mx-0 py-4 justify-content-center tabContent">
          <div className="col-12">
            <h6 className="heading">Add New Shipping Address</h6>
            <div className="list-unstyled d-flex flex-wrap align-items-center justify-content-between cardSec w-100">
              <div className="row m-0 w-100">
                <div className="col-6 p-0 left-input">
                  <div className="FormInput">
                    <label>Address</label>
                    <LocationSearchInput
                      id="user-address-box"
                      updateLocation={updateUserAddress}
                      handleChange={updateUserAddress}
                      showOnlyAddress={true}
                      showAddressFromProps={true}
                      locationModel={false}
                    ></LocationSearchInput>
                  </div>
                </div>
                <div className="col-6 p-0 right-input">
                  <div>
                    <div className="FormInput">
                      <label>Country</label>
                      <InputBox
                        type="text"
                        className="inputBox form-control"
                        name="country"
                        // placeholder="Optional"
                        value={state && state.country}
                        onChange={handleOnchangeInput}
                        autoComplete="off"
                      ></InputBox>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row m-0 w-100">
                <div className="col-6 p-0 left-input">
                  <div className="FormInput">
                    <label>City</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="city"
                      placeholder=""
                      value={state && state.city}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-4 p-0 right-input">
                  <div className="FormInput">
                    <label>State</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="_state"
                      //   placeholder="Optional"
                      value={state && state._state}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-2 p-0 pl-3">
                  <div className="FormInput">
                    <label>ZIP</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="zip"
                      //   placeholder="Optional"
                      value={state && state.zip}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
              </div>

              <div className="row m-0 w-100 align-items-center">
                <div className="col-2 p-0 left-input">
                  <div className="FormInput">
                    <label>Phone Number</label>
                    <InputBox
                      type="text"
                      className="inputBox form-control"
                      name="cc"
                      placeholder="+1 (US)"
                      value={state && state.cc}
                      onChange={handleOnchangeInput}
                      autoComplete="off"
                    ></InputBox>
                  </div>
                </div>
                <div className="col-4 p-0 left-input">
                  <div>
                    <div className="FormInput">
                      <label></label>
                      <InputBox
                        type="text"
                        className="inputBox form-control"
                        name="phoneNumber"
                        // placeholder="Optional"
                        value={state && state.phoneNumber}
                        onChange={handleOnchangeInput}
                        autoComplete="off"
                      ></InputBox>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row m-0 w-100 align-items-center checkbox-section">
                <label className="mb-0 checkbox-container">
                  {/* <input type="checkbox" className="checkbox" checked={state.isChecked} onChange={toggleCheckbox} />
                  <span class="checkmark"></span> */}
                  <Checkbox
                    icon={<GreenCheckbox fontSize="small" checked={state.isChecked} onChange={toggleCheckbox} />}
                    checkedIcon={<GreenCheckbox fontSize="small" checked={state.isChecked} onChange={toggleCheckbox} />}
                  />
                  <span className="checkbox-text">Default Address</span>
                </label>
              </div>

              <div className="row m-0 w-100 align-items-center">
                <div className="col-7 p-0 left-input"></div>
                <div className="col-5 p-0 right-input">
                  <div className="row mx-0">
                    <div className="col-4 pl-0 cancel_btn">
                      <ButtonComp onClick={toggleView}>Cancel</ButtonComp>
                    </div>
                    <div className="col-8 px-0 save_btn">
                      <ButtonComp onClick={saveNewLocation}>Save and Continue</ButtonComp>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="row mx-0 py-4 justify-content-center tabContent">
          <div className="col-12">
            <h6 className="heading">Shipping Settings</h6>
            <ul className="list-unstyled d-flex flex-wrap align-items-center justify-content-between cardSec">
              {state.shippingAddresses.length > 0
                ? state.shippingAddresses.map((cardData, index) => (
                    <li key={index} onClick={() => setActiveCard(index)}>
                      <div className={state.activeCard == index ? "savedCard activeCard" : "savedCard"}>
                        {state.activeCard == index ? (
                          <img src={Selected_Checkbox_Green_Icon} className="selectedCardIcon"></img>
                        ) : (
                          <div className="iconSec">
                            <img src={Edit_Icon} className="editIcon"></img>
                            <img src={Close_Light_Grey_Icon} className="closeIcon" onClick={() => {}}></img>
                          </div>
                        )}

                        <p className="cardName">{cardData.street1}</p>
                        <p className="expiryInfo mb-0">
                          {cardData.city}, {cardData.country}
                        </p>
                      </div>
                    </li>
                  ))
                : ""}
              <li>
                <div className="AddCard" onClick={toggleView}>
                  <div className="d-flex align-items-center">
                    <img src={Add_Card_Green_Icon} className="addIcon"></img>
                    <p className="addpayment">Add Shipping Address</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      )}
      <Snackbar variant={state.variant} message={state.usermessage} open={state.open} onClose={handleSnackbarClose} />
      <style jsx>
        {`
          .checkbox-section {
            padding-top: 1.0416vw;
          }
          :global(.checkbox-container span:nth-child(1)) {
            padding: 0px !important;
          }
          :global(.checkbox-text) {
            font-size: 0.7291vw;
            padding-left: 0.46875vw !important;
            font-weight: 400;
            font-family: "Open Sans";
            color: #c0c7d9;
          }
          :global(.save_btn button) {
            width: 100%;
            margin: 1.098vw 0 0 0;
            padding: 0.366vw 0;
            text-transform: initial;
            background: ${GREEN_COLOR};
            color: ${BG_LightGREY_COLOR};
            font-size: 1.024vw;
            font-family: "Museo-Sans" !important;
            position: relative;
          }
          :global(.save_btn button span) {
            font-family: "Museo-Sans" !important;
            font-size: 1.024vw;
          }
          :global(.save_btn button:hover) {
            background: ${GREEN_COLOR};
          }
          :global(.save_btn button:focus),
          :global(.save_btn button:active) {
            background: ${GREEN_COLOR};
            outline: none;
          }
          :global(.cancel_btn button) {
            width: 100%;
            margin: 1.098vw 0 0 0;
            padding: 0.366vw 0;
            text-transform: initial;
            background: ${Border_LightGREY_COLOR};
            color: ${GREY_VARIANT_1};
            font-size: 1.024vw;
            font-family: "Museo-Sans" !important;
            position: relative;
          }
          :global(.cancel_btn button span) {
            font-family: "Museo-Sans" !important;
            font-size: 1.024vw;
          }
          :global(.cancel_btn button:hover) {
            background: ${Border_LightGREY_COLOR};
          }
          :global(.cancel_btn button:focus),
          :global(.cancel_btn button:active) {
            background: ${Border_LightGREY_COLOR};
            outline: none;
          }
          .left-input {
            padding-right: 1.60256vw !important;
          }
          .right-input {
            padding-left: 1.60256vw !important;
          }
          .FormInput {
            padding-top: 0.732vw;
            position: relative;
          }
          .FormInput label {
            font-size: 0.805vw;
            color: ${FONTGREY_COLOR} !important;
            font-family: "Open Sans-SemiBold" !important;
            margin-bottom: 0.292vw;
          }
          .FormInput label:first-letter {
            text-transform: capitalize;
          }
          :global(.FormInput .inputBox) {
            width: 100%;
            height: 2.196vw;
            padding: 0 0.732vw;
            color: ${FONTGREY_COLOR};
            background-color: #fff;
            background-clip: padding-box;
            border: 0.0732vw solid ${GREY_VARIANT_3};
            border-radius: 0.292vw;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            font-size: 0.805vw;
            letter-spacing: 0.0366vw;
            font-family: "Open Sans" !important;
          }
          :global(.FormInput .inputBox:focus) {
            color: ${FONTGREY_COLOR};
            background-color: #fff;
            border-color: ${THEME_COLOR};
            outline: 0;
            font-size: 0.805vw;
            box-shadow: none;
          }
          :global(.inputBox) {
            // width: 94%;
          }
          :global(.inputBox::placeholder) {
            font-size: 0.805vw;
            font-family: "Open Sans" !important;
            color: ${GREY_VARIANT_2};
          }
          .cardSec li {
            width: 48%;
            margin-top: 1.683vw;
          }
          .cardSec {
            border-top: 0.0732vw solid ${GREY_VARIANT_3};
          }
          .AddCard {
            width: 100%;
            height: 7.247vw;
            display: flex;
            cursor: pointer;
            align-items: center;
            justify-content: center;
            border: 0.0732vw dashed ${GREY_VARIANT_3};
          }
          .savedCard {
            width: 100%;
            height: auto;
            border-radius: 0.219vw;
            padding: 1.244vw;
            position: relative;
            cursor: pointer;
            border: 0.0732vw solid ${GREY_VARIANT_3};
          }
          .activeCard {
            border: 0.0732vw solid ${GREEN_COLOR} !important;
          }
          .tabContent {
            background: ${WHITE_COLOR};
            position: relative;
          }
          .heading {
            font-size: 1.024vw;
            color: ${FONTGREY_COLOR};
            font-family: "Museo-Sans" !important;
            font-weight: 600;
            letter-spacing: 0.0219vw !important;
            margin-bottom: 0.732vw;
          }
          .heading:first-letter {
            text-transform: capitalize;
          }
          .cardLogo {
            width: 7%;
            object-fit: contain;
          }
          .cardName {
            font-size: 1.024vw;
            color: ${FONTGREY_COLOR};
            font-family: "Museo-Sans" !important;
            font-weight: 600;
            margin-bottom: 0;
            margin-left: 0.366vw;
          }
          .expiryInfo {
            font-size: 0.732vw;
            color: ${GREY_VARIANT_2};
            font-family: "Open Sans" !important;
            letter-spacing: 0.0219vw !important;
            margin-left: 0.366vw;
            text-transform: capitalize;
          }
          .cardNumber {
            font-size: 0.878vw;
            color: ${FONTGREY_COLOR};
            font-family: "Open Sans" !important;
            letter-spacing: 0.0219vw !important;
            margin: 0;
            padding: 0.366vw 0 0 0;
            text-transform: capitalize;
          }
          .invalidCard {
            font-size: 0.658vw;
            color: ${Red_Color_1};
            font-family: "Open Sans-Semibold" !important;
            margin: 0;
            padding: 0.146vw 0 0 0;
          }
          .invalidCard:first-letter {
            text-transform: capitalize;
          }
          .selectedCardIcon {
            width: 0.951vw;
            position: absolute;
            top: 0.732vw;
            right: 1.098vw;
          }
          .iconSec {
            position: absolute;
            top: 0.366vw;
            right: 2.40384vw;
          }
          .editIcon {
            width: 0.658vw;
          }
          .closeIcon {
            width: 0.658vw;
            margin-left: 0.585vw;
            top: 0.35vw;
            position: absolute;
          }
          .addpayment {
            font-size: 0.805vw;
            color: ${GREEN_COLOR};
            font-family: "Open Sans-Semibold" !important;
            margin: 0;
            padding: 0.146vw 0 0 0;
          }
          .addIcon {
            width: 0.878vw;
            margin-top: 0.219vw;
            margin-right: 0.366vw;
          }
          .warningIcon {
            width: 0.732vw;
            margin-right: 0.366vw;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default ShippingSettings;
