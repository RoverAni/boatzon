import React from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";

const stripePromise = loadStripe("pk_test_5oeXg9WPuhPBeDrgCilIOfPc000s92lqFQ");

const App = () => {
  return (
    <Elements stripe={stripePromise}>
      <button>Hi</button>
    </Elements>
  );
};

export default App;
