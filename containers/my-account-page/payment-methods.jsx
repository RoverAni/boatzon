import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  Mastercard_Icon,
  GREY_VARIANT_2,
  Red_Color_1,
  GREEN_COLOR,
  Add_Card_Green_Icon,
  Selected_Checkbox_Green_Icon,
  Edit_Icon,
  Close_Light_Grey_Icon,
} from "../../lib/config";
import AddPaymentMethod from "./add-payment-method";
import { getSetupIntent, getCustomer, deleteCard } from "../../services/transaction";
import { getCookie } from "../../lib/session";

class PaymentMethods extends Component {
  state = {
    activeCard: 0,
    stripe: "",
    getCustomerCards: [],
  };

  // Function for store selectedAddress data
  handleSelectCard = (data) => {
    this.setState({
      activeCard: data,
    });
  };

  componentDidMount() {
    getSetupIntent()
      .then((res) => {
        console.log("res getSetupIntent", res);
      })
      .catch((err) => console.log("err getSetupIntent", err));
    getCustomer()
      .then((res) => {
        this.setState({ getCustomerCards: res.data.data });
        console.log("res getCustomer", res);
      })
      .catch((err) => console.log("err getCustomer", err));
  }

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleAddPaymentMethod = () => {
    this.updateScreen(<AddPaymentMethod updateScreen={this.updateScreen} />);
  };

  _deleteCard = (id) => {
    let cards = [...this.state.getCustomerCards];
    let index = cards.findIndex((k) => k.id === id);
    if (index > -1) cards.splice(index, 1);
    deleteCard(id)
      .then((res) => {
        console.log("succ deleting card", res);
        this.setState({ getCustomerCards: cards });
      })
      .catch((err) => console.log("err deleting card", err));
  };

  render() {
    const { activeCard, getCustomerCards } = this.state;
    const { _deleteCard } = this;
    return (
      <Wrapper>
        {!this.state.currentScreen ? (
          <div className="row mx-0 py-4 justify-content-center tabContent">
            <div className="col-12">
              <h6 className="heading">Payment Methods</h6>
              <ul className="list-unstyled d-flex flex-wrap align-items-center justify-content-between cardSec">
                {/* {SavedCardData &&
                  SavedCardData.map((cardData, index) => (
                    <li key={index} onClick={this.handleSelectCard.bind(this, cardData)}>
                      <div className={activeCard == index ? "savedCard activeCard" : "savedCard"}>
                        {activeCard == index ? (
                          <img src={Selected_Checkbox_Green_Icon} className="selectedCardIcon"></img>
                        ) : (
                          <div className="iconSec">
                            <img src={Edit_Icon} className="editIcon"></img>
                            <img src={Close_Light_Grey_Icon} className="closeIcon"></img>
                          </div>
                        )}

                        <div className="d-flex align-items-center">
                          <img src={cardData.cardLogo} className="cardLogo"></img>
                          <p className="cardName">{cardData.cardName}</p>
                          <span className="expiryInfo">{cardData.expiryData}</span>
                        </div>
                        <p className="cardNumber">
                          {cardData.cardName == "ACH" ? "Account Number:" : ""} {cardData.cardNumber}
                        </p>
                        {cardData.cardValid ? (
                          ""
                        ) : (
                          <div className="d-flex align-items-center pt-1">
                            <img src={Warning_Icon} className="warningIcon"></img>
                            <p className="invalidCard">Incomplete card information</p>
                          </div>
                        )}
                      </div>
                    </li>
                  ))} */}
                {getCustomerCards.length > 0
                  ? getCustomerCards.map((cardData, index) => (
                      <li key={index} onClick={this.handleSelectCard.bind(this, index)}>
                        <div className={activeCard == index ? "savedCard activeCard" : "savedCard"}>
                          {activeCard == index ? (
                            <img src={Selected_Checkbox_Green_Icon} className="selectedCardIcon"></img>
                          ) : (
                            <div className="iconSec">
                              <img src={Edit_Icon} className="editIcon"></img>
                              <img src={Close_Light_Grey_Icon} className="closeIcon" onClick={() => _deleteCard(cardData.id)}></img>
                            </div>
                          )}

                          <div className="d-flex align-items-center">
                            <img src={Mastercard_Icon} className="cardLogo"></img>
                            <p className="cardName">{cardData.brand}</p>
                            <span className="expiryInfo">
                              {cardData.exp_month} {cardData.exp_year}
                            </span>
                          </div>
                          <p className="cardNumber">
                            **** **** **** {cardData.last4}
                            {/* {cardData.cardName == "ACH" ? "Account Number:" : ""} {cardData.cardNumber} */}
                          </p>
                          {/* {cardData.cardValid ? (
                            ""
                          ) : (
                            <div className="d-flex align-items-center pt-1">
                              <img src={Warning_Icon} className="warningIcon"></img>
                              <p className="invalidCard">Incomplete card information</p>
                            </div>
                          )} */}
                        </div>
                      </li>
                    ))
                  : ""}
                <li>
                  <div className="AddCard" onClick={this.handleAddPaymentMethod}>
                    <div className="d-flex align-items-center">
                      <img src={Add_Card_Green_Icon} className="addIcon"></img>
                      <p className="addpayment">Add payment method</p>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        ) : (
          this.state.currentScreen
        )}
        <style jsx>
          {`
            .cardSec li {
              width: 48%;
              margin-top: 1.683vw;
            }
            .cardSec {
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .AddCard {
              width: 100%;
              height: 7.247vw;
              display: flex;
              cursor: pointer;
              align-items: center;
              justify-content: center;
              border: 0.0732vw dashed ${GREY_VARIANT_3};
            }
            .savedCard {
              width: 100%;
              height: auto;
              border-radius: 0.219vw;
              padding: 1.244vw;
              position: relative;
              cursor: pointer;
              border: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .activeCard {
              border: 0.0732vw solid ${GREEN_COLOR} !important;
            }
            .tabContent {
              background: ${WHITE_COLOR};
              position: relative;
            }
            .heading {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.732vw;
            }
            .heading:first-letter {
              text-transform: capitalize;
            }
            .cardLogo {
              width: 7%;
              object-fit: contain;
            }
            .cardName {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0;
              margin-left: 0.366vw;
            }
            .expiryInfo {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-left: 0.366vw;
              text-transform: capitalize;
            }
            .cardNumber {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0.366vw 0 0 0;
              text-transform: capitalize;
            }
            .invalidCard {
              font-size: 0.658vw;
              color: ${Red_Color_1};
              font-family: "Open Sans-Semibold" !important;
              margin: 0;
              padding: 0.146vw 0 0 0;
            }
            .invalidCard:first-letter {
              text-transform: capitalize;
            }
            .selectedCardIcon {
              width: 0.951vw;
              position: absolute;
              top: 0.732vw;
              right: 1.098vw;
            }
            .iconSec {
              position: absolute;
              top: 0.366vw;
              right: 1.098vw;
            }
            .editIcon {
              width: 0.658vw;
            }
            .closeIcon {
              width: 0.658vw;
              margin-left: 0.585vw;
            }
            .addpayment {
              font-size: 0.805vw;
              color: ${GREEN_COLOR};
              font-family: "Open Sans-Semibold" !important;
              margin: 0;
              padding: 0.146vw 0 0 0;
            }
            .addIcon {
              width: 0.878vw;
              margin-top: 0.219vw;
              margin-right: 0.366vw;
            }
            .warningIcon {
              width: 0.732vw;
              margin-right: 0.366vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default PaymentMethods;
