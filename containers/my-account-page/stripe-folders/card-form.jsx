import React, { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import _JSXStyle from "styled-jsx/style";
import Wrapper from "../../../hoc/Wrapper";
import { THEME_COLOR } from "../../../lib/config";
import CircularProgress from "@material-ui/core/CircularProgress";
import { addCardForCustomer } from "../../../services/transaction";
import { getCookie } from "../../../lib/session";
import PaymentMethods from "../payment-methods";

const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: THEME_COLOR,
      fontFamily: "Museo-Sans-Cyrl",
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "#aab7c4",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
};

export const CardForm = (props) => {
  const stripe = useStripe();
  const elements = useElements();
  const [loader, handleloader] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!stripe || !elements) {
      return;
    }

    const cardElement = elements.getElement(CardElement);
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
    });

    if (error) {
    } else {
      handleloader(true);
      addCardForCustomer({
        userId: getCookie("mqttId"),
        paymentMethod: paymentMethod.id,
      })
        .then((res) => {
          console.log("card successfully addded to boatzon", res);
          handleloader(false);
          props.updateScreen(<PaymentMethods updateScreen={props.updateScreen} />);
        })
        .catch((err) => console.log("card failed to add in boatzon", err));
    }
  };

  return (
    <Wrapper>
      <form onSubmit={handleSubmit}>
        <CardElement options={CARD_ELEMENT_OPTIONS} />
        {loader ? (
          <div className="loading_card">
            <CircularProgress />
          </div>
        ) : (
          <button className="add-card-btn">Add Card</button>
        )}
      </form>

      <style jsx>
        {`
          .add-card-btn {
            margin: 30px auto;
            display: block;
            background: ${THEME_COLOR};
            border: none;
            color: #fff;
            padding: 7px 20px;
            border-radius: 4px;
            font-size: 14px;
            width: 100%;
          }

          :global (.loading_card) {
            height: 85vh;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          .loading_card {
            height: 85vh;
            display: flex;
            justify-content: center;
            align-items: center;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default CardForm;
