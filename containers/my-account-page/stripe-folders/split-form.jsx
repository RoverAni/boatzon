import { loadStripe } from "@stripe/stripe-js";
import React, { useState } from "react";
import {
  Elements,
  useStripe,
  useElements,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement
} from "@stripe/react-stripe-js";
import { addCardForCustomer } from "../../../services/transaction";
import PaymentMethods from "../payment-methods";
import ButtonComp from "../../../components/button/button";
import { getCookie } from '../../../lib/session'
import {
  GREY_VARIANT_1,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR,
} from "../../../lib/config";

const createOptions = () => {
  return {
    base: {
      fontSize: '14px',
      color: '#D3D9E7',
      fontFamily: "Open Sans, sans-serif",
      letterSpacing: '0.025em',
      '::placeholder': {
        color: '#D3D9E7',
      },
    },
    // invalid: {
    //   color: '#c23d4b',
    // },
  }
};

const CheckoutForm = ({ addCard, updateScreen }) => {

  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async event => {
    event.preventDefault();
    const res = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardNumberElement),
      card: elements.getElement(CardExpiryElement),
      card: elements.getElement(CardCvcElement),
    });
    addCard(res.paymentMethod.id)
  };

  return (
    <div>
      <form
        onSubmit={handleSubmit}
        className="payment-form"
      >
        <label className="input-header-zip w-100">
          <div>Name on card</div>
          <input name="name" type="text" placeholder="First and Last name" className="common-input-style-btm" required />
        </label>
        <div className="split-form1">
          <label className="input-header w-100">
            Card Number
            <CardNumberElement options={{ style: createOptions(), placeholder: "XXXX-XXXX-XXXX-XXXX" }} className="common-input-style-btm" />
          </label>
        </div>

        <div className="split-form2">
          <label className="input-header">
            Expire date
            <CardExpiryElement options={{ style: createOptions(), placeholder: "MM/YYYY" }} className="common-input-style-btm" />
          </label>
          <label className="input-header">
            CVV
            <CardCvcElement options={{ style: createOptions(), placeholder: "123" }} className="common-input-style-btm" />
          </label>
          <label className="input-header-zip">
            <div>Zip code</div>
            <input name="name" type="text" placeholder="12345" className="zipInput" required />
          </label>
        </div>
        <div className="col-12 p-0">
          <div className="row m-0">
            <div className="col-6 p-0">
              <div className="cancel_btn">
                <ButtonComp onClick={() => updateScreen(<PaymentMethods updateScreen={updateScreen} />)}>Cancel</ButtonComp>
              </div>
            </div>
            <div className="col-6 pr-0">
              <div className="save_btn">
                <ButtonComp onClick={handleSubmit}>
                  Save and Continue
                </ButtonComp>
              </div>
            </div>
          </div>
        </div>

      </form>
      <style jsx>
        {`
            :global(.common-input-style-btm) {
              border: 2px solid #e9ecf3;
              padding: 0.52083vw 0.8333vw;
              border-radius: 4px;
              margin-top: 0.52083vw;
              margin-bottom: 0.52083vw;
              font-size: 0.72916vw;
              font-family: Open Sans !important;
            }
            :global(.zipInput) {
              border: 2px solid #e9ecf3;
              padding: 0.42708vw 0.8333vw;
              border-radius: 4px;
              margin-top: 0.52083vw;
              margin-bottom: 0.52083vw;
              font-size: 0.72916vw;
              font-family: Open Sans !important;
              color: #D3D9E7  !important;
            }
            :global(.zipInput::placeholder) {
              font-weight: 100 !important;
              font-size: 0.72916vw;
              font-family: Open Sans !important;
              color: #d3d9e7 !important;
            }
            :global(.zipInput:focus) {
              outline-width: 0;
              outline: none;
            }
            :global(.common-input-style-btm::placeholder) {
              font-weight: 100 !important;
              font-size: 0.72916vw;
              font-family: Open Sans !important;
              color: #d3d9e7 !important;
            }
            :global(.common-input-style-btm:focus) {
              outline-width: 0;
              outline: none;
            }
            :global(.payment-form) {
              border: 1px solid #d3d9e7;
              padding: 1.5625vw;
              margin-top: 1.77083vw;
              border-style: dotted;
              border-width: 2px;
            }
            :global(.split-form2) {
              width: 100%;
              display: flex;
              justify-content: space-between;
            }
            :global(.split-form2 .input-header) {
              width: 30%;
            }
            :global(.input-header-zip) {
              width: 30%;
              flex-direction: column;
              color: #4b5777;
              font-size: 0.72916vw;
              font-family: Open Sans !important;
              font-weight: 600;
            }
            :global(.input-header-zip input) {
              width: 100%;
              // margin-top: 0.52083vw;
            }
            :global(.input-header) {
              color: #4b5777;
              font-size: 0.72916vw;
              font-family: Open Sans !important;
              font-weight: 600;
            }
            :global(.cancel_btn button) {
              width: 100%;
              padding: 0.585vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
            }
            :global(.cancel_btn button span) {
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_1};
              font-size: 0.805vw;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.save_btn button) {
              width: 100%;
              padding: 0.585vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
            }
            :global(.save_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${BG_LightGREY_COLOR};
              font-size: 0.805vw;
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
      </style>
    </div >

  );
};

const stripePromise = loadStripe(
  "pk_test_5oeXg9WPuhPBeDrgCilIOfPc000s92lqFQ"
);

const Checkout = ({ updateScreen }) => {

  const [loader, handleloader] = useState(false);

  const addCard = async (id) => {
    handleloader(true)
    let res = await addCardForCustomer({
      userId: getCookie("mqttId"),
      paymentMethod: id,
    });
    if (res) {
      console.log("card successfully addded to boatzon", res);
      handleloader(false);
      updateScreen(<PaymentMethods updateScreen={updateScreen} />);
    }
  }

  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm
        updateScreen={updateScreen}
        addCard={addCard}
      />
    </Elements>
  );
};

export default Checkout;
