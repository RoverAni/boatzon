import React, { Component } from "react";
import { connect } from "react-redux";
import Wrapper from "../../hoc/Wrapper";
import {
  DummyProfilePic_Icon,
  THEME_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR_Opacity,
  GREY_VARIANT_3,
  Verified_Icon,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  Facebook_Icon,
  Facebook_Blue,
  WHITE_COLOR,
  GREEN_COLOR,
  NotVerified_Icon,
  IMAGE_UPLOAD_SIZE,
  FB_APP_ID,
  Facebook_Login_Type,
  Camera_Icon,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import {
  PureTextValidator,
  requiredValidator,
  validatePhoneNumber,
} from "../../lib/validation/validation";
import InputBox from "../../components/input-box/input-box";
import {
  EMAIL,
  NAME,
  MOBILE,
  COUNTRY_CODE,
} from "../../lib/input-control-data/input-definitions";
import LocationSearchInput from "../location/location-search-box";
// International-Tel-Input from npm package
import IntlTelInput from "react-intl-tel-input";
import ButtonComp from "../../components/button/button";
import SwitchInput from "../../components/input-box/switch-input";
import {
  newEmailCheck,
  saveEditProfileData,
  newPhoneNumber,
  newPhoneNumberVerifyOtp,
} from "../../services/userProfile";
import Snackbar from "../../components/snackbar";
import Model from "../../components/model/model";
import VerifyOtp from "./verify-otp";
import { forgotPassword, userLogin } from "../../services/auth";
import FacebookLogin from "react-facebook-login";
import { handlePreferredCountries } from "../../lib/phone-num/phone-num";

class AccSettings extends Component {
  state = {
    inputpayload: {
      preferredCountries: ["in"],
    },
    isnewsletter: true,
    isFormValid: false,
    EmailVerified: false,
    PhoneVerified: true,
    isPhoneValid: true,
    userProfileData: {},
  };

  componentDidMount() {
    console.log(
      "userProfileData--componentDidMount",
      this.props.userProfileData
    );
    this.setState(
      {
        userProfileData: this.props.userProfileData,
      },
      () => {
        this.setState(
          {
            Name: this.state.userProfileData.fullName,
            Email: this.state.userProfileData.email,
            mobile: this.state.userProfileData.phoneNumber.replace(
              this.state.userProfileData.countryCode,
              ""
            ),
            countryCode: this.state.userProfileData.countryCode,
            preferredCountries: handlePreferredCountries(
              this.state.userProfileData.countryCode,
              this.state.userProfileData.phoneNumber.replace(
                this.state.userProfileData.countryCode,
                ""
              )
            ),
            profilePicUrl: this.state.userProfileData.profilePicUrl,
            EmailVerified: this.state.userProfileData.emailVerified,
            address: this.state.userProfileData.location,
            longitude: this.state.userProfileData.longitude,
            latitude: this.state.userProfileData.latitude,
          },
          () => {
            let tempPayload = { ...this.state.inputpayload };
            tempPayload[`ProductImg`] = this.state.profilePicUrl;
            tempPayload[`Name`] = this.state.Name;
            tempPayload[`Email`] = this.state.Email;
            tempPayload[`mobile`] = this.state.mobile;
            tempPayload[`countryCode`] = this.state.countryCode;
            tempPayload[`address`] = this.state.address;
            tempPayload[`preferredCountries`] = this.state.preferredCountries;
            this.setState(
              {
                inputpayload: { ...tempPayload },
              },
              () => {
                console.log("PAYLOAD", this.state.inputpayload);
              }
            );
          }
        );
      }
    );
    this.validatePhoneInput();
  }

  // Number Validation function for phoneInput
  validatePhoneInput = () => {
    validatePhoneNumber("phoneNum");
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state.Name && this.state[EMAIL] ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [inputControl.name]: validate }, () => {
      this.checkIfFormValid();
    });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
      [inputControl.name]: inputControl.value,
    });
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log("valuenumber", dialInfo);
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
      },
      () => {
        let phoneNum = this.state.userProfileData.phoneNumber.startsWith("+")
          ? this.state.userProfileData.phoneNumber.replace(
              this.state.userProfileData.countryCode,
              ""
            )
          : this.state.userProfileData.phoneNumber;
        if (phoneNum != this.state.UserNumber) {
          if (this.state.isPhoneValid) {
            this.setState({
              PhoneVerified: false,
            });
          }
        }
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        tempPayload[`preferredCountries`] = this.state.preferredCountries;
        this.setState({
          inputpayload: { ...tempPayload },
          [`${MOBILE}`]: this.state.UserNumber,
          [`${COUNTRY_CODE}`]: "+" + this.state.cCode,
        });
      }
    );
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    console.log("onSelectFlag", num, country.dialCode, fullNum, status);
    this.handleOnchangePhone(status, num, country);
  };

  // used to store the switch checked status
  handleSwitchChange = (name) => (event) => {
    let inputControl = event;
    this.setState({ [name]: inputControl.target.checked }, () => {
      let tempPayload = this.state.inputpayload;
      tempPayload[[name]] = this.state[name];
      this.setState(
        {
          inputpayload: tempPayload,
        },
        () => {
          console.log("inputpayload", this.state.inputpayload);
        }
      );
    });
  };

  updateUserAddress = (data) => {
    console.log("nvdkvn", data);
    if (data) {
      this.setState(
        {
          latitude: data.lat,
          longitude: data.lng,
          address: data.address,
        },
        () => {
          console.log("nvdkvn", this.state.latitude, this.state.longitude);
        }
      );
    }
  };

  // Function to handle snackbar variant color
  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 404:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleResponseMsg = (response) => {
    switch (response.code) {
      case 200:
        return "Successfully Updated the Profile";
        break;
      default:
        response.message;
    }
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function to send Email Varification link
  handleNewEmailCheck = () => {
    let payload = {
      email: this.state.Email,
    };
    newEmailCheck(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // used to upload image from local storage
  handleUploadPic = async (event) => {
    let files = event.target.files;
    console.log("njdf", files);
    let file_type = files[0].type.split("/")[0];
    console.log("nfdj", file_type);
    if (
      files &&
      files[0] &&
      (files[0].type == "image/jpeg" || files[0].type == "image/png")
    ) {
      const data = new FormData();
      data.append("file", files[0]);
      data.append("upload_preset", "avzsfq8q");
      const res = await fetch(
        `https://api.cloudinary.com/v1_1/boatzon/${file_type}/upload`,
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      try {
        console.log("SuccessRepsonce", file);
        if (file) {
          let ProductImg = file.secure_url;
          if (this.isFileSizeValid(files[0])) {
            this.setState(
              {
                productpic: true,
                fileErr: null,
                isDocValid: true,
              },
              () => {
                let tempInputPayload = { ...this.state.inputpayload };
                tempInputPayload.productImgs = ProductImg;
                this.setState(
                  {
                    ProductImg,
                    profilePicUrl: ProductImg,
                    inputpayload: { ...tempInputPayload },
                  },
                  () => {
                    this.checkIfFormValid();
                    console.log("PAYLOAD", this.state.inputpayload);
                  }
                );
              }
            );
          } else {
            this.setState({
              fileErr:
                files[0].type == "video"
                  ? "File size should be less than 4MB!"
                  : "File size should be less than 2MB!",
              isDocValid: false,
            });
          }
        }
      } catch (err) {
        console.log("imager url error", err);
        this.setState({
          fileErr: "imager url error",
          isDocValid: false,
          productpic: false,
        });
      }
    } else {
      this.setState({
        fileErr:
          "File format is invalid! Image should be of .jpeg or .png format",
        isDocValid: false,
      });
    }
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    console.log("Validation", fileData);
    switch (fileData.type.split("/")[0]) {
      case "video":
        return fileData.size <= VIDEO_UPLOAD_SIZE;
        break;
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };

  handleChangePassword = () => {
    let payload = {
      type: "0", // type --> 0-email,1-phoneNumber
      email: this.state.Email,
    };
    forgotPassword(payload)
      .then((res) => {
        console.log("res---->", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      })
      .catch((err) => {
        console.log("err--->", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handlePhoneNumberVerify = () => {
    let payload = {
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };
    console.log("newPhoneNum Payload", payload);
    newPhoneNumber(payload)
      .then((res) => {
        console.log("phoneRes", res.data);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message.toString(),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.setState({
              otpModel: true,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleCloseOtpModel = () => {
    this.setState({
      otpModel: false,
    });
  };

  handleVerifyOtp = (otp) => {
    let payload = {
      otp: otp,
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
      countryCode: this.state.inputpayload.countryCode,
    };

    newPhoneNumberVerifyOtp(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response.code == 200) {
          this.setState(
            {
              PhoneVerified: true,
            },
            () => {
              this.handleCloseOtpModel();
            }
          );
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  responseFacebookLogin = (response) => {
    console.log("FacebookResponse", response);
    let facebookResponse = response;

    let facebookLoginPayload = {
      loginType: Facebook_Login_Type,
      facebookId: facebookResponse.userID,
      longitude: this.props.currentLocation.longitude,
      latitude: this.props.currentLocation.latitude,
      city: this.props.currentLocation.city,
      countrySname: this.props.currentLocation.country,
      location: this.props.currentLocation.address,
    };
    userLogin(facebookLoginPayload)
      .then((res) => {
        console.log("res", res.data);
        let response = res.data;
        this.setState({
          usermessage: response.message,
          variant: this.handleSnackbar(response),
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
        if (response.code == 200) {
        }
        if (response.code == 204) {
        }
      })
      .catch((err) => {
        console.log("loginerr", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleSaveChanges = () => {
    let payload = {
      fullName: this.state.Name,
      profilePicUrl: this.state.profilePicUrl,
      thumbnailImageUrl: this.state.profilePicUrl,
      location: this.state.address,
      latitude: this.state.latitude.toString(),
      longitude: this.state.longitude.toString(),
    };
    saveEditProfileData(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: this.handleResponseMsg(response),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            // window.location.reload();
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  render() {
    const { isnewsletter } = this.state;
    console.log("userProfileData--render", this.props.userProfileData);
    return (
      <Wrapper>
        <div className="row mx-0 py-4 justify-content-center tabContent">
          <div className="col-md-6 col-lg-6 col-xl-6 AccSettings_Sec">
            <div className="text-center">
              <label for="profileRedImage" className="uploadSec">
                <div className="position-relative">
                  {this.state.productpic && this.state.ProductImg ? (
                    <div className="ProfileImgSec">
                      <div className="content-overlay">
                        <img src={Camera_Icon} className="cameraIcon"></img>
                      </div>
                      <img
                        src={this.state.ProductImg}
                        className="newProfileImg"
                      ></img>
                    </div>
                  ) : (
                    <div className="dummyPicSec">
                      {this.state.profilePicUrl ? (
                        <div className="overlay">
                          <img src={Camera_Icon} className="cameraIcon"></img>
                        </div>
                      ) : (
                        ""
                      )}
                      <img
                        src={
                          this.state.profilePicUrl
                            ? this.state.profilePicUrl
                            : DummyProfilePic_Icon
                        }
                        className="dummyPic"
                      ></img>
                    </div>
                  )}
                </div>
                <input
                  id="profileRedImage"
                  type="file"
                  style={{ display: "none" }}
                  onChange={this.handleUploadPic}
                  accept="image/*,application/pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                />
              </label>
            </div>
            <div className="FormInput">
              <label>Name</label>
              <InputBox
                type="text"
                className="inputBox form-control"
                name={NAME}
                placeholder="Bryan"
                value={this.state.Name}
                onChange={this.handleOnchangeInput}
                autoComplete="off"
                onKeyPress={PureTextValidator}
              ></InputBox>
            </div>
            <div className="FormInput">
              <label>Email</label>
              <div className="position-relative">
                <InputBox
                  type="email"
                  className="inputBox form-control"
                  name={EMAIL}
                  value={this.state.Email}
                  placeholder="Bryan677@gmail.com"
                  onChange={this.handleOnchangeInput}
                  autoComplete="off"
                ></InputBox>
                {this.state.EmailVerified ? (
                  <img src={Verified_Icon} className="verifiedIcon"></img>
                ) : (
                  <img src={NotVerified_Icon} className="notVerifiedIcon"></img>
                )}
                {this.state.EmailVerified ? (
                  " "
                ) : (
                  <p className="verify_btn" onClick={this.handleNewEmailCheck}>
                    Verify
                  </p>
                )}
              </div>

              {this.state[EMAIL] == 0 || this.state[EMAIL] == 2 ? (
                <p className="errMessage">Enter Valid Email</p>
              ) : (
                ""
              )}
              <p className="emailChangeMsg">
                Changing your email will require re-verefication.
              </p>
            </div>
            {/* Address Module */}
            <div className="FormInput">
              <label>Address</label>
              <LocationSearchInput
                id="user-address-box"
                address={this.state.address}
                updateLocation={this.updateUserAddress}
                handleChange={this.updateUserAddress}
                showOnlyAddress={true}
                showAddressFromProps={true}
              ></LocationSearchInput>
            </div>
            <div className="FormInput">
              <label>Phone number</label>
              <div className="PhoneInput">
                <IntlTelInput
                  key={this.state.preferredCountries}
                  preferredCountries={this.state.preferredCountries}
                  containerClassName="intl-tel-input"
                  onSelectFlag={this.handleOnchangeflag}
                  onPhoneNumberChange={this.handleOnchangePhone}
                  formatOnInit={false}
                  separateDialCode={true}
                  value={this.state.mobile}
                  fieldId="phoneNum"
                  autoComplete="off"
                />
                {this.state.isPhoneValid ? (
                  this.state.PhoneVerified ? (
                    <img src={Verified_Icon} className="verifiedIcon"></img>
                  ) : (
                    <img
                      src={NotVerified_Icon}
                      className="notVerifiedIcon"
                    ></img>
                  )
                ) : (
                  ""
                )}
                {this.state.PhoneVerified ? (
                  " "
                ) : (
                  <p
                    className="verify_btn"
                    onClick={this.handlePhoneNumberVerify}
                  >
                    Verify
                  </p>
                )}
              </div>
            </div>
            <div className="FormInput borderBottom">
              <label>Password</label>
              <div className="changePass_btn">
                <ButtonComp onClick={this.handleChangePassword}>
                  Change Password
                </ButtonComp>
              </div>
            </div>
            <div className="connectFb_Sec">
              <div className="FacebookLogin_Btn">
                <img src={Facebook_Icon} className="facebookIcon"></img>
                <FacebookLogin
                  appId={FB_APP_ID}
                  autoLoad={false}
                  textButton={`Connect Facebook Account`}
                  fields="name,email,picture"
                  callback={this.responseFacebookLogin}
                />
              </div>
            </div>

            <div className="toggleInput">
              <SwitchInput
                triggerOnChange={true}
                newsletter={true}
                label="newsletter"
                onChange={this.handleSwitchChange("isnewsletter")}
                value={isnewsletter}
                checked={isnewsletter}
                name="isnewsletter"
              />
            </div>
            <div className="save_btn">
              <ButtonComp onClick={this.handleSaveChanges}>
                Save Changes
              </ButtonComp>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        {/* Login Model */}
        <Model
          open={this.state.otpModel}
          onClose={this.handleCloseOtpModel}
          authmodals={true}
        >
          <VerifyOtp
            onClose={this.handleCloseOtpModel}
            handleVerifyOtp={this.handleVerifyOtp}
            inputpayload={this.state.inputpayload}
          />
        </Model>
        <style jsx>
          {`
            .dummyPicSec {
              width: 6.222vw;
              height: 6.222vw;
              cursor: pointer;
              border-radius: 50% !important;
              position: relative;
              margin: 0 auto;
            }
            .ProfileImgSec {
              width: 6.222vw;
              height: 6.222vw;
              position: relative;
              margin: 0 auto;
              border-radius: 50% !important;
            }

            .content-overlay {
              background: rgba(255, 255, 255, 0.8);
              position: absolute;
              width: 100%;
              left: 0;
              top: 65%;
              bottom: 0;
              cursor: pointer;
              right: 0;
              opacity: 0;
              -webkit-transition: all 0.4s ease-in-out 0s;
              -moz-transition: all 0.4s ease-in-out 0s;
              transition: all 0.4s ease-in-out 0s;
              z-index: 1;
            }
            .overlay {
              background: rgba(255, 255, 255, 0.8);
              position: absolute;
              width: 100%;
              left: 0;
              top: 65%;
              bottom: 0;
              cursor: pointer;
              right: 0;
              opacity: 0;
              -webkit-transition: all 0.4s ease-in-out 0s;
              -moz-transition: all 0.4s ease-in-out 0s;
              transition: all 0.4s ease-in-out 0s;
              z-index: 1;
            }
            .ProfileImgSec:hover .content-overlay {
              opacity: 1;
            }
            .dummyPicSec:hover .overlay {
              opacity: 1;
            }
            .newProfileImg,
            .dummyPicSec .dummyPic {
              width: 100%;
              height: 100%;
              // cursor: pointer;
              object-fit: cover;
              border-radius: 50%;
            }
            :global(.cameraIcon) {
              width: 1.098vw !important;
              object-fit: cover !important;
              margin-bottom: 0.219vw;
              height: unset !important;
              border-radius: none !important;
            }
            .tabContent {
              background: ${WHITE_COLOR};
            }
            .borderBottom {
              border-bottom: 1px solid ${GREY_VARIANT_3} !important;
            }
            .AccSettings_Sec .FormInput {
              padding-top: 0.732vw;
              position: relative;
            }
            .AccSettings_Sec .FormInput label {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin-bottom: 0.292vw;
            }
            .AccSettings_Sec .FormInput label:first-letter {
              text-transform: capitalize;
            }
            :global(.AccSettings_Sec .FormInput .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.AccSettings_Sec .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              font-size: 0.805vw;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.AccSettings_Sec .FormInput .PhoneInput) {
              position: relative;
            }
            :global(.AccSettings_Sec .FormInput .PhoneInput > div) {
              width: 100%;
              display: flex;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.196vw;
              padding: 0 0.366vw;
            }
            :global(.AccSettings_Sec .FormInput .PhoneInput > div > input) {
              padding: 0.366vw 0;
              width: 100%;
              height: auto;
              line-height: 1.5;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border-radius: none;
              border: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              padding-left: 15% !important;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .country-list) {
              width: 100%;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .country-list
                .country) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                > input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }

            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag
                .selected-dial-code) {
              font-size: 0.805vw;
              padding: 0 0.366vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag
                .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag
                .iti-flag) {
              display: none;
            }
            :global(.AccSettings_Sec
                .FormInput
                .PhoneInput
                > div
                div
                .selected-flag
                .iti-arrow) {
              display: none;
            }
            :global(.AccSettings_Sec .FormInput .PhoneInput > div div:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            .errMessage {
              font-size: 0.732vw;
              color: red;
              padding: 0.585vw 0 0 0;
              margin: 0;
            }
            .notVerifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 0.951vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .emailChangeMsg {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              padding: 0.366vw 0 0 0;
              opacity: 0.8;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }

            .verify_btn {
              position: absolute;
              right: -2.928vw;
              top: 50%;
              transform: translate(0, -50%);
              background: transparent;
              width: fit-content;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.732vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              cursor: pointer;
              margin: 0;
            }
            .changePass_btn {
              margin-bottom: 1.098vw;
            }
            :global(.changePass_btn button) {
              width: 100%;
              background: ${Border_LightGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans" !important;
            }
            :global(.changePass_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
            }
            :global(.changePass_btn button:focus),
            :global(.changePass_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.changePass_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .connectFb_Sec {
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
              margin: 1.098vw 0 0 0;
            }
            .FacebookLogin_Btn {
              position: relative;
            }
            :global(.FacebookLogin_Btn button) {
              width: 100%;
              margin: 0;
              padding: 0;
              height: 2.635vw;
              text-transform: initial;
              background: ${Facebook_Blue};
              color: ${WHITE_COLOR};
              position: relative !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0366vw !important;
              font-size: 1.024vw;
              font-weight: 600;
              border-radius: 0.146vw;
            }
            :global(.FacebookLogin_Btn button:hover) {
              background: ${Facebook_Blue};
            }
            :global(.FacebookLogin_Btn button:focus),
            :global(.FacebookLogin_Btn button:active) {
              background: ${Facebook_Blue};
              outline: none;
            }
            .facebookIcon {
              position: absolute;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 1.098vw;
              z-index: 1;
            }
            .toggleInput {
              border-bottom: 1px solid ${GREY_VARIANT_3} !important;
              padding-bottom: 0.219vw;
              margin-bottom: 0.366vw;
            }
            :global(.save_btn button) {
              width: 100%;
              margin: 0.732vw 0 0 0;
              text-transform: initial;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
              position: relative;
            }
            :global(.save_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.save_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.save_btn button:focus),
            :global(.save_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(AccSettings);
