import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  Acc_Settings_Blue,
  Acc_Settings_Grey,
  Payment_Methods_Blue,
  Payment_Methods_Grey,
  Deposit_Account_Blue,
  Deposit_Account_Grey,
  Location_Icon_Blue,
  Loation_Icon_Grey,
} from "../../lib/config";
import { getUrlSlugs } from "../../lib/url/getSlugs";
import Router from "next/router";
import AccSettings from "./acc-settings";
import DepositAccount from "./deposit-account";
import PaymentMethods from "./payment-methods";
import NestedList from "../../components/custom-list/nested-list";
import MessengerSetting from "./messenger-setting";
import Locations from "./locations";
import ShippingSettings from "./shipping-settings";

class index extends Component {
  state = {
    activeLink: "general-settings",
    openProfile: false,
    accSetting: true,
  };

  // On Component load this called
  componentDidMount = () => {
    let slugData = getUrlSlugs();
    this.setState(
      {
        activeLink: slugData[1],
        openProfile: true,
        slugData: slugData,
      },
      () => {
        if (
          this.state.activeLink == "payment-methods" ||
          this.state.activeLink == "deposit-account" ||
          this.state.activeLink == "shipping-settings" ||
          this.state.activeLink == "locations"
        ) {
          this.setState({
            accSetting: !this.state.accSetting,
          });
        }
      }
    );
  };

  // Used to get Current Slug
  handelTabClick = (slug) => {
    Router.push(`/my-account/${slug}`);
  };

  handleToggleAccSettings = () => {
    this.setState({
      accSetting: !this.state.accSetting,
    });
  };

  render() {
    let pageContent = <AccSettings userProfileData={this.props.userProfileData} />;

    switch (this.state.activeLink) {
      case "general-settings":
        pageContent = <AccSettings userProfileData={this.props.userProfileData} />;
        break;
      case "messenger-settings":
        pageContent = <MessengerSetting />;
        break;
      case "payment-methods":
        pageContent = <PaymentMethods />;
        break;
      case "shipping-settings":
        pageContent = <ShippingSettings />;
        break;
      case "deposit-account":
        pageContent = <DepositAccount />;
        break;
      case "locations":
        pageContent = <Locations />;
        break;
      default:
        pageContent = <AccSettings userProfileData={this.props.userProfileData} />;
    }
    const accSettingLabel = (
      <div className="row m-0 align-items-center">
        <div className="col-2 p-0 iconSec">
          {"general-settings" === this.state.activeLink || "messenger-settings" === this.state.activeLink || this.state.accSetting ? (
            <img src={Acc_Settings_Blue} className="accIcon"></img>
          ) : (
            <img src={Acc_Settings_Grey} className="accIcon"></img>
          )}
        </div>
        <div className="col p-0">
          <p
            className={
              "general-settings" === this.state.activeLink || "messenger-settings" === this.state.activeLink || this.state.accSetting
                ? "activeLabel tabLabel"
                : "tabLabel"
            }
          >
            Account settings
          </p>
        </div>
      </div>
    );
    return (
      <Wrapper>
        {this.state.openProfile ? (
          <div className="row m-0 align-items-center ProfilePageSec">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0">
                <div className="row m-0 justify-content-center">
                  <div className="col-md-12 col-lg-11 col-xl-11 my-4 py-3">
                    <div className="row m-0">
                      <div className="col-lg-3 col-md-3 Tabs">
                        <h6 className="heading">My Account</h6>
                        <ul className="list-unstyled AccountTabs">
                          <li className="p-0">
                            <NestedList
                              accSetting={true}
                              name={accSettingLabel}
                              open={this.state.accSetting}
                              onClick={this.handleToggleAccSettings}
                            >
                              <ul className="list-unstyled">
                                <li onClick={this.handelTabClick.bind(this, "general-settings")} className="accList">
                                  <div className="row m-0 align-items-center">
                                    <div className="col-2 p-0 iconSec"></div>
                                    <div className="col p-0">
                                      <p
                                        className={"general-settings" === this.state.activeLink ? "activeLinkLabel linkLabel" : "linkLabel"}
                                      >
                                        General Settings
                                      </p>
                                    </div>
                                  </div>
                                </li>
                                <li className="accList" onClick={this.handelTabClick.bind(this, "messenger-settings")}>
                                  <div className="row m-0 align-items-center">
                                    <div className="col-2 p-0 iconSec"></div>
                                    <div className="col p-0">
                                      <p
                                        className={
                                          "messenger-settings" === this.state.activeLink ? "activeLinkLabel linkLabel" : "linkLabel"
                                        }
                                      >
                                        Messenger Settings
                                      </p>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </NestedList>
                          </li>
                          <li
                            className={"payment-methods" === this.state.activeLink ? "activeLink" : " "}
                            onClick={this.handelTabClick.bind(this, "payment-methods")}
                          >
                            <div className="row m-0 align-items-center">
                              <div className="col-2 p-0 iconSec">
                                {"payment-methods" === this.state.activeLink ? (
                                  <img src={Payment_Methods_Blue} className="paymentIcon"></img>
                                ) : (
                                  <img src={Payment_Methods_Grey} className="paymentIcon"></img>
                                )}{" "}
                              </div>
                              <div className="col p-0">
                                <p className={"payment-methods" === this.state.activeLink ? "activeLabel tabLabel" : "tabLabel"}>
                                  Payment methods
                                </p>
                              </div>
                            </div>
                          </li>

                          <li
                            className={"shipping-settings" === this.state.activeLink ? "activeLink" : " "}
                            onClick={this.handelTabClick.bind(this, "shipping-settings")}
                          >
                            <div className="row m-0 align-items-center">
                              <div className="col-2 p-0 iconSec">
                                {"shipping-settings" === this.state.activeLink ? (
                                  <img src={Deposit_Account_Blue} className="depositIcon"></img>
                                ) : (
                                  <img src={Deposit_Account_Grey} className="depositIcon"></img>
                                )}{" "}
                              </div>
                              <div className="col p-0">
                                <p className={"shipping-settings" === this.state.activeLink ? "activeLabel tabLabel" : "tabLabel"}>
                                  Shipping Settings
                                </p>
                              </div>
                            </div>
                          </li>

                          <li
                            className={"deposit-account" === this.state.activeLink ? "activeLink" : " "}
                            onClick={this.handelTabClick.bind(this, "deposit-account")}
                          >
                            <div className="row m-0 align-items-center">
                              <div className="col-2 p-0 iconSec">
                                {"deposit-account" === this.state.activeLink ? (
                                  <img src={Deposit_Account_Blue} className="depositIcon"></img>
                                ) : (
                                  <img src={Deposit_Account_Grey} className="depositIcon"></img>
                                )}{" "}
                              </div>
                              <div className="col p-0">
                                <p className={"deposit-account" === this.state.activeLink ? "activeLabel tabLabel" : "tabLabel"}>
                                  Deposit account
                                </p>
                              </div>
                            </div>
                          </li>
                          <li
                            className={"locations" === this.state.activeLink ? "activeLink" : " "}
                            onClick={this.handelTabClick.bind(this, "locations")}
                          >
                            <div className="row m-0 align-items-center">
                              <div className="col-2 p-0 iconSec">
                                {"locations" === this.state.activeLink ? (
                                  <img src={Location_Icon_Blue} className="depositIcon"></img>
                                ) : (
                                  <img src={Loation_Icon_Grey} className="depositIcon"></img>
                                )}{" "}
                              </div>
                              <div className="col p-0">
                                <p className={"locations" === this.state.activeLink ? "activeLabel tabLabel" : "tabLabel"}>Locations</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div className="col TabPanel p-0">{pageContent}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .ProfilePageSec {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              font-size: 1.25vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.iconSec) {
              max-width: 1.464vw;
            }
            .Tabs {
              padding: 0.732vw 0;
              max-width: 11.713vw;
            }
            .AccountTabs {
              //   position: sticky;
              //   top: 73px;
            }
            .AccountTabs > li {
              padding: 0.219vw 0.732vw;
              cursor: pointer;
            }
            .accList {
              padding: 0.366vw 0.732vw;
            }
            :global(.tabLabel) {
              cursor: pointer;
              font-size: 0.841vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
              line-height: 1;
            }
            .TabPanel {
              margin-left: 0.585vw;
            }
            .activeLink {
              background: ${THEME_COLOR_Opacity};
            }
            :global(.activeLabel) {
              color: ${THEME_COLOR} !important;
              font-family: "OpenSans-Bold" !important;
            }
            .linkLabel {
              cursor: pointer;
              font-size: 0.833vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
              line-height: 1;
            }
            .activeLinkLabel {
              color: ${THEME_COLOR} !important;
              font-family: "OpenSans-Bold" !important;
            }
            :global(.accIcon) {
              width: 0.732vw;
              margin-bottom: 0.146vw;
            }
            .paymentIcon {
              width: 0.951vw;
              margin-bottom: 0.146vw;
            }
            .depositIcon {
              width: 0.951vw;
              margin-bottom: 0.146vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default index;
