import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_2,
  WHITE_COLOR,
  FONTGREY_COLOR_Dark,
  Close_Icon,
  DEFAULT_OTP_TIMER,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  Border_LightGREY_COLOR_1,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
// Resend OTP Components
import dynamic from "next/dynamic";

import Countdown from "react-countdown-now";
import Snackbar from "../../components/snackbar";
import { newPhoneNumber } from "../../services/userProfile";

class VerifyOtp extends Component {
  state = {
    OtpValid: false,
    otpTimer: true,
    isFormValid: false,
    date: Date.now() + DEFAULT_OTP_TIMER,
  };

  // Function for otppayload
  handleOTPChange = (event) => {
    console.log("nswjd", event, event.length);
    if (event.length == 4) {
      this.setOtp(event);
    }
  };

  setOtp = (otp) => {
    console.log("OTPPPPP", otp);
    this.setState(
      {
        OtpValid: true,
        otpCode: otp,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state.OtpValid ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  // handle Send OTP Timer
  handleTimer = () => {
    this.setState({
      date: Date.now() + DEFAULT_OTP_TIMER,
    });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleResendOtp = () => {
    let payload = {
      phoneNumber:
        this.props.inputpayload.countryCode + this.props.inputpayload.mobile,
    };
    newPhoneNumber(payload)
      .then((res) => {
        console.log("res", res.data);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.setState({
              otpTimer: true,
              otpCode: "",
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const { OtpValid, otpTimer, isFormValid } = this.state;
    const ReactCodeInput = dynamic(import("react-code-input"));
    const renderer = ({ hours, minutes, seconds, completed }) => {
      if (completed) {
        this.setState({
          otpTimer: false,
        });
        return null;
      } else {
        // Render a countdown
        return (
          <span className="text-right timer">
            {minutes}:{seconds}
          </span>
        );
      }
    };

    return (
      <Wrapper>
        <div className="col-12 VerifyOtpModel p-0">
          <div className="modelContent">
            <img
              src={Close_Icon}
              className="closeIcon"
              onClick={this.props.onClose}
            ></img>
            <div className="text-center">
              <h5 className="heading">Verify Otp</h5>
              <div className="FormInput">
                <div className="otpInput">
                  <ReactCodeInput
                    type="text"
                    value={this.state.otpCode}
                    fields={4}
                    autoFocus={false}
                    onChange={this.handleOTPChange}
                  />
                </div>
                {otpTimer ? (
                  <Countdown
                    date={this.state.date}
                    intervalDelay={0}
                    precision={1}
                    zeroPadTime={1}
                    renderer={renderer}
                  />
                ) : (
                  <span
                    onClick={this.handleResendOtp}
                    className="text-right resendOtp"
                  >
                    Resend
                  </span>
                )}
                <div className={isFormValid ? "continue_btn" : "inactive_Btn"}>
                  <ButtonComp
                    disabled={!isFormValid}
                    onClick={this.props.handleVerifyOtp.bind(
                      this,
                      this.state.otpCode
                    )}
                  >
                    Verify
                  </ButtonComp>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .FormInput {
              margin: 0.951vw 0;
            }
            :global(.otpInput > div) {
              padding: 0.732vw 0;
              display: flex !important;
              align-items: center;
              justify-content: space-between;
              width: 100%;
            }

            :global(.otpInput > div input) {
              display: block;
              width: max-content !important;
              max-width: 2.928vw;
              padding: 0 1.171vw;
              text-align: center;
              height: 2.196vw !important;
              color: ${GREY_VARIANT_2} !important;
              font-size: 0.878vw;
              border: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              border-radius: 0.219vw;
              opacity: 1 !important;
            }

            :global(.otpInput > div input:active),
            :global(.otpInput > div input:focus) {
              outline: none;
              color: black !important;
              border: 0.146vw solid ${THEME_COLOR} !important;
            }

            .resendOtp {
              font-size: 0.878vw;
              cursor: pointer;
              float: right;
            }

            :global(.timer) {
              font-size: 0.878vw;
              float: right;
            }

            .back_btn {
              position: absolute;
              left: 0;
            }
            .backIcon {
              width: 0.951vw !important;
              margin-right: 0.658vw !important;
            }
            :global(.back_btn button) {
              width: 100%;
              min-width: fit-content;
              height: fit-content;
              text-transform: initial;
              background: transparent;
              color: ${GREY_VARIANT_2};
              padding: 0;
              margin: 0;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              font-weight: 600;
            }
            :global(.back_btn button:hover) {
              background: transparent;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: transparent;
              outline: none;
            }
            .modelContent {
              background: ${WHITE_COLOR};
              box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),
                0px 24px 38px 3px rgba(0, 0, 0, 0.14),
                0px 9px 46px 8px rgba(0, 0, 0, 0.12);
              border-radius: 0.292vw;
              padding: 1.464vw 1.83vw;
            }
            .slider-dots {
              display: flex;
              align-items: center;
              margin: 0.951vw auto;
              justify-content: center;
              position: relative;
            }
            .slider-dots img {
              width: 0.585vw;
            }
            .VerifyOtpModel {
              width: 24.158vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              margin: 0.366vw 0;
              font-size: 1.874vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .continue_btn,
            .inactive_Btn {
              margin: 1.83vw 0 0 0;
            }
            :global(.continue_btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.292vw 0;
              margin: 0 !important;
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              border-radius: 0.146vw;
            }
            :global(.continue_btn button span),
            :global(.inactive_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.continue_btn button:focus),
            :global(.continue_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.continue_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default VerifyOtp;
