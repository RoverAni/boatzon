// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// asstes and colors
import {
  WHITE_COLOR,
  BG_LightGREY_COLOR,
  GREEN_COLOR,
  No_Boat_Post_Img,
  FONTGREY_COLOR,
  enabledTruck,
  enabledMap,
  disabledTruck,
  disabledMap,
  THEME_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_17,
  GREY_VARIANT_8,
} from "../../lib/config";

// imported components
import ProductList from "../landing-page/product-list";

// redux component
import Context from "../../context/context";
import { connect } from "react-redux";

// reusable component
import PageLoader from "../../components/loader/page-loader";


import ProductsFilterSec from "../filter-sec/products-filter-sec";
import NestedList from "../../components/custom-list/nested-list";
import { isEmpty } from "../../lib/global";

class ProductsPage extends Component {
  state = {
    selectedProductType: [],
    selectedProduct: [],
    inputpayload: {},
    priceDropdown: false,
    ProductTypes: [],
    radiusDropdown: false,
    postedinputpayload: {},
    payload: {
      category: "Products",
      similar: false,
    },
    categories: [
      { label: "All", toShow: true },
      {
        label: "Shipping",
        toShow: false,
        img1: disabledTruck,
        img2: enabledTruck,
      },
      { label: "Pickup", toShow: false, img1: disabledMap, img2: enabledMap },
    ],
    selectedCategory: 0, // index
    selectedProductNames: [],
    activeLinkList: [], // temp var
  };

  /** temp func */
  handelTabClick = (mainNode, endNode, nodeId) => {
    let payload = {
      specialityId: nodeId,
    };
    this.props.ProfessionalsDataAPI(payload);
  };

  /** temp func */
  handleListToggle = (name) => {
    this.setState({
      [name]: !this.state[name],
    });
  };

  componentDidMount() {
    /** create payload and call filterProduct API depending on url params if any */
    if (!isEmpty(this.props.urlString)) {
      const {
        condition,
        distanceMax,
        latitude,
        longitude,
        manufactureId,
        fromPrice,
        toPrice,
        subCategory,
        selectedProductType,
        postedWithin,
        zipCode,
      } = {
        ...this.props.urlString,
      };
      // price payload
      let tempPayload = { ...this.state.inputpayload };
      tempPayload.minPrice = fromPrice;
      tempPayload.maxPrice = toPrice;

      // selected manufacturer payload
      let manufactureIdList = manufactureId && manufactureId.split(",");
    

      // selected subCategory and type payload
      let subCategoryList = subCategory ? subCategory.split(",") : [];
      let tempProductTypes =
        this.props.ProductsSubCategoriesList &&
        this.props.ProductsSubCategoriesList.filter((data) => {
          return subCategoryList && subCategoryList.includes(data.subCategoryName);
        });
      let ProductTypePayload =
        tempProductTypes &&
        tempProductTypes.length &&
        tempProductTypes.map((data) => {
          return data.filter && data.filter.length ? data.filter[0].values.split(",") : [];
        });
      let tempProductTypePayload = ProductTypePayload
        ? ProductTypePayload.length > 1
          ? ProductTypePayload[ProductTypePayload.length - 1].concat(this.state.ProductTypes)
          : ProductTypePayload[ProductTypePayload.length - 1]
        : [];

      // postedWithin payload
      let tempPostedWithin =
        postedWithin == "24hr"
          ? { value: postedWithin, label: "last 24 hr" }
          : postedWithin == "15days"
          ? { value: postedWithin, label: "last 15 days" }
          : postedWithin == "30days"
          ? { value: postedWithin, label: "last 30 days" }
          : {};

      this.setState(
        {
          condition: condition,
          valueRadius: distanceMax,
          inputpayload: { ...tempPayload },
          selectedProductNames: subCategoryList,
          ProductTypes: [...tempProductTypePayload],
          selectedProduct: [...tempProductTypes],
          selectedProductType: selectedProductType ? selectedProductType.split(",") : [],
          postedWithin: { ...tempPostedWithin },
        },
        () => {
          let apiPayload = { ...this.state.payload };
          apiPayload.condition = this.state.condition == "All Conditions" ? "" : this.state.condition;
          apiPayload.distanceMax = this.state.valueRadius;
          apiPayload.latitude = distanceMax ? latitude : "";
          apiPayload.distanceMax = distanceMax ? longitude : "";
          apiPayload[`priceRange`] = {};
          apiPayload.priceRange.fromPrice = fromPrice;
          apiPayload.priceRange.toPrice = toPrice;
          apiPayload[`subCategory`] =
            this.state.selectedProductNames && this.state.selectedProductNames.length
              ? this.state.selectedProductNames.toString().replace(/,/g, " OR ")
              : "";
          apiPayload[`type`] =
            this.state.selectedProductType && this.state.selectedProductType.length ? this.state.selectedProductType.toString() : "";
          apiPayload.postedWithin = postedWithin;
          apiPayload.zipCode = zipCode;

          this.setState(
            {
              payload: apiPayload,
            },
            () => {
              this.props.handlePostProductsAPI(this.state.payload); // API call
            }
          );
        }
      );
    }
  }

  /** function to create subCategory and subCategory type payload */
  handleSelectedProductList = (selectedProduct) => {
    let temppayload = [...this.state.selectedProduct];
    let elementIndex = temppayload.findIndex((data) => {
      return data.subCategoryName === selectedProduct.subCategoryName;
    });
    elementIndex == -1 ? temppayload.push(selectedProduct) : temppayload.splice(elementIndex, 1);

    this.setState(
      {
        selectedProduct: [...temppayload],
      },
      () => {
        let ProductTypePayload =
          this.state.selectedProduct &&
          this.state.selectedProduct.length &&
          this.state.selectedProduct.map((data) => {
            return data.filter && data.filter.length ? data.filter[0].values.split(",") : [];
          });
        let tempPayload = ProductTypePayload
          ? ProductTypePayload.length > 1
            ? ProductTypePayload[ProductTypePayload.length - 1].concat(this.state.ProductTypes)
            : ProductTypePayload[ProductTypePayload.length - 1]
          : "";
        let selectedProductNames =
          this.state.selectedProduct &&
          this.state.selectedProduct.length &&
          this.state.selectedProduct.map((data) => {
            return data.subCategoryName;
          });
        this.setState(
          {
            ProductTypes: [...tempPayload],
            selectedProductNames,
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`subCategory`] = this.state.selectedProductNames.toString().replace(/,/g, " OR ");
            this.props.handleUrlParams("subCategory", this.state.selectedProductNames.toString()); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload); // API call
              }
            );
          }
        );
      }
    );
  };

  /** funtion to Picky Selection inputpayload */
  handleTypeOnSelect = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        console.log("ndsjcfjdkswbdhsb", name);

        let apiPayload = { ...this.state.payload };
        if (this.state.condition == "All Conditions") {
          apiPayload[`${name}`] = "";
        } else if (name == "selectedProductType") {
          apiPayload[`type`] = this.state[name].toString();
        } else {
          apiPayload[`${name}`] = this.state[name];
        }
        this.props.handleUrlParams(name, this.state[name].toString()); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.preventAPICallOnFilterFunction(false);
            this.props.handlePostProductsAPI(this.state.payload); // API call
          }
        );
      }
    );
  };


  /** function to toggle radius dropdown */
  radiusDropdownOpen = () => {
    this.props.currentLocation
      ? this.setState({
          radiusDropdown: !this.state.radiusDropdown,
        })
      : this.props.dispatch(getCurrentLocation());
  };

  /** function to set radius value on slide */
  onChangeRadius = (event, value) => {
    this.setState(
      {
        valueRadius: value,
      },
      () => {
        let urlParams = {};
        urlParams[`distanceMax`] = this.state.valueRadius;
        urlParams[`latitude`] = this.props.currentLocation.latitude;
        urlParams[`longitude`] = this.props.currentLocation.longitude;
        let apiPayload = { ...this.state.payload, ...urlParams };

        this.props.handleUrlParams("distanceMax", urlParams); // function to add filterdata in url params
        console.log("ndsjcfjdk", apiPayload);

        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API call
          }
        );
      }
    );
  };

  /** function to toggle price dropdown */
  priceDropdownOpen = () => {
    if (!this.state.minPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.minPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    if (!this.state.maxPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.maxPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    this.setState({
      priceDropdown: !this.state.priceDropdown,
    });
  };

  /** function to handle priceInput */
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  /** function to set min and max price */
  handlePriceRange = () => {
    let { minPrice, maxPrice } = this.state.inputpayload;
    this.setState(
      {
        minPrice,
        maxPrice,
        priceDropdown: !this.state.priceDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`priceRange`] = {};
        apiPayload[`priceRange`].fromPrice = this.state.minPrice;
        apiPayload[`priceRange`].toPrice = this.state.maxPrice;

        this.props.handleUrlParams("priceRange", apiPayload.priceRange); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API call
          }
        );
      }
    );
  };

  handleOnSelectInput = (name) => (event) => {
    this.setState(
      {
        [name]: event,
      },
      () => {
        if (event instanceof Array || this.state[name] == null) {
          let apiPayload = { ...this.state.payload };
          let valArr =
            this.state[name] != null && this.state[name] && this.state[name].length > 0 ? this.state[name].map((data) => data.value) : "";
          apiPayload[name] = valArr.toString();
          this.props.handleUrlParams(`${name}`, valArr.toString()); // function to add filterdata in url params
          this.setState(
            {
              payload: apiPayload,
            },
            () => {
              this.props.handlePostProductsAPI(this.state.payload);
            }
          );
        } else {
          let apiPayload = { ...this.state.payload };

          apiPayload[name] =
            name == "postedWithin"
              ? this.handlePostedWithin(this.state[name].value)
              : name == "boat_model" || name == "color"
              ? this.state[name].label
              : this.state[name].value;
          this.props.handleUrlParams(`${name}`, name == "boat_model" || name == "color" ? this.state[name].label : this.state[name].value); // function to add filterdata in url params

          this.setState(
            {
              payload: apiPayload,
            },
            () => {
              this.props.handlePostProductsAPI(this.state.payload);
            }
          );
        }
      }
    );
  };

  /** function to set API postedWithin payload  */
  handlePostedWithin = (value) => {
    switch (value) {
      case "24hr":
        return "1";
        break;
      case "15days":
        return "2";
        break;
      case "30days":
        return "3";
        break;
    }
  };

  selectCategory = (i) => {
    let cat = [...this.state.categories];
    if (i !== 0 || i !== 1 || i !== 2) {
      let index = cat.findIndex((k) => k.toShow === true);
      if (index > -1) {
        cat[index].toShow = !cat[index].toShow;
      }
    }
    cat[i].toShow = !cat[i].toShow;
    this.setState({ selectedCategory: i, categories: cat });
  };

  // _productCategories={this.props.productCategories}
  // _subSubCategories={this.props.subSubCategories}
  // _getProductSubSubCategoriesFunc={this.props._getProductSubSubCategoriesFunc}

  handleChangeCondition = (conditionName) => {
    this.setState(
      {
        condition: conditionName,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`condition`] = this.state.condition;
        this.props.handleUrlParams(`condition`, this.state.condition); // function to add filterdata in url params

        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  render() {
    let { _productCategories, _appendToCategory, _getProductSubSubCategoriesFunc } = this.props;
    const { activeLinkList } = this.state;
    return (
      <Context.Consumer>
        {(Context) => {
          return (
            <Wrapper>
              <div className="position-relative ProductListSec pt-4">
                <div
                  id="loderOverlay"
                  style={{
                    display: this.props.apiCallLoader ? "block" : "none",
                  }}
                >
                  <div className="loader">
                    <PageLoader loading={true} color={WHITE_COLOR} />
                  </div>
                </div>
                <section className="product-page-m-section">
                  <div className="row m-0 FilterSec">
                    <div className="col-2 ProductListSec px-0">
                      <div className="row mx-0">
                        <div className="select-order-type w-100 d-flex">
                          {this.state.categories.map((k, i) => (
                            <div
                              key={i}
                              className={
                                i === this.state.selectedCategory
                                  ? "selected d-flex align-items-center"
                                  : "non-selected d-flex align-items-center"
                              }
                              onClick={() => this.selectCategory(i)}
                            >
                              <span className="pr-1">
                                <img className={i > 0 ? "selector-img" : ""} src={i > 0 ? (k.toShow ? k.img1 : k.img2) : ""} />
                              </span>
                              <span className="products-options">{k.label}</span>
                            </div>
                          ))}
                        </div>
                        <div className="c-nested-list-drawer mt-3">
                          <ul className="list-unstyled AccountTabs">
                            {_productCategories &&
                              _productCategories.map((k, i) => (
                                <li key={i}>
                                  <NestedList
                                    productsPage={true}
                                    products={"products"}
                                    name={k.subCategoryName}
                                    onClick={() => {
                                      _appendToCategory(k.subCategoryName);
                                      _getProductSubSubCategoriesFunc(k.subCategoryName, i);

                                      this.handleListToggle.bind(this, k.subCategoryName.toLowerCase().replace(/ /g, "_"));
                                    }}
                                    open={k.isOpen}
                                    // onClick={this.handleListToggle.bind(this, data.name.toLowerCase().replace(/ /g, "_"))}
                                  >
                                    {k && k.subSubCategoryList && k.subSubCategoryList.length > 0 ? (
                                      <ul className="list-unstyled engineService_List">
                                        {k.subSubCategoryList &&
                                          k.subSubCategoryList.map((items, key) => (
                                            <li
                                              key={key}
                                              className={
                                                activeLinkList[key] === this.state.activeLink
                                                  ? "products-activeLink"
                                                  : "products-aactiveLink-selected"
                                              }
                                              onClick={() => _appendToCategory(items.subCategoryName)}
                                              // onClick={this.handelTabClick.bind(this, data.name, items.name, data.nodeId)}
                                            >
                                              {items.subCategoryName}
                                            </li>
                                          ))}
                                      </ul>
                                    ) : (
                                      ""
                                    )}
                                  </NestedList>
                                </li>
                              ))}
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-10 px-0 product-sec-with-data">
                      <div className="row m-0">
                        {/* Filter Section Module */}
                        <ProductsFilterSec
                          handleOnSelectInput={this.handleOnSelectInput}
                          manufactureId={this.state.manufactureId}
                          productpage={true}
                          _categoryArray={this.props._categoryArray}
                          heading={`${Context.locale.all} ${Context.locale.products}`}
                          ProductsSubCategoriesList={this.props.ProductsSubCategoriesList}
                          handleSelectedProductList={this.handleSelectedProductList}
                          ProductTypes={this.state.ProductTypes}
                          handleTypeOnSelect={this.handleTypeOnSelect}
                          condition={this.state.condition}
                          handleChangeCondition={this.handleChangeCondition}
                          selectedProductType={this.state.selectedProductType}
                          selectedProductNames={this.state.selectedProductNames}
                          radiusDropdownOpen={this.radiusDropdownOpen}
                          radiusDropdown={this.state.radiusDropdown}
                          onChangeRadius={this.onChangeRadius}
                          valueRadius={this.state.valueRadius}
                          priceDropdownOpen={this.priceDropdownOpen}
                          handleOnchangeInput={this.handleOnchangeInput}
                          handlePriceRange={this.handlePriceRange}
                          inputpayload={this.state.inputpayload}
                          priceDropdown={this.state.priceDropdown}
                        />
                        {this.props.filterPostData && this.props.filterPostData.length > 0 ? (
                          // Boat List Module
                          <ProductList
                            breakpointCols={4}
                            productpage={true}
                            ProductsList={this.props.filterPostData}
                            postedWithin={this.state.postedWithin}
                            handleOnSelectInput={this.handleOnSelectInput}
                          />
                        ) : (
                          <div className="container p-md-0">
                            <div className="screenWidth mx-auto">
                              <div className="noFavorites">
                                <div className="text-center">
                                  <img src={No_Boat_Post_Img} className="noFavFollowingImg"></img>
                                  <p className="noFavoritesMsg">No Products Post</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </section>
                {/* <div className="shopProducts_btn">
                  <ButtonComp>
                    <img
                      src={Shop_Cart_White_Icon}
                      className="shopcartIcon"
                    ></img>
                    Shop Products for Sea Ray
                  </ButtonComp>
                </div> */}
              </div>

              <style jsx>
                {`
                  // temp using
                  .c-nested-list-drawer {
                    width: 92%;
                  }
                  .products-options {
                    font-family: Open Sans !important;
                  }
                  .selector-img {
                    width: 0.963vw;
                    height: 0.8025vw;
                  }
                  .ProfessionalsPageSec {
                    background: ${BG_LightGREY_COLOR};
                  }
                  .AccountTabs {
                    background: #f5f5f5 !important;
                    // padding: 0 0.585vw;
                  }
                  .TabPanel {
                    margin-left: 0.585vw;
                  }
                  :global(.products-activeLink) {
                    background: #f5f5f5 !important;
                    color: #4b5777 !important;
                    font-size: 0.749vw !important;
                    // color: ${THEME_COLOR} !important;
                  }
                  :global(.products-activeLink-selected) {
                    background: #f5f5f5 !important;
                    color: #4b5777 !important;
                    // color: ${THEME_COLOR} !important;
                  }
                  .engineService_List li {
                    background: #f5f5f5 !important;
                    cursor: pointer;
                    font-size: 0.805vw;
                    padding: 0.219vw 0 0.219vw 0.732vw;
                    margin-left: 0.585vw;
                    font-family: "Open Sans" !important;
                    color: ${GREY_VARIANT_2};
                  }
                  .engineService_List li:hover {
                    color: ${GREY_VARIANT_8} !important;
                    font-weight: 600;
                    background: ${GREY_VARIANT_17} !important;
                  }
                  // ends

                  .select-order-type {
                    border-radius: 4px;
                  }
                  .selected {
                    background: #378bcb;
                    font-size: 0.7291vw;
                    font-weight: 600;
                    color: #fff;
                    font-family: Open Sans !important;
                    padding: 0.321vw 0.642vw;
                    border-top-left-radius: ${this.state.selectedCategory === 0 ? "0.160vw" : "0px"};
                    border-bottom-left-radius: ${this.state.selectedCategory === 0 ? "0.160vw" : "0px"};
                    border-top-right-radius: ${this.state.selectedCategory === 2 ? "0.160vw" : "0px"};
                    border-bottom-right-radius: ${this.state.selectedCategory === 2 ? "0.160vw" : "0px"};
                    cursor: pointer !important;
                  }
                  .non-selected {
                    background: #fff;
                    color: #717c99;
                    font-size: 0.7291vw;
                    font-weight: 600;
                    padding: 0.321vw 0.642vw;
                    font-family: Open Sans !important;
                    border-top-right-radius: 0.16vw;
                    border-bottom-right-radius: 0.16vw;
                    cursor: pointer !important;
                  }
                  .product-sec-with-data {
                    background: ${WHITE_COLOR};
                    border-radius: 4px;
                  }
                  .product-page-m-section {
                    width: 85vw;
                    margin: auto;
                    background: ${BG_LightGREY_COLOR};
                  }
                  .ProductListSec {
                    // padding: 0.732vw 0;
                    background: ${BG_LightGREY_COLOR};
                  }
                  .FilterSec {
                    background: ${WHITE_COLOR};
                  }
                  .shopcartIcon {
                    width: 1.024vw;
                    margin-right: 0.512vw;
                  }
                  .shopProducts_btn {
                    position: fixed;
                    bottom: 10px;
                    left: 50%;
                    transform: translate(-50%);
                    z-index: 1;
                    // width: 45%;
                  }
                  :global(.shopProducts_btn button) {
                    width: 100%;
                    padding: 0.732vw 20px;
                    background: ${GREEN_COLOR};
                    color: ${WHITE_COLOR};
                    margin: 0;
                    text-transform: capitalize;
                    position: relative;
                    font-weight: 500;
                    box-shadow: 2px 2px 3px #999;
                  }
                  :global(.shopProducts_btn button span) {
                    font-family: "Museo-Sans-Cyrl-Semibold" !important;
                    letter-spacing: 0.0439vw !important;
                    font-size: 11px;
                  }
                  :global(.shopProducts_btn button:focus),
                  :global(.shopProducts_btn button:active) {
                    background: ${GREEN_COLOR};
                    outline: none;
                    box-shadow: none;
                  }
                  :global(.shopProducts_btn button:hover) {
                    background: ${GREEN_COLOR};
                  }
                  :global(.FollowingLoader) {
                    height: 13.762vw;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                  }
                  :global(.noFavFollowingImg) {
                    width: 14.641vw;
                    object-fit: cover;
                  }
                  :global(.noFavorites) {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 32.942vw;
                    text-align: center;
                    background: ${WHITE_COLOR};
                  }
                  :global(.noFavoritesMsg) {
                    max-width: 80%;
                    font-size: 1.098vw;
                    color: ${FONTGREY_COLOR};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    margin: 0.732vw auto;
                  }
                `}
              </style>
            </Wrapper>
          );
        }}
      </Context.Consumer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ProductsSubCategoriesList: state.ProductsSubCategoriesList,
    currentLocation: state.currentLocation,
    filterPostData: state.filterPostData,
    apiCallLoader: state.apiCallLoader,
  };
};

export default connect(mapStateToProps)(ProductsPage);
