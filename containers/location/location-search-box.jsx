import React from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import Wrapper from "../../hoc/Wrapper";

import {
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  FONTGREY_COLOR,
  Map_Marker,
  GREY_VARIANT_2,
  GREY_VARIANT_10,
  MapPin,
  BG_LightGREY_COLOR,
  MapSearch,
} from "../../lib/config";

import {
  getCityFromGoogle,
  getCountryFromGoogle,
  getAreaFromGoogle,
  getZipCodeFromGoogle,
  getCountryShortNameFromGoogle,
  getStateFromGoogle,
} from "../../lib/location/location";

let commonStyles = {
  fontSize: "0.878vw",
  cursor: "pointer",
  margin: "0.366vw 0",
  transition: "0.3s",
  // padding: "0.732vw",
  paddingLeft: "32px",
  textAlign: "left",
};

const activeItem = {
  background: THEME_COLOR,
  color: WHITE_COLOR,
  ...commonStyles,
};

const normalItem = {
  ...commonStyles,
};

export default class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: "", showForm: false, isNewAdrChanged: false };
  }

  handleChange = (address) => {
    this.setState({ address: address, isNewAdrChanged: true }, () => {
      console.log("address", this.state.address);
    });
  };

  handleSelect = (address) => {
    let latLong;
    let country = "";
    let city = "";
    let stateName = "";
    let area = "";
    let zipCode = "";
    let countryShortName = "";
    const { showOnlyAddress } = this.props;

    geocodeByAddress(address)
      .then((results) => {
        console.log("Response", results, address);
        country = getCountryFromGoogle(results);
        city = getCityFromGoogle(results);
        stateName = getStateFromGoogle(results);
        area = getAreaFromGoogle(results);
        zipCode = getZipCodeFromGoogle(results);
        countryShortName = getCountryShortNameFromGoogle(results);

        let addressLine = address.toString();
        console.log(
          "Response",
          addressLine,
          city,
          country,
          stateName,
          area,
          zipCode,
          countryShortName
        );
        // this.props && this.props.updateUserAddress(addressLine);
        this.handleChange(
          addressLine && addressLine.length > 1 && showOnlyAddress
            ? addressLine
            : address
        );

        return getLatLng(results[0]);
      })
      .then((latLng) =>
        this.updateData(
          latLng,
          city,
          country,
          stateName,
          area,
          zipCode,
          countryShortName
        )
      )
      .catch((error) => console.error("Error", error));
  };

  updateData(data, city, country, area, stateName, zipCode, countryShortName) {
    console.log(
      "febufhdu",
      data,
      city,
      country,
      stateName,
      area,
      zipCode,
      countryShortName
    );
    data["address"] = this.state.address;
    data["city"] = city;
    data["country"] = country;
    data["stateName"] = stateName;
    data["area"] = area;
    data["zipCode"] = zipCode;
    data["countryShortName"] = countryShortName;
    this.props.updateLocation(data);
  }

  componentWillReceiveProps = async () => {
    (await !this.state.isNewAdrChanged) ? this.clearAddress() : "";
    this.props.locErrMessage ? this.clearAddress() : "";
    await this.setState({
      address:
        this.props.address && !this.state.isNewAdrChanged
          ? this.props.address
          : this.state.address,
      showForm: true,
    });
  };

  clearAddress() {
    this.setState({ address: "" });
  }

  clearState = () => this.setState({ isNewAdrChanged: false });
  componentWillUnmount() {
    this.setState({ address: "" });
  }

  componentDidMount() {
    this.props.onRef ? this.props.onRef(this) : "";
  }

  componentDidUpdate = (preProps) => {
    console.log("XXXXX");
    if (preProps.value != this.props.value) {
      console.log("XXXXX222", preProps.value, this.props.value);
      this.setState(
        {
          address: this.props.value,
        },
        () => {
          console.log("XXXXX2223333", this.state.address);
        }
      );
    }
  };

  render() {
    const { locale, inputStyle, locationModel } = this.props;

    console.log("MY ADDRESS--> ", this.state.address);

    return (
      <Wrapper>
        <PlacesAutocomplete
          value={this.state.address}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
          shouldFetchSuggestions={this.state.address.length >= 2}
        >
          {({ getInputProps, suggestions, getSuggestionItemProps }) => (
            <div
              className={
                this.props.borderBottomWithIcon
                  ? this.props.locationIcon
                    ? "position-relative locationWithIconSearchBox inputBox-underline"
                    : "position-relative locationWithoutIconSearchBox inputBox-underline"
                  : this.props.servicesInput
                  ? "position-relative servicesInput"
                  : locationModel
                  ? "position-relative locationModelSearchBox"
                  : "position-relative locationSearchBox"
              }
            >
              {locationModel ? (
                <img src={MapSearch} alt="map-search" className="searchIcon" />
              ) : (
                ""
              )}
              {locationModel ? (
                <img src={MapPin} alt="map-pin" className="mapPinIcon" />
              ) : (
                ""
              )}
              <input
                {...getInputProps({
                  placeholder: this.props.placeHolder
                    ? this.props.placeHolder
                    : locationModel
                    ? "Address, City, Zip"
                    : "Enter Location",
                  className: "location-input-box",
                  style: inputStyle ? { ...inputStyle } : {},
                  id: this.props.id,
                  autoFocus: this.props.autoFocus,
                  required: this.props.required,
                })}
              />
              {suggestions && suggestions.length > 0 ? (
                <div className="autocomplete-dropdown-container">
                  {suggestions.map((suggestion) => {
                    const className = suggestion.active
                      ? "suggestion-item--active"
                      : "suggestion-item";
                    // inline style for demonstration purpose
                    const style = suggestion.active
                      ? { ...activeItem }
                      : { ...normalItem };
                    return (
                      <div
                        {...getSuggestionItemProps(suggestion, {
                          className,
                          style,
                        })}
                      >
                        <span className="sugText">
                          {suggestion.description}
                        </span>
                      </div>
                    );
                  })}
                </div>
              ) : (
                ""
              )}
            </div>
          )}
        </PlacesAutocomplete>
        {locationModel ? (
          <div className="d_input_helper_text">
            For example, a country, city, region,or postal code
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .autocomplete-dropdown-container {
              border-radius: 0.22vw;
              padding: 0.22vw;
              box-shadow: 1px 1px 10px -2px rgba(0, 0, 0, 0.3);
              position: absolute;
              z-index: 999;
              background: #fff;
              width: 100%;
            }

            .locationSearchBox :global(input) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.036vw;
              font-family: "Open Sans" !important;
            }
            .locationSearchBox :global(input):focus {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: none;
              outline: 0;
              font-size: 0.805vw;
              box-shadow: none;
            }
            .locationSearchBox :global(input::placeholder) {
              color: ${this.props.addlocationScreen
                ? GREY_VARIANT_2
                : GREY_VARIANT_3};
              font-size: ${this.props.addlocationScreen ? "0.805vw" : ""};
              font-family: ${this.props.addlocationScreen
                ? "Open Sans !important"
                : ""};
              opacity: ${this.props.addlocationScreen ? 1 : 0.8};
            }
            :global(.servicesInput input) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.036vw;
              font-family: "Open Sans" !important;
            }
            :global(.servicesInput input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: none;
              outline: 0;
              font-size: 0.805vw;
              box-shadow: none;
            }
            :global(.servicesInput input::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              opacity: 1;
            }
            :global(.locationModelSearchBox) {
              position: relative;
            }
            .d_input_helper_text {
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              margin: 0.366vw 0 0 0;
              letter-spacing: 0.036vw;
              color: ${GREY_VARIANT_3};
            }
            .searchIcon {
              width: 0.915vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              left: 0.366vw;
            }
            .mapPinIcon {
              width: 1.537vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.366vw;
              padding-left: 0.366vw;
              border-left: 0.0732vw solid ${GREY_VARIANT_10};
            }
            :global(.locationModelSearchBox input) {
              width: 100%;
              height: 2.342vw;
              padding: 0.366vw 1.83vw;
              color: ${FONTGREY_COLOR};
              background-color: ${BG_LightGREY_COLOR};
              background-clip: padding-box;
              border: none;
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.036vw;
              font-family: "Open Sans" !important;
            }
            :global(.locationModelSearchBox input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${BG_LightGREY_COLOR};
              outline: 0;
              font-size: 0.951vw;
              box-shadow: none;
            }
            :global(.locationModelSearchBox input::placeholder) {
              font-size: 0.951vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_10};
              opacity: 1;
            }

            .locationWithIconSearchBox,
            .locationWithoutIconSearchBox {
              display: flex;
              border-bottom: 0.073vw solid ${GREY_VARIANT_3};
            }
            .locationWithIconSearchBox .autocomplete-dropdown-container,
            .locationWithoutIconSearchBox .autocomplete-dropdown-container {
              border-radius: 0.22vw;
              padding: 0.22vw;
              box-shadow: 1px 1px 10px -2px rgba(0, 0, 0, 0.3);
              position: absolute;
              top: 2.342vw;
              z-index: 999;
              background: #fff;
              width: 100%;
            }
            :global(.locationWithIconSearchBox input),
            :global(.locationWithoutIconSearchBox input) {
              width: 100%;
              height: 2.489vw;
              padding: 0 0 0 0.366vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
            }
            :global(.locationWithIconSearchBox)::before {
              content: url(${Map_Marker});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.439vw 0 0 !important;
              position: relative;
              top: 0.292vw;
            }
            :global(.locationWithIconSearchBox input:focus),
            :global(.locationWithoutIconSearchBox input:focus) {
              border: none !important;
              outline: none !important;
            }
            :global(.locationWithIconSearchBox
                input:focus
                .locationWithIconSearchBox) {
              border-color: ${THEME_COLOR} !important;
            }
            :global(.locationWithIconSearchBox input::placeholder),
            :global(.locationWithoutIconSearchBox input::placeholder) {
              color: ${GREY_VARIANT_3};
              font-size: 0.878vw;
              font-family: "Open Sans" !important;
              opacity: 1;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
