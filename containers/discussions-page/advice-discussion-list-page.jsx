import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  THEME_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  NoDiscussionFollowing_Img,
  FONTGREY_COLOR,
} from "../../lib/config";
import DiscussionPostCard from "./discussion-post-card";
import SelectInput from "../../components/input-box/select-input";

class AdviceDiscussionListPage extends Component {
  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: temppayload,
      });
    }
  };

  render() {
    const SortTypes = [
      { value: "Latest", label: "Latest" },
      { value: "Popular", label: "Popular" },
      { value: "15 days", label: "15 days" },
    ];
    const { AllAdviceDiscussionsData } = this.props;

    return (
      <Wrapper>
        <div className="adviceListingPage_Sec">
          {AllAdviceDiscussionsData && AllAdviceDiscussionsData.length > 0 ? (
            <Wrapper>
              <div className="borderBottom pb-md-2 pb-lg-3">
                <div className="row m-0 align-items-center justify-content-between">
                  <h4 className="heading">
                    All Discussions{" "}
                    <span>
                      /{" "}
                      {AllAdviceDiscussionsData &&
                        AllAdviceDiscussionsData.length}{" "}
                      posts
                    </span>
                  </h4>
                  <div className="d-flex align-items-center">
                    <p className="sortLabel">Sort by: </p>
                    <div className="NoBoxSelectInput">
                      <SelectInput
                        noboxstyle={true}
                        placeholder="Latest"
                        options={SortTypes}
                        onChange={this.handleOnSelectInput(`sortTypes`)}
                      />
                    </div>
                  </div>
                </div>
              </div>
              {AllAdviceDiscussionsData &&
                AllAdviceDiscussionsData.length &&
                AllAdviceDiscussionsData.map((data, index) => (
                  <div className="borderBottom" key={index}>
                    <DiscussionPostCard
                      postData={data}
                      value={this.props.value}
                      AuthPass={this.props.AuthPass}
                    />
                  </div>
                ))}
            </Wrapper>
          ) : (
            <div className="noDiscussion">
              <div className="text-center">
                <img
                  src={NoDiscussionFollowing_Img}
                  className="nodiscussionImg"
                ></img>
                <p className="noDiscussionMsg">
                  There are no advice discussions yet.
                </p>
              </div>
            </div>
          )}
        </div>

        <style jsx>
          {`
            .borderBottom {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .nodiscussionImg {
              width: 8.784vw;
              object-fit: cover;
            }
            .noDiscussion {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 25.622vw;
              text-align: center;
            }
            .noDiscussionMsg {
              max-width: 100%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            .adviceListingPage_Sec {
              background: ${WHITE_COLOR};
              padding: 1.098vw;
              margin-top: 0.366vw;
            }
            .heading {
              font-size: 1.317vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .heading span {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .sortLabel {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0;
              letter-spacing: 0.0219vw !important;
            }
            .NoBoxSelectInput {
              margin: 0 0.366vw;
              height: auto;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default AdviceDiscussionListPage;
