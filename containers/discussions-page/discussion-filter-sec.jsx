// Main React Components
import React, { Component } from "react";
import { connect } from "react-redux";

// materail-UI Components
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

// asstes and colors
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_1,
} from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import SelectInput from "../../components/input-box/select-input";
import { getManufactures } from "../../services/manufacturer";

function TabContainer(props) {
  return <Typography component="div">{props.children}</Typography>;
}

const styles = (theme) => ({
  Discussionroot: {
    flexGrow: 1,
    backgroundColor: "none !important",
  },
  flexRoot: {
    width: "100%",
    minHeight: "auto",
  },
  indicatorButton: {
    background: "none",
    top: 0,
  },
  scrollerDiscussionButton: {
    height: "2.342vw !important",
  },
  flexContainerDiscussionButton: {
    width: "100%",
    maxHeight: "3.147vw",
    height: "100%",
    alignItems: "center",
    margin: "0 auto",
  },
  discussionbuttonLabel: {
    textTransform: "uppercase !important",
    minWidth: "50%",
    display: "flex",
    alignItems: "center",
    padding: "0.292vw 0",
    borderRadius: "0",
    fontSize: "0.805vw",
    alignItems: "center",
    fontWeight: "500 !important",
    height: "100% !important",
    minHeight: "1.830vw !important",
    backgroundColor: `${WHITE_COLOR} !important`,
    color: `${GREY_VARIANT_1} !important`,
    "&:focus, &:active": {
      outline: "none",
      backgroundColor: `${THEME_COLOR} !important`,
      color: `${WHITE_COLOR} !important`,
      fontWeight: "500 !important",
    },
    "& $span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
      letterSpacing: "0.0219vw !important",
      color: `${GREY_VARIANT_1} !important`,
      display: "flex",
      lineHeight: "1 !important",
      alignItems: "center",
      "& $span": {
        lineHeight: "1 !important",
      },
    },
    "&:first-child": {
      borderTopLeftRadius: "0.146vw !important",
      borderBottomLeftRadius: "0.146vw !important",
    },
    "&:last-child": {
      borderTopRightRadius: "0.146vw !important",
      borderBottomRightRadius: "0.146vw !important",
    },
  },
  selectedDiscussionButton: {
    backgroundColor: `${THEME_COLOR} !important`,
    color: `${WHITE_COLOR} !important`,
    fontFamily: `"Open Sans-SemiBold" !important`,
    letterSpacing: "0.0219vw !important",
    "& $span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
      letterSpacing: "0.0219vw !important",
      opacity: "1",
      color: `${WHITE_COLOR} !important`,
    },
  },
  adddiscussionbuttonColor: {
    color: `${FONTGREY_COLOR} !important`,
    opacity: 1,
    fontWeight: "500 !important",
  },
  wrapper: {
    display: "flex !important",
    flexDirection: "row !important",
  },
});

class DiscussionFilterSec extends Component {
  state = {
    inputpayload: {},
    boatManufacturerList: [],
    total_count: "",
    limit: 15,
  };

  // Async Boat Manufacturer List API call
  handleBoatManufacturerList = async (search, loadedOptions, { page }) => {
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "1", // 0-all manufactureres,1-boats and products,2,engines,3-trailers,4-popular manufactures
    };

    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        let oldPayload = [...this.state.boatManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName.toLowerCase(),
                };
              })
            : [];
        this.setState({
          boatManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.boatManufacturerList &&
      this.state.boatManufacturerList.length < this.state.total_count;

    return {
      options: this.state.boatManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const {
      classes,
      tabs,
      tabcontent,
      value,
      specialities,
      handleOnchangeInput,
      handleOnSelectInput,
    } = this.props;

    const { searchInput, categoryId, manufacturerId } = this.props.inputpayload;

    const CategoryListOption =
      specialities &&
      specialities.length &&
      specialities.map((data) => {
        return { value: data.nodeId, label: data.name };
      });


    return (
      <Wrapper>
        <div className={classes.Discussionroot}>
          <div className="row m-0">
            <div className="col-4 d-flex p-0">
              <Tabs
                {...this.props}
                value={value}
                onChange={this.props.handleChange}
                classes={{
                  root: classes.flexRoot,
                  indicator: classes.indicatorButton,
                  scroller: classes.scrollerDiscussionButton,
                  flexContainer: classes.flexContainerDiscussionButton,
                }}
              >
                {tabs.map((tab, key) => (
                  <Tab
                    key={key}
                    label={tab.label}
                    icon={value == key ? tab.activeIcon : tab.inactiveIcon}
                    classes={{
                      root: classes.discussionbuttonLabel,
                      selected: classes.selectedDiscussionButton,
                      textColorInherit: classes.adddiscussionbuttonColor,
                      wrapper: classes.wrapper,
                    }}
                  />
                ))}
              </Tabs>
            </div>
            <div className="col-8 pr-0">
              <div className="filterSec d-flex align-items-center flex-wrap">
                <div className="FormInput">
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="searchInput"
                    value={searchInput}
                    onChange={handleOnchangeInput.bind(this, value)}
                    autoComplete="off"
                    placeholder="Quick Search ..."
                  ></InputBox>
                </div>
                <div className="SelectInput">
                  <SelectInput
                    discussionselectinput={true}
                    placeholder="Categories"
                    value={categoryId}
                    options={CategoryListOption}
                    onChange={handleOnSelectInput(`categoryId`, value)}
                  />
                </div>
                <div className="SelectInput">
                  {/* <SelectInput
                    discussionselectinput={true}
                    placeholder="Manufacturer"
                    value={manufacturerId}
                    options={ManufacturerListOption}
                    onChange={handleOnSelectInput(`manufacturerId`, value)}
                  /> */}

                  <SelectInput
                    asyncSelect={true}
                    discussionselectinput={true}
                    defaultValue={manufacturerId}
                    value={manufacturerId}
                    loadOptions={handleBoatManufacturerList}
                    onChange={handleOnSelectInput(`manufacturerId`, value)}
                    additional={{
                      page: 0,
                    }}
                    hideSelectedOptions={false}
                  />
                </div>
              </div>
            </div>
          </div>
          {tabcontent.map((tabcontent, key) =>
            key === value ? (
              <TabContainer key={key}>{tabcontent.content}</TabContainer>
            ) : (
              ""
            )
          )}
        </div>
        <style jsx>
          {`
            .min-padding {
              padding: 0 0.732vw;
            }
            .minArchive-padding {
              padding: 0 1.464vw;
            }
            :global(.filterSec .FormInput) {
              margin: 0 1.098vw 0.732vw 0;
            }
            :global(.filterSec .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              // padding: 0.292vw 0.732vw;
              padding: 0.292vw 3.125vw 0.292vw 0.72vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0.146vw !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
            }
            :global(.filterSec .FormInput .inputBox:focus) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .SelectInput {
              height: auto;
              margin: 0 1.098vw 0.732vw 0;
            }
            :global(.SelectInput > div:nth-child(1) > div:nth-child(1)) {
              padding: 0px 1.04166vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    specialities: state.specialities,
  };
};

export default connect(mapStateToProps)(
  withStyles(styles)(DiscussionFilterSec)
);
