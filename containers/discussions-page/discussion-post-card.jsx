import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Advice_Blue_Icon_Tab,
  User_Dp,
  Border_LightGREY_COLOR,
  GREEN_COLOR,
  WHITE_COLOR,
  THEME_COLOR,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import Router from "next/router";
import {
  formatDate,
  findDayAgo,
} from "../../lib/date-operation/date-operation";
import Model from "../../components/model/model";
import LoginPage from "../auth-modals/login/login-page";

class DiscussionPostCard extends Component {
  state = {
    loginModel: false,
  };

  handleDiscussionDetailsPage = (postData) => {
    Router.push(
      `/discussion-details/${postData._source.title.replace(/ /g, "-")}?d_id=${
        postData._id
      }`
    );
  };

  handleLoginModel = () => {
    this.setState({
      loginModel: !this.state.loginModel,
    });
  };
  render() {
    const { postData } = this.props;
    return (
      <Wrapper>
        <div className="row m-0 my-3">
          <div className="col-9 p-0 postContent_Sec">
            <h6 className="title mb-1">{postData._source.title}</h6>
            <span className="lastUpdate py-3">
              Updated {findDayAgo(postData._source.updatedOn, "DD MMM YYYY")}
            </span>
            <p className="desc overflow mt-1">{postData._source.description}</p>
            <ul className="list-unstyled d-flex align-items-center specializationList">
              <li className="postName">
                <img src={Advice_Blue_Icon_Tab} className="adviceTabIcon"></img>
                <span>{this.props.value ? "Project" : "Advice"}</span>
              </li>
              <li className="postNameV2">{postData._source.category}</li>
              <li className="postNameV3">{postData._source.manufacturer}</li>
            </ul>
          </div>
          <div className="col-3 pr-0">
            <span className="postedBy">Posted by:</span>
            <div className="row m-0 align-items-center postedBySec">
              <div className="col-1 p-0">
                <img src={User_Dp} className="authorDp"></img>
              </div>
              <div className="col-11 p-0">
                <p className="authorName">{postData._source.username}</p>
              </div>
            </div>
            <p className="postedDate">
              {formatDate(postData._source.createdOn, "MMMM DD YYYY")}
            </p>
            <p className="commentsNum">
              {postData._source.commentCount} comments
            </p>
            <div className="view_btn">
              <ButtonComp
                onClick={
                  this.props.AuthPass
                    ? this.handleDiscussionDetailsPage.bind(this, postData)
                    : this.handleLoginModel
                }
              >
                View
              </ButtonComp>
            </div>
          </div>
        </div>

        {/* Login Model */}
        <Model
          open={this.state.loginModel}
          onClose={this.handleLoginModel}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLoginModel} />
        </Model>

        <style jsx>
          {`
            .postedBySec {
              margin-bottom: 0.219vw !important;
            }
            :global(.view_btn button) {
              width: 100%;
              padding: 0.321vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.585vw 0 0 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.view_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.view_btn button:focus),
            :global(.view_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.view_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .postContent_Sec {
              border-right: 0.0732vw solid ${Border_LightGREY_COLOR};
              padding-right: 0.732vw !important;
            }
            .title {
              font-size: 0.9375vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .lastUpdate {
              font-size: 0.625vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              margin-bottom: 0.219vw;
              letter-spacing: 0.0292vw !important;
              opacity: 0.7;
            }
            .desc {
              font-size: 0.625vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              margin-bottom: 0.219vw;
              letter-spacing: 0.0219vw !important;
              opacity: 0.8;
              overflow: hidden;
            }
            .overflow {
              display: -webkit-box;
              -webkit-line-clamp: 3;
              -webkit-box-orient: vertical;
              overflow: hidden;
            }
            .postedBy,
            .postedDate,
            .commentsNum {
              font-size: 0.625vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              margin-bottom: 0.219vw;
              letter-spacing: 0.0219vw !important;
              opacity: 0.7;
            }
            .authorDp {
              width: 1.1458vw;
              height: 1.1458vw;

              object-fit: cover;
              // padding-right: 0.366vw;
            }
            .authorName {
              font-size: 0.72916vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              margin-bottom: 0 !important;
              letter-spacing: 0.0292vw !important;
              margin-left: 0.416666vw;
            }
            .specializationList {
              padding: 0.732vw 0 0 0;
              margin: 0;
            }
            .specializationList li {
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.219vw 10px;
              font-size: 0.768vw;
              margin-right: 0.512vw;
              cursor: pointer;
              border-radius: 0.146vw;
            }
            .postName {
              border: 0.0732vw solid ${THEME_COLOR};
              padding: 0.219vw 0.366vw 0.219vw 0.585vw !important;
              border-radius: 0.146vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              background: ${WHITE_COLOR} !important;
            }
            .postName span {
              color: ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              background: ${WHITE_COLOR} !important;
              padding: 0.219vw 0.585vw;
              font-size: 0.768vw;
              cursor: pointer;
            }
            .postNameV2 {
              padding: 0.219vw 1.5625vw 0.219vw 1.5625vw !important;
              border-radius: 0.146vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              background: #eaeaea !important;
            }
            .postNameV2 span {
              color: #98a1b9;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.219vw 0.585vw;
              font-size: 0.768vw;
              cursor: pointer;
            }
            .postNameV3 {
              padding: 0.219vw 1.14583vw 0.219vw 1.14583vw !important;
              border-radius: 0.146vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              background: #eaeaea !important;
            }
            .postNameV3 span {
              color: #98a1b9;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.219vw 0.585vw;
              font-size: 0.768vw;
              cursor: pointer;
            }

            .adviceTabIcon {
              width: 0.732vw;
              margin-bottom: 0.146vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default DiscussionPostCard;
