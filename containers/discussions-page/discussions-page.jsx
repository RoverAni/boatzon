import React, { Component } from "react";

import {
  Discussion_Banner,
  GREEN_COLOR,
  WHITE_COLOR,
  THEME_COLOR,
  Add_Discussion_White_Icon,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR,
  Advice_White_Icon,
  Advice_Grey_Icon,
  Project_White_Icon,
  Project_Grey_Icon,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";
import ButtonComp from "../../components/button/button";
import MainDrawer from "../../components/Drawer/Drawer";
import AddDiscussionPage from "../add-discussion-page/add-discussion-page";
import Model from "../../components/model/model";
import SignUpPage from "../auth-modals/sign-up/sign-up-page";
import AdviceDiscussionListPage from "./advice-discussion-list-page";
import DiscussionFilterSec from "./discussion-filter-sec";
import {
  getDiscussions,
  getDiscussionsPostLogin,
} from "../../services/discussions";
import ProjectDiscussionListPage from "./project-dicussion-list-page";
import PageLoader from "../../components/loader/page-loader";
import { getCookie } from "../../lib/session";
import LoginPage from "../auth-modals/login/login-page";
import { connect } from "react-redux";

class DiscussionsPage extends Component {
  state = {
    addDiscussion: false,
    signUpModel: false,
    value: 0,
    inputpayload: {},
    payload: {},
    openPage: false,
    loginModel: false,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState(
      {
        AuthPass,
      },
      () => {
        this.getAdviceDiscussionCategories();
        this.getProjectDiscussionCategories();
      }
    );
  }

  getAdviceDiscussionCategories = (apiPayload) => {
    let payload = {
      type: "0",
      ...apiPayload,
    };
    if (this.state.AuthPass) {
      getDiscussionsPostLogin(payload)
        .then((res) => {
          this.setState({
            AllAdviceDiscussionsData: res?.data?.message
              ? res.data.message
              : [],
          });
        })
        .catch((err) => {
          console.log("err", err);
          this.setState({
            AllAdviceDiscussionsData: [],
          });
        });
    } else {
      getDiscussions(payload)
        .then((res) => {
          this.setState({
            AllAdviceDiscussionsData: res?.data?.message
              ? res.data.message
              : [],
          });
        })
        .catch((err) => {
          console.log("err", err);
          this.setState({
            AllAdviceDiscussionsData: [],
          });
        });
    }
  };

  getProjectDiscussionCategories = (apiPayload) => {
    let payload = {
      type: "1",
      ...apiPayload,
    };
    if (this.state.AuthPass) {
      getDiscussionsPostLogin(payload)
        .then((res) => {
          this.setState(
            {
              AllProjectDiscussions: res?.data?.message ? res.data.message : [],
              AllProjectDiscussionsData: res.data.message,
            },
            () => {
              let tempPayload = [...this.state.AllProjectDiscussions];
              let tempprojectCategories = tempPayload.map((data) => {
                return data._source.category;
              });
              tempprojectCategories = Array.from(
                new Set(tempprojectCategories)
              );
              let tempprojectCategoryIds = tempPayload.map((data) => {
                return data._source.category;
              });
              tempprojectCategoryIds = Array.from(
                new Set(tempprojectCategoryIds)
              );
              this.setState({
                projectCategories: tempprojectCategories,
                projectCategoryIds: tempprojectCategoryIds,
                openPage: true,
              });
            }
          );
        })
        .catch((err) => {
          console.log("err", err);
        });
    } else {
      getDiscussions(payload)
        .then((res) => {
          this.setState(
            {
              AllProjectDiscussions: res.data.message,
              AllProjectDiscussionsData: res.data.message,
            },
            () => {
              let tempPayload = [...this.state.AllProjectDiscussions];
              let tempprojectCategories = tempPayload.map((data) => {
                return data._source.category;
              });
              tempprojectCategories = Array.from(
                new Set(tempprojectCategories)
              );
              let tempprojectCategoryIds = tempPayload.map((data) => {
                return data._source.category;
              });
              tempprojectCategoryIds = Array.from(
                new Set(tempprojectCategoryIds)
              );
              this.setState({
                projectCategories: tempprojectCategories,
                projectCategoryIds: tempprojectCategoryIds,
                openPage: true,
              });
            }
          );
        })
        .catch((err) => {
          console.log("err", err);
        });
    }
  };

  handleCategorySelection = (categoryData) => {
    let categoryId;
    let manufacturer;
    let title;
    let temppayload = { ...this.state.inputpayload };

    if (categoryData == "") {
      categoryId = "";
      manufacturer = "";
      title = "";
      temppayload[`categoryId`] = "";
    } else {
      categoryId = categoryData.nodeId;
      manufacturer =
        temppayload.manufacturerId && temppayload.manufacturerId.value;
      title = temppayload.searchInput;
      temppayload[`categoryId`] = {
        value: categoryData.nodeId,
        label: categoryData.name,
      };
    }
    let apiPayload = { ...this.state.payload };
    apiPayload[`categoryId`] = categoryId;
    apiPayload[`manufacturerId`] = manufacturer ? manufacturer : "";
    apiPayload[`title`] = title ? title : "";

    this.setState(
      {
        payload: { ...apiPayload },
        inputpayload: { ...temppayload },
      },
      () => {
        this.state.value
          ? this.getProjectDiscussionCategories(this.state.payload)
          : this.getAdviceDiscussionCategories(this.state.payload);
      }
    );
  };

  handleAddDiscussion = () => {
    this.setState({
      addDiscussion: !this.state.addDiscussion,
    });
  };

  handleSignupModel = () => {
    this.setState({
      signUpModel: !this.state.signUpModel,
    });
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name, value) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl;
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          let apiPayload = { ...this.state.payload };
          apiPayload[`${name}`] = this.state.inputpayload[name].value;
          this.setState(
            {
              payload: { ...apiPayload },
            },
            () => {
              value
                ? this.getProjectDiscussionCategories(this.state.payload)
                : this.getAdviceDiscussionCategories(this.state.payload);
            }
          );
        }
      );
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (value, event) => {
    let inputControl = event.target;
    console.log("jdwsid", inputControl.name);
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
      },
      () => {
        if (this.state.inputpayload[inputControl.name].length >= 3) {
          let apiPayload = { ...this.state.payload };
          apiPayload[`title`] = this.state.inputpayload.searchInput;
          this.setState(
            {
              payload: { ...apiPayload },
            },
            () => {
              value
                ? this.getProjectDiscussionCategories(this.state.payload)
                : this.getAdviceDiscussionCategories(this.state.payload);
            }
          );
        } else if (this.state.inputpayload[inputControl.name] == "") {
          let apiPayload = { ...this.state.payload };
          apiPayload[`title`] = "";
          this.setState(
            {
              payload: { ...apiPayload },
            },
            () => {
              value
                ? this.getProjectDiscussionCategories(this.state.payload)
                : this.getAdviceDiscussionCategories(this.state.payload);
            }
          );
        }
      }
    );
  };

  handleLoginModel = () => {
    this.setState({
      loginModel: !this.state.loginModel,
    });
  };

  render() {
    const {
      AllAdviceDiscussionsData,
      AllProjectDiscussionsData,
      value,
      AuthPass,
      openPage,
      inputpayload,
      addDiscussion,
    } = this.state;
    const { specialities } = this.props;
    const {
      handleOnchangeInput,
      handleChange,
      handleOnSelectInput,
      handleCategorySelection,
      handleSignupModel,
      handleAddDiscussion,
      handleLoginModel,
      getAdviceDiscussionCategories,
      getProjectDiscussionCategories,
    } = this;
    const Advice = (
      <AdviceDiscussionListPage
        AllAdviceDiscussionsData={AllAdviceDiscussionsData}
        value={value}
        AuthPass={AuthPass}
      />
    );
    const Project = (
      <ProjectDiscussionListPage
        AllProjectDiscussionsData={AllProjectDiscussionsData}
        value={value}
        AuthPass={AuthPass}
      />
    );

    return (
      <Wrapper>
        {openPage ? (
          <Wrapper>
            <div className="discussionPage_Sec">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div className="row m-0 align-items-center justify-content-center">
                    <div className="col-6 text-center">
                      <h2 className="bannerHeading">
                        What are you working on?
                      </h2>
                      <h6 className="bannerCaption">
                        Seek advice from professionals and the community. Start
                        a project or get help on your project, show before and
                        after.
                      </h6>
                      <div className="addDiscussion_btn">
                        <ButtonComp
                          onClick={
                            AuthPass ? handleAddDiscussion : handleLoginModel
                          }
                        >
                          <img
                            src={Add_Discussion_White_Icon}
                            className="addDiscussionIcon"
                          ></img>
                          Add Discussion
                        </ButtonComp>
                      </div>
                    </div>
                    <div className="position-relative">
                      <MainDrawer
                        open={addDiscussion}
                        onClose={handleAddDiscussion}
                        anchor="top"
                        discussionDrawer={true}
                      >
                        <AddDiscussionPage
                          onClose={handleAddDiscussion}
                          getAdviceDiscussionCategories={
                            getAdviceDiscussionCategories
                          }
                          getProjectDiscussionCategories={
                            getProjectDiscussionCategories
                          }
                        />
                      </MainDrawer>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="discussionPageListing py-3">
              <div className="container p-md-0">
                <div className="screenWidth mx-auto">
                  <div className="row m-0 justify-content-center">
                    <div className="col-3 pl-0">
                      {AuthPass ? (
                        ""
                      ) : (
                        <div className="section">
                          <h6 className="title">New to Boatzon?</h6>
                          <p className="desc">
                            Sign Up to get your own personalized feed
                          </p>
                          <div
                            className="signUp_btn"
                            onClick={handleSignupModel}
                          >
                            <ButtonComp>Sign Up</ButtonComp>
                          </div>
                        </div>
                      )}

                      <div className="section">
                        <div className="featuredAdvice_Sec">
                          <h6 className="title">Featured Advice</h6>
                          <ul className="list-unstyled adviceList m-0">
                            {specialities &&
                              specialities.map((data, index) => (
                                <li
                                  key={index}
                                  onClick={handleCategorySelection.bind(
                                    this,
                                    data
                                  )}
                                >
                                  {data.name}
                                </li>
                              ))}
                          </ul>
                          <a
                            target="_blank"
                            className="viewAll"
                            onClick={handleCategorySelection.bind(this, "")}
                          >
                            View All
                          </a>
                        </div>

                        <div className="featuredProject_Sec">
                          <h6 className="title">Featured Project</h6>
                          <ul className="list-unstyled projectList m-0">
                            {specialities &&
                              specialities.map((data, index) => (
                                <li
                                  key={index}
                                  onClick={handleCategorySelection.bind(
                                    this,
                                    data
                                  )}
                                >
                                  {data.name}
                                </li>
                              ))}
                          </ul>
                          <a
                            target="_blank"
                            className="viewAll"
                            onClick={handleCategorySelection.bind(this, "")}
                          >
                            View All
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="col-9 p-0">
                      <DiscussionFilterSec
                        tabs={[
                          {
                            label: <span>Advice</span>,
                            activeIcon: (
                              <img
                                src={Advice_White_Icon}
                                className="adviceIcon"
                              ></img>
                            ),
                            inactiveIcon: (
                              <img
                                src={Advice_Grey_Icon}
                                className="adviceIcon"
                              ></img>
                            ),
                          },
                          {
                            label: <span>Project</span>,
                            activeIcon: (
                              <img
                                src={Project_White_Icon}
                                className="projectIcon"
                              ></img>
                            ),
                            inactiveIcon: (
                              <img
                                src={Project_Grey_Icon}
                                className="projectIcon"
                              ></img>
                            ),
                          },
                        ]}
                        tabcontent={[{ content: Advice }, { content: Project }]}
                        AllAdviceDiscussionsData={AllAdviceDiscussionsData}
                        AllProjectDiscussionsData={AllProjectDiscussionsData}
                        handleOnSelectInput={handleOnSelectInput}
                        handleChange={handleChange}
                        handleOnchangeInput={handleOnchangeInput}
                        value={value}
                        inputpayload={inputpayload}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Wrapper>
        ) : (
          <div className="Loader">
            <PageLoader loading={!openPage} />
          </div>
        )}

        {/* SignUp Model */}
        <Model
          open={this.state.signUpModel}
          onClose={this.handleSignupModel}
          authmodals={true}
        >
          <SignUpPage onClose={this.handleSignupModel} />
        </Model>

        {/* Login Model */}
        <Model
          open={this.state.loginModel}
          onClose={this.handleLoginModel}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLoginModel} />
        </Model>

        <style jsx>
          {`
            .discussionPageListing {
              background: ${BG_LightGREY_COLOR};
            }
            .section {
              background: ${WHITE_COLOR};
              padding: 0.732vw;
              border-radius: 0.219vw;
              margin-bottom: 0.732vw;
            }
            .discussionPage_Sec {
              background-image: url(${Discussion_Banner});
              min-height: 13.177vw;
              background-repeat: no-repeat;
              background-size: cover;
              position: relative;
              display: flex;
              flex-direction: row-reverse;
              align-items: center;
              position: relative;
            }
            .bannerHeading {
              font-size: 1.6rem;
              color: ${WHITE_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-weight: 500;
            }
            .bannerHeading:first-letter {
              text-transform: capitalize;
            }
            .bannerCaption {
              font-size: 0.805vw;
              margin: 0 auto;
              max-width: 80%;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              text-transform: capitalize;
            }
            :global(.addDiscussion_btn button) {
              width: 40%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.104vw;
            }
            :global(.addDiscussion_btn button span) {
              font-family: "Museo-Sans" !important;
              line-height: 1;
              font-size: 0.878vw;
            }
            :global(.addDiscussion_btn button:focus),
            :global(.addDiscussion_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.addDiscussion_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .addDiscussionIcon {
              width: 1.098vw;
              margin-right: 0.732vw;
            }
            .title {
              font-size: 0.951vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans" !important;
            }
            .desc {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              margin-bottom: 0.219vw;
            }
            :global(.signUp_btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              position: relative;
            }
            :global(.signUp_btn button span) {
              font-family: "Open Sans" !important;
              text-transform: capitalize;
              font-size: 0.878vw;
              line-height: 1;
            }
            :global(.signUp_btn button:focus),
            :global(.signUp_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.signUp_btn button:hover) {
              background: ${THEME_COLOR};
            }
            .adviceList li,
            .projectList li {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              margin-bottom: 0.292vw;
              cursor: pointer;
            }
            .adviceList li:hover,
            .projectList li:hover {
              color: ${THEME_COLOR};
            }
            .viewAll {
              font-size: 0.805vw;
              color: ${THEME_COLOR} !important;
              font-family: "Open Sans" !important;
              margin-bottom: 0.292vw;
              cursor: pointer;
            }
            .featuredAdvice_Sec {
              padding: 1.098vw 0 0.585vw 0;
            }
            .featuredProject_Sec {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR};
              padding: 1.464vw 0 0.585vw 0;
            }
            .adviceIcon {
              width: 0.878vw;
              margin-right: 0.292vw;
              margin-bottom: 0 !important;
            }
            .projectIcon {
              width: 0.732vw;
              margin-bottom: 0 !important;
              margin-right: 0.292vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
    specialities: state.specialities,
  };
};

export default connect(mapStateToProps)(DiscussionsPage);
