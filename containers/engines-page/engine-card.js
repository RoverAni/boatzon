// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_2,
  GREY_VARIANT_1,
  THEME_COLOR,
  WHITE_COLOR,
  PlaceHolder_Boat,
  InfoIcon,
} from "../../lib/config";

class EngineCard extends Component {
  /** if image returns 404, a placeholder image would be replaced */
  onError = (e) => {
    e.target.src = PlaceHolder_Boat;
  };
  render() {
    const { Productdetails, wishlist, professionalpage } = this.props;
    let price = Productdetails && Productdetails.price;
    let intr = 4.5 / 1200;
    let term = 20 * 12; // 20 years
    let loanQuote = (price * intr) / (1 - Math.pow(1 / (1 + intr), term));

    return (
      <Wrapper>
        <div
          className={
            wishlist ? "col-12 wishlistItem p-0" : "col-12 singleItem p-0"
          }
        >
          <div className="row m-0">
            <img
              src={Productdetails.mainUrl || Productdetails.img}
              width="100%"
              onError={this.onError}
              className={professionalpage ? "wishlistCardImg" : "productImg"}
            />
          </div>
          <div className="row m-0">
            <div
              className={
                wishlist
                  ? "col-12 p-0 wishlistCardContent"
                  : "col-12 p-0 cardContent"
              }
            >
              <div className="row m-0 d-flex justify-content-between">
                <div className="col-7 p-0 m-0">
                  <h6 className="itemTitle">
                    {Productdetails.productName || Productdetails.title}
                  </h6>
                </div>
                <div className="col-5 p-0 m-0 text-right">
                  <p className="itemPrice m-0">
                    {Productdetails.currency == "INR" ? (
                      <span id="currancySymbol">₹</span>
                    ) : Productdetails.currency == "USD" ? (
                      <span id="currancySymbol">$</span>
                    ) : (
                      Productdetails.currency
                    )}
                    <span id="price">{Productdetails.price}</span>
                  </p>
                  <p className="loanEst m-0">
                    ${Math.round(loanQuote)}/mo <img src={InfoIcon} />
                  </p>
                  <p className="m-0 down-payment">$0 cash down</p>
                  {/* <p className="loanEst m-0">{Productdetails.ships_for}</p> */}
                </div>
              </div>

              <div
                className="mx-0 d-flex align-items-end"
                style={{ paddingTop: "0.585vw", lineHeight: 1 }}
              >
                <p className="m-0 itemLoc">
                  {Productdetails.city
                    ? `${Productdetails.city}, ${Productdetails.countrySname}`
                    : `${Productdetails.place}`}
                </p>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            .ships_forPrice {
              font-size: 0.7vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw;
              color: ${THEME_COLOR};
              font-weight: 600;
              margin: 0.366vw 0 0 0;
            }
            .my-boats-title {
              color: #2a3656;
              font-size: 0.856vw;
              font-weight: 500;
              font-family: Museo-Sans !important;
              text-transform: capitalize;
            }
            .my-boats-price {
              color: #2a3656;
              font-size: 0.856vw;
              font-weight: 600;
              font-family: Museo-Sans !important;
            }
            .my-boats-location {
              color: #98a1b9;
              font-size: 0.749vw;
              text-transform: capitalize;
              font-weight: 600;
              font-family: Open Sans-SemiBold;
            }
            .down-payment {
              color: #98a1b9;
              font-size: 0.5208vw;
              font-weight: 600;
              font-family: Open Sans;
            }
            .singleItem {
              width: 100%;
              background-color: ${WHITE_COLOR};
              border-radius: 0.292vw;
              position: relative;
            }
            .wishlistItem {
              width: 100%;
              background-color: ${WHITE_COLOR};
              border-radius: 0.292vw;
              position: relative;
            }
            .wishlistCardImg {
              border-top-left-radius: 3px;
              border-top-right-radius: 3px;
              height: 9.516vw;
            }
            .productImg {
              height: ${window.location.pathname === "/" ||
              window.location.pathname === "/my-boats"
                ? "auto"
                : "14.635vw"};
              min-height: ${window.location.pathname === "/my-boats"
                ? "150px"
                : "unset"};

              border-top-left-radius: 3px;
              border-top-right-radius: 3px;
            }
            .wishlistCardContent {
              padding: 0.878vw 0.732vw !important;
              height: auto;
              display: flex;
              flex-direction: row-reverse;
              position: relative;
              height: 7.32vw;
            }
            .wishlistCardContent div {
              width: 100%;
              margin-right: 0.878vw;
            }
            .cardContent {
              padding: 0.878vw 0.732vw !important;
              position: relative;
              height: ${window.location.pathname === "/my-boats"
                ? "auto"
                : "6vw"};
            }
            .itemTitle {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 0.781vw;
              margin: 0;
              letter-spacing: 0.0219vw;
              font-family: "Open Sans-SemiBold" !important;
              text-transform: capitalize;
            }
            .itemLoc {
              color: ${GREY_VARIANT_1};
              font-size: 0.6955vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw;
              text-transform: capitalize;
              position: absolute;
              bottom: 0.878vw;
              left: 0.732vw;
            }
            .timePutOnAuction {
              display: flex;
              align-items: center;
              color: ${GREY_VARIANT_2};
              font-size: 0.6955vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw;
              text-transform: capitalize;
              // position: absolute;
              // bottom: 0.878vw;
              // right: 0.732vw;
            }
            .clock {
              height: 0.732vw;
              width: 0.732vw;
              margin-right: 0.22vw;
            }
            .itemPrice {
              line-height: 1;
            }
            .itemPrice #price {
              color: ${FONTGREY_COLOR_Dark};
              font-size: 0.99vw;
              font-family: "Open Sans-SemiBold" !important;
            }
            #currancySymbol {
              // font-size: 0.585vw;
              margin-right: 2px;
              position: relative;
              bottom: ${window.location.pathname === "/boats"
                ? "0.293vw"
                : "0.05vw"};
              font-family: Museo-Sans !important;
              font-weight: 500 !important;
              color: ${window.location.pathname === "/boats"
                ? THEME_COLOR
                : FONTGREY_COLOR_Dark};
              font-size: ${window.location.pathname === "/boats"
                ? "0.512vw"
                : "0.856vw"};
            }
            .loanEst {
              color: ${THEME_COLOR};
              font-size: 0.625vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw;
              text-transform: capitalize;
              padding-top: 0.2083vw;
            }
            .shippingIcon {
              width: 0.878vw;
              margin-right: 0.292vw;
              margin-bottom: 0.219vw;
            }
            .ships_forPrice {
              font-size: 0.658vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw;
              color: ${THEME_COLOR};
              margin: 0.366vw 0 0 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EngineCard;
