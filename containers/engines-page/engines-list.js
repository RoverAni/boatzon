// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// reusable component
import MasonaryLayout from "../../components/masonry-layout/masonry-layout";

import SelectInput from "../../components/input-box/select-input";
import {
  GREY_VARIANT_2,
  Wishlist_Icon,
  Saved_Blue_Icon,
  WHITE_COLOR,
  No_Boat_Post_Img,
  FONTGREY_COLOR,
} from "../../lib/config";
import { getCookie } from "../../lib/session";
import { unfollowPost, followPost } from "../../services/following";
import Model from "../../components/model/model";
import LoginPage from "../auth-modals/login/login-page";
import Router from "next/router";
import EngineCard from "./engine-card";

class EnginesList extends Component {
  state = {
    inputpayload: {},
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      AuthPass,
    });
  }

  handleWishlistPost = (event, item) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.state.AuthPass) {
      //   let srcImg = document
      //     .getElementById(item.postId)
      //     .src.replace("http://localhost:7122/", "/");
      //   console.log("fdufh-->srcImg", srcImg);
      //   if (!item.likeStatus || srcImg == `${Wishlist_Icon}`) {
      //     this.handleFollowPost(item.postId);
      //   } else if (item.likeStatus || srcImg == `${Saved_Blue_Icon}`) {
      //     this.handleUnfollowPost(item.postId);
      //   }
    } else {
      this.handleLogin();
    }
  };

  handleUnfollowPost = (postId) => {
    let payload = {
      postId: postId.toString(),
      label: "Photo",
    };
    unfollowPost(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            document.getElementById(postId).src = `${Wishlist_Icon}`;
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  handleFollowPost = (postId) => {
    let payload = {
      postId: postId.toString(),
      label: "Photo",
    };
    followPost(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            document.getElementById(postId).src = `${Saved_Blue_Icon}`;
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleEnginesDetails = (item) => {
    let pName = encodeURIComponent(
      item.productName ? item.productName : item.title
    );
    Router.push(`/engine-detail/${pName.toLowerCase()}?pid=${item.postId}`);
  };

  render() {
    const BoatsTypes = [
      { value: "Recommended", label: "Recommended" },
      { value: "Popular", label: "Popular" },
      { value: "Recent", label: "Recent" },
    ];

    const PostedTypes = [
      { value: "24hr", label: "last 24 hr" },
      { value: "15days", label: "last 15 days" },
      { value: "30days", label: "last 30 days" },
    ];

    const { myboats, boatsPage, sortBy, EnginesList } = this.props;

    return (
      <Wrapper>
        <div className={myboats ? "container p-md-0" : "container p-md-0"}>
          <div
            className={
              myboats && window.location.pathname === "/my-boats"
                ? "screenWidth mx-auto"
                : "w-100 mx-auto"
            }
          >
            {/* <div className={myboats ? "w-100 mx-auto" : "w-100 mx-auto"}> */}

            <div className="d-flex justify-content-end align-items-center pb-2 sortFilter_Sec">
              {/* <div className="d-flex align-items-center">
                <p className="sortLabel">Sort by</p>
                <div className="SelectInput">
                  <SelectInput
                    noboxstyle={true}
                    placeholder="Recommended"
                    options={BoatsTypes}
                    onChange={this.props.handleOnSelectInput(`boatTypes`)}
                  />
                </div>
              </div> */}
              <div className="d-flex align-items-center">
                <p className="postedLabel">Posted within</p>
                <div className="SelectInput">
                  <SelectInput
                    noboxstyle={true}
                    placeholder="select"
                    value={this.props.postedWithin}
                    options={PostedTypes}
                    onChange={this.props.handleOnSelectInput(`postedWithin`)}
                  />
                </div>
              </div>
            </div>
            {EnginesList && EnginesList.length > 0 ? (
              <div className={myboats ? "col-12 p-0" : "col-12 px-2"}>
                <div className="row m-0">
                  <MasonaryLayout breakpointCols={this.props.breakpointCols}>
                    {EnginesList &&
                      EnginesList.map((item, index) => (
                        <div className="col-12 p-0">
                          <div key={index} className="boatCard w-100">
                            <div
                              onClick={this.handleEnginesDetails.bind(
                                this,
                                item
                              )}
                            >
                              <EngineCard
                                Productdetails={item}
                                wishlist={this.props.productpage ? true : false}
                                shipping={
                                  this.props.myboats &&
                                  item.category == "Products"
                                    ? true
                                    : false
                                }
                              />
                              <img
                                src={Wishlist_Icon}
                                id={item.postId}
                                className="boatswishlistIcon"
                                onClick={(e) =>
                                  this.handleWishlistPost(e, item)
                                }
                              ></img>
                            </div>
                          </div>
                        </div>
                      ))}
                  </MasonaryLayout>
                </div>
              </div>
            ) : (
              <div className="noFavorites">
                <div className="text-center">
                  <img
                    src={No_Boat_Post_Img}
                    className="noFavFollowingImg"
                  ></img>
                  <p className="noFavoritesMsg">No Engines Post</p>
                </div>
              </div>
            )}
          </div>
        </div>

        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>
        <style jsx>
          {`
            .boatCard {
              cursor: pointer;
              box-shadow: 0 2px 4px 0px rgba(48, 56, 97, 0.1);
              position: relative;
              min-height: ${this.props.myboats ? "auto" : "20.417vw"};
              border-radius: 3px;
              margin-bottom: 0.878vw !important;
              margin-right: 0.878vw !important;
              background: ${WHITE_COLOR};
            }
            .boatswishlistIcon {
              position: absolute;
              bottom: 0.781vw;
              right: 0.732vw;
              width: 0.776vw;
              height: 0.729vw;
              z-index: 1 !important;
            }
            .clipLoader {
              position: absolute;
              bottom: 0.732vw;
              right: 0.732vw;
            }
            .SelectInput {
              margin: 0 1.464vw 0 0.366vw;
              height: auto;
            }
            .sortLabel,
            .postedLabel {
              font-size: 0.768vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0;
              letter-spacing: 0.0219vw !important;
            }
            :global(.sortFilter_Sec .SelectInput .blueChevronIcon) {
              width: 0.586vw;
              margin-top: 0.207vw;
            }
            :global(.noFavFollowingImg) {
              width: 14.641vw;
              object-fit: cover;
            }
            :global(.noFavorites) {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 32.942vw;
              text-align: center;
              background: ${WHITE_COLOR};
            }
            :global(.noFavoritesMsg) {
              max-width: 80%;
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EnginesList;
