import React, { Component } from "react";
import CustomBreadCrumbs from "../../components/breadcrumbs/breadcrumbs";
import Wrapper from "../../hoc/Wrapper";
import * as env from "../../lib/config";
import EnginesFilter from "./engines-filters";
import { connect } from "react-redux";
import { getCurrentLocation } from "../../redux/actions/location/location";
import EnginesList from "./engines-list";
import EnginesGrids from "./engine-grids";
import debounce from "lodash.debounce";
import { isEmpty } from "../../lib/global";

class EnginesPage extends Component {
  state = {
    condition: "",
    radiusDropdown: false,
    inputpayload: {},
    yearDropdown: false,
    yearsValue: [1970, 2017],
    minYears: 1970,
    maxYears: 2017,
    fromYear: 1970,
    toYear: 2017,
    yearsRange: [1970, 2017],
    horsePowerDropdown: false,
    horsePowerValue: [100, 1700],
    minHorsePower: 100,
    maxHorsePower: 1700,
    fromHorsePower: 100,
    toHorsePower: 1700,
    horsePowerRange: [100, 1700],
    payload: {
      category: "Engines",
    },
    breadcrumbsData: ["Engines"],
  };

  componentDidMount() {
    if (!isEmpty(this.props.urlString)) {
      const {
        condition,
        manufactor,
        modelName,
        hasWarranty,
        postedWithin,
        distanceMax,
        fromLoan,
        toLoan,
        fromPrice,
        toPrice,
        fromYear,
        toYear,
        fromHorsePower,
        toHorsePower,
        engineType,
      } = { ...this.props.urlString };

      // price payload
      let tempPayload = { ...this.state.inputpayload };
      tempPayload.minPrice = fromPrice;
      tempPayload.maxPrice = toPrice;

      // horsePower payload
      let temphorsePowerValue = [];
      fromHorsePower
        ? temphorsePowerValue.push(parseInt(fromHorsePower, 10))
        : this.state.minHorsePower;
      toHorsePower
        ? temphorsePowerValue.push(parseInt(toHorsePower, 10))
        : this.state.maxHorsePower;

      // year payload
      let tempYearValue = [];
      fromYear
        ? tempYearValue.push(parseInt(fromYear, 10))
        : this.state.minYears;
      toYear ? tempYearValue.push(parseInt(toYear, 10)) : this.state.maxYears;

      // Manufacturer payload
      let manufactureIdList = manufactor && manufactor.split(",");
      let tempPay =
        manufactureIdList?.length > 0
          ? manufactureIdList.map((data) => {
              return { label: data };
            })
          : "";

      // model payload
      let modelNameList = modelName?.split(",");
      let tempModelPayload =
        modelNameList?.length > 0
          ? modelNameList.map((data) => {
              return { label: data };
            })
          : "";

      // postedWithin payload
      let tempPostedWithin =
        postedWithin == "24hr"
          ? { value: postedWithin, label: "last 24 hr" }
          : postedWithin == "15days"
          ? { value: postedWithin, label: "last 15 days" }
          : postedWithin == "30days"
          ? { value: postedWithin, label: "last 30 days" }
          : "";

      // budget payload
      const FinanceTypeOptions = [
        { value: "0000000000", label: "All" },
        { value: 149, label: "Engines under $149/mo" },
        { value: 299, label: "Engines under $299/mo" },
        { value: 499, label: "Engines under $499/mo" },
        { value: 699, label: "Engines under $699/mo" },
        { value: 999, label: "Engines under $999/mo" },
        { value: 1500, label: "Engines under $1500/mo" },
        { value: 2000, label: "Engines under $2000/mo" },
        { value: 2500, label: "Engines under $2500/mo" },
      ];

      let tempBudget =
        FinanceTypeOptions?.length > 0 && toLoan
          ? FinanceTypeOptions.filter((data) => data.value == toLoan)
          : "";

      // breadcrumbs payload
      let tempbreadcrumbsData = [...this.state.breadcrumbsData];
      let value =
        manufactureIdList?.length > 0
          ? manufactureIdList[manufactureIdList.length - 1]
          : null;
      tempbreadcrumbsData[1] = value;

      this.setState(
        {
          inputpayload: { ...tempPayload },
          condition: condition,
          manufactor: tempPay,
          engine_model: tempModelPayload,
          valueRadius: distanceMax ? distanceMax : 0,
          Budget: tempBudget,
          minPrice: fromPrice ? fromPrice : "",
          maxPrice: toPrice ? toPrice : "",
          yearsValue: tempYearValue,
          minYears: fromYear ? fromYear : this.state.minYears,
          maxYears: toYear ? toYear : this.state.maxYears,
          fromYear: fromYear ? parseInt(fromYear, 10) : this.state.minYears,
          toYear: toYear ? parseInt(toYear, 10) : this.state.maxYears,
          yearsRange: tempYearValue,
          horsePowerValue: temphorsePowerValue,
          minHorsePower: fromHorsePower
            ? fromHorsePower
            : this.state.minHorsePower,
          maxHorsePower: toHorsePower ? toHorsePower : this.state.maxHorsePower,
          fromHorsePower: fromHorsePower
            ? parseInt(fromHorsePower, 10)
            : this.state.minHorsePower,
          toHorsePower: toHorsePower
            ? parseInt(toHorsePower, 10)
            : this.state.maxHorsePower,
          horsePowerRange: temphorsePowerValue,
          hasWarranty: hasWarranty,
          engineType: { value: engineType, label: engineType },
          breadcrumbsData: [...tempbreadcrumbsData],
          postedWithin: { ...tempPostedWithin },
        },
        () => {
          let apiPayload = { ...this.state.payload, ...this.props.urlString };

          if (fromHorsePower && toHorsePower) {
            apiPayload.horsePower = {
              fromHorsePower,
              toHorsePower,
            };
            delete apiPayload.fromHorsePower;
            delete apiPayload.toHorsePower;
          }
          if (fromLoan && toLoan) {
            apiPayload.loan = {
              fromLoan,
              toLoan,
            };
            delete apiPayload.fromLoan;
            delete apiPayload.toLoan;
          }
          if (fromYear && toYear) {
            apiPayload.year = {
              fromYear,
              toYear,
            };
            delete apiPayload.fromYear;
            delete apiPayload.toYear;
          }
          if (fromPrice && toPrice) {
            apiPayload.priceRange = {
              fromPrice,
              toPrice,
            };
            delete apiPayload.fromPrice;
            delete apiPayload.toPrice;
          }

          if (hasWarranty) {
            apiPayload.hasWarranty =
              hasWarranty == "All Options"
                ? ""
                : hasWarranty == "With a Warranty"
                ? "Yes"
                : hasWarranty == "No Warranty"
                ? "No"
                : null;
          }
          this.setState(
            {
              payload: apiPayload,
            },
            () => {
              this.props.handlePostProductsAPI(this.state.payload);
            }
          );
        }
      );
    }
  }

  handleTypeOnSelect = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        if (name == "hasWarranty") {
          apiPayload[`${name}`] =
            this.state.hasWarranty == "All Options"
              ? ""
              : this.state.hasWarranty == "With a Warranty"
              ? "Yes"
              : this.state.hasWarranty == "No Warranty"
              ? "No"
              : null;
          this.props.handleUrlParams(`${name}`, this.state[name]); // function to add filterdata in url params
        } else {
          apiPayload[`${name}`] = this.state[name];
          this.props.handleUrlParams(`${name}`, this.state[name]); // function to add filterdata in url params
        }
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  handleChangeCondition = (conditionName) => {
    this.setState(
      {
        condition: conditionName,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`condition`] = this.state.condition;
        this.props.handleUrlParams(`condition`, this.state.condition); // function to add filterdata in url params

        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  /** function to toggle radius dropdown */
  radiusDropdownOpen = () => {
    this.props.currentLocation
      ? this.setState({
          radiusDropdown: !this.state.radiusDropdown,
        })
      : this.props.dispatch(getCurrentLocation());
  };

  /** function to set radius value on slide */
  onChangeRadius = (event, value) => {
    this.setState(
      {
        valueRadius: value,
      },
      () => {
        this.handleApiCallforRadius(this.state.valueRadius);
      }
    );
  };

  handleApiCallforRadius = debounce((value) => {
    let urlParams = {};
    urlParams[`distanceMax`] = value;
    urlParams[`latitude`] = this.props.currentLocation.latitude;
    urlParams[`longitude`] = this.props.currentLocation.longitude;
    let apiPayload = { ...this.state.payload, ...urlParams };
    this.props.handleUrlParams("distanceMax", urlParams); // function to add filterdata in url params
    this.setState(
      {
        payload: apiPayload,
      },
      () => {
        this.props.handlePostProductsAPI(this.state.payload); // API call
      }
    );
  }, 500);

  /** function to toggle price dropdown */
  priceDropdownOpen = () => {
    if (!this.state.minPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.minPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    if (!this.state.maxPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.maxPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    this.setState({
      priceDropdown: !this.state.priceDropdown,
    });
  };

  /** function to handle priceInput */
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  /** function to set min and max price */
  handlePriceRange = () => {
    let { minPrice, maxPrice } = this.state.inputpayload;
    this.setState(
      {
        minPrice,
        maxPrice,
        priceDropdown: !this.state.priceDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`priceRange`] = {};
        apiPayload[`priceRange`].fromPrice = this.state.minPrice;
        apiPayload[`priceRange`].toPrice = this.state.maxPrice;

        this.props.handleUrlParams("priceRange", apiPayload.priceRange); // function to add filterdata in url params
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            this.props.handlePostProductsAPI(this.state.payload); // API call
          }
        );
      }
    );
  };

  /** Function to inputpayload for selectInput */
  handleOnSelectInput = (name) => (event, { option }) => {
    if (
      event != null &&
      option != undefined &&
      (name == "manufactor" || name == "engine_model") &&
      this.state[name]?.length > 0
    ) {
      let tempPayload = this.state[name];
      let elementId = tempPayload.findIndex(
        (data) => data.label == option.label
      );
      if (elementId != -1) {
        tempPayload.splice(elementId, 1);
      } else {
        tempPayload = event;
      }
      this.setState(
        {
          [name]: tempPayload,
        },
        () => {
          this.handleAPIPayload(event, name);
        }
      );
    } else {
      this.setState(
        {
          [name]: event,
        },
        () => {
          this.handleAPIPayload(event, name);
        }
      );
    }
  };

  handleAPIPayload = (event, name) => {
    if (name == "manufactor") {
      let tempPayload = [...this.state.breadcrumbsData];
      if (this.state.manufactor != null) {
        let value =
          this.state.manufactor?.length > 0
            ? this.state.manufactor[this.state.manufactor.length - 1]?.label
            : null;
        tempPayload[1] = value;
      } else {
        tempPayload.pop();
      }

      this.setState(
        {
          engine_model: {},
          breadcrumbsData: [...tempPayload],
        },
        () => {
          if (this.props.urlString && this.props.urlString.modelName) {
            this.props.handleUrlParams("modelName", ""); // function to add filterdata in url params
          }
        }
      );
    }
    if (event instanceof Array || this.state[name] == null) {
      let apiPayload = { ...this.state.payload };
      let labelArr =
        this.state[name] != null &&
        this.state[name] &&
        this.state[name].length > 0
          ? this.state[name].map((data) => data.label)
          : "";
      apiPayload[name] = labelArr.toString();
      this.props.handleUrlParams(`${name}`, labelArr.toString()); // function to add filterdata in url params
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    } else {
      let apiPayload = { ...this.state.payload };
      if (name == "Budget") {
        apiPayload[`loan`] = {};
        apiPayload[`loan`].fromLoan = 0;
        apiPayload[`loan`].toLoan = this.state[name].value;
        let urlPayload = this.state[name].label == "All" ? "" : apiPayload.loan;
        this.props.handleUrlParams("loan", urlPayload); // function to add filterdata in url params
      } else {
        let payloadName = name == "engine_model" ? "modelName" : name;
        apiPayload[payloadName] =
          name == "postedWithin"
            ? this.handlePostedWithin(this.state[name].value)
            : name == "engine_model"
            ? this.state[name].label
            : this.state[name].value;
        this.props.handleUrlParams(
          `${payloadName}`,
          name == "engine_model"
            ? this.state[name].label
            : this.state[name].value
        ); // function to add filterdata in url params
      }
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    }
  };

  /** function to set API postedWithin payload  */
  handlePostedWithin = (value) => {
    switch (value) {
      case "24hr":
        return "1";
        break;
      case "15days":
        return "2";
        break;
      case "30days":
        return "3";
        break;
    }
  };

  /** function to toggle Year dropdown */
  yearDropdownOpen = () => {
    if (
      this.state.yearDropdown == true &&
      (this.state.fromYear || this.state.toYear)
    ) {
      this.setState({
        minYears: this.state.fromYear,
        maxYears: this.state.toYear,
        yearsValue: this.state.yearsRange,
      });
    } else if (
      this.state.yearDropdown == true &&
      (!this.state.fromYear || !this.state.toYear)
    ) {
      this.setState({
        minYears: 1970,
        maxYears: 2017,
        yearsValue: [1970, 2017],
      });
    }
    this.setState({
      yearDropdown: !this.state.yearDropdown,
    });
  };

  /** function to set min and max Year */
  handleYearsRange = () => {
    this.setState(
      {
        yearDropdown: !this.state.yearDropdown,
        fromYear: this.state.minYears ? parseInt(this.state.minYears, 10) : 0,
        toYear: this.state.maxYears ? parseInt(this.state.maxYears, 10) : 0,
      },
      () => {
        this.setState(
          {
            yearsRange: [this.state.fromYear, this.state.toYear],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`year`] = {};
            apiPayload[`year`].fromYear = this.state.fromYear;
            apiPayload[`year`].toYear = this.state.toYear;
            this.props.handleUrlParams("year", apiPayload.year); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set Year value on slide */
  handleSliderYearsRange = (event, newValue) => {
    this.setState(
      {
        yearsValue: newValue,
      },
      () => {
        this.handleYearMinMaxValues(this.state.yearsValue);
      }
    );
  };

  /** function to set min and max Year value from slider value */
  handleYearMinMaxValues = (value) => {
    let minYears = value[0];
    let maxYears = value[1];
    this.setState({
      minYears,
      maxYears,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeYearInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minYears") {
      this.setState(
        {
          maxYears: this.state.maxYears,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minYears, this.state.maxYears];
          this.setState({
            yearsValue: newValue,
          });
        }
      );
    } else if (name == "maxYears") {
      this.setState(
        {
          minYears: this.state.minYears,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minYears, this.state.maxYears];
          this.setState({
            yearsValue: newValue,
          });
        }
      );
    }
  };

  // -------------------------
  /** function to toggle Horse Power dropdown */
  horsePowerDropdownOpen = () => {
    if (
      this.state.horsePowerDropdown == true &&
      (this.state.fromHorsePower || this.state.toHorsePower)
    ) {
      this.setState({
        minHorsePower: this.state.fromHorsePower,
        maxHorsePower: this.state.toHorsePower,
        horsePowerValue: this.state.horsePowerRange,
      });
    } else if (
      this.state.horsePowerDropdown == true &&
      (!this.state.fromHorsePower || !this.state.toHorsePower)
    ) {
      this.setState({
        minHorsePower: 100,
        maxHorsePower: 1700,
        horsePowerValue: [100, 1700],
      });
    }
    this.setState({
      horsePowerDropdown: !this.state.horsePowerDropdown,
    });
  };

  /** function to set min and max HorsePower */
  handleHorsePowerRange = () => {
    this.setState(
      {
        horsePowerDropdown: !this.state.horsePowerDropdown,
        fromHorsePower: this.state.minHorsePower
          ? parseInt(this.state.minHorsePower, 10)
          : 0,
        toHorsePower: this.state.maxHorsePower
          ? parseInt(this.state.maxHorsePower, 10)
          : 0,
      },
      () => {
        this.setState(
          {
            horsePowerRange: [
              this.state.fromHorsePower,
              this.state.toHorsePower,
            ],
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`horsePower`] = {};
            apiPayload[`horsePower`].fromHorsePower = this.state.fromHorsePower;
            apiPayload[`horsePower`].toHorsePower = this.state.toHorsePower;
            this.props.handleUrlParams("horsePower", apiPayload.horsePower); // function to add filterdata in url params
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                this.props.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  /** function to set HorsePower value on slide */
  handleSliderHorsePowerRange = (event, newValue) => {
    this.setState(
      {
        horsePowerValue: newValue,
      },
      () => {
        this.handleHorsePowerMinMaxValues(this.state.horsePowerValue);
      }
    );
  };

  /** function to set min and max HorsePower value from slider value */
  handleHorsePowerMinMaxValues = (value) => {
    let minHorsePower = value[0];
    let maxHorsePower = value[1];
    this.setState({
      minHorsePower,
      maxHorsePower,
    });
  };

  /** function to set slider value on min and max value input change */
  handleOnchangeHorsePowerInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minHorsePower") {
      this.setState(
        {
          maxHorsePower: this.state.maxHorsePower,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minHorsePower, this.state.maxHorsePower];
          this.setState({
            horsePowerValue: newValue,
          });
        }
      );
    } else if (name == "maxHorsePower") {
      this.setState(
        {
          minHorsePower: this.state.minHorsePower,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minHorsePower, this.state.maxHorsePower];
          this.setState({
            horsePowerValue: newValue,
          });
        }
      );
    }
  };

  handleManufacturerFilter = (data) => {
    if (data.value != "Trolling Motors" || data.value != "Engine Parts") {
      let apiPayload = { ...this.state.payload };
      apiPayload[`manufactor`] = data.value; // value is manufacturer name here
      this.props.handleUrlParams("manufactor", data.value); // function to add filterdata in url params
      this.setState(
        {
          payload: apiPayload,
        },
        () => {
          this.props.handlePostProductsAPI(this.state.payload);
        }
      );
    }
  };

  render() {
    const { breadcrumbsData } = this.state;
    return (
      <Wrapper>
        <div className="row m-0 align-items-center ProfessionalsPageSec">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 my-3 py-3">
              <div className="row m-0">
                <CustomBreadCrumbs
                  boatsPage={true}
                  breadcrumbs={breadcrumbsData}
                  separator={
                    <img
                      src={env.Chevron_Right_Darkgrey}
                      width="4"
                      style={{ margin: "0 3px" }}
                    ></img>
                  }
                />
              </div>
              <div className="row m-0 align-items-center justify-content-between my-3">
                <div className="col p-0">
                  <h6 className="Heading">
                    Engines{" "}
                    <span>
                      {this.props.filterPostData &&
                      this.props.filterPostData.length > 0
                        ? `(${this.props.filterPostData.length})`
                        : ""}
                    </span>
                  </h6>
                </div>
              </div>
              <div>
                <EnginesGrids
                  handleManufacturerFilter={this.handleManufacturerFilter}
                />
              </div>
              <div className="row m-0 mt-3">
                <EnginesFilter
                  manufactor={this.state.manufactor}
                  condition={this.state.condition}
                  handleChangeCondition={this.handleChangeCondition}
                  handleTypeOnSelect={this.handleTypeOnSelect}
                  selectedYear={this.state.selectedYear}
                  radiusDropdownOpen={this.radiusDropdownOpen}
                  radiusDropdown={this.state.radiusDropdown}
                  onChangeRadius={this.onChangeRadius}
                  valueRadius={this.state.valueRadius}
                  priceDropdownOpen={this.priceDropdownOpen}
                  handleOnchangeInput={this.handleOnchangeInput}
                  handlePriceRange={this.handlePriceRange}
                  inputpayload={this.state.inputpayload}
                  priceDropdown={this.state.priceDropdown}
                  hasWarranty={this.state.hasWarranty}
                  engineData={this.props.engineData}
                  handleOnSelectInput={this.handleOnSelectInput}
                  engineType={this.state.engineType}
                  engineFuelType={this.state.engineFuelType}
                  saleBy={this.state.saleBy}
                  Budget={this.state.Budget}
                  horsePower={this.state.horsePower}
                  engine_model={this.state.engine_model}
                  yearDropdown={this.state.yearDropdown}
                  yearDropdownOpen={this.yearDropdownOpen}
                  handleYearsRange={this.handleYearsRange}
                  handleSliderYearsRange={this.handleSliderYearsRange}
                  yearsValue={this.state.yearsValue}
                  handleOnchangeYearInput={this.handleOnchangeYearInput}
                  maxYears={this.state.maxYears}
                  minYears={this.state.minYears}
                  horsePowerDropdown={this.state.horsePowerDropdown}
                  horsePowerDropdownOpen={this.horsePowerDropdownOpen}
                  handleHorsePowerRange={this.handleHorsePowerRange}
                  handleSliderHorsePowerRange={this.handleSliderHorsePowerRange}
                  horsePowerValue={this.state.horsePowerValue}
                  handleOnchangeHorsePowerInput={
                    this.handleOnchangeHorsePowerInput
                  }
                  maxHorsePower={this.state.maxHorsePower}
                  minHorsePower={this.state.minHorsePower}
                />
              </div>
              <div className="row m-0 BoatListSec justify-content-center position-relative">
                {/* Engines List Module */}
                <EnginesList
                  boatsPage={true}
                  EnginesList={this.props.filterPostData}
                  handleOnSelectInput={this.handleOnSelectInput}
                  postedWithin={this.state.postedWithin}
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .ProfessionalsPageSec {
              background: ${env.BG_LightGREY_COLOR};
            }
            .Heading {
              margin: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 1.25vw;
              color: ${env.FONTGREY_COLOR};
            }
            .Heading span {
              font-size: 0.878vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${env.GREY_VARIANT_3};
            }
            .BoatListSec {
              padding: 0.732vw 0;
              background: ${env.WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    apiCallLoader: state.apiCallLoader,
  };
};

export default connect(mapStateToProps)(EnginesPage);
