import React, { PureComponent } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREEN_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  THEME_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_6,
  View_Transacrtion_Grey_Icon,
  GREY_VARIANT_9,
  GREY_VARIANT_3,
  Orange_Color,
  Sold_Grey_Icon,
  Available_Green_Icon,
  Chat_Icon_Green,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import { BuyingBoatsList } from "../../fixtures/buying/buying";

class BuyingBoats extends PureComponent {
  state = {
    inputpayload: {},
    peopleList: true,
    activeMsgId: 0,
  };
  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: temppayload,
      });
    }
  };

  handleActiveItem = (data) => {
    console.log("fsds", data);
    this.setState(
      {
        activeMsgId: data.id,
      },
      () => {
        // this.props.handleSelectedMsg(data);
      }
    );
  };

  render() {
    const { activeMsgId } = this.state;
    return (
      <Wrapper>
        <div className="BuyingBoatsPage my-3">
          {BuyingBoatsList &&
            BuyingBoatsList.map((data, index) => (
              <div
                className={"col-12 ChatListCard p-0"}
                onClick={this.handleActiveItem.bind(this, data)}
              >
                <div className="section">
                  <div className="row m-0 align-items-center" key={index}>
                    <div className="col p-0 LeftSec">
                      <div className="row m-0 align-items-center">
                        <div className="col-4 p-0 boatprofileSec">
                          <div className="row m-0 d-flex align-items-center">
                            <div className="col-3 p-0">
                              <img
                                src={data.boat_img}
                                className="BoatImg"
                              ></img>
                            </div>
                            <div className="col-9 p-0 boatInfoSec">
                              <h6 className="boatName">{data.boat_name}</h6>
                              <p className="boatSellerName">
                                <img
                                  src={data.boat_seller_pic}
                                  className="sellerPic"
                                ></img>{" "}
                                <span>{data.boat_seller_name}</span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col p-0">
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 availabilty_status">
                              {data.boat_availabilty_status == "Available" ? (
                                <div className="available_btn">
                                  <ButtonComp>
                                    <img
                                      src={Available_Green_Icon}
                                      className="availableIcon"
                                    ></img>
                                    Available
                                  </ButtonComp>
                                </div>
                              ) : data.boat_availabilty_status == "Sold" ? (
                                <div className="sold_btn">
                                  <ButtonComp>
                                    <img
                                      src={Sold_Grey_Icon}
                                      className="soldIcon"
                                    ></img>
                                    Sold
                                  </ButtonComp>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="col-2 p-0 priceSec">
                              <p className="boatPrice">{data.boat_price}</p>
                            </div>
                            <div className="col-2 p-0 view_btn">
                              <ButtonComp>
                                <img
                                  src={View_Transacrtion_Grey_Icon}
                                  className="viewIcon"
                                ></img>
                                View
                              </ButtonComp>
                            </div>
                            <div className="col-2 p-0 messages_btn">
                              <ButtonComp>
                                <img
                                  src={Chat_Icon_Green}
                                  className="chatIcon"
                                ></img>
                                Messages
                              </ButtonComp>
                            </div>
                            <div className="col-3 p-0 getPreApproved_btn text-center">
                              {data.boat_availabilty_status == "Sold" ? (
                                ""
                              ) : (
                                <div>
                                  <ButtonComp>$ Get Pre-Approved</ButtonComp>
                                  <p className="mothlyPlan">$498/mo</p>
                                </div>
                              )}
                            </div>
                            <div className="col p-0 getInsurance_btn text-center">
                              {data.boat_availabilty_status == "Sold" ? (
                                ""
                              ) : (
                                <div>
                                  <ButtonComp>$ Get Insurance</ButtonComp>
                                  <p className="mothlyPlan">From $46/mo</p>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-1 p-0 archive_btn">
                      <p>Archive</p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>

        <style jsx>
          {`
            .availableIcon {
              width: 0.658vw;
              margin-right: 0.219vw;
            }
            .soldIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .mothlyPlan {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
              position: absolute;
              bottom: -1.024vw;
              left: 50%;
              width: 100%;
              transform: translate(-50%);
            }
            .priceSec {
              max-width: 5.124vw;
            }
            .chatIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .viewIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .LeftSec {
              max-width: 95%;
            }
            .boatprofileSec {
              max-width: 13.909vw;
            }
            .archive_btn {
              max-width: 3.66vw;
              margin: 0 0 0 auto;
            }
            .archive_btn p {
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize;
              font-size: 0.658vw;
              margin: 0;
              text-align: right;
            }
            .ChatListCard {
              cursor: pointer;
            }
            .ChatListCard:hover {
              background: ${GREY_VARIANT_6};
            }
            .ChatListCard:last-child .section {
              border: none;
            }
            .section {
              padding: 0.878vw 0;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .BoatImg {
              width: 100%;
              height: 2.562vw;
              object-fit: cover;
              border-radius: 0.366vw;
            }
            .boatInfoSec {
              padding: 0 0 0 0.512vw !important;
            }
            .boatName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              margin: 0 0 0.219vw 0;
            }
            .boatPrice {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0 0 0 0.512vw;
            }
            .boatSellerName {
              display: flex;
              align-items: center;
              margin: 0;
            }
            .sellerPic {
              width: 0.951vw;
            }
            .boatSellerName span {
              font-size: 0.607vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              padding: 0 0 0 0.366vw;
            }
            .availabilty_status {
              max-width: 4.758vw;
            }
            :global(.available_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
              border: 0.0732vw solid ${GREEN_COLOR};
            }
            :global(.available_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.658vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.available_btn button:focus),
            :global(.available_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.available_btn button:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.sold_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.512vw 0;
              background: ${GREY_VARIANT_9};
              color: ${GREY_VARIANT_2};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.sold_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.658vw;
              line-height: 1;
            }
            :global(.sold_btn button:focus),
            :global(.sold_btn button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.sold_btn button:hover) {
              background: ${GREY_VARIANT_9};
            }
            .view_btn {
              max-width: 3.66vw;
              margin: 0 0.512vw 0 0;
            }
            :global(.view_btn button) {
              width: 100%;
              min-width: fit-content;
              padding: 0.512vw;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.view_btn button span) {
              font-family: "Museo-Sans" !important;
              width: fit-content;
              font-size: 0.658vw;
              line-height: 1;
            }
            :global(.view_btn button:focus),
            :global(.view_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.view_btn button:hover) {
              background: ${GREY_VARIANT_3};
            }
            .messages_btn {
              max-width: 5.124vw;
            }
            :global(.messages_btn button) {
              width: 100%;
              min-width: fit-content;
              padding: 0.512vw 0;
              background: ${WHITE_COLOR};
              color: ${GREEN_COLOR};
              margin: 0;
              border: 0.0732vw solid ${GREEN_COLOR};
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.messages_btn button span) {
              font-family: "Museo-Sans" !important;
              width: fit-content;
              font-size: 0.658vw;
              line-height: 1;
            }
            :global(.messages_btn button:focus),
            :global(.messages_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.messages_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .getPreApproved_btn {
              max-width: 7.686vw;
              margin: 0 0.366vw;
              position: relative;
            }
            :global(.getPreApproved_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.512vw 0;
              background: ${Orange_Color};
              color: ${WHITE_COLOR};
              margin: 0 0 0.366vw 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.getPreApproved_btn button span) {
              font-family: "Museo-Sans" !important;
              width: fit-content;
              font-size: 0.658vw;
              line-height: 1;
            }
            :global(.getPreApproved_btn button:focus),
            :global(.getPreApproved_btn button:active) {
              background: ${Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.getPreApproved_btn button:hover) {
              background: ${Orange_Color};
            }
            .getInsurance_btn {
              max-width: 6.222vw;
              position: relative;
            }
            :global(.getInsurance_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.512vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0 0 0.366vw 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.getInsurance_btn button span) {
              font-family: "Museo-Sans" !important;
              width: fit-content;
              font-size: 0.658vw;
              line-height: 1;
            }
            :global(.getInsurance_btn button:focus),
            :global(.getInsurance_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.getInsurance_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default BuyingBoats;
