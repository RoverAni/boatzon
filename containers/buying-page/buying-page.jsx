import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR,
  Archive_Blue_Filled_Icon,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import BuyingBoats from "./buying-boats";
import ButtonComp from "../../components/button/button";

class BuyingPage extends Component {
  state = {
    openPage: false,
  };

  componentDidMount() {
    this.setState({
      openPage: true,
    });
  }
  render() {
    const Boats = <BuyingBoats />;
    const Products = <h1>Products</h1>;
    const { openPage } = this.state;

    return (
      <Wrapper>
        {openPage ? (
          <div className="BuyingPage">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0 py-3">
                <div className="col-11 p-0 mx-auto">
                  <div className="row mx-0 py-4 justify-content-center PageContent">
                    <div className="col-12 paddingXThree">
                      <div className="d-flex align-items-center justify-content-between">
                        <h6 className="heading">Buying</h6>
                        <div className="archive_btn">
                          <ButtonComp>
                            <img
                              src={Archive_Blue_Filled_Icon}
                              className="archiveIcon"
                            ></img>
                            View Archive
                          </ButtonComp>
                        </div>
                      </div>
                      <div className="my-2">
                        <Tabs
                          followingsmalltabs={true}
                          tabs={[
                            {
                              label: `Boats`,
                            },
                            {
                              label: `Products`,
                            },
                          ]}
                          tabcontent={[
                            { content: Boats },
                            { content: Products },
                          ]}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}

        <style jsx>
          {`
            .BuyingPage {
              background: ${BG_LightGREY_COLOR};
            }
            .PageContent {
              background: ${WHITE_COLOR};
              max-width: 95%;
              margin: 0 auto !important;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
            .archive_btn {
              float: right;
            }
            :global(.archive_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.archive_btn button span) {
              font-family: "Open Sans-Semibold" !important;
              font-size: 0.732vw;
              line-height: 1;
              width: fit-content;
            }
            :global(.archive_btn button:focus),
            :global(.archive_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.archive_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .archiveIcon {
              width: 0.732vw;
              margin-top: 0.146vw;
              margin-right: 0.366vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default BuyingPage;
