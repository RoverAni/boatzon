import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import {
  Search_Blue_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  Dark_Blue_Color,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
  Advice_Blue_Icon_Tab,
  NoDiscussionFollowing_Img,
  Project_Blue_Icon_Tab,
  GREY_VARIANT_3,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import ButtonComp from "../../components/button/button";
import Pagination from "../../hoc/divPagination";
import PageLoader from "../../components/loader/page-loader";
import {
  findDayAgo,
  formatDate,
} from "../../lib/date-operation/date-operation";
import { unfollowDiscussion } from "../../services/following";
import debounce from "lodash.debounce";


class FollowingDiscussions extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchKey: "",
      postedWithin: "",
    };
    this.divRef = React.createRef();
  }

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      this.setState(
        {
          [name]: inputControl,
        },
        () => {
          this.props.handleFollowingDiscussion(
            10,
            page,
            true,
            this.state.searchKey,
            this.state.postedWithin.value
          );
        }
      );
    }
  };

  searchItem = (event) => {
    let inputControl = event.target;
    this.setState(
      {
        searchKey: inputControl.value,
      },
      () => {
        this.handleSearchApiCall(this.state.searchKey);
      }
    );
  };

  handleSearchApiCall = debounce((value) => {
    this.props.handleFollowingDiscussion(
      10,
      0,
      true,
      value,
      this.state.postedWithin.value,
      true
    );
  }, 500);

  handleUnfollowDiscussion = (discussionId) => {
    let payload = {
      discussionId: discussionId.toString(),
    };

    unfollowDiscussion(payload)
      .then((res) => {
        console.log("resFollow", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.props.handleFollowingDiscussion(
              10,
              0,
              true,
              this.state.searchKey,
              this.state.postedWithin.value,
              true
            );
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  getList = (page = 0) => {
    return new Promise(async (res, rej) => {
      try {
        await this.props.handleFollowingDiscussion(
          10,
          page,
          false,
          this.state.searchKey,
          this.state.postedWithin.value
        );
        res();
      } catch (e) {
        rej();
      }
    });
  };

  render() {
    const PostedTypes = [
      { value: "last 7 days", label: "last 7 days" },
      { value: "last 15 days", label: "last 15 days" },
      { value: "last month", label: "last month" },
    ];
    const {
      getList,
      searchItem,
      handleOnSelectInput,
      handleUnfollowDiscussion,
    } = this;
    const { searchKey, postedWithin } = this.state;
    const { apiLoading, followingDiscussionData } = this.props;
    return (
      <Wrapper>
        <div className="followingPeoplePage">
          <div className="row m-0 align-items-center">
            <div className="col-6 p-0">
              <div className="searchInput">
                <InputBox
                  id="searchInput"
                  placeholder="Search"
                  type="text"
                  className="inputBox form-control"
                  onChange={searchItem}
                  value={searchKey}
                />
                <img src={Search_Blue_Icon} className="searchIcon"></img>
              </div>
            </div>
            <div className="col-6 p-0">
              <div className="d-flex justify-content-end align-items-center followingSortFilter_Sec">
                <div className="d-flex align-items-center">
                  <p className="postedLabel">Sort by</p>
                  <div className="SelectInput">
                    <SelectInput
                      noboxstyle={true}
                      placeholder="Recently added"
                      options={PostedTypes}
                      value={postedWithin}
                      onChange={handleOnSelectInput(`postedWithin`)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          {!apiLoading ? (
            followingDiscussionData && followingDiscussionData.length > 0 ? (
              <div id="followingDiv">
                <Pagination
                  id={"followingDiv"}
                  items={followingDiscussionData}
                  getItems={getList}
                ></Pagination>
                {followingDiscussionData &&
                  followingDiscussionData.map((data, index) => (
                    <div className="col-12 ChatListCard p-0">
                      <div className="section">
                        <div className="row m-0 align-items-center" key={index}>
                          <div className="col-6 p-0">
                            <div>
                              <h6 className="discussionName">
                                {data && data._source.title}
                              </h6>
                              <div className="d-flex align-items-center">
                                <div className="d-flex align-items-center">
                                  {data && data._source.type == "0" ? (
                                    <img
                                      src={Advice_Blue_Icon_Tab}
                                      className="adviceIcon"
                                    ></img>
                                  ) : (
                                    <img
                                      src={Project_Blue_Icon_Tab}
                                      className="projectIcon"
                                    ></img>
                                  )}
                                  <p className="discussionType">
                                    {data && data._source.type == "0"
                                      ? "Advice"
                                      : "Project"}
                                  </p>
                                </div>
                                <p className="lastUpdatedTime">
                                  Updated{" "}
                                  {findDayAgo(data && data._source.updatedOn)}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="row m-0 align-items-center justify-content-end">
                              <div className="col-10 p-0">
                                <div className="row m-0">
                                  <div className="col-6 p-0">
                                    <div className="postInfo">
                                      <div className="d-flex align-items-center">
                                        <span className="postedBy">
                                          Posted by:
                                        </span>
                                        <img
                                          src={
                                            data && data._source.profilePicUrl
                                          }
                                          className="authorDp"
                                        ></img>
                                        <p className="authorName">
                                          {data && data._source.username}
                                        </p>
                                      </div>
                                      <p className="postedDate">
                                        {formatDate(
                                          data && data._source.createdOn,
                                          "MMM DD, YYYY"
                                        )}
                                      </p>
                                      <p className="commentsNum">
                                        {data && data._source.commentCount}{" "}
                                        Comments
                                      </p>
                                    </div>
                                  </div>
                                  <div className="col-6 p-0">
                                    <div className="following_btn">
                                      <ButtonComp
                                        onClick={handleUnfollowDiscussion.bind(
                                          this,
                                          data._id
                                        )}
                                      >
                                        Following
                                      </ButtonComp>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            ) : (
              <div className="noDiscussion">
                <div className="text-center">
                  <img
                    src={NoDiscussionFollowing_Img}
                    className="nodiscussionImg"
                  ></img>
                  <p className="noDiscussionMsg">
                    You haven't followed a discussion yet.
                  </p>
                  <div className="dismiss_btn">
                    <ButtonComp>Dismiss</ButtonComp>
                  </div>
                </div>
              </div>
            )
          ) : (
            <div className="FollowingLoader">
              <PageLoader loading={apiLoading} />
            </div>
          )}
        </div>
        <style jsx>
          {`
            .nodiscussionImg {
              width: 8.784vw;
              object-fit: cover;
            }
            .authorDp {
              width: 0.732vw;
              margin: 0 0.366vw;
            }
            .postInfo {
              padding: 0 0.366vw;
              border-left: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .authorName {
              font-size: 0.585vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              margin-bottom: 0 !important;
              letter-spacing: 0.0292vw !important;
            }
            .postedBy,
            .postedDate {
              font-size: 0.585vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              margin-bottom: 0;
              letter-spacing: 0.0219vw !important;
              opacity: 0.7;
            }
            .commentsNum {
              font-size: 0.585vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              margin-bottom: 0;
              letter-spacing: 0.0219vw !important;
              opacity: 0.7;
            }
            .adviceIcon,
            .projectIcon {
              width: 0.732vw;
            }
            .noDiscussion {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 21.961vw;
              text-align: center;
            }
            .noDiscussionMsg {
              max-width: 80%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            :global(.dismiss_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0 0 0.585vw 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
            }
            :global(.dismiss_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.dismiss_btn button:focus),
            :global(.dismiss_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.dismiss_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .ChatListCard {
              cursor: pointer;
            }
            .ChatListCard:hover {
              cursor: pointer;
              background: ${BG_LightGREY_COLOR};
            }
            .ChatListCard:last-child .section {
              border: none;
            }
            .section {
              padding: 0.732vw;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .discussionName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${Dark_Blue_Color};
              max-width: 80%;
              margin: 0 0 0.366vw 0;
            }
            .discussionType {
              font-size: 0.607vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              margin: 0 0.366vw;
            }
            .lastUpdatedTime {
              font-size: 0.512vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
              padding: 0 0.366vw;
              border-left: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .following_btn {
              float: right;
            }
            :global(.following_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
            }
            :global(.following_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.following_btn button:focus),
            :global(.following_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.following_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.followingPeoplePage .searchInput) {
              position: relative;
              margin: 1.464vw 0;
            }
            :global(.followingPeoplePage .searchInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.196vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.followingPeoplePage .searchInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.followingPeoplePage .searchInput .inputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            .searchIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 1.098vw;
              width: 0.805vw;
            }
            .postedLabel {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0 0.366vw 0 0;
              letter-spacing: 0.0219vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(FollowingDiscussions);
