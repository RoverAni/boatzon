import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR_Opacity,
  Border_LightGREY_COLOR,
  GREY_VARIANT_2,
  NoFavFollowing_Img,
  Wishlist_Icon,
  Saved_Blue_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  Search_Blue_Icon,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import ProductCard from "../post-card/product-card";
import CustomLink from "../../components/Link/Link";
import ButtonComp from "../../components/button/button";
import { unfollowPost } from "../../services/following";
import PageLoader from "../../components/loader/page-loader";
import Pagination from "../../hoc/divPagination";
import debounce from "lodash.debounce";


class FollowingFavorites extends Component {
  state = {
    searchKey: "",
    postedWithin: "",
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      this.setState(
        {
          [name]: inputControl,
        },
        () => {
          this.props.handleFollowingPosts(
            10,
            page,
            true,
            this.state.searchKey,
            this.state.postedWithin.value
          );
        }
      );
    }
  };

  searchItem = (event) => {
    let inputControl = event.target;
    this.setState(
      {
        searchKey: inputControl.value,
      },
      () => {
        this.handleSearchApiCall(this.state.searchKey);
      }
    );
  };

  handleSearchApiCall = debounce((value) => {
    this.props.handleFollowingPosts(
      10,
      0,
      true,
      value,
      this.state.postedWithin.value,
      true
    );
  }, 500);

  handleUnfollowPost = (postId) => {
    let payload = {
      postId: postId.toString(),
      label: "Photo",
    };
    unfollowPost(payload)
      .then((res) => {
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            document.getElementById(`${postId}`).src = `${Wishlist_Icon}`;
            this.props.handleFollowingPosts(
              10,
              0,
              true,
              this.state.searchKey,
              this.state.postedWithin.value,
              true
            );
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  getList = (page = 0) => {
    return new Promise(async (res, rej) => {
      try {
        await this.props.handleFollowingPosts(
          10,
          page,
          false,
          this.state.searchKey,
          this.state.postedWithin.value
        );
        res();
      } catch (e) {
        rej();
      }
    });
  };

  render() {
    const PostedTypes = [
      { value: "1", label: "last 24 hr" },
      { value: "2", label: "last 15 days" },
      { value: "3", label: "last 30 days" },
    ];

    const { apiLoading, followingPostData } = this.props;
    const { postedWithin, searchKey } = this.state;
    const {
      getList,
      searchItem,
      handleOnSelectInput,
      handleUnfollowPost,
    } = this;
    return (
      <Wrapper>
        <div>
          <div className="followingFavoritesPage">
            <div className="row m-0 align-items-center">
              <div className="col-6 p-0">
                <div className="searchInput">
                  <InputBox
                    id="searchInput"
                    placeholder="Search"
                    type="text"
                    className="inputBox form-control"
                    onChange={searchItem}
                    value={searchKey}
                  />
                  <img src={Search_Blue_Icon} className="searchIcon"></img>
                </div>
              </div>
              <div className="col-6 p-0">
                <div className="d-flex justify-content-end align-items-center followingSortFilter_Sec">
                  <div className="d-flex align-items-center">
                    <p className="postedLabel">Sort by</p>
                    <div className="SelectInput">
                      <SelectInput
                        noboxstyle={true}
                        placeholder="Recently added"
                        options={PostedTypes}
                        value={postedWithin}
                        onChange={handleOnSelectInput(`postedWithin`)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {!apiLoading ? (
              followingPostData && followingPostData.length > 0 ? (
                <div id="followingPosts">
                  <Pagination
                    id={"followingPosts"}
                    items={followingPostData}
                    getItems={getList}
                  ></Pagination>
                  <div className="row m-0 my-2">
                    {followingPostData &&
                      followingPostData.map((item, index) => (
                        <div
                          key={index}
                          className="col-md-4 col-lg-3 p-0 boatCard"
                        >
                          <CustomLink href="/boat-detail">
                            <a target="_blank">
                              <ProductCard
                                Productdetails={item}
                                wishlist={true}
                                shipping={true}
                                professionalpage={true}
                              />
                            </a>
                          </CustomLink>
                          <img
                            src={
                              item.likeStatus ? Saved_Blue_Icon : Wishlist_Icon
                            }
                            id={item.postId}
                            className="wishlistIcon"
                            onClick={handleUnfollowPost.bind(this, item.postId)}
                          ></img>
                        </div>
                      ))}
                  </div>
                </div>
              ) : (
                <div className="noFavorites">
                  <div className="text-center">
                    <img
                      src={NoFavFollowing_Img}
                      className="noFavFollowingImg"
                    ></img>
                    <p className="noFavoritesMsg">
                      You haven't added items to your favorites
                    </p>
                    <div className="dismiss_btn">
                      <ButtonComp>Dismiss</ButtonComp>
                    </div>
                  </div>
                </div>
              )
            ) : (
              <div className="FollowingLoader">
                <PageLoader loading={apiLoading} />
              </div>
            )}
          </div>
        </div>

        <style jsx>
          {`
            .FollowingLoader {
              height: 13.762vw;
              display: flex;
              justify-content: center;
              align-items: center;
              background-color: ${WHITE_COLOR};
            }
            .noFavFollowingImg {
              width: 7.32vw;
              object-fit: cover;
            }
            .noFavorites {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 25.622vw;
              text-align: center;
            }
            .noFavoritesMsg {
              max-width: 80%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            :global(.dismiss_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0 0 0.585vw 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
            }
            :global(.dismiss_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.dismiss_btn button:focus),
            :global(.dismiss_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.dismiss_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            :global(.followingFavoritesPage .searchInput) {
              position: relative;
              margin: 1.464vw 0;
            }
            :global(.followingFavoritesPage .searchInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.196vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.followingFavoritesPage .searchInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              // box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.followingFavoritesPage
                .searchInput
                .inputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            .searchIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 1.098vw;
              width: 0.805vw;
            }
            .postedLabel {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0 0.366vw 0 0;
              letter-spacing: 0.0219vw !important;
            }
            .boatCard {
              cursor: pointer;
              max-width: 11.713vw;
              min-height: 13.909vw;
              min-height: 14.641vw;
              box-shadow: 0 0px 6px 0 rgba(48, 56, 97, 0.1);
              margin-right: 0.878vw !important;
              margin-top: 0.878vw !important;
              position: relative;
            }
            .wishlistIcon {
              position: absolute;
              bottom: 1.098vw;
              right: 0.732vw;
              width: 1.024vw;
            }
            #followingPosts {
              height: 150px;
              overflow-y: auto;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(FollowingFavorites);
