import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import {
  Search_Blue_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  Dark_Blue_Color,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
  NoPeopleFollowing_Img,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import { findDayAgo } from "../../lib/date-operation/date-operation";
import ButtonComp from "../../components/button/button";
import PageLoader from "../../components/loader/page-loader";
import { unfollowPeople } from "../../services/following";
import Pagination from "../../hoc/divPagination";
import debounce from "lodash.debounce";


class FollowingPeople extends Component {
  state = {
    searchKey: "",
    postedWithin: "",
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      this.setState(
        {
          [name]: inputControl,
        },
        () => {
          this.props.handleFollowingPeople(
            10,
            page,
            true,
            this.state.searchKey,
            this.state.postedWithin.value
          );
        }
      );
    }
  };

  searchItem = (event) => {
    let inputControl = event.target;
    this.setState(
      {
        searchKey: inputControl.value,
      },
      () => {
        this.handleSearchApiCall(this.state.searchKey);
      }
    );
  };

  handleSearchApiCall = debounce((value) => {
    this.props.handleFollowingPeople(
      10,
      0,
      true,
      value,
      this.state.postedWithin.value,
      true
    );
  }, 500);

  handleUnfollowPeople = (personName) => {
    let payload = {
      membername: personName,
    };
    unfollowPeople(payload)
      .then((res) => {
        console.log("resFollow", res);
        let response = res.data;
        if (response.code == 200) {
          this.props.handleFollowingPeople(
            10,
            0,
            true,
            this.state.searchKey,
            this.state.postedWithin.value,
            true
          );
        }
      })
      .catch((err) => {
        console.log("errFollow", err);
      });
  };

  getList = (page = 0) => {
    return new Promise(async (res, rej) => {
      try {
        await this.props.handleFollowingPeople(
          10,
          page,
          false,
          this.state.searchKey,
          this.state.postedWithin.value
        );
        res();
      } catch (e) {
        rej();
      }
    });
  };

  render() {
    const PostedTypes = [
      { value: "1", label: "last 24 hr" },
      { value: "2", label: "last 15 days" },
      { value: "3", label: "last 30 days" },
    ];
    const {
      getList,
      searchItem,
      handleOnSelectInput,
      handleUnfollowPeople,
    } = this;
    const { followingPeopleData, apiLoading } = this.props;
    const { searchKey, postedWithin } = this.state;
    return (
      <Wrapper>
        <div className="followingPeoplePage">
          <div className="row m-0 align-items-center">
            <div className="col-6 p-0">
              <div className="searchInput">
                <InputBox
                  id="searchInput"
                  placeholder="Search"
                  type="text"
                  className="inputBox form-control"
                  onChange={searchItem}
                  value={searchKey}
                />
                <img src={Search_Blue_Icon} className="searchIcon"></img>
              </div>
            </div>
            <div className="col-6 p-0">
              <div className="d-flex justify-content-end align-items-center followingSortFilter_Sec">
                <div className="d-flex align-items-center">
                  <p className="postedLabel">Sort by</p>
                  <div className="SelectInput">
                    <SelectInput
                      noboxstyle={true}
                      placeholder="Recently added"
                      options={PostedTypes}
                      value={postedWithin}
                      onChange={handleOnSelectInput(`postedWithin`)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          {!apiLoading ? (
            followingPeopleData && followingPeopleData.length > 0 ? (
              <div id="followingDiv">
                <Pagination
                  id={"followingDiv"}
                  items={followingPeopleData}
                  getItems={getList}
                ></Pagination>
                {followingPeopleData &&
                  followingPeopleData.map((data, index) => (
                    <div className="col-12 ChatListCard p-0">
                      <div className="section">
                        <div className="row m-0 align-items-center" key={index}>
                          <div className="col-6 p-0">
                            <div className="d-flex align-items-center">
                              <img
                                src={data.profilePicUrl}
                                className="profilePic"
                              ></img>
                              <div>
                                <h6 className="messengerName">
                                  {data.fullName}
                                </h6>
                                <p className="manufacturerName">
                                  {data.username && data.username}
                                </p>
                                <p className="chatDate">
                                  {findDayAgo(
                                    data.userStartedFollowingOn &&
                                      data.userStartedFollowingOn
                                  ) || "2 days ago"}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="following_btn">
                              <ButtonComp
                                onClick={handleUnfollowPeople.bind(
                                  this,
                                  data.username
                                )}
                              >
                                Following
                              </ButtonComp>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            ) : (
              <div className="noPeople">
                <div className="text-center">
                  <img
                    src={NoPeopleFollowing_Img}
                    className="noPeopleImg"
                  ></img>
                  <p className="noPeopleMsg">
                    You haven't followed anyone yet.
                  </p>
                  <div className="dismiss_btn">
                    <ButtonComp>Dismiss</ButtonComp>
                  </div>
                </div>
              </div>
            )
          ) : (
            <div className="FollowingLoader">
              <PageLoader loading={apiLoading} />
            </div>
          )}
        </div>

        <style jsx>
          {`
            .FollowingLoader {
              height: 13.762vw;
              display: flex;
              justify-content: center;
              align-items: center;
              background-color: ${WHITE_COLOR};
            }
            .noPeopleImg {
              width: 7.32vw;
              object-fit: cover;
            }
            .noPeople {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 25.622vw;
              text-align: center;
            }
            .noPeopleMsg {
              max-width: 80%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0.732vw auto;
            }
            :global(.dismiss_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0 0 0.585vw 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
            }
            :global(.dismiss_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.dismiss_btn button:focus),
            :global(.dismiss_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.dismiss_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            .ChatListCard {
              cursor: pointer;
            }
            .ChatListCard:hover {
              cursor: pointer;
              background: ${BG_LightGREY_COLOR};
            }
            .ChatListCard:last-child .section {
              border: none;
            }
            .section {
              padding: 0.732vw;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .profilePic {
              width: 2.928vw;
              height: 2.928vw;
              object-fit: cover;
              margin: 0 0.585vw;
              border-radius: 50%;
            }
            .messengerName {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${Dark_Blue_Color};
              margin: 0;
            }
            .manufacturerName {
              font-size: 0.607vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${THEME_COLOR};
              margin: 0;
            }
            .chatDate {
              font-size: 0.512vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
              margin: 0;
            }
            .following_btn {
              float: right;
            }
            :global(.following_btn button) {
              width: 7.32vw;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
            }
            :global(.following_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.following_btn button:focus),
            :global(.following_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.following_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.followingPeoplePage .searchInput) {
              position: relative;
              margin: 1.464vw 0;
            }
            :global(.followingPeoplePage .searchInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.196vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.followingPeoplePage .searchInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.followingPeoplePage .searchInput .inputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            .searchIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 1.098vw;
              width: 0.878vw;
            }
            .postedLabel {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              margin: 0 0.366vw 0 0;
              letter-spacing: 0.0219vw !important;
            }
            #followingDiv {
              height: 350px;
              overflow-y: auto;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(FollowingPeople);
