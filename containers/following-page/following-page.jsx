import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  WHITE_COLOR,
  FONTGREY_COLOR,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import FollowingFavorites from "./following-favorites";
import FollowingPeople from "./following-people";
import FollowingBusiness from "./following-business";
import FollowingDiscussions from "./following-discussions";
import { getCookie } from "../../lib/session";
import { ParseToken } from "../../lib/parsers/token-parser";
import {
  getFollowingPost,
  getfollowingPeople,
  getfollowingBusiness,
} from "../../services/following";
import { setApiLoading } from "../../redux/actions/auth";
import { getDiscussionsPostLogin } from "../../services/discussions";

class FollowingPage extends Component {
  state = {
    tabValue: 0,
    followingPostData: [],
    followingPeopleData: [],
    followingBusinessData: [],
    followingDiscussionData: [],
  };

  componentDidMount() {
    this.handleFollowingAPICalls();
  }
  // function to change tabValue
  handleTabChange = (event, value) => {
    this.setState({ tabValue: value }, () => {
      this.handleFollowingAPICalls(value);
    });
  };

  handleFollowingAPICalls = (tabValue) => {
    switch (tabValue) {
      case 0:
        this.setState(
          {
            followingPostData: [],
          },
          () => {
            this.handleFollowingPosts();
          }
        );
        break;
      case 1:
        this.setState(
          {
            followingPeopleData: [],
          },
          () => {
            this.handleFollowingPeople();
          }
        );
        break;
      case 2:
        this.setState(
          {
            followingBusinessData: [],
          },
          () => {
            this.handleFollowingBusiness();
          }
        );
        break;
      case 3:
        this.setState(
          {
            followingDiscussionData: [],
          },
          () => {
            this.handleFollowingDiscussion();
          }
        );
        break;

      default:
        this.handleFollowingPosts();
        break;
    }
  };

  handleFollowingPosts = (
    limit = 10,
    page = 0,
    pageloader = true,
    search = "",
    postedWithin = "",
    listrefresh = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        pageloader ? this.props.dispatch(setApiLoading(true)) : false;
        let payload = {
          limit,
          offset: page * limit,
          membername: ParseToken(getCookie("userName")),
          searchText: search,
          // postedWithin: postedWithin,
        };
        let result = await getFollowingPost(payload);

        let response = result.data;
        let oldList = listrefresh ? [] : [...this.state.followingPostData];
        let newList = response && response.data;
        if (response.code == 200) {
          this.setState({
            followingPostData: search ? [...newList] : [...oldList, ...newList],
          });
          this.props.dispatch(setApiLoading(false));
        }
        if (response.code == 201322 || response.code == 204) {
          this.props.dispatch(setApiLoading(false));
          this.setState({
            followingPostData: [...oldList],
          });
          return rej();
        }
        res();
      } catch (err) {
        this.props.dispatch(setApiLoading(false));
        console.log("djwj", err);
        this.setState({
          followingPostData: [],
        });
      }
    });
  };

  handleFollowingPeople = (
    limit = 10,
    page = 0,
    pageloader = true,
    search = "",
    postedWithin = "",
    listrefresh = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        pageloader ? this.props.dispatch(setApiLoading(true)) : false;
        let payload = {
          limit,
          offset: page * limit,
          searchText: search,
          // postedWithin: postedWithin,
          tab: "businesses",
        };
        let result = await getfollowingPeople(payload);

        let response = result.data;
        let oldList = listrefresh ? [] : [...this.state.followingPeopleData];
        let newList = response && response.result;
        if (response.code == 200) {
          this.setState({
            followingPeopleData: search
              ? [...newList]
              : [...oldList, ...newList],
          });
          this.props.dispatch(setApiLoading(false));
        }
        if (response.code == 201322 || response.code == 204) {
          this.props.dispatch(setApiLoading(false));
          this.setState({
            followingPeopleData: [...oldList],
          });
          return rej();
        }
        res();
      } catch (err) {
        this.props.dispatch(setApiLoading(false));
        console.log("djwj", err);
        this.setState({
          followingPeopleData: [],
        });
      }
    });
  };

  handleFollowingBusiness = (
    limit = 10,
    page = 0,
    pageloader = true,
    search = "",
    postedWithin = "",
    listrefresh = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        pageloader ? this.props.dispatch(setApiLoading(true)) : false;
        let payload = {
          limit,
          offset: page * limit,
          searchText: search,
          // postedWithin: postedWithin,
        };
        let result = await getfollowingBusiness(payload);
        let response = result.data;
        let oldList = listrefresh ? [] : [...this.state.followingBusinessData];
        let newList = response && response.result;
        if (response.code == 200) {
          this.setState({
            followingBusinessData: search
              ? [...newList]
              : [...oldList, ...newList],
          });
          this.props.dispatch(setApiLoading(false));
        }
        if (response.code == 201322 || response.code == 204) {
          this.props.dispatch(setApiLoading(false));
          this.setState({
            followingBusinessData: [...oldList],
          });
          return rej();
        }
        res();
      } catch (err) {
        this.props.dispatch(setApiLoading(false));
        console.log("djwj", err);
        this.setState({
          followingBusinessData: [],
        });
      }
    });
  };

  handleFollowingDiscussion = (
    limit = 10,
    page = 0,
    pageloader = true,
    search = "",
    postedWithin = "",
    listrefresh = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        pageloader ? this.props.dispatch(setApiLoading(true)) : false;
        let payload = {
          limit,
          offset: page * limit,
          title: search,
          type: "",
          following: true,
        };
        let result = await getDiscussionsPostLogin(payload);
        let response = result.data;
        console.log("djwj", response);
        let oldList = listrefresh
          ? []
          : [...this.state.followingDiscussionData];
        let newList = response && response.message;
        if (response.code == 200) {
          this.setState({
            followingDiscussionData: search
              ? [...newList]
              : [...oldList, ...newList],
          });
          this.props.dispatch(setApiLoading(false));
        }
        if (response.code == 201322 || response.code == 204) {
          this.props.dispatch(setApiLoading(false));
          this.setState({
            followingDiscussionData: [...oldList],
          });
          return rej();
        }
        res();
      } catch (err) {
        this.props.dispatch(setApiLoading(false));
        console.log("djwj", err);
        this.setState({
          followingDiscussionData: [],
        });
      }
    });
  };

  render() {
    const {
      tabValue,
      followingPostData,
      followingPeopleData,
      followingBusinessData,
      followingDiscussionData,
    } = this.state;
    const {
      handleTabChange,
      handleFollowingPosts,
      handleFollowingPeople,
      handleFollowingBusiness,
      handleFollowingDiscussion,
    } = this;
    const Favorites = (
      <FollowingFavorites
        followingPostData={followingPostData}
        handleFollowingPosts={handleFollowingPosts}
      />
    );
    const People = (
      <FollowingPeople
        followingPeopleData={followingPeopleData}
        handleFollowingPeople={handleFollowingPeople}
      />
    );
    const Business = (
      <FollowingBusiness
        followingBusinessData={followingBusinessData}
        handleFollowingBusiness={handleFollowingBusiness}
      />
    );
    const Discussions = (
      <FollowingDiscussions
        followingDiscussionData={followingDiscussionData}
        handleFollowingDiscussion={handleFollowingDiscussion}
      />
    );

    return (
      <Wrapper>
        <div className="FollowingPage py-3">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0 py-3">
              <div className="col-11 p-0 mx-auto">
                <div className="row mx-0 py-4 justify-content-center PageContent">
                  <div className="col-12 paddingXThree">
                    <h6 className="heading">Following</h6>
                    <div className="my-2">
                      <Tabs
                        followingsmalltabs={true}
                        editPostValue={tabValue}
                        handleEditTabChange={handleTabChange}
                        tabs={[
                          {
                            label: `Favorites`,
                          },
                          {
                            label: `People`,
                          },
                          {
                            label: `Business`,
                          },
                          {
                            label: `Discussions`,
                          },
                        ]}
                        tabcontent={[
                          { content: Favorites },
                          { content: People },
                          { content: Business },
                          { content: Discussions },
                        ]}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            .FollowingPage {
              background: ${BG_LightGREY_COLOR};
              position: relative;
              box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
            }
            .PageContent {
              background: ${WHITE_COLOR};
              max-width: 95%;
              margin: 0 auto !important;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};
export default connect(mapStateToProps)(FollowingPage);
