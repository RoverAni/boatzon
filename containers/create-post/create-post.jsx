// Main React Components
// lodash function
import cloneDeep from "lodash.clonedeep";
import React, { Component } from "react";
import { connect } from "react-redux";
// re-usable components
import Snackbar from "../../components/snackbar";
import Tabs from "../../components/tabs/tabs";
// wrapping component
import Wrapper from "../../hoc/Wrapper";
import {
  Boat_White,
  Close_Icon,
  Engines_White,
  Product_White,
  Trailers_White,
} from "../../lib/config";
import {
  AdvanceBoatDetailsComponent,
  BoatDetailsComponent,
  EngineDetailsComponent,
  ProductDetailsComponent,
  SuccessfullPostComponent,
  TrailerDetailsComponent,
} from "../../lib/create-post/create-post";
import { __BlobUpload } from "../../lib/image/cloudinaryUpload";
import { setApiLoading } from "../../redux/actions/auth";
import { getCurrentLocation } from "../../redux/actions/location/location";
// redux fucntions
import {
  getBoatsSubCategoryData,
  getEngineData,
  getProductsSubCategoryData,
  getTrailersData,
} from "../../redux/actions/sub-categories/sub-categories";
// API fucntions
import {
  createBoatPost,
  createEnginePost,
  createPost,
  createtrailerPost,
} from "../../services/create-post";
// imported components
import CreateBoatPost from "./create-boat-post";
import CreateEnginePost from "./create-engine-post";
import CreateProductPost from "./create-product-post";
import CreateTrailersPost from "./create-trailers-post";

class CreatePost extends Component {
  state = {
    currentScreen: "",
    screen: 1,
    pageName: "",
    finalpayload: {},
    tabValue: 0,
  };

  componentDidMount() {
    this.props.dispatch(getBoatsSubCategoryData());
    this.props.dispatch(getProductsSubCategoryData());
    this.props.dispatch(getEngineData());
    this.props.dispatch(getTrailersData());
    this.props.dispatch(getCurrentLocation());
    this.props.dispatch(setApiLoading(false));
  }

  handleTabValue = (finalpayload) => {
    switch (finalpayload.category) {
      case "Boats":
        return 0;
        break;
      case "Products":
        return 1;
        break;
      case "Engines":
        return 2;
        break;
      case "Trailers":
        return 3;
        break;
      default:
        return 0;
        break;
    }
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  // function to redirect to boat-details page
  handleBoatDetailsPage = (payload) => {
    let tempPayload = cloneDeep(payload).__wrapped__;
    this.setState(
      {
        screen: this.state.screen + 1,
        finalpayload: { ...tempPayload },
      },
      () => {
        this.updateScreen(BoatDetailsComponent(this));
      }
    );
  };

  // function to redirect to engine-details page
  handleEngineDetailsPage = (pageName, payload) => {
    let tempPayload = cloneDeep(this.state.finalpayload).__wrapped__;
    let payLoad = cloneDeep(payload).__wrapped__;
    let newPayload = { ...tempPayload, ...payLoad };
    this.setState(
      {
        screen: this.state.screen + 1,
        pageName: pageName,
        finalpayload: cloneDeep(newPayload).__wrapped__,
      },
      () => {
        this.updateScreen(EngineDetailsComponent(this));
      }
    );
  };

  // function to redirect to advance-boat-details page
  handleAdvanceBoatDetails = (pageName, payload) => {
    let tempPayload = cloneDeep(this.state.finalpayload).__wrapped__;
    let payLoad = cloneDeep(payload).__wrapped__;
    tempPayload.AllEngineData = [payLoad];
    tempPayload.engineCount =
      payLoad.num_of_engines && payLoad.num_of_engines.value;
    let newPayload = cloneDeep(tempPayload).__wrapped__;
    this.setState(
      {
        screen: this.state.screen + 1,
        pageName: pageName,
        finalpayload: cloneDeep(newPayload).__wrapped__,
      },
      () => {
        this.updateScreen(AdvanceBoatDetailsComponent(this));
      }
    );
  };

  // function to redirect to product-details page
  handleProductDetailsPage = (payload) => {
    console.log("fjwid-->screen1", this.state);
    let tempPayload = cloneDeep(payload).__wrapped__;
    this.setState(
      {
        screen: this.state.screen + 1,
        finalpayload: { ...tempPayload },
      },
      () => {
        this.updateScreen(ProductDetailsComponent(this));
      }
    );
  };

  // function to redirect to delivery-methods page
  handleDeliveryMethodPage = (pageName, payload) => {
    console.log("fjwid-->screen2", this.state);
    let tempPayload = cloneDeep(this.state.finalpayload).__wrapped__;
    let payLoad;
    let newPayload;
    if (pageName == "enginedetail") {
      payLoad = cloneDeep(payload).__wrapped__;
      tempPayload.AllEngineData = [payLoad];
      tempPayload.engineCount =
        payLoad.num_of_engines && payLoad.num_of_engines.value;
      newPayload = cloneDeep(tempPayload).__wrapped__;
    } else {
      payLoad = cloneDeep(payload).__wrapped__;
      newPayload = { ...tempPayload, ...payLoad };
    }
    this.setState(
      {
        screen: this.state.screen + 1,
        pageName: pageName,
        finalpayload: cloneDeep(newPayload).__wrapped__,
      },
      () => {
        this.updateScreen(DeliveryMethodComponent(this));
      }
    );
  };

  // function to redirect to boat-details page
  handleTrailerDetailsPage = (payload) => {
    let tempPayload = cloneDeep(payload).__wrapped__;
    this.setState(
      {
        screen: this.state.screen + 1,
        finalpayload: { ...tempPayload },
      },
      () => {
        this.updateScreen(TrailerDetailsComponent(this));
      }
    );
  };

  // function to create image cloudinary-urls and call create-post API and redirect to successfull-post page
  handleSuccessfullPost = async (pageName, payload) => {
    this.props.dispatch(setApiLoading(true));
    let tempPayload = cloneDeep(this.state.finalpayload).__wrapped__;
    tempPayload.productAssets = [];
    for (let i = 0; i < tempPayload.productImgs.length; i++) {
      if (tempPayload.productImgs[i].fileDetails) {
        try {
          let res = await __BlobUpload(tempPayload.productImgs[i].fileDetails);
          let imagePayload = {};
          imagePayload.url = res.body.secure_url;
          imagePayload.cloudinaryPublicId = res.body.public_id;
          imagePayload.urlType = res.body.resource_type;
          imagePayload.imgWidth = res.body.width;
          imagePayload.imgHeight = res.body.height;
          tempPayload.productAssets.push(imagePayload);
          this.setState(
            {
              finalpayload: cloneDeep(tempPayload).__wrapped__,
            },
            () => {
              if (i == tempPayload.productImgs.length - 1) {
                let Payload = cloneDeep(this.state.finalpayload).__wrapped__;
                let payLoad = cloneDeep(payload).__wrapped__;
                let newPayload = { ...Payload, ...payLoad };
                this.setState(
                  {
                    screen: this.state.screen + 1,
                    pageName: pageName,
                    finalpayload: cloneDeep(newPayload).__wrapped__,
                  },
                  () => {
                    console.log("dwdj--1", this.state.finalpayload);
                    setTimeout(() => {
                      if (pageName == "trailerdetails") {
                        this.handleCreateTrailerPost({
                          ...this.state.finalpayload,
                        });
                      } else if (pageName == "engineDetails") {
                        this.handleCreateEnginePost({
                          ...this.state.finalpayload,
                        });
                      } else if (pageName == "advance_boat_details") {
                        this.handleCreateBoatPostAPI({
                          ...this.state.finalpayload,
                        });
                      } else {
                        this.handleCreatePostAPI({
                          ...this.state.finalpayload,
                        });
                      }
                    }, 1500);
                  }
                );
              }
            }
          );
        } catch (err) {
          this.setState({
            usermessage: "Err uploading image",
            variant: "error",
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      }
    }
  };

  // function to redirect back to respective screens - create product post
  handleBackScreen = () => {
    switch (this.state.screen) {
      case 2:
        this.setState(
          {
            screen: this.state.screen - 1,
            tabValue: this.handleTabValue(this.state.finalpayload),
          },
          () => {
            this.updateScreen();
          }
        );
        break;
      case 3:
        this.setState(
          {
            screen: this.state.screen - 1,
          },
          () => {
            if (this.state.pageName == "product" || "deliveryPage") {
              this.updateScreen(ProductDetailsComponent(this));
            }
            if (this.state.pageName == "enginedetail") {
              this.updateScreen(EngineDetailsComponent(this));
            }
          }
        );
        break;
      case 4:
        this.setState(
          {
            screen: this.state.screen - 1,
          },
          () => {
            if (this.state.pageName == "deliveryPage") {
              this.updateScreen(DeliveryMethodComponent(this));
            }
          }
        );
        break;
    }
  };

  // function to change tabValue
  handleTabChange = (event, value) => {
    this.setState({ tabValue: value });
  };

  // function to redirect back to respective screens - create engine post
  handleBackScreenEngine = () => {
    switch (this.state.screen) {
      case 2:
        this.setState(
          {
            screen: this.state.screen - 1,
            tabValue: this.handleTabValue(this.state.finalpayload),
          },
          () => {
            this.updateScreen();
          }
        );
        break;
    }
  };

  // function to redirect back to respective screens - create boat post
  handleBackScreenBoat = () => {
    switch (this.state.screen) {
      case 2:
        this.setState(
          {
            screen: this.state.screen - 1,
            tabValue: this.handleTabValue(this.state.finalpayload),
          },
          () => {
            this.updateScreen();
          }
        );
        break;
      case 3:
        this.setState(
          {
            screen: this.state.screen - 1,
          },
          () => {
            this.updateScreen(BoatDetailsComponent(this));
          }
        );
        break;
      case 4:
        this.setState(
          {
            screen: this.state.screen - 1,
          },
          () => {
            this.updateScreen(EngineDetailsComponent(this));
          }
        );
        break;
      case 5:
        this.setState(
          {
            screen: this.state.screen - 1,
          },
          () => {
            this.updateScreen(AdvanceBoatDetailsComponent(this));
          }
        );
        break;
    }
  };

  // function for snackbar variant type
  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  remoteSnackbarHandler = (message, code) => {
    this.setState({
      usermessage: message,
      variant: this.handleSnackbar(code),
      open: true,
      vertical: "bottom",
      horizontal: "left",
    });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // function to call create boat post API
  handleCreateBoatPostAPI = (finalpayload) => {
    let FinalPayload;
    let tempUrlPayload = cloneDeep(finalpayload).__wrapped__;

    let otherUrlPayload =
      tempUrlPayload &&
      tempUrlPayload.productAssets &&
      tempUrlPayload.productAssets.length > 2 &&
      tempUrlPayload.productAssets.filter((data, index) => {
        return index > 0;
      });

    let payload =
      otherUrlPayload &&
      otherUrlPayload.length > 0 &&
      otherUrlPayload.map((data) => {
        delete data.imgWidth;
        delete data.imgHeight;
        return data;
      });

    let tempPayload = payload && payload.length > 0 ? [...payload] : [];

    tempPayload &&
      tempPayload.length > 0 &&
      tempPayload.map((item) => {
        let type =
          item.urlType == "image" ? "0" : item.urlType == "video" ? "1" : "";
        item.urlType = type;
      });

    let AllEngineData =
      tempUrlPayload &&
      tempUrlPayload.AllEngineData &&
      tempUrlPayload.AllEngineData.length > 0
        ? tempUrlPayload.AllEngineData.map((item) => {
            delete item.num_of_engines;
            return item;
          })
        : [];

    let enginePayload = [
      {
        currency: AllEngineData && AllEngineData[0].currency.value,
        engineFuelType: AllEngineData && AllEngineData[0].engineFuelType.value,
        engineHorsePower: AllEngineData && AllEngineData[0].engineHorsePower,
        engineHour: AllEngineData && AllEngineData[0].engineHour,
        engineMake: AllEngineData && AllEngineData[0].engineMake.value,
        engineModelName: AllEngineData && AllEngineData[0].engineModel.label,
        engineModel: AllEngineData && AllEngineData[0].engineModel.value,
        engineType: AllEngineData && AllEngineData[0].engineType.value,
        engineYear: AllEngineData && AllEngineData[0].engineYear.value,
        warrantyDate:
          finalpayload.hasWarranty.value == "No"
            ? "null"
            : AllEngineData && AllEngineData[0].warrantyDate,
      },
    ];

    FinalPayload = {
      mainUrl: finalpayload.productAssets[0].url,
      mainUrlType:
        finalpayload.productAssets[0].urlType == "image"
          ? "0"
          : finalpayload.productAssets[0].urlType == "video"
          ? "1"
          : "",
      thumbnailUrl: finalpayload.productAssets[0].url,
      thumbnailImageUrl: finalpayload.productAssets[0].url,
      otherUrls: JSON.stringify([...tempPayload]),
      imageCount: finalpayload.productAssets.length,
      containerHeight: finalpayload.productAssets[0].imgHeight,
      containerWidth: finalpayload.productAssets[0].imgWidth,
      price: finalpayload.price,
      currency: finalpayload.currency.value,
      productName: finalpayload.boat_title,
      condition: finalpayload.condition.value,
      negotiable: finalpayload.negotiable ? "1" : "0",
      cloudinaryPublicId: finalpayload.productAssets[0].cloudinaryPublicId,
      category: finalpayload.category,
      manufactor: finalpayload.manufacturer.label,
      manufactorId: finalpayload.manufacturer.value.toString(),
      location:
        this.props.currentLocation && this.props.currentLocation.address,
      latitude:
        this.props.currentLocation && this.props.currentLocation.latitude,
      longitude:
        this.props.currentLocation && this.props.currentLocation.longitude,
      city: this.props.currentLocation && this.props.currentLocation.city,
      stateSname:
        this.props.currentLocation && this.props.currentLocation.stateName,
      countrySname:
        this.props.currentLocation &&
        this.props.currentLocation.countryShortName,
      subCategory: finalpayload.boat_type.value,
      zipCode: finalpayload.zipCode,
      engineDetails: JSON.stringify([...enginePayload]),
      hasEngine:
        finalpayload.AllEngineData && finalpayload.AllEngineData.length > 0
          ? true
          : false,
      engineCount: finalpayload.engineCount,
      hasWarranty: finalpayload.hasWarranty.value,
      trailer: finalpayload.trailerValue,
      hullMaterial: finalpayload.hull_material,
      hullShape: finalpayload.hull_shape,
      nominalLength: finalpayload.nominalLength,
      overallLength: finalpayload.overallLength,
      beam: finalpayload.beam,
      draft: finalpayload.draft,
      dryWeight: finalpayload.dry_weight,
      fuelTankCapacity: finalpayload.fuelTank_capacity,
      heads: finalpayload.heads_count,
      length: parseInt(finalpayload.boat_length, 10),
      year: finalpayload.boat_year.value,
      boatModel: finalpayload.boat_model.value,
      boatModelName: finalpayload.boat_model.label,
      isSwap: 0, // 0: false, 1: true
      businessPost: false, // 1: true, 2: false
      title: finalpayload.boat_title,
      description: finalpayload.boat_description,
      nationWide: "false",
      color: finalpayload.boat_color.label,
    };

    createBoatPost(FinalPayload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          this.props.dispatch(setApiLoading(false));

          if (response.code == 200) {
            let ProductData = response.data[0];
            setTimeout(() => {
              this.updateScreen(SuccessfullPostComponent(this, ProductData));
            }, 500);
          }
        }
      })
      .catch((err) => {
        this.props.dispatch(setApiLoading(false));
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // function to call create product post API
  handleCreatePostAPI = (finalpayload) => {
    let FinalPayload;
    if (finalpayload.category == "Products") {
      let tempUrlPayload = cloneDeep(finalpayload).__wrapped__;

      let otherUrlPayload =
        tempUrlPayload &&
        tempUrlPayload.productAssets &&
        tempUrlPayload.productAssets.length > 2 &&
        tempUrlPayload.productAssets.filter((data, index) => {
          return index > 0;
        });

      let payload =
        otherUrlPayload &&
        otherUrlPayload.length > 0 &&
        otherUrlPayload.map((data) => {
          delete data.imgWidth;
          delete data.imgHeight;
          return data;
        });

      let tempPayload = payload && payload.length > 0 ? [...payload] : [];

      tempPayload &&
        tempPayload.length > 0 &&
        tempPayload.map((item) => {
          let type =
            item.urlType == "image" ? 0 : item.urlType == "video" ? 1 : "";
          item.urlType = type;
        });
      FinalPayload = {
        nationWide: finalpayload.nationWide === true ? true : false,
        mainUrl: finalpayload.productAssets[0].url,
        mainUrlType:
          finalpayload.productAssets[0].urlType == "image"
            ? "0"
            : finalpayload.productAssets[0].urlType == "video"
            ? "1"
            : "",
        thumbnailUrl: finalpayload.productAssets[0].url,
        thumbnailImageUrl: finalpayload.productAssets[0].url,
        otherUrls: JSON.stringify([...tempPayload]),
        imageCount: finalpayload.productAssets.length,
        containerHeight: finalpayload.productAssets[0].imgHeight,
        containerWidth: finalpayload.productAssets[0].imgWidth,
        price: finalpayload.price,
        currency: finalpayload.currency.value,
        productName: finalpayload.product_title,
        condition: finalpayload.condition.value,
        negotiable: finalpayload.negotiable ? "1" : "0",
        cloudinaryPublicId: finalpayload.productAssets[0].cloudinaryPublicId,
        category: finalpayload.category,
        manufactor: finalpayload.manufacturer.label,
        manufactorId: finalpayload.manufacturer.value.toString(),
        location: finalpayload.selectedAddress.address,
        latitude: finalpayload.selectedAddress.lat.toString(),
        longitude: finalpayload.selectedAddress.lng.toString(),
        city: finalpayload.selectedAddress.city,
        stateSname: finalpayload.selectedAddress.stateName,
        countrySname: finalpayload.selectedAddress.countrySname,
        subCategory: finalpayload.subCategory,
        subSubCategory: finalpayload.subSubCategory,
        title: finalpayload.product_title,
        description: finalpayload.product_description,
        isSwap: 0, // 0: false, 1: true
        businessPost: false, // 1: true, 2: false
      };

      if (finalpayload.nationWide == true) {
        FinalPayload["parcel"] = {
          width: finalpayload.packageWidth,
          height: finalpayload.packageHeight,
          distance_unit: "in",
          weight: finalpayload.packageWeightLB,
          package_length: finalpayload.packageLength,
          mass_unit: "lb",
        };
        FinalPayload["address"] = finalpayload.addressID;
        FinalPayload["street"] = finalpayload.selectedAddress.address;
        FinalPayload["state"] = finalpayload.selectedAddress.stateName;
      }
    }
    createPost(FinalPayload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          this.props.dispatch(setApiLoading(false));

          if (response.code == 200) {
            let ProductData = response.data[0];
            setTimeout(() => {
              this.updateScreen(SuccessfullPostComponent(this, ProductData));
            }, 500);
          }
        }
      })
      .catch((err) => {
        this.props.dispatch(setApiLoading(false));
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // function to call create trailer post API
  handleCreateTrailerPost = (finalpayload) => {
    let tempUrlPayload = cloneDeep(finalpayload).__wrapped__;

    let otherUrlPayload =
      tempUrlPayload &&
      tempUrlPayload.productAssets &&
      tempUrlPayload.productAssets.length > 2 &&
      tempUrlPayload.productAssets.filter((data, index) => {
        return index > 0;
      });

    let payload =
      otherUrlPayload &&
      otherUrlPayload.length > 0 &&
      otherUrlPayload.map((data) => {
        delete data.imgWidth;
        delete data.imgHeight;
        return data;
      });

    let tempPayload = payload && payload.length > 0 ? [...payload] : [];

    tempPayload &&
      tempPayload.length > 0 &&
      tempPayload.map((item) => {
        let type =
          item.urlType == "image" ? 0 : item.urlType == "video" ? 1 : "";
        item.urlType = type;
      });

    let FinalPayload = {
      mainUrl: finalpayload.productAssets[0].url,
      mainUrlType:
        finalpayload.productAssets[0].urlType == "image"
          ? "0"
          : finalpayload.productAssets[0].urlType == "video"
          ? "1"
          : "",
      thumbnailUrl: finalpayload.productAssets[0].url,
      thumbnailImageUrl: finalpayload.productAssets[0].url,
      otherUrls: JSON.stringify([...tempPayload]),
      imageCount: finalpayload.productAssets.length,
      containerHeight: finalpayload.productAssets[0].imgHeight,
      containerWidth: finalpayload.productAssets[0].imgWidth,
      title: finalpayload.trailers_title,
      price: finalpayload.price,
      currency: finalpayload.currency.value,
      condition: finalpayload.condition.value,
      cloudinaryPublicId: finalpayload.productAssets[0].cloudinaryPublicId,
      category: finalpayload.category,
      manufactor: finalpayload.manufacturer.label,
      manufactorId: finalpayload.manufacturer.value.toString(),
      location:
        this.props.currentLocation && this.props.currentLocation.address,
      latitude:
        this.props.currentLocation && this.props.currentLocation.latitude,
      longitude:
        this.props.currentLocation && this.props.currentLocation.longitude,
      city: this.props.currentLocation && this.props.currentLocation.city,
      countrySname:
        this.props.currentLocation &&
        this.props.currentLocation.countryShortName,
      stateSname:
        this.props.currentLocation && this.props.currentLocation.stateName,
      zipCode: this.props.currentLocation && this.props.currentLocation.zipCode,
      description: finalpayload.trailers_description,
      hasWarranty: finalpayload.hasWarranty.value, //--? input missing
      warrantyDate:
        finalpayload.hasWarranty.value == "No"
          ? "null"
          : finalpayload.warrantyDate,
      trailerType: finalpayload.trailerType.value,
      year: finalpayload.trailerYear.value,
      length: finalpayload.length,
      axles: finalpayload.axies.value,
    };

    createtrailerPost(FinalPayload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          this.props.dispatch(setApiLoading(false));
          if (response.code == 200) {
            let ProductData = response.data;
            setTimeout(() => {
              this.updateScreen(SuccessfullPostComponent(this, ProductData));
            }, 500);
          }
        }
      })
      .catch((err) => {
        this.props.dispatch(setApiLoading(false));
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // function to call create engine post API
  handleCreateEnginePost = (finalpayload) => {
    let tempUrlPayload = cloneDeep(finalpayload).__wrapped__;
    let otherUrlPayload =
      tempUrlPayload &&
      tempUrlPayload.productAssets &&
      tempUrlPayload.productAssets.length > 2 &&
      tempUrlPayload.productAssets.filter((data, index) => {
        return index > 0;
      });

    let payload =
      otherUrlPayload &&
      otherUrlPayload.length > 0 &&
      otherUrlPayload.map((data) => {
        delete data.imgWidth;
        delete data.imgHeight;
        return data;
      });

    let tempPayload = payload && payload.length > 0 ? [...payload] : [];

    tempPayload &&
      tempPayload.length > 0 &&
      tempPayload.map((item) => {
        let type =
          item.urlType == "image" ? 0 : item.urlType == "video" ? 1 : "";
        item.urlType = type;
      });

    let FinalPayload = {
      mainUrl: finalpayload.productAssets[0].url,
      mainUrlType:
        finalpayload.productAssets[0].urlType == "image"
          ? "0"
          : finalpayload.productAssets[0].urlType == "video"
          ? "1"
          : "",
      thumbnailUrl: finalpayload.productAssets[0].url,
      thumbnailImageUrl: finalpayload.productAssets[0].url,
      otherUrls: JSON.stringify([...tempPayload]),
      imageCount: finalpayload.productAssets.length,
      containerHeight: finalpayload.productAssets[0].imgHeight,
      containerWidth: finalpayload.productAssets[0].imgWidth,
      price: finalpayload.price,
      currency: finalpayload.currency && finalpayload.currency.value,
      title: finalpayload.engine_title,
      condition: finalpayload.condition.value,
      category: finalpayload.category,
      description: finalpayload.engine_description,
      cloudinaryPublicId: finalpayload.productAssets[0].cloudinaryPublicId,
      location:
        this.props.currentLocation && this.props.currentLocation.address,
      latitude:
        this.props.currentLocation && this.props.currentLocation.latitude,
      longitude:
        this.props.currentLocation && this.props.currentLocation.longitude,
      city: this.props.currentLocation && this.props.currentLocation.city,
      countrySname:
        this.props.currentLocation &&
        this.props.currentLocation.countryShortName,
      stateSname:
        this.props.currentLocation && this.props.currentLocation.stateName,
      zipCode: this.props.currentLocation && this.props.currentLocation.zipCode,
      hasWarranty: finalpayload.hasWarranty.value,
      warrantyDate:
        finalpayload.hasWarranty.value == "No"
          ? "null"
          : finalpayload.warrantyDate,
      engineMake: finalpayload.engineMake.label,
      engineModel: finalpayload.engineModel.label,
      horsePower: finalpayload.engineHorsePower,
      engineHours: finalpayload.engineHour,
      engineType: finalpayload.engineType.value,
      fuelType: finalpayload.engineFuelType.value,
      noOfEngines: finalpayload.num_of_engines.value,
      year: finalpayload.engineYear.value,
    };
    createEnginePost(FinalPayload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          this.props.dispatch(setApiLoading(false));
          if (response.code == 200) {
            let ProductData = response.data;
            setTimeout(() => {
              this.updateScreen(SuccessfullPostComponent(this, ProductData));
            }, 500);
          }
        }
      })
      .catch((err) => {
        this.props.dispatch(setApiLoading(false));
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  // function to re-direct to create-new-post page again
  handleCreateNewPost = () => {
    this.props.dispatch(setApiLoading(false));
    this.setState(
      {
        currentScreen: "",
        screen: 1,
        pageName: "",
        finalpayload: {},
        tabValue: 0,
      },
      () => {
        this.updateScreen();
      }
    );
  };

  render() {
    // console.log("finalpayload", this.state.finalpayload);
    const {
      finalpayload,
      currentScreen,
      tabValue,
      variant,
      usermessage,
      open,
      vertical,
      horizontal,
    } = this.state;
    const { onClose } = this.props;
    const {
      handleSnackbarClose,
      handleBoatDetailsPage,
      handleProductDetailsPage,
      handleEngineDetailsPage,
      handleTabChange,
      handleTrailerDetailsPage,
    } = this;
    const Boatlabel = (
      <div className="d-flex align-items-center">
        <img src={Boat_White} className="boatIcon"></img>
        <span>Boat</span>
      </div>
    );
    const Productlabel = (
      <div className="d-flex align-items-center">
        <img src={Product_White} className="productIcon"></img>
        <span>Product</span>
      </div>
    );
    const Enginelabel = (
      <div className="d-flex align-items-center">
        <img src={Engines_White} className="productIcon"></img>
        <span>Engine</span>
      </div>
    );
    const Trailerslabel = (
      <div className="d-flex align-items-center">
        <img src={Trailers_White} className="productIcon"></img>
        <span>Trailers</span>
      </div>
    );

    const Boats = (
      <CreateBoatPost
        handleBoatDetailsPage={handleBoatDetailsPage}
        finalpayload={finalpayload}
      />
    );

    const Products = (
      <CreateProductPost
        remoteSnackbarHandler={this.remoteSnackbarHandler}
        handleProductDetailsPage={handleProductDetailsPage}
        finalpayload={finalpayload}
      />
    );

    const Engines = (
      <CreateEnginePost
        handleEngineDetailsPage={handleEngineDetailsPage}
        finalpayload={finalpayload}
      />
    );

    const Trailers = (
      <CreateTrailersPost
        handleTrailerDetailsPage={handleTrailerDetailsPage}
        finalpayload={finalpayload}
      />
    );

    return (
      <Wrapper>
        <div className="col-12 createPostPage">
          {!currentScreen ? (
            <div>
              <img
                src={Close_Icon}
                className="closeIcon"
                onClick={onClose}
              ></img>
              <div className="drawerContent px-2">
                <h5 className="heading mb-3">What are you selling?</h5>
                <Tabs
                  tabbutton={true}
                  createPostValue={tabValue}
                  handleTabChange={handleTabChange}
                  tabs={[
                    { label: Boatlabel },
                    { label: Productlabel },
                    { label: Enginelabel },
                    { label: Trailerslabel },
                  ]}
                  tabcontent={[
                    { content: Boats },
                    { content: Products },
                    { content: Engines },
                    { content: Trailers },
                  ]}
                />
              </div>
            </div>
          ) : (
            currentScreen
          )}
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={variant}
          message={usermessage}
          open={open}
          onClose={handleSnackbarClose}
          vertical={vertical}
          horizontal={horizontal}
        />

        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.MuiPaper-root) {
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .createPostPage {
              width: 21.458vw;
              padding: 1.098vw;
              position: relative;
            }
            .longArrowLeftIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.024vw;
              cursor: pointer;
            }
            .closeIconRight {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .drawerContent {
              margin: 2.196vw 0 1.098vw 0;
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.boatIcon) {
              width: 1.464vw;
              margin-right: 0.585vw;
              margin-bottom: 0.219vw;
            }
            :global(.productIcon) {
              width: 1.098vw;
              margin-right: 0.585vw;
              margin-bottom: 0.219vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    engineData: state.engineData,
  };
};

export default connect(mapStateToProps)(CreatePost);
