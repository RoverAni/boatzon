// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// imported components
import InputBox from "../../components/input-box/input-box";
import TextAreaBox from "../../components/input-box/text-area-input";

// reusable component
import ButtonComp from "../../components/button/button";

// asstes and colors
import {
  Border_LightGREY_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR,
  GREY_VARIANT_3,
  Img_Download_Icon,
  GREEN_COLOR,
  GREY_VARIANT_1,
  IMAGE_UPLOAD_SIZE,
  VIDEO_UPLOAD_SIZE,
  Close_Icon,
  WHITE_COLOR,
  Border_LightGREY_COLOR_2,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR_1,
} from "../../lib/config";
import Snackbar from "../../components/snackbar";

class CreateEnginePost extends Component {
  state = {
    inputpayload: {
      category: "Engines",
    },
    ProductAssets: [],
    isFormValid: false,
    cropModal: false,
  };

  componentDidMount() {
    let tempPayload = { ...this.state.inputpayload };
    let newPayload = { ...tempPayload, ...this.props.finalpayload };
    this.setState(
      {
        inputpayload: { ...newPayload },
        ProductAssets: this.props.finalpayload.hasOwnProperty("productImgs")
          ? this.props.finalpayload.productImgs
          : [],
        isDocValid:
          this.props.finalpayload.hasOwnProperty("productImgs") &&
          this.props.finalpayload.productImgs.length
            ? true
            : false,
        productpic:
          this.props.finalpayload.hasOwnProperty("productImgs") &&
          this.props.finalpayload.productImgs.length
            ? true
            : false,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  }

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempsignUpPayload = { ...this.state.inputpayload };
    tempsignUpPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempsignUpPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  fileHandler = (event) => {
    let arr = [...this.state.ProductAssets];
    let files = event.target.files;
    if (files.length > 1) {
      for (let i = 0; i < files.length; i++) {
        arr.push({
          fileDetails: files[i],
          blobUrl: URL.createObjectURL(files[i]),
          index: i,
        });
      }
    } else {
      arr.push({
        fileDetails: files[0],
        blobUrl: URL.createObjectURL(files[0]),
        index: arr.length + 1,
      });
    }
    this.setState(
      {
        productpic: true,
        fileErr: null,
        ProductAssets: arr,
        isDocValid: true,
      },
      () => {
        let tempInputPayload = { ...this.state.inputpayload };
        tempInputPayload.productImgs = [...this.state.ProductAssets];
        this.setState(
          {
            inputpayload: { ...tempInputPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    );
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid =
      this.state.inputpayload.engine_title &&
      this.state.inputpayload.engine_description &&
      this.state.isDocValid
        ? true
        : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Function for remove the images
  handleRemoveImg = (ImageData) => {
    let ProductAssets = [...this.state.ProductAssets];
    let elementPosition = ProductAssets.findIndex((data, index) => {
      return data.index === ImageData.index;
    });
    if (elementPosition != -1) {
      ProductAssets.splice(elementPosition, 1);
    }

    this.setState(
      {
        ProductAssets: ProductAssets,
      },
      () => {
        this.setState({
          productpic: this.state.ProductAssets.length > 0 ? true : false,
        });
      }
    );
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // used to upload image from local storage
  handleLoadCropperModel = (event) => {
    let files = event.target.files;
    if (
      files &&
      files[0] &&
      (files[0].type == "image/jpeg" ||
        files[0].type == "image/png" ||
        files[0].type == "image/jpg")
    ) {
      let reader = new FileReader();
      reader.onload = (e) => {
        this.setState({
          cropModal: true,
          files: [...files],
          currentImg: e.target.result,
        });
      };
      reader.readAsDataURL(event.target.files[0]);
    } else if (files && files[0] && files[0].type == "video/mp4") {
      let reader = new FileReader();
      reader.onload = (e) => {
        this.setState(
          {
            currentVideo: e.target.result,
            files: [...files],
          },
          () => {
            this.handleUploadMedia(this.state.currentVideo);
          }
        );
      };
      reader.readAsDataURL(event.target.files[0]);
    } else {
      this.setState({
        isDocValid: false,
        fileErr:
          files[0].type == "video"
            ? "Invalid file format! Video should be in .mp4 format"
            : "Invalid file format! Image should be in .jpeg|.jpg|.png format",
        variant: "error",
        open: true,
        vertical: "bottom",
        horizontal: "left",
      });
    }
  };

  // Funtion to create croppedImg Payload
  handleUploadMedia = (croppedImg) => {
    this.CloseModal();
    if (this.isFileSizeValid(this.state.files[0])) {
      let tempImgArr = [...this.state.ProductAssets];
      let imgObject = {};
      imgObject.url = croppedImg;
      imgObject.urlType = this.state.files[0].type.split("/")[0];
      tempImgArr.push(imgObject);
      this.setState(
        {
          productpic: true,
          fileErr: null,
          ProductAssets: [...tempImgArr],
          isDocValid: true,
        },
        () => {
          let tempInputPayload = { ...this.state.inputpayload };
          tempInputPayload.productImgs = [...this.state.ProductAssets];
          this.setState(
            {
              inputpayload: { ...tempInputPayload },
            },
            () => {
              this.checkIfFormValid();
            }
          );
        }
      );
    } else {
      this.setState(
        {
          fileErr:
            this.state.files[0].type == "video"
              ? "File size should be less than 4MB!"
              : "File size should be less than 2MB!",
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
          isDocValid: false,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // used to check file-size validation
  isFileSizeValid = (fileData) => {
    switch (fileData.type.split("/")[0]) {
      case "video":
        return fileData.size <= VIDEO_UPLOAD_SIZE;
        break;
      case "image":
        return fileData.size <= IMAGE_UPLOAD_SIZE;
        break;
    }
  };

  // used to close cropper modal
  CloseModal = () => {
    this.setState({
      cropModal: false,
    });
  };

  render() {
    console.log("fjifjed", this.state.inputpayload);
    const {
      isFormValid,
      productpic,
      inputpayload,
      variant,
      fileErr,
      open,
      vertical,
      horizontal,
      ProductAssets,
    } = this.state;
    const { engine_title, engine_description } = { ...this.state.inputpayload };
    const {
      handleOnchangeInput,
      handleRemoveImg,
      handleSnackbarClose,
    } = this;
    const { handleEngineDetailsPage } = this.props;
    return (
      <Wrapper>
        <div className="boatPostPage">
          <div className="FormInput">
            <label>
              Title <span className="mandatory">*</span>
            </label>
            <InputBox
              type="text"
              className="inputBox form-control"
              placeholder="For ex. Bayliner"
              name="engine_title"
              value={engine_title}
              onChange={handleOnchangeInput}
              autoComplete="off"
            ></InputBox>
          </div>
          <div className="FormInput">
            <label>
              Description <span className="mandatory">*</span>
            </label>
            <TextAreaBox
              type="textarea"
              className="textareaBox form-control"
              placeholder="Detailed Descriptions of key features, accessories and electronics."
              name="engine_description"
              value={engine_description}
              onChange={handleOnchangeInput}
              autoComplete="off"
            ></TextAreaBox>
          </div>
          <div className="imageUploadSec">
            {productpic ? (
              <div className="d-flex flex-wrap">
                {ProductAssets &&
                  ProductAssets.map((data, index) =>
                    data.fileDetails.type == "video" ? (
                      <div className="videoDiv" key={index}>
                        <div className="videoBlock">
                          <video className="video" autoPlay controls>
                            <source
                              src={data.url || data.blobUrl}
                              type="video/mp4"
                            ></source>
                          </video>
                        </div>
                        <img
                          src={Close_Icon}
                          className="closeIcon"
                          onClick={handleRemoveImg.bind(this, data)}
                        ></img>
                        {/* <div className="videoIconDiv">
                          <img
                            src={Play_Video_Icon}
                            className="videoIcon"
                          ></img>
                        </div> */}
                      </div>
                    ) : data.fileDetails.type == "image/png" ||
                      data.fileDetails.type == "image/jpg" ||
                      data.fileDetails.type == "image/jpeg" ? (
                      <div className="ImgSec" key={index}>
                        <div
                          style={{
                            background: `url(${data.url || data.blobUrl})`,
                            height: "inherit",
                            width: "inherit",
                          }}
                        ></div>
                        <img
                          src={Close_Icon}
                          className="closeIcon"
                          onClick={handleRemoveImg.bind(this, data)}
                        ></img>
                      </div>
                    ) : (
                      ""
                    )
                  )}
                {ProductAssets && ProductAssets.length <= 9 ? (
                  <label for="uploadImage" className="UploadSec">
                    <div>
                      <div className="UploadImgSec">
                        <img src={Img_Download_Icon}></img>
                      </div>
                      <input
                        multiple
                        id="uploadImage"
                        type="file"
                        style={{ display: "none" }}
                        onChange={this.fileHandler}
                        accept=".jpeg,.jpg,.png,.mp4"
                      />
                    </div>
                  </label>
                ) : (
                  ""
                )}
              </div>
            ) : (
              <label for="cmpRegImage" className="ImageSec">
                <div>
                  <div className="UploadImgSec">
                    <img src={Img_Download_Icon} className="uploadImg"></img>
                    <h6>
                      Drop here to upload image or <span>browse</span>
                    </h6>
                    <p>Upload up to 10 images. Max. size per photo: 5mb.</p>
                  </div>
                  <input
                    multiple
                    id="cmpRegImage"
                    type="file"
                    style={{ display: "none" }}
                    onChange={this.fileHandler}
                    // onChange={handleLoadCropperModel}
                    accept=".jpeg,.jpg,.png,.mp4"
                  />
                </div>
              </label>
            )}
          </div>
          <div className={!isFormValid ? "inactive_Btn" : "Next_btn"}>
            <ButtonComp
              disabled={!isFormValid}
              onClick={handleEngineDetailsPage.bind(
                this,
                "engine",
                inputpayload
              )}
            >
              Next
            </ButtonComp>
          </div>
        </div>

        {/* SignUp Model */}
        {/* <Model open={cropModal} onClose={CloseModal}>
          <ImageCropper currentImg={currentImg} handleUploadMedia={handleUploadMedia} onClose={CloseModal} />
        </Model> */}

        <Snackbar
          variant={variant}
          message={fileErr}
          open={open}
          onClose={handleSnackbarClose}
          vertical={vertical}
          horizontal={horizontal}
        />

        <style jsx>{`
          .boatPostPage {
            margin: 1.464vw 0;
          }

          .boatPostPage .FormInput {
            margin: 1.098vw 0;
          }
          .boatPostPage .FormInput label {
            font-size: 0.732vw;
            margin: 0;
            color: ${GREY_VARIANT_2};
            font-family: "Museo-Sans" !important;
          }

          :global(.boatPostPage .FormInput .inputBox) {
            display: block;
            width: 100%;
            height: 2.489vw;
            padding: 0.439vw 0;
            line-height: 1.5;
            background-color: #fff;
            background-clip: padding-box;
            border: none;
            border-bottom: 1px solid ${Border_LightGREY_COLOR_2};
            border-radius: 0 !important;
            transition: border-color 0.15s ease-in-out,
              box-shadow 0.15s ease-in-out;
            font-size: 0.878vw;
            color: ${GREY_VARIANT_2};
            font-family: "Open Sans" !important;
          }
          :global(.boatPostPage .FormInput .inputBox:focus) {
            color: ${GREY_VARIANT_2};
            background-color: #fff;
            border-color: ${THEME_COLOR};
            outline: 0;
            box-shadow: none;
          }
          :global(.inputBox::placeholder) {
            font-size: 0.878vw;
            color: ${GREY_VARIANT_3};
            font-family: "Open Sans" !important;
          }
          :global(.boatPostPage .FormInput .textareaBox) {
            display: block;
            width: 100%;
            height: 7.32vw;
            padding: 0.439vw 0;
            line-height: 1.5;
            background-color: #fff;
            background-clip: padding-box;
            border: none;
            border-bottom: 1px solid ${Border_LightGREY_COLOR_2};
            border-radius: 0 !important;
            transition: border-color 0.15s ease-in-out,
              box-shadow 0.15s ease-in-out;
            font-size: 0.878vw;
            color: ${GREY_VARIANT_2};
            font-family: "Open Sans" !important;
          }
          :global(.boatPostPage .FormInput .textareaBox:focus) {
            color: ${GREY_VARIANT_2};
            background-color: #fff;
            border-color: ${THEME_COLOR};
            outline: 0;
            box-shadow: none;
          }
          :global(.textareaBox::placeholder) {
            font-size: 0.878vw;
            color: ${GREY_VARIANT_3};
            font-family: "Open Sans" !important;
          }
          .closeIcon {
            position: absolute;
            top: 0.366vw;
            right: 0.366vw;
            width: 0.732vw;
            cursor: pointer;
            display: none;
            z-index: 1;
          }
          .ImageSec {
            background: ${Border_LightGREY_COLOR};
            border: 0.0732vw dashed ${GREY_VARIANT_3};
            width: 100%;
            height: 8.784vw;
            position: relative;
            cursor: pointer;
            margin: 0;
            box-shadow: 0px 2px 4px 2px rgba(209, 208, 208, 0.3);
          }
          .ImgSec {
            width: 7.32vw;
            height: 8.784vw;
            cursor: pointer;
            margin: 0 0.732vw 0.732vw 0;
            border-radius: 0.366vw;
            position: relative;
          }
          .ImgSec > div {
            width: 100%;
            height: 100%;
            background-size: cover !important;
          }
          .ImgSec:hover > .closeIcon {
            display: block;
          }
          .videoDiv {
            width: 13.177vw;
            height: 100%;
            margin: 0 0.732vw 0.732vw 0;
            cursor: pointer;
            position: relative;
          }
          .videoBlock {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          .videoIconDiv {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
          }
          .videoIcon {
            width: 1.83vw;
            object-fit: cover;
            display: none;
            z-index: 1;
          }
          .videoDiv:hover > .videoIcon {
            display: block;
          }
          .videoDiv:hover > .closeIcon {
            display: block;
          }
          .UploadSec {
            background: ${Border_LightGREY_COLOR};
            border: 0.0732vw dashed ${GREY_VARIANT_3};
            width: 7.32vw;
            height: 8.784vw;
            border-radius: 0.366vw;
            position: relative;
            cursor: pointer;
            margin: 0 0 0.732vw 0;
            box-shadow: 0px 2px 4px 2px rgba(209, 208, 208, 0.3);
          }
          .UploadImgSec {
            width: 16.837vw;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            text-align: center;
          }
          .uploadImg {
            width: 1.683vw;
            object-fit: cover;
          }
          .UploadImgSec > h6 {
            color: ${GREY_VARIANT_1};
            font-size: 0.878vw;
            padding: 0.585vw;
            margin: 0;
            font-family: "Open Sans" !important;
            letter-spacing: 0.0219vw !important;
          }
          .UploadImgSec > h6 span {
            color: ${GREEN_COLOR};
            font-family: "Open Sans" !important;
            letter-spacing: 0.0219vw !important;
          }
          .UploadImgSec > p {
            font-size: 0.658vw;
            text-transform: capiltalize;
            color: ${GREY_VARIANT_2};
            font-family: "Open Sans" !important;
            letter-spacing: 0.0219vw !important;
            margin: 0;
          }
          :global(.inactive_Btn button) {
            width: 100%;
            padding: 0.366vw 0;
            background: ${Border_LightGREY_COLOR_1};
            color: ${WHITE_COLOR};
            margin: 0.732vw 0;
            text-transform: capitalize;
          }
          :global(.Next_btn button) {
            width: 100%;
            padding: 0.366vw 0;
            background: ${GREEN_COLOR};
            color: ${BG_LightGREY_COLOR};
            margin: 0.732vw 0;
            text-transform: capitalize;
          }
          :global(.Next_btn button span),
          :global(.inactive_Btn button span) {
            font-size: 1.024vw;
            font-family: "Museo-Sans" !important;
            font-weight: 600;
          }
          :global(.Next_btn button:focus),
          :global(.Next_btn button:active) {
            background: ${GREEN_COLOR};
            outline: none;
            box-shadow: none;
          }
          :global(.Next_btn button:hover) {
            background: ${GREEN_COLOR};
          }
        `}</style>
      </Wrapper>
    );
  }
}

export default CreateEnginePost;
