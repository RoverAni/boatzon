import React, { Component } from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREEN_COLOR,
  Border_LightGREY_COLOR_2,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  WHITE_COLOR,
  THEME_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_3,
  BG_LightGREY_COLOR,
} from "../../lib/config";
// reusable component
import ButtonComp from "../../components/button/button";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { withStyles } from "@material-ui/core";
import clsx from "clsx";

// reactstrap components
import { InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import SelectInput from "../../components/input-box/select-input";
import SelectTopLabel from "../../components/input-box/select-top-label";
import cloneDeep from "lodash.clonedeep";
import { NumberValidator } from "../../lib/validation/validation";
import CircularProgressButton from "../../components/button-loader/button-loader";

const styles = (theme) => ({
  radiogrouRoot: {
    flexDirection: "row",
  },
  radioLabelRoot: {
    margin: "0",
  },
  radioLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Museo-Sans !important",
  },
  label: {
    color: `${FONTGREY_COLOR}`,
    margin: "0",
    fontSize: "0.951vw",
    fontFamily: "Museo-Sans !important",
  },
  root: {
    paddingLeft: "0",
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  checked: {
    color: `${THEME_COLOR} !important`,
  },
  plainicon: {
    borderRadius: 3,
    width: "1.024vw",
    height: "1.024vw",
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto rgba(19,124,189,.6)",
      outlineOffset: 2,
    },
    "input:hover ~ &": {
      backgroundColor: "#ebf1f5",
    },
    "input:disabled ~ &": {
      boxShadow: "none",
      background: "rgba(206,217,224,.5)",
    },
  },
  plaincheckedIcon: {
    backgroundColor: `${WHITE_COLOR}`,
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: "1.024vw",
      height: "1.024vw",
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%232A3656'/%3E%3C/svg%3E\")",
      content: '""',
    },
    "input:hover ~ &": {
      backgroundColor: `${WHITE_COLOR}`,
    },
  },
});

class AdvanceBoatDetails extends Component {
  state = {
    inputpayload: {},
    newAddress: "",
  };

  componentDidMount() {
    let {
      trailerValue,
      hull_material,
      hull_shape,
      nominalLength,
      overallLength,
      beam,
      draft,
      dry_weight,
      fuelTank_capacity,
      heads_count,
    } = this.props.finalpayload;
    this.setState(
      {
        trailerValue: trailerValue ? trailerValue : "",
        hull_material: hull_material
          ? { value: hull_material, label: hull_material }
          : "",
        hull_shape: hull_shape ? { value: hull_shape, label: hull_shape } : "",
        nominalLength: nominalLength ? nominalLength : "",
        overallLength: overallLength ? overallLength : "",
        beam: beam ? beam : "",
        draft: draft ? draft : "",
        dry_weight: dry_weight ? dry_weight : "",
        fuelTank_capacity: fuelTank_capacity ? fuelTank_capacity : "",
        heads_count: heads_count
          ? { value: heads_count, label: heads_count }
          : "",
      },
      () => {
        this.setState({
          inputpayload: cloneDeep(this.state.finalpayload).__wrapped__,
        });
      }
    );
  }

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: tempPayload,
      [inputControl.name]: inputControl.value,
    });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = inputControl.value;
      let selectPayload = { ...this.state[name] };
      selectPayload.label = inputControl.label;
      selectPayload.value = inputControl.value;
      this.setState({
        inputpayload: tempPayload,
        [name]: { ...selectPayload },
      });
    }
  };

  handleRadioInputChange = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[[inputControl.name]] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
      [inputControl.name]: event.target.value,
    });
  };

  render() {
    const HeadsNum = [
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
      { value: "6", label: "6" },
      { value: "7", label: "7" },
      { value: "8", label: "8" },
      { value: "9", label: "9" },
      { value: "10", label: "10" },
    ];

    const Hull_Material =
      this.props.engineData.hullMaterial &&
      this.props.engineData.hullMaterial.map((data) => {
        return { value: data, label: data };
      });

    const Hull_Shape_Types =
      this.props.engineData.hullShape &&
      this.props.engineData.hullShape.map((data) => {
        return { value: data, label: data };
      });

    const {
      trailerValue,
      hull_material,
      hull_shape,
      nominalLength,
      overallLength,
      beam,
      draft,
      dry_weight,
      fuelTank_capacity,
      heads_count,
      inputpayload,
    } = this.state;
    const {
      classes,
      handleBackScreenBoat,
      handleSuccessfullPost,
      apiLoading,
      onClose,
    } = this.props;
    const {
      handleRadioInputChange,
      handleOnSelectInput,
      handleOnchangeInput,
    } = this;
    return (
      <Wrapper>
        <div>
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        </div>
        <div className="advanceBoatDetailsPage px-2">
          <h3 className="heading">Advanced Boat Details</h3>
          <div>
            <h6 className="subHeading">General</h6>
            <div>
              <FormControl component="fieldset">
                <FormLabel
                  classes={{
                    root: classes.label,
                  }}
                  component="legend"
                >
                  Trailer
                </FormLabel>
                <RadioGroup
                  classes={{
                    root: classes.radiogrouRoot,
                  }}
                  aria-label="trailerValue"
                  name="trailerValue"
                  value={trailerValue}
                  onChange={handleRadioInputChange}
                >
                  <FormControlLabel
                    classes={{
                      root: classes.radioLabelRoot,
                      label: classes.radioLabel,
                    }}
                    value="yes"
                    control={
                      <Radio
                        classes={{
                          root: classes.root,
                          checked: classes.checked,
                        }}
                        checkedIcon={
                          <span
                            className={clsx(
                              classes.plainicon,
                              classes.plaincheckedIcon
                            )}
                          />
                        }
                        icon={<span className={classes.plainicon} />}
                      />
                    }
                    label="Yes"
                  />
                  <FormControlLabel
                    classes={{
                      root: classes.radioLabelRoot,
                      label: classes.radioLabel,
                    }}
                    value="no"
                    control={
                      <Radio
                        classes={{
                          root: classes.root,
                          checked: classes.checked,
                        }}
                        checkedIcon={
                          <span
                            className={clsx(
                              classes.plainicon,
                              classes.plaincheckedIcon
                            )}
                          />
                        }
                        icon={<span className={classes.plainicon} />}
                      />
                    }
                    label="No"
                  />
                </RadioGroup>
              </FormControl>
            </div>
            <div className="selectInputSec pt-2">
              <SelectTopLabel
                bottomborder={true}
                value={hull_material}
                label="Hull Material"
                placeholder="Ferro-Cement"
                options={Hull_Material}
                onChange={handleOnSelectInput(`hull_material`)}
              />
            </div>
          </div>

          <div className="selectInputSec">
            <SelectTopLabel
              bottomborder={true}
              label="Hull Shape"
              value={hull_shape}
              placeholder="Semi Displacement"
              options={Hull_Shape_Types}
              onChange={handleOnSelectInput(`hull_shape`)}
            />
          </div>
          <div className="formInput">
            <label className="labelTitle">Dimensions & Weights</label>
            <div className="row m-0 pt-1">
              <div className="col-12 p-0">
                <div className="row m-0 align-items-center">
                  <div className="col-4 p-0 DaysFormInput">
                    <label className="labelName">Nominal Length</label>
                    <InputGroup>
                      <Input
                        className="packageInput"
                        type="text"
                        name="nominalLength"
                        value={nominalLength}
                        onChange={handleOnchangeInput}
                        onKeyPress={NumberValidator}
                      />
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>ft</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                  <div className="col-4 p-0 DaysFormInput">
                    <label className="labelName">Overall Length</label>
                    <InputGroup>
                      <Input
                        className="packageInput"
                        type="text"
                        value={overallLength}
                        name="overallLength"
                        onChange={handleOnchangeInput}
                        onKeyPress={NumberValidator}
                      />
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>ft</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                  <div className="col-4 p-0 DaysFormInput">
                    <label className="labelName">Beam</label>
                    <InputGroup className="w-100">
                      <Input
                        className="packageInput"
                        type="text"
                        value={beam}
                        name="beam"
                        onChange={handleOnchangeInput}
                        onKeyPress={NumberValidator}
                      />
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>ft</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                </div>
              </div>
            </div>
            <div className="row m-0">
              <div className="col-12 p-0 pt-2">
                <div className="row m-0 align-items-center">
                  <div className="col-4 p-0 DaysFormInput">
                    <label className="labelName">Draft</label>
                    <InputGroup>
                      <Input
                        className="packageInput"
                        type="text"
                        value={draft}
                        name="draft"
                        onChange={handleOnchangeInput}
                        onKeyPress={NumberValidator}
                      />
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>in</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                  <div className="col-8 p-0 DaysFormInput">
                    <label className="labelName">Dry Weight</label>
                    <InputGroup className="w-100">
                      <Input
                        className="packageInput"
                        type="text"
                        value={dry_weight}
                        name="dry_weight"
                        onChange={handleOnchangeInput}
                        onKeyPress={NumberValidator}
                      />
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>lb</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="formInput border-0">
            <label className="labelTitle">Tanks & Accommodations</label>
            <div className="row m-0">
              <div className="col-12 p-0 pt-2">
                <div className="row m-0 align-items-center">
                  <div className="col-6 p-0 DaysFormInput">
                    <label className="labelName">Fuel Tank Capacity</label>
                    <InputGroup>
                      <Input
                        className="packageInput"
                        type="text"
                        value={fuelTank_capacity}
                        name="fuelTank_capacity"
                        onChange={handleOnchangeInput}
                        onKeyPress={NumberValidator}
                      />
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>gal</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                  <div className="col-6 p-0">
                    <label className="labelName">Heads</label>
                    <div>
                      <SelectInput
                        placeholder="8"
                        value={heads_count}
                        options={HeadsNum}
                        onChange={handleOnSelectInput(`heads_count`)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="delivery_btns">
            <ButtonComp className="back_btn" onClick={handleBackScreenBoat}>
              Back
            </ButtonComp>
            <div className="post_btn">
              <CircularProgressButton
                buttonText={"Post"}
                onClick={handleSuccessfullPost.bind(
                  this,
                  "advance_boat_details",
                  inputpayload
                )}
                loading={apiLoading}
              />
            </div>
          </div>
        </div>

        <style jsx>
          {`
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .subHeading {
              font-size: 1.054vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 1.683vw 0 1.098vw 0;
            }
            .title {
              font-size: 0.951vw;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.366vw;
            }
            :global(.selectInputSec) {
              padding-top: 1.098vw;
            }
            :global(.addAddress_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              border-radius: 0 !important;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            :global(.addAddress_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            :global(.addAddress_btn button:focus),
            :global(.addAddress_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.addAddress_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }

            :global(.DaysFormInput > div) {
              width: 90%;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
            }
            :global(.DaysFormInput > div > div) {
              height: 2.489vw;
              width: 2.635vw !important;
            }
            :global(.DaysFormInput > div > div > span) {
              border: none !important;
              font-size: 0.805vw;
              width: 97%;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_2};
              background: ${BG_LightGREY_COLOR};
            }
            :global(.packageInput) {
              padding: 0.146vw 0.585vw;
              height: 2.342vw;
              font-size: 0.805vw;
              border: none !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.packageInput:focus) {
              color: #495057;
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }

            .advanceBoatDetailsPage {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }
            .advanceBoatDetailsPage .formInput {
              padding: 1.464vw 0;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              left: 0.732vw;
              width: 0.732vw;
              cursor: pointer;
            }
            :global(.labelTitle) {
              font-size: 1.098vw;
              margin-bottom: 0.366vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .labelName {
              font-size: 0.658vw;
              margin-bottom: 0.439vw !important;
              color: ${GREY_VARIANT_2};
              text-transform: "capitalize";
              font-family: "Museo-Sans" !important;
            }
            :global(.labelDesc) {
              font-size: 0.805vw;
              margin: 0;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
            }
            :global(.labelDesc a) {
              color: ${GREEN_COLOR} !important;
              text-decoration: none;
              font-family: "Open Sans" !important;
            }
            .policyDesc {
              font-size: 0.805vw;
              padding-top: 1.464vw;
              margin: 0;
              text-align: center;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
            }
            .policyDesc a {
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
            }
            .delivery_btns {
              display: flex;
              justify-content: space-between;
              align-items: center;
              padding: 0;
            }
            :global(.delivery_btns .back_btn) {
              width: 35%;
              padding: 0.732vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .back_btn span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.delivery_btns .back_btn:focus),
            :global(.delivery_btns .back_btn:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.delivery_btns .back_btn:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.delivery_btns .post_btn) {
              width: 60%;
            }
            :global(.delivery_btns .post_btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 10px 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .post_btn button span) {
              font-weight: 600;
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
            }
            :global(.delivery_btns .post_btn button:focus),
            :global(.delivery_btns .post_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.delivery_btns .post_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.advanceBoatDetailsPage .react-select__control) {
              min-height: 2.635vw !important;
              margin: 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
  };
};

export default withStyles(styles)(connect(mapStateToProps)(AdvanceBoatDetails));
