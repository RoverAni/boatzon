import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import SelectTopLabel from "../../components/input-box/select-top-label";
import {
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREEN_COLOR,
  WHITE_COLOR,
  Close_Icon,
  BG_LightGREY_COLOR,
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  Chevron_Down,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
  Radio_Darkgrey_Icon,
  THEME_COLOR_Opacity,
  Checked_Darkgrey_Icon,
  Close_White_Icon,
  Orange_Color,
  Dropdown_1,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR,
} from "../../lib/config";
// reusable component
import ButtonComp from "../../components/button/button";
import InputBox from "../../components/input-box/input-box";
import SelectInput from "../../components/input-box/select-input";
import CheckBox from "../../components/input-box/check-box";

// Reactstrap Components
import { ButtonDropdown, DropdownToggle, DropdownMenu } from "reactstrap";
import CategoryDropdown from "./category-dropdown";
import currencies from "../../translations/currency.json";
import { NumberValidator } from "../../lib/validation/validation";
import { getSubSubCateogry } from "../../services/categories";
import cloneDeep from "lodash.clonedeep";
import { getManufactures } from "../../services/manufacturer";

let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class ProductDetails extends Component {
  state = {
    negotiable: false,
    categoryName: "",
    inputpayload: {
      negotiable: false,
      currency: {
        value: CurrencyArray[0].code,
        label: CurrencyArray[0].symbol_native,
      },
    },
    isFormValid: false,
    categoryDropdown: false,
    limit: 15,
    boatManufacturerList: [],
    total_count: "",
  };

  componentDidMount() {
    let { subCategory, subSubCategory } = this.props.finalpayload;

    let tempFinalPayload = cloneDeep(this.props.finalpayload).__wrapped__;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload.negotiable = tempFinalPayload.negotiable
      ? tempFinalPayload.negotiable
      : tempPayload.negotiable;

    tempPayload.currency = tempFinalPayload.currency
      ? tempFinalPayload.currency
      : tempPayload.currency;

    this.setState(
      {
        categoryName: subCategory ? subCategory : "",
        subCategoryName: subSubCategory ? subSubCategory : "",
        subCat: subSubCategory ? true : false,
        inputpayload: { ...tempFinalPayload, ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  }

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl;
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;
    const { inputpayload, subCat, subCategoryName, categoryName } = this.state;
    isFormValid =
      inputpayload.condition &&
      inputpayload.condition.value &&
      (subCat ? subCategoryName && categoryName : categoryName) &&
      inputpayload.currency &&
      inputpayload.currency.value &&
      inputpayload.price
        ? true
        : false;
    this.setState({ isFormValid: isFormValid });
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for changing screen
  updateDropdownScreen = (screen) =>
    this.setState({ currentDropdownScreen: screen });

  handleToggleCategoryDropdown = () => {
    this.setState({
      categoryDropdown: !this.state.categoryDropdown,
    });
  };

  handleSubCategorySelection = (data) => {
    this.setState(
      {
        subCategoryName: data.subCategoryName,
      },
      () => {
        let tempInputPayload = { ...this.state.inputpayload };
        tempInputPayload.subCategory = this.state.categoryName;
        tempInputPayload.subSubCategory = this.state.subCategoryName;
        this.setState(
          {
            inputpayload: { ...tempInputPayload },
            categoryDropdown: false,
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    );
  };

  handleCategorySelection = (data) => {
    if (data && data.childCount > 0) {
      this.setState(
        {
          categoryName: data.subCategoryName,
          subCat: true,
          subCategoryName: "",
        },
        () => {
          this.setState(
            {
              categoryDropdown: true,
            },
            () => {
              getSubSubCateogry(data.subCategoryName)
                .then((res) => {
                  if (res && res.data && res.data.code == 200) {
                    let subCatList =
                      res && res.data && res.data.data && res.data.data.length
                        ? res.data.data
                        : [];
                    this.updateDropdownScreen(
                      <CategoryDropdown
                        handleCategorySelection={
                          this.handleSubCategorySelection
                        }
                        categoryName={this.state.categoryName}
                        ProductsSubCategoriesList={subCatList}
                        backtoCategoryList={this.backtoCategoryList}
                      />
                    );
                  }
                })
                .catch((err) => {
                  console.log("fwkfj--err", err);
                });
            }
          );
        }
      );
    } else if (data && data.childCount == 0) {
      this.setState(
        {
          categoryName: data.subCategoryName,
          subCategoryName: "",
          subCat: false,
        },
        () => {
          let tempInputPayload = { ...this.state.inputpayload };
          tempInputPayload.subCategory = this.state.categoryName;
          tempInputPayload.subSubCategory = this.state.subCategoryName;
          this.setState(
            {
              inputpayload: { ...tempInputPayload },
              categoryDropdown: false,
            },
            () => {
              this.checkIfFormValid();
            }
          );
        }
      );
    }
  };

  backtoCategoryList = () => {
    this.updateDropdownScreen(
      <CategoryDropdown
        handleCategorySelection={this.handleCategorySelection}
        ProductsSubCategoriesList={this.props.ProductsSubCategoriesList}
      />
    );
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    this.setState({ [name]: event.target.checked }, () => {
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[name] = this.state[name];
      this.setState({
        inputpayload: { ...tempInputPayload },
      });
    });
  };

  // Async Boat Manufacturer List API call
  handleBoatManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "1", // 0-all manufactureres,1-boats and products,2-engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.boatManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName,
                };
              })
            : [];
        this.setState({
          boatManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.boatManufacturerList &&
      this.state.boatManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.boatManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const {
      isFormValid,
      inputpayload,
      currentDropdownScreen,
      subCat,
      subCategoryName,
      categoryDropdown,
      categoryName,
      apiCallLoader,
    } = this.state;
    const {
      condition,
      manufacturer,
      currency,
      negotiable,
      price,
    } = this.state.inputpayload;
    const {
      handleDeliveryMethodPage,
      ProductsSubCategoriesList,
      onClose,
      handleBackScreen,
    } = this.props;

    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));

    const {
      handleOnSelectInput,
      handleToggleCategoryDropdown,
      handleCategorySelection,
      handleOnchangeCheckbox,
      handleOnchangeInput,
      handleBoatManufacturerList,
    } = this;

    return (
      <Wrapper>
        <div>
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        </div>
        <div className="productDetailsPage position-relative px-2">
          <h3 className="heading">Product Details</h3>
          <div className="selectInputSec">
            <SelectTopLabel
              bottomborder={true}
              label="Condition"
              mandatory={true}
              placeholder="Used"
              value={condition}
              options={Conditions}
              onChange={handleOnSelectInput(`condition`)}
            />
          </div>
          <div className="MultiSelectCheckbox categoryCheckbox">
            <label className="multiselectLabel">
              Category <span className="mandatory">*</span>
            </label>
            <ButtonDropdown
              isOpen={categoryDropdown}
              toggle={handleToggleCategoryDropdown}
              className="categoryDropdownBtn"
            >
              <DropdownToggle>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="Choose Category"
                  value={
                    subCategoryName
                      ? `${categoryName} ${" "} > ${" "} ${subCategoryName}`
                      : `${categoryName}`
                  }
                  autoComplete="off"
                ></InputBox>
                <img src={Chevron_Down} className="chevronIcon"></img>
              </DropdownToggle>
              <DropdownMenu className="categoryDropdownList">
                {!currentDropdownScreen ? (
                  <CategoryDropdown
                    handleCategorySelection={handleCategorySelection}
                    ProductsSubCategoriesList={ProductsSubCategoriesList}
                    apiCallLoader={apiCallLoader}
                  />
                ) : (
                  currentDropdownScreen
                )}
              </DropdownMenu>
            </ButtonDropdown>

            {subCat && subCategoryName == "" ? (
              <p className="errMessage">subCategory type is not selected</p>
            ) : (
              ""
            )}
          </div>
          <div className="selectInputSec">
            <SelectTopLabel
              asyncSelect={true}
              bottomborder={true}
              value={manufacturer}
              label="Product Came Off"
              // placeholder="Manufacturer List"
              loadOptions={handleBoatManufacturerList}
              additional={{
                page: 0,
              }}
              onChange={handleOnSelectInput(`manufacturer`)}
            />
          </div>
          <div className="FormInput">
            <label>
              Price <span className="mandatory">*</span>
            </label>
            <div className="d-flex align-items-center justify-content-between priceInputContainer">
              <SelectInput
                defaultValue={currency}
                value={currency}
                currency={true}
                placeholder="$"
                options={CurrencyOptions}
                onChange={handleOnSelectInput(`currency`)}
              />
              <InputBox
                type="text"
                className="priceInputBox form-control"
                name="price"
                value={price}
                onChange={handleOnchangeInput}
                autoComplete="off"
                onKeyPress={NumberValidator}
              ></InputBox>
            </div>
          </div>
          <div className="my-2">
            <CheckBox
              checked={negotiable}
              plaincheckbox={true}
              onChange={handleOnchangeCheckbox("negotiable")}
              value="negotiable"
              label="Is price negotiable?"
            />
          </div>
          <section className="d-flex mx-auto">
            <div className="backButton mr-1">
              <ButtonComp onClick={handleBackScreen}>Back</ButtonComp>
            </div>
            <div className={!isFormValid ? "inactive_Btn" : "Next_btn"}>
              <ButtonComp
                disabled={!isFormValid}
                onClick={handleDeliveryMethodPage.bind(
                  this,
                  "product",
                  inputpayload
                )}
              >
                Next
              </ButtonComp>
            </div>
          </section>
        </div>

        <style jsx>
          {`
            .chevronRight {
              width: 0.585vw;
              margin: 0 0.366vw;
            }
            :global(.categoryDropdownBtn) {
              width: 100%;
            }
            :global(.categoryDropdownBtn > button) {
              background: ${WHITE_COLOR} !important;
              border: none;
              padding: 0 !important;
              width: 100%;
              position: relative;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              padding: 0.585vw 0 0 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .chevronIcon {
              position: absolute;
              right: 0.585vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.658vw;
              height: 0.366vw;
            }
            :global(.categoryDropdownBtn > button > .inputBox) {
              width: 100%;
              height: 2.049vw;
              padding: 0.375rem 0;
              line-height: 1.5;
              background-clip: padding-box;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              border-radius: 0 !important;
              border: none !important;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              cursor: context-menu;
            }
            :global(.categoryDropdownBtn > button > .inputBox:focus) {
              box-shadow: none !important;
              outline: none !important;
            }
            :global(.categoryDropdownBtn > button:active),
            :global(.categoryDropdownBtn > button:focus) {
              outline: none !important;
              background: none !important;
              box-shadow: none !important;
            }
            :global(.categoryDropdownBtn
                > .categoryDropdownList
                > .dropdownItem) {
              display: flex !important;
              align-items: center !important;
              justify-content: space-between !important;
              padding: 0.366vw 0.585vw !important;
              cursor: pointer;
            }
            :global(.categoryDropdownBtn
                > .categoryDropdownList
                > .dropdownItem
                .catName) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
            }
            :global(.categoryDropdownList .dropdownItem:hover) {
              background: ${BG_LightGREY_COLOR} !important;
            }
            :global(.categoryDropdownList) {
              padding: 0.146vw !important;
              width: 100%;
              box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.2);
              border-radius: 0.219vw;
              border: none;
              max-height: 14.641vw;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .productDetailsPage .MultiSelectCheckbox {
              margin: 1.098vw 0 0 0;
              height: auto;
              border: none;
              position: relative;
              border-radius: 0.219vw;
            }
            .multiselectLabel {
              font-size: 0.732vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .optionContainer) {
              border: none !important;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.366vw 0.732vw;
              text-transform: capitalize;
              background: ${WHITE_COLOR} !important;
              display: flex;
              align-items: center;
              position: relative;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option
                .checkbox) {
              -webkit-appearance: none;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 0.146vw;
              display: inline-block;
              float: right;
              position: absolute;
              right: 0.732vw;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option
                .checkbox:checked:after) {
              content: url(${Checked_Darkgrey_Icon});
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer
                .option:hover) {
              background: ${THEME_COLOR_Opacity} !important;
              color: ${THEME_COLOR} !important;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .Dv7FLAST-3bZceA5TIxEN),
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                .optionListContainer) {
              z-index: 1 !important;
              margin: 0.732vw 0 0 0 !important;
              box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1) !important;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact) {
              background: ${WHITE_COLOR};
              border: none !important;
              border-radius: 0.292vw;
              width: auto;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI) {
              border-bottom: 1px solid ${Border_LightGREY_COLOR_2} !important;
              border-radius: 0 !important;
              margin: 0 !important;
              padding: 0 !important;
              display: flex;
              flex-wrap: wrap;
              align-items: center;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI
                > :first-of-type) {
              margin-left: 0 !important;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .searchBox) {
              margin-top: 0 !important;
              font-size: 0.805vw !important;
              width: 10.98vw !important;
              max-width: 10.98vw !important;
              padding: 0.366vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .searchBox::placeholder) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }

            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .chip) {
              background: ${THEME_COLOR} !important;
              color: ${BG_LightGREY_COLOR} !important;
              font-family: "Open Sans" !important;
              border-radius: 0.146vw !important;
              font-size: 0.805vw !important;
              text-transform: capitalize;
              padding: 0.292vw 0.585vw !important;
              margin: 0 0.366vw 0.366vw 0 !important;
              display: flex;
              align-items: center;
              width: fit-content;
            }
            :global(.productDetailsPage
                .categoryCheckbox
                #multiselectContainerReact
                > div
                .chip:first-child) {
              background: ${Dropdown_1} !important;
            }
            :global(.productDetailsPage
                .categoryCheckbox
                #multiselectContainerReact
                > div
                .chip:nth-child(2)) {
              background: ${Orange_Color} !important;
            }

            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .chip
                > i) {
              font-size: unset;
              width: 0.732vw;
              height: 0.951vw;
            }
            :global(.productDetailsPage
                .MultiSelectCheckbox
                #multiselectContainerReact
                > div
                .chip
                > i::before) {
              content: url(${Close_White_Icon});
              line-height: 1 !important;
            }

            .productDetailsPage .radioSelectInput,
            .productDetailsPage .checkboxSelectInput {
              margin: 1.098vw 0 0 0;
            }
            .productDetailsPage .checkboxSelectInput label,
            .productDetailsPage .radioSelectInput label {
              font-size: 0.732vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
            }
            :global(.productDetailsPage .radioSelectInput > div),
            :global(.productDetailsPage .checkboxSelectInput > div) {
              min-width: fit-content;
              height: 2.342vw;
              position: relative;
            }
            :global(.productDetailsPage .radioSelectInput > div > button),
            :global(.productDetailsPage .checkboxSelectInput > div > button) {
              position: relative;
              height: 2.342vw;
              border-radius: 0;
              border: none !important;
              border-bottom: 1px solid ${Border_LightGREY_COLOR_2} !important;
              padding: 0 0.366vw !important;
            }
            :global(.productDetailsPage
                .radioSelectInput
                > div
                > button
                > span),
            :global(.productDetailsPage
                .checkboxSelectInput
                > div
                > button
                > span) {
              font-size: 0.805vw;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              align-items: center;
              position: relative;
              padding: 0 0.732vw 0 0;
            }
            :global(.productDetailsPage
                .radioSelectInput
                > div
                > button
                > span) {
              display: inline-block;
              min-width: 90%;
              width: max-content;
            }
            :global(.productDetailsPage
                .checkboxSelectInput
                > div
                > button
                > span) {
              display: inline-block;
              width: 94%;
            }
            :global(.productDetailsPage .radioSelectInput > div > button:focus),
            :global(.productDetailsPage
                .radioSelectInput
                > div
                > button:active),
            :global(.productDetailsPage
                .checkboxSelectInput
                > div
                > button:focus),
            :global(.productDetailsPage
                .checkboxSelectInput
                > div
                > button:active) {
              outline: none !important;
            }
            :global(.productDetailsPage
                .radioSelectInput
                > div
                > button::after),
            :global(.productDetailsPage
                .checkboxSelectInput
                > div
                > button::after) {
              content: url(${Chevron_Down});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.292vw !important;
              position: relative;
              top: -0.146vw;
              left: 0.146vw;
            }
            :global(.productDetailsPage
                .radioSelectInput
                > div
                > button::after) {
              left: 0.732vw;
            }
            :global(.productDetailsPage .radioSelectInput .picky__dropdown),
            :global(.productDetailsPage .checkboxSelectInput .picky__dropdown) {
              min-width: 100% !important;
              width: max-content !important;
              overflow: auto !important;
              padding: 0.585vw 0.512vw !important;
              margin: 0.366vw 0 0 0;
              box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1) !important;
              z-index: 1 !important;
            }
            :global(.productDetailsPage
                .radioSelectInput
                .picky__dropdown
                > div
                > div),
            :global(.productDetailsPage
                .checkboxSelectInput
                .picky__dropdown
                > div
                > div) {
              width: auto;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              border-bottom: none !important;
              letter-spacing: 0.0146vw !important;
              text-transform: capitalize;
              background: ${WHITE_COLOR} !important;
              display: flex;
              align-items: center;
            }
            :global(.productDetailsPage
                .radioSelectInput
                .picky__dropdown
                > div
                > div
                input) {
              -webkit-appearance: none;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 1.83vw;
              display: block;
              position: relative;
              margin-right: 0.366vw !important;
            }
            :global(.productDetailsPage
                .radioSelectInput
                .picky__dropdown
                > div
                > div
                input:checked::after) {
              content: url(${Radio_Darkgrey_Icon});
              position: absolute;
              top: 50% !important;
              transform: translate(0, -50%) !important;
              right: 0.146vw !important;
            }
            .productDetailsPage {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }
            .longArrowLeftIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.024vw;
              cursor: pointer;
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              left: 0.732vw;
              width: 0.732vw;
              cursor: pointer;
            }
            :global(.productDetailsPage .selectInputSec),
            .productDetailsPage .FormInput {
              margin-top: 1.098vw;
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            :global(.productDetailsPage .FormInput label) {
              font-size: 0.732vw !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0;
              color: ${GREY_VARIANT_2};
            }
            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: fit-content;
              flex: 1;
            }
            :global(.productDetailsPage .FormInput > .priceInputContainer) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.priceInputContainer .react-select__control) {
              min-height: 2.342vw !important;
            }

            :global(.productDetailsPage .FormInput .priceInputBox) {
              width: 100%;
              height: 2.049vw;
              padding: 0.439vw 0 0.439vw 0.366vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.productDetailsPage .FormInput .priceInputBox > input) {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.productDetailsPage .FormInput .priceInputBox:focus),
            :global(.productDetailsPage .FormInput .priceInputBox:active) {
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.productDetailsPage
                .FormInput
                .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
            :global(.productDetailsPage .FormInput .priceInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.productDetailsPage .FormInput .priceInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            :global(.productDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.productDetailsPage .FormInput .inputBox:focus),
            :global(.productDetailsPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            .Next_btn,
            .inactive_Btn {
              // width: 70%;
              width: 11.77083vw;
              margin: 0.732vw 0 0 0;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.Next_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }

            :global(.inactive_Btn button span),
            :global(.backButton button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 200;
              color: ${GREY_VARIANT_1};
              font-size: 1.024vw;
            }
            :global(.Next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 200;
              color: ${WHITE_COLOR};
              font-size: 1.024vw;
            }
            .backButton {
              width: 5.72916vw;
              margin: 0.732vw 0 0 0;
            }
            :global(.backButton button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.backButton button:focus),
            :global(.backButton button:active),
            :global(.backButton button:hover) {
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              outline: none;
              box-shadow: none;
            }
            :global(.Next_btn button:focus),
            :global(.Next_btn button:active) {
              background: ${GREEN_COLOR} !important;
              outline: none;
              box-shadow: none;
              color: ${WHITE_COLOR};
            }
            :global(.Next_btn button:hover) {
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ProductsSubCategoriesList: state.ProductsSubCategoriesList,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(ProductDetails);
