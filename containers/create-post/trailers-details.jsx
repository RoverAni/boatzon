import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import SelectTopLabel from "../../components/input-box/select-top-label";
import {
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREEN_COLOR,
  WHITE_COLOR,
  Close_Icon,
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_3,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import { NumberValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import { getYearsList } from "../../lib/date-operation/date-operation";
import moment from "moment";
import { DatePicker } from "antd";
const YearsList = getYearsList();
import { connect } from "react-redux";
import currencies from "../../translations/currency.json";
import SelectInput from "../../components/input-box/select-input";
import CircularProgressButton from "../../components/button-loader/button-loader";
import cloneDeep from "lodash.clonedeep";
import { getManufactures } from "../../services/manufacturer";

let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class TrailerDetails extends Component {
  state = {
    inputpayload: {
      currency: {
        value: CurrencyArray[0].code,
        label: CurrencyArray[0].symbol_native,
      },
    },
    isFormValid: false,
    limit: 15,
    trailerManufacturerList: [],
    total_count: "",
  };

  componentDidMount() {
    let tempFinalPayload = cloneDeep(this.props.finalpayload).__wrapped__;
    let tempPayload = { ...this.state.inputpayload };

    tempPayload.currency = tempFinalPayload.currency
      ? tempFinalPayload.currency
      : tempPayload.currency;

    this.setState(
      {
        inputpayload: { ...tempFinalPayload, ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  }

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = inputControl;
      this.setState(
        {
          inputpayload: tempPayload,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    const { inputpayload } = this.state;
    isFormValid = (
      inputpayload.hasWarranty && inputpayload.hasWarranty.value == "Yes"
        ? inputpayload.warrantyDate
        : true
    )
      ? true &&
        inputpayload.condition &&
        inputpayload.condition.value &&
        inputpayload.trailerType &&
        inputpayload.trailerType.value &&
        inputpayload.currency &&
        inputpayload.currency.value &&
        inputpayload.price &&
        inputpayload.trailerYear &&
        inputpayload.trailerYear.value &&
        inputpayload.length &&
        inputpayload.axies &&
        inputpayload.axies.value
        ? true
        : false
      : false;

    this.setState({ isFormValid: isFormValid });
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  handleMonthPicker = (event) => {
    if (event) {
      let Date = event._d;
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[`warrantyDate`] = moment(Date);
      this.setState(
        {
          inputpayload: { ...tempInputPayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else if (event == null) {
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[`warrantyDate`] = "";
      this.setState(
        {
          inputpayload: { ...tempInputPayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Async Boat Manufacturer List API call
  handleTrailerManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "3", // 0-all manufactureres,1-boats and products,2-engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.trailerManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName,
                };
              })
            : [];
        this.setState({
          trailerManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.trailerManufacturerList &&
      this.state.trailerManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.trailerManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const AxiesNum = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
    ];

    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    const TrailerType =
      this.props.TrailerData &&
      this.props.TrailerData.length &&
      this.props.TrailerData.map((data) => {
        return { value: data, label: data };
      });

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));

    const warrantyConditions = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const { isFormValid, inputpayload } = this.state;
    const {
      condition,
      trailerType,
      manufacturer,
      trailerYear,
      length,
      axies,
      hasWarranty,
      warrantyDate,
      currency,
      price,
    } = this.state.inputpayload;
    const {
      handleOnSelectInput,
      handleOnchangeInput,
      handleMonthPicker,
      disabledDate,
      handleTrailerManufacturerList,
    } = this;
    const {
      onClose,
      handleSuccessfullPost,
      handleBackScreen,
      apiLoading,
    } = this.props;

    return (
      <Wrapper>
        <div>
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        </div>
        <div className="trailerDetailsPage px-2">
          <h3 className="heading">Trailer Details</h3>
          <div className="selectInputSec">
            <SelectTopLabel
              mandatory={true}
              bottomborder={true}
              label="Condition"
              placeholder="Select Condition"
              value={condition}
              options={Conditions}
              onChange={handleOnSelectInput(`condition`)}
            />
          </div>
          <div className="selectInputSec">
            <SelectTopLabel
              bottomborder={true}
              mandatory={true}
              label="Trailer Type"
              placeholder="Select Type"
              value={trailerType}
              options={TrailerType}
              onChange={handleOnSelectInput(`trailerType`)}
            />
          </div>
          <div className="selectInputSec">
            <SelectTopLabel
              asyncSelect={true}
              mandatory={true}
              bottomborder={true}
              value={manufacturer}
              label="Manufacturer"
              placeholder="Manufacturer List"
              loadOptions={handleTrailerManufacturerList}
              additional={{
                page: 0,
              }}
              onChange={handleOnSelectInput(`manufacturer`)}
            />
          </div>
          <div className="row">
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  mandatory={true}
                  bottomborder={true}
                  label="Year"
                  value={trailerYear}
                  placeholder="ex: 2013"
                  options={YearOptions}
                  onChange={handleOnSelectInput(`trailerYear`)}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="FormInput">
                <label>
                  Length (Feet) <span className="mandatory">*</span>
                </label>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="length"
                  value={length}
                  placeholder="50'"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row flex-wrap">
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  mandatory={true}
                  value={axies}
                  bottomborder={true}
                  label="Axies"
                  placeholder="5"
                  options={AxiesNum}
                  onChange={handleOnSelectInput(`axies`)}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  mandatory={true}
                  bottomborder={true}
                  value={hasWarranty}
                  label="Has Warranty"
                  placeholder="Yes"
                  options={warrantyConditions}
                  onChange={handleOnSelectInput(`hasWarranty`)}
                />
              </div>
            </div>
            <div className="col-6">
              {hasWarranty && hasWarranty.value == "Yes" ? (
                <div className="FormInput">
                  <label>
                    Warranty Till?{" "}
                    {hasWarranty && hasWarranty.value == "Yes" ? (
                      <span className="mandatory">*</span>
                    ) : (
                      ""
                    )}
                  </label>
                  <DatePicker
                    picker="month"
                    format="MM-YYYY"
                    placeholder="MM-YYYY"
                    value={warrantyDate}
                    onChange={handleMonthPicker}
                    disabledDate={disabledDate}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </div>

          <div className="FormInput">
            <label>
              Price <span className="mandatory">*</span>
            </label>
            <div className="d-flex align-items-center justify-content-between priceInputContainer">
              <SelectInput
                defaultValue={currency}
                value={currency}
                currency={true}
                placeholder="$"
                options={CurrencyOptions}
                onChange={handleOnSelectInput(`currency`)}
              />
              <InputBox
                type="text"
                className="priceInputBox form-control"
                name="price"
                value={price}
                onChange={handleOnchangeInput}
                autoComplete="off"
                onKeyPress={NumberValidator}
              ></InputBox>
            </div>
          </div>
          <div className="delivery_btns">
            <ButtonComp className="back_btn" onClick={handleBackScreen}>
              Back
            </ButtonComp>
            <div className={!isFormValid ? "inactive_Btn" : "post_btn"}>
              <CircularProgressButton
                buttonText={"Post"}
                disabled={!isFormValid}
                onClick={handleSuccessfullPost.bind(
                  this,
                  "trailerdetails",
                  inputpayload
                )}
                loading={apiLoading}
              />
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .trailerDetailsPage {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }

            .closeIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            :global(.trailerDetailsPage .selectInputSec),
            .trailerDetailsPage .FormInput {
              padding-top: 1.098vw;
              position: relative;
            }
            :global(.ant-picker-dropdown) {
              z-index: 1500 !important;
            }
            :global(.ant-picker) {
              cursor: pointer;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-disabled) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.ant-picker-focused) {
              outline: 0;
              box-shadow: none !important;
            }
            :global(.ant-picker-input input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-input input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
            :global(.trailerDetailsPage .FormInput label) {
              font-size: 0.732vw !important;
              font-family: "Museo-Sans" !important;
              margin: 0;
              padding: 0;
              color: ${GREY_VARIANT_2};
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            :global(.trailerDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 1px solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.trailerDetailsPage .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_3};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }

            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: fit-content;
              flex: 1;
            }
            :global(.trailerDetailsPage .FormInput > .priceInputContainer) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.priceInputContainer .react-select__control) {
              min-height: 2.342vw !important;
            }

            :global(.trailerDetailsPage .FormInput .priceInputBox) {
              width: 100%;
              height: 2.049vw;
              padding: 0.439vw 0 0.439vw 0.366vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.trailerDetailsPage .FormInput .priceInputBox > input) {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.trailerDetailsPage .FormInput .priceInputBox:focus),
            :global(.trailerDetailsPage .FormInput .priceInputBox:active) {
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.trailerDetailsPage
                .FormInput
                .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
            :global(.trailerDetailsPage .FormInput .priceInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.trailerDetailsPage .FormInput .priceInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
            .delivery_btns {
              display: flex;
              justify-content: space-between;
              align-items: center;
              padding: 1.464vw 0;
            }
            :global(.delivery_btns .back_btn) {
              width: 35%;
              padding: 0.732vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .back_btn span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.delivery_btns .back_btn:focus),
            :global(.delivery_btns .back_btn:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.delivery_btns .back_btn:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.delivery_btns .inactive_Btn),
            :global(.delivery_btns .post_btn) {
              width: 60%;
            }
            :global(.delivery_btns .inactive_Btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .post_btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .post_btn button span),
            :global(.delivery_btns .inactive_Btn button span) {
              font-weight: 600;
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
            }
            :global(.delivery_btns .post_btn button:focus),
            :global(.delivery_btns .post_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.delivery_btns .post_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    TrailerData: state.TrailerData,
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(TrailerDetails);
