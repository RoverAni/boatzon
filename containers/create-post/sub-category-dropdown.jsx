import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
  Chevron_Left_Darkgrey,
} from "../../lib/config";
// Reactstrap Components
import { DropdownItem } from "reactstrap";

class SubCategoryDropdown extends Component {
  state = {
    SubCategoryList: [],
  };
  componentDidMount() {
    this.setState({
      SubCategoryList: this.props.subCatList,
    });
  }

  render() {
    const { SubCategoryList } = this.state;
    const {
      backtoCategoryList,
      categoryName,
      handleSubCategorySelection,
    } = this.props;
    console.log("vnsjc", SubCategoryList);
    return (
      <Wrapper>
        <h6 className="catName" onClick={backtoCategoryList}>
          <img src={Chevron_Left_Darkgrey}></img> <span>{categoryName}</span>
        </h6>
        {SubCategoryList &&
          SubCategoryList.map((data, index) => (
            <DropdownItem
              className="subCatdropdownItem"
              key={index}
              onClick={handleSubCategorySelection.bind(this, data)}
            >
              <span className="subcatName">{data}</span>
            </DropdownItem>
          ))}
        <style jsx>
          {`
            .catName {
              display: flex;
              align-items: center;
              padding: 0.292vw;
              border-bottom: 0.0732vw solid ${BG_LightGREY_COLOR};
              cursor: pointer;
            }
            .catName span {
              font-size: 0.951vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans-Semibold" !important;
            }
            .catName img {
              width: 0.585vw;
              margin-right: 0.585vw;
            }
            :global(.subCatdropdownItem) {
              display: flex !important;
              align-items: center !important;
              justify-content: space-between !important;
              padding: 0.366vw 0.585vw !important;
              cursor: pointer;
            }
            :global(.subCatdropdownItem .subcatName) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
            }
            :global(.subCatdropdownItem:hover) {
              background: ${BG_LightGREY_COLOR} !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SubCategoryDropdown;
