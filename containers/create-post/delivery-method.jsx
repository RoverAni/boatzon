import React, { Component } from "react";
import { connect } from "react-redux";
// wrapping component
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREEN_COLOR,
  Border_LightGREY_COLOR_2,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  WHITE_COLOR,
  THEME_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_3,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR_1,
  InfoIcon,
} from "../../lib/config";
import SwitchInput from "../../components/input-box/switch-input";
// reusable component
import ButtonComp from "../../components/button/button";
import {
  storeShippingAddress,
  getAllShippingAddress,
} from "../../services/contact";
// reactstrap components
import { InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import SelectInput from "../../components/input-box/select-input";
import Model from "../../components/model/model";
import MyLocationModel from "../filter-sec/my-location-model";
import CircularProgressButton from "../../components/button-loader/button-loader";
import { validateParcel } from "../../services/product-details";
import { getCookie } from "../../lib/session";
import { getConnectedStripeAccount } from "../../services/transaction";

const addAddress = (
  <div className="addAddress_btn">
    <ButtonComp>Add Address</ButtonComp>
  </div>
);

class DeliveryMethod extends Component {
  state = {
    inputpayload: {},
    newAddress: "",
    selectedAddress: {},
    addressID: "",
    AddressList: [
      {
        value: "",
        label: addAddress,
      },
    ],
    isFormValid: false,
    addAddressModel: false,
    isConnectedUser: false,
    isServiceNationwide: false,
    isFirstTime: true,
  };

  componentDidMount = () => {
    let tempPayload = [...this.state.AddressList];
    let currentAddress = {
      value: {
        address: this.props.currentLocation.address,
        lat: this.props.currentLocation.latitude,
        lng: this.props.currentLocation.longitude,
        city: this.props.currentLocation.city,
        countrySname: this.props.currentLocation.countryShortName,
      },
      label: this.props.currentLocation.address,
    };
    tempPayload.unshift(currentAddress);
    this.setState(
      {
        selectedAddress: { ...currentAddress },
      },
      () => {
        let tempInputpayload = { ...this.state.inputpayload };
        tempInputpayload.selectedAddress = this.state.selectedAddress.value;
        this.setState(
          {
            AddressList: [...tempPayload],
            inputpayload: { ...tempInputpayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    );
    getConnectedStripeAccount(getCookie("mqttId")).then((res) => {
      if (res.status === "200" && res.data.data === true) {
        this.setState({ isConnectedUser: true });
      }
      console.log("res", res);
    });
    getAllShippingAddress(0, 20).then((res) => {
      if (res.data.data.length > 0) {
        this.setState({ isFirstTime: false });
      }
    });
  };

  // used to create inputpayload
  handleOnchangeInput = (name) => (e) => {
    let temppayload = { ...this.state.inputpayload };
    temppayload[[name]] = e.target.value;
    this.setState({ inputpayload: temppayload });
  };

  // used to store the switch checked status
  handleChange = (name) => (event) => {
    let inputControl = event;
    this.setState({ [name]: inputControl.target.checked }, () => {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[[name]] = this.state[name];
      this.setState({
        inputpayload: tempPayload,
      });
    });
  };

  handleCloseAddAddress = () => {
    this.setState({
      addAddressModel: false,
    });
  };

  // handleGooglePlacesPress = (details) => {
  //   var StreetNumber = null;
  //   var StreetName = null;
  //   var City = null;
  //   var State = null;
  //   var Country = null;
  //   var PostalCode = null;

  //   const addressComponents = details.address_components;
  //   addressComponents.forEach((addressComponent) => {
  //     const addressType = addressComponent.types[0];
  //     if (addressType == "street_number") {
  //       StreetNumber = addressComponent.long_name;
  //     }
  //     if (addressType == "route") {
  //       StreetName = addressComponent.long_name;
  //     }
  //     if (addressType == "sublocality_level_1") {
  //       City = addressComponent.long_name;
  //     }
  //     if (addressType == "locality") {
  //       City = addressComponent.long_name;
  //     }
  //     if (addressType == "administrative_area_level_1") {
  //       State = addressComponent.long_name;
  //     }
  //     if (addressType == "postal_code") {
  //       PostalCode = addressComponent.long_name;
  //     }
  //     if (addressType == "country") {
  //       Country = addressComponent.long_name;
  //     }
  //   });
  //   return {
  //     StreetNumber,
  //     StreetName,
  //     City,
  //     State,
  //     Country,
  //     PostalCode,
  //   };
  // };

  _storeShippingAddress = async (AddressData) => {
    let obj = {
      userId: getCookie("mqttId"),
      countryCode: AddressData.countryShortName,
      country: AddressData.countryShortName,
      city: AddressData.city,
      postalCode: AddressData.zipCode,
      state: AddressData.area,
      address: AddressData.address,
      defaultAddress: this.state.isFirstTime,
      phone: 15553419393,
    };
    console.log("AddressData", AddressData);
    let res = await storeShippingAddress(obj, "");
    console.log("_storeShippingAddress", res);
    this.setState({ addressID: res.data.data });
  };

  handleAddAddress = (AddressData) => {
    this.setState(
      {
        newAddress: AddressData.address,
        newAddressLat: AddressData.lat,
        newAddressLng: AddressData.lng,
        newAddresscity: AddressData.city,
        newAddresscountrySname: AddressData.country,
      },
      () => {
        let tempPayload = [...this.state.AddressList];
        let elementPos = tempPayload.findIndex((data) => {
          return data.value == this.state.newAddress;
        });
        if (elementPos == -1) {
          let payload = {
            address: this.state.newAddress,
            lat: this.state.newAddressLat,
            lng: this.state.newAddressLng,
            city: this.state.newAddresscity,
            countrySname: this.state.newAddresscountrySname,
          };
          let selectPayload = {
            value: { ...payload },
            label: this.state.newAddress,
          };
          tempPayload.unshift(selectPayload);
          let tempInputpayload = { ...this.state.inputpayload };
          tempInputpayload.selectedAddress = selectPayload.value;
          this.setState({
            AddressList: [...tempPayload],
            selectedAddress: { ...selectPayload },
            inputpayload: tempInputpayload,
            addAddressModel: false,
          });
        }
      }
    );
    this._storeShippingAddress(AddressData);
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl.value == "") {
      this.setState({
        addAddressModel: true,
      });
    }
    if (inputControl && inputControl.length > 0) {
      let tempArray = this.state[name];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = { ...this.state.inputpayload };
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl.value;

      let tempSelectedAddress = {
        value: inputControl.value,
        label: inputControl.label,
      };
      let tempInputpayload = { ...this.state.inputpayload };
      tempInputpayload.selectedAddress = tempSelectedAddress.value;
      this.setState(
        {
          [name]: { ...tempSelectedAddress },
          inputpayload: { ...tempInputpayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  _validateParcelFunc = async () => {
    const { handleSuccessfullPost } = this.props;
    let data = {};
    if (this.state.isServiceNationwide) {
      data = {
        ...this.state.inputpayload,
        distance_unit: "in",
        mass_unit: "lb",
        nationWide: true,
        addressID: this.state.addressID,
      };
      let obj = {
        width: this.state.inputpayload.packageWidth,
        height: this.state.inputpayload.packageHeight,
        distance_unit: "in",
        weight: this.state.inputpayload.packageWeightLB,
        mass_unit: "lb",
        package_length: this.state.inputpayload.packageLength,
      };
      if (this.state.isConnectedUser) {
        let validateParcelObj = await validateParcel(obj);
        if (
          validateParcelObj.status === 400 ||
          validateParcelObj.status === "400"
        ) {
          this.props.remoteSnackbarHandler(
            validateParcelObj.data.message,
            validateParcelObj.status
          );
        } else if (
          validateParcelObj.status === 200 ||
          validateParcelObj.status === "200"
        ) {
          this.props.remoteSnackbarHandler(
            validateParcelObj.data.message || "Parcel validation successfull",
            parseInt(validateParcelObj.status)
          );
          handleSuccessfullPost("deliveryPage", data);
        } else {
          this.props.remoteSnackbarHandler(
            validateParcelObj.data.message || "Something went wrong",
            parseInt(validateParcelObj.status)
          );
        }
      } else {
        this.props.remoteSnackbarHandler(
          "Please connect your account to receive payments before you proceed to post ad",
          404
        );
      }
    } else {
      data = {
        ...this.state.inputpayload,
        nationWide: false,
      };
      handleSuccessfullPost("deliveryPage", data);
    }
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;

    isFormValid = this.state.selectedAddress ? true : false;
    this.setState({ isFormValid: isFormValid });
  };

  render() {
    const serviceNationwideLabel = (
      <div>
        <h6 className="labelTitle">Sell & Ship Nationwide</h6>
        <p className="labelDesc">
          A low 9.9% service fee applies <a target="_blank">Shipping policy</a>
        </p>
      </div>
    );

    const buyNowEnabledLabel = (
      <div>
        <div className="d-flex align-items-center position-relative">
          <img
            src={InfoIcon}
            alt="info"
            height={9}
            width={10}
            className="infoIcon"
          />
          <h6 className="labelTitle">Enable Buy Now</h6>
          {/* <img src={Question_LightGrey_Icon} className="infoIcon"></img> */}
        </div>
        <p className="labelDesc">Auto-accept first full-price shipping offer</p>
      </div>
    );
    const {
      isFormValid,
      isServiceNationwide,
      buyNowEnabled,
      selectedAddress,
      addAddressModel,
    } = this.state;
    const {
      handleBackScreen,
      onClose,
      apiLoading,
    } = this.props;
    const addressList = this.state.AddressList;
    const {
      handleChange,
      handleOnchangeInput,
      handleCloseAddAddress,
      handleAddAddress,
    } = this;

    return (
      <Wrapper>
        <div>
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        </div>
        <div className="deliveryMethodPage px-2">
          <h3 className="heading">Delivery Method</h3>
          <div className="formInput">
            <SwitchInput
              triggerOnChange={true}
              label={serviceNationwideLabel}
              onChange={handleChange("isServiceNationwide")}
              value={isServiceNationwide}
              name="isServiceNationwide"
            />
          </div>
          <div className="formInput">
            <SwitchInput
              triggerOnChange={true}
              label={buyNowEnabledLabel}
              onChange={handleChange("buyNowEnabled")}
              value={buyNowEnabled}
              name="buyNowEnabled"
            />
          </div>
          {isServiceNationwide ? (
            <div className="formInput">
              <label className="labelTitle">Estimate the item’s weight:</label>
              <div className="row m-0">
                <div className="col-8 p-0">
                  <label className="labelName">Package weight</label>
                  <div className="row m-0 align-items-center">
                    <div className="col-6 p-0 DaysFormInput">
                      <InputGroup>
                        <Input
                          className="packageInput"
                          type="text"
                          name="packageWeightLB"
                          onChange={handleOnchangeInput(`packageWeightLB`)}
                        />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>lb</InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                    <div className="col-6 p-0 DaysFormInput">
                      <InputGroup>
                        <Input
                          className="packageInput"
                          type="text"
                          name="packageWeightOZ"
                          onChange={handleOnchangeInput(`packageWeightOZ`)}
                        />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>oz</InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row m-0 pt-3">
                <div className="col-12 p-0">
                  <label className="labelName">Package dimensions</label>
                  <div className="row m-0 align-items-center">
                    <div className="col-4 p-0 DaysFormInput">
                      <InputGroup>
                        <Input
                          className="packageInput"
                          type="text"
                          name="packageLength"
                          onChange={handleOnchangeInput(`packageLength`)}
                        />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>in</InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                    <div className="col-4 p-0 DaysFormInput">
                      <InputGroup>
                        <Input
                          className="packageInput"
                          type="text"
                          name="packageWidth"
                          onChange={handleOnchangeInput(`packageWidth`)}
                        />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>in</InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                    <div className="col-4 p-0 DaysFormInput">
                      <InputGroup>
                        <Input
                          className="packageInput"
                          type="text"
                          name="packageHeight"
                          onChange={handleOnchangeInput(`packageHeight`)}
                        />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>in</InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            "  "
          )}
          <div className="formInput">
            <label className="labelTitle">
              Ship from: <span className="mandatory">*</span>
            </label>
            <SelectInput
              id="addressInput"
              options={addressList}
              value={selectedAddress}
              onChange={this.handleOnSelectInput(`selectedAddress`)}
              buttonOption={true}
            />
          </div>
          <p className="policyDesc">
            By posting nationwide you agree to our shipping policy and{" "}
            <a target="_blank">Terms of Service</a>
          </p>
          <div className="delivery_btns">
            <ButtonComp className="back_btn" onClick={handleBackScreen}>
              Back
            </ButtonComp>
            <div className={!isFormValid ? "inactive_Btn" : "post_btn"}>
              <CircularProgressButton
                buttonText={"Post"}
                disabled={!isFormValid}
                onClick={() => this._validateParcelFunc()}
                // onClick={handleSuccessfullPost.bind(this, "deliveryPage", inputpayload)}
                loading={apiLoading}
              />
            </div>
          </div>
        </div>
        <Model open={addAddressModel} onClose={handleCloseAddAddress}>
          <MyLocationModel
            createPost={true}
            onClose={handleCloseAddAddress}
            handleAddAddress={handleAddAddress}
          />
        </Model>
        <style jsx>
          {`
            :global(.infoIcon) {
              position: absolute;
              left: 8.7vw;
              top: -2px;
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            :global(.addAddress_btn button) {
              width: 100%;
              padding: 0.219vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              border-radius: 0 !important;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Light" !important;
            }
            :global(.addAddress_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-size: 0.878vw;
            }
            :global(.addAddress_btn button:focus),
            :global(.addAddress_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.addAddress_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .DaysFormInput {
              position: relative;
            }
            :global(.DaysFormInput > div) {
              width: 90%;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
            }
            :global(.DaysFormInput > div > div) {
              height: 2.489vw;
              width: 2.635vw !important;
            }
            :global(.DaysFormInput > div > div > span) {
              border: none !important;
              font-size: 0.805vw;
              width: 97%;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_2};
              background: ${BG_LightGREY_COLOR};
            }
            :global(.packageInput) {
              padding: 0.146vw 0.585vw;
              height: 2.342vw;
              font-size: 0.878vw;
              border: none !important;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.packageInput:focus) {
              color: #495057;
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }

            .deliveryMethodPage {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }
            .deliveryMethodPage .formInput {
              padding: 1.464vw 0;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            :global(.labelTitle) {
              font-size: 1.024vw;
              margin-bottom: 0.366vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .labelName {
              font-size: 0.951vw;
              margin-bottom: 0.292vw !important;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.labelDesc) {
              font-size: 0.729vw;
              margin: 0;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
            }
            :global(.labelDesc a) {
              color: ${GREEN_COLOR} !important;
              text-decoration: none;
              font-family: "Open Sans" !important;
            }
            .policyDesc {
              font-size: 0.729vw;
              padding-top: 1.464vw;
              margin: 0;
              text-align: center;
              color: ${GREY_VARIANT_2};
              text-transform: initial;
              font-family: "Open Sans" !important;
            }
            .policyDesc a {
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
            }
            .delivery_btns {
              display: flex;
              justify-content: space-between;
              align-items: center;
              padding: 1.464vw 0;
            }
            :global(.delivery_btns .back_btn) {
              width: 35%;
              padding: 0.732vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .back_btn span) {
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.delivery_btns .back_btn:focus),
            :global(.delivery_btns .back_btn:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.delivery_btns .back_btn:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.delivery_btns .inactive_Btn),
            :global(.delivery_btns .post_btn) {
              width: 60%;
            }
            :global(.delivery_btns .inactive_Btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .post_btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.delivery_btns .post_btn button span),
            :global(.delivery_btns .inactive_Btn button span) {
              font-weight: 600;
              font-size: 1.024vw;
              font-family: "Museo-Sans" !important;
            }
            :global(.delivery_btns .post_btn button:focus),
            :global(.delivery_btns .post_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.delivery_btns .post_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .infoIcon {
              width: 1.042vw;
              margin-left: 0.366vw;
              margin-bottom: 0.366vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(DeliveryMethod);
