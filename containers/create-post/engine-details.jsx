import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import SelectTopLabel from "../../components/input-box/select-top-label";
import {
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREEN_COLOR,
  WHITE_COLOR,
  Close_Icon,
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_3,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import { NumberValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import CheckBox from "../../components/input-box/check-box";
import { getYearsList } from "../../lib/date-operation/date-operation";
import moment from "moment";
import { DatePicker } from "antd";
const YearsList = getYearsList();
import SelectInput from "../../components/input-box/select-input";
import currencies from "../../translations/currency.json";
import CircularProgressButton from "../../components/button-loader/button-loader";
import { getManufactures, getModels } from "../../services/manufacturer";

let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class EngineDetails extends Component {
  state = {
    inputpayload: {
      num_of_engines: 1,
      currency: {
        value: CurrencyArray[0].code,
        label: CurrencyArray[0].symbol_native,
      },
      num_of_engines: {
        value: 1,
        label: 1,
      },
    },
    selectedType: [],
    lengthDropdown: false,
    lengthValue: [20, 50],
    isFormValid: false,
    engineNotAvaiable: false,

    limit: 15,
    engineManufacturerList: [],
    engineModelList: [],
    total_count: "",
    model_total_count: "",
  };

  componentDidMount() {
    let {
      engineHorsePower,
      engineHour,
      engineNotAvaiable,
      price,
      num_of_engines,
      currency,
    } =
      this.props.finalpayload.AllEngineData &&
      this.props.finalpayload.AllEngineData[0]
        ? this.props.finalpayload.AllEngineData[0]
        : "";

    this.setState(
      {
        engineHorsePower: engineHorsePower != undefined ? engineHorsePower : "",
        engineHour: engineHour != undefined ? engineHour : "",
        engineNotAvaiable: engineNotAvaiable
          ? engineNotAvaiable
          : this.state.engineNotAvaiable,
        price: price ? price : "",
      },
      () => {
        let tempPayload = {
          ...(this.props.finalpayload.AllEngineData &&
          this.props.finalpayload.AllEngineData[0]
            ? this.props.finalpayload.AllEngineData[0]
            : ""),
        };
        tempPayload.engineNotAvaiable = this.state.engineNotAvaiable;
        tempPayload.num_of_engines = num_of_engines
          ? num_of_engines
          : this.state.inputpayload.num_of_engines;
        tempPayload.currency = currency
          ? currency
          : this.state.inputpayload.currency;
        this.setState(
          {
            inputpayload: {
              ...tempPayload,
            },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    );
  }

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;

    if (inputControl && inputControl.value) {
      if (name == "num_of_engines") {
        this.setState(
          {
            engineNotAvaiable:
              parseInt(inputControl.value, 10) != 0 ? false : true,
          },
          () => {
            let tempInputPayload = { ...this.state.inputpayload };
            tempInputPayload.engineNotAvaiable = this.state.engineNotAvaiable;
            this.setState({
              inputpayload: { ...tempInputPayload },
            });
          }
        );
      }
      if (name == "engineMake") {
        let Payload = { ...this.state.inputpayload };
        Payload.engineModel = {};
        Payload[[name]] = inputControl;
        this.setState(
          {
            inputpayload: { ...Payload },
            engineModelList: [],
          },
          () => {
            this.checkIfFormValid();
          }
        );
      } else {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[[name]] = inputControl;
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: tempPayload,
        [inputControl.name]: inputControl.value,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;
    const { inputpayload, engineNotAvaiable } = this.state;
    const { finalpayload } = this.props;
    if (finalpayload.category == "Engines") {
      isFormValid = (
        inputpayload.hasWarranty && inputpayload.hasWarranty.value == "Yes"
          ? inputpayload.warrantyDate
          : true
      )
        ? true &&
          inputpayload.engineMake &&
          inputpayload.engineMake.value &&
          inputpayload.engineModel &&
          inputpayload.engineModel.value &&
          inputpayload.engineYear &&
          inputpayload.engineYear.value &&
          inputpayload.engineHorsePower &&
          inputpayload.engineHour &&
          inputpayload.engineType &&
          inputpayload.engineType.value &&
          inputpayload.engineFuelType &&
          inputpayload.engineFuelType.value &&
          inputpayload.num_of_engines &&
          inputpayload.num_of_engines.value &&
          inputpayload.condition &&
          inputpayload.condition.value &&
          inputpayload.currency &&
          inputpayload.currency.value &&
          inputpayload.price
          ? true
          : false
        : false;
    } else {
      isFormValid = engineNotAvaiable
        ? // (finalpayload.hasWarranty && finalpayload.hasWarranty.value == "Yes"
          //   ? inputpayload.warrantyDate
          //   : true)
          true
        : inputpayload.engineMake &&
          inputpayload.engineMake.value &&
          inputpayload.engineModel &&
          inputpayload.engineModel.value &&
          inputpayload.engineYear &&
          inputpayload.engineYear.value &&
          inputpayload.engineHorsePower &&
          inputpayload.engineHour &&
          inputpayload.engineType &&
          inputpayload.engineType.value &&
          inputpayload.engineFuelType &&
          inputpayload.engineFuelType.value &&
          inputpayload.num_of_engines &&
          inputpayload.num_of_engines.value &&
          (finalpayload.hasWarranty && finalpayload.hasWarranty.value == "Yes"
            ? inputpayload.warrantyDate
            : true)
        ? true
        : false;
    }

    this.setState({ isFormValid: isFormValid });
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  handleMonthPicker = (event) => {
    if (event) {
      let Date = event._d;
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[`warrantyDate`] = moment(Date);
      this.setState(
        {
          inputpayload: { ...tempInputPayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else if (event == null) {
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[`warrantyDate`] = "";
      this.setState(
        {
          inputpayload: { ...tempInputPayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    this.setState({ [name]: event.target.checked }, () => {
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[name] = this.state[name];
      this.setState(
        {
          inputpayload: { ...tempInputPayload },
        },
        () => {
          this.checkIfFormValid();
          console.log("PAYLOAD", this.state.inputpayload);
          if (this.state.inputpayload.engineNotAvaiable) {
            let InputPayload = { ...this.state.inputpayload };
            InputPayload[`num_of_engines`] = 0;
            InputPayload[`engineMake`] = {};
            InputPayload[`engineModel`] = "";
            InputPayload[`engineYear`] = {};
            InputPayload[`engineHorsePower`] = "";
            InputPayload[`engineHour`] = "";
            InputPayload[`engineType`] = {};
            InputPayload[`engineFuelType`] = {};
            this.setState({
              inputpayload: { ...InputPayload },
              num_of_engines: {
                value: 0,
                label: 0,
              },
              engineMake: {},
              engineModel: "",
              engineYear: {},
              engineHorsePower: "",
              engineHour: "",
              engineType: {},
              engineFuelType: {},
            });
          } else if (!this.state.inputpayload.engineNotAvaiable) {
            let InputPayload = { ...this.state.inputpayload };
            InputPayload[`num_of_engines`] = 1;
            this.setState({
              inputpayload: { ...InputPayload },
              num_of_engines: {
                value: 1,
                label: 1,
              },
            });
          }
        }
      );
    });
  };

  // Async Boat Manufacturer List API call
  handleEngineManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "2", // 0-all manufactureres,1-boats and products,2-engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.engineManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName,
                };
              })
            : [];
        this.setState({
          engineManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.engineManufacturerList &&
      this.state.engineManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.engineManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Async Boat Model List API call
  handleEngineModelList = async (search, loadedOptions, { page }) => {
    let payload = {
      searchText: search,
      manufactureId: this.state.inputpayload.engineMake.value,
      limit: this.state.limit,
      offset: page * this.state.limit,
    };
    getModels(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.engineModelList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.modelId,
                  label: item.modelName,
                };
              })
            : [];
        this.setState({
          engineModelList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          model_total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("fjwifj--err", err);
      });
    await this.sleep(2000);

    let hasMore =
      this.state.engineModelList &&
      this.state.engineModelList.length < this.state.model_total_count;

    return {
      options: this.state.engineModelList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const EngineTypes =
      this.props.engineData.engineTypes &&
      this.props.engineData.engineTypes.map((data) => {
        return { value: data, label: data };
      });

    const FuelTypes =
      this.props.engineData.fuelType &&
      this.props.engineData.fuelType.map((data) => {
        return { value: data, label: data };
      });

    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const NumOfEngines = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
    ];

    const warrantyConditions = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    const {
      isFormValid,
      engineNotAvaiable,
      engineHorsePower,
      engineHour,
      inputpayload,
      price,
    } = this.state;
    const {
      condition,
      engineMake,
      engineModel,
      engineYear,
      num_of_engines,
      engineType,
      engineFuelType,
      hasWarranty,
      currency,
      warrantyDate,
    } = this.state.inputpayload;
    const {
      handleOnSelectInput,
      handleOnchangeInput,
      handleMonthPicker,
      disabledDate,
      handleOnchangeCheckbox,
      handleEngineManufacturerList,
      handleEngineModelList,
    } = this;
    const {
      handleBackScreenBoat,
      handleBackScreenEngine,
      onClose,
      handleAdvanceBoatDetails,
      handleSuccessfullPost,
      apiLoading,
      finalpayload,
    } = this.props;

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));
    return (
      <Wrapper>
        <div>
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        </div>
        <div className="engineDetailsPage px-2">
          <h3 className="heading">Engine Details</h3>
          {finalpayload.category == "Engines" ? (
            <div className="selectInputSec">
              <SelectTopLabel
                bottomborder={true}
                label="Condition"
                mandatory={true}
                placeholder="Select Condition"
                value={condition}
                options={Conditions}
                onChange={handleOnSelectInput(`condition`)}
              />
            </div>
          ) : (
            ""
          )}
          <div className="selectInputSec">
            <SelectTopLabel
              asyncSelect={true}
              bottomborder={true}
              label="Engine Make"
              mandatory={true}
              value={engineMake}
              isDisabled={engineNotAvaiable ? true : false}
              placeholder="Engine Make Types"
              loadOptions={handleEngineManufacturerList}
              additional={{
                page: 0,
              }}
              onChange={handleOnSelectInput(`engineMake`)}
            />
          </div>
          <div className="selectInputSec">
            <SelectTopLabel
              asyncSelect={true}
              bottomborder={true}
              label="Engine Model"
              mandatory={true}
              value={engineModel}
              isDisabled={
                engineNotAvaiable || !(engineMake && engineMake.value)
                  ? true
                  : false
              }
              placeholder="Engine Model"
              loadOptions={handleEngineModelList}
              additional={{
                page: 0,
              }}
              cacheUniqs={[engineMake && engineMake.value]}
              onChange={handleOnSelectInput(`engineModel`)}
            />
          </div>
          <div className="row">
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  bottomborder={true}
                  label="Year"
                  mandatory={true}
                  isDisabled={engineNotAvaiable ? true : false}
                  value={engineYear}
                  placeholder="ex: 2013"
                  options={YearOptions}
                  onChange={handleOnSelectInput(`engineYear`)}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="FormInput">
                <label>
                  Horse Power <span className="mandatory">*</span>
                </label>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="engineHorsePower"
                  disabled={engineNotAvaiable ? true : false}
                  value={engineHorsePower}
                  placeholder="90"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  value={num_of_engines}
                  bottomborder={true}
                  mandatory={true}
                  label="Number of Engines"
                  placeholder="5"
                  options={NumOfEngines}
                  onChange={handleOnSelectInput(`num_of_engines`)}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="FormInput">
                <label>
                  Engine Hours <span className="mandatory">*</span>
                </label>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="engineHour"
                  disabled={engineNotAvaiable ? true : false}
                  value={engineHour}
                  placeholder="7600"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  mandatory={true}
                  bottomborder={true}
                  isDisabled={engineNotAvaiable ? true : false}
                  value={engineType}
                  label="Engine Type"
                  placeholder="Electric"
                  options={EngineTypes}
                  onChange={handleOnSelectInput(`engineType`)}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  bottomborder={true}
                  mandatory={true}
                  label="Fuel Type"
                  isDisabled={engineNotAvaiable ? true : false}
                  value={engineFuelType}
                  placeholder="Diesel"
                  options={FuelTypes}
                  onChange={handleOnSelectInput(`engineFuelType`)}
                />
              </div>
            </div>
          </div>
          <div className="row">
            {finalpayload.category == "Engines" ? (
              <div className="col-6">
                <div className="selectInputSec">
                  <SelectTopLabel
                    mandatory={true}
                    bottomborder={true}
                    value={hasWarranty}
                    label="Has Warranty"
                    placeholder="Yes"
                    options={warrantyConditions}
                    onChange={handleOnSelectInput(`hasWarranty`)}
                  />
                </div>
              </div>
            ) : (
              ""
            )}
            <div className="col-6">
              {(finalpayload.hasWarranty &&
                finalpayload.hasWarranty.value == "Yes") ||
              (finalpayload.category == "Engines" &&
                hasWarranty &&
                hasWarranty.value == "Yes") ? (
                <div className="FormInput">
                  <label>
                    Warranty Till? <span className="mandatory">*</span>
                  </label>
                  <DatePicker
                    picker="month"
                    format="MM-YYYY"
                    placeholder="MM-YYYY"
                    value={warrantyDate}
                    disabled={engineNotAvaiable ? true : false}
                    onChange={handleMonthPicker}
                    disabledDate={disabledDate}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </div>

          {this.props.finalpayload.category == "Engines" ? (
            <div className="FormInput">
              <label>
                Price <span className="mandatory">*</span>
              </label>
              <div className="d-flex align-items-center justify-content-between priceInputContainer">
                <SelectInput
                  defaultValue={currency}
                  value={currency}
                  currency={true}
                  placeholder="$"
                  options={CurrencyOptions}
                  onChange={handleOnSelectInput(`currency`)}
                />
                <InputBox
                  type="text"
                  className="priceInputBox form-control"
                  name="price"
                  value={price}
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
              </div>
            </div>
          ) : (
            <div className="my-2">
              <CheckBox
                checked={engineNotAvaiable}
                plaincheckbox={true}
                onChange={handleOnchangeCheckbox("engineNotAvaiable")}
                value="engineNotAvaiable"
                label="This boat does not have an engine"
              />
            </div>
          )}
          <section className="d-flex mx-auto">
            <div className="backButton mr-1">
              <ButtonComp
                onClick={
                  this.props.finalpayload.category == "Engines"
                    ? handleBackScreenEngine
                    : handleBackScreenBoat
                }
              >
                Back
              </ButtonComp>
            </div>
            <div className={isFormValid ? "Next_btn" : "inactive_Btn"}>
              {this.props.finalpayload.category == "Engines" ? (
                <CircularProgressButton
                  buttonText={"Post"}
                  disabled={!isFormValid}
                  onClick={handleSuccessfullPost.bind(
                    this,
                    "engineDetails",
                    inputpayload
                  )}
                  loading={apiLoading}
                  // cssStyles={
                  //   isFormValid
                  //     ? {
                  //         background: `${GREEN_COLOR}`,
                  //         color: `${WHITE_COLOR}`,
                  //         height: "2.292vw",
                  //         margin: "0.732vw 0",
                  //         fontSize: "1.024vw",
                  //         fontFamily: "Museo-Sans",
                  //       }
                  //     : {
                  //         background: `${Border_LightGREY_COLOR_1}`,
                  //         color: `${GREY_VARIANT_1}`,
                  //         height: "2.292vw",
                  //         margin: "0.732vw 0",
                  //         fontSize: "1.024vw",
                  //         fontFamily: "Museo-Sans",
                  //       }
                  // }
                />
              ) : (
                <ButtonComp
                  disabled={isFormValid ? false : true}
                  onClick={handleAdvanceBoatDetails.bind(
                    this,
                    "engine",
                    inputpayload
                  )}
                  // cssStyles={
                  //   isFormValid
                  //     ? {
                  //         background: `${GREEN_COLOR}`,
                  //         color: `${WHITE_COLOR}`,
                  //         height: "2.292vw",
                  //         margin: "0.732vw 0",
                  //         fontSize: "1.024vw",
                  //         fontFamily: "Museo-Sans",
                  //       }
                  //     : {
                  //         background: `${Border_LightGREY_COLOR_1}`,
                  //         color: `${GREY_VARIANT_1}`,
                  //         height: "2.292vw",
                  //         margin: "0.732vw 0",
                  //         fontSize: "1.024vw",
                  //         fontFamily: "Museo-Sans",
                  //       }
                  // }`
                >
                  Next
                </ButtonComp>
              )}
            </div>
          </section>
        </div>

        <style jsx>
          {`
            .engineDetailsPage {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }

            .longArrowLeftIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.024vw;
              cursor: pointer;
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              left: 0.732vw;
              width: 0.732vw;
              cursor: pointer;
            }
            :global(.engineDetailsPage .selectInputSec),
            .engineDetailsPage .FormInput {
              padding-top: 1.098vw;
              position: relative;
            }
            :global(.ant-picker-dropdown) {
              z-index: 1500 !important;
            }
            :global(.ant-picker) {
              cursor: pointer;
              width: 100%;
              height: 2.0497vw;
              padding: 0;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-disabled) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.ant-picker-focused) {
              outline: 0;
              box-shadow: none !important;
            }
            :global(.ant-picker-input input) {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.ant-picker-input input::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
            :global(.engineDetailsPage .FormInput label) {
              font-size: 0.732vw !important;
              font-family: "Museo-Sans" !important;
              margin: 0;
              padding: 0;
              color: ${GREY_VARIANT_2};
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            :global(.engineDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.0497vw;
              padding: 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.073vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.engineDetailsPage .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_3};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .Next_btn,
            .inactive_Btn {
              // width: 70%;
              width: 11.77083vw;
              margin: 0.732vw 0 0 auto;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.Next_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }

            :global(.inactive_Btn button span),
            :global(.backButton button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 200;
              color: ${GREY_VARIANT_1};
              font-size: 1.024vw;
            }
            :global(.Next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 200;
              color: ${WHITE_COLOR};
              font-size: 1.024vw;
            }
            .backButton {
              width: 5.72916vw;
              margin: 0.732vw 0 0 0;
            }
            :global(.backButton button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.backButton button:focus),
            :global(.backButton button:active),
            :global(.backButton button:hover) {
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              outline: none;
              box-shadow: none;
            }
            :global(.Next_btn button:focus),
            :global(.Next_btn button:active) {
              background: ${GREEN_COLOR} !important;
              outline: none;
              box-shadow: none;
              color: ${WHITE_COLOR};
            }
            :global(.Next_btn button:hover) {
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
            }

            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: fit-content;
              flex: 1;
            }
            :global(.engineDetailsPage .FormInput > .priceInputContainer) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.priceInputContainer .react-select__control) {
              min-height: 2.342vw !important;
            }

            :global(.engineDetailsPage .FormInput .priceInputBox) {
              width: 100%;
              height: 2.049vw;
              padding: 0 0 0 0.366vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.engineDetailsPage .FormInput .priceInputBox > input) {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.engineDetailsPage .FormInput .priceInputBox:focus),
            :global(.engineDetailsPage .FormInput .priceInputBox:active) {
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.engineDetailsPage .FormInput .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
            :global(.engineDetailsPage .FormInput .priceInputBox::before) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.engineDetailsPage .FormInput .priceInputBox::after) {
              border-bottom: 0.0732vw solid ${THEME_COLOR} !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(EngineDetails);
