// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// asstes and colors
import {
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  Border_LightGREY_COLOR_2,
  GREEN_COLOR,
  Long_Arrow_Left,
  Close_Icon,
  BG_LightGREY_COLOR,
  Light_Blue_Color,
  FONTGREY_COLOR_Dark,
} from "../../lib/config";

// reusable component
import ButtonComp from "../../components/button/button";
import { CopyToClipboard } from "react-copy-to-clipboard";
import InfoTooltip from "../../components/tooltip/info-tooltip";

class SuccessfullPost extends Component {
  state = {
    category: "",
    productName: "",
    postId: "",
    openPage: false,
    linkCopied: false,
  };

  componentDidMount() {
    let { productName, postId, category, title } = {
      ...this.props.ProductData,
    };
    console.log("ProductData-->payload", this.props.ProductData);
    let productname = productName
      ? productName.replace(/ /g, "-")
      : title.replace(/ /g, "-");
    this.setState(
      {
        category: category,
        productName: productname,
        postId: postId,
      },
      () => {
        this.setState({
          openPage: true,
        });
      }
    );
  }

  handleOnClipboardCopy = () => {
    this.setState({
      linkCopied: true,
    });
  };

  componentDidUpdate = () => {
    if (this.state.linkCopied) {
      setTimeout(() => {
        this.setState({
          linkCopied: false,
        });
      }, 500);
    }
  };

  render() {
    let slug =
      this.state.category == "Products"
        ? "product-detail"
        : this.state.category == "Boats"
        ? "boat-detail"
        : this.state.category == "Engines"
        ? "engine-detail"
        : "";
    const { productName, postId, linkCopied } = this.state;
    const {
      handleCreateNewPost,
      onClose,
      handleBackScreenBoat,
      handleBackScreen,
      pageName,
    } = this.props;
    const { productAssets } = { ...this.props.finalpayload };
    return (
      <Wrapper>
        {this.state.openPage ? (
          <Wrapper>
            <div>
              <img
                src={Long_Arrow_Left}
                className="longArrowLeftIcon"
                onClick={
                  pageName == "advance_boat_details"
                    ? handleBackScreenBoat
                    : handleBackScreen
                }
              ></img>
              <img
                src={Close_Icon}
                className="closeIconRight"
                onClick={onClose}
              ></img>
            </div>
            <div className="successfullPost_Page px-2">
              <h3 className="heading">Posted!!</h3>
              <p className="postMsg">Soon local buyers will see this item!</p>
              <div className="postedItemImg">
                {productAssets[0].urlType == "video" ? (
                  <div className="videoDiv" key={index}>
                    <div className="videoBlock">
                      <video className="video" autoPlay loop controls muted>
                        <source
                          src={productAssets[0].url}
                          type="video/mp4"
                        ></source>
                      </video>
                    </div>
                    <div className="videoIconDiv">
                      <img src={Play_Video_Icon} className="videoIcon"></img>
                    </div>
                  </div>
                ) : productAssets[0].urlType == "image" ? (
                  <img src={productAssets[0].url}></img>
                ) : (
                  ""
                )}
              </div>
              <h6 className="itemName">{productName}</h6>
              <div className="support_btns">
                <ButtonComp onClick={handleCreateNewPost}>
                  Post Another
                </ButtonComp>
                <InfoTooltip
                  tooltipContent="link copied to clipboard"
                  placement="bottom-end"
                  open={linkCopied}
                  color={Light_Blue_Color}
                  disableFocusListener
                  disableHoverListener
                  disableTouchListener
                >
                  <CopyToClipboard
                    text={`https://stagingbz.com/${slug}/${productName}?pid=${postId}`}
                    onCopy={this.handleOnClipboardCopy}
                  >
                    <ButtonComp>Share</ButtonComp>
                  </CopyToClipboard>
                </InfoTooltip>
              </div>
              <div className="done_btn">
                <ButtonComp onClick={onClose}>Done</ButtonComp>
              </div>
            </div>
          </Wrapper>
        ) : (
          ""
        )}

        <style jsx>
          {`
            .longArrowLeftIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.024vw;
              cursor: pointer;
            }

            .closeIconRight {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .successfullPost_Page {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            .postMsg {
              font-size: 1.024vw;
              margin: 0;
              text-transform: initial;
              color: ${FONTGREY_COLOR};
              opcaity: 1;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .postedItemImg {
              display: block;
              width: 100%;
              margin: 1.098vw 0;
            }
            .postedItemImg img {
              width: 100%;
              object-fit: cover;
            }
            .videoDiv {
              width: 100%;
              height: 100%;
              cursor: pointer;
              position: relative;
            }
            .videoBlock {
              width: 100%;
              height: 100%;
              display: flex;
              justify-content: center;
              align-items: center;
            }
            .videoIconDiv {
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            .videoIcon {
              width: 1.83vw;
              object-fit: cover;
              display: none;
              z-index: 1;
            }
            .videoDiv:hover > .videoIcon {
              display: block;
            }
            .itemName {
              font-size: 1.171vw;
              margin: 0;
              text-transform: initial;
              color: ${FONTGREY_COLOR};
              opcaity: 1;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .support_btns {
              display: flex;
              justify-content: space-between;
              align-items: center;
              padding: 1.464vw 0;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
            }
            :global(.support_btns button) {
              width: 49%;
              padding: 0.732vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.support_btns button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
            }
            :global(.support_btns button:focus),
            :global(.support_btns button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.support_btns button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            :global(.done_btn button) {
              width: 100%;
              padding: 0.732vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 1.464vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.done_btn button span) {
              font-weight: 600;
              font-family: "Museo-Sans" !important;
              font-size: 1.024vw;
            }
            :global(.done_btn button:focus),
            :global(.done_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.done_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SuccessfullPost;
