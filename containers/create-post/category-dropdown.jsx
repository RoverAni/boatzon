import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Chevron_Right,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
  Chevron_Left_Darkgrey,
} from "../../lib/config";
// Reactstrap Components
import { DropdownItem } from "reactstrap";

class CategoryDropdown extends Component {
  render() {
    const {
      ProductsSubCategoriesList,
      handleCategorySelection,
      backtoCategoryList,
      categoryName,
    } = this.props;
    console.log("fbeuf", ProductsSubCategoriesList);
    return (
      <Wrapper>
        {categoryName ? (
          <h6 className="catName" onClick={backtoCategoryList}>
            <img src={Chevron_Left_Darkgrey}></img> <span>{categoryName}</span>
          </h6>
        ) : (
          ""
        )}
        {ProductsSubCategoriesList &&
          ProductsSubCategoriesList.map((data, index) => (
            <DropdownItem
              className="catDropdownItem"
              key={index}
              onClick={handleCategorySelection.bind(this, data)}
            >
              <span className="catName">{data.subCategoryName}</span>
              {data && data.childCount > 0 ? (
                <img src={Chevron_Right} width="6px"></img>
              ) : (
                ""
              )}
            </DropdownItem>
          ))}
        <style jsx>
          {`
            .catName {
              display: flex;
              align-items: center;
              padding: 0.292vw;
              border-bottom: 0.0732vw solid ${BG_LightGREY_COLOR};
              cursor: pointer;
            }
            .catName span {
              font-size: 0.951vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans-Semibold" !important;
            }
            .catName img {
              width: 0.585vw;
              margin-right: 0.585vw;
            }
            :global(.catDropdownItem) {
              display: flex !important;
              align-items: center !important;
              justify-content: space-between !important;
              padding: 0.366vw 0.585vw !important;
              cursor: pointer;
            }
            :global(.catDropdownItem .catName) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
            }
            :global(.catDropdownItem:hover) {
              background: ${BG_LightGREY_COLOR} !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default CategoryDropdown;
