import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import SelectTopLabel from "../../components/input-box/select-top-label";
import {
  GREY_VARIANT_2,
  Border_LightGREY_COLOR_2,
  THEME_COLOR,
  GREEN_COLOR,
  WHITE_COLOR,
  Close_Icon,
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_3,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import { NumberValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import { getYearsList } from "../../lib/date-operation/date-operation";
import SelectInput from "../../components/input-box/select-input";
import currencies from "../../translations/currency.json";
import CheckBox from "../../components/input-box/check-box";
import cloneDeep from "lodash.clonedeep";
import ColorPicker from "../../components/input-box/color-picker";
import { getManufactures, getModels } from "../../services/manufacturer";

const YearsList = getYearsList();
let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);

class BoatDetails extends Component {
  state = {
    isFormValid: false,
    inputpayload: {
      negotiable: false,
      currency: {
        value: CurrencyArray[0].code,
        label: CurrencyArray[0].symbol_native,
      },
      zipCode: this.props.currentLocation.zipCode,
    },
    limit: 15,
    boatManufacturerList: [],
    boatModelList: [],
    total_count: "",
    model_total_count: "",
  };

  componentDidMount() {
    let tempFinalPayload = cloneDeep(this.props.finalpayload).__wrapped__;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload.negotiable = tempFinalPayload.negotiable
      ? tempFinalPayload.negotiable
      : tempPayload.negotiable;

    tempPayload.currency = tempFinalPayload.currency
      ? tempFinalPayload.currency
      : tempPayload.currency;

    tempPayload.zipCode = tempFinalPayload.zipCode
      ? tempFinalPayload.zipCode
      : tempPayload.zipCode;

    this.setState(
      {
        inputpayload: { ...tempFinalPayload, ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  }

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.value) {
      if (name == "manufacturer") {
        let Payload = { ...this.state.inputpayload };
        Payload[[name]] = inputControl;
        Payload.boat_model = {};
        this.setState(
          {
            inputpayload: { ...Payload },
            boatModelList: [],
          },
          () => {
            this.checkIfFormValid();
          }
        );
      } else {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[[name]] = inputControl;
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            this.checkIfFormValid();
          }
        );
      }
    }
  };

  // Function for form validation
  checkIfFormValid = () => {
    let isFormValid = false;
    const { inputpayload } = this.state;
    isFormValid =
      inputpayload.boat_type &&
      inputpayload.boat_type.value &&
      inputpayload.manufacturer &&
      inputpayload.manufacturer.value &&
      inputpayload.boat_model &&
      inputpayload.boat_model.value &&
      inputpayload.boat_color &&
      inputpayload.boat_color.value &&
      inputpayload.condition &&
      inputpayload.condition.value &&
      inputpayload.boat_year &&
      inputpayload.boat_year.value &&
      inputpayload.boat_length &&
      inputpayload.hasWarranty &&
      inputpayload.hasWarranty.value &&
      inputpayload.currency &&
      inputpayload.currency.value &&
      inputpayload.price &&
      inputpayload.zipCode &&
      inputpayload.zipCode.length > 4
        ? true
        : false;

    this.setState({ isFormValid: isFormValid });
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    let tempInputPayload = { ...this.state.inputpayload };
    tempInputPayload[name] = event.target.checked;
    this.setState(
      {
        inputpayload: { ...tempInputPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Async Boat Model List API call
  handleModelList = async (search, loadedOptions, { page }) => {
    let payload = {
      searchText: search,
      manufactureId: this.state.inputpayload.manufacturer.value,
      limit: this.state.limit,
      offset: page * this.state.limit,
    };
    getModels(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.boatModelList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.modelId,
                  label: item.modelName,
                };
              })
            : [];
        this.setState({
          boatModelList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          model_total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("fjwifj--err", err);
      });
    await this.sleep(2000);

    let hasMore =
      this.state.boatModelList &&
      this.state.boatModelList.length < this.state.model_total_count;

    return {
      options: this.state.boatModelList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Async Boat Manufacturer List API call
  handleBoatManufacturerList = async (search, loadedOptions, { page }) => {
    console.log("apiCall");
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "1", // 0-all manufactureres,1-boats and products,2,engines,3-trailers,4-popular manufactures
    };
    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        console.log("apiCall--res", response);
        let oldPayload = [...this.state.boatManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName,
                };
              })
            : [];
        this.setState({
          boatManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.boatManufacturerList &&
      this.state.boatManufacturerList.length < this.state.total_count;
    console.log("apiCall--hasMore", hasMore);

    return {
      options: this.state.boatManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const BoatsTypes =
      this.props.BoatsSubCategoriesList &&
      this.props.BoatsSubCategoriesList.map((data) => ({
        value: data.subCategoryName,
        label: data.subCategoryName,
      }));

    const ColorList = [
      {
        value: "#FFFFFD",
        label: "White",
      },
      {
        value: "#A4F8E1",
        label: "Turquoise",
      },
      {
        value: "#FFFFB5",
        label: "Soft Yellow",
      },
      {
        value: "#777D79",
        label: "Grey",
      },
      {
        value: "#0E1003",
        label: "Black",
      },
      {
        value: "#152F74",
        label: "Blue",
      },
      {
        value: "#1B253C",
        label: "Dark Blue",
      },
      {
        value: "#005E60",
        label: "Teal",
      },
      {
        value: "#024F19",
        label: "Green",
      },
      {
        value: "#EAC500",
        label: "Yellow",
      },
      {
        value: "#EA6500",
        label: "Orange",
      },
      {
        value:
          "linear-gradient(180deg, #80FF00 0%, #1FA3C0 50.52%, #FF003D 100%)",
        label: "Other",
      },
    ];

    const Conditions = [
      { value: "Used", label: "Used" },
      { value: "New", label: "New" },
    ];

    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const CurrencyOptions =
      CurrencyArray &&
      CurrencyArray.map((data) => ({
        value: data.code,
        label: data.symbol_native,
      }));

    const warrantyConditions = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const { isFormValid, inputpayload } = this.state;

    const {
      boat_type,
      manufacturer,
      boat_model,
      boat_color,
      condition,
      boat_year,
      hasWarranty,
      currency,
      negotiable,
      zipCode,
      boat_length,
      price,
    } = this.state.inputpayload;

    const {
      handleOnchangeInput,
      handleOnSelectInput,
      handleOnchangeCheckbox,
      handleBoatManufacturerList,
      handleModelList,
    } = this;

    const {
      handleBackScreenBoat,
      onClose,
      handleEngineDetailsPage,
    } = this.props;

    return (
      <Wrapper>
        <div>
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
        </div>
        <div className="boatDetailsPage px-2">
          <h3 className="heading">Boat Details</h3>
          <div className="selectInputSec">
            <SelectTopLabel
              bottomborder={true}
              value={boat_type}
              label="Type"
              mandatory={true}
              placeholder="All Boat Types"
              options={BoatsTypes}
              onChange={handleOnSelectInput(`boat_type`)}
            />
          </div>
          <div className="selectInputSec">
            <SelectTopLabel
              asyncSelect={true}
              bottomborder={true}
              value={manufacturer}
              label="Manufacturer"
              mandatory={true}
              placeholder="Manufacturer List"
              loadOptions={handleBoatManufacturerList}
              additional={{
                page: 0,
              }}
              onChange={handleOnSelectInput(`manufacturer`)}
            />
          </div>
          <div className="selectInputSec">
            <SelectTopLabel
              asyncSelect={true}
              bottomborder={true}
              isDisabled={manufacturer ? false : true}
              value={boat_model}
              label="Model"
              mandatory={true}
              placeholder="Boat Model"
              loadOptions={handleModelList}
              additional={{
                page: 0,
              }}
              cacheUniqs={[manufacturer && manufacturer.value]}
              onChange={handleOnSelectInput(`boat_model`)}
            />
          </div>
          <div className="FormInput">
            <label>
              Boat Color <span className="mandatory">*</span>
            </label>
            <ColorPicker
              value={boat_color}
              placeholder="select color"
              options={ColorList}
              onChange={handleOnSelectInput(`boat_color`)}
            ></ColorPicker>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  bottomborder={true}
                  label="Condition"
                  mandatory={true}
                  value={condition}
                  placeholder="Used"
                  options={Conditions}
                  onChange={handleOnSelectInput(`condition`)}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  bottomborder={true}
                  value={boat_year}
                  label="Year"
                  mandatory={true}
                  placeholder="ex: 2013"
                  options={YearOptions}
                  onChange={handleOnSelectInput(`boat_year`)}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="FormInput">
                <label>
                  Length (Feet) <span className="mandatory">*</span>
                </label>
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="boat_length"
                  value={boat_length}
                  placeholder="50'"
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                ></InputBox>
              </div>
            </div>
            <div className="col-6">
              <div className="selectInputSec">
                <SelectTopLabel
                  bottomborder={true}
                  value={hasWarranty}
                  label="Has Warranty"
                  mandatory={true}
                  placeholder="Yes"
                  options={warrantyConditions}
                  onChange={handleOnSelectInput(`hasWarranty`)}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="FormInput">
                <label>
                  Price <span className="mandatory">*</span>
                </label>
                <div className="d-flex align-items-center justify-content-between priceInputContainer">
                  <SelectInput
                    defaultValue={currency}
                    value={currency}
                    currency={true}
                    placeholder="$"
                    options={CurrencyOptions}
                    onChange={handleOnSelectInput(`currency`)}
                  />
                  <InputBox
                    type="text"
                    className="priceInputBox"
                    name="price"
                    value={price}
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                    onKeyPress={NumberValidator}
                  ></InputBox>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="FormInput">
                <label>
                  Zip Code <span className="mandatory">*</span>
                </label>
                <InputBox
                  type="text"
                  className="inputBox zipcodeInput form-control"
                  name="zipCode"
                  value={zipCode}
                  onChange={handleOnchangeInput}
                  autoComplete="off"
                  onKeyPress={NumberValidator}
                  readonly
                  maxlength={5}
                ></InputBox>
              </div>
            </div>
          </div>

          <div className="mt-2">
            <CheckBox
              checked={negotiable}
              plaincheckbox={true}
              onChange={handleOnchangeCheckbox("negotiable")}
              value="negotiable"
              label="Is price negotiable?"
            />
          </div>
          <section className="d-flex mx-auto">
            <div className="backButton mr-1">
              <ButtonComp onClick={handleBackScreenBoat}>Back</ButtonComp>
            </div>
            <div
              className={!isFormValid ? "inactive_Btn ml-1" : "Next_btn ml-1"}
            >
              <ButtonComp
                disabled={!isFormValid}
                onClick={handleEngineDetailsPage.bind(
                  this,
                  "boat",
                  inputpayload
                )}
              >
                Next
              </ButtonComp>
            </div>
          </section>
        </div>

        <style jsx>
          {`
            
            .boatDetailsPage {
              position: relative;
              margin: 2.196vw 0 1.098vw 0;
            }
            .longArrowLeftIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.024vw;
              cursor: pointer;
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              left: 0.732vw;
              width: 0.732vw;
              cursor: pointer;
            }
            :global(.boatDetailsPage .selectInputSec),
            .boatDetailsPage .FormInput {
              padding-top: 1.098vw;
            }
            :global(.boatDetailsPage .FormInput label) {
              font-size: 0.732vw !important;
              font-family: "Museo-Sans" !important;
              margin: 0;
              padding: 0;
              color: ${GREY_VARIANT_2};
            }
            :global(.boatDetailsPage .FormInput label .unitText){
              font-size: 0.677vw !important;
              font-family: "Museo-Sans" !important;
              margin: 0;
              padding: 0;
              color: ${GREY_VARIANT_2};
            }
            .heading {
              font-size: 1.667vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            :global(.priceInputContainer .css-2b097c-container) {
              display: flex;
              width: fit-content;
              flex: 1;
            }
            :global(.priceInputContainer .react-select__control) {
              min-height: 2.342vw !important;
            }
            :global(.boatDetailsPage .FormInput > div .priceInputBox) {
              width: max-content !important;
              max-width: 70% !important;
              height: 2.049vw;
              padding: 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
            }
            :global(.boatDetailsPage .FormInput > div .priceInputBox:focus),
            :global(.boatDetailsPage .FormInput > div .priceInputBox:active) {
              outline: none !important;
              box-shadow: none !important;
            }
            :global(.boatDetailsPage .FormInput > div .priceInputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Museo-Sans" !important;
            }
            :global(.boatDetailsPage .FormInput > .priceInputContainer) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.boatDetailsPage .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.boatDetailsPage .FormInput .inputBox:focus),
            :global(.boatDetailsPage .FormInput .inputBox:active) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.boatDetailsPage .FormInput .zipcodeInput) {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2} !important;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_3};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
            }

            .Next_btn,
            .inactive_Btn {
              // width: 70%;
              width:  11.77083vw;
              margin: 0.732vw 0 0 0;
            }
            :global(.inactive_Btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.Next_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
           
            :global(.inactive_Btn button span),
            :global(.backButton button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 200;
              color: ${GREY_VARIANT_1};
              font-size: 1.024vw;
            }
            :global(.Next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 200;
              color: ${WHITE_COLOR};
              font-size: 1.024vw;
            }
            .backButton {
              width: 5.72916vw;
              margin: 0.732vw 0 0 0;
            }
            :global(.backButton button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.backButton button:focus),
            :global(.backButton button:active),
            :global(.backButton button:hover) {
                background: ${Border_LightGREY_COLOR};
                outline: none;
                box-shadow: none;
              }
            }
            :global(.Next_btn button:focus),
            :global(.Next_btn button:active) {
              background: ${GREEN_COLOR} !important;
              outline: none;
              box-shadow: none;
              color: ${WHITE_COLOR};
            }
            :global(.Next_btn button:hover) {
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR}
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    BoatsSubCategoriesList: state.BoatsSubCategoriesList,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(BoatDetails);
