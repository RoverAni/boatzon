// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// asstes and colors
import {
  WHITE_COLOR,
  BG_LightGREY_COLOR,
  Boat_Front_Icon_Grey,
  Boat_Front_Icon_Blue,
  Anchor_Grey_Icon,
  Anchor_Blue_Icon,
  Discussion_Blue_Icon,
  Discussion_Grey_Icon,
  FONTGREY_COLOR,
  No_Boat_Post_Img,
} from "../../lib/config";

// imported components
import BoatList from "../landing-page/boat-list";
import FilterSec from "../filter-sec/filter-sec";

// redux context component
import Context from "../../context/context";
import Tabs from "../../components/tabs/tabs";
import ProductList from "../landing-page/product-list";
import { AllFollowingPosts } from "../../services/my-boats";
import {
  updatefollowManufacturers,
  getFollowManufactures,
} from "../../services/manufacturer";
import Snackbar from "../../components/snackbar";
import PageLoader from "../../components/loader/page-loader";

class MyBoatsPage extends Component {
  state = {
    postedinputpayload: {},
    payload: {
      category: "Products",
    },
    activeLink: "All",
    addManufaturer: false,
  };

  componentDidMount() {
    this.setState(
      {
        followingManufacturerList: [...this.props.getManufacturerData],
      },
      () => {
        let allManufacturerName =
          this.state.followingManufacturerList &&
          this.state.followingManufacturerList.map((data) => {
            return data.manufactureName;
          });
        this.handleBoatsFollowingPost(allManufacturerName);
        this.handleProductsFollowingPost(allManufacturerName);
      }
    );
  }

  componentDidUpdate = (prevProps) => {
    if (
      prevProps.allManufacturersList.length !==
      this.props.allManufacturersList.length
    ) {
      this.handleUnfollowManufacturerList();
    }
  };

  handleUnfollowManufacturerList = () => {
    let tempFollowingManufacturerList = this.state.followingManufacturerList;

    let allManufacturersList = this.props.allManufacturersList;

    let notFollowingList =
      allManufacturersList &&
      allManufacturersList.length > 0 &&
      allManufacturersList.filter(
        ({ manufactureName: id1 }) =>
          !tempFollowingManufacturerList.some(
            ({ manufactureName: id2 }) => id2 === id1
          )
      );
    this.setState({
      notFollowingList,
    });
  };

  handleProductsFollowingPost = (ManufacturerName) => {
    this.setState(
      {
        openPage: false,
      },
      () => {
        let ManufacturerNamelist = ManufacturerName;
        let ManufacturerId =
          this.props.allManufacturersList &&
          this.props.allManufacturersList.length > 0 &&
          this.props.allManufacturersList
            .filter((item) => {
              return ManufacturerNamelist.includes(item.manufactureName);
            })
            .map((data) => {
              return data.manufactureId;
            })
            .toString();
        let payload = {
          latitude:
            this.props.currentLocation && this.props.currentLocation.latitude,
          longitude:
            this.props.currentLocation && this.props.currentLocation.longitude,
          manufactureId: ManufacturerId,
          manufacture: ManufacturerNamelist.toString(),
          category: "Products",
          distanceMax: "500",
        };

        AllFollowingPosts(payload)
          .then((res) => {
            let response = res.data;
            if (response) {
              if (response.code == 200) {
                this.setState({
                  ProductsList: response.data,
                  openPage: true,
                });
              } else if (response.code == 204) {
                this.setState({
                  ProductsList: [],
                  openPage: true,
                });
              }
            }
          })
          .catch((err) => {
            console.log("err", err);
          });
      }
    );
  };

  handleBoatsFollowingPost = (ManufacturerName) => {
    this.setState(
      {
        openPage: false,
      },
      () => {
        let ManufacturerNamelist = ManufacturerName;

        let ManufacturerId =
          this.props.allManufacturersList &&
          this.props.allManufacturersList.length > 0 &&
          this.props.allManufacturersList
            .filter((item) => {
              return ManufacturerNamelist.includes(item.manufactureName);
            })
            .map((data) => {
              return data.manufactureId;
            })
            .toString();

        let payload = {
          latitude:
            this.props.currentLocation && this.props.currentLocation.latitude,
          longitude:
            this.props.currentLocation && this.props.currentLocation.longitude,
          manufactureId: ManufacturerId,
          manufacture: ManufacturerNamelist.toString(),
          category: "Boats",
          distanceMax: "500",
        };

        AllFollowingPosts(payload)
          .then((res) => {
            let response = res.data;
            if (response.code == 200) {
              this.setState({
                BoatsList: response.data,
                openPage: true,
              });
            } else if (response.code == 204) {
              this.setState({
                BoatsList: [],
                openPage: true,
              });
            }
          })
          .catch((err) => {
            console.log("err", err);
          });
      }
    );
  };

  // Function for postedinputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = { ...this.state.postedinputpayload };
        temppayload[[name]] = [...this.state[name]];
        this.setState({ postedinputpayload: { ...temppayload } });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.postedinputpayload };
      temppayload[[name]] = inputControl.value;
      this.setState(
        {
          postedinputpayload: { ...temppayload },
        },
        () => {
          let apiPayload = { ...this.state.payload };
          apiPayload[name] = this.state.postedinputpayload[name];
          this.setState(
            {
              payload: { ...apiPayload },
            },
            () => {
              this.handlePostProductsAPI(this.state.payload);
            }
          );
        }
      );
    }
  };

  handleManufacturerClick = (activeManufacturer) => {
    this.setState(
      {
        activeLink: activeManufacturer,
      },
      () => {
        if (activeManufacturer == "All") {
          let allManufacturerName =
            this.state.followingManufacturerList &&
            this.state.followingManufacturerList.map((data) => {
              return data.manufactureName;
            });
          this.handleBoatsFollowingPost(allManufacturerName);
          this.handleProductsFollowingPost(allManufacturerName);
        } else {
          this.handleBoatsFollowingPost(activeManufacturer.manufactureName);
          this.handleProductsFollowingPost(activeManufacturer.manufactureName);
        }
      }
    );
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleFollowManufacturer = (selectedManufacturer) => {
    let tempfollowingManufacturerList = [
      ...this.state.followingManufacturerList,
    ];
    tempfollowingManufacturerList.push(selectedManufacturer);

    let allManufacturersList = this.props.allManufacturersList;

    let notFollowingList =
      allManufacturersList &&
      allManufacturersList.length &&
      allManufacturersList.filter(
        ({ manufactureName: id1 }) =>
          !tempfollowingManufacturerList.some(
            ({ manufactureName: id2 }) => id2 === id1
          )
      );

    this.setState({
      notFollowingList,
    });
    let followingManufacturerId =
      tempfollowingManufacturerList &&
      tempfollowingManufacturerList.map((data) => {
        return data.manufactureId;
      });

    let payload = {
      manufactor: followingManufacturerId.toString(),
    };
    updatefollowManufacturers(payload)
      .then((res) => {
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.handleManufacturersList();
            this.setState(
              {
                followingManufacturerList: [...tempfollowingManufacturerList],
              },
              () => {
                let allManufacturerName =
                  this.state.followingManufacturerList &&
                  this.state.followingManufacturerList.map((data) => {
                    return data.manufactureName;
                  });
                this.handleGetManufacturerList();

                if (this.state.activeLink == "All" || !this.state.activeLink) {
                  this.handleBoatsFollowingPost(allManufacturerName);
                  this.handleProductsFollowingPost(allManufacturerName);
                } else {
                  this.handleBoatsFollowingPost(
                    this.state.activeLink.manufactureName
                  );
                  this.handleProductsFollowingPost(
                    this.state.activeLink.manufactureName
                  );
                }
              }
            );
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleGetManufacturerList = () => {
    getFollowManufactures()
      .then((res) => {
        let response = res.data;
        if (response.code == 200) {
          this.setState({
            followingManufacturerList: response.data,
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleUnfollowManufacturer = (selectedManufacturerId) => {
    let tempfollowingManufacturerList = [
      ...this.state.followingManufacturerList,
    ];
    let elementId =
      tempfollowingManufacturerList &&
      tempfollowingManufacturerList.findIndex((data) => {
        return data.manufactureId == selectedManufacturerId;
      });

    if (elementId != -1) {
      tempfollowingManufacturerList.splice(elementId, 1);
    }
    let manufactureIdList =
      tempfollowingManufacturerList.length > 0
        ? tempfollowingManufacturerList.map((data) => data.manufactureId)
        : "";

    let payload = {
      manufactor: manufactureIdList.toString(),
    };
    updatefollowManufacturers(payload)
      .then((res) => {
        let response = res.data;
        this.setState({
          usermessage: response.message,
          variant: this.handleSnackbar(response),
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
        if (response.code == 200) {
          this.setState(
            {
              followingManufacturerList: [...tempfollowingManufacturerList],
            },
            () => {
              let allManufacturerName =
                this.state.followingManufacturerList &&
                this.state.followingManufacturerList.map((data) => {
                  return data.manufactureName;
                });
              this.handleGetManufacturerList();
              this.handleBoatsFollowingPost(allManufacturerName);
              this.handleProductsFollowingPost(allManufacturerName);
            }
          );
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleManufacturersList = () => {
    this.setState({
      addManufaturer: !this.state.addManufaturer,
    });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    let tempObject = {};
    tempObject.value = inputControl.value;
    tempObject.label = inputControl.label;
    this.setState({
      [name]: { ...tempObject },
    });
  };

  render() {
    const { openPage, BoatsList, postedWithin } = this.state;
    const { handleOnSelectInput } = this;
    // Boats Tab
    const Boats = (
      <div className="row mx-0 BoatListSec py-4">
        {openPage ? (
          BoatsList && BoatsList.length > 0 ? (
            /* Boat List Module */
            <BoatList
              breakpointCols={6}
              myboats={true}
              BoatsList={BoatsList}
              handleOnSelectInput={handleOnSelectInput}
              postedWithin={postedWithin}
            />
          ) : (
            <div className="container p-md-0">
              <div className="screenWidth mx-auto">
                <div className="noFavorites">
                  <div className="text-center">
                    <img
                      src={No_Boat_Post_Img}
                      className="noFavFollowingImg"
                    ></img>
                    <p className="noFavoritesMsg">No Boat Post</p>
                  </div>
                </div>
              </div>
            </div>
          )
        ) : (
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="FollowingLoader">
                <PageLoader loading={!this.state.openPage} />
              </div>
            </div>
          </div>
        )}
      </div>
    );

    // Products Tab
    const Products = (
      <div className="row mx-0 BoatListSec py-4">
        {this.state.openPage ? (
          this.state.ProductsList && this.state.ProductsList.length > 0 ? (
            /* Boat List Module */
            <ProductList
              breakpointCols={6}
              myboats={true}
              handleOnSelectInput={this.handleOnSelectInput}
              ProductsList={this.state.ProductsList}
            />
          ) : (
            <div className="container p-md-0">
              <div className="screenWidth mx-auto">
                <div className="noFavorites">
                  <div className="text-center">
                    <img
                      src={No_Boat_Post_Img}
                      className="noFavFollowingImg"
                    ></img>
                    <p className="noFavoritesMsg">No Product Post</p>
                  </div>
                </div>
              </div>
            </div>
          )
        ) : (
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="FollowingLoader">
                <PageLoader loading={!this.state.openPage} />
              </div>
            </div>
          </div>
        )}
      </div>
    );

    // Discussion Tab
    const Discussion = (
      <div className="row mx-0 BoatListSec py-4">
        <h1>Discussion</h1>
      </div>
    );

    return (
      <Context.Consumer>
        {(Context) => {
          return (
            <Wrapper>
              <div className="row m-0 FilterSec">
                {/* Filter Section Module */}
                <FilterSec
                  heading={`${Context.locale.my} ${Context.locale.boats}`}
                  followingManufacturerList={
                    this.state.followingManufacturerList
                  }
                  myboat={true}
                  activeLink={this.state.activeLink}
                  handleManufacturerClick={this.handleManufacturerClick}
                  handleManufacturersList={this.handleManufacturersList}
                  handleFollowManufacturer={this.handleFollowManufacturer}
                  addManufaturer={this.state.addManufaturer}
                  handleUnfollowManufacturer={this.handleUnfollowManufacturer}
                  notFollowingList={this.state.notFollowingList}
                  followingList={this.state.followingList}
                  handleAllGetManufacturerList={
                    this.props.handleAllGetManufacturerList
                  }
                  allManufacturersList={this.props.allManufacturersList}
                  total_count={this.props.total_count}
                />
              </div>

              <Tabs
                myboatstabs={true}
                tabs={[
                  {
                    label: `Boats`,
                    activeIcon: (
                      <img
                        src={Boat_Front_Icon_Blue}
                        className="boatIcon"
                      ></img>
                    ),
                    inactiveIcon: (
                      <img
                        src={Boat_Front_Icon_Grey}
                        className="boatIcon"
                      ></img>
                    ),
                  },
                  {
                    label: `Products`,
                    activeIcon: (
                      <img src={Anchor_Blue_Icon} className="anchorIcon"></img>
                    ),
                    inactiveIcon: (
                      <img src={Anchor_Grey_Icon} className="anchorIcon"></img>
                    ),
                  },
                  {
                    label: `Discussion`,
                    activeIcon: (
                      <img
                        src={Discussion_Blue_Icon}
                        className="discussionIcon"
                      ></img>
                    ),
                    inactiveIcon: (
                      <img
                        src={Discussion_Grey_Icon}
                        className="discussionIcon"
                      ></img>
                    ),
                  },
                ]}
                tabcontent={[
                  { content: Boats },
                  { content: Products },
                  { content: Discussion },
                ]}
              />

              {/* Snakbar Components */}
              <Snackbar
                variant={this.state.variant}
                message={this.state.usermessage}
                open={this.state.open}
                onClose={this.handleSnackbarClose}
                vertical={this.state.vertical}
                horizontal={this.state.horizontal}
              />

              <style jsx>
                {`
                  :global(.FollowingLoader) {
                    height: 13.762vw;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                  }
                  :global(.noFavFollowingImg) {
                    width: 14.641vw;
                    object-fit: cover;
                  }
                  :global(.noFavorites) {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 32.942vw;
                    text-align: center;
                    background: ${WHITE_COLOR};
                  }
                  :global(.noFavoritesMsg) {
                    max-width: 80%;
                    font-size: 1.098vw;
                    color: ${FONTGREY_COLOR};
                    font-family: "Open Sans" !important;
                    letter-spacing: 0.0219vw !important;
                    margin: 0.732vw auto;
                  }
                  :global(.BoatListSec) {
                    margin: 1.464vw 0 0 0;
                    padding: 0.732vw 0;
                    background: ${BG_LightGREY_COLOR};
                  }
                  .FilterSec {
                    background: ${WHITE_COLOR};
                  }
                  .boatIcon,
                  .anchorIcon,
                  .discussionIcon {
                    width: 0.878vw;
                    margin-right: 0.512vw;
                    margin-bottom: 0.0732vw !important;
                  }
                `}
              </style>
            </Wrapper>
          );
        }}
      </Context.Consumer>
    );
  }
}

export default MyBoatsPage;
