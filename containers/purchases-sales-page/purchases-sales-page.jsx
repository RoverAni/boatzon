import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  WHITE_COLOR,
  BG_LightGREY_COLOR_2,
  GREY_VARIANT_1,
  GREY_VARIANT_10,
  THEME_COLOR,
  Border_LightGREY_COLOR,
  GREY_VARIANT_3,
  Cart_White_Icon,
  Cart_Grey_Icon,
  Selling_White_Icon,
  Selling_Grey_Icon,
  Red_Color_1,
  Search_Blue_Icon,
  NoTransation_Dollar_Symbol,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import Tabs from "../../components/tabs/tabs";
import InputBox from "../../components/input-box/input-box";
import SelectInput from "../../components/input-box/select-input";
import PurchasesSalesSelling from "./purchases-sales-selling";
import DatesRangePicker from "../../components/date-range-picker/date-range-picker";
import PurchasesSalesDetails from "./purchases-sales-details";

class PurchasesSalesPage extends Component {
  state = {
    inputpayload: {},
    start_date: "10.12.2018",
    end_date: "22.12.2019",
    transactionHistory: true,
    openpage: false,
  };

  componentDidMount() {
    this.setState({
      openpage: true,
    });
  }
  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleViewTransactionDetails = () => {
    this.updateScreen(<PurchasesSalesDetails handleBackScreen={this.handleBackScreen} />);
  };

  handleBackScreen = () => {
    this.updateScreen();
  };

  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let tempPayload = this.state.inputpayload;
    tempPayload[[name]] = inputControl.value;
    this.setState({
      inputpayload: tempPayload,
    });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: temppayload,
      });
    }
  };

  handleSelectedDateRange = (event, picker) => {
    console.log("cbschdshd", event, picker, picker.startDate.format("D.MM.YYYY"), picker.endDate.format("D.MM.YYYY"));
    this.setState({
      start_date: picker ? picker.startDate.format("D.MM.YYYY") : "",
      end_date: picker ? picker.endDate.format("D.MM.YYYY") : "",
    });
  };
  render() {
    const statusTypes = [
      { value: "New", label: "New" },
      { value: "Used", label: "Used" },
    ];

    const { openpage } = this.state;
    const Selling = this.state.transactionHistory ? (
      <PurchasesSalesSelling handleViewTransactionDetails={this.handleViewTransactionDetails} />
    ) : (
      <div className="noTransaction_Sec">
        <div className="text-center">
          <img src={NoTransation_Dollar_Symbol} className="noTransactionDollarImg"></img>
          <h6 className="noTransactionMsg">You don’t have any transactions</h6>
        </div>
      </div>
    );
    const Buying = <h1>Buying</h1>;
    return (
      <Wrapper>
        {openpage ? (
          <div className="PurchasesSalesPage">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0 py-3">
                <div className="col-md-12   col-lg-10 p-0 mx-auto">
                  <div className="row mx-0 py-4 justify-content-center tabContent">
                    {!this.state.currentScreen ? (
                      <div className="col-12 paddingXThree">
                        <h6 className="heading">Purchases & Sales</h6>
                        {this.state.transactionHistory ? (
                          <div className="row m-0 align-items-center transactionSummary_Sec">
                            <div className="col-8 p-0">
                              <div className="row m-0 align-items-center">
                                <div className="col-5 summaryContent py-2 pr-0">
                                  <span className="label">This Week</span>
                                  <p className="price">$500.50</p>
                                  <p>2 trades</p>
                                </div>
                                <div className="col-5 summaryContent py-2 pl-0">
                                  <span className="label">Your Balance</span>
                                  <p className="price">$7800.50</p>
                                  <p>$7800.50 avaiable</p>
                                </div>
                              </div>
                            </div>
                            <div className="col-4">
                              <div className="text-right">
                                <div className="payNow_btn">
                                  <ButtonComp>Payout now</ButtonComp>
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : (
                          <div className="row m-0 align-items-center transactionSummary_Sec">
                            <div className="col-8 p-0">
                              <div className="row m-0 align-items-center">
                                <div className="col-5 summaryContent py-2 pr-0">
                                  <span className="label">Your Balance</span>
                                  <p className="price">$7800.50</p>
                                  <p>$7800.50 avaiable</p>
                                </div>
                                <div className="col-5 summaryContent py-2 pl-0">
                                  <span className="label">This Week</span>
                                  <p className="price">$500.50</p>
                                  <p>2 trades</p>
                                </div>
                                <div className="col noDeposit_Sec pr-0">
                                  <div className="float-right">
                                    <span className="noDepositLabel">No pending deposit.</span>
                                    <p className="noDepositMsg">Pending: Deposit account needed to accept payments</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-4">
                              <div className="text-right">
                                <div className="depositeAcc_btn">
                                  <ButtonComp>Add desposite account</ButtonComp>
                                </div>
                              </div>
                            </div>
                          </div>
                        )}

                        <div className="row mx-0 mt-4 align-items-center">
                          <div className="position-relative w-100 d-flex align-items-center">
                            <Tabs
                              tabtransactionbutton={true}
                              tabs={[
                                {
                                  label: <span>Selling</span>,
                                  activeIcon: <img src={Selling_White_Icon} className="sellingIcon"></img>,
                                  inactiveIcon: <img src={Selling_Grey_Icon} className="sellingIcon"></img>,
                                },
                                {
                                  label: <span>Buying</span>,
                                  activeIcon: <img src={Cart_White_Icon} className="buyingIcon"></img>,
                                  inactiveIcon: <img src={Cart_Grey_Icon} className="buyingIcon"></img>,
                                },
                              ]}
                              tabcontent={[{ content: Selling }, { content: Buying }]}
                            />
                            <div className="transactionFilterSec">
                              <div className="FormInput">
                                <InputBox
                                  type="text"
                                  className="inputBox form-control"
                                  name="searchInput"
                                  onChange={this.handleOnchangeInput(`searchInput`)}
                                  autoComplete="off"
                                  placeholder="Search item"
                                ></InputBox>
                                <img src={Search_Blue_Icon} className="searchIcon"></img>
                              </div>
                              <div className="SelectInput">
                                <SelectInput placeholder="Status" options={statusTypes} onChange={this.handleOnSelectInput(`statusType`)} />
                              </div>
                              <div className="DatePickerInput">
                                <DatesRangePicker
                                  startDate={this.state.start_date}
                                  endDate={this.state.end_date}
                                  onApply={this.handleSelectedDateRange}
                                  value={this.state.start_date + ` - ` + this.state.end_date}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      this.state.currentScreen
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .PurchasesSalesPage {
              background: ${BG_LightGREY_COLOR};
            }
            :global(.noTransaction_Sec) {
              display: flex;
              align-items: center;
              justify-content: center;
              height: 21.961vw;
            }
            :global(.noTransactionDollarImg) {
              width: 10.98vw;
              object-fit: cover;
            }
            :global(.noTransactionMsg) {
              font-size: 0.951vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.219vw;
              padding: 1.098vw 0;
              text-transform: capitalize;
            }
            :global(.DatePickerInput div) {
              position: relative;
            }
            :global(.DatePickerInput div .calenderIcon) {
              position: absolute;
              right: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.878vw !important;
              cursor: pointer;
            }
            :global(.daterangepicker) {
              right: 27% !important;
            }
            .noDepositLabel {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.219vw;
              text-transform: capitalize;
            }
            .noDepositMsg {
              font-size: 0.805vw;
              color: ${Red_Color_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.219vw;
              text-transform: capitalize;
            }
            .searchIcon {
              position: absolute;
              right: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              width: 0.878vw;
            }
            :global(.DatePickerInput div input) {
              display: block;
              width: fit-content;
              height: 2.342vw;
              padding: 0.292vw 1.464vw 0.292vw 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.219vw !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              cursor: pointer;
              min-width: 13.899vw;
            }
            :global(.DatePickerInput div input:focus),
            :global(.DatePickerInput div input:active) {
              outline: none;
            }
            .sellingIcon {
              width: 1.171vw;
              margin-right: 0.439vw;
              margin-bottom: 0 !important;
            }
            .buyingIcon {
              width: 1.024vw;
              margin-right: 0.439vw;
              margin-bottom: 0 !important;
            }
            :global(.transactionFilterSec .react-select__control),
            :global(.transactionFilterSec .react-select__value-container) {
              height: 2.342vw !important;
              margin: 0 !important;
              font-size: 0.878vw !important;
              color: ${GREY_VARIANT_1} !important;
              font-family: "Open Sans" !important;
            }
            :global(.transactionFilterSec .react-select__placeholder) {
              font-size: 0.878vw !important;
              color: ${GREY_VARIANT_3} !important;
              font-family: "Open Sans" !important;
            }
            :global(.transactionFilterSec .react-select__single-value) {
              font-size: 0.878vw !important;
              color: ${GREY_VARIANT_1} !important;
              font-family: "Open Sans" !important;
            }
            .SelectInput {
              margin-right: 0.732vw;
              // height: 32px;
            }
            .transactionFilterSec {
              position: absolute;
              top: 0;
              right: 0;
              display: flex;
              align-items: center;
              width: 75%;
            }
            :global(.transactionFilterSec .FormInput) {
              margin: 0 0.732vw;
              position: relative;
            }
            :global(.transactionFilterSec .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.292vw 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0.219vw !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
            }
            :global(.transactionFilterSec .FormInput .inputBox:focus) {
              color: ${GREY_VARIANT_1};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.transactionFilterSec .FormInput .inputBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .tabContent {
              background: ${WHITE_COLOR};
              position: relative;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
            }
            .transactionSummary_Sec {
              background: ${BG_LightGREY_COLOR_2};
              //   opacity: 0.8;
              border-radius: 0.219vw;
            }
            .summaryContent {
              max-width: 8.418vw;
            }
            .noDeposit_Sec > div {
              max-width: 10.248vw;
            }
            .summaryContent .label {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.219vw;
              text-transform: capitalize;
            }
            .summaryContent .price {
              font-size: 1.024vw;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0;
            }
            .summaryContent p {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_10};
              font-family: "Open Sans" !important;
              margin-bottom: 0.585vw;
              text-transform: lowercase;
              margin-bottom: 0;
            }
            :global(.payNow_btn button) {
              width: 11.346vw;
              padding: 0.366vw 0.732vw;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              font-size: 0.805vw;
              border-radius: 0.104vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.payNow_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0366vw !important;
              font-size: 0.805vw;
            }
            :global(.payNow_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.payNow_btn button:focus),
            :global(.payNow_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.depositeAcc_btn button) {
              width: 90%;
              padding: 0.366vw 0.732vw;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0;
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.depositeAcc_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-weight: 600;
              font-size: 0.805vw;
              letter-spacing: 0.0366vw !important;
            }
            :global(.depositeAcc_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.depositeAcc_btn button:focus),
            :global(.depositeAcc_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default PurchasesSalesPage;
