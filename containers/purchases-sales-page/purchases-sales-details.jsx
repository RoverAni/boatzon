import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_1,
  Long_Arrow_Left,
  Dollar_Symbol_Icon,
  GREEN_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  Orange_Color,
  Item_1,
  Dark_Blue_Color,
  Seller_1,
} from "../../lib/config";
import VerticalStepper from "../../components/stepper/vertical-stepper";

class PurchasesSalesDetails extends Component {
  getStepsName = () => {
    return [
      "offer accepted",
      "time to ship",
      "payment scheduled",
      "payment sent",
    ];
  };

  getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <div className="col-12 p-0 mt-4">
            <div className="d-flex">
              <img src={Dollar_Symbol_Icon} className="dollarIcon"></img>
              <h4 className="price">260.98</h4>
            </div>
            <h6 className="stepLabel">Offer accepted</h6>
            <p className="paymentMsg">
              Your payment will be scheduled after the package has been scanned
              by the carrier.
            </p>
            <table className="paymentDetails_Sec">
              <tr>
                <td>Payment info</td>
                <td className="viewReceipt">View receipt</td>
              </tr>
              <tr>
                <td>Transaction ID</td>
                <td>198321</td>
              </tr>
              <tr>
                <td>Byuer paid</td>
                <td>$494.65</td>
              </tr>
              <tr>
                <td>Service fee (7.9%)</td>
                <td>-$24.25</td>
              </tr>
              <tr>
                <td className="bolderFont">You get</td>
                <td className="bolderFont">$470.74</td>
              </tr>
              <tr>
                <td className="bolderFont">Deposit to</td>
                <td className="bolderFont">Visa Debit .... 6985</td>
              </tr>
              <tr>
                <td></td>
                <td className="depositMsg">fast Deposit enabled</td>
              </tr>
            </table>
          </div>
        );
      case 1:
        return "time to ship";
      case 2:
        return "payment scheduled";
      case 3:
        return "payment sent";
    }
  };

  render() {
    return (
      <Wrapper>
        <div className="col-12 px-4">
          <p
            className="d-flex align-items-center m-0 mt-1 back_btn"
            onClick={this.props.handleBackScreen}
          >
            <img src={Long_Arrow_Left} className="leftArrowIcon"></img>{" "}
            <span className="back_btn">Back</span>
          </p>
          <h6 className="heading">Transaction Details</h6>

          <div className="row m-0 align-items-center justify-content-between productDetails_Sec">
            <div className="col-6 p-0">
              <div className="row m-0">
                <div className="col-2 p-0">
                  <img src={Item_1} className="productImg"></img>
                </div>
                <div className="col-10 p-0">
                  <div className="d-flex flex-column ml-3">
                    <span className="productName">Submarine</span>
                    <span className="productStatus">Time to ship</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6 p-0">
              <div className="row m-0">
                <div className="col-10 p-0 text-right">
                  <div className="d-flex flex-column mr-3">
                    <span className="sellerName">Marine Max</span>
                    <span className="paymentDate">Paid on Feb. 23, 2020</span>
                  </div>
                </div>
                <div className="col-2 p-0">
                  <img src={Seller_1} className="sellerImg"></img>
                </div>
              </div>
            </div>
          </div>
          <div className="my-2">
            <VerticalStepper
              getSteps={this.getStepsName}
              getStepContent={this.getStepContent}
            />
          </div>
        </div>
        <style jsx>
          {`
            .heading {
              font-size: 1.317vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0 0 0.732vw 0;
            }
            .back_btn {
              opacity: 0.8;
              cursor: pointer;
            }
            .back_btn span {
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              color: ${FONTGREY_COLOR};
              padding: 0.146vw 0;
            }
            .leftArrowIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            :global(.dollarIcon) {
              width: 0.951vw;
            }
            :global(.price) {
              font-size: 2.928vw;
              color: ${GREEN_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.stepLabel) {
              font-size: 0.878vw;
              color: ${Orange_Color};
              font-family: "Open Sans" !important;
              font-weight: bold;
            }
            :global(.paymentMsg) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.paymentDetails_Sec) {
              border-top: 0.0732vw solid ${GREY_VARIANT_3};
              width: 100%;
            }
            .productDetails_Sec {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
              padding: 0.732vw 0;
            }
            :global(.paymentDetails_Sec tr:first-child td) {
              padding: 1.098vw 0 0 0;
            }
            :global(.paymentDetails_Sec td:nth-child(2)) {
              text-align: right;
            }
            :global(.paymentDetails_Sec td) {
              width: 50%;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              text-transform: capitalize;
              color: ${GREY_VARIANT_2};
              padding-top: 0.366vw;
            }
            :global(.viewReceipt) {
              color: ${GREEN_COLOR} !important;
              font-size: 0.951vw !important;
            }
            :global(.bolderFont) {
              font-weight: bold;
              margin-bottom: 0;
              font-size: 0.878vw !important;
              color: ${GREY_VARIANT_1} !important;
            }
            :global(.depositMsg) {
              color: ${GREY_VARIANT_3} !important;
              font-size: 0.585vw !important;
              padding: 0 !important;
            }
            .productImg {
              width: 4.026vw;
              height: 2.635vw;
              object-fit: cover;
            }
            .sellerImg {
              width: 2.562vw;
              object-fit: cover;
            }
            .productName,
            .sellerName {
              font-size: 0.878vw;
              font-family: "Open Sans-SemiBold" !important;
              text-transform: capitalize;
              color: ${Dark_Blue_Color} !important;
              margin-bottom: ;
            }
            .productStatus {
              font-size: 0.695vw;
              font-family: "Open Sans-SemiBold" !important;
              text-transform: capitalize;
              color: ${Orange_Color} !important;
              margin-bottom: 0;
            }
            .paymentDate {
              font-size: 0.695vw;
              font-family: "Open Sans-SemiBold" !important;
              text-transform: capitalize;
              color: ${GREY_VARIANT_1} !important;
              margin-bottom: 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default PurchasesSalesDetails;
