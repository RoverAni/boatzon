import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  View_Transacrtion_Blue_Icon,
  BG_LightGREY_COLOR,
  GREY_VARIANT_6,
  Orange_Color,
  Red_Color_1,
  Light_Blue_Color,
  THEME_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_2,
  Time_To_Ship_Orange_Icon,
  Canceled_Red_Icon,
  Offer_Accepted_Light_Blue_Icon,
  Payment_Scheduled_Blue_Icon,
  Payment_Sent_Blue_Icon,
} from "../../lib/config";
import { TransactionRecords } from "../../fixtures/transaction-history/transaction-history";
import moment from "moment";
import PaginationComp from "../../components/pagination/pagination";
import SelectInput from "../../components/input-box/select-input";
import InfoTooltip from "../../components/tooltip/info-tooltip";
import { getOwnPosts } from "../../services/my-listing";
import { connect } from "react-redux";

class PurchasesSalesSelling extends Component {
  state = {
    inputpayload: {},
    tooltip: false,
    ownPosts: [],
  };

  handleTooltipToggle = () => {
    this.setState({
      tooltip: !this.state.tooltip,
    });
  };

  componentDidMount() {
    getOwnPosts({ limit: "20", offset: "0" })
      .then((res) => {
        this.setState({ ownPosts: res.data.data });
        console.log("res", res.data.data);
      })
      .catch((err) => console.log("err", err));
  }

  handlestatusColor = (status) => {
    let Status = status.toLowerCase();
    switch (Status) {
      case "time to ship":
        return `${Orange_Color}`;
      case "canceled":
        return `${Red_Color_1}`;
      case "offer accepted":
        return `${Light_Blue_Color}`;
      case "payment scheduled":
        return `${THEME_COLOR}`;
      case "payment sent":
        return `${GREEN_COLOR}`;
    }
  };

  handlestatusIcon = (status) => {
    let Status = status.toLowerCase();
    switch (Status) {
      case "time to ship":
        return `${Time_To_Ship_Orange_Icon}`;
      case "canceled":
        return `${Canceled_Red_Icon}`;
      case "offer accepted":
        return `${Offer_Accepted_Light_Blue_Icon}`;
      case "payment scheduled":
        return `${Payment_Scheduled_Blue_Icon}`;
      case "payment sent":
        return `${Payment_Sent_Blue_Icon}`;
    }
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = this.state.inputpayload;
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: temppayload,
      });
    }
  };
  render() {
    const NumberOfItems = [
      { value: "10", label: "10" },
      { value: "20", label: "20" },
      { value: "30", label: "30" },
    ];

    return (
      <Wrapper>
        <div className="transactionSellingPage my-4">
          <table>
            <tr>
              <th className="itemColumn">
                <p>item</p>
              </th>
              <th className="dateColumn">
                <p>date</p>
              </th>
              <th className="transactionColumn">
                <p>transaction id </p>
              </th>
              <th className="sellerColumn">
                <p>seller</p>
              </th>
              <th className="priceColumn">
                <p>price</p>
              </th>
              <th className="statusColumn">
                <p>status</p>
              </th>
              <th></th>
            </tr>
            {this.state.ownPosts && this.state.ownPosts.length > 0
              ? this.state.ownPosts.map((data, index) => (
                  <tr key={index}>
                    <td>
                      <div className="d-flex align-items-center pl-3">
                        <img src={data.thumbnailImageUrl}></img>
                        <p id="itemName">{data.productName}</p>
                      </div>
                    </td>
                    <td>
                      <p>
                        {moment(data.postedOn).format("MMM")}. {moment(data.postedOn).format("DD")}, {moment(data.postedOn).format("YYYY")}
                      </p>
                    </td>
                    <td>
                      <p>{data.transactionId ? data.transactionId : "000000"}</p>
                    </td>
                    <td>
                      <div className="d-flex align-items-center pl-3">
                        <img src={this.props.userProfileData && this.props.userProfileData.profilePicUrl} className="sellerImg"></img>
                        <p>{data.sellerName}</p>
                      </div>
                    </td>
                    <td>
                      <p>${data.price}</p>
                    </td>
                    <td>
                      {/* <p style={{ color: this.handlestatusColor(data.status) }}>
                        <img src={this.handlestatusIcon(data.status)} className="statusIcon"></img>
                        {data.status}
                      </p> */}
                    </td>
                    <td>
                      <InfoTooltip tooltipContent="View Details" placement="top" color={Light_Blue_Color}>
                        <img src={View_Transacrtion_Blue_Icon} onClick={this.props.handleViewTransactionDetails} className="viewIcon"></img>
                      </InfoTooltip>
                    </td>
                  </tr>
                ))
              : ""}
            {/* {TransactionRecords &&
              TransactionRecords.map((data, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center pl-3">
                      <img src={data.itemImg}></img>
                      <p id="itemName">{data.itemName}</p>
                    </div>
                  </td>
                  <td>
                    <p>{data.date}</p>
                  </td>
                  <td>
                    <p>{data.transactionId}</p>
                  </td>
                  <td>
                    <div className="d-flex align-items-center pl-3">
                      <img src={data.sellerImg} className="sellerImg"></img>
                      <p>{data.sellerName}</p>
                    </div>
                  </td>
                  <td>
                    <p>{data.price}</p>
                  </td>
                  <td>
                    <p style={{ color: this.handlestatusColor(data.status) }}>
                      <img src={this.handlestatusIcon(data.status)} className="statusIcon"></img>
                      {data.status}
                    </p>
                  </td>
                  <td>
                    <InfoTooltip tooltipContent="View Details" placement="top" color={Light_Blue_Color}>
                      <img src={View_Transacrtion_Blue_Icon} onClick={this.props.handleViewTransactionDetails} className="viewIcon"></img>
                    </InfoTooltip>
                  </td>
                </tr>
              ))} */}
          </table>
        </div>
        <div className="paginationSec">
          <div className="row m-0 align-items-center">
            <div className="col-6 p-0">
              <PaginationComp count={8} />
            </div>
            <div className="col-6 p-0">
              <div className="d-flex align-items-center justify-content-end">
                <p className="numpagesMsg">showing 60-70 from 100 results</p>
                <div className="selectInput">
                  <SelectInput placeholder="10" options={NumberOfItems} onChange={this.handleOnSelectInput(`number_of_items`)} />
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            .itemColumn {
              width: 26%;
            }
            .dateColumn {
              width: 13%;
            }
            .statusColumn {
              width: 12%;
            }
            .transactionColumn {
              width: 14%;
            }
            .priceColumn {
              width: 10%;
            }
            .statusColumn {
              width: 20%;
            }
            .sellerColumn {
              width: 16%;
            }
            :global(.selectInput .react-select__placeholder) {
              font-size: 0.805vw;
            }
            :global(.selectInput .react-select__control) {
              min-width: auto !important;
              min-height: 100% !important;
              height: 1.903vw !important;
              margin: 0 !important;
            }
            :global(.selectInput .greyChevronIcon) {
              width: 0.658vw;
              margin-top: 0.219vw;
            }
            :global(.selectInput .react-select__value-container) {
              padding: 0.146vw 0.292vw !important;
            }
            :global(.selectInput .react-select__single-value) {
              font-size: 0.805vw;
            }
            :global(.selectInput .react-select__option) {
              font-size: 0.805vw;
              padding: 0.146vw 0.439vw;
              text-align: center;
            }
            .numpagesMsg {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              margin-bottom: 0;
              max-width: 31%;
              letter-spacing: 0.0219vw !important;
              line-height: 1.3;
            }
            .numpagesMsg:first-letter {
              text-transform: capitalize;
            }
            .transactionSellingPage table tr {
              border-top: 0.0732vw solid ${BG_LightGREY_COLOR};
              cursor: pointer;
            }
            .transactionSellingPage table tr:hover {
              background: ${GREY_VARIANT_6};
            }
            .transactionSellingPage table tr:first-child {
              border-top: none;
              background: none;
            }
            .transactionSellingPage table tr th p {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              padding: 0.219vw 1.098vw;
              margin: 0;
            }
            .transactionSellingPage table tr td:first-child,
            .transactionSellingPage table tr th:first-child {
              padding-left: 0 !important;
            }
            .transactionSellingPage table tr td:last-child,
            .transactionSellingPage table tr th:last-child {
              width: 2.928vw;
              text-align: center;
            }
            .transactionSellingPage table tr td p {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin: 0;
              padding: 1.024vw 1.098vw;
            }
            #itemName {
              padding: 0.732vw 1.464vw;
            }
            .transactionSellingPage table tr td img {
              width: 3.125vw;
              height: 1.934vw;
              object-fit: cover;
            }
            .transactionSellingPage table tr td .sellerImg {
              width: 1.83vw;
              object-fit: cover;
            }
            .transactionSellingPage table tr td .statusIcon {
              width: 10%;
              height: 0.732vw;
              margin-right: 0.366vw;
              margin-bottom: 0.146vw;
              object-fit: contain;
            }
            .viewIcon {
              width: 1.171vw !important;
              object-fit: contain;
              height: unset !important;
              padding: 0.146vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps, null)(PurchasesSalesSelling);
