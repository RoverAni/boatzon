// Main React Components
import React from "react";

// asstes and colors
import { APP_LOGO } from "../../lib/config";

// imported components
import CustomHead from "../html/head/head";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// redux context component
import Context from "../../context/context";

function MainHeader(props) {
  return (
    <Context.Consumer>
      {(Context) => {
        return (
          <Wrapper>
            <header className="container-fluid px-0">
              <CustomHead
                ogTitle={props.ogTitle}
                ogDescription={Context.locale.appDesc}
                ogImage={APP_LOGO}
              />
              {props.children}
            </header>
          </Wrapper>
        );
      }}
    </Context.Consumer>
  );
}

export default MainHeader;
