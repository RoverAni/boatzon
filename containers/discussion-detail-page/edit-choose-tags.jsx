import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  BLACK_COLOR,
  Search_Light_Grey_Icon,
  GREEN_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  Long_Arrow_Left_White_Icon,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import SelectInput from "../../components/input-box/select-input";
import CircularProgressButton from "../../components/button-loader/button-loader";
import { getManufactures } from "../../services/manufacturer";

class EditChooseTags extends Component {
  state = {
    inputpayload: {},
    openDropdown: false,
    options: [
      { name: "Srigar", id: 1 },
      { name: "Sam", id: 2 },
    ],
    selectedCategoryValue: [],
    selectedManufacturerValue: [],
    isMenuOpen: true,
    boatManufacturerList: [],
    total_count: "",
    limit: 15,
  };

  componentDidMount() {
    const { category, categoryId, manufacturerId, manufacturer } =
      this.props.discussionDetailsData;
    this.setState(
      {
        openDropdown: true,
        type:
          this.props.pageName == "advice"
            ? 0
            : this.props.pageName == "project"
            ? 1
            : "",
        selectedCategoryValue: { value: categoryId, label: category },
        selectedManufacturerValue: {
          value: manufacturerId,
          label: manufacturer,
        },
      },
      () => {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`type`] = this.state.type;
        tempPayload[`category`] = category;
        tempPayload[`categoryId`] = categoryId;
        tempPayload[`manufacturer`] = manufacturer;
        tempPayload[`manufacturerId`] = manufacturerId;
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            console.log("PAYLOAD", this.state.inputpayload);
          }
        );
      }
    );
  }

  handleCategoryOnSelect = (event) => {
    this.setState(
      {
        selectedCategoryValue: event,
        isMenuOpen: true,
      },
      () => {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`category`] =
          this.state.selectedCategoryValue.label.toString();
        tempPayload[`categoryId`] =
          this.state.selectedCategoryValue.value.toString();
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            console.log("PAYLOAD", this.state.inputpayload);
          }
        );
      }
    );
  };

  // Function will trigger on select event
  handleManufacturerOnSelect = (event) => {
    this.setState(
      {
        selectedManufacturerValue: event,
        isMenuOpen: true,
      },
      () => {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`manufacturer`] =
          this.state.selectedManufacturerValue.label.toString();
        tempPayload[`manufacturerId`] =
          this.state.selectedManufacturerValue.value.toString();
        this.setState(
          {
            inputpayload: { ...tempPayload },
          },
          () => {
            console.log("PAYLOAD", this.state.inputpayload);
          }
        );
      }
    );
  };

  // Async Boat Manufacturer List API call
  handleBoatManufacturerList = async (search, loadedOptions, { page }) => {
    let payload = {
      searchKey: search,
      limit: this.state.limit,
      offset: page * this.state.limit,
      type: "1", // 0-all manufactureres,1-boats and products,2,engines,3-trailers,4-popular manufactures
    };

    getManufactures(payload)
      .then((res) => {
        let response = res.data;
        let oldPayload = [...this.state.boatManufacturerList];
        let newPayload =
          response && response.data && response.data.length
            ? response.data.map((item) => {
                return {
                  value: item.manufactureId,
                  label: item.manufactureName.toLowerCase(),
                };
              })
            : [];
        this.setState({
          boatManufacturerList: search
            ? [...newPayload]
            : [...oldPayload, ...newPayload],
          total_count: response && response.count ? response.count : 0,
        });
      })
      .catch((err) => {
        console.log("apiCall--err--boatManufacturer", err);
      });

    await this.sleep(2000);

    let hasMore =
      this.state.boatManufacturerList &&
      this.state.boatManufacturerList.length < this.state.total_count;

    return {
      options: this.state.boatManufacturerList,
      hasMore,
      additional: {
        page: page + 1,
      },
    };
  };

  // Function to delay the execution
  sleep = (ms) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });

  render() {
    const {
      selectedManufacturerValue,
      selectedCategoryValue,
      inputpayload,
      isMenuOpen,
    } = this.state;
    const { handleEditDiscussion, apiLoading } = this.props;
    const {
      handleCategoryOnSelect,
      handleManufacturerOnSelect,
      handleBoatManufacturerList,
    } = this;

    const categoryOptions =
      this.props.specialities &&
      this.props.specialities.map((data) => {
        return { value: data.nodeId, label: data.name };
      });

    return (
      <Wrapper>
        <div className="col-12 mx-auto py-3 px-0 text-center ChooseTag_Sec">
          <h6 className="heading">Choose Category & Manufacturer Tags</h6>
          <p className="subHeading">
            Get more answers by choosing the right tags.
          </p>
          {this.state.openDropdown ? (
            <div className="row m-0">
              <div className="col-6">
                <div className="SelectInput">
                  <img
                    src={Search_Light_Grey_Icon}
                    className="searchIcon"
                  ></img>
                  <SelectInput
                    addDiscussion={true}
                    options={categoryOptions}
                    value={selectedCategoryValue}
                    onChange={handleCategoryOnSelect}
                    defaultMenuIsOpen={isMenuOpen}
                    menuIsOpen={isMenuOpen}
                  />
                </div>
                <div className="back_btn">
                  <ButtonComp onClick={this.props.handleBackScreen}>
                    <img
                      src={Long_Arrow_Left_White_Icon}
                      className="backArrowIcon"
                    ></img>{" "}
                    Back
                  </ButtonComp>
                </div>
              </div>
              <div className="col-6">
                <div className="SelectInput">
                  <img
                    src={Search_Light_Grey_Icon}
                    className="searchIcon"
                  ></img>

                  <SelectInput
                    asyncSelect={true}
                    addDiscussion={true}
                    defaultValue={selectedManufacturerValue}
                    value={selectedManufacturerValue}
                    loadOptions={handleBoatManufacturerList}
                    onChange={handleManufacturerOnSelect}
                    additional={{
                      page: 0,
                    }}
                    hideSelectedOptions={false}
                    menuIsOpen={isMenuOpen}
                    defaultMenuIsOpen={isMenuOpen}
                  />
                </div>
                <div className="post_btn">
                  <CircularProgressButton
                    buttonText={"Post"}
                    onClick={handleEditDiscussion.bind(this, inputpayload)}
                    loading={apiLoading}
                  />
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
        <style jsx>
          {`
            .back_btn {
              float: left;
              padding: 0.732vw 0;
            }
            .post_btn {
              float: right;
              padding: 0.732vw 0;
            }
            .heading {
              color: ${BLACK_COLOR};
              font-size: 1.171vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .subHeading {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              padding: 0.732vw 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              opacity: 0.7;
            }
            .searchIcon {
              width: 0.878vw;
              position: absolute;
              top: 0.878vw;
              left: 0.732vw;
              z-index: 2;
              margin-right: 0.732vw;
            }
            .closeIcon {
              width: 0.585vw;
            }
            .SelectInput {
              margin-right: 0.732vw;
              height: 18vw;
              position: relative;
            }
            .ChooseTag_Sec {
              min-height: 29.282vw !important;
            }
            :global(.back_btn button) {
              width: 7.32vw;
              padding: 0.439vw 0;
              background: ${GREY_VARIANT_3};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_3};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_3};
            }
            .backArrowIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              left: 0.366vw;
              width: 0.732vw;
            }
            :global(.post_btn button) {
              width: 7.32vw;
              padding: 0.439vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.post_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.post_btn button:focus),
            :global(.post_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.post_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    specialities: state.specialities,
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(EditChooseTags);
