import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR_1,
  THEME_COLOR,
  GREY_VARIANT_2,
  BG_LightGREY_COLOR,
  Advice_Blue_Icon_Tab,
  WHITE_COLOR,
  Review_Grey_Icon,
  Close_Red_Icon,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  Red_Color_1,
  Follow_Grey_Icon,
  Follow_Blue_Icon,
  Comment_Icon,
  Projects_Blue_Icon,
  My_Profile_Pic,
  Flag_Icon_Grey,
  GREEN_COLOR,
  Like_Bordered_Grey_Icon,
  Saved_Grey_Border_Icon,
} from "../../lib/config";
import {
  formatDate,
  findDayAgo,
} from "../../lib/date-operation/date-operation";
import PageLoader from "../../components/loader/page-loader";
import ButtonComp from "../../components/button/button";
import InputBox from "../../components/input-box/input-box";
import {
  postComment,
  deleteComment,
  editComment,
  deleteDiscussion,
} from "../../services/discussions";
import { setDiscussionComments } from "../../redux/actions/dicussion/discussion";
import { getCookie } from "../../lib/session";
import Snackbar from "../../components/snackbar";
import Router from "next/router";
import Model from "../../components/model/model";
import DeleteDiscussionModel from "./delete-discussion-model";
import DiscussionEditModel from "./discussion-edit-model";

class DiscussionDetailPage extends Component {
  state = {
    inputpayload: {},
    minchats: 3,
    discussionDelete: false,
    discussionEdit: false,
    isEditComment: false,
  };

  componentDidMount() {
    let uid = getCookie("uid");
    this.setState({
      uid,
    });
  }

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    if (event.keyCode == 13 || event.which == 13) {
      this.state.isEditComment
        ? this.handleEditCommentAPI()
        : this.handlePostComment();
    } else {
      let inputControl = event.target;
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[inputControl.name] = inputControl.value;
      this.setState(
        {
          inputpayload: { ...tempPayload },
        },
        () => {
          console.log("PAYLOAD", this.state.inputpayload);
        }
      );
    }
  };

  handleEditCommentAPI = () => {
    let payload = {
      payload: this.state.inputpayload.comment,
      commentId: this.state.commentId,
    };
    editComment(payload)
      .then((res) => {
        console.log("resEdit", res.data);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            let tempPayload = [...this.props.discussionCommentsData];
            let commentIndex =
              tempPayload &&
              tempPayload.findIndex(
                (data) => data._id === this.state.commentId
              );
            let commentPayload = response.data;
            tempPayload.splice(commentIndex, 1, commentPayload);
            this.props.dispatch(setDiscussionComments(tempPayload));
            document.getElementById("commentInput").value = "";
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handlePostComment = () => {
    let payload = {
      payload: this.state.inputpayload.comment,
      discussionId: this.props.discussionDetailsData._id,
    };
    postComment(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            let tempPayload = [...this.props.discussionCommentsData];
            let commentPayload = response.data;
            tempPayload.push(commentPayload);
            this.props.dispatch(setDiscussionComments(tempPayload));
            document.getElementById("commentInput").value = "";
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleViewMoreComments = () => {
    this.setState({
      minchats: this.state.minchats + 2,
    });
  };

  handleEditComment = (commentData, commentIndex) => {
    console.log("resEdit-->", commentData, commentIndex);
    document.getElementById("commentInput").value = commentData.payload;
    this.setState({
      isEditComment: true,
      editDiscussionId: commentData.discussionId,
      commentId: commentData._id,
    });
  };

  handleDeleteComment = (commentData, commentIndex) => {
    console.log("ngdfn", commentData, commentIndex);
    let payload = {
      commentId: commentData._id,
    };
    deleteComment(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            let tempPayload = [...this.props.discussionCommentsData];
            tempPayload.splice(commentIndex, 1);
            this.props.dispatch(setDiscussionComments(tempPayload));
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleDeleteDiscussion = () => {
    let payload = {
      _id: this.props.discussionDetailsData._id,
    };

    deleteDiscussion(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            Router.push(`/discussions`);
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: this.handleErrMsg(err),
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
        this.handleDeleteDiscussionModel();
      });
  };

  handleErrMsg = (error) => {
    console.log("Err", error);
    switch (error.message) {
      case `Request failed with status code 401`:
        return "You are not authorized to delete this discussion";
        break;
      case `Request failed with status code 422`:
        return "mandatory parameter _id of the discussion is missing";
        break;
      default:
        return "Something went wrong";
        break;
    }
  };

  handleDeleteDiscussionModel = () => {
    this.setState({
      discussionDelete: !this.state.discussionDelete,
    });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleEditDiscussionModel = () => {
    this.setState({
      discussionEdit: !this.state.discussionEdit,
    });
  };

  handleDiscussionDetailsPage = (postData) => {
    Router.push(
      `/discussion-details/${postData._source.title.replace(/ /g, "-")}?d_id=${
        postData._id
      }`
    );
  };

  render() {
    const {
      discussionDetailsData,
      relatedDiscussionsData,
      MoreDiscussionsData,
      followStatus,
      discussionCommentsData,
      userProfileData,
    } = this.props;
    const { handleDiscussionDetailsPage } = this;

    console.log("discussionDetailsData", discussionDetailsData);
    return (
      <Wrapper>
        {this.props.openPage ? (
          <div className="discussionDetailsPage">
            <div className="container p-md-0">
              <div className="screenWidth mx-auto py-3">
                <div className="row m-0 justify-content-center">
                  <div className="col-9 pl-0">
                    <div className="contentSec">
                      <div className="position-relative">
                        {discussionDetailsData &&
                        discussionDetailsData.id != this.state.uid ? (
                          <img
                            src={
                              followStatus ? Follow_Blue_Icon : Follow_Grey_Icon
                            }
                            className="followIcon"
                            onClick={
                              followStatus
                                ? this.props.handleUnfollowDiscussion.bind(
                                    this,
                                    discussionDetailsData &&
                                      discussionDetailsData._id
                                  )
                                : this.props.handleFollowDiscussion.bind(
                                    this,
                                    discussionDetailsData &&
                                      discussionDetailsData._id
                                  )
                            }
                          ></img>
                        ) : (
                          ""
                        )}
                        <ul className="list-unstyled d-flex align-items-center specializationList">
                          <li className="postName">
                            <img
                              src={
                                discussionDetailsData &&
                                discussionDetailsData.type == 1
                                  ? Projects_Blue_Icon
                                  : discussionDetailsData &&
                                    discussionDetailsData.type == 0
                                  ? Advice_Blue_Icon_Tab
                                  : ""
                              }
                              className="adviceTabIcon"
                            ></img>
                            <span>
                              {discussionDetailsData &&
                              discussionDetailsData.type == 1
                                ? "Project"
                                : discussionDetailsData &&
                                  discussionDetailsData.type == 0
                                ? "Advice"
                                : ""}
                            </span>
                          </li>
                          <li>
                            {discussionDetailsData &&
                              discussionDetailsData.category}
                          </li>
                          <li>
                            {discussionDetailsData &&
                              discussionDetailsData.manufacturer}
                          </li>
                        </ul>
                      </div>
                      <div className="row m-0">
                        <div className="col-1 pl-0 pr-2 profilePicSec">
                          <img
                            src={
                              discussionDetailsData &&
                              discussionDetailsData.profilePicUrl
                            }
                            className="userDp"
                          ></img>
                        </div>
                        <div className="col p-0">
                          <h4 className="heading">
                            {discussionDetailsData &&
                              discussionDetailsData.title}
                          </h4>
                          <div className="row m-0 align-items-center justify-content-between">
                            <div>
                              <span className="authorName">
                                {discussionDetailsData &&
                                  discussionDetailsData.username}
                              </span>
                              <span className="postedDate">
                                {formatDate(
                                  discussionDetailsData &&
                                    discussionDetailsData.updatedOn,
                                  "LL"
                                )}
                              </span>
                            </div>
                            {discussionDetailsData &&
                            discussionDetailsData.id == this.state.uid ? (
                              <div>
                                <span
                                  target="_blank"
                                  className="editLink"
                                  onClick={this.handleEditDiscussionModel}
                                >
                                  <img
                                    src={Review_Grey_Icon}
                                    className="editIcon"
                                  ></img>{" "}
                                  Edit
                                </span>
                                <span
                                  target="_blank"
                                  className="deleteLink"
                                  onClick={this.handleDeleteDiscussionModel}
                                >
                                  <img
                                    src={Close_Red_Icon}
                                    className="deleteIcon"
                                  ></img>{" "}
                                  Delete
                                </span>
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                          <p className="desc">
                            {discussionDetailsData &&
                              discussionDetailsData.description
                                .split("\n")
                                .map((item, idx) => (
                                  <span key={idx}>
                                    {item}
                                    <br />
                                  </span>
                                ))}
                          </p>
                          <div>
                            <img
                              src={
                                discussionDetailsData &&
                                discussionDetailsData.mainUrl
                              }
                              className="postImg"
                            ></img>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="contentSec mt-3">
                      <div className="row m-0">
                        <div className="col-1 pl-0 pr-2"></div>
                        <div className="col-11 p-0 commentContent">
                          <h5 className="commentHeading">
                            Comments (
                            {discussionCommentsData &&
                              discussionCommentsData.length}
                            )
                          </h5>
                          {discussionCommentsData &&
                          discussionCommentsData.length >
                            this.state.minchats ? (
                            <p
                              className="morecommentsMsg"
                              onClick={this.handleViewMoreComments}
                            >
                              see more comments
                            </p>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                      {discussionCommentsData &&
                        discussionCommentsData.map((data, index) =>
                          index >=
                          discussionCommentsData.length -
                            this.state.minchats ? (
                            <div className="row m-0 commentRow">
                              <div className="col-1 pl-0 pr-2 commentPPSec">
                                <img
                                  src={data.commenterProfile || My_Profile_Pic}
                                  className="commentUserDp"
                                ></img>
                              </div>
                              <div className="col p-0 commentContent">
                                <div className="flagSec">
                                  <img
                                    src={Flag_Icon_Grey}
                                    className="flagIcon"
                                  ></img>
                                </div>
                                <div className="commentSec">
                                  <div className="d-flex align-items-center">
                                    <span className="commentPersonName">
                                      {data.commenterName}
                                    </span>
                                    <span className="commentPostedDate">
                                      | {findDayAgo(data.updatedOn)}
                                    </span>
                                  </div>
                                  <p className="commentDesc">{data.payload}</p>
                                  <div className="media_btns">
                                    <ButtonComp>
                                      <img
                                        src={Like_Bordered_Grey_Icon}
                                        className="likeIcon"
                                      ></img>
                                      Like
                                      {/* | 2 */}
                                    </ButtonComp>
                                    <ButtonComp>
                                      <img
                                        src={Saved_Grey_Border_Icon}
                                        className="saveIcon"
                                      ></img>
                                      Save
                                    </ButtonComp>
                                  </div>
                                  {data.commenterId == this.state.uid ? (
                                    <div className="EditSec">
                                      <span
                                        target="_blank"
                                        className="editLink"
                                        onClick={this.handleEditComment.bind(
                                          this,
                                          data,
                                          index
                                        )}
                                      >
                                        <img
                                          src={Review_Grey_Icon}
                                          className="editIcon"
                                        ></img>{" "}
                                        Edit
                                      </span>
                                      <span
                                        target="_blank"
                                        className="deleteLink"
                                        onClick={this.handleDeleteComment.bind(
                                          this,
                                          data,
                                          index
                                        )}
                                      >
                                        <img
                                          src={Close_Red_Icon}
                                          className="deleteIcon"
                                        ></img>{" "}
                                        Delete
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </div>
                            </div>
                          ) : (
                            ""
                          )
                        )}

                      <div className="row m-0 commentingSec">
                        <div className="col-1 pl-0 commentPPSec">
                          <img
                            src={
                              userProfileData && userProfileData.profilePicUrl
                            }
                            className="commentUserDp"
                          ></img>
                        </div>
                        <div className="col p-0">
                          <div className="FormInput">
                            <div className="leftTip"></div>
                            <InputBox
                              type="text"
                              className="inputBox form-control"
                              name="comment"
                              id="commentInput"
                              onKeyUp={this.handleOnchangeInput}
                              autoComplete="off"
                              placeholder="Write a comment"
                            ></InputBox>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-3 p-0">
                    <div className="relatedDiscussion">
                      {relatedDiscussionsData &&
                      relatedDiscussionsData.length > 0 ? (
                        <>
                          <h6 className="title">Related Discussions</h6>
                          {relatedDiscussionsData &&
                            relatedDiscussionsData.map((data, index) => (
                              <div className="borderTop" key={index}>
                                <p
                                  className="postHeading"
                                  onClick={() =>
                                    handleDiscussionDetailsPage(data)
                                  }
                                >
                                  {data._source.title}
                                </p>
                                <div className="row m-0 align-items-center">
                                  <div className="col-6 pl-0">
                                    <div className="d-flex align-items-center">
                                      <img
                                        src={data._source.profilePicUrl}
                                        className="postuserDp"
                                      ></img>
                                      <span className="postAuthor">
                                        {data._source.username}
                                      </span>
                                    </div>
                                  </div>
                                  <div className="col-6 p-0">
                                    <div className="d-flex align-items-center py-2">
                                      <img
                                        src={Comment_Icon}
                                        className="commentIcon"
                                      ></img>
                                      <span className="commentNum">
                                        {" "}
                                        {data._source.commentCount}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ))}
                        </>
                      ) : (
                        ""
                      )}
                      {MoreDiscussionsData && MoreDiscussionsData.length > 0 ? (
                        <>
                          <h6
                            className={
                              relatedDiscussionsData &&
                              relatedDiscussionsData.length > 0
                                ? "title pt-3"
                                : "title"
                            }
                          >
                            More Discussions
                          </h6>
                          {MoreDiscussionsData &&
                            MoreDiscussionsData.map((data, index) =>
                              index <= 5 ? (
                                <div className="borderTop" key={index}>
                                  <p
                                    className="postHeading"
                                    onClick={() =>
                                      handleDiscussionDetailsPage(data)
                                    }
                                  >
                                    {data._source.title}
                                  </p>
                                  <div className="row m-0 align-items-center">
                                    <div className="col-6 pl-0">
                                      <div className="d-flex align-items-center">
                                        <img
                                          src={data._source.profilePicUrl}
                                          className="postuserDp"
                                        ></img>
                                        <span className="postAuthor">
                                          {data._source.username}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="col-6 p-0">
                                      <div className="d-flex align-items-center py-2">
                                        <img
                                          src={Comment_Icon}
                                          className="commentIcon"
                                        ></img>
                                        <span className="commentNum">
                                          {data._source.commentCount}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                ""
                              )
                            )}
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="Loader">
            <PageLoader loading={!this.props.openPage} />
          </div>
        )}

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        {/* Login Model */}
        <Model
          open={this.state.discussionDelete}
          onClose={this.handleDeleteDiscussionModel}
        >
          <DeleteDiscussionModel
            onClose={this.handleDeleteDiscussionModel}
            handleDeleteDiscussion={this.handleDeleteDiscussion}
          />
        </Model>

        <Model
          open={this.state.discussionEdit}
          onClose={this.handleEditDiscussionModel}
        >
          <DiscussionEditModel
            onClose={this.handleEditDiscussionModel}
            handleDeleteDiscussion={this.handleDeleteDiscussion}
            discussionDetailsData={discussionDetailsData}
          />
        </Model>

        <style jsx>
          {`
            .commentIcon {
              width: 0.878vw;
              margin-right: 0.366vw;
            }
            .commentNum {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.156vw;
            }
            .postAuthor {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .postHeading {
              font-size: 0.805vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              text-transform: capitalize;
            }
            .postuserDp {
              width: 1.317vw;
              height: 1.317vw;
              object-fit: cover;
              margin-right: 0.366vw;
              border-radius: 50%;
            }
            .borderTop {
              border-top: 0.0732vw solid ${BG_LightGREY_COLOR};
              padding: 0.732vw 0 0 0;
              cursor: pointer;
            }
            .title {
              font-size: 0.951vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.585vw;
              text-transform: capitalize;
            }
            .contentSec,
            .relatedDiscussion {
              background: ${WHITE_COLOR};
              padding: 0.878vw 1.098vw 0.878vw 0.878vw;
              border-radius: 0.146vw;
              position: relative;
            }
            .discussionDetailsPage {
              background: ${BG_LightGREY_COLOR_1};
            }
            .specializationList li {
              background: ${BG_LightGREY_COLOR};
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.219vw 0.732vw;
              font-size: 0.732vw;
              margin-right: 0.512vw;
              cursor: pointer;
              border-radius: 0.146vw;
            }
            .postName {
              border: 0.0732vw solid ${THEME_COLOR};
              padding: 0.219vw 0.366vw 0.219vw 0.585vw !important;
              border-radius: 0.146vw;
              background: ${WHITE_COLOR} !important;
            }
            .postName span {
              color: ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
              padding: 0.219vw 0.366vw;
              font-size: 0.732vw;
              background: ${WHITE_COLOR} !important;
              cursor: pointer;
            }
            .adviceTabIcon {
              width: 0.732vw;
              margin-bottom: 0.146vw;
            }
            .profilePicSec {
              max-width: 3.448vw;
            }
            .userDp {
              width: 2.448vw;
              height: 2.448vw;
              object-fit: cover;
              border-radius: 50%;
            }
            .heading {
              font-size: 1.317vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .authorName {
              font-size: 0.805vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              padding-right: 0.732vw;
              border-right: 0.0732vw solid ${GREY_VARIANT_1};
            }
            .postedDate {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              padding-left: 0.732vw;
            }
            .editLink {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2} !important;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin: 0 0.878vw;
              cursor: pointer;
            }
            .deleteLink {
              font-size: 0.805vw;
              color: ${Red_Color_1} !important;
              font-family: "Font Awesome 5 Pro" !important;
              letter-spacing: 0.0146vw !important;
              cursor: pointer;
            }
            .editIcon {
              width: 0.732vw;
              margin-bottom: 0.146vw;
            }
            .deleteIcon {
              width: 0.658vw;
              margin-bottom: 0.146vw;
            }
            .followIcon {
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0;
              width: 0.732vw;
              opacity: 0.7;
              cursor: pointer;
            }
            .desc {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              padding: 0.732vw 0 0 0;
            }
            .postImg {
              width: 70%;
              object-fit: cover;
              padding: 0.732vw 0;
            }
            .commentHeading {
              font-size: 1.098vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              // font-weight: 600;
              text-transform: capitalize;
            }
            .morecommentsMsg {
              font-size: 0.805vw;
              color: ${GREEN_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0 0 0.439vw 0;
              cursor: pointer;
            }
            .morecommentsMsg:first-letter {
              text-transform: capitalize;
            }
            .commentPPSec {
              max-width: 3.396vw;
            }
            .commentUserDp {
              width: 2.396vw;
              height: 2.396vw;
              object-fit: cover;
              border-radius: 50%;
            }
            .commentRow {
              padding: 0.439vw 0;
              cursor: pointer;
            }
            .commentContent {
              position: relative;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .commentSec {
              max-width: 95%;
              position: relative;
            }
            .commentPersonName {
              font-size: 0.805vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding-right: 0.366vw;
            }
            .commentPostedDate {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
            }
            .flagIcon {
              width: 0.732vw;
              object-fit: cover;
            }
            .commentDesc {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin: 0;
              padding: 0.585vw 0;
            }
            :global(.media_btns button) {
              width: fit-content;
              min-width: fit-content;
              padding: 0.439vw 0.585vw 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_2};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.media_btns button span) {
              font-family: "Open Sans" !important;
              font-size: 0.805vw;
            }
            :global(.media_btns button:focus),
            :global(.media_btns button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.media_btns button:hover) {
              background: ${WHITE_COLOR};
            }
            .likeIcon {
              width: 0.732vw;
              object-fit: cover;
              margin-right: 0.219vw;
            }
            .saveIcon {
              width: 0.732vw;
              object-fit: cover;
              margin-right: 0.219vw;
            }
            .commentingSec {
              padding: 0.439vw 0;
            }
            .FormInput {
              position: relative;
            }
            .leftTip:after {
              content: "";
              position: absolute;
              width: 0;
              height: 0;
              border-width: 0.439vw;
              border-style: solid;
              border-color: transparent #ffffff transparent transparent;
              top: 50%;
              transform: translate(0, -50%);
              left: -0.878vw;
            }
            :global(.commentingSec .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${Border_LightGREY_COLOR};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.commentingSec .FormInput .inputBox:focus) {
              color: ${GREY_VARIANT_2};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.inputBox::placeholder:first-letter) {
              text-transform: capitalize;
            }
            .EditSec {
              display: none;
              position: absolute;
              top: 0;
              right: -0.219vw;
            }
            .commentSec:hover .EditSec {
              display: block;
            }
            .flagSec {
              position: absolute;
              top: 0;
              right: 0;
              display: block;
            }
            .commentSec:hover .flagSec {
              display: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(DiscussionDetailPage);
