import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import ButtonComp from "../../components/button/button";
import {
  Border_LightGREY_COLOR_1,
  WHITE_COLOR,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
  FONTGREY_COLOR_Dark,
} from "../../lib/config";

class DeleteDiscussionModel extends Component {
  render() {
    return (
      <Wrapper>
        <div className="deleteModel">
          <p>Are sure you want to delete the Discussion?</p>
          <div className="row m-0 align-items-center justify-content-end">
            <div className="cancel_btn">
              <ButtonComp onClick={this.props.onClose}>Cancle</ButtonComp>
            </div>
            <div className="ok_btn">
              <ButtonComp onClick={this.props.handleDeleteDiscussion}>
                Ok
              </ButtonComp>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            .deleteModel {
              padding: 1.464vw;
              text-align: center;
            }
            .deleteModel {
              font-family: "Museo-Sans" !important;
              font-size: 0.951vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
              margin: 0;
            }
            :global(.cancel_btn button) {
              width: fit-content;
              padding: 0.439vw 0;
              background: ${Border_LightGREY_COLOR_1};
              color: ${WHITE_COLOR};
              margin: 0 0.366vw;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.cancel_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${Border_LightGREY_COLOR_1};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${Border_LightGREY_COLOR_1};
            }
            :global(.ok_btn button) {
              width: 100%;
              padding: 0.439vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.ok_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
            }
            :global(.ok_btn button:focus),
            :global(.ok_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.ok_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default DeleteDiscussionModel;
