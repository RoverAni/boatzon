import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Question_Grey_Icon,
  GREEN_COLOR,
  GREY_VARIANT_10,
  Long_Arrow_Right_White_Icon,
  Profile_Logo,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import InputBox from "../../components/input-box/input-box";
import CircularProgressButton from "../../components/button-loader/button-loader";

class AdviceDiscussionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputpayload: {},
      openPage: false,
      text: {},
      FormValid: false,
      load: false,
    };
    if (typeof window !== "undefined") {
      this.ReactQuill = require("react-quill");
      // this.ImageUploader = require("quill-image-uploader");
      this.Quill = require("quill");
    }
  }

  componentDidMount() {
    this.iconsCustomization();
    const {
      title,
      description,
      mainUrl,
      link,
      _id,
      thumbnailImageUrl,
    } = this.props.finalPayload;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[`title`] = title;
    tempPayload[`description`] = description;
    tempPayload[`mainUrl`] = mainUrl;
    tempPayload[`link`] = link;
    tempPayload[`_id`] = _id;
    tempPayload[`thumbnailImageUrl`] = thumbnailImageUrl;
    let textEditor = {};

    if (description) {
      textEditor.ops = [];
      let textEditor_description = {};
      textEditor_description["insert"] = description;
      textEditor.ops.push(textEditor_description);
      if (mainUrl) {
        let imagUrl = {};
        imagUrl["insert"] = {};
        imagUrl["insert"]["image"] = mainUrl;
        textEditor.ops.push(imagUrl);
      }
      let newLine = {};
      newLine["insert"] = "\n";
      textEditor.ops.push(newLine);
    }
    this.setState(
      {
        inputpayload: { ...tempPayload },
        text: textEditor,
        load: true,
      },
      () => {
        console.log("PAYLOAD", this.state.inputpayload);
      }
    );
  }

  iconsCustomization = () => {
    this.setState(
      {
        openPage: true,
      },
      () => {
        const Quill = this.Quill;
        var icons = Quill.import("ui/icons");
        icons[
          "bold"
        ] = `<svg xmlns="http://www.w3.org/2000/svg" width="10.063" height="12.25" viewBox="0 0 10.063 12.25">
    <path id="_" data-name="   " d="M8.656,6.422a2.868,2.868,0,0,0,1.477-2.6A2.9,2.9,0,0,0,8.246.98,5.49,5.49,0,0,0,6.059.625H1a.45.45,0,0,0-.437.438v.93A.45.45,0,0,0,1,2.43h.9v8.7H1a.432.432,0,0,0-.437.438v.875A.432.432,0,0,0,1,12.875H6.332a6.727,6.727,0,0,0,1.832-.191,3.437,3.437,0,0,0,2.461-3.336A2.887,2.887,0,0,0,8.656,6.422Zm-4.43-3.91H6.059A2.277,2.277,0,0,1,7.1,2.7a1.467,1.467,0,0,1,.684,1.34A1.4,1.4,0,0,1,6.332,5.6H4.227Zm3.063,8.367a2.387,2.387,0,0,1-.848.137H4.227V7.461h2.3A1.608,1.608,0,0,1,8.246,9.184,1.78,1.78,0,0,1,7.289,10.879Z" transform="translate(-0.563 -0.625)" fill="#717c99"/>
  </svg>
  `;
        icons[
          "italic"
        ] = `<svg xmlns="http://www.w3.org/2000/svg" width="7.429" height="12.25" viewBox="0 0 7.429 12.25">
    <path id="_" data-name="   " d="M32.828,11.125H31.9l1.723-8.75h1.094a.421.421,0,0,0,.438-.328l.164-.875a.449.449,0,0,0-.437-.547H30.531a.443.443,0,0,0-.437.355l-.164.875a.422.422,0,0,0,.41.52h.93l-1.7,8.75H28.508a.468.468,0,0,0-.437.356l-.164.875a.427.427,0,0,0,.438.519h4.32a.468.468,0,0,0,.438-.328l.164-.875A.463.463,0,0,0,32.828,11.125Z" transform="translate(-27.897 -0.625)" fill="#717c99"/>
  </svg>`;
        icons[
          "list"
        ].ordered = `<svg xmlns="http://www.w3.org/2000/svg" width="13.949" height="12.25" viewBox="0 0 13.949 12.25">
    <path id="_" data-name="   " d="M82.895,3.578v.246a.3.3,0,0,0,.3.328h1.859a.3.3,0,0,0,.3-.328V3.578a.3.3,0,0,0-.3-.328h-.437V.953a.29.29,0,0,0-.328-.328h-.328a.5.5,0,0,0-.383.164l-.6.547a.3.3,0,0,0,0,.465l.137.164a.307.307,0,0,0,.438.027,1.1,1.1,0,0,0,.082-.109,1.717,1.717,0,0,0-.027.3V3.25H83.2A.3.3,0,0,0,82.895,3.578ZM82.867,8.09V8.2a.316.316,0,0,0,.328.328h1.859a.3.3,0,0,0,.3-.328V7.953a.274.274,0,0,0-.3-.3H83.988c.055-.3,1.313-.52,1.313-1.559A1.114,1.114,0,0,0,84.1,5a1.37,1.37,0,0,0-1.121.52.292.292,0,0,0,.082.438l.246.164c.137.137.3.082.438-.055a.334.334,0,0,1,.246-.109.248.248,0,0,1,.273.246C84.262,6.559,82.867,6.8,82.867,8.09Zm.082,4.1a.289.289,0,0,0,.082.438,1.714,1.714,0,0,0,.984.246,1.178,1.178,0,0,0,1.313-1.2.965.965,0,0,0-.793-.93l.492-.574a.476.476,0,0,0,.137-.383V9.7a.3.3,0,0,0-.3-.328H83.25c-.191,0-.3.109-.3.328v.246a.274.274,0,0,0,.3.3h.355a1.654,1.654,0,0,0,.355-.027,2.688,2.688,0,0,0-.355.383l-.137.191a.391.391,0,0,0-.082.41l.027.055a.332.332,0,0,0,.328.191h.137c.273,0,.438.082.438.273,0,.109-.109.219-.383.219a1.1,1.1,0,0,1-.41-.082c-.164-.109-.3-.109-.41.082Zm3.8-8.832h9.625a.45.45,0,0,0,.438-.437V1.828a.47.47,0,0,0-.437-.437H86.75a.45.45,0,0,0-.437.438V2.922A.432.432,0,0,0,86.75,3.359Zm0,4.375h9.625a.45.45,0,0,0,.438-.437V6.2a.47.47,0,0,0-.437-.437H86.75a.45.45,0,0,0-.437.438V7.3A.432.432,0,0,0,86.75,7.734Zm0,4.375h9.625a.45.45,0,0,0,.438-.437V10.578a.47.47,0,0,0-.437-.437H86.75a.45.45,0,0,0-.437.438v1.094A.432.432,0,0,0,86.75,12.109Z" transform="translate(-82.864 -0.625)" fill="#717c99"/>
  </svg>`;
        icons[
          "list"
        ].bullet = `<svg xmlns="http://www.w3.org/2000/svg" width="14" height="11.375" viewBox="0 0 14 11.375">
    <path id="_" data-name="   " d="M55.031,2.375a1.331,1.331,0,0,0-1.312-1.312,1.313,1.313,0,0,0-1.313,1.313,1.3,1.3,0,0,0,1.313,1.313A1.313,1.313,0,0,0,55.031,2.375ZM53.719,5.438A1.313,1.313,0,0,0,52.406,6.75a1.3,1.3,0,0,0,1.313,1.313A1.313,1.313,0,0,0,55.031,6.75,1.331,1.331,0,0,0,53.719,5.438Zm0,4.375a1.313,1.313,0,0,0-1.313,1.313,1.3,1.3,0,0,0,1.313,1.313,1.313,1.313,0,0,0,1.312-1.312A1.331,1.331,0,0,0,53.719,9.813Zm2.625-6.453h9.625a.45.45,0,0,0,.437-.437V1.828a.47.47,0,0,0-.437-.437H56.344a.45.45,0,0,0-.438.438V2.922A.432.432,0,0,0,56.344,3.359Zm0,4.375h9.625a.45.45,0,0,0,.437-.437V6.2a.47.47,0,0,0-.437-.437H56.344a.45.45,0,0,0-.438.438V7.3A.432.432,0,0,0,56.344,7.734Zm0,4.375h9.625a.45.45,0,0,0,.437-.437V10.578a.47.47,0,0,0-.437-.437H56.344a.45.45,0,0,0-.438.438v1.094A.432.432,0,0,0,56.344,12.109Z" transform="translate(-52.406 -1.063)" fill="#717c99"/>
  </svg>`;
        icons[
          "image"
        ] = `<svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.5" viewBox="0 0 12 10.5">
    <path id="_Add_Photo_Add_Video_Link" data-name=" Add Photo  Add Video  Link" d="M12.656,3.875A1.141,1.141,0,0,0,11.531,2.75H9.468L9.164,2a1.136,1.136,0,0,0-1.055-.75H5.179A1.136,1.136,0,0,0,4.125,2l-.281.75H1.781A1.125,1.125,0,0,0,.656,3.875v6.75A1.111,1.111,0,0,0,1.781,11.75h9.75a1.125,1.125,0,0,0,1.125-1.125ZM9.468,7.25A2.813,2.813,0,1,1,6.656,4.438,2.806,2.806,0,0,1,9.468,7.25Zm-.75,0a2.063,2.063,0,0,0-4.125,0,2.063,2.063,0,0,0,4.125,0Z" transform="translate(-0.656 -1.25)" fill="#717c99"/>
  </svg>`;
        icons[
          "video"
        ] = `<svg xmlns="http://www.w3.org/2000/svg" width="13.5" height="9" viewBox="0 0 13.5 9">
    <path id="_Add_Photo_Add_Video_Link" data-name=" Add Photo  Add Video  Link" d="M98.836,2H92.062a1.121,1.121,0,0,0-1.1,1.125V9.9a1.1,1.1,0,0,0,1.1,1.1h6.773a1.12,1.12,0,0,0,1.125-1.1V3.125A1.141,1.141,0,0,0,98.836,2Zm4.429.891-2.554,1.781v3.68l2.554,1.781a.773.773,0,0,0,1.2-.609V3.5A.773.773,0,0,0,103.265,2.891Z" transform="translate(-90.96 -2)" fill="#717c99"/>
  </svg>`;
        icons[
          "link"
        ] = `<svg xmlns="http://www.w3.org/2000/svg" width="11.989" height="11.988" viewBox="0 0 11.989 11.988">
    <path id="_Add_Photo_Add_Video_Link" data-name=" Add Photo  Add Video  Link" d="M183.886,4.859a3.851,3.851,0,0,0-.468-.4.389.389,0,0,0-.493.023l-.492.492a1.08,1.08,0,0,0-.281.7.492.492,0,0,0,.164.3c.07.047.164.117.258.188a1.7,1.7,0,0,1,0,2.391l-1.594,1.57a1.657,1.657,0,0,1-2.391,0,1.7,1.7,0,0,1,.024-2.391l.3-.3a.359.359,0,0,0,.093-.4,4.489,4.489,0,0,1-.234-1.219.37.37,0,0,0-.633-.258l-.867.867a3.563,3.563,0,1,0,5.039,5.039l1.57-1.57V9.875A3.529,3.529,0,0,0,183.886,4.859Zm3.3-3.3a3.579,3.579,0,0,0-5.039,0l-1.57,1.57v.023a3.529,3.529,0,0,0,0,5.016,3.851,3.851,0,0,0,.468.4.389.389,0,0,0,.493-.023l.492-.492a1.08,1.08,0,0,0,.281-.7.492.492,0,0,0-.164-.3c-.07-.047-.164-.117-.258-.187a1.7,1.7,0,0,1,0-2.391l1.594-1.57a1.657,1.657,0,0,1,2.391,0,1.7,1.7,0,0,1-.024,2.391l-.3.3a.359.359,0,0,0-.093.4,4.488,4.488,0,0,1,.234,1.219.37.37,0,0,0,.633.258l.867-.867A3.579,3.579,0,0,0,187.191,1.555Z" transform="translate(-176.24 -0.518)" fill="#717c99"/>
  </svg>`;
      }
    );
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
      },
      () => {
        this.checkFormValid();
        console.log("PAYLOAD", this.state.inputpayload);
      }
    );
  };

  checkFormValid = () => {
    let FormValid =
      this.state.inputpayload.title &&
      (this.state.inputpayload.description ||
        this.state.inputpayload.mainUrl ||
        this.state.inputpayload.link)
        ? true
        : false;

    this.setState({
      FormValid,
    });
  };

  handleChange = (content, delta, source, editor) => {
    console.log(
      "quillvalue",
      content,
      editor.getSelection(),
      editor.getContents()
    );
    let textEditor = {};
    console.log("nush 11", this.props.finalPayload);
    if (Object.keys(this.props.finalPayload).length === 1) {
      console.log("nush--TRUE");
      textEditor = content;
    } else {
      console.log("nush--FALSE");
      textEditor = editor.getContents();
    }
    console.log("nush", textEditor);
    this.setState({ text: textEditor }, () => {
      let tempPayload = { ...this.state.inputpayload };
      tempPayload[`description`] = editor.getText();
      let imageIndex =
        editor.getContents().ops &&
        editor.getContents().ops.findIndex((data) => {
          return data.insert && data.insert.image;
        });
      if (imageIndex != -1) {
        tempPayload[`mainUrl`] = editor.getContents().ops[
          imageIndex
        ].insert.image;
        tempPayload[`thumbnailImageUrl`] = editor.getContents().ops[
          imageIndex
        ].insert.image;
      }

      tempPayload[`containerHeight`] = this.state.imgHeight;
      tempPayload[`containerWidth`] = this.state.imgWidth;

      let linkIndex =
        editor.getContents().ops &&
        editor.getContents().ops.findIndex((data) => {
          return data.attributes && data.attributes.link;
        });
      if (linkIndex != -1) {
        tempPayload[`link`] = editor.getContents().ops[
          linkIndex
        ].attributes.link;
      }
      this.setState(
        {
          inputpayload: { ...tempPayload },
        },
        () => {
          this.checkFormValid();
          console.log("PAYLOAD", tempPayload);
        }
      );
    });
  };

  handleImageupload = () => {
    const quill = this.quillRef.getEditor();
    const input = document.createElement("input");

    input.setAttribute("type", "file");
    input.setAttribute("accept", "image/*");
    input.click();

    input.onchange = async () => {
      let files = input.files;
      console.log("nfdj", files);
      let file_type = files[0].type.split("/")[0];
      console.log("nfdj", file_type);
      if (
        files &&
        files[0] &&
        (files[0].type == "image/jpeg" || files[0].type == "image/png")
      ) {
        const data = new FormData();
        data.append("file", files[0]);
        data.append("upload_preset", "avzsfq8q");
        const res = await fetch(
          `https://api.cloudinary.com/v1_1/boatzon/${file_type}/upload`,
          {
            method: "POST",
            body: data,
          }
        );

        const file = await res.json();
        try {
          console.log("SuccessRepsonce", file);
          const range = quill.getSelection(true);
          quill.getLine(range.index);
          quill.insertEmbed(range.index + 1, "image", `${file.secure_url}`);
          quill.setSelection(range.index + 2);
        } catch (err) {
          console.log("imager url error", err);
          this.setState({
            fileErr: "imager url error",
            isDocValid: false,
            productpic: false,
          });
        }
      } else {
        console.log(
          "File format is invalid! Image should be of .jpeg or .png format"
        );
        this.setState({
          fileErr:
            "File format is invalid! Image should be of .jpeg or .png format",
          isDocValid: false,
        });
      }
    };
  };

  handleVideoupload = () => {
    const quill = this.quillRef.getEditor();
    const input = document.createElement("input");

    input.setAttribute("type", "file");
    input.setAttribute("accept", "video/*");
    input.click();

    input.onchange = async () => {
      let files = input.files;
      console.log("nfdj", files);
      let file_type = files[0].type.split("/")[0];
      console.log("nfdj", file_type);
      if (files && files[0] && files[0].type == "video/mp4") {
        const data = new FormData();
        data.append("file", files[0]);
        data.append("upload_preset", "avzsfq8q");
        const res = await fetch(
          `https://api.cloudinary.com/v1_1/boatzon/${file_type}/upload`,
          {
            method: "POST",
            body: data,
          }
        );

        const file = await res.json();
        try {
          console.log("SuccessRepsonce", file);
          const range = quill.getSelection(true);
          quill.getLine(range.index);
          quill.insertEmbed(range.index + 1, "video", `${file.secure_url}`);
          quill.setSelection(range.index + 2);
        } catch (err) {
          console.log("video url error", err);
          this.setState({
            fileErr: "video url error",
            isDocValid: false,
            productpic: false,
          });
        }
      } else {
        console.log("File format is invalid! video should be of .mp4 format");
        this.setState({
          fileErr: "File format is invalid! video should be of .mp4 format",
          isDocValid: false,
        });
      }
    };
  };

  quillModules = {
    toolbar: {
      container: [
        ["bold", "italic"],
        [{ list: "bullet" }, { list: "ordered" }],
        ["image", "video", "link"],
      ],
      handlers: {
        image: this.handleImageupload,
        video: this.handleVideoupload,
      },
    },
  };

  render() {
    const ReactQuill = this.ReactQuill;
    // const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
    // const { Quill } = { ...ReactQuill };

    // const ImageUploader = dynamic(() => import("quill-image-uploader"), {
    //   ssr: false,
    // });
    // console.log("ImageUploader", Quill);

    // Quill.register("quillModules/imageUploader", ImageUploader);

    const { title } = { ...this.state.inputpayload };
    const { FormValid, load, text, inputpayload, openPage } = this.state;
    const {
      apiLoading,
      userProfileData,
      onClose,
      handleChooseTagPage,
    } = this.props;
    const { handleOnchangeInput, quillModules, handleChange } = this;
    return (
      <Wrapper>
        {openPage ? (
          <div className="AdviceDiscussionPage_Sec">
            <p className="heading">
              Start a project, get help on your project or show off your before
              & after.
            </p>

            <div className="row m-0 align-items-center">
              <div className="col-1 pl-0">
                <img
                  src={
                    (userProfileData && userProfileData.profilePicUrl) ||
                    Profile_Logo
                  }
                  className="profilePic"
                ></img>
              </div>
              <div className="col-11 p-0">
                <div className="FormInput">
                  <div className="leftTip"></div>
                  <InputBox
                    type="text"
                    className="inputBox form-control"
                    name="title"
                    value={title}
                    onChange={handleOnchangeInput}
                    autoComplete="off"
                    placeholder="advice title"
                  ></InputBox>
                </div>
              </div>
            </div>
            <div className="text-left mt-3 quillTexteditor">
              {typeof window !== "undefined" && ReactQuill ? (
                <ReactQuill
                  key={load}
                  ref={(el) => (this.quillRef = el)}
                  onChange={handleChange}
                  value={text}
                  modules={quillModules}
                  placeholder="What do you need advice on today?"
                />
              ) : (
                <textarea />
              )}
            </div>
            <div className="row m-0">
              <div className="col-6 p-0" style={{ marginTop: "0.642vw" }}>
                <p className="d-flex m-0 align-items-center text-left">
                  <img src={Question_Grey_Icon} className="questionIcon"></img>
                  <span className="note">Add photos to get better answers</span>
                </p>
              </div>
              <div className="col-6 my-3">
                <div className="d-flex align-items-center justify-content-between">
                  <div className="cancel_btn">
                    <ButtonComp onClick={onClose}>Cancel</ButtonComp>
                  </div>
                  <div className={FormValid ? "next_btn" : "cancel_btn"}>
                    <CircularProgressButton
                      buttonText={"Next"}
                      onClick={handleChooseTagPage.bind(this, inputpayload)}
                      disabled={!FormValid}
                      loading={apiLoading}
                    />

                    <img
                      src={Long_Arrow_Right_White_Icon}
                      className="lognArrowIcon"
                    ></img>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            .lognArrowIcon {
              width: 1.83vw;
              height: 0.878vw;
              position: absolute;
              top: 50%;
              right: 1.098vw;
              transform: translate(0, -50%);
            }
            .cancel_btn,
            .next_btn {
              width: 50%;
              position: relative;
            }
            :global(.boldIcon) {
              width: 0.732vw;
            }
            .note {
              font-size: 0.732vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              opacity: 0.7;
            }
            .heading {
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              padding: 1.04166vw 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              opacity: 0.7;
              margin-bottom: 0.52083vw !important;
            }
            .profilePic {
              width: 2.196vw;
              height: 2.196vw;
              object-fit: cover;
              border-radius: 50%;
            }
            .FormInput {
              position: relative;
            }
            .leftTip:after {
              content: "";
              position: absolute;
              width: 0;
              height: 0;
              border-width: 0.439vw;
              border-style: solid;
              border-color: transparent #ffffff transparent transparent;
              top: 50%;
              transform: translate(0, -50%);
              left: -0.732vw;
            }
            :global(.AdviceDiscussionPage_Sec .FormInput .inputBox) {
              display: block;
              width: 100%;
              height: 2.196vw;
              padding: 0.439vw 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.AdviceDiscussionPage_Sec .FormInput .inputBox:focus) {
              color: ${GREY_VARIANT_2};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.inputBox::placeholder) {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .quillTexteditor .quill {
              position: relative;
              background: ${WHITE_COLOR};
              padding: 0.366vw 0.732vw;
              margin: 0;
              min-height: 16.837vw !important;
            }

            :global(.quillTexteditor .quill .ql-toolbar) {
              border: none !important;
              position: absolute !important;
              bottom: 0 !important;
              width: 96%;
              padding: 0.366vw 0;
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR} !important;
            }
            :global(.quillTexteditor .quill .ql-container) {
              border: none !important;
            }
            :global(.quillTexteditor .quill .ql-container .ql-editor) {
              padding: 0.878vw 0;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              font-style: normal !important;
              overflow-y: auto;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
              height: 12.445vw;
            }
            :global(.quillTexteditor .quill .ql-container .ql-editor::before) {
              left: 0;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              font-style: normal !important;
            }
            :global(.quillTexteditor .quill .ql-toolbar > .ql-formats) {
              margin-right: 0;
            }
            :global(.quillTexteditor .quill .ql-container .ql-tooltip),
            :global(.quillTexteditor .quill .ql-container .ql-editing) {
              left: 50% !important;
              top: 1.83vw !important;
              transform: translate(-50%) !important;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(1)
                > .ql-bold
                > svg) {
              width: 0.658vw !important;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(1)
                > .ql-italic
                > svg) {
              width: 0.512vw !important;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(2)
                > .ql-list
                > svg) {
              width: 0.951vw;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)) {
              float: right !important;
              display: flex;
              justify-content: flex-end;
              align-items: center;
              width: 43%;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-image) {
              position: relative;
              width: 6.588vw;
              padding-right: 0;
              display: flex;
              align-items: center;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-image
                > svg) {
              width: 0.878vw;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-image::after) {
              content: "add image";
              position: absolute;
              left: 30%;
              top: unset;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              font-size: 0.732vw;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-video) {
              position: relative;
              width: 6.588vw;
              padding-right: 0;
              display: flex;
              align-items: center;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-video
                > svg) {
              width: 0.951vw;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-video::after) {
              content: "add video";
              position: absolute;
              left: 30%;
              top: unset;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              font-size: 0.732vw;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-link) {
              position: relative;
              width: 3.879vw;
              padding-left: 0;
              padding-right: 0;
              display: flex;
              align-items: center;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-link
                > svg) {
              width: 0.805vw;
            }
            :global(.quillTexteditor
                .quill
                .ql-toolbar
                > .ql-formats:nth-child(3)
                .ql-link::after) {
              content: "link";
              position: absolute;
              left: 35%;
              top: unset;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              font-size: 0.732vw;
            }
            .questionIcon {
              width: 0.732vw;
              margin: 0 0.219vw 0 0.146vw;
            }
            :global(.cancel_btn button) {
              width: 95%;
              padding: 0.439vw 0;
              background: ${GREY_VARIANT_10};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.cancel_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              color: ${WHITE_COLOR};
              font-size: 0.878vw;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: 95%;
              padding: 0.439vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans-Cyrl-Light" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
    apiLoading: state.apiLoading,
  };
};

export default connect(mapStateToProps)(AdviceDiscussionPage);
