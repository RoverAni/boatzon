import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  BLACK_COLOR,
  Advice_Grey_Icon,
  Advice_White_Icon,
  Project_Grey_Icon,
  Project_White_Icon,
} from "../../lib/config";
import Tabs from "../../components/tabs/tabs";
import AdviceDiscussionPage from "./advice-discussion-page";
import ChooseTags from "./choose-tags";
import ProjectDiscussionPage from "./project-discussion-page";
import { addDiscussion } from "../../services/discussions";
import Snackbar from "../../components/snackbar";
import { setApiLoading } from "../../redux/actions/auth";

class AddDiscussionPage extends Component {
  state = {
    currentScreen: "",
    pageName: "",
    tabValue: 0,
    finalPayload: {},
  };

  componentDidMount() {
    let tempPayload = { ...this.state.finalPayload };
    tempPayload.tabValue = this.state.tabValue;
    this.setState({
      finalPayload: { ...tempPayload },
    });
  }
  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleChooseTagPage = (payload) => {
    let tempPayload = { ...this.state.finalPayload };
    this.setState(
      {
        pageName: this.state.tabValue ? "project" : "advice",
        finalPayload: { ...tempPayload, ...payload },
      },
      () => {
        this.updateScreen(
          <ChooseTags
            handleBackScreen={this.handleBackScreen}
            pageName={this.state.pageName}
            finalPayload={this.state.finalPayload}
            handlePostDiscussion={this.handlePostDiscussion}
          />
        );
      }
    );
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleBackScreen = () => {
    switch (this.state.pageName) {
      case "advice":
        this.setState(
          {
            tabValue: 0,
          },
          () => {
            this.updateScreen();
          }
        );
        break;
      case "project":
        this.setState(
          {
            tabValue: 1,
          },
          () => {
            this.updateScreen();
          }
        );
        break;
    }
  };

  handleDiscussionChange = (event, value) => {
    this.setState({ tabValue: value });
  };

  handlePostDiscussion = (payload) => {
    this.props.dispatch(setApiLoading(true));
    let tempPayload = { ...this.state.finalPayload };
    this.setState(
      {
        finalPayload: { ...tempPayload, ...payload },
      },
      () => {
        addDiscussion(this.state.finalPayload)
          .then((res) => {
            let response = res.data;
            if (response) {
              this.props.dispatch(setApiLoading(false));
              this.setState({
                usermessage: response.message,
                variant: this.handleSnackbar(response),
                open: true,
                vertical: "bottom",
                horizontal: "left",
              });
              if (response.code == 200) {
                this.props.onClose();
                this.state.tabValue
                  ? this.props.getProjectDiscussionCategories()
                  : this.props.getAdviceDiscussionCategories();
              }
            }
          })
          .catch((err) => {
            this.props.dispatch(setApiLoading(false));
            this.setState({
              usermessage: err.message,
              variant: "error",
              open: true,
              vertical: "bottom",
              horizontal: "left",
            });
          });
      }
    );
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  render() {
    let tempFinalPayload = {
      tabValue: this.state.tabValue,
    };
    const Advice = (
      <AdviceDiscussionPage
        onClose={this.props.onClose}
        handleChooseTagPage={this.handleChooseTagPage}
        handleBackScreen={this.handleBackScreen}
        finalPayload={
          this.state.finalPayload.tabValue == this.state.tabValue
            ? this.state.finalPayload
            : tempFinalPayload
        }
      />
    );
    const Project = (
      <ProjectDiscussionPage
        onClose={this.props.onClose}
        handleChooseTagPage={this.handleChooseTagPage}
        handleBackScreen={this.handleBackScreen}
        finalPayload={
          this.state.finalPayload.tabValue == this.state.tabValue
            ? this.state.finalPayload
            : tempFinalPayload
        }
      />
    );

    return (
      <Wrapper>
        <div className="AddDiscussionPage_Sec">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto">
              <div className="row m-0">
                {!this.state.currentScreen ? (
                  <div className="col-7 mx-auto py-5 text-center">
                    <h6 className="heading">Add A Discussion</h6>
                    <Tabs
                      adddiscussionbutton={true}
                      addDiscussionValue={this.state.tabValue}
                      handleDiscussionChange={this.handleDiscussionChange}
                      tabs={[
                        {
                          label: <span>Advice</span>,
                          activeIcon: (
                            <img
                              src={Advice_White_Icon}
                              className="adviceIcon"
                            ></img>
                          ),
                          inactiveIcon: (
                            <img
                              src={Advice_Grey_Icon}
                              className="adviceIcon"
                            ></img>
                          ),
                        },
                        {
                          label: <span>Project</span>,
                          activeIcon: (
                            <img
                              src={Project_White_Icon}
                              className="projectIcon"
                            ></img>
                          ),
                          inactiveIcon: (
                            <img
                              src={Project_Grey_Icon}
                              className="projectIcon"
                            ></img>
                          ),
                        },
                      ]}
                      tabcontent={[{ content: Advice }, { content: Project }]}
                    />
                  </div>
                ) : (
                  this.state.currentScreen
                )}
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        <style jsx>
          {`
            .AddDiscussionPage_Sec {
              background: ${BG_LightGREY_COLOR};
            }
            .heading {
              color: ${BLACK_COLOR};
              font-size: 1.171vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 1.14583vw;
            }
            .adviceIcon {
              width: 1.024vw;
              margin-right: 0.366vw;
              margin-top: 0.366vw;
            }
            .projectIcon {
              width: 0.878vw;
              margin-top: 0.292vw;
              margin-right: 0.366vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(AddDiscussionPage);
