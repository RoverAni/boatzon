import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { connect } from "react-redux";
import Router from "next/router";
import {
  Successful_SignUp,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_2,
  GREEN_COLOR,
  Orange_Color,
  WHITE_COLOR,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import { setBoatFilterPayload } from "../../redux/actions/post-data/post-data";

class SuccessfullConfirmation extends Component {
  handleFilter = (slugName) => {
    let payload = {
      manufactureId: this.props.inputpayload.manufacturer_type.value.toString(),
    };

    this.props.dispatch(setBoatFilterPayload(payload));
    setTimeout(() => {
      let newRouter = { ...payload };
      Router.push({
        pathname: `/${slugName}`,
        query: { ...newRouter },
      });
    }, 1000);
  };

  render() {
    const { sellerName, manufacturer_type } = this.props.inputpayload;
    return (
      <Wrapper>
        <div className="confirmedPage col-12 p-0">
          <div className="text-center">
            <img src={Successful_SignUp} className="loaderIcon"></img>
            <h5 className="heading">Congratulations!</h5>
            <p className="caption">
              You’re all set! {sellerName} will contact you shortly to confirm
              your appointment. A Boatzon representative will also contact you
              about your application. If you have any questions, contact us at
              833-262-8966
            </p>
            <div className="row m-0 my-2">
              <div className="col-10 p-0 mx-auto">
                <div className="row m-0 d-flex align-items-center justify-content-between">
                  <div className="boat_btn">
                    <ButtonComp onClick={this.handleFilter.bind(this, "boats")}>
                      View Other {manufacturer_type && manufacturer_type.label}
                    </ButtonComp>
                  </div>
                  <div className="product_btn">
                    <ButtonComp
                      onClick={this.handleFilter.bind(this, "products")}
                    >
                      View Products for{" "}
                      {manufacturer_type && manufacturer_type.label}
                    </ButtonComp>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .confirmedPage {
              display: flex;
              justify-content: center;
              align-items: center;
              height: 100%;
            }
            .loaderIcon {
              width: 4.392vw;
              margin: 0 auto;
              object-fit: cover;
              margin-bottom: 1.098vw;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.366vw;
              padding: 0.732vw 0 0 0;
            }
            .caption {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              line-height: 1.6;
              letter-spacing: 0.0219vw !important;
              max-width: 68%;
              margin: 0 auto 0.585vw auto;
            }
            .boat_btn,
            .product_btn {
              width: fit-content;
            }
            :global(.boat_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.boat_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.boat_btn button:focus),
            :global(.boat_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.boat_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.product_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${Orange_Color};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.product_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.product_btn button:focus),
            :global(.product_btn button:active) {
              background: ${Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.product_btn button:hover) {
              background: ${Orange_Color};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(SuccessfullConfirmation);
