import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_3,
  FONTGREY_COLOR,
  Video_Icon_White,
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_9,
  Clock_Grey_Icon,
  User_Grey_Icon,
  Map_Marker,
  Long_Arrow_Left,
  THEME_COLOR_Opacity,
  Globe_Grey_Icon,
  SelectInput_Chevron_Down_Grey,
  GREY_VARIANT_2,
  Long_Arrow_Left_White_Icon,
  Long_Arrow_Right_White_Icon,
  GREY_VARIANT_10,
  scheduleTimes,
  User_White_Icon,
  Video_Icon_Grey1,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import { Calendar } from "antd";
import CircularProgressButton from "../../components/button-loader/button-loader";
import moment from "moment";

class TrailAppointment extends Component {
  handeMonthNames = (monthValue) => {
    switch (monthValue) {
      case 0:
        return "January";
        break;
      case 1:
        return "February";
        break;
      case 2:
        return "March";
        break;
      case 3:
        return "April";
        break;
      case 4:
        return "May";
        break;
      case 5:
        return "June";
        break;
      case 6:
        return "July";
        break;
      case 7:
        return "August";
        break;
      case 8:
        return "September";
        break;
      case 9:
        return "October";
        break;
      case 10:
        return "November";
        break;
      case 11:
        return "December";
        break;
    }
  };

  handleDayNames = (dayValue) => {
    switch (dayValue) {
      case 0:
        return "Sunday";
        break;
      case 1:
        return "Monday";
        break;
      case 2:
        return "Tuesday";
        break;
      case 3:
        return "Wednesday";
        break;
      case 4:
        return "Thrusday";
        break;
      case 5:
        return "Friday";
        break;
      case 6:
        return "Saturday";
        break;
    }
  };

  render() {
    const CalenderHeader = ({ value, type, onChange, onTypeChange }) => {
      return (
        <div className="calenderHeader d-flex align-items-center justify-content-between">
          <h6 className="m-0">
            {this.handeMonthNames(value.month())} {value.year()}
          </h6>{" "}
          <div>
            <img
              src={Long_Arrow_Left}
              className="leftarrowIcon"
              onClick={this.props.handlePrevMonth.bind(this, value)}
            ></img>
            <img
              src={Long_Arrow_Left}
              className="rightarrowIcon"
              onClick={this.props.handleNextMonth.bind(this, value)}
            ></img>
          </div>
        </div>
      );
    };

    const {
      timeSelectedForCall,
      selectedDate,
      selectTimeForCall,
      onPanelChange,
      handleOnDateChange,
      Apiloading,
      disabledAppointmentDate,
      handleMeetingType,
      todaysBookedTimes,
    } = this.props;
    const { meeting_type, boatAddress } = this.props.inputpayload;
    return (
      <Wrapper>
        <div className="py-3 trialAppointmentPage">
          <div className="col-12 p-0 terms,Sec">
            <div className="row m-0">
              <div className="col-4 leftSec pl-2">
                <div>
                  <h5 className="heading">Appointment Type</h5>
                  <div className="AptBtnSec">
                    <div
                      className={
                        meeting_type == "In-Person"
                          ? "videoAPt_btn"
                          : "personApt_btn"
                      }
                    >
                      <ButtonComp
                        onClick={handleMeetingType.bind(this, "In-Person")}
                      >
                        <img
                          src={
                            meeting_type == "In-Person"
                              ? User_White_Icon
                              : User_Grey_Icon
                          }
                          className="userIcon"
                        ></img>
                        In person Appointment
                      </ButtonComp>
                    </div>
                    <div
                      className={
                        meeting_type == "Video"
                          ? "videoAPt_btn"
                          : "personApt_btn"
                      }
                    >
                      <ButtonComp
                        onClick={handleMeetingType.bind(this, "Video")}
                      >
                        <img
                          src={
                            meeting_type == "Video"
                              ? Video_Icon_White
                              : Video_Icon_Grey1
                          }
                          className="videoIcon"
                        ></img>
                        Video Appointment
                      </ButtonComp>
                    </div>
                  </div>
                  <p className="appointmentInfo">
                    <img src={Clock_Grey_Icon} className="clockIcon"></img>
                    <span>Typically 30 to 60 min</span>
                  </p>
                  <p className="appointmentInfo">
                    <img src={Map_Marker} className="mapMarkerIcon"></img>
                    <span>{boatAddress}</span>
                  </p>
                </div>
              </div>
              <div className="col-8 pr-0 rightSec">
                <h5 className="heading">Select a Date & Time</h5>

                <div className="row m-0 my-3">
                  <div className="col-6 pl-0">
                    <div className="Calender">
                      <Calendar
                        fullscreen={false}
                        value={selectedDate}
                        onPanelChange={onPanelChange}
                        onChange={handleOnDateChange}
                        headerRender={CalenderHeader}
                        disabledDate={disabledAppointmentDate}
                      />
                    </div>
                  </div>
                  <div className="col-6 pr-0">
                    <div>
                      <h6 className="m-0 todayDate">
                        {this.handleDayNames(selectedDate.day())},{" "}
                        {this.handeMonthNames(selectedDate.month())}{" "}
                        {selectedDate.date()}
                      </h6>{" "}
                      <p className="timeStamp">
                        <img src={Globe_Grey_Icon} className="globeIcon"></img>
                        <span>Eastern Time - {moment().format("hh:mm a")}</span>
                        <img
                          src={SelectInput_Chevron_Down_Grey}
                          className="chevronIcon"
                        ></img>
                      </p>
                      <ul
                        key={todaysBookedTimes}
                        className="list-unstyled d-flex align-items-center flex-wrap justify-content-between"
                      >
                        {scheduleTimes.map((k, i) => (
                          <li
                            className={
                              k.ms === timeSelectedForCall
                                ? "timeCard"
                                : todaysBookedTimes &&
                                  todaysBookedTimes.includes(k.time)
                                ? "timeCardBooked"
                                : "timeCardNotSelected"
                            }
                            key={i}
                            onClick={() => selectTimeForCall(k)}
                          >
                            {k.label}
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 p-0 pt-3">
            <div className="d-flex align-items-center justify-content-between">
              <div className="back_btn">
                <ButtonComp onClick={this.props.handleBack}>
                  <img
                    src={Long_Arrow_Left_White_Icon}
                    className="backArrow"
                  ></img>{" "}
                  Back
                </ButtonComp>
              </div>
              <div
                className={
                  this.props.isAppointmentFormValid ? "next_btn" : "back_btn"
                }
              >
                <CircularProgressButton
                  buttonText={
                    <span>
                      Next{" "}
                      <img
                        src={Long_Arrow_Right_White_Icon}
                        className="nextArrow"
                      ></img>
                    </span>
                  }
                  disabled={!this.props.isAppointmentFormValid}
                  onClick={this.props.handleNext}
                  loading={Apiloading}
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .leftSec {
              max-width: 15.373vw;
            }
            .rightSec {
              border-left: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0;
              padding: 0.732vw 0 0 0;
            }
            .AptBtnSec {
              padding: 1.317vw 0;
            }
            .videoAPt_btn {
              margin-bottom: 0.366vw;
            }
            :global(.videoAPt_btn button) {
              width: 100%;
              height: 2.342vw;
              padding: 0;
              background: ${THEME_COLOR};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.videoAPt_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.videoAPt_btn button:focus),
            :global(.videoAPt_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.videoAPt_btn button:hover) {
              background: ${THEME_COLOR};
            }
            .videoIcon {
              position: absolute;
              top: 50%;
              left: 0.585vw;
              transform: translate(0, -50%);
              width: 0.878vw;
            }
            .userIcon {
              position: absolute;
              top: 50%;
              left: 0.585vw;
              transform: translate(0, -50%);
              width: 0.951vw;
            }
            :global(.personApt_btn button) {
              width: 100%;
              height: 2.342vw;
              padding: 0;
              background: ${GREY_VARIANT_9};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.personApt_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_1};
              position: relative;
            }
            :global(.personApt_btn button:focus),
            :global(.personApt_btn button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.personApt_btn button:hover) {
              background: ${GREY_VARIANT_9};
            }
            .appointmentInfo {
              display: flex;
            }
            .appointmentInfo span {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_1};
            }
            .clockIcon {
              width: 0.732vw;
              margin-right: 0.732vw;
            }
            .mapMarkerIcon {
              width: 0.951vw;
              margin-right: 0.732vw;
              margin-bottom: 1.317vw;
            }
            :global(.leftarrowIcon) {
              width: 0.732vw;
              height: 0.878vw;
              margin-right: 0.585vw;
              cursor: pointer;
            }
            :global(.rightarrowIcon) {
              width: 0.732vw;
              height: 0.878vw;
              transform: rotate(180deg);
              cursor: pointer;
            }
            :global(.calenderHeader h6),
            .todayDate {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${FONTGREY_COLOR};
              position: relative;
            }
            .timeStamp {
              display: flex;
              align-items: center;
              justify-content: space-evenly;
              margin: 0.585vw 0;
            }
            .timeStamp span {
              font-family: "Open Sans" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_1};
            }
            .globeIcon {
              width: 0.732vw;
              margin-right: 0.732vw;
            }
            .chevronIcon {
              width: 0.658vw;
              margin-left: 0.732vw;
            }
            .timeCardNotSelected {
              width: 47.5%;
              border: 0.0732vw solid ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${THEME_COLOR};
              text-align: center;
              padding: 0.512vw 0;
              margin-bottom: 0.585vw;
              cursor: pointer;
            }
            .timeCardBooked {
              width: 47.5%;
              border: 0.0732vw solid ${GREY_VARIANT_10};
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${WHITE_COLOR};
              background: ${GREY_VARIANT_10};
              text-align: center;
              padding: 0.512vw 0;
              margin-bottom: 0.585vw;
              cursor: pointer;
            }
            .timeCard {
              width: 47.5%;
              border: 0.0732vw solid ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${WHITE_COLOR};
              background: ${THEME_COLOR};
              text-align: center;
              padding: 0.512vw 0;
              margin-bottom: 0.585vw;
              cursor: pointer;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
            .termsSec {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
              padding-bottom: 5.856vw !important;
            }
            :global(.Calender .ant-picker-calendar .ant-picker-panel) {
              border: none !important;
              padding: 1.098vw 0 !important;
            }
            :global(.Calender .ant-picker-calendar-mini .ant-picker-content) {
              height: auto;
            }
            :global(.Calender
                .ant-picker-calendar
                .ant-picker-panel
                .ant-picker-content) {
              width: auto !important;
            }
            :global(.Calender .ant-picker-calendar) {
              font-size: 0.732vw;
              font-family: "Museo-Sans" !important;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
            }
            :global(.Calender .ant-picker-cell-disabled::before) {
              border-radius: 50% !important;
              background: ${WHITE_COLOR};
            }
            :global(.Calender
                .ant-picker-cell-disabled
                .ant-picker-cell-inner) {
              color: ${GREY_VARIANT_2};
            }
            :global(.Calender
                .ant-picker-cell-in-view.ant-picker-cell-selected
                .ant-picker-cell-inner) {
              border-radius: 50% !important;
              background: ${THEME_COLOR} !important;
              color: ${WHITE_COLOR} !important;
            }
            :global(.Calender .ant-picker-cell .ant-picker-cell-inner) {
              z-index: 1;
            }
            :global(.Calender
                .ant-picker-cell-in-view.ant-picker-cell-today
                .ant-picker-cell-inner::before) {
              border: none !important;
            }
            :global(.Calender
                .ant-picker-cell:not(.ant-picker-cell-disabled):not(.ant-picker-cell-selected)
                .ant-picker-cell-inner) {
              border-radius: 50% !important;
              background: ${THEME_COLOR_Opacity};
              color: ${THEME_COLOR};
            }
            :global(.Calender
                .ant-picker-cell:hover:not(.ant-picker-cell-selected):not(.ant-picker-cell-range-start):not(.ant-picker-cell-range-end):not(.ant-picker-cell-range-hover-start):not(.ant-picker-cell-range-hover-end)
                .ant-picker-cell-inner) {
              border-radius: 50% !important;
            }
            :global(.Calender .ant-picker-content td, .Calender
                .ant-picker-content
                th) {
              max-width: 2.122vw;
              min-width: fit-content;
            }
            :global(.Calender .ant-picker-content th) {
              color: ${GREY_VARIANT_2};
            }
            :global(.Calender .ant-picker-content tbody) {
              padding: 0.732vw 0 0 0;
            }
            :global(.Calender .ant-picker-content tr) {
              display: flex;
              align-items: center;
              justify-content: space-evenly;
              width: auto !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default TrailAppointment;
