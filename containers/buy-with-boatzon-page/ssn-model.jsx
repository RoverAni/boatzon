import React, { Component } from "react";
import InputBox from "../../components/input-box/input-box";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_10,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  WHITE_COLOR,
} from "../../lib/config";
import { NumberValidator } from "../../lib/validation/validation";
import ButtonComp from "../../components/button/button";
import CircularProgressButton from "../../components/button-loader/button-loader";

class SsnModel extends Component {
  render() {
    const {
      handleOnchangeInput,
      onClose,
      ssnapiloading,
      SSNFormValid,
      handleGetCreditWithSSN,
    } = this.props;
    const { SSN } = { ...this.props.inputpayload };
    return (
      <Wrapper>
        <div className="SsnModel_Sec col-12 p-4">
          <h6 className="heading">Almost There...</h6>
          <p className="desc pb-3">
            Based on the name and address you provided, we are unable to locate
            your credit file information. if you wish to continue, please
            provide your SSN and click Submit to continue with the
            pre-qualification process. Otherwise, click No Thank you.
          </p>
          <p className="desc">
            As a reminder this does not log a hard inquiry on your credit file
            or affect your credit score
          </p>
          <div className="pt-3">
            <InputBox
              type="text"
              autoFocus={true}
              className="inputBox form-control"
              name="SSN"
              value={SSN}
              placeholder="SSN"
              onChange={handleOnchangeInput}
              autoComplete="off"
              onKeyPress={NumberValidator}
            ></InputBox>
          </div>
          <div className={SSNFormValid ? "next_btn" : "back_btn"}>
            <CircularProgressButton
              buttonText="Submit"
              disabled={!SSNFormValid}
              onClick={handleGetCreditWithSSN}
              loading={ssnapiloading}
            />
          </div>
          <div className="back_btn">
            <ButtonComp onClick={onClose}>No Thank you</ButtonComp>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: inherit !important;
            }
            .SsnModel_Sec {
              width: 30vw;
              position: relative;
              background: ${WHITE_COLOR} !important;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
            }
            .desc {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0.219vw;
              line-height: 1.2;
            }
            :global(.SsnModel_Sec .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              margin: 0.366vw 0;
              font-family: "Open Sans" !important;
            }
            :global(.SsnModel_Sec .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.SsnModel_Sec .inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            .back_btn,
            .next_btn {
              margin: 15px 0;
            }
            :global(.back_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: 100%;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SsnModel;
