import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import { PureTextValidator, validatePhoneNumber } from "../../lib/validation/validation";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  Question_LightGrey_Icon,
  Question_LightBlue_Icon,
  WHITE_COLOR,
  Long_Arrow_Right_White_Icon,
  GREY_VARIANT_10,
  Red_Color,
} from "../../lib/config";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { withStyles } from "@material-ui/core";
import { getCountries, getStates } from "country-state-picker";
import SelectInput from "../../components/input-box/select-input";
import { DatePicker } from "antd";
import IntlTelInput from "react-intl-tel-input";
import CircularProgressButton from "../../components/button-loader/button-loader";
import moment from "moment";
import { isEmpty } from "../../lib/global";

const styles = (theme) => ({
  radiogrouRoot: {
    flexDirection: "row",
  },
  radioLabelRoot: {
    margin: "0",
  },
  radioLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Museo-Sans !important",
  },
  root: {
    paddingLeft: "0",
    color: `${GREY_VARIANT_3}`,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
    "& span": {
      "& div": {
        "& svg": {
          width: "0.951vw",
        },
      },
    },
  },
  checked: {
    color: `${THEME_COLOR} !important`,
  },
});

class PersonalInfoPage extends Component {
  componentDidMount() {
    this.validatePhoneInput();
  }

  // Number Validation function for phoneInput
  validatePhoneInput = () => {
    validatePhoneNumber("phoneNum");
  };

  componentDidUpdate = (prevProps) => {
    if (
      prevProps.inputpayload.mobile !== this.props.inputpayload.mobile ||
      prevProps.inputpayload.countryCode !== this.props.inputpayload.countryCode
    ) {
      this.validatePhoneInput();
    }
  };

  render() {
    const { first_name, last_name, Email, dob, mobile, address, country, city, stateName, zipCode, maritalStatus } = {
      ...this.props.inputpayload,
    };
    const { apiloading, preferredCountries, latLngAPI } = this.props;
    const { classes } = this.props;

    const StateList = getStates(this.props.countryShortName).map((data) => ({
      value: data,
      label: data,
    }));

    const CountryList = getCountries().map((data) => ({
      value: data.code,
      label: data.name,
    }));

    return (
      <Wrapper>
        <div className="py-3 PersonalInfoPage">
          <div className="col-12 section">
            <h6 className="title">
              Your Information{" "}
              <span className="helperText">
                (<span className="mandatory">*</span> Required Field)
              </span>
            </h6>
            <div className="row m-0">
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tbody>
                    <tr>
                      <th>
                        First Name: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <InputBox
                          type="text"
                          autoFocus={true}
                          className="inputBox form-control"
                          name="first_name"
                          value={first_name}
                          placeholder="First Name"
                          onChange={this.props.handleOnchangeInput}
                          autoComplete="off"
                          onKeyPress={PureTextValidator}
                          required={true}
                        ></InputBox>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Last Name: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="last_name"
                          value={last_name}
                          placeholder="Last Name"
                          onChange={this.props.handleOnchangeInput}
                          autoComplete="off"
                          onKeyPress={PureTextValidator}
                          required={true}
                        ></InputBox>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Contact Number: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <div className="PhoneInput">
                          <div className="position-relative">
                            <IntlTelInput
                              key={preferredCountries}
                              preferredCountries={preferredCountries}
                              containerClassName="intl-tel-input"
                              value={mobile}
                              onSelectFlag={this.props.handleOnchangeflag}
                              onPhoneNumberChange={this.props.handleOnchangePhone}
                              formatOnInit={false}
                              separateDialCode={true}
                              fieldId="phoneNum"
                              autoComplete="off"
                              required={true}
                            />
                          </div>
                        </div>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th></th>
                      <td>
                        {this.props.inputpayload.hasOwnProperty("mobile") && !isEmpty(this.props.inputpayload.mobile) ? (
                          this.props.isPhoneValid ? (
                            " "
                          ) : (
                            <p className="errMessage" style={{ color: `${Red_Color}` }}>
                              Phone Number not valid
                            </p>
                          )
                        ) : (
                          ""
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Email Address: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <InputBox
                          type="email"
                          className="inputBox form-control"
                          name="Email"
                          value={Email}
                          placeholder="Email address"
                          onChange={this.props.handleOnchangeInput}
                          autoComplete="off"
                          required={true}
                        ></InputBox>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th></th>
                      <td>
                        {this.props.inputpayload.hasOwnProperty("Email") && !isEmpty(this.props.inputpayload.Email) ? (
                          this.props.EmailValid == 1 ? (
                            " "
                          ) : (
                            <p className="errMessage" style={{ color: `${Red_Color}` }}>
                              Email is not valid
                            </p>
                          )
                        ) : (
                          ""
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Date of Birth: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <div className="dobInput">
                          <DatePicker
                            defaultPickerValue={moment().subtract(18, "year")}
                            onChange={this.props.handleDOBChange}
                            format="MM-DD-YYYY"
                            value={dob}
                            placeholder="MM-DD-YYYY"
                            disabledDate={this.props.disabledDate}
                          />
                        </div>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Marital Status: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <FormControl component="fieldset">
                          <RadioGroup
                            classes={{
                              root: classes.radiogrouRoot,
                            }}
                            aria-label="maritalStatus"
                            name="maritalStatus"
                            value={maritalStatus ? maritalStatus : ""}
                            onChange={this.props.handleRadioInputChange}
                          >
                            <FormControlLabel
                              classes={{
                                root: classes.radioLabelRoot,
                                label: classes.radioLabel,
                              }}
                              value="Single"
                              control={
                                <Radio
                                  classes={{
                                    root: classes.root,
                                    checked: classes.checked,
                                  }}
                                />
                              }
                              label="Single"
                            />
                            <FormControlLabel
                              classes={{
                                root: classes.radioLabelRoot,
                                label: classes.radioLabel,
                              }}
                              value="Married"
                              control={
                                <Radio
                                  classes={{
                                    root: classes.root,
                                    checked: classes.checked,
                                  }}
                                />
                              }
                              label="Married"
                            />
                          </RadioGroup>
                        </FormControl>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tbody>
                    <tr>
                      <th className="headerWidth">
                        Address: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="address"
                          value={address}
                          placeholder="Enter Address"
                          autoComplete="off"
                          onChange={this.props.handleOnchangeInput}
                          required={true}
                        ></InputBox>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th className="headerWidth">
                        Country: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <div className="widthDiv">
                          <SelectInput
                            servicesSelect={true}
                            placeholder="Select Country"
                            options={CountryList}
                            value={country}
                            onChange={this.props.handleOnSelectInput(`country`)}
                          />
                        </div>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th className="headerWidth">
                        State: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <div className="widthDiv">
                          <SelectInput
                            servicesSelect={true}
                            placeholder="Select State"
                            options={StateList}
                            value={stateName}
                            onChange={this.props.handleOnSelectInput(`stateName`)}
                          />
                        </div>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th className="headerWidth">
                        City: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="city"
                          value={city}
                          placeholder="Enter City"
                          autoComplete="off"
                          onChange={this.props.handleOnchangeInput}
                          required={true}
                        ></InputBox>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                    <tr>
                      <th className="headerWidth">
                        Zip Code: <span className="mandatory">*</span>
                      </th>
                      <td>
                        <InputBox
                          type="text"
                          className="inputBox form-control"
                          name="zipCode"
                          value={zipCode}
                          placeholder="Zip Code"
                          autoComplete="off"
                          onChange={this.props.handleOnchangeInput}
                          required={true}
                        ></InputBox>
                        <div className="questionIcon"></div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="col-12 p-0 py-2">
            <div className="d-flex align-items-center justify-content-between">
              <div className="back_btn"></div>
              <div className={this.props.isPersonalInfoFormValid ? "next_btn" : "back_btn"}>
                <CircularProgressButton
                  buttonText={
                    <span>
                      Next <img src={Long_Arrow_Right_White_Icon} className="nextArrow"></img>
                    </span>
                  }
                  disabled={!this.props.isPersonalInfoFormValid}
                  onClick={this.props.handleNext}
                  loading={apiloading || latLngAPI}
                />
              </div>
            </div>
          </div>
          <div className="col-12">
            <p className="terms text-center">
              You agree that by providing your phone number, Boatzon, or Boatzon's <b>authorized representatives*</b>, may call and/or send
              text messages (including by using equipment to automatically dial telephone numbers) about your interest in a purchase, for
              marketing/sales purposes, or for any other servicing or informational purpose related to your account. You do not have to
              consent to receiving calls or texts to purchase from Boatzon.
              <b>*Including, but not limited to, affiliated banks, insurance carriers, and warranty providers.</b>
            </p>
          </div>
        </div>
        <style jsx>
          {`
            .terms {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-weight: 500;
            }
            .helperText {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-weight: 500;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .errMessage {
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.366vw 0 0 0;
              text-transform: capitalize;
            }
            .heading {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              font-style: italic;
              letter-spacing: 0.0219vw !important;
            }
            .widthDiv {
              width: 95%;
            }
            .dobInput {
              width: 95% !important;
            }
            :global(.dobInput .ant-picker) {
              margin-top: 0.366vw;
              height: 2.196vw;
              width: 100%;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
            }
            :global(.dobInput .ant-picker-focused) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.dobInput .ant-picker-input input) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.dobInput .ant-picker-input input::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.ant-picker-dropdown) {
              font-size: 0.805vw !important;
            }
            :global(.ant-picker-date-panel) {
              width: 16.837vw !important;
            }
            :global(.ant-picker-date-panel .ant-picker-content) {
              width: 15.373vw !important;
              table-layout: auto;
            }
            :global(.ant-picker-panel .ant-picker-footer) {
              display: none;
            }
            .caption {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              font-style: italic;
              letter-spacing: 0.0219vw !important;
            }
            .title {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
              padding-bottom: 0.585vw;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .section {
              padding: 1.098vw 0;
            }
            .contentTabel {
              width: 90%;
            }
            .contentTabel th {
              width: 40%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
            }
            .contentTabel .headerWidth {
              width: 35% !important;
            }
            .questionIcon {
              background: url(${Question_LightGrey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
              cursor: pointer;
            }
            .questionIcon:hover {
              background: url(${Question_LightBlue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
            }
            .contentHeading {
              font-size: 0.915vw;
              color: ${FONTGREY_COLOR};
              font-weight: 600;
              margin-bottom: 0.366vw;
              font-family: "Open Sans" !important;
            }
            .contentTabel td {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
            }
            .marginTop {
              margin-top: 0.366vw;
            }
            :global(.contentTabel td .inputBox) {
              width: 95%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              margin-top: 0.366vw;
              font-family: "Open Sans" !important;
            }
            :global(.contentTabel td .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.contentTabel td .inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }

            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.PersonalInfoPage .PhoneInput) {
              position: relative;
              margin-top: 0.366vw;
              width: 93%;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input) {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
              border: none;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.342vw;
              padding: 0;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input > #phoneNum) {
              width: 81%;
              height: 100%;
              line-height: 1 !important;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              padding: 0 0.366vw !important;
              margin-left: 0.366vw !important;
              flex-grow: 1;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .country-list) {
              width: 14.641vw !important;
              margin: 0.219vw 0 0 0;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .country-list .country) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input > #phoneNum:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container) {
              height: 2.342vw;
              position: relative !important;
              width: fit-content !important;
              display: block ruby !important;
              padding: 0 !important;
              flex-grow: 2;
              border-radius: 0.292vw;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag) {
              width: 100% !important;
              background: ${WHITE_COLOR} !important;
              height: 100%;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.292vw;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag:focus),
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag:active) {
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.292vw;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag .selected-dial-code) {
              font-size: 0.805vw;
              height: 2.122vw;
              align-items: center;
              display: flex;
              padding: 0 0.366vw;
              line-height: 1 !important;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag:focus) {
              box-shadow: none !important;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag .iti-flag) {
              display: none;
            }
            :global(.PersonalInfoPage .PhoneInput > div > .intl-tel-input .flag-container .selected-flag .iti-arrow) {
              display: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(PersonalInfoPage);
