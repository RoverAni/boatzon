import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Confirming_Loader,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_2,
} from "../../lib/config";
import SuccessfullConfirmation from "./successfull-confirmation";

class ConfirmingPage extends Component {
  state = {};
  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  componentDidMount() {
    this.handleSuccessfullConfirmation();
  }

  handleSuccessfullConfirmation = () => {
    setTimeout(() => {
      this.updateScreen(
        <SuccessfullConfirmation inputpayload={this.props.inputpayload} />
      );
    }, 2000);
  };
  render() {
    return (
      <Wrapper>
        {!this.state.currentScreen ? (
          <div className="confirmingPage">
            <div className="text-center">
              <img src={Confirming_Loader} className="loaderIcon"></img>
              <h5 className="heading">Confirmation ...</h5>
              <p className="caption">
                We are organizing all your documentation for Marine Max.
              </p>
            </div>
          </div>
        ) : (
          this.state.currentScreen
        )}
        <style jsx>
          {`
            .confirmingPage {
              display: flex;
              justify-content: center;
              align-items: center;
              height: 100%;
            }
            .loaderIcon {
              width: 4.392vw;
              margin: 0 auto;
              object-fit: cover;
              margin-bottom: 1.464vw;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.366vw;
              padding: 0.732vw 0 0 0;
            }
            .caption {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              font-family: "Museo-Sans" !important;
              line-height: 1.6;
              letter-spacing: 0.0219vw !important;
              max-width: 60%;
              margin: 0 auto 0.585vw auto;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ConfirmingPage;
