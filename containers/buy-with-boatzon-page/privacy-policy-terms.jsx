import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  THEME_COLOR,
  THEME_COLOR_Opacity2,
  WHITE_COLOR,
} from "../../lib/config";

class PrivacyPolicyTerms extends Component {
  render() {
    const { onClose, pageOpen } = this.props;
    return (
      <Wrapper>
        <div className="col-12 p-4 PrivacyPolicyTerms_Sec mx-auto">
          {pageOpen ? (
            ""
          ) : (
            <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
          )}
          <div className="row m-0">
            <div className="col-3 p-0">
              <h4 className="heading" style={{ height: "1.866vw" }}>
                FACTS
              </h4>
            </div>
            <div className="col-9 p-0">
              <h6 className="caption" style={{ height: "1.866vw" }}>
                WHAT DOES BOATZON DO WITH YOUR PERSONAL INFORMATION?
              </h6>
            </div>
          </div>
          <div className="row m-0">
            <div className="col-1 p-0">
              <div className="label_sec">
                <p className="label" style={{ height: "1.734vw" }}>
                  Why?
                </p>
              </div>
            </div>
            <div className="col-11 p-0">
              <div className="desc_sec">
                <p className="desc_text">
                  Financial companies choose how they share your personal
                  information. Federal law gives consumers the right to limit
                  some but not all sharing. Federal law also requires us to tell
                  you how we collect, share, and protect your personal
                  information. Please read this notice carefully to understand
                  what we do.
                </p>
              </div>
            </div>
          </div>
          <div className="row m-0">
            <div className="col-1 p-0">
              <div className="label_sec">
                <p className="label" style={{ height: "4.202vw" }}>
                  What?
                </p>
              </div>
            </div>
            <div className="col-11 p-0">
              <div className="desc_sec">
                <p className="desc_text">
                  The types of personal information we collect and share depend
                  on the product or service you have with us. This information
                  can include:
                </p>
                <div className="col-12 p-0 minimalPadding">
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Social Security number and income
                      </p>
                    </div>
                  </div>
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Account balance and payment history
                      </p>
                    </div>
                  </div>
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Credit history and employment information
                      </p>
                    </div>
                  </div>
                </div>
                <p className="desc_text">
                  When you are no longer our customer, we continue to share your
                  information as described in this notice.
                </p>
              </div>
            </div>
          </div>
          <div className="row m-0">
            <div className="col-1 p-0">
              <div className="label_sec" style={{ height: "1.734vw" }}>
                <p className="label">How?</p>
              </div>
            </div>
            <div className="col-11 p-0">
              <div className="desc_sec">
                <p className="desc_text">
                  All financial companies need to share customers' personal
                  information to run their everyday business. In the section
                  below, we list the reasons financial companies can share their
                  customers' personal information; the reasons BOATZON chooses
                  to share; and whether you can limit this sharing.
                </p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="label_sec m-0 align-items-center">
                <p className="label">
                  REASONS WE CAN SHARE YOUR PERSONAL INFORMATION
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="label_sec m-0 align-items-center justify-content-center">
                <p className="label">What do we share?</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="label_sec m-0 align-items-center justify-content-center">
                <p className="label">Can you limit this sharing?</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For our everyday business purposes — </b>such as to process
                  your transactions,maintain your account(s), respond to court
                  orders and legal investigations, or report to credit bureaus
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div
                className="desc_sec m-0 align-items-center justify-content-center"
                style={{ height: "1.734vw" }}
              >
                <p className="desc_text">Yes</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div
                className="desc_sec m-0 align-items-center justify-content-center"
                style={{ height: "1.734vw" }}
              >
                <p className="desc_text">No</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For our everyday business purposes — </b> to offer our
                  products and services to you
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">Yes</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">No</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For joint marketing with other financial companies</b>
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">Yes</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">No</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For our affiliates’ everyday business purposes — </b>
                  information about your transactions and experiences
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">Yes</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">No</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For our affiliates’ everyday business purposes — </b>
                  information about your creditworthiness
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">No</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">We don't share</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For our affiliates to market to you</b>
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">No</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec m-0 align-items-center justify-content-center">
                <p className="desc_text">We don't share</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-8 p-0">
              <div className="desc_sec align-items-center justify-content-center">
                <p className="desc_text maxWidth">
                  <b>For nonaffiliates to market to you</b>
                </p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec align-items-center justify-content-center">
                <p className="desc_text">No</p>
              </div>
            </div>
            <div className="col-2 p-0 text-center">
              <div className="desc_sec align-items-center justify-content-center">
                <p className="desc_text">We don't share</p>
              </div>
            </div>
          </div>

          <div className="row m-0 d-flex align-items-center">
            <div className="col-3 p-0">
              <div className="label_sec m-0 align-items-center">
                <p className="label">Questions?</p>
              </div>
            </div>
            <div className="col-9 p-0">
              <div
                className="desc_sec m-0 align-items-start justify-content-center"
                style={{ height: "1.200vw" }}
              >
                <p className="desc_text">Call 833-262-8966 or go to N/A</p>
              </div>
            </div>
          </div>
          <div className="row m-0 mt-1 d-flex align-items-center">
            <div className="col-12 p-0">
              <div className="label_sec m-0 align-items-center">
                <p className="label">WHO WE ARE</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-4 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">
                  <b>Who is providing this notice?</b>
                </p>
              </div>
            </div>
            <div className="col-8 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">BOATZON</p>
              </div>
            </div>
          </div>
          <div className="row m-0 mt-1 d-flex align-items-center">
            <div className="col-12 p-0">
              <div className="label_sec m-0 align-items-center">
                <p className="label">WHAT WE DO</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-start">
            <div className="col-4 p-0">
              <div
                className="desc_sec minimalPadding m-0 align-items-start justify-content-center"
                style={{ height: "1.881vw" }}
              >
                <p className="desc_text">
                  <b>How is my personal information protected?</b>
                </p>
              </div>
            </div>
            <div className="col-8 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">
                  To protect your personal information from unauthorized access
                  and use, we use security measures that comply with federal
                  law. These measures include computer safeguards and secured
                  files and buildings
                </p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-start">
            <div className="col-4 p-0">
              <div
                className="desc_sec minimalPadding m-0 align-items-start justify-content-center"
                style={{ height: "4.344vw" }}
              >
                <p className="desc_text">
                  <b>How is my personal information collected?</b>
                </p>
              </div>
            </div>
            <div className="col-8 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">
                  We collect your personal information, for example, when you
                </p>
                <div className="col-12 p-0 minimalPadding">
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">Apply for financing</p>
                    </div>
                  </div>
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Give us your income information or provide employment
                        information
                      </p>
                    </div>
                  </div>
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Provide account information or give us your contact
                        information
                      </p>
                    </div>
                  </div>
                </div>
                <p className="desc_text maxWidth">
                  We also contact your personal information from others, such as
                  credit bureaus, affiliates or other companies
                </p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-start">
            <div className="col-4 p-0">
              <div
                className="desc_sec minimalPadding m-0 align-items-start justify-content-center"
                style={{ height: "4.344vw" }}
              >
                <p className="desc_text">
                  <b>Why can’t I limit all sharing?</b>
                </p>
              </div>
            </div>
            <div className="col-8 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">
                  Federal law gives you the right to limit only
                </p>
                <div className="col-12 p-0 minimalPadding">
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        {" "}
                        Sharing for affiliates’ everyday business
                        purposes—information about your creditworthiness
                      </p>
                    </div>
                  </div>
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Affiliates from using your information to market to you
                      </p>
                    </div>
                  </div>
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Sharing for nonaffiliates to market to you
                      </p>
                    </div>
                  </div>
                </div>
                <p className="desc_text maxWidth">
                  State laws and individual companies may give you additional
                  rights to limit sharing.
                </p>
              </div>
            </div>
          </div>

          <div className="row m-0 mt-1 d-flex align-items-center">
            <div className="col-12 p-0">
              <div className="label_sec m-0 align-items-center">
                <p className="label">DEFINITIONS</p>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-2 p-0">
              <div
                className="desc_sec minimalPadding m-0 align-items-start justify-content-center"
                style={{ height: "2.174vw" }}
              >
                <p className="desc_text">
                  <b>Affiliates</b>
                </p>
              </div>
            </div>
            <div className="col-10 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">
                  Companies related by common ownership or control. They can be
                  financial and nonfinancial companies.
                </p>
                <div className="col-12 p-0 minimalPadding">
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Boatzon Holdings, L.L.C. and all institutions in the
                        Boatzon family of companies; Boatzon Insurance L.L.C,
                        Boatzon Finance L.L.C
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-2 p-0">
              <div
                className="desc_sec minimalPadding m-0 align-items-start justify-content-center"
                style={{ height: "2.174vw" }}
              >
                <p className="desc_text">
                  <b>Nonaffiliates</b>
                </p>
              </div>
            </div>
            <div className="col-10 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text">
                  Companies not related by common ownership or control. They can
                  be financial and nonfinancial companies.
                </p>
                <div className="col-12 p-0 minimalPadding">
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Nonaffiliates we share with could include insurance
                        companies, financial banks, mortgage companies, warranty
                        companies, credit card companies and direct marketing
                        companies.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-2 p-0">
              <div
                className="desc_sec minimalPadding m-0 align-items-start justify-content-center"
                style={{ height: "2.174vw" }}
              >
                <p className="desc_text w-100">
                  <b>Joint marketing</b>
                </p>
              </div>
            </div>
            <div className="col-10 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-start justify-content-center">
                <p className="desc_text text-left">
                  A formal agreement between nonaffiliated financial companies
                  that together market financial products or services to you.
                </p>
                <div className="col-12 p-0 minimalPadding">
                  <div className="row m-0 d-flex flex-row align-items-center">
                    <div className="col-1 point pl-0">
                      <span className="bullet-point"></span>
                    </div>
                    <div className="col-auto p-0">
                      <p className="desc_text pl-0">
                        Our Joint marketing partners include finance companies
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row m-0 mt-1 d-flex align-items-center">
            <div className="col-12 p-0">
              <div className="label_sec m-0 align-items-center">
                <p className="label">Other important information</p>
              </div>
            </div>
          </div>
          <div className="row m-0 justify-content-center">
            <div className="col-12 p-0">
              <div className="desc_sec minimalPadding m-0 align-items-center justify-content-center">
                <p className="desc_text">
                  I/WE ACKNOWLEDGE THAT I/WE HAVE RECEIVED A COPY OF THIS
                  NOTICE.
                </p>
                <div className="col-12 p-0">
                  <div className="row m-0 align-items-center justify-content-start">
                    <p className="desc_text text-left pt-1">
                      <b>California residents:</b> We will not share personal
                      information with nonaffiliates to market to you, except
                      with your express consent. We will not share your personal
                      and financial information with affiliates or joint
                      marketing companies, if you instruct us not to do so. To
                      limit our sharing with affiliates and joint marketing
                      companies, please send an email to 
                      <u>
                        <a
                          className="desc_text"
                          href="mailto:Privacy@Boatzon.com"
                        >
                          Privacy@Boatzon.com
                        </a>
                      </u>
                       and include the words “Opt Out” in the subject line and
                      body of your email, or call us at (833) BOATZON.
                    </p>
                    <p className="desc_text text-left pt-1">
                      <b>Vermont residents:</b> We will not share personal
                      information with nonaffiliates to market to you, or share
                      consumer report information about you with affiliates or
                      joint marketing companies, except with your express
                      consent.
                    </p>
                  </div>
                </div>
                <div className="col-12 p-0">
                  <div className="row m-0 align-items-center">
                    <div className="col-4 pl-0 text-center">
                      <div className="borderTop">
                        <p className="desc_text">Print Customer Name</p>
                      </div>
                    </div>
                    <div className="col-4 pl-0 text-center">
                      <div className="borderTop">
                        <p className="desc_text">Customer Signature</p>
                      </div>
                    </div>
                    <div className="col-4 pl-0 text-center">
                      <div className="borderTop">
                        <p className="desc_text">Date</p>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0 align-items-center">
                    <div className="col-4 pl-0 text-center">
                      <div className="borderTop">
                        <p className="desc_text">Print Customer Name</p>
                      </div>
                    </div>
                    <div className="col-4 pl-0 text-center">
                      <div className="borderTop">
                        <p className="desc_text">Customer Signature</p>
                      </div>
                    </div>
                    <div className="col-4 pl-0 text-center">
                      <div className="borderTop">
                        <p className="desc_text">Date</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: inherit !important;
            }
            :global(.MuiDialog-paper) {
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            .mx-auto {
              width: 90vw;
              position: relative;
              background: ${WHITE_COLOR} !important;
            }
            .closeIcon {
              position: absolute;
              top: 0.732vw;
              right: 0.732vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              font-size: 1.317vw;
              color: ${WHITE_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.219vw;
              text-transform: uppercase;
              background: ${THEME_COLOR};
              align-items: center;
              display: flex;
              justify-content: center;
              padding: 0.146vw 0;
            }
            .caption {
              font-size: 1.171vw;
              color: ${THEME_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.219vw;
              text-transform: uppercase;
              border: 0.0732vw solid ${THEME_COLOR};
              border-bottom-width: 0.219vw !important;
              align-items: center;
              display: flex;
              justify-content: flex-start;
              padding: 0.124vw 0.732vw;
            }
            .label_sec {
              align-items: baseline;
              display: flex;
              justify-content: flex-start;
              padding: 0 0.585vw;
              background: ${THEME_COLOR_Opacity2};
              margin-bottom: 0.219vw;
            }
            .label {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
            }
            .label::first-letter {
              text-transform: capitalize;
            }
            .desc_sec {
              margin-bottom: 0.219vw;
              border: 0.0732vw solid ${THEME_COLOR};
              align-items: flex-start;
              display: flex;
              flex-direction: column;
              padding: 0.0732vw 0.732vw;
            }
            .desc_text {
              font-size: 0.658vw;
              color: ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
              line-height: 1.1;
            }
            .desc_text::first-letter {
              text-transform: capitalize;
            }
            .point {
              max-width: 0.732vw;
            }
            .bullet-point {
              display: block;
              margin: 0;
              width: 0.366vw;
              height: 0.366vw;
              border-radius: 50%;
              background: ${THEME_COLOR};
            }
            .minimalPadding {
              padding-top: 0.146vw !important;
              padding-bottom: 0.146vw !important;
            }
            .maxWidth {
              max-width: 75%;
              margin: 0 auto 0 0;
            }
            .borderTop {
              margin-top: 1.683vw;
              padding: 0.366vw 0;
              border-top: 0.219vw solid ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default PrivacyPolicyTerms;
