import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Get_Approval_Icon,
  FONTGREY_COLOR_Dark,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
  Long_Arrow_Left_White_Icon,
  WHITE_COLOR,
  GREY_VARIANT_10,
  Completed_White_Icon,
} from "../../lib/config";
import CheckBox from "../../components/input-box/check-box";
import ButtonComp from "../../components/button/button";
import CircularProgressButton from "../../components/button-loader/button-loader";
import Model from "../../components/model/model";
import EleSignTerms from "./ele-sign-terms";
import SsnModel from "./ssn-model";
import PrivacyPolicyTerms from "./privacy-policy-terms";

class getPreApproved extends Component {
  state = {
    eleSignModel: false,
    privacyModel: false,
  };

  handleEleSignModel = () => {
    this.setState({
      eleSignModel: !this.state.eleSignModel,
    });
  };

  handlePrivacyModel = () => {
    this.setState({
      privacyModel: !this.state.privacyModel,
    });
  };

  render() {
    const {
      handleOnchangeCheckbox,
      apiloading,
      currentScreen,
      handleBack,
      handleApprovedPurchasePage,
      handleOnchangeInput,
      handleSSNModel,
      ssnModel,
      inputpayload,
      SSNFormValid,
      handleGetCreditWithSSN,
      ssnapiloading,
    } = this.props;
    const { loanTermsApproved } = { ...this.props.inputpayload };
    return (
      <Wrapper>
        {!currentScreen ? (
          <div className="py-3 getPreApprovedPage">
            <div className="col-12 termsSec">
              <div className="row m-0">
                <div className="col-9 mx-auto">
                  <div className="text-center">
                    <img
                      src={Get_Approval_Icon}
                      className="getApprovalIcon"
                    ></img>
                    <h5 className="heading">Get Pre-Qualified</h5>
                    <h6 className="caption">
                      Get Pre-Qualified for this purchase in seconds. This
                      pre-qualify does not impact your credit score.
                    </h6>
                  </div>
                  <div className="py-3">
                    <CheckBox
                      getLoanApproval={true}
                      checked={loanTermsApproved}
                      plaincheckbox={true}
                      onChange={handleOnchangeCheckbox("loanTermsApproved")}
                      value="loanTermsApproved"
                      label="Yes, I Agree"
                    />
                    <div className="row m-0 d-flex justify-content-start">
                      <div className="col-1 p-0 point">
                        <span className="bullet-point"></span>
                      </div>
                      <div className="col-11 p-0 ">
                        <p className="terms">
                          I have read and agree to the{" "}
                          <span
                            className="links"
                            onClick={this.handleEleSignModel}
                          >
                            E-SIGN Consent
                          </span>{" "}
                          that enables all transactions and disclosure delivery
                          to occur electronically.
                        </p>
                      </div>
                    </div>
                    <div className="row m-0 d-flex justify-content-start">
                      <div className="col-1 p-0 point">
                        <span className="bullet-point"></span>
                      </div>
                      <div className="col-11 p-0">
                        <p className="terms">
                          I have received and read{" "}
                          <span
                            className="links"
                            onClick={this.handlePrivacyModel}
                          >
                            Boatzon's Financial Privacy Policy.
                          </span>
                        </p>
                      </div>
                    </div>
                    <div className="row m-0 d-flex justify-content-start">
                      <div className="col-1 p-0 point">
                        <span className="bullet-point"></span>
                      </div>
                      <div className="col-11 p-0">
                        <p className="terms">
                          By clicking on the <b>I Agree</b> checkbox and typing
                          in your name, you are confirming that you have read
                          and understand the{" "}
                          <span
                            className="links"
                            onClick={this.handlePrivacyModel}
                          >
                            Privacy Policy
                          </span>{" "}
                          and{" "}
                          <span
                            className="links"
                            onClick={this.handleEleSignModel}
                          >
                            Electronic Signature Disclosure
                          </span>
                          , and you are authorizing <b>Boatzon</b> under all
                          applicable federal and state laws, including the Fair
                          Credit Reporting Act, to obtain information from your
                          personal credit profile. Also, you are confirming you
                          understand that any pre-approval is subject to review
                          and acceptance of credit information.
                        </p>
                      </div>
                    </div>
                    <div className="row m-0 d-flex justify-content-start reportSec">
                      <div className="col-1 p-0 point"></div>
                      <div className="col-11 p-0">
                        <p className="reportMsg">
                          Consumer Report: By clicking "GET MY TERMS", I give
                          Boatzon written consent to obtain consumer reports
                          from one or more consumer reporting agencies to show
                          me credit options I prequalify for when financing with
                          Boatzon. Retrieving my pre-qualification credit terms
                          generates a soft credit inquiry, which is visible only
                          to me and does not affect my credit score. This
                          prequalification is a soft pull of your credit, and
                          does not impact your credit score.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 p-0 pt-3">
              <div className="d-flex align-items-center justify-content-between">
                <div className="back_btn">
                  <ButtonComp onClick={handleBack}>
                    <img
                      src={Long_Arrow_Left_White_Icon}
                      className="backArrow"
                    ></img>{" "}
                    Back
                  </ButtonComp>
                </div>
                <div className={loanTermsApproved ? "next_btn" : "back_btn"}>
                  <CircularProgressButton
                    buttonText={
                      <span>
                        <img
                          src={Completed_White_Icon}
                          className="nextArrow"
                        ></img>{" "}
                        Get Pre-Qualified
                      </span>
                    }
                    disabled={!loanTermsApproved}
                    onClick={handleApprovedPurchasePage}
                    loading={apiloading}
                  />
                </div>
              </div>
            </div>
          </div>
        ) : (
          currentScreen
        )}

        {/* EleSignTerms Model */}
        <Model open={this.state.eleSignModel} onClose={this.handleEleSignModel}>
          <EleSignTerms onClose={this.handleEleSignModel} />
        </Model>

        {/* Privacy Policy Model */}
        <Model open={this.state.privacyModel} onClose={this.handlePrivacyModel}>
          <PrivacyPolicyTerms onClose={this.handlePrivacyModel} />
        </Model>

        {/* SSN Model */}
        <Model open={ssnModel} onClose={handleSSNModel}>
          <SsnModel
            handleOnchangeInput={handleOnchangeInput}
            onClose={handleSSNModel}
            inputpayload={inputpayload}
            ssnapiloading={ssnapiloading}
            SSNFormValid={SSNFormValid}
            handleGetCreditWithSSN={handleGetCreditWithSSN}
          />
        </Model>
        <style jsx>
          {`
            .getApprovalIcon {
              width: 4.392vw;
              object-fit: contain;
              margin-top: 1.098vw;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.366vw;
              padding: 0.732vw 0 0 0;
            }

            .links {
              cursor: pointer;
            }

            .caption {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              line-height: 1.6;
              letter-spacing: 0.0219vw !important;
              max-width: 80%;
              margin: 0 auto 0.585vw auto;
            }
            .point {
              max-width: 1.464vw;
            }
            .terms {
              font-size: 0.658vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0.219vw;
              line-height: 1.2;
            }
            .terms span {
              cursor: pointer;
              color: ${GREEN_COLOR};
              font-family: "Open Sans-SemiBold" !important;
            }
            .termsSec {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .reportSec {
              margin-top: 1.024vw !important;
            }
            .reportMsg {
              font-size: 0.658vw;
              font-style: italic;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0.219vw;
              line-height: 1.2;
            }
            .bullet-point {
              display: block;
              margin: 0.366vw 0.732vw 0 0.366vw;
              width: 0.366vw;
              height: 0.366vw;
              border-radius: 50%;
              background: ${GREEN_COLOR};
            }
            .nextArrow {
              width: 0.878vw;
              left: 0.732vw;
              top: 50%;
              transform: translate(0, -50%);
              position: absolute;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default getPreApproved;
