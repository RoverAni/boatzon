// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import {
  BG_LightGREY_COLOR,
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  GREY_VARIANT_10,
  GREY_VARIANT_1,
  WHITE_COLOR,
  Purchase_Hamburger_Icon,
  Cart_Dark_Grey_Icon,
  Radio_Button_Active,
  Radio_Button_Inactive,
  Chevron_Right,
  Selected_Checkbox_Green_Icon,
  Profile_Logo,
  Play_Video_Icon,
  BuyNow_Video,
} from "../../lib/config";
import PersonalInfoPage from "./personal-info-page";
import BoatInfoPage from "./boat-info-page";
import LoansPage from "./loans-page";
import GetPreApproved from "./get-pre-approved";
import ApprovedPurchase from "./approved-purchase";
import MyInsurance from "./my-insurance";
import ViewQuote from "./view-quote";
import TrailAppointment from "./trail-appointment";
import ConfirmingPage from "./confirming-page";
import { getStates } from "country-state-picker";
import {
  MOBILE,
  COUNTRY_CODE,
} from "../../lib/input-control-data/input-definitions";
import { requiredValidator } from "../../lib/validation/validation";
import moment from "moment";
import currencies from "../../translations/currency.json";
import {
  getLocationfromLatLng,
  getLocationfromStateZipcode,
} from "../../lib/location/geo-location";
import {
  storeBuyingInfo,
  postCreditScore,
  getStoreBuyingInfo,
} from "../../services/buy-with-boatzon";
import Snackbar from "../../components/snackbar";
import { getCookie } from "../../lib/session";
import { sendAVCallRequest } from "../../services/calling";
import VideoModel from "../services-page/video-model";
import Model from "../../components/model/model";
import { handlePreferredCountries } from "../../lib/phone-num/phone-num";
import {
  formatDate,
  timestampTomomentizedDate,
} from "../../lib/date-operation/date-operation";
import {
  handleFirstName,
  handleLastName,
  handleMiddleName,
} from "../../lib/name-handlers/handlers";
import { saveEditProfileData } from "../../services/userProfile";
import { getAppointmentData } from "../../services/my-appointment";

class BuyWithBoatzonPage extends Component {
  state = {
    activeLink: "personal-info",
    inputpayload: {
      loanTermsApproved: false,
      meeting_type: "In-Person",
    },
    isPersonalInfoFormValid: false,
    isBoatInfoFormValid: false,
    isLoanFormValid: false,
    loanTermsApproved: false,
    isInsuranceFormValid: false,
    isQuoteFormValid: false,
    isAppointmentFormValid: false,
    interested_parties_list: false,
    countryShortName: "us",
    apiloading: false,
    ssnapiloading: false,
    preferredCountries: ["us"],
    timeSelectedForCall: "",
    selectedDate: moment(),
    openVideoModel: false,
    ssnModel: false,
    SSNFormValid: false,
    todaysBookedTimes: [],
  };

  componentDidMount = () => {
    this.handleOnLoad();
  };

  handleOnLoad = async () => {
    const {
      fullName,
      phoneNumber,
      countryCode,
      email,
      latitude,
      longitude,
      location,
      creditScore,
    } = {
      ...this.props.userProfileData,
    };
    const DateOfBirth = this.props.userProfileData.dob;
    const {
      mainUrl,
      productName,
      memberProfilePicUrl,
      memberFullName,
      currency,
      price,
      condition,
      subCategory,
      manufactor,
      manufactorId,
      year,
      length,
      engineType1,
      engineMake1,
      engineYear1,
      engineCount,
      engineHorsePower1,
      engineFuelType1,
      boatModel,
      place,
    } = {
      ...this.props.postBoatDetailsData,
    };
    const {
      productImage,
      ProductName,
      sellerProfilePicUrl,
      sellerName,
      productCurrency,
      productPrice,
      first_name,
      last_name,
      Email,
      dob,
      mobile,
      address,
      country,
      city,
      stateName,
      zipCode,
      maritalStatus,
      boat_type,
      manufacturer_type,
      boat_make,
      boat_year,
      hull_idNum,
      boat_usage,
      boat_speed,
      engine_type,
      engine_make,
      engine_year,
      num_of_engines,
      hourse_power,
      fuel_type,
      residence_type,
      monthly_rent,
      rent_year,
      rent_month,
      employment_status,
      employer_name,
      employer_year,
      employer_month,
      gross_income,
      applicant,
      loan_type,
      terms_year,
      purchase_price,
      down_payment,
      loan_amount,
      tradeIn_allowance,
      trade_payoff,
      purchase_date,
      insurance_purchase_price,
      years_owned,
      replacement_cost,
      hull_deduction,
      liability_limit,
      medical,
      mooring_locations,
      mooring_types,
      mooring_zip_code,
      fire_fighting_system,
      burglary_detection,
      trailer_avaiable,
      years_boat_ownership,
      years_boat_experience,
      boat_courses,
      boating_loss,
      premium_up_front,
      loanTermsApproved,
      CustomerCreditScore,
      CustomerToken,
      CustomerTransid,
    } = {
      ...this.props.buyBoatDetailsData,
    };
    const dialCode =
      this.props.buyBoatDetailsData &&
      this.props.buyBoatDetailsData.countryCode;
    const PhoneNumber = phoneNumber.startsWith("+")
      ? phoneNumber.replace(countryCode, "")
      : phoneNumber;
    const boatCondition =
      this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.condition;
    const Length =
      this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.length;

    let tempPreferredCountries =
      this.props.buyBoatDetailsData && this.props.buyBoatDetailsData.countryCode
        ? handlePreferredCountries(
            this.props.buyBoatDetailsData.countryCode,
            mobile
          )
        : countryCode != "undefined"
        ? handlePreferredCountries(countryCode, PhoneNumber)
        : this.state.preferredCountries;

    let userLocation;
    try {
      if (latitude && longitude) {
        userLocation = await getLocationfromLatLng(latitude, longitude);
      }
      const StateList = userLocation
        ? getStates(userLocation.countryShortName.toLowerCase()).map(
            (data) => ({
              value: data,
              label: data,
            })
          )
        : "";
      let countryShortName = userLocation
        ? userLocation.countryShortName.toLowerCase()
        : "us";

      const StateName =
        StateList &&
        StateList.filter((data) => {
          return data.value.includes(userLocation.stateName);
        });

      let tempPayload = { ...this.state.inputpayload };
      tempPayload.userId = this.props.userId;
      tempPayload.productId = this.props.productId;
      tempPayload.productImage = productImage
        ? productImage
        : mainUrl
        ? mainUrl
        : "";
      tempPayload.ProductName = ProductName
        ? ProductName
        : productName
        ? productName
        : "";
      tempPayload.sellerProfilePicUrl = sellerProfilePicUrl
        ? sellerProfilePicUrl
        : memberProfilePicUrl
        ? memberProfilePicUrl
        : Profile_Logo;
      tempPayload.sellerName = sellerName
        ? sellerName
        : memberFullName
        ? memberFullName
        : "";
      tempPayload.productCurrency = productCurrency
        ? productCurrency
        : currency
        ? currency
        : "";
      tempPayload.productPrice = productPrice
        ? productPrice
        : price
        ? price
        : "";

      let fullNameArr = fullName.split(" ");
      tempPayload.first_name = first_name
        ? first_name
        : `${handleFirstName(fullNameArr)}${handleMiddleName(fullNameArr)}`;
      tempPayload.last_name = last_name
        ? last_name
        : handleLastName(fullNameArr);
      tempPayload.mobile = mobile ? mobile : PhoneNumber;
      tempPayload.countryCode = dialCode
        ? dialCode
        : countryCode != "undefined"
        ? countryCode
        : "+91";
      tempPayload.Email = Email ? Email : email ? email : "";
      tempPayload.dob = dob
        ? moment(dob)
        : DateOfBirth
        ? timestampTomomentizedDate(DateOfBirth)
        : "";
      tempPayload.address = address
        ? address
        : userLocation && userLocation.address
        ? userLocation.address
        : location
        ? location
        : "";
      tempPayload.country = country
        ? country
        : userLocation && userLocation.country
        ? { value: userLocation.countryShortName, label: userLocation.country }
        : {};
      tempPayload.city = city ? city : userLocation && userLocation.city;
      tempPayload.zipCode = zipCode
        ? zipCode
        : userLocation && userLocation.zipCode;
      tempPayload.stateName = stateName
        ? stateName
        : StateName
        ? StateName[0]
        : "";
      tempPayload.maritalStatus = maritalStatus ? maritalStatus : "";
      tempPayload.condition = boatCondition
        ? boatCondition
        : condition
        ? condition
        : "";
      tempPayload.boat_type = boat_type
        ? boat_type
        : subCategory
        ? { value: subCategory, label: subCategory }
        : {};
      tempPayload.manufacturer_type = manufacturer_type
        ? manufacturer_type
        : manufactor
        ? { value: manufactorId, label: manufactor }
        : {};
      tempPayload.boat_make = boatModel
        ? boatModel
        : boat_make
        ? boat_make
        : "";
      tempPayload.boat_year = boat_year
        ? boat_year
        : year
        ? { value: year, label: year }
        : {};
      tempPayload.hull_idNum = hull_idNum ? hull_idNum : "";
      tempPayload.length = Length ? Length : length ? length : "";
      tempPayload.boat_usage = boat_usage ? boat_usage : {};
      tempPayload.boat_speed = boat_speed ? boat_speed : "";
      tempPayload.engine_type = engine_type
        ? engine_type
        : engineType1
        ? { value: engineType1, label: engineType1 }
        : {};
      tempPayload.engine_make = engine_make
        ? engine_make
        : engineMake1
        ? { value: engineMake1, label: engineMake1 }
        : {};
      tempPayload.engine_year = engine_year
        ? engine_year
        : engineYear1
        ? { value: engineYear1, label: engineYear1 }
        : {};
      tempPayload.num_of_engines = num_of_engines
        ? num_of_engines
        : engineCount
        ? { value: engineCount, label: engineCount }
        : {};
      tempPayload.hourse_power = hourse_power
        ? hourse_power
        : engineHorsePower1
        ? engineHorsePower1
        : "";
      tempPayload.fuel_type = fuel_type
        ? fuel_type
        : engineFuelType1
        ? { value: engineFuelType1, label: engineFuelType1 }
        : {};

      tempPayload.residence_type = residence_type ? residence_type : {};
      tempPayload.monthly_rent = monthly_rent ? monthly_rent : "";
      tempPayload.rent_year = rent_year ? rent_year : {};
      tempPayload.rent_month = rent_month ? rent_month : {};
      tempPayload.employment_status = employment_status
        ? employment_status
        : {};
      tempPayload.employer_name = employer_name ? employer_name : "";
      tempPayload.employer_year = employer_year ? employer_year : {};
      tempPayload.employer_month = employer_month ? employer_month : {};
      tempPayload.gross_income = gross_income ? gross_income : "";
      tempPayload.applicant = applicant ? applicant : "";
      tempPayload.loan_type = loan_type
        ? loan_type
        : { value: "Purchase", label: "Purchase" };
      tempPayload.terms_year = terms_year ? terms_year : {};
      tempPayload.purchase_price = purchase_price
        ? purchase_price
        : price
        ? price
        : "";
      tempPayload.down_payment = down_payment ? down_payment : "";
      tempPayload.loan_amount = loan_amount ? loan_amount : price ? price : "";
      tempPayload.tradeIn_allowance = tradeIn_allowance
        ? tradeIn_allowance
        : "";
      tempPayload.trade_payoff = trade_payoff ? trade_payoff : "";

      tempPayload.purchase_date = purchase_date
        ? moment(purchase_date)
        : moment();
      tempPayload.insurance_purchase_price = insurance_purchase_price
        ? insurance_purchase_price
        : "";
      tempPayload.years_owned = years_owned ? years_owned : {};
      tempPayload.replacement_cost = replacement_cost
        ? replacement_cost
        : price
        ? price
        : "";
      tempPayload.hull_deduction = hull_deduction ? hull_deduction : {};
      tempPayload.liability_limit = liability_limit ? liability_limit : {};
      tempPayload.medical = medical ? medical : {};
      tempPayload.mooring_locations = mooring_locations
        ? mooring_locations
        : {};
      tempPayload.mooring_types = mooring_types ? mooring_types : {};
      tempPayload.mooring_zip_code = mooring_zip_code
        ? mooring_zip_code
        : userLocation && userLocation.zipCode;
      tempPayload.fire_fighting_system = fire_fighting_system
        ? fire_fighting_system
        : {};
      tempPayload.burglary_detection = burglary_detection
        ? burglary_detection
        : {};
      tempPayload.trailer_avaiable = trailer_avaiable ? trailer_avaiable : {};
      tempPayload.years_boat_ownership = years_boat_ownership
        ? years_boat_ownership
        : "";
      tempPayload.years_boat_experience = years_boat_experience
        ? years_boat_experience
        : "";
      tempPayload.boat_courses = boat_courses ? boat_courses : {};
      tempPayload.boating_loss = boating_loss ? boating_loss : {};
      tempPayload.premium_up_front = premium_up_front ? premium_up_front : {};
      tempPayload.loanTermsApproved = loanTermsApproved
        ? loanTermsApproved
        : false;
      tempPayload.loanTermsApproved = loanTermsApproved
        ? loanTermsApproved
        : false;
      tempPayload.CustomerCreditScore = CustomerCreditScore
        ? CustomerCreditScore
        : creditScore
        ? parseInt(creditScore, 10)
        : 0;
      tempPayload.CustomerToken = CustomerToken ? CustomerToken : "";
      tempPayload.CustomerTransid = CustomerTransid ? CustomerTransid : "";
      tempPayload.boatAddress = place ? place : "";
      this.setState(
        {
          inputpayload: { ...tempPayload },
          EmailValid: tempPayload.Email ? 1 : 2,
          isPhoneValid: phoneNumber ? true : false,
          preferredCountries: [...tempPreferredCountries],
          countryShortName: countryShortName,
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } catch (err) {
      console.log("err---->", err);
    }
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let validate = requiredValidator(inputControl);
    this.setState({ [`${inputControl.name}Valid`]: validate });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[[inputControl.name]] = inputControl.value;
    if (inputControl.name === "purchase_price") {
      tempPayload.loan_amount =
        inputControl.value -
        (tempPayload.down_payment !== "" && tempPayload.down_payment
          ? tempPayload.down_payment
          : 0);
      tempPayload.insurance_purchase_price =
        inputControl.value -
        (tempPayload.down_payment !== "" && tempPayload.down_payment
          ? tempPayload.down_payment
          : 0);
      tempPayload.replacement_cost = inputControl.value;
      this.setState({
        loan_amount: tempPayload.loan_amount,
        insurance_purchase_price: tempPayload.insurance_purchase_price,
      });
    } else if (inputControl.name === "down_payment") {
      tempPayload.loan_amount = tempPayload.purchase_price - inputControl.value;
      tempPayload.insurance_purchase_price =
        tempPayload.purchase_price - inputControl.value;
      this.setState({
        loan_amount: tempPayload.loan_amount,
        insurance_purchase_price: tempPayload.insurance_purchase_price,
      });
    }
    this.setState(
      {
        inputpayload: { ...tempPayload },
        [inputControl.name]: inputControl.value,
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleSnackbar = (response) => {
    switch (response.code || response.status) {
      case 200:
        return "success";
        break;
      case 204 || 422:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
      default:
        return "error";
        break;
    }
  };

  // Function for form validation
  checkIfFormValid = () => {
    const { inputpayload, isPhoneValid, EmailValid, SSNValid } = this.state;
    let isPersonalInfoFormValid = false;
    isPersonalInfoFormValid =
      inputpayload.first_name &&
      inputpayload.last_name &&
      isPhoneValid &&
      EmailValid == 1 &&
      inputpayload.maritalStatus &&
      inputpayload.dob &&
      inputpayload.address &&
      inputpayload.city &&
      inputpayload.stateName &&
      inputpayload.zipCode
        ? true
        : false;

    let SSNFormValid = false;
    SSNFormValid = SSNValid == 1 ? true : false;

    let isBoatInfoFormValid = false;
    isBoatInfoFormValid =
      inputpayload.condition &&
      inputpayload.boat_type &&
      inputpayload.manufacturer_type &&
      inputpayload.boat_make &&
      inputpayload.boat_year &&
      inputpayload.length &&
      inputpayload.boat_usage &&
      inputpayload.boat_speed &&
      inputpayload.engine_type &&
      inputpayload.engine_make &&
      inputpayload.engine_year &&
      inputpayload.num_of_engines &&
      inputpayload.hourse_power &&
      inputpayload.fuel_type
        ? true
        : false;

    let isLoanFormValid = false;
    isLoanFormValid =
      inputpayload.residence_type &&
      inputpayload.monthly_rent &&
      inputpayload.rent_year &&
      inputpayload.rent_month &&
      inputpayload.employment_status &&
      inputpayload.employer_name &&
      inputpayload.employer_year &&
      inputpayload.employer_month &&
      inputpayload.gross_income &&
      inputpayload.applicant &&
      inputpayload.loan_type &&
      inputpayload.terms_year &&
      inputpayload.purchase_price &&
      inputpayload.down_payment &&
      inputpayload.loan_amount
        ? true
        : false;

    let isInsuranceFormValid = false;
    isInsuranceFormValid =
      inputpayload.purchase_date &&
      inputpayload.insurance_purchase_price &&
      inputpayload.years_owned != {} &&
      inputpayload.replacement_cost &&
      inputpayload.hull_deduction != {} &&
      inputpayload.liability_limit != {} &&
      inputpayload.medical != {} &&
      inputpayload.mooring_locations != {} &&
      inputpayload.mooring_types != {} &&
      inputpayload.mooring_zip_code &&
      inputpayload.years_boat_ownership &&
      inputpayload.years_boat_experience &&
      inputpayload.fire_fighting_system &&
      inputpayload.burglary_detection != {} &&
      inputpayload.trailer_avaiable != {} &&
      inputpayload.boat_courses != {} &&
      inputpayload.boating_loss != {} &&
      inputpayload.premium_up_front != {}
        ? true
        : false;

    let isAppointmentFormValid = false;
    isAppointmentFormValid =
      this.state.timeSelectedForCall !== "" && this.state.selectedDate
        ? true
        : false;

    this.setState({
      isPersonalInfoFormValid: isPersonalInfoFormValid,
      isBoatInfoFormValid: isBoatInfoFormValid,
      isLoanFormValid: isLoanFormValid,
      isInsuranceFormValid: isInsuranceFormValid,
      isAppointmentFormValid: isAppointmentFormValid,
      SSNFormValid: SSNFormValid,
    });
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    let temppayload = { ...this.state.inputpayload };
    temppayload[[name]] = {
      value: inputControl.value,
      label: inputControl.label,
    };
    if (name == "country") {
      this.setState({
        countryShortName: event.value,
      });
    }
    this.setState(
      {
        inputpayload: { ...temppayload },
        [name]: {
          value: inputControl.value,
          label: inputControl.value,
        },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  handleRadioInputChange = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[[inputControl.name]] = inputControl.value;
    this.setState(
      {
        [inputControl.name]: inputControl.value,
        inputpayload: { ...tempPayload },
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  disabledDate = (current) => {
    // Can not select days after today and today
    return current && current >= moment().subtract(18, "year");
  };

  handleDOBChange = (date, dateString) => {
    if (date) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`dob`] = moment(date);
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`dob`] = "";
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  handlePurchaseDateChange = (date, dateString) => {
    if (date) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`purchase_date`] = moment(date);
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    } else {
      let temppayload = { ...this.state.inputpayload };
      temppayload[`purchase_date`] = "";
      this.setState(
        {
          inputpayload: { ...temppayload },
        },
        () => {
          this.checkIfFormValid();
        }
      );
    }
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    console.log("onSelectFlag", num, country.dialCode, fullNum, status);
    this.handleOnchangePhone(status, num, country);
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log("valuenumber", dialInfo, valuenumber);
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        preferredCountries: [`${dialInfo.iso2}`],
        isPhoneValid: status,
      },
      () => {
        console.log("valuenumber", this.state.isPhoneValid);
        this.checkIfFormValid();
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        tempPayload[`preferredCountries`] = this.state.preferredCountries;
        this.setState({
          inputpayload: { ...tempPayload },
          [`${MOBILE}`]: this.state.UserNumber,
          [`${COUNTRY_CODE}`]: "+" + this.state.cCode,
        });
      }
    );
  };

  // Function to Store form values
  handleStoreInfoAPICall = () => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          apiloading: true,
        },
        () => {
          storeBuyingInfo(this.state.inputpayload)
            .then((res) => {
              this.setState({
                usermessage: res.data.message,
                variant: this.handleSnackbar(res.data),
                open: true,
                vertical: "bottom",
                horizontal: "left",
                apiloading: res.data.code == 200 ? false : true,
              });
              let apiResponse = res.data.code == 200 ? true : false;
              return resolve(apiResponse);
            })
            .catch((err) => {
              reject(err);
            });
        }
      );
    });
  };

  selectTimeForCall = (time) => {
    let updatedTime = moment
      .utc(this.state.selectedDate)
      .set({
        h: time.hour,
        m: 0,
        s: 0,
      })
      .utc();
    this.setState(
      {
        timeSelectedForCall: time.ms,
        seletedTimestamp: formatDate(updatedTime, "x"),
      },
      () => {
        this.checkIfFormValid();
      }
    );
  };

  // Function for checkbox inputpayload
  handleOnchangeCheckbox = (name) => (event) => {
    this.setState({ [name]: event.target.checked }, () => {
      let tempInputPayload = { ...this.state.inputpayload };
      tempInputPayload[name] = this.state[name];
      this.setState({
        inputpayload: { ...tempInputPayload },
      });
    });
  };

  handleMeetingType = (meetTye) => {
    let tempPayload = { ...this.state.inputpayload };
    tempPayload.meeting_type = meetTye;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  getMeetingsData = () => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          apiResponse: true,
          apiloading: true,
        },
        () => {
          let { memberMqttId } = this.props && this.props.postBoatDetailsData;
          let obj = {
            userId: memberMqttId,
            type: 1,
            startDate: 0,
            endDate: 0,
          };
          getAppointmentData(obj)
            .then((res) => {
              this.setState({
                bookedMeetings: res && res.status == 200 ? res.data.data : [],
                apiloading: res.status == 200 || 204 ? false : true,
              });
              this.handleMeetingTimeArr(moment());
              let apiResponse = res.status == 200 || 204 ? true : false;
              return resolve(apiResponse);
            })
            .catch((err) => {
              reject(err);
            });
        }
      );
    });
  };

  handleMeetingTimeArr = (selectedDate) => {
    const { bookedMeetings } = this.state;
    let SelectedDate = moment(selectedDate).format("L");
    bookedMeetings &&
      bookedMeetings.map((data) => {
        let meetingStartDate = moment(data.startDate).format("L");
        if (meetingStartDate == SelectedDate) {
          let bookedTimes = [...this.state.todaysBookedTimes];
          let time = moment.utc(data.startDate).format("HH:mm:ss");
          bookedTimes.push(time);
          this.setState({
            todaysBookedTimes: [...bookedTimes],
          });
        }
      });
  };

  scheduleVideoMeeting = () => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          apiResponse: true,
          Apiloading: true,
        },
        () => {
          let _meetingDate = this.state.seletedTimestamp;
          let {
            postId,
            memberMqttId,
            longitude,
            latitude,
            place,
            productName,
          } = this.props && this.props.postBoatDetailsData;
          const { meeting_type } = this.state.inputpayload;
          let obj = {
            fromId: getCookie("mqttId"),
            toId: memberMqttId,
            secretId: postId.toString(),
            docId: "xxx",
            meetingDate: _meetingDate.toString(),
            meetingType: meeting_type == "In-Person" ? 2 : 1,
            title: productName,
            description: "In-person Meeting to check the boat",
            location: place,
            latitude: latitude.toString(),
            longitude: longitude.toString(),
          };
          sendAVCallRequest(obj)
            .then((res) => {
              console.log("fnesjd", res);
              this.setState({
                usermessage: res.data.message,
                variant: this.handleSnackbar(res),
                open: true,
                vertical: "bottom",
                horizontal: "left",
                Apiloading: res.data.code || res.status == 200 ? false : true,
              });
              let apiResponse =
                res.data.code || res.status == 200 ? true : false;
              return resolve(apiResponse);
            })
            .catch((err) => {
              reject(err);
            });
        }
      );
    });
  };

  onPanelChange = (value, mode) => {
    this.setState({
      newDate:
        moment(
          `${value.year()}-${value.month()}-${value.date()} 00:00:00`
        ).unix() * 1000,
    });
  };

  handleOnDateChange = (value) => {
    this.setState(
      {
        selectedDate: value,
        newDate:
          moment(
            `${value.year()}-${value.month()}-${value.date()} 00:00:00`
          ).unix() * 1000,
        todaysBookedTimes: [],
        timeSelectedForCall: "",
      },
      () => {
        this.handleMeetingTimeArr(this.state.selectedDate);
        this.checkIfFormValid();
      }
    );
  };

  handlePrevMonth = (value) => {
    const newValue = value.clone();
    newValue.month(parseInt(value.month() - 1, 10));
    this.setState(
      {
        selectedDate: moment(newValue).endOf("month"),
        getMeetings: true,
        todaysBookedTimes: [],
        timeSelectedForCall: "",
      },
      () => {
        this.handleMeetingTimeArr(this.state.selectedDate);
        this.checkIfFormValid();
      }
    );
  };

  handleNextMonth = (value) => {
    const newValue = value.clone();
    newValue.month(parseInt(value.month() + 1, 10));
    this.setState(
      {
        selectedDate: moment(newValue).startOf("month"),
        todaysBookedTimes: [],
        timeSelectedForCall: "",
      },
      () => {
        this.handleMeetingTimeArr(this.state.selectedDate);
        this.checkIfFormValid();
      }
    );
  };

  disabledAppointmentDate = (current) => {
    return current && current < moment().endOf("day");
  };

  // Function to get loanQuote and insuranceQuote
  handleGetStoreBuyingInfo = () => {
    this.handleStoreInfoAPICall();
    return new Promise((resolve, reject) => {
      this.setState(
        {
          apiResponse: true,
        },
        () => {
          let payload = {
            mqttId: this.props.userId,
            postId: this.props.productId,
          };
          getStoreBuyingInfo(payload)
            .then((res) => {
              console.log("fbeuf", res);
              let tempPayload = { ...this.state.inputpayload };
              tempPayload.loanQuote = Math.round(res.data.data[0].loanQuote);
              tempPayload.insuranceQuote = Math.round(
                res.data.data[0].insuranceQuote
              );
              this.setState({
                inputpayload: { ...tempPayload },
                apiloading: res.data.code == 200 ? false : true,
              });
              let apiResponse = res.data.code == 200 ? true : false;
              return resolve(apiResponse);
            })
            .catch((err) => {
              reject(err);
            });
        }
      );
    });
  };

  // Function to get Latitude and Longitude from stateName and zipCode
  handleGetLatLng = async () => {
    this.setState({
      latLngAPI: true,
    });
    const {
      address,
      country,
      stateName,
      city,
      zipCode,
    } = this.state.inputpayload;
    let location;
    try {
      if (address && country && stateName && city && zipCode) {
        location = await getLocationfromStateZipcode(
          address,
          country,
          stateName,
          city,
          zipCode
        );
      }
      let tempPayload = { ...this.state.inputpayload };
      tempPayload["latitude"] = location.lat || location.latitude;
      tempPayload["longitude"] = location.lng || location.longitude;
      this.setState({
        inputpayload: { ...tempPayload },
        locationUpdate: true,
        latLngAPI: false,
      });
    } catch (err) {
      console.log("err---->", err);
      this.setState({
        locationUpdate: false,
      });
    }
    return this.state.locationUpdate;
  };

  handleNext = async () => {
    try {
      let lanLanUpdate =
        this.state.activeLink == "personal-info"
          ? await this.handleGetLatLng()
          : true;
      let apiResponse =
        this.state.activeLink == "view-quotes"
          ? await this.getMeetingsData()
          : this.state.activeLink == "sea-trial-appointment"
          ? await this.scheduleVideoMeeting()
          : this.state.activeLink == "my-insurance"
          ? this.handleGetStoreBuyingInfo()
          : await this.handleStoreInfoAPICall();
      this.setState(
        {
          apiResponse: apiResponse && lanLanUpdate,
        },
        () => {
          if (this.state.apiResponse) {
            switch (this.state.activeLink) {
              case "personal-info":
                this.setState({
                  step1: true,
                  activeLink: "boat-info",
                });
                break;
              case "boat-info":
                this.setState({
                  step2: true,
                  activeLink: "my-loan",
                });
                break;
              case "my-loan":
                this.setState({
                  step3: true,
                  activeLink: "my-pre-approval",
                });
                break;
              case "my-pre-approval":
                this.setState({
                  step4: true,
                  activeLink: "my-insurance",
                });
                break;
              case "my-insurance":
                this.setState({
                  step5: true,
                  activeLink: "view-quotes",
                });
                break;
              case "view-quotes":
                this.setState({
                  step6: true,
                  activeLink: "sea-trial-appointment",
                });
                break;
              case "sea-trial-appointment":
                this.setState({
                  step7: true,
                  activeLink: "confirmation",
                });
                break;
            }
          }
        }
      );
    } catch (err) {
      console.log("err", err);
      this.setState({
        usermessage: err.message,
        variant: "error",
        open: true,
        vertical: "bottom",
        horizontal: "left",
      });
    }
  };

  handleBack = () => {
    switch (this.state.activeLink) {
      case "boat-info":
        this.setState({
          step1: false,
          activeLink: "personal-info",
        });
        break;
      case "my-loan":
        this.setState({
          step2: false,
          activeLink: "boat-info",
        });
        break;
      case "my-pre-approval":
        this.setState({
          step3: false,
          activeLink: "my-loan",
        });
        break;
      case "my-insurance":
        this.setState({
          step4: false,
          activeLink: "my-pre-approval",
        });
        break;
      case "view-quotes":
        this.setState({
          step5: false,
          activeLink: "my-insurance",
        });
        break;
      case "sea-trial-appointment":
        this.setState({
          step6: false,
          activeLink: "view-quotes",
        });
        break;
      case "confirmation":
        this.setState({
          step7: false,
          activeLink: "sea-trial-appointment",
        });
        break;
    }
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  // Function to get Credit
  handleApprovedPurchasePage = () => {
    if (
      this.state.inputpayload.CustomerCreditScore != 0 &&
      this.state.inputpayload.CustomerTransid
    ) {
      this.updateScreen(
        <ApprovedPurchase
          inputpayload={this.state.inputpayload}
          handleBackGetPreApproval={this.handleBackGetPreApproval}
          handleNext={this.handleNext}
          apiloading={this.state.apiloading}
        />
      );
    } else {
      const {
        productId,
        first_name,
        last_name,
        address,
        city,
        stateName,
        zipCode,
        mobile,
      } = this.state.inputpayload;

      let Address = address.replaceAll(",", "");
      let payload = {
        userId: getCookie("uid"),
        productId: productId,
        first_name: first_name,
        last_name: last_name,
        address: Address,
        city: city,
        stateName: stateName.value,
        zipCode: zipCode,
        mobile: mobile,
      };
      this.setState({
        apiloading: true,
      });
      // checkCreditScore(this.state.inputpayload)
      postCreditScore(payload)
        .then((res) => {
          // var xml = convert.xml2json(res, { compact: true, spaces: 2 });
          // let response = JSON.parse(xml);
          // let creditScoreData = response && response.Results;
          let creditScoreData =
            res.data && res.data.message && res.data.message.Results;
          console.log("fbfbh", res, creditScoreData);
          this.setState(
            {
              apiloading: false,
              creditScoreData,
              usermessage: this.handleCreditScoreResponseMsg(creditScoreData),
              variant: this.handleCreditScoreResponseClr(creditScoreData),
              open: true,
              vertical: "bottom",
              horizontal: "left",
            },
            () => {
              if (
                this.handleCreditScoreResponseClr(creditScoreData) == "success"
              ) {
                let tempPayload = { ...this.state.inputpayload };
                tempPayload.CustomerCreditScore =
                  creditScoreData.XML_Report.Prescreen_Report.Score &&
                  creditScoreData.XML_Report.Prescreen_Report.Score._text
                    ? creditScoreData.XML_Report.Prescreen_Report.Score._text
                    : 0;
                tempPayload.CustomerTransid =
                  creditScoreData.XML_Report.Transid &&
                  creditScoreData.XML_Report.Transid._text
                    ? creditScoreData.XML_Report.Transid._text
                    : "";
                tempPayload.CustomerToken =
                  creditScoreData.XML_Report.Token &&
                  creditScoreData.XML_Report.Token._text
                    ? creditScoreData.XML_Report.Token._text
                    : "";
                this.setState(
                  {
                    inputpayload: { ...tempPayload },
                  },
                  () => {
                    this.handleSaveChanges();
                    this.updateScreen(
                      <ApprovedPurchase
                        inputpayload={this.state.inputpayload}
                        handleBackGetPreApproval={this.handleBackGetPreApproval}
                        handleNext={this.handleNext}
                        apiloading={this.state.apiloading}
                      />
                    );
                  }
                );
              } else if (
                this.handleCreditScoreResponseClr(creditScoreData) == "warning"
              ) {
                setTimeout(() => {
                  this.handleSSNModel();
                }, 1200);
              }
            }
          );
        })
        .catch((err) => {
          console.log("err", err);
          this.setState({
            apiloading: true,
          });
        });
    }
  };

  // Function to Save Profile Details
  handleSaveChanges = () => {
    const { inputpayload } = this.state;
    const { userProfileData } = this.props;
    let payload = {
      fullName: `${inputpayload.firstName} ${inputpayload.middleName} ${inputpayload.lastName}`,
      profilePicUrl: userProfileData.profilePicUrl,
      thumbnailImageUrl: userProfileData.profilePicUrl,
      creditScore: inputpayload.CustomerCreditScore,
      dob: parseInt(formatDate(inputpayload.dob, "X"), 10),
    };
    saveEditProfileData(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: this.handleResponseMsg(response),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleResponseMsg = (response) => {
    switch (response.code) {
      case 200:
        return "Successfully Updated the Profile";
        break;
      default:
        response.message;
    }
  };

  handleCreditScoreResponseMsg = (data) => {
    if (data && data.Creditsystem_Error) {
      return data.Creditsystem_Error._attributes.message;
    } else if (data && data.XML_Report && data.XML_Report.Prescreen_Report) {
      return data.XML_Report.Prescreen_Report.ResultDescription._text;
    }
  };

  handleCreditScoreResponseClr = (data) => {
    if (data && data.Creditsystem_Error) {
      return "error";
    } else if (data && data.XML_Report && data.XML_Report.Prescreen_Report) {
      switch (data.XML_Report.Prescreen_Report.ResultDescription._text) {
        case "Consumer Found and Score Returned":
          return "success";
          break;
        case "No Hit, Consumers file not found":
          return "warning";
          break;
      }
    }
  };

  // Function to toggle SSN Model
  handleSSNModel = () => {
    this.setState({
      ssnModel: !this.state.ssnModel,
    });
  };

  // Function to get creditScore with SSN
  handleGetCreditWithSSN = () => {
    const {
      productId,
      first_name,
      last_name,
      address,
      city,
      stateName,
      zipCode,
      mobile,
      SSN,
    } = this.state.inputpayload;

    let Address = address.replaceAll(",", "");
    let payload = {
      userId: getCookie("uid"),
      productId: productId,
      first_name: first_name,
      last_name: last_name,
      address: Address,
      city: city,
      stateName: stateName.value,
      zipCode: zipCode,
      mobile: mobile,
      ssn: SSN,
    };
    this.setState({
      ssnapiloading: true,
    });
    postCreditScore(payload)
      .then((res) => {
        // var xml = convert.xml2json(res, { compact: true, spaces: 2 });
        // let response = JSON.parse(xml);
        // let creditScoreData = response && response.Results;
        let creditScoreData =
          res.data && res.data.message && res.data.message.Results;
        this.setState(
          {
            ssnapiloading: false,
            creditScoreData,
            usermessage: this.handleCreditScoreResponseMsg(creditScoreData),
            variant: this.handleCreditScoreResponseClr(creditScoreData),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          },
          () => {
            if (
              this.handleCreditScoreResponseClr(creditScoreData) == "success"
            ) {
              let tempPayload = { ...this.state.inputpayload };
              tempPayload.CustomerCreditScore =
                creditScoreData.XML_Report.Prescreen_Report.Score &&
                creditScoreData.XML_Report.Prescreen_Report.Score._text
                  ? creditScoreData.XML_Report.Prescreen_Report.Score._text
                  : 0;
              tempPayload.CustomerTransid =
                creditScoreData.XML_Report.Transid &&
                creditScoreData.XML_Report.Transid._text
                  ? creditScoreData.XML_Report.Transid._text
                  : "";
              tempPayload.CustomerToken =
                creditScoreData.XML_Report.Token &&
                creditScoreData.XML_Report.Token._text
                  ? creditScoreData.XML_Report.Token._text
                  : "";
              this.setState(
                {
                  inputpayload: { ...tempPayload },
                  ssnModel: false,
                },
                () => {
                  this.updateScreen(
                    <ApprovedPurchase
                      inputpayload={this.state.inputpayload}
                      handleBackGetPreApproval={this.handleBackGetPreApproval}
                      handleNext={this.handleNext}
                      apiloading={this.state.apiloading}
                    />
                  );
                }
              );
            }
          }
        );
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          ssnapiloading: true,
          ssnModel: false,
        });
      });
  };

  handleBackGetPreApproval = () => {
    this.updateScreen();
  };

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  handleLoanFinancingVideo = () => {
    let videoSrc = BuyNow_Video;
    this.setState({
      openVideoModel: !this.state.openVideoModel,
      videoSrc,
    });
  };

  render() {
    const {
      isPersonalInfoFormValid,
      inputpayload,
      countryShortName,
      isPhoneValid,
      EmailValid,
      apiloading,
      preferredCountries,
      isBoatInfoFormValid,
      isLoanFormValid,
      currentScreen,
      isInsuranceFormValid,
      timeSelectedForCall,
      selectedDate,
      isAppointmentFormValid,
      ssnModel,
      SSNFormValid,
      ssnapiloading,
      latLngAPI,
      bookedMeetings,
      todaysBookedTimes,
    } = this.state;
    const {
      handleNext,
      handleBack,
      handleOnchangePhone,
      handleOnchangeflag,
      handleDOBChange,
      disabledDate,
      handleRadioInputChange,
      handleOnSelectInput,
      handleOnchangeInput,
      handleApprovedPurchasePage,
      handleOnchangeCheckbox,
      handleSSNModel,
      handlePurchaseDateChange,
      selectTimeForCall,
      scheduleVideoMeeting,
      onPanelChange,
      handleOnDateChange,
      handlePrevMonth,
      handleNextMonth,
      disabledAppointmentDate,
      handleMeetingType,
      handleGetCreditWithSSN,
    } = this;
    let personalInfoPage = (
      <PersonalInfoPage
        handleNext={handleNext}
        handleOnchangePhone={handleOnchangePhone}
        handleOnchangeflag={handleOnchangeflag}
        handleDOBChange={handleDOBChange}
        disabledDate={disabledDate}
        handleRadioInputChange={handleRadioInputChange}
        handleOnSelectInput={handleOnSelectInput}
        handleOnchangeInput={handleOnchangeInput}
        isPersonalInfoFormValid={isPersonalInfoFormValid}
        inputpayload={inputpayload}
        countryShortName={countryShortName}
        isPhoneValid={isPhoneValid}
        EmailValid={EmailValid}
        apiloading={apiloading}
        latLngAPI={latLngAPI}
        preferredCountries={preferredCountries}
      />
    );

    let pageContent = personalInfoPage;

    switch (this.state.activeLink) {
      case "personal-info":
        pageContent = personalInfoPage;
        break;
      case "boat-info":
        pageContent = (
          <BoatInfoPage
            handleNext={handleNext}
            handleBack={handleBack}
            inputpayload={inputpayload}
            handleOnSelectInput={handleOnSelectInput}
            handleOnchangeInput={handleOnchangeInput}
            handleRadioInputChange={handleRadioInputChange}
            isBoatInfoFormValid={isBoatInfoFormValid}
            apiloading={apiloading}
          />
        );
        break;
      case "my-loan":
        pageContent = (
          <LoansPage
            handleNext={handleNext}
            handleBack={handleBack}
            inputpayload={inputpayload}
            handleOnSelectInput={handleOnSelectInput}
            handleOnchangeInput={handleOnchangeInput}
            handleRadioInputChange={handleRadioInputChange}
            isLoanFormValid={isLoanFormValid}
            apiloading={apiloading}
          />
        );
        break;
      case "my-pre-approval":
        pageContent = (
          <GetPreApproved
            handleApprovedPurchasePage={handleApprovedPurchasePage}
            currentScreen={currentScreen}
            handleBack={handleBack}
            inputpayload={inputpayload}
            handleOnchangeCheckbox={handleOnchangeCheckbox}
            apiloading={apiloading}
            ssnModel={ssnModel}
            handleSSNModel={handleSSNModel}
            handleOnchangeInput={handleOnchangeInput}
            SSNFormValid={SSNFormValid}
            ssnapiloading={ssnapiloading}
            handleGetCreditWithSSN={handleGetCreditWithSSN}
          />
        );
        break;
      case "my-insurance":
        pageContent = (
          <MyInsurance
            handleNext={handleNext}
            handleBack={handleBack}
            inputpayload={inputpayload}
            handlePurchaseDateChange={handlePurchaseDateChange}
            handleOnchangeInput={handleOnchangeInput}
            handleOnSelectInput={handleOnSelectInput}
            isInsuranceFormValid={isInsuranceFormValid}
            apiloading={apiloading}
          />
        );
        break;
      case "view-quotes":
        pageContent = (
          <ViewQuote
            handleNext={handleNext}
            handleBack={handleBack}
            inputpayload={inputpayload}
          />
        );
        break;
      case "sea-trial-appointment":
        pageContent = (
          <TrailAppointment
            handleNext={handleNext}
            handleBack={handleBack}
            inputpayload={inputpayload}
            timeSelectedForCall={timeSelectedForCall}
            selectedDate={selectedDate}
            selectTimeForCall={selectTimeForCall}
            scheduleVideoMeeting={scheduleVideoMeeting}
            onPanelChange={onPanelChange}
            handleOnDateChange={handleOnDateChange}
            handlePrevMonth={handlePrevMonth}
            handleNextMonth={handleNextMonth}
            disabledAppointmentDate={disabledAppointmentDate}
            apiloading={apiloading}
            isAppointmentFormValid={isAppointmentFormValid}
            handleMeetingType={handleMeetingType}
            bookedMeetings={bookedMeetings}
            todaysBookedTimes={todaysBookedTimes}
          />
        );
        break;
      case "confirmation":
        pageContent = <ConfirmingPage inputpayload={inputpayload} />;
        break;
      default:
        pageContent = personalInfoPage;
    }

    const {
      productImage,
      ProductName,
      sellerProfilePicUrl,
      sellerName,
      productCurrency,
      productPrice,
    } = this.state.inputpayload;
    return (
      <Wrapper>
        <div className="row m-0 align-items-center ProfilePageSec">
          <div className="container p-md-0">
            <div className="screenWidth mx-auto p-0">
              <div className="row m-0 justify-content-start">
                <div className="col-11 p-0 py-4">
                  <div className="row m-0 align-items-center mb-3">
                    <div className="col-lg-3 col-md-3 TabHeading p-0">
                      <div className="HeadingSec d-flex align-items-center">
                        <img
                          src={Purchase_Hamburger_Icon}
                          className="hamburgerIcon"
                        ></img>
                        <h5 className="Heading">Purchase Steps</h5>
                      </div>
                    </div>
                    <div className="col TabPanel ProductHeader p-0">
                      <div className="row m-0 align-items-center">
                        <div className="col-8 p-0">
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 pl-2 productImg_Sec">
                              <img src={productImage} className="itemImg"></img>
                            </div>
                            <div className="col p-0 pl-2">
                              <h6 className="itemTitle">{ProductName}</h6>
                            </div>
                          </div>
                        </div>
                        <div className="col-4 p-0">
                          <div className="row m-0">
                            <div className="col-7 p-0 SellerInfo">
                              <div className="d-flex align-items-center">
                                <img
                                  src={sellerProfilePicUrl}
                                  className="sellerPic"
                                ></img>
                                <h5 className="sellerName">{sellerName}</h5>
                              </div>
                            </div>
                            <div className="col p-0">
                              <div className="cartDetails">
                                <p className="d-flex align-items-center justify-content-end m-0 mb-1">
                                  <img
                                    src={Cart_Dark_Grey_Icon}
                                    className="cartIcon"
                                  ></img>{" "}
                                  <span className="cartSummaryLabel">
                                    Cart Summary
                                  </span>
                                </p>
                                <h6 className="itemPrice">
                                  {this.handleCurrencySymbol(productCurrency)}{" "}
                                  {new Number(productPrice).toLocaleString(
                                    "en-US"
                                  )}
                                </h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0">
                    <div className="col-lg-3 col-md-3 Tabs p-0">
                      <ul className="list-unstyled BuyingTabs m-0">
                        <li
                          className={
                            this.state.activeLink === "personal-info"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          onClick={
                            this.state.activeLink === "personal-info"
                              ? ""
                              : !this.state.step1
                              ? ""
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step1 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink === "personal-info" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "personal-info" ||
                                  this.state.step1
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                Personal Info
                              </p>
                              <span className="statusLabel">
                                {this.state.step1
                                  ? "Completed"
                                  : "About 1 minutes"}
                              </span>
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "boat-info"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.isPersonalInfoFormValid
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.isPersonalInfoFormValid}
                          onClick={
                            this.state.activeLink === "boat-info"
                              ? ""
                              : this.state.isPersonalInfoFormValid &&
                                !this.state.step2
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step2 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink === "boat-info" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "boat-info" ||
                                  this.state.step2
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                Boat Info
                              </p>
                              <span className="statusLabel">
                                {this.state.step2
                                  ? "Completed"
                                  : "About 2 minutes"}
                              </span>
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "my-loan"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.isBoatInfoFormValid
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.isBoatInfoFormValid}
                          onClick={
                            this.state.activeLink === "my-loan"
                              ? ""
                              : this.state.isBoatInfoFormValid &&
                                !this.state.step3
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step3 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink === "my-loan" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "my-loan" ||
                                  this.state.step3
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                My Loan
                              </p>
                              <span className="statusLabel">
                                {this.state.step3
                                  ? "Completed"
                                  : "About 2 minutes"}
                              </span>
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "my-pre-approval"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.isLoanFormValid
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.isLoanFormValid}
                          onClick={
                            this.state.activeLink === "my-pre-approval"
                              ? ""
                              : this.state.isLoanFormValid && !this.state.step4
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step4 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink ===
                                "my-pre-approval" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "my-pre-approval" ||
                                  this.state.step4
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                My PreQualification
                              </p>
                              <span className="statusLabel">
                                {this.state.step4
                                  ? "Completed"
                                  : "About 1 minutes"}
                              </span>
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "my-insurance"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.loanTermsApproved
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.loanTermsApproved}
                          onClick={
                            this.state.activeLink === "my-insurance"
                              ? ""
                              : this.state.loanTermsApproved &&
                                !this.state.step5
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step5 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink === "my-insurance" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "my-insurance" ||
                                  this.state.step5
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                My Insurance
                              </p>
                              <span className="statusLabel">
                                {this.state.step5
                                  ? "Completed"
                                  : "About 2 minutes"}
                              </span>
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "view-quotes"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.isInsuranceFormValid
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.isInsuranceFormValid}
                          onClick={
                            this.state.activeLink === "view-quotes"
                              ? ""
                              : this.state.isInsuranceFormValid &&
                                !this.state.step6
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step6 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink === "view-quotes" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "view-quotes" ||
                                  this.state.step6
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                View Quotes
                              </p>
                              <span className="statusLabel">
                                {this.state.step6
                                  ? "Completed"
                                  : "About 2 minutes"}
                              </span>
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "sea-trial-appointment"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.isQuoteFormValid
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.isQuoteFormValid}
                          onClick={
                            this.state.activeLink === "sea-trial-appointment"
                              ? ""
                              : this.state.isQuoteFormValid && !this.state.step7
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step7 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink ===
                                "sea-trial-appointment" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink ===
                                    "sea-trial-appointment" || this.state.step7
                                    ? "activeLabel tabLabel appointmentLabel"
                                    : "tabLabel appointmentLabel"
                                }
                              >
                                Sea Trial / Appointment
                              </p>
                              {/* <span className="statusLabel">
                                {this.state.step7
                                  ? "Completed"
                                  : "About 5 minutes"}
                              </span> */}
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                        <li
                          className={
                            this.state.activeLink === "confirmation"
                              ? "activeLink tabLink"
                              : "tabLink"
                          }
                          style={
                            this.state.isAppointmentFormValid
                              ? { cursor: "pointer" }
                              : { cursor: "no-drop" }
                          }
                          disabled={!this.state.isAppointmentFormValid}
                          onClick={
                            this.state.activeLink === "confirmation"
                              ? ""
                              : this.state.isAppointmentFormValid &&
                                !this.state.step8
                              ? this.handleNext.bind(
                                  this,
                                  this.state.inputpayload
                                )
                              : this.handleBack
                          }
                        >
                          <div className="row m-0 align-items-center">
                            <div className="col-2 p-0 iconSec">
                              {this.state.step8 ? (
                                <img
                                  src={Selected_Checkbox_Green_Icon}
                                  className="statusIcon"
                                ></img>
                              ) : this.state.activeLink === "confirmation" ? (
                                <img
                                  src={Radio_Button_Active}
                                  className="statusIcon"
                                ></img>
                              ) : (
                                <img
                                  src={Radio_Button_Inactive}
                                  className="statusIcon"
                                ></img>
                              )}
                            </div>
                            <div className="col p-0 tabDiv">
                              <p
                                className={
                                  this.state.activeLink === "confirmation" ||
                                  this.state.step8
                                    ? "activeLabel tabLabel"
                                    : "tabLabel"
                                }
                              >
                                Confirmation
                              </p>
                              {/* <span className="statusLabel">
                                {this.state.step8
                                  ? "Completed"
                                  : "About 5 minutes"}
                              </span> */}
                            </div>
                            <img
                              src={Chevron_Right}
                              className="chevronRight"
                            ></img>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div className="col TabPanel">{pageContent}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="videoDiv absoluteDiv">
            <div className="videoBlock">
              <video className="video">
                <source src={BuyNow_Video} type="video/mp4"></source>
              </video>
            </div>
            <div className="videoIconDiv">
              <img
                src={Play_Video_Icon}
                className="videoIcon"
                onClick={this.handleLoanFinancingVideo}
              ></img>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />

        {/* Video Model */}
        <Model
          open={this.state.openVideoModel}
          onClose={this.handleLoanFinancingVideo}
        >
          <VideoModel
            onClose={this.handleLoanFinancingVideo}
            videoSrc={this.state.videoSrc}
          />
        </Model>
        <style jsx>
          {`
            .ProfilePageSec {
              background: ${BG_LightGREY_COLOR};
              position: relative;
            }
            .heading {
              font-size: 1.171vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              color: ${FONTGREY_COLOR};
            }
            .iconSec {
              max-width: 1.756vw;
              line-height: 1;
            }
            .TabHeading {
              max-width: 14.641vw;
            }
            .Tabs {
              max-width: 13.177vw;
            }
            .BuyingTabs p-0 > li {
              padding: 0.292vw 0.439vw;
            }
            .tabDiv {
              line-height: 1;
            }
            .tabLabel {
              font-size: 0.841vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_2};
              margin: 0;
              line-height: 0.8;
            }
            .appointmentLabel {
              line-height: 1.2;
              max-width: 80%;
            }
            .statusLabel {
              font-size: 0.585vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
              line-height: 0.8;
              margin: 0;
            }
            .chevronRight {
              position: absolute;
              top: 50%;
              right: 1.464vw;
              width: 0.439vw;
              transform: translate(0, -50%);
            }
            .TabPanel {
              background: ${WHITE_COLOR};
            }
            .ProductHeader {
              padding: 0.878vw 0 !important;
              border-radius: 0.219vw;
            }
            .activeLink {
              background: ${WHITE_COLOR};
            }
            .tabLink {
              padding: 1.244vw 0.732vw;
              cursor: pointer;
              position: relative;
            }
            .activeLabel {
              color: ${FONTGREY_COLOR} !important;
              font-family: "Museo-Sans" !important;
            }
            .statusIcon {
              width: 1.098vw;
              margin-bottom: 0.146vw;
            }
            .paymentIcon {
              width: 0.951vw;
              margin-bottom: 0.146vw;
            }
            .depositIcon {
              width: 0.951vw;
              margin-bottom: 0.146vw;
            }
            .HeadingSec {
              padding: 0.732vw;
            }
            .hamburgerIcon {
              width: 0.878vw;
              margin: 0 0.366vw 0.146vw 0;
            }
            .Heading {
              font-size: 1.098vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .productImg_Sec {
              max-width: 4.758vw;
            }
            .itemImg {
              width: 100%;
              object-fit: cover;
              border-radius: 0.0732vw;
              height: 2.928vw;
            }
            .itemTitle {
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              font-style: normal;
              color: ${FONTGREY_COLOR};
              margin: 0 0 0 0.366vw;
              max-width: 50%;
              font-weight: 600;
            }
            .SellerInfo {
              max-width: 8.784vw;
            }
            .sellerPic {
              width: 2.196vw;
              height: 2.196vw;
              margin-right: 0.366vw;
              border-radius: 50%;
            }
            .sellerName {
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .cartDetails {
              border-left: 0.0732vw solid ${GREY_VARIANT_10};
              padding: 0 0.951vw;
            }
            .cartSummaryLabel {
              font-size: 0.732vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              font-style: normal;
              letter-spacing: 0.0219vw !important;
              color: ${GREY_VARIANT_1};
            }
            .cartIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .itemPrice {
              font-size: 0.878vw;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              color: ${FONTGREY_COLOR};
              margin: 0 0 0 auto;
              font-weight: 600;
              text-align: right;
            }
            .videoDiv {
              cursor: pointer;
              padding: 0.292vw;
              position: absolute;
              right: 1.756vw;
              top: 1.5rem;
              background: ${WHITE_COLOR};
              border-radius: 0.219vw;
            }
            .videoBlock {
              width: 100%;
              height: 100%;
              display: flex;
              justify-content: center;
              align-items: center;
            }
            .videoBlock .video {
              width: 100%;
            }
            .videoIconDiv {
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            .videoIcon {
              width: 2.342vw;
              object-fit: cover;
              display: block;
              z-index: 1;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default BuyWithBoatzonPage;
