import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import { NumberValidator } from "../../lib/validation/validation";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  WHITE_COLOR,
  Question_LightGrey_Icon,
  Question_LightBlue_Icon,
  Long_Arrow_Right_White_Icon,
  Long_Arrow_Left_White_Icon,
  GREY_VARIANT_10,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import { DatePicker } from "antd";
import CircularProgressButton from "../../components/button-loader/button-loader";

class MyInsurance extends Component {
  render() {
    const NumYearOptions = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
      { value: "6", label: "6" },
      { value: "7", label: "7" },
      { value: "8", label: "8" },
      { value: "9", label: "9" },
      { value: "10+", label: "10+" },
    ];
    const HullDeductionOptions = [
      { value: "1%", label: "1%" },
      { value: "2%", label: "2%" },
      { value: "3%", label: "3%" },
      { value: "4%", label: "4%" },
      { value: "5%", label: "5%" },
    ];

    const MedicalLimitOptions = [
      { value: "10000", label: "10000" },
      { value: "25000", label: "25000" },
    ];

    const LiabilityLimitOptions = [
      { value: "100000", label: "100000" },
      { value: "300000", label: "300000" },
      { value: "500000", label: "500000" },
      { value: "1000000", label: "1000000" },
    ];

    const MooringLocations = [
      { value: "Marina", label: "Marina" },
      { value: "Private Residence", label: "Private Residence" },
    ];

    const MooringTypes = [
      { value: "Slipped", label: "Slipped" },
      { value: "Dry Storage", label: "Dry Storage" },
      { value: "Lift", label: "Lift" },
      { value: "Trailered", label: "Trailered" },
    ];

    const FireFightingSystem = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const BurglaryDetection = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];
    const BoatingLoss = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const TrailerAvaiable = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];

    const PremiumUpFront = [
      { value: "Yes", label: "Yes" },
      { value: "No", label: "No" },
    ];
    const BoatCourses = [
      { value: "None", label: "None" },
      { value: "Boat Course", label: "Boat Course" },
      {
        value: "USCG Auxiliary Current Member",
        label: "USCG Auxiliary Current Member",
      },
      {
        value: "US Power Squadron Current Member",
        label: "US Power Squadron Current Member",
      },
      {
        value: "Current USCG Licensed Captain",
        label: "Current USCG Licensed Captain",
      },
    ];
    const {
      purchase_date,
      insurance_purchase_price,
      years_owned,
      replacement_cost,
      hull_deduction,
      liability_limit,
      medical,
      mooring_locations,
      mooring_types,
      mooring_zip_code,
      fire_fighting_system,
      burglary_detection,
      trailer_avaiable,
      years_boat_ownership,
      years_boat_experience,
      boat_courses,
      boating_loss,
      premium_up_front,
    } = {
      ...this.props.inputpayload,
    };
    const { apiloading } = this.props;
    return (
      <Wrapper>
        <div className="py-3 InsurancePage">
          <div className="col-12 section">
            <h6 className="title">
              Boat Coverange Info{" "}
              <span className="helperText">
                (<span className="mandatory">*</span> Required Field)
              </span>
            </h6>
            <div className="row m-0">
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Purchase Date: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <div className="dobInput">
                            <DatePicker
                              onChange={this.props.handlePurchaseDateChange}
                              format="DD-MM-YYYY"
                              value={purchase_date}
                              placeholder="DD-MM-YYYY"
                            />
                          </div>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Purchase Price: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="insurance_purchase_price"
                            value={insurance_purchase_price}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Years Owned Boat: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={NumYearOptions}
                            value={years_owned}
                            onChange={this.props.handleOnSelectInput(
                              `years_owned`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Replacement Cost: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="replacement_cost"
                            value={replacement_cost}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Hull Deduction: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={HullDeductionOptions}
                            value={hull_deduction}
                            onChange={this.props.handleOnSelectInput(
                              `hull_deduction`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Liability Limit: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={LiabilityLimitOptions}
                            value={liability_limit}
                            onChange={this.props.handleOnSelectInput(
                              `liability_limit`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <p className="lineHeight">
                        <span>
                          Medical: <span className="mandatory">*</span>
                        </span>
                        <span className="optionalMsg">(Per Person)</span>
                      </p>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={MedicalLimitOptions}
                            value={medical}
                            onChange={this.props.handleOnSelectInput(`medical`)}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <div className="col-12 section">
            <h6 className="title">Mooring Info</h6>
            <div className="row m-0">
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Mooring Location: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={MooringLocations}
                            value={mooring_locations}
                            onChange={this.props.handleOnSelectInput(
                              `mooring_locations`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Type of Mooring: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={MooringTypes}
                            value={mooring_types}
                            onChange={this.props.handleOnSelectInput(
                              `mooring_types`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Zip Code: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="mooring_zip_code"
                            value={mooring_zip_code}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-6 p-0"></div>
            </div>
          </div>

          <div className="col-12 section">
            <h6 className="title">Questions To Obtain Discounts!</h6>
            <div className="row m-0">
              <div className="col-10 p-0">
                <table className="discountTable">
                  <tr>
                    <th>
                      Is there an Automatic Fire Fighting System/Fire
                      extinguisher: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={FireFightingSystem}
                            value={fire_fighting_system}
                            onChange={this.props.handleOnSelectInput(
                              `fire_fighting_system`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 5% Discount</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Is there a GPS/burglary detection system in boat:{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={BurglaryDetection}
                            value={burglary_detection}
                            onChange={this.props.handleOnSelectInput(
                              `burglary_detection`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 5% Discount</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Do you have a trailer you would like cover:{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={TrailerAvaiable}
                            value={trailer_avaiable}
                            onChange={this.props.handleOnSelectInput(
                              `trailer_avaiable`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">
                        One of our programs applies discount if your boat is
                        stored on the coast, and you have a trailer. The closer
                        you are to the coast, the larger the discount.
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Total years of boat ownership:{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="years_boat_ownership"
                            value={years_boat_ownership}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 5% Discount</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Total years of boating experience:{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="years_boat_experience"
                            value={years_boat_experience}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 5% Discount</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Boating Cources Take: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={BoatCourses}
                            value={boat_courses}
                            onChange={this.props.handleOnSelectInput(
                              `boat_courses`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 10% Discount</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Have you had a boating loss or claim in the past 3 years:{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={BoatingLoss}
                            value={boating_loss}
                            onChange={this.props.handleOnSelectInput(
                              `boating_loss`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 10% Discount</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Would you be interested in paying your premium up front?{" "}
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={PremiumUpFront}
                            value={premium_up_front}
                            onChange={this.props.handleOnSelectInput(
                              `premium_up_front`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr className="mt-1">
                    <th></th>
                    <td>
                      <p className="discountMsg">Up to 15% Discount</p>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <div className="col-12 p-0 py-2">
            <div className="d-flex align-items-center justify-content-between">
              <div className="back_btn">
                <ButtonComp onClick={this.props.handleBack}>
                  <img
                    src={Long_Arrow_Left_White_Icon}
                    className="backArrow"
                  ></img>{" "}
                  Back
                </ButtonComp>
              </div>
              <div
                className={
                  this.props.isInsuranceFormValid ? "next_btn" : "back_btn"
                }
              >
                <CircularProgressButton
                  buttonText={
                    <span>
                      Next{" "}
                      <img
                        src={Long_Arrow_Right_White_Icon}
                        className="nextArrow"
                      ></img>
                    </span>
                  }
                  disabled={!this.props.isInsuranceFormValid}
                  onClick={this.props.handleNext}
                  loading={apiloading}
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .helperText {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-weight: 500;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            .title {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
              padding-bottom: 0.585vw;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .section {
              padding: 1.098vw 0;
            }
            .contentTabel {
              width: 90%;
            }
            .contentTabel th {
              width: 43%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
              line-height: 1.2;
            }
            .lineHeight {
              line-height: 0.8;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
              margin: 0;
              display: flex;
              flex-direction: column;
            }
            .optionalMsg {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
              top: 3px;
              position: relative;
            }
            .discountTable th {
              width: 60%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
            }
            .discountMsg {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans-SemiBold" !important;
              margin: 0;
            }
            .questionIcon {
              background: url(${Question_LightGrey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
              cursor: pointer;
            }
            .questionIcon:hover {
              background: url(${Question_LightBlue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
            }
            .contentHeading {
              font-size: 0.915vw;
              color: ${FONTGREY_COLOR};
              font-weight: 600;
              margin-bottom: 0.366vw;
              font-family: "Open Sans" !important;
            }
            .contentTabel td {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
            }
            :global(.discountTable td .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              margin-top: 0.366vw;
              font-family: "Open Sans" !important;
            }
            :global(.discountTable td .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.discountTable td .inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.contentTabel td .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              margin-top: 0.366vw;
              font-family: "Open Sans" !important;
            }
            :global(.contentTabel td .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.contentTabel td .inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            .dobInput {
              width: 100% !important;
            }
            :global(.dobInput .ant-picker) {
              margin-top: 0.366vw;
              height: 2.196vw;
              width: 100%;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
            }
            :global(.dobInput .ant-picker-focused) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.dobInput .ant-picker-input input) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.dobInput .ant-picker-input input::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.ant-picker-dropdown) {
              font-size: 0.805vw !important;
            }
            :global(.ant-picker-date-panel) {
              width: 230px !important;
            }
            :global(.ant-picker-date-panel .ant-picker-content) {
              width: 210px !important;
              table-layout: auto;
            }
            :global(.ant-picker-panel .ant-picker-footer) {
              display: none;
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default MyInsurance;
