import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import { NumberValidator } from "../../lib/validation/validation";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  THEME_COLOR,
  WHITE_COLOR,
  Question_LightGrey_Icon,
  Question_LightBlue_Icon,
  Long_Arrow_Right_White_Icon,
  Long_Arrow_Left_White_Icon,
  GREY_VARIANT_10,
  Light_Blue_Color,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
const YearsList = getYearsList();
import { getYearsList } from "../../lib/date-operation/date-operation";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { withStyles } from "@material-ui/core";
import CircularProgressButton from "../../components/button-loader/button-loader";
import InfoTooltip from "../../components/tooltip/info-tooltip";

const styles = (theme) => ({
  radiogrouRoot: {
    flexDirection: "row",
  },
  radioLabelRoot: {
    margin: "0",
  },
  radioLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Museo-Sans !important",
  },
  root: {
    paddingLeft: "0",
    color: `${GREY_VARIANT_3}`,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
    "& span": {
      "& div": {
        "& svg": {
          width: "0.951vw",
        },
      },
    },
  },
  checked: {
    color: `${THEME_COLOR} !important`,
  },
});

class BoatInfoPage extends Component {
  render() {
    const BoatsTypes =
      this.props.BoatsSubCategoriesList &&
      this.props.BoatsSubCategoriesList.map((data) => ({
        value: data.subCategoryName,
        label: data.subCategoryName,
      }));

    const ManufacturerType = [
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
    ];
    const YearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const UsageOptions = [
      { value: "Personal", label: "Personal" },
      { value: "Charter-Fishing Guide", label: "Charter-Fishing Guide" },
    ];

    const EngineTypes = [
      { value: "Electric", label: "Electric" },
      { value: "Outboard", label: "Outboard" },
      { value: "Inboard", label: "Inboard" },
    ];

    const EngineMakeTypes =
      this.props.engineData.engines &&
      this.props.engineData.engines.map((data) => {
        return { value: data, label: data };
      });

    const EngineNumbers = [
      { value: "0", label: "0" },
      { value: "1", label: "1" },
      { value: "2", label: "2" },
      { value: "3", label: "3" },
      { value: "4", label: "4" },
      { value: "5", label: "5" },
      { value: "6", label: "6" },
    ];

    const EngineYearOptions =
      YearsList &&
      YearsList.map((data) => ({
        value: data,
        label: data,
      }));

    const HoursePowerTypes = [
      { value: "115", label: "115" },
      { value: "200", label: "200" },
      { value: "300", label: "300" },
    ];

    const FuelTypes = [
      { value: "Gas", label: "Gas" },
      { value: "Diesel", label: "Diesel" },
      { value: "Electric", label: "Electric" },
    ];

    const { classes, apiloading } = this.props;
    const {
      condition,
      boat_type,
      manufacturer_type,
      boat_make,
      boat_year,
      hull_idNum,
      length,
      boat_usage,
      boat_speed,
      engine_type,
      engine_make,
      engine_year,
      num_of_engines,
      hourse_power,
      fuel_type,
    } = this.props.inputpayload;
    const buyWithBoatzonMsg = (
      <p className="buyWithBoatzonMsg">
        Category typically refers to the way in which the boat is designed to be
        used.
      </p>
    );
    return (
      <Wrapper>
        <div className="py-3 BoatInfoPage">
          <div className="col-12 section">
            <h6 className="title">
              Boat Information{" "}
              <span className="helperText">
                (<span className="mandatory">*</span> Required Field)
              </span>
            </h6>
            <div className="row m-0">
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Boat Condition: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <FormControl component="fieldset">
                        <RadioGroup
                          classes={{
                            root: classes.radiogrouRoot,
                          }}
                          aria-label="condition"
                          name="condition"
                          value={condition}
                          onChange={this.props.handleRadioInputChange}
                        >
                          <FormControlLabel
                            classes={{
                              root: classes.radioLabelRoot,
                              label: classes.radioLabel,
                            }}
                            value="New"
                            control={
                              <Radio
                                classes={{
                                  root: classes.root,
                                  checked: classes.checked,
                                }}
                              />
                            }
                            label="New"
                          />
                          <FormControlLabel
                            classes={{
                              root: classes.radioLabelRoot,
                              label: classes.radioLabel,
                            }}
                            value="Used"
                            control={
                              <Radio
                                classes={{
                                  root: classes.root,
                                  checked: classes.checked,
                                }}
                              />
                            }
                            label="Used"
                          />
                        </RadioGroup>
                      </FormControl>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Boat Type: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={BoatsTypes}
                            value={boat_type}
                            onChange={this.props.handleOnSelectInput(
                              `boat_type`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <InfoTooltip
                            tooltipContent={buyWithBoatzonMsg}
                            placement="right"
                            color={Light_Blue_Color}
                          >
                            <div className="questionIcon"></div>
                          </InfoTooltip>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Manufacturer: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={ManufacturerType}
                            value={manufacturer_type}
                            onChange={this.props.handleOnSelectInput(
                              `manufacturer_type`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Boat Make: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="boat_make"
                            value={boat_make}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Boat Year: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={YearOptions}
                            value={boat_year}
                            onChange={this.props.handleOnSelectInput(
                              `boat_year`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      <p className="lineHeight">
                        Hull ID Number:
                        <span className="optionalMsg">(optional)</span>
                      </p>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="hull_idNum"
                            value={hull_idNum}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Length: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="length"
                            value={length}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Boat Use: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={UsageOptions}
                            value={boat_usage}
                            onChange={this.props.handleOnSelectInput(
                              `boat_usage`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Boat Top Speed:
                      <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="boat_speed"
                            value={boat_speed}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <div className="col-12 section">
            <h6 className="title">Engine Information</h6>
            <div className="row m-0">
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      Engine Type: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={EngineTypes}
                            value={engine_type}
                            onChange={this.props.handleOnSelectInput(
                              `engine_type`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Engine Make: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={EngineMakeTypes}
                            value={engine_make}
                            onChange={this.props.handleOnSelectInput(
                              `engine_make`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Engine Year: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={EngineYearOptions}
                            value={engine_year}
                            onChange={this.props.handleOnSelectInput(
                              `engine_year`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="col-6 p-0">
                <table className="contentTabel">
                  <tr>
                    <th>
                      No. of Engines: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={EngineNumbers}
                            value={num_of_engines}
                            onChange={this.props.handleOnSelectInput(
                              `num_of_engines`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Horse Power <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <InputBox
                            type="text"
                            className="inputBox form-control"
                            name="hourse_power"
                            value={hourse_power}
                            onChange={this.props.handleOnchangeInput}
                            autoComplete="off"
                            onKeyPress={NumberValidator}
                          ></InputBox>
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Fuel Type: <span className="mandatory">*</span>
                    </th>
                    <td>
                      <div className="row m-0 align-items-center divWidth">
                        <div className="col-11 p-0">
                          <SelectInput
                            servicesSelect={true}
                            options={FuelTypes}
                            value={fuel_type}
                            onChange={this.props.handleOnSelectInput(
                              `fuel_type`
                            )}
                          />
                        </div>
                        <div className="col-1 p-0">
                          <div className="questionIcon"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <div className="col-12 p-0 py-2">
            <div className="d-flex align-items-center justify-content-between">
              <div className="back_btn">
                <ButtonComp onClick={this.props.handleBack}>
                  <img
                    src={Long_Arrow_Left_White_Icon}
                    className="backArrow"
                  ></img>{" "}
                  Back
                </ButtonComp>
              </div>
              <div
                className={
                  this.props.isBoatInfoFormValid ? "next_btn" : "back_btn"
                }
              >
                <CircularProgressButton
                  buttonText={
                    <span>
                      Next{" "}
                      <img
                        src={Long_Arrow_Right_White_Icon}
                        className="nextArrow"
                      ></img>
                    </span>
                  }
                  disabled={!this.props.isBoatInfoFormValid}
                  onClick={this.props.handleNext}
                  loading={apiloading}
                />
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .helperText {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-weight: 500;
            }
            :global(.buyWithBoatzonMsg) {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              color: ${WHITE_COLOR};
              margin: 0;
              padding: 0.366vw;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            .divWidth {
              width: 95%;
            }

            .title {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
              padding-bottom: 0.585vw;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .section {
              padding: 1.098vw 0;
            }
            .contentTabel {
              width: 90%;
            }
            .contentTabel th {
              width: 40%;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
            }
            .lineHeight {
              line-height: 0.8;
              font-size: 0.878vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
              margin: 0;
              display: flex;
              flex-direction: column;
            }
            .optionalMsg {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans-SemiBold" !important;
              font-weight: 500 !important;
              top: 0.219vw;
              position: relative;
            }
            .questionIcon {
              background: url(${Question_LightGrey_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
              cursor: pointer;
            }
            .questionIcon:hover {
              background: url(${Question_LightBlue_Icon});
              background-repeat: no-repeat;
              background-size: contain;
              width: 0.732vw;
              height: 0.805vw;
              margin-left: 0.366vw;
              margin-top: 0.366vw;
            }
            .contentHeading {
              font-size: 0.915vw;
              color: ${FONTGREY_COLOR};
              font-weight: 600;
              margin-bottom: 0.366vw;
              font-family: "Open Sans" !important;
            }
            .contentTabel td {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
            }
            :global(.contentTabel td .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              margin-top: 0.366vw;
              font-family: "Open Sans" !important;
            }
            :global(.contentTabel td .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.contentTabel td .inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_2};
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    BoatsSubCategoriesList: state.BoatsSubCategoriesList,
    currentLocation: state.currentLocation,
    engineData: state.engineData,
  };
};

export default connect(mapStateToProps)(withStyles(styles)(BoatInfoPage));
