import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  FONTGREY_COLOR,
  FONTGREY_COLOR_Dark,
  GREEN_COLOR,
  WHITE_COLOR,
} from "../../lib/config";

class EleSignTerms extends Component {
  render() {
    const { onClose } = this.props;
    return (
      <Wrapper>
        <div className="col-12 EleSignTerms_Sec p-4">
          <img src={Close_Icon} className="closeIcon" onClick={onClose}></img>
          <h6 className="heading">
            Consent for Electronic Disclosures Electronic Signatures in Global
            and National Commerce Act.
          </h6>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">Introduction: </span>
                You are submitting a request for a loan pre-qualification, a
                loan application or form through us (a "Request"). We can only
                give you the benefits of our service by conducting our business
                through the Internet. In order to do this, we need you to
                consent to our giving you certain disclosures electronically.
                This document informs you of your rights when receiving legally
                required disclosures, notices and information ("Disclosures")
                from us and the lenders to whom your Request is submitted. By
                completing and submitting a Request through us, you acknowledge
                receipt of this document and consent to the electronic delivery
                of such Disclosures.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">Electronic Communications: </span>
                Any Disclosures related to your Request will be provided to you
                electronically through our technology provider, 700Credit LLC.
                However, if you wish to obtain a paper copy of any of the
                Disclosures, you may write to 700Credit LLC., 31440 Northwestern
                Highway Suite 250 Farmington Hills, MI 48334 with the details of
                your request. Paper copies will be provided to you at no charge.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">
                  Consenting to Do Business Electronically:{" "}
                </span>
                Before you decide to do business electronically with us, you
                should consider whether you have the required hardware and
                software capabilities described in this document.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">Scope of Consent: </span>
                Your consent to receive Disclosures and to do business
                electronically, and our agreement to do so, only applies to this
                Request. You will receive Disclosures from our technology
                provider 700Credit LLC., and you may also receive Disclosures
                and other communications from our participating Lenders. After
                your Request is transmitted to one or more of our participating
                Lenders, and after you decide to continue to pursue your
                Request, then you and the Lender should discuss how subsequent
                Disclosures will be delivered.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">
                  Hardware and Software Requirements:{" "}
                </span>
                To access and retain the Disclosures electronically, you will
                need to use the following computer software and hardware:
                Internet Explorer 4.0 or above, Netscape Navigator 4.0 or above
                or equivalent software; and hardware capable of running this
                software.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">Withdrawing Consent: </span>
                Because we will provide the Disclosures to you instantaneously,
                you will not be able to withdraw your consent to do business
                electronically with us. However, you may withdraw your consent
                to do business electronically with our participating Lenders at
                no cost to you. You may do so by contacting the Lender at the
                mailing address, e-mail address or telephone number they provide
                with their offer. If you decide to withdraw your consent, the
                legal validity and enforceability of prior electronic
                Disclosures will not be affected.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">
                  Changes to Your Contact Information:{" "}
                </span>
                You should keep us informed of any change in your electronic or
                mailing address. You may contact Customer Care by telephone at
                833-262-8966 regarding any such changes.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-11 p-0">
              <p className="desc">
                <span className="label">
                  YOUR ABILITY TO ACCESS DISCLOSURES.{" "}
                </span>
                BY COMPLETING AND SUBMITTING YOUR REQUEST, YOU ACKNOWLEDGE THAT
                YOU CAN ACCESS THE ELECTRONIC DISCLOSURES IN THE DESIGNATED
                FORMATS DESCRIBED ABOVE.
              </p>
            </div>
          </div>
          <div className="row m-0 d-flex justify-content-start">
            <div className="col-1 p-0 point">
              <span className="bullet-point"></span>
            </div>
            <div className="col-10 p-0">
              <p className="desc">
                <span className="label">CONSENT. </span>
                BY COMPLETING AND SUBMITTING YOUR REQUEST, YOU CONSENT TO HAVING
                ALL DISCLOSURES PROVIDED OR MADE AVAILABLE TO YOU IN ELECTRONIC
                FORM AND TO DOING BUSINESS WITH US ELECTRONICALLY.
              </p>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(
                0deg,
                rgba(17, 39, 56, 0.7),
                rgba(17, 39, 56, 0.7)
              );
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: inherit !important;
            }
            .EleSignTerms_Sec {
              width: 70vw;
              position: relative;
              background: ${WHITE_COLOR} !important;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0.732vw;
            }
            .point {
              max-width: 1.464vw;
            }
            .bullet-point {
              display: block;
              margin: 0.366vw 0.732vw 0 0.366vw;
              width: 0.366vw;
              height: 0.366vw;
              border-radius: 50%;
              background: ${GREEN_COLOR};
            }
            .label {
              width: fit-content;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR_Dark};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0.219vw;
              line-height: 1.2;
            }
            .desc {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.029vw !important;
              margin: 0 0 0.585vw 0.219vw;
              line-height: 1.2;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EleSignTerms;
