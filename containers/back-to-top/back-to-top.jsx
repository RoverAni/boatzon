import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_1,
  Back_To_Top,
  LIGHT_THEME_BG,
} from "../../lib/config";
import { handleSlidetoTop } from "../../lib/global";

class BackToTop extends Component {
  render() {
    return (
      <Wrapper>
        <div className="col-12 backToTop_Sec">
          <div className="msgSec">
            <p className="msg" onClick={() => handleSlidetoTop()}>
              <img src={Back_To_Top}></img>
              <span>back to top</span>
            </p>
          </div>
        </div>
        <style jsx>
          {`
            .backToTop_Sec {
              position: relative;
              padding: 1.683vw 1.098vw;
              background: ${LIGHT_THEME_BG};
            }
            .msgSec {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
            }
            .msg {
              margin-bottom: 0;
              display: flex;
              cursor: pointer;
            }
            .msg img {
              width: 0.658vw;
              margin-right: 0.366vw;
            }
            .msg span {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0146vw !important;
            }
            span:first-letter {
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default BackToTop;
