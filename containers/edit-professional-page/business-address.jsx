import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  WHITE_COLOR,
  THEME_COLOR,
  GREY_VARIANT_2,
  GREEN_COLOR,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import SelectInput from "../../components/input-box/select-input";
import { getCountries, getStates } from "country-state-picker";
import LocationSearchInput from "../location/location-search-box";
import ButtonComp from "../../components/button/button";
import { editProProfile } from "../../services/professionals";
import Snackbar from "../../components/snackbar";

class BusinessAddress extends Component {
  state = {
    inputpayload: {},
    country: {
      value: "India",
      label: "India",
    },
    countryCode: "in",
  };

  componentDidMount() {
    const { location, city, state, country, zipcode, speciality } = {
      ...this.props.ProfileData,
    };
    let tempPayload = { ...this.state.inputpayload };
    tempPayload.city = city ? city : "";
    tempPayload.address_line = location ? location : "";
    tempPayload.zipCode = zipcode ? zipcode : "";
    let tempSpeciality = [...speciality];
    let tempselectedServicesValue =
      tempSpeciality &&
      tempSpeciality.map((data) => {
        data[`id`] = data[`nodeId`];
        delete data.nodeId;
        // delete data.ImageUrl;
        delete data.serviceId;
        return data;
      });
    this.setState(
      {
        country: country
          ? {
              value: country,
              label: country,
            }
          : {},
        stateName: state
          ? {
              value: state,
              label: state,
            }
          : {},
        inputpayload: { ...tempPayload },
        selectedSpeciality: [...tempselectedServicesValue],
      },
      () => {
        if (this.state.country == "United States") {
          this.setState({
            country: {
              value: "United States Of America",
              label: "United States Of America",
            },
          });
        }
        let countryCode = getCountries()
          .filter((data) => {
            return data.name.includes(this.state.country.value);
          })
          .map((item) => {
            return item.code;
          });
        this.setState({
          countryCode: countryCode[0].toLowerCase(),
        });
      }
    );
  }
  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
        [inputControl.name]: inputControl.value,
      },
      () => {
        console.log("PAYLOAD", this.state.inputpayload);
      }
    );
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    let temppayload = { ...this.state.inputpayload };
    temppayload[[name]] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...temppayload },
        [name]: {
          value: inputControl.value,
          label: inputControl.value,
        },
      },
      () => {
        if (name == "country") {
          let countryCode = getCountries()
            .filter((data) => {
              return data.name.includes(this.state.country.value);
            })
            .map((item) => {
              return item.code;
            });
          let tempObject = { ...this.state.inputpayload };
          tempObject.city = "";
          tempObject.address_line = "";
          tempObject.zipCode = "";
          this.setState({
            inputpayload: { ...tempObject },
            stateName: "",
            countryCode: countryCode[0].toLowerCase(),
          });
        }
      }
    );
  };

  updateUserAddress = (data) => {
    console.log("nficf", data);
    if (data) {
      this.setState(
        {
          address_line1: data.address,
          city: data.city,
          zipCode: data.zipCode,
          country: { value: data.country, label: data.country },
          isAddressSelected: true,
          latitude: data.lat,
          longitude: data.lng,
          countryShortName: data.countryShortName,
          stateName: "",
        },
        () => {
          if (this.state.country == "United States") {
            this.setState({
              country: {
                value: "United States of America",
                label: "United States of America",
              },
            });
          }
          let countryCode = getCountries()
            .filter((data) => {
              return data.name.includes(this.state.country.value);
            })
            .map((item) => {
              return item.code;
            });
          this.setState({
            countryCode: countryCode[0].toLowerCase(),
          });
          let tempPayload = { ...this.state.inputpayload };
          tempPayload["address_line"] = this.state.address_line1;
          tempPayload["city"] = this.state.city;
          tempPayload["zipCode"] = this.state.zipCode;
          tempPayload["country"] = this.state.country;
          tempPayload["latitude"] = this.state.latitude;
          tempPayload["longitude"] = this.state.longitude;

          let stateList = getStates(this.state.countryShortName.toLowerCase());
          this.setState({
            stateList,
            locationModel: false,
            inputpayload: { ...tempPayload },
          });
        }
      );
    } else {
      this.setState({ isAddressSelected: false });
    }
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 404:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  handleUpdateProAddress = () => {
    let payload = {
      email: this.props.ProfileData.email,
      username: this.props.ProfileData.username,
      businessName: this.props.ProfileData.businessName,
      bussinessStatus: this.props.ProfileData.bussinessStatus.toString(),
      websiteLink: this.props.ProfileData.websiteLink,
      latitude: this.state.inputpayload.latitude.toString(),
      longitude: this.state.inputpayload.longitude.toString(),
      address: this.state.inputpayload.address_line,
      country: this.state.country.value,
      state: this.state.stateName.value,
      city: this.state.inputpayload.city,
      zipcode: this.state.inputpayload.zipCode,
      aboutMe: this.props.ProfileData.aboutMe,
      award: this.props.ProfileData.award,
      businessLogo: this.props.ProfileData.businessLogo,
      speciality:
        this.state.selectedSpeciality &&
        this.state.selectedSpeciality
          .map((data) => {
            return data.name;
          })
          .toString(),
    };

    editProProfile(payload)
      .then((res) => {
        console.log("resProfile", res.data);
        let response = res.data;
        if (response) {
          // this.setState({
          //   usermessage: response.message,
          //   variant: this.handleSnackbar(response),
          //   open: true,
          //   vertical: "bottom",
          //   horizontal: "left",
          // });
          if (response.code == 200) {
            // window.location.reload();
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const Countrylist = getCountries().map((data) => ({
      value: data.name,
      label: data.name,
    }));

    const StateList = getStates(this.state.countryCode).map((data) => ({
      value: data,
      label: data,
    }));

    const { country, stateName } = this.state;

    const { city, address_line, zipCode, addressLine2 } = {
      ...this.state.inputpayload,
    };
    return (
      <Wrapper>
        <div className="BusinessAddressPage py-3 col-12 px-0">
          <h6 className="heading">Business Address</h6>
          <div className="row m-0 pt-3 pb-4">
            <div className="col-7 p-0">
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Country:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <SelectInput
                      editProProfile={true}
                      placeholder="Country"
                      options={Countrylist}
                      value={country}
                      onChange={this.handleOnSelectInput(`country`)}
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">State:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <SelectInput
                      editProProfile={true}
                      placeholder="Select State"
                      options={StateList}
                      value={stateName}
                      onChange={this.handleOnSelectInput(`stateName`)}
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">City:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <InputBox
                      type="text"
                      name="city"
                      value={city}
                      onChange={this.handleOnchangeInput}
                      className="EditinputBox"
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Zip Code:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <InputBox
                      type="text"
                      name="zipCode"
                      value={zipCode}
                      onChange={this.handleOnchangeInput}
                      className="EditinputBox"
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Address Line 1:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div className="FormInput">
                    <LocationSearchInput
                      autoFocus={true}
                      id="user-address-box"
                      address={address_line}
                      updateLocation={this.updateUserAddress}
                      handleChange={this.updateUserAddress}
                      showOnlyAddress={true}
                      showAddressFromProps={true}
                    ></LocationSearchInput>
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Address Line 2:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <InputBox
                      type="text"
                      name="addressLine2"
                      value={addressLine2}
                      onChange={this.handleOnchangeInput}
                      className="EditinputBox"
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-end">
                <div className="update_btn">
                  <ButtonComp onClick={this.handleUpdateProAddress}>
                    Update
                  </ButtonComp>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        <style jsx>
          {`
            .heading {
              font-size: 0.951vw;
              font-family: "Museo-Sans" !important;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .label {
              cursor: pointer;
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .label span {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_3};
            }
            .lightFont {
              font-family: "Open Sans" !important;
            }
            :global(.BusinessAddressPage .EditinputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: ${WHITE_COLOR};
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.BusinessAddressPage .EditinputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none;
            }
            :global(.BusinessAddressPage .EditinputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            :global(.BusinessAddressPage .react-select__placeholder) {
              font-size: 0.805vw;
            }
            :global(.BusinessAddressPage .react-select__input input) {
              font-size: 0.805vw;
            }
            :global(.BusinessAddressPage .react-select__control) {
              min-height: 2.342vw !important;
            }
            :global(.BusinessAddressPage .greyChevronIcon) {
              width: 0.732vw;
              margin-top: 0.219vw;
            }
            :global(.BusinessAddressPage .blueChevronIcon) {
              width: 0.512vw;
              margin-left: 0.366vw;
            }
            :global(.BusinessAddressPage .react-select__single-value) {
              font-size: 0.805vw;
            }
            :global(.BusinessAddressPage .react-select__option) {
              font-size: 0.805vw;
            }
            :global(.update_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0.732vw 0;
              padding: 0 1.83vw;
              height: 2.489vw;
              text-transform: initial;
              background: ${GREEN_COLOR};
              border-radius: 0.146vw;
              border: none;
              color: ${BG_LightGREY_COLOR};
              position: relative;
            }
            :global(.update_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.update_btn button:focus),
            :global(.update_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.update_btn button span) {
              font-size: 0.951vw;
              font-family: "Open Sans" !important;
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default BusinessAddress;
