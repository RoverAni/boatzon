import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import {
  GREY_VARIANT_2,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  THEME_COLOR,
  THEME_COLOR_Opacity,
  WHITE_COLOR,
  BG_LightGREY_COLOR,
  GREY_VARIANT_1,
  Edit_Icon_Grey,
  Password_Icon_White,
  Verified_Icon,
  NotVerified_Icon,
  Red_Color,
  Chevron_Down,
  Checked_Darkgrey_Icon,
  Search_Light_Grey_Icon,
  GREY_VARIANT_10,
  Delete_Grey_Icon,
  GREEN_COLOR,
} from "../../lib/config";
import SelectInput from "../../components/input-box/select-input";
import IntlTelInput from "react-intl-tel-input";
import {
  MOBILE,
  COUNTRY_CODE,
  WEBSITE,
  EMAIL,
} from "../../lib/input-control-data/input-definitions";
import { getCountry } from "country-state-picker";
import TextAreaBox from "../../components/input-box/text-area-input";
import dynamic from "next/dynamic";
import {
  forgotPassword,
  UserNameVerfication,
  EmailVerfication,
} from "../../services/auth";
import Snackbar from "../../components/snackbar";
import {
  newEmailCheck,
  newPhoneNumber,
  newPhoneNumberVerifyOtp,
} from "../../services/userProfile";
import { editProProfile } from "../../services/professionals";
import {
  requiredValidator,
  validatePhoneNumber,
} from "../../lib/validation/validation";
import Model from "../../components/model/model";
import VerifyOtp from "../my-account-page/verify-otp";
import cloneDeep from "lodash.clonedeep";
import { updatefollowManufacturers } from "../../services/manufacturer";

class GeneralInfo extends Component {
  state = {
    editUserName: false,
    editEmail: false,
    EmailVerified: false,
    inputpayload: {
      username: "Nate1982",
      Email: "Nate_cantalupo@hotmail.com",
      businessName: "Nate Cantalupo Company",
      professionalType: "Boat Dealer",
      Website: "www.businessname.com",
      about_proUser:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum, justo ut tempor condimentum, magna nunc laoreet magna, eleifend pulvinar lorem nisi vitae ex. Integer eleifend vitae nisl sit amet malesuada. Phasellus felis est, ornare non sem sed, sollicitudin convallis dui. Aliquam eget ullamcorper eros. Vestibulum imperdiet, ipsum vel scelerisque dapibus, risus ipsum fermentum purus, ac vulputate augue massa ut elit. In id est elit. Proin ac facilisis arcu. Vivamus consequat auctor risus, sit amet tempor risus feugiat vitae. Maecenas et lectus est. Etiam quis consequat libero. Mauris egestas risus et dolor pharetra euismod. Praesent eu fermentum purus, sed faucibus risus. Maecenas id lectus non tortor condimentum efficitur et eget lacus. Proin vitae fermentum tortor. Nulla ullamcorper aliquet urna, ac tempus erat tincidunt non. Mauris ante lorem, finibus quis nibh in, scelerisque tincidunt enim. Donec posuere nulla at tristique convallis. Praesent sodales hendrerit nunc imperdiet consequat. Donec et ligula eu massa viverra tempor id eu nibh. In hac habitasse platea dictumst. Phasellus ornare, arcu non porttitor aliquam, ex ex aliquam lacus, euismod aliquam sem ex sed sapien. Sed consectetur mattis diam et accumsan. Vivamus rutrum bibendum enim, id porta turpis lobortis at. Fusce dignissim id tellus in vestibulum. Duis molestie gna sit amet augue finibus blandit. Donec a dictum dui, quis pharetra lectus. Nulla imrdiet quam et",
    },
    phoneNumVerified: false,
    verifyOtpModel: false,
    countryShortName: "IN",
    selectedManufacturerValue: [{ name: "Alex Willis", id: 3 }],
    selectedSpeciality: [],
    usernameValid: true,
    usernameChecked: true,
    EmailVerified: true,
    preferredCountries: ["IN"],
    otpModel: false,
    PhoneVerified: true,
    isPhoneValid: true,
  };

  componentDidMount() {
    const {
      email,
      username,
      businessName,
      websiteLink,
      speciality,
      emailVerified,
      bussinessStatus,
      aboutMe,
      award,
    } = this.props.ProfileData;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload.businessName = businessName ? businessName : "";
    tempPayload.Website = websiteLink ? websiteLink : "";
    tempPayload.Email = email ? email : "";
    tempPayload.username = username ? username : "";
    tempPayload.professionalType =
      bussinessStatus && bussinessStatus == 1
        ? "Boat Dealer"
        : bussinessStatus && bussinessStatus == 2
        ? "Professional"
        : "";
    tempPayload.about_proUser = aboutMe;
    tempPayload.mobile = this.props.userProfileData
      ? this.props.userProfileData.phoneNumber.replace(
          `${this.props.userProfileData.countryCode}`,
          ""
        )
      : "";
    tempPayload.certification_award = award ? award : "";
    let tempSpeciality = [...speciality];
    let tempselectedServicesValue =
      tempSpeciality &&
      tempSpeciality.map((data) => {
        data[`id`] = data[`nodeId`];
        delete data.nodeId;
        // delete data.ImageUrl;
        delete data.serviceId;
        return data;
      });
    let tempPreferredCountries = [...this.state.preferredCountries];
    let countyName = getCountry(
      this.props.userProfileData &&
        this.props.userProfileData.countryCode &&
        this.props.userProfileData.countryCode != "undefined"
        ? this.props.userProfileData.countryCode
        : "+91"
    ).code.toUpperCase();
    tempPreferredCountries[0] = countyName;
    console.log("tempPreferredCountries", tempPreferredCountries);

    let tempselectedManufacturerValue =
      this.props.followedManufacturer &&
      this.props.followedManufacturer
        .filter((item) => {
          return item.status == 1;
        })
        .map((data) => {
          data[`id`] = data[`manufactureId`];
          data[`name`] = data[`manufactureName`];
          // delete data.ImageUrl;
          delete data.manufactureId;
          delete data.manufactureName;
          delete data.status;
          return data;
        });

    this.setState(
      {
        inputpayload: { ...tempPayload },
        EmailVerified: emailVerified,
        professionalType:
          bussinessStatus && bussinessStatus == 1
            ? {
                value: "Boat Dealer",
                label: "Boat Dealer",
              }
            : bussinessStatus && bussinessStatus == 2
            ? {
                value: "Professional",
                label: "Professional",
              }
            : {},
        preferredCountries: [...tempPreferredCountries],
        selectedSpeciality: [...tempselectedServicesValue],
        selectedManufacturerValue: [...tempselectedManufacturerValue],
      },
      () => {
        this.setState({
          initialSelectedManufacturer: [
            ...this.state.selectedManufacturerValue,
          ],
        });
      }
    );
    this.validatePhoneInput();
  }

  // Number Validation function for phoneInput
  validatePhoneInput = () => {
    validatePhoneNumber("phoneNum");
  };

  handleEditUserName = () => {
    this.setState({
      editUserName: true,
    });
  };

  handleEditEmail = () => {
    this.setState({
      editEmail: true,
    });
  };

  // Funtion for inputpayload
  handleOnchangeInput = (event) => {
    console.log("nfwhfu", event);
    if (event.target.name == "username") {
      this.setState({
        usernameChecked: false,
      });
    }
    console.log("nfwhfu", event.target.value);

    let inputControl = event.target;
    let validate = requiredValidator(inputControl);

    this.setState({ [inputControl.name]: validate });
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState(
      {
        inputpayload: { ...tempPayload },
      },
      () => {
        // this.checkIfFormValid();
        console.log("PAYLOAD", this.state.inputpayload);
      }
    );
  };

  // Function for inputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = this.state.inputpayload;
        temppayload[[name]] = [...this.state[name]];
        this.setState({ inputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.inputpayload };
      temppayload[[name]] = inputControl.value;
      this.setState({
        inputpayload: { ...temppayload },
        [name]: {
          value: inputControl.value,
          label: inputControl.value,
        },
      });
    }
  };

  // Function for the Phone Number Flag
  handleOnchangeflag = (num, country, fullNum, status) => {
    console.log("onSelectFlag", num, country.dialCode, fullNum, status);
    this.handleOnchangePhone(status, num, country);
  };

  // Function for inputpayload of phoneNumber-input
  handleOnchangePhone = (status, valuenumber, dialInfo) => {
    console.log("valuenumber", dialInfo);
    this.setState(
      {
        UserNumber: valuenumber,
        cCode: dialInfo.dialCode,
        isPhoneValid: status,
      },
      () => {
        console.log(
          "valuenumber",
          this.state.UserNumber,
          this.props.userProfileData.phoneNumber.replace("+91", "")
        );
        if (
          this.props.userProfileData.phoneNumber.replace("+91", "") !=
          this.state.UserNumber
        ) {
          if (this.state.isPhoneValid) {
            this.setState({
              PhoneVerified: false,
            });
          }
        }
        let tempPayload = { ...this.state.inputpayload };
        tempPayload[`${MOBILE}`] = this.state.UserNumber;
        tempPayload[`${COUNTRY_CODE}`] = "+" + this.state.cCode;
        this.setState({
          inputpayload: { ...tempPayload },
          [`${MOBILE}`]: this.state.UserNumber,
          [`${COUNTRY_CODE}`]: "+" + this.state.cCode,
        });
      }
    );
  };

  //   handlePhoneNumberCheck = () => {
  //     if (this.state.isPhoneValid) {
  //       let phoneNumberpayload = {
  //         phoneNumber:
  //           this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
  //       };
  //       PhoneNumberVerfication(phoneNumberpayload)
  //         .then((res) => {
  //           let response = res.data;
  //           if (response) {
  //             this.setState(
  //               {
  //                 mobileChecked: true,
  //                 mobileErrMsg: response.message,
  //                 mobileErrMsgClr: this.handleErrMsgColor(response),
  //                 mobileValid: response.code == 200 ? true : false,
  //               },
  //               () => {
  //                 let tempPayload = { ...this.state.inputpayload };
  //                 tempPayload[`mobileValid`] = this.state.mobileValid;
  //                 this.setState(
  //                   {
  //                     inputpayload: { ...tempPayload },
  //                   },
  //                   () => {
  //                     this.checkIfFormValid();
  //                   }
  //                 );
  //               }
  //             );
  //           }
  //         })
  //         .catch((err) => {
  //           console.log("err", err, err.message);
  //           this.setState(
  //             {
  //               mobileErrMsg: err.message,
  //               mobileErrMsgClr: `${Red_Color}`,
  //               mobileValid: false,
  //             },
  //             () => {
  //               let tempPayload = { ...this.state.inputpayload };
  //               tempPayload[`mobileValid`] = this.state.mobileValid;
  //               this.setState(
  //                 {
  //                   inputpayload: { ...tempPayload },
  //                 },
  //                 () => {
  //                   this.checkIfFormValid();
  //                 }
  //               );
  //             }
  //           );
  //         });
  //     }
  //   };

  // Function will trigger on select event
  handleManufacturerOnSelect = (selectedList, selectedItem) => {
    console.log("selectedItem", selectedList, selectedItem);
    let selectedValue = [...this.state.selectedManufacturerValue];
    selectedValue.push(selectedItem);
    this.setState({
      selectedManufacturerValue: selectedValue,
    });
  };

  // Function will trigger on remove event
  handleManufacturerOnRemove = (selectedList, removedItem) => {
    console.log("removedItem", selectedList, removedItem);
    let selectedValue = [...this.state.selectedManufacturerValue];
    var removeIndex = selectedValue
      .map((item) => {
        return item.id;
      })
      .indexOf(removedItem.id);
    selectedValue.splice(removeIndex, 1);
    this.setState({
      selectedManufacturerValue: selectedValue,
    });
  };

  handleSpecialityOnSelect = (selectedList, selectedItem) => {
    console.log("selectedItem", selectedList, selectedItem);
    let selectedValue = [...this.state.selectedSpeciality];
    selectedValue.push(selectedItem);
    this.setState(
      {
        selectedSpeciality: selectedValue,
      },
      () => {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload["speciality"] = [...this.state.selectedSpeciality];
        this.setState({
          inputpayload: cloneDeep(tempPayload).__wrapped__,
        });
      }
    );
  };
  handleSpecialityOnRemove = (selectedList, removedItem) => {
    console.log("removedItem", selectedList, removedItem);
    let selectedValue = [...this.state.selectedSpeciality];
    var removeIndex = selectedValue
      .map((item) => {
        return item.id;
      })
      .indexOf(removedItem.id);
    if (removeIndex != -1) {
      selectedValue.splice(removeIndex, 1);
    }
    this.setState(
      {
        selectedSpeciality: selectedValue,
      },
      () => {
        let tempPayload = { ...this.state.inputpayload };
        tempPayload["speciality"] = [...this.state.selectedSpeciality];
        this.setState({
          inputpayload: cloneDeep(tempPayload).__wrapped__,
        });
      }
    );
  };

  handleSnackbar = (response) => {
    switch (response.code) {
      case 200:
        return "success";
        break;
      case 204:
        return "error";
        break;
      case 400:
        return "warning";
        break;
      case 404:
        return "warning";
        break;
      case 401:
        return "warning";
        break;
    }
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleChangePassword = () => {
    let payload = {
      type: "0", // type --> 0-email,1-phoneNumber
      email: this.state.inputpayload.Email,
    };
    forgotPassword(payload)
      .then((res) => {
        console.log("res---->", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      })
      .catch((err) => {
        console.log("err--->", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleNewEmailCheck = () => {
    let payload = {
      email: this.state.inputpayload.Email,
    };
    newEmailCheck(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleUpdateBusinessProfile = () => {
    let payload = {
      email: this.state.inputpayload.Email,
      username: this.state.inputpayload.username,
      businessName: this.state.inputpayload.businessName,
      bussinessStatus:
        this.state.professionalType == "Professional" ? "2" : "1",
      websiteLink: this.state.inputpayload.Website,
      latitude: this.props.ProfileData.latitude.toString(),
      longitude: this.props.ProfileData.longitude.toString(),
      address: this.props.ProfileData.location,
      country: this.props.ProfileData.country,
      state: this.props.ProfileData.state,
      city: this.props.ProfileData.city,
      zipcode: this.props.ProfileData.zipcode,
      aboutMe: this.state.inputpayload.about_proUser,
      award: this.state.inputpayload.certification_award,
      businessLogo: this.props.ProfileData.businessLogo,
      speciality:
        this.state.selectedSpeciality &&
        this.state.selectedSpeciality
          .map((data) => {
            return data.name;
          })
          .toString(),
    };

    editProProfile(payload)
      .then((res) => {
        console.log("resProfile", res.data);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message,
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            // window.location.reload();
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });

    if (
      this.state.initialSelectedManufacturer !=
      this.state.selectedManufacturerValue
    ) {
      let manufacturerPayload = {
        manufactor:
          this.state.selectedManufacturerValue &&
          this.state.selectedManufacturerValue
            .map((data) => {
              return data.id;
            })
            .toString(),
      };

      updatefollowManufacturers(manufacturerPayload)
        .then((res) => {
          console.log("resManufacturer", res);
          let response = res.data;
          if (response) {
            this.setState({
              usermessage: response.message,
              variant: this.handleSnackbar(response),
              open: true,
              vertical: "bottom",
              horizontal: "left",
            });
            if (response.code == 200) {
              // window.location.reload();
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
        });
    }
  };

  handleCloseEmailEdit = () => {
    this.setState({
      editUserName: false,
    });
  };

  handleErrMsgColor = (response) => {
    switch (response.code) {
      case 200:
        return `${GREEN_COLOR}`;
        break;
      case 409:
        return `${Red_Color}`;
        break;
    }
  };

  handleUserNameCheck = () => {
    if (this.state.username != this.props.ProfileData.username) {
      let userNamepayload = {
        username: this.state.inputpayload.username,
      };
      UserNameVerfication(userNamepayload)
        .then((res) => {
          console.log("res--->", res);
          let response = res.data;
          if (response) {
            this.setState({
              usernameChecked: true,
              usernameErrMsg: response.message,
              usernameErrMsgClr: this.handleErrMsgColor(response),
              usernameValid: response.code == 200 ? true : false,
            });
          }
        })
        .catch((err) => {
          console.log("err", err, err.message);
          this.setState({
            usernameErrMsg: err.message,
            usernameErrMsgClr: `${Red_Color}`,
            usernameValid: false,
          });
        });
    } else if (this.state.username == this.props.ProfileData.username) {
      this.setState({
        usernameChecked: true,
      });
    }
  };

  handleEmailCheck = () => {
    if (this.state[EMAIL] == 1) {
      let payload = {
        email: this.state.inputpayload.Email || this.state.inputpayload.email,
      };
      EmailVerfication(payload)
        .then((res) => {
          let response = res.data;
          if (response) {
            this.setState({
              emailChecked: true,
              emailErrMsg: response.message,
              emailErrMsgClr: this.handleErrMsgColor(response),
              emailValid: response.code == 200 ? true : false,
            });
          }
        })
        .catch((err) => {
          console.log("err", err, err.message);
          this.setState({
            emailErrMsg: err.message,
            emailErrMsgClr: `${Red_Color}`,
            emailValid: false,
          });
        });
    }
  };

  handlePhoneNumberVerify = () => {
    let payload = {
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };
    console.log("newPhoneNum Payload", payload);
    newPhoneNumber(payload)
      .then((res) => {
        console.log("phoneRes", res.data);
        let response = res.data;
        if (response) {
          this.setState({
            usermessage: response.message.toString(),
            variant: this.handleSnackbar(response),
            open: true,
            vertical: "bottom",
            horizontal: "left",
          });
          if (response.code == 200) {
            this.setState({
              otpModel: true,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  handleCloseOtpModel = () => {
    this.setState({
      otpModel: false,
    });
  };

  handleVerifyOtp = (otp) => {
    let payload = {
      otp: otp,
      phoneNumber:
        this.state.inputpayload.countryCode + this.state.inputpayload.mobile,
    };

    newPhoneNumberVerifyOtp(payload)
      .then((res) => {
        console.log("res", res);
        let response = res.data;
        if (response.code == 200) {
          this.setState(
            {
              PhoneVerified: true,
            },
            () => {
              this.handleCloseOtpModel();
            }
          );
        }
      })
      .catch((err) => {
        console.log("err", err);
        this.setState({
          usermessage: err.message,
          variant: "error",
          open: true,
          vertical: "bottom",
          horizontal: "left",
        });
      });
  };

  render() {
    const {
      username,
      Email,
      businessName,
      Website,
      about_proUser,
      certification_award,
    } = {
      ...this.state.inputpayload,
    };
    const { professionalType } = this.state;

    const ProfessionalType = [
      { value: "Professional", label: "Professional" },
      { value: "Boat Dealer", label: "Boat Dealer" },
    ];

    const Multiselect = dynamic(
      () => import("multiselect-react-dropdown").then((mod) => mod.Multiselect),
      {
        ssr: false,
      }
    );

    const specialityOptions =
      this.props.specialityList &&
      this.props.specialityList.map((data) => ({
        name: data.name,
        id: data.nodeId,
      }));

    const manufacturerOptions = [
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
      { value: "manu1", label: "manu1" },
    ];

    const { ProfileData } = this.props;
    console.log("ProfileData", ProfileData);
    return (
      <Wrapper>
        <div className="GeneralInfoPage py-3 col-12 px-0">
          <h6 className="heading">Account Information</h6>
          <div className="row m-0 pt-3 pb-4">
            <div className="col-7 p-0">
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label">User Name:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    {this.state.editUserName ? (
                      <Wrapper>
                        {/* <OutsideAlerter onClose={this.handleCloseEmailEdit}> */}
                        <div className="position-relative">
                          <InputBox
                            type="text"
                            name="username"
                            value={username}
                            onChange={this.handleOnchangeInput}
                            onBlur={this.handleUserNameCheck}
                            className="EditinputBox"
                          />
                          {this.state.usernameChecked ? (
                            this.state.usernameValid &&
                            this.state.inputpayload.username ? (
                              <img
                                src={Verified_Icon}
                                className="verifiedIcon"
                              ></img>
                            ) : (
                              <img
                                src={NotVerified_Icon}
                                className="notVerifiedIcon"
                              ></img>
                            )
                          ) : (
                            ""
                          )}
                        </div>
                        {this.state.usernameChecked ? (
                          <p
                            className="errMessage"
                            style={{ color: this.state.usernameErrMsgClr }}
                          >
                            {this.state.usernameErrMsg}
                          </p>
                        ) : (
                          ""
                        )}
                        {/* </OutsideAlerter> */}
                      </Wrapper>
                    ) : (
                      <div className="position-relative">
                        <InputBox
                          readOnly
                          type="text"
                          value={username}
                          className="inputBox form-control"
                        />
                        {this.state.usernameChecked ? (
                          this.state.usernameValid &&
                          this.state.inputpayload.username ? (
                            <img
                              src={Verified_Icon}
                              className="verifiedIcon"
                            ></img>
                          ) : (
                            <img
                              src={NotVerified_Icon}
                              className="notVerifiedIcon"
                            ></img>
                          )
                        ) : (
                          ""
                        )}
                      </div>
                    )}
                  </div>
                </div>
                <div className="col-2 p-0">
                  {this.state.editUserName ? (
                    ""
                  ) : (
                    <div className="edit_btn">
                      <ButtonComp onClick={this.handleEditUserName}>
                        <img src={Edit_Icon_Grey} className="editIcon"></img>
                        Edit
                      </ButtonComp>
                    </div>
                  )}
                </div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label">
                      Email <span>(private):</span>
                    </p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    {this.state.editEmail ? (
                      <div>
                        <div className="position-relative">
                          <InputBox
                            type="text"
                            name={EMAIL}
                            value={Email}
                            onChange={this.handleOnchangeInput}
                            className="EditinputBox"
                          />
                          {this.state.EmailVerified ? (
                            <img
                              src={Verified_Icon}
                              className="verifiedIcon"
                            ></img>
                          ) : (
                            <img
                              src={NotVerified_Icon}
                              className="notVerifiedIcon"
                            ></img>
                          )}
                        </div>
                        {this.state[EMAIL] == 0 || this.state[EMAIL] == 2 ? (
                          <p
                            className="errMessage"
                            style={{ color: `${Red_Color}` }}
                          >
                            Enter Valid Email
                          </p>
                        ) : (
                          ""
                        )}
                      </div>
                    ) : (
                      <div className="position-relative">
                        <InputBox
                          readOnly
                          type="text"
                          value={Email}
                          className="inputBox form-control"
                        />
                        {this.state.EmailVerified ? (
                          <img
                            src={Verified_Icon}
                            className="verifiedIcon"
                          ></img>
                        ) : (
                          <img
                            src={NotVerified_Icon}
                            className="notVerifiedIcon"
                          ></img>
                        )}
                      </div>
                    )}
                  </div>
                </div>
                <div className="col-2 p-0">
                  {this.state.editEmail ? (
                    ""
                  ) : (
                    <div className="edit_btn">
                      <ButtonComp onClick={this.handleEditEmail}>
                        <img src={Edit_Icon_Grey} className="editIcon"></img>
                        Edit
                      </ButtonComp>
                    </div>
                  )}
                </div>
              </div>
              <div className="row m-0">
                <div className="col-3 p-0"></div>
                <div className="col-7 p-0 px-2">
                  <div className="position-relative p-2">
                    {this.state.EmailVerified ? (
                      " "
                    ) : (
                      <p
                        className="verify_btn"
                        onClick={this.handleNewEmailCheck}
                      >
                        Verify
                      </p>
                    )}
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0"></div>
                <div className="col-7 p-0 px-2">
                  <div className="changePass_btn">
                    <ButtonComp onClick={this.handleChangePassword}>
                      <img
                        src={Password_Icon_White}
                        className="passwordIcon"
                      ></img>
                      Change Password
                    </ButtonComp>
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
            </div>
          </div>
          <h6 className="heading">General Business Information</h6>
          <div className="row m-0 pt-3 pb-4">
            <div className="col-7 p-0">
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Business Name:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <InputBox
                      type="text"
                      name="businessName"
                      value={businessName}
                      onChange={this.handleOnchangeInput}
                      className="EditinputBox"
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Professional Type:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div>
                    <SelectInput
                      editProProfile={true}
                      placeholder="Professional Type"
                      options={ProfessionalType}
                      value={professionalType}
                      onChange={this.handleOnSelectInput(`professionalType`)}
                    />
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-12 p-0">
                  <div className="row m-0 align-items-center">
                    <div className="col-3 p-0">
                      <div>
                        <p className="label lightFont">Phone Number:</p>
                      </div>
                    </div>
                    <div className="col-7 p-0 px-2">
                      <div className="PhoneInput">
                        <div className="position-relative">
                          <IntlTelInput
                            key={this.state.timestamp}
                            preferredCountries={this.state.preferredCountries}
                            containerClassName="intl-tel-input"
                            value={this.state.inputpayload.mobile}
                            onSelectFlag={this.handleOnchangeflag}
                            onPhoneNumberChange={this.handleOnchangePhone}
                            formatOnInit={false}
                            separateDialCode={true}
                            fieldId="phoneNum"
                            autoComplete="off"
                          />
                          {this.state.isPhoneValid ? (
                            this.state.PhoneVerified ? (
                              <img
                                src={Verified_Icon}
                                className="verifiedIcon"
                              ></img>
                            ) : (
                              <img
                                src={NotVerified_Icon}
                                className="notVerifiedIcon"
                              ></img>
                            )
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="col-2 p-0"></div>
                  </div>
                </div>
                <div className="col-12 p-0">
                  <div className="row m-0">
                    <div className="col-3 p-0"></div>
                    <div className="col-7 p-0 px-2">
                      <div className="position-relative">
                        {this.state.isPhoneValid === false ? (
                          <p
                            className="errMessage"
                            style={{ color: `${Red_Color}` }}
                          >
                            Phone Number not valid
                          </p>
                        ) : (
                          ""
                        )}
                        {this.state.PhoneVerified ? (
                          " "
                        ) : (
                          <p
                            className="phoneVerify_btn"
                            onClick={this.handlePhoneNumberVerify}
                          >
                            Verify
                          </p>
                        )}
                      </div>
                    </div>
                    <div className="col-2 p-0"></div>
                  </div>
                </div>
              </div>

              <div className="row m-0 align-items-center justify-content-center pb-2">
                <div className="col-3 p-0">
                  <div>
                    <p className="label lightFont">Business Website:</p>
                  </div>
                </div>
                <div className="col-7 p-0 px-2">
                  <div className="position-relative">
                    <InputBox
                      type="text"
                      name={WEBSITE}
                      value={Website}
                      onChange={this.handleOnchangeInput}
                      className="EditinputBox"
                    />
                    {this.state[WEBSITE] == 4 ? (
                      <p
                        className="errMessage"
                        style={{ color: `${Red_Color}` }}
                      >
                        Enter Valid Website Link
                      </p>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                <div className="col-2 p-0"></div>
              </div>
            </div>
          </div>

          <h6 className="heading border-0">Business Description:</h6>
          <div className="row m-0 pt-2 pb-4">
            <div className="col-12 p-0">
              <div className="mb-3">
                <TextAreaBox
                  type="textarea"
                  id="about"
                  className="textareaBox form-control"
                  name="about_proUser"
                  value={about_proUser}
                  onChange={this.handleOnchangeInput}
                  autoComplete="off"
                ></TextAreaBox>
              </div>
              <div className="py-2">
                <p className="label lightFont mb-2">
                  Certification and Awards:
                </p>
                <InputBox
                  type="text"
                  name="certification_award"
                  value={certification_award}
                  onChange={this.handleOnchangeInput}
                  className="EditinputBox"
                />
              </div>
            </div>
          </div>

          <h6 className="heading border-0">Services You Provide</h6>
          <div className="row m-0 pt-1 pb-4">
            <div className="col-12 p-0">
              <div className="SelectInput">
                <img src={Search_Light_Grey_Icon} className="searchIcon"></img>
                <Multiselect
                  options={specialityOptions}
                  selectedValues={this.state.selectedSpeciality}
                  onSelect={this.handleSpecialityOnSelect}
                  onRemove={this.handleSpecialityOnRemove}
                  displayValue="name"
                  showCheckbox={true}
                  closeIcon="cancel"
                />
              </div>
            </div>
          </div>

          <h6 className="heading border-0">Dealer for Boat Manufacturers</h6>
          <div className="row m-0 pt-1 pb-4">
            <div className="col-12 p-0">
              <div className="SelectInput">
                <img src={Search_Light_Grey_Icon} className="searchIcon"></img>
                <Multiselect
                  options={manufacturerOptions}
                  selectedValues={this.state.selectedManufacturerValue}
                  onSelect={this.handleManufacturerOnSelect}
                  onRemove={this.handleManufacturerOnRemove}
                  displayValue="name"
                  showCheckbox={true}
                  closeIcon="cancel"
                />
              </div>
            </div>
          </div>
          <div className="row m-0 align-items-center justify-content-between">
            <div className="removePro_btn">
              <ButtonComp>
                <img src={Delete_Grey_Icon} className="deleteIcon"></img>
                Remove Professional Profile
              </ButtonComp>
            </div>
            <div className="update_btn">
              <ButtonComp onClick={this.handleUpdateBusinessProfile}>
                Update
              </ButtonComp>
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={this.state.vertical}
          horizontal={this.state.horizontal}
        />
        {/* Login Model */}
        <Model
          open={this.state.otpModel}
          onClose={this.handleCloseOtpModel}
          authmodals={true}
        >
          <VerifyOtp
            onClose={this.handleCloseOtpModel}
            handleVerifyOtp={this.handleVerifyOtp}
            inputpayload={this.state.inputpayload}
          />
        </Model>

        <style jsx>
          {`
            .heading {
              font-size: 0.951vw;
              font-family: "Museo-Sans" !important;
              text-transform: capitalize;
              color: ${FONTGREY_COLOR};
              margin: 0;
              padding: 0.732vw 0;
              border-bottom: 0.073vw solid ${GREY_VARIANT_3};
            }
            .notVerifiedIcon {
              width: 1.024vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .verifiedIcon {
              width: 1.024vw;
              position: absolute;
              top: 50%;
              transform: translate(0, -50%);
              right: 0.732vw;
            }
            .phoneVerify_btn {
              float: right;
              padding: 0.512vw 0 0 0;
              background: transparent;
              width: fit-content;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.732vw;
              margin: 0;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_1};
              cursor: pointer;
            }
            .verify_btn {
              position: absolute;
              right: 0.366vw;
              top: 50%;
              transform: translate(0, -50%);
              background: transparent;
              width: fit-content;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.732vw;
              margin: 0;
              letter-spacing: 0.021vw !important;
              color: ${GREY_VARIANT_1};
              cursor: pointer;
            }
            .label {
              cursor: pointer;
              font-size: 0.805vw;
              text-transform: capitalize;
              font-family: "Open Sans-SemiBold" !important;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .label span {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_3};
            }
            .lightFont {
              font-family: "Open Sans" !important;
            }
            :global(.GeneralInfoPage .EditinputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: ${WHITE_COLOR};
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.GeneralInfoPage .EditinputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none;
            }
            :global(.GeneralInfoPage .EditinputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            .errMessage {
              color: red;
              font-size: 0.732vw;
              text-align: left;
              margin: 0;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              padding: 0.366vw 0 0 0;
            }
            :global(.GeneralInfoPage .inputBox) {
              display: block;
              width: 100%;
              height: 2.342vw;
              padding: 0.439vw 0.878vw !important;
              line-height: 1.5;
              background-color: ${BG_LightGREY_COLOR};
              background-clip: padding-box;
              border: none !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.GeneralInfoPage .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${BG_LightGREY_COLOR};
              border-color: ${THEME_COLOR};
              outline: 0 !important;
              box-shadow: none !important;
            }
            :global(.GeneralInfoPage .inputBox::placeholder) {
              color: ${GREY_VARIANT_2};
              font-size: 0.805vw;
            }
            .editIcon {
              width: 0.732vw;
              margin-right: 0.219vw;
            }
            .passwordIcon {
              width: 0.732vw;
              margin-right: 0.732vw;
              margin-bottom: 0.219vw;
            }

            :global(.changePass_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0;
              height: 2.489vw;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.366vw 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.changePass_btn button span) {
              font-family: "Museo-Sans" !important;
              width: fit-content;
              font-weight: normal;
              font-size: 0.878vw;
              line-height: 1;
            }
            :global(.changePass_btn button:focus),
            :global(.changePass_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.changePass_btn button:hover) {
              background: ${THEME_COLOR};
            }
            :global(.edit_btn button) {
              width: 100%;
              min-width: 100%;
              padding: 0;
              height: 2.342vw;
              background: ${BG_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.edit_btn button span) {
              font-family: "Open Sans" !important;
              width: fit-content;
              font-weight: normal;
              font-size: 0.805vw;
              line-height: 1;
            }
            :global(.edit_btn button:focus),
            :global(.edit_btn button:active) {
              background: ${BG_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.edit_btn button:hover) {
              background: ${BG_LightGREY_COLOR};
            }
            :global(.GeneralInfoPage .react-select__placeholder) {
              font-size: 0.805vw;
            }
            :global(.GeneralInfoPage .react-select__input input) {
              font-size: 0.805vw;
            }
            :global(.GeneralInfoPage .react-select__control) {
              min-height: 2.342vw !important;
            }
            :global(.GeneralInfoPage .greyChevronIcon) {
              width: 0.732vw;
              margin-top: 0.219vw;
            }
            :global(.GeneralInfoPage .blueChevronIcon) {
              width: 0.512vw;
              margin-left: 0.366vw;
            }
            :global(.GeneralInfoPage .react-select__single-value) {
              font-size: 0.805vw;
            }
            :global(.GeneralInfoPage .react-select__option) {
              font-size: 0.805vw;
            }

            :global(.GeneralInfoPage .PhoneInput) {
              position: relative;
            }
            :global(.GeneralInfoPage .PhoneInput > div > div) {
              width: 100%;
              display: flex;
              align-items: center;
              justify-content: space-between;
              border: none;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              align-items: center;
              height: 2.342vw;
              padding: 0;
            }
            :global(.GeneralInfoPage .PhoneInput > div > div > input) {
              width: 100%;
              height: 100%;
              line-height: 1 !important;
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              padding: 0 0.366vw !important;
              margin-left: 0.366vw !important;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .country-list) {
              width: 14.641vw !important;
              margin: 0.219vw 0 0 0;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .country-list
                .country) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
            :global(.GeneralInfoPage .PhoneInput > div > div > input:focus) {
              color: ${FONTGREY_COLOR};
              background-color: ${WHITE_COLOR} !important;
              border-color: none;
              outline: 0;
              box-shadow: none;
            }
            :global(.flag-container) {
              height: 2.342vw;
              position: relative !important;
              width: fit-content !important;
              display: block ruby !important;
              padding: 0 !important;
            }
            :global(.flag-container:hover .selected-flag) {
              background: ${WHITE_COLOR} !important;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag) {
              width: 100% !important;
              background: ${WHITE_COLOR} !important;
              height: 100%;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus),
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:active) {
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code) {
              font-size: 0.805vw;
              height: 2.122vw;
              align-items: center;
              display: flex;
              padding: 0 0 0 0.219vw;
              line-height: 1 !important;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code::after) {
              content: "${this.state.countryShortName}" url(${Chevron_Down});
              border: none !important;
              border-right: none !important;
              border-left: none !important;
              margin: 0 0.219vw !important;
              position: relative;
              text-transform: uppercase;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:focus) {
              box-shadow: none !important;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .selected-dial-code:focus) {
              border: none !important;
              box-shadow: none !important;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag) {
              background: ${WHITE_COLOR};
              width: fit-content !important;
              padding: 0;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag:hover) {
              background: ${WHITE_COLOR};
            }

            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-flag) {
              display: none;
            }
            :global(.GeneralInfoPage
                .PhoneInput
                > div
                > div
                div
                .selected-flag
                .iti-arrow) {
              display: none;
            }
            :global(.GeneralInfoPage .PhoneInput > div > div div:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }

            :global(.GeneralInfoPage .textareaBox) {
              display: block;
              width: 100%;
              height: 14.641vw;
              padding: 0.732vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.146vw !important;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              overflow-y: scroll;
              scrollbar-width: thin !important;
              scrollbar-color: #c4c4c4 #f5f5f5;
            }
            :global(.GeneralInfoPage .textareaBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.textareaBox::placeholder) {
              font-size: 0.805vw;
              color: ${GREY_VARIANT_3};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            .searchIcon {
              width: 0.878vw;
              position: absolute;
              top: 1.024vw;
              left: 0.732vw;
              z-index: 2;
              margin-right: 0.732vw;
            }

            .SelectInput {
              height: auto;
              position: relative;
            }
            :global(.SelectInput
                #multiselectContainerReact
                .optionListContainer) {
              position: relative;
              display: block !important;
            }
            :global(.SelectInput
                #multiselectContainerReact
                .optionListContainer
                .optionContainer) {
              border: none !important;
            }
            :global(.SelectInput
                #multiselectContainerReact
                .optionListContainer
                .option) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              padding: 0.366vw 0.732vw;
              background: ${WHITE_COLOR} !important;
              display: flex;
              align-items: center;
            }
            :global(.SelectInput
                #multiselectContainerReact
                .optionListContainer
                .option
                .checkbox) {
              -webkit-appearance: none;
              border: 0.073vw solid ${GREY_VARIANT_3};
              padding: 0.439vw;
              border-radius: 0.146vw;
              display: inline-block;
              position: relative;
            }
            :global(.SelectInput
                #multiselectContainerReact
                .optionListContainer
                .option
                .checkbox:checked:after) {
              content: url(${Checked_Darkgrey_Icon});
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
            :global(.SelectInput
                #multiselectContainerReact
                .optionListContainer
                .option:hover) {
              background: ${THEME_COLOR_Opacity} !important;
              color: ${THEME_COLOR} !important;
            }
            :global(.SelectInput
                #multiselectContainerReact
                .Dv7FLAST-3bZceA5TIxEN) {
              display: block !important;
            }
            :global(.SelectInput #multiselectContainerReact) {
              background: ${WHITE_COLOR};
              border: 0.073vw solid ${GREY_VARIANT_3} !important;
              border-radius: 0.292vw;
            }
            :global(.SelectInput
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI) {
              border: none !important;
              padding: 0 0.366vw;
              border-radius: 0 !important;
              margin: 0 0.366vw;
              border-bottom: 0.073vw solid ${GREY_VARIANT_10} !important;
            }
            :global(.SelectInput
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI
                input) {
              margin: 0.366vw 0;
            }
            :global(.SelectInput
                #multiselectContainerReact
                ._2OR24XnUXt8OCrysr3G0XI
                > :first-of-type) {
              margin-left: 1.464vw !important;
            }
            :global(.SelectInput #multiselectContainerReact > div .searchBox) {
              margin-top: 0 !important;
              font-size: 0.805vw !important;
              width: 10.98vw !important;
              max-width: 10.98vw !important;
              padding: 0.366vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.SelectInput
                #multiselectContainerReact
                > div
                .searchBox::placeholder) {
              font-size: 0.805vw !important;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
            }
            :global(.SelectInput #multiselectContainerReact > div .chip) {
              background: ${GREY_VARIANT_3} !important;
              color: ${GREY_VARIANT_1} !important;
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              border-radius: 0.146vw !important;
              font-size: 0.805vw !important;
              padding: 0.292vw 0.585vw !important;
              margin-top: 0.366vw;
            }

            :global(.removePro_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0;
              padding: 0 0.878vw;
              height: 2.489vw;
              text-transform: initial;
              background: ${BG_LightGREY_COLOR};
              border-radius: 0.146vw;
              border: none;
              color: ${GREY_VARIANT_1};
              position: relative;
            }
            :global(.removePro_btn button:hover) {
              background: ${BG_LightGREY_COLOR};
            }
            :global(.removePro_btn button:focus),
            :global(.removePro_btn button:active) {
              background: ${BG_LightGREY_COLOR};
              outline: none;
            }
            :global(.removePro_btn button span) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              text-transform: capitalize;
            }
            .deleteIcon {
              width: 0.732;
              margin-right: 0.366vw;
            }

            :global(.update_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0;
              padding: 0 1.83vw;
              height: 2.489vw;
              text-transform: initial;
              background: ${GREEN_COLOR};
              border-radius: 0.146vw;
              border: none;
              color: ${BG_LightGREY_COLOR};
              position: relative;
            }
            :global(.update_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.update_btn button:focus),
            :global(.update_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
            }
            :global(.update_btn button span) {
              font-size: 0.951vw;
              font-family: "Open Sans" !important;
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(GeneralInfo);
