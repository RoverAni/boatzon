import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  WHITE_COLOR,
  FONTGREY_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR_Opacity,
  THEME_COLOR,
  Acc_Settings_Blue,
  Acc_Settings_Grey,
  Payment_Methods_Blue,
  Payment_Methods_Grey,
  Navigation_Icon_Grey,
  Navigation_Icon_Blue,
  Close_White_Icon,
  BG_LightGREY_COLOR,
} from "../../lib/config";
import StarRating from "../../components/rating/star-rating";
import Tabs from "../../components/tabs/tabs";
import GeneralInfo from "./general-info";
import BusinessAddress from "./business-address";
import ButtonComp from "../../components/button/button";

class EditProfessionalPage extends Component {
  state = {
    activeLink: "general-info",
  };

  // On Component load this called
  componentDidMount = () => {
    // let slugData = getUrlSlugs();
    // this.setState({
    //   activeLink: slugData[slugData.length - 1],
    //   openProfile: true,
    //   slugData: slugData,
    // });
  };

  // Used to get Current Slug
  handelTabClick = (slug) => {
    // console.log("asasdasdasd", slug);
    // Router.push(`/my-account/${slug}`);

    this.setState({
      activeLink: slug,
    });
  };

  render() {
    let pageContent = (
      <GeneralInfo
        ProfileData={this.props.ProfileData}
        specialityList={this.props.specialityList}
        followedManufacturer={this.props.followedManufacturer}
      />
    );

    switch (this.state.activeLink) {
      case "general-info":
        pageContent = (
          <GeneralInfo
            ProfileData={this.props.ProfileData}
            specialityList={this.props.specialityList}
            followedManufacturer={this.props.followedManufacturer}
          />
        );
        break;
      case "business-address":
        pageContent = <BusinessAddress ProfileData={this.props.ProfileData} />;
        break;
      case "billing-info":
        pageContent = <h1>billing-info</h1>;
        break;
      default:
        pageContent = (
          <GeneralInfo
            ProfileData={this.props.ProfileData}
            specialityList={this.props.specialityList}
            followedManufacturer={this.props.followedManufacturer}
          />
        );
    }
    const { ProfileData } = this.props;
    let userLocation = ProfileData.location.split(",");

    return (
      <Wrapper>
        <div className="row m-0 ProfessionalUserProfile_ContentSec">
          <div className="cancel_btn">
            <ButtonComp onClick={this.props.handleBusinessProProfile}>
              <img src={Close_White_Icon} className="cancelIcon"></img>
              Cancel
            </ButtonComp>
          </div>
          <div className="col-md-3 LeftSec col-lg-2 px-2">
            <img src={ProfileData.businessLogo} className="profileDp"></img>
            <h6 className="heading">Business Account</h6>
            <ul className="list-unstyled AccountTabs">
              <li
                className={
                  "general-info" === this.state.activeLink ? "activeLink" : " "
                }
                onClick={this.handelTabClick.bind(this, "general-info")}
              >
                <div className="row m-0">
                  <div className="col-2 p-0 iconSec">
                    {"general-info" === this.state.activeLink ? (
                      <img src={Acc_Settings_Blue} className="accIcon"></img>
                    ) : (
                      <img src={Acc_Settings_Grey} className="accIcon"></img>
                    )}
                  </div>
                  <div className="col p-0">
                    <span
                      className={
                        "general-info" === this.state.activeLink
                          ? "activeLabel tabLabel"
                          : "tabLabel"
                      }
                    >
                      General Info
                    </span>
                  </div>
                </div>
              </li>
              <li
                className={
                  "business-address" === this.state.activeLink
                    ? "activeLink"
                    : " "
                }
                onClick={this.handelTabClick.bind(this, "business-address")}
              >
                <div className="row m-0">
                  <div className="col-2 p-0 iconSec">
                    {"business-address" === this.state.activeLink ? (
                      <img
                        src={Navigation_Icon_Blue}
                        className="navigationIcon"
                      ></img>
                    ) : (
                      <img
                        src={Navigation_Icon_Grey}
                        className="navigationIcon"
                      ></img>
                    )}
                  </div>
                  <div className="col p-0">
                    <span
                      className={
                        "business-address" === this.state.activeLink
                          ? "activeLabel tabLabel"
                          : "tabLabel"
                      }
                    >
                      Business Address
                    </span>
                  </div>
                </div>
              </li>
              <li
                className={
                  "billing-info" === this.state.activeLink ? "activeLink" : " "
                }
                onClick={this.handelTabClick.bind(this, "billing-info")}
              >
                <div className="row m-0">
                  <div className="col-2 p-0 iconSec">
                    {"billing-info" === this.state.activeLink ? (
                      <img
                        src={Payment_Methods_Blue}
                        className="paymentIcon"
                      ></img>
                    ) : (
                      <img
                        src={Payment_Methods_Grey}
                        className="paymentIcon"
                      ></img>
                    )}
                  </div>
                  <div className="col p-0">
                    <span
                      className={
                        "billing-info" === this.state.activeLink
                          ? "activeLabel tabLabel"
                          : "tabLabel"
                      }
                    >
                      Billing Info
                    </span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div className="col-md col-lg-10 py-1">
            <h2 className="userName">{ProfileData.businessName}</h2>
            <div className="d-flex align-items-center">
              <StarRating
                value={4}
                startFontSize={12}
                disableHover={true}
                readOnly={true}
              />
              <span className="reviwesNum">19 reviews</span>
            </div>
            <span className="professionalUserLocation">
              {userLocation[userLocation.length - 3] +
                "," +
                userLocation[userLocation.length - 1]}
            </span>
            <div className="my-4 TabPanel">
              <Tabs
                normalsmalltabs={true}
                editProProfile={true}
                noValue=""
                tabs={[
                  {
                    label: `Boats`,
                  },
                  {
                    label: `Products`,
                  },
                  {
                    label: `Services`,
                  },
                  {
                    label: `Reviews`,
                  },
                  {
                    label: `Photos`,
                  },
                  {
                    label: `Directions`,
                  },
                ]}
                tabcontent={[
                  { content: "" },
                  { content: "" },
                  { content: "" },
                  { content: "" },
                  { content: "" },
                  { content: "" },
                ]}
              />
              {pageContent}
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .profileDp {
              width: 100%;
              border-radius: 0.292vw;
              object-fit: cover;
              height: 10.98vw;
            }
            .ProfessionalUserProfile_ContentSec {
              position: relative;
              top: -5.856vw;
              z-index: 1;
              width: 100%;
            }
            .cancelIcon {
              width: 0.658vw;
              margin-right: 0.439vw;
            }
            .cancel_btn {
              position: absolute;
              top: 0;
              right: 0.732vw;
              z-index: 1;
            }
            :global(.cancel_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: transparent;
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.021vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.cancel_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.021vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.cancel_btn button:focus),
            :global(.cancel_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.cancel_btn button:hover) {
              background: transparent;
            }

            .userName {
              color: ${WHITE_COLOR};
              font-size: 1.317vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              margin-bottom: 0.366vw;
            }
            .heading {
              color: ${FONTGREY_COLOR};
              font-size: 1.098vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.021vw !important;
              margin: 0;
              padding: 0.732vw 0;
            }
            .reviwesNum {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              margin-bottom: 0;
              margin-left: 0.585vw;
              text-transform: capitalize;
            }
            .professionalUserLocation {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.021vw !important;
              margin-bottom: 0;
              text-transform: capitalize;
            }

            .AccountTabs {
              //   position: sticky;
              //   top: 73px;
            }
            .AccountTabs > li {
              padding: 0.292vw 0.439vw;
            }
            .tabLabel {
              cursor: pointer;
              font-size: 0.805vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
            }
            .activeLink {
              background: ${THEME_COLOR_Opacity};
            }
            .activeLabel {
              color: ${THEME_COLOR} !important;
              font-family: "OpenSans-Bold" !important;
            }
            .accIcon {
              width: 0.732vw;
            }
            .paymentIcon {
              width: 0.951vw;
            }
            .navigationIcon {
              width: 0.878vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default EditProfessionalPage;
