import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import ProductCard from "../post-card/product-card";
import CustomLink from "../../components/Link/Link";
import {
  FONTGREY_COLOR,
  Wishlist_Icon,
  Saved_Blue_Icon,
} from "../../lib/config";
import { getCurrentLocation } from "../../redux/actions/location/location";

class ProfessionalBoatPage extends Component {
  state = {
    inputpayload: {},
    selectedManufacturerNames: [],
    selectedBoatsType: [],
    priceDropdown: false,
    locationModel: false,
    locationDropdown: false,
    lengthDropdown: false,
    lengthValue: [20, 50],
    minLength: "",
    maxLength: "",
  };

  handleWishlistPost = (id) => {
    let srcImg = document
      .getElementById(id)
      .src.replace("http://localhost:7122/", "/");
    if (srcImg == `${Wishlist_Icon}`) {
      document.getElementById(id).src = `${Saved_Blue_Icon}`;
    } else if (srcImg == `${Saved_Blue_Icon}`) {
      document.getElementById(id).src = `${Wishlist_Icon}`;
    }
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.currentLocation != this.props.currentLocation) {
      this.setState(
        {
          currentLocation: prevProps.currentLocation,
        },
        () => {
          this.state.mylocation
            ? this.handleMylocationModel()
            : this.state.nationWide
            ? this.handleNationWide()
            : "";
        }
      );
    }
    if (prevProps.filterPostData != this.props.filterPostData) {
      this.setState(
        {
          BoatsList: this.props.filterPostData,
        },
        () => {
          this.setState({
            openpage: true,
          });
        }
      );
    }
  };

  handleTypeOnSelect = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        if (name == "selectedBoatsType") {
          apiPayload[`subCategory`] = this.state[name]
            .toString()
            .replace(/,/g, " OR ");
        } else if (this.state.condition == "All Conditions") {
          apiPayload[`${name}`] = "";
        } else if (name == "hasWarranty") {
          apiPayload[`${name}`] =
            this.state.hasWarranty == "All Options"
              ? ""
              : this.state.hasWarranty == "With a Warranty"
              ? "Yes"
              : this.state.hasWarranty == "No Warranty"
              ? "No"
              : null;
        } else {
          apiPayload[`${name}`] = this.state[name];
        }
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  handleSelectedManufacturerList = (SelectedManufacturer) => {
    let tempmanufacturePayload = [...this.state.selectedManufacturers];
    let elementPosition = tempmanufacturePayload.findIndex((data) => {
      return data.manufactureName === SelectedManufacturer.manufactureName;
    });
    elementPosition == -1
      ? tempmanufacturePayload.push(SelectedManufacturer)
      : tempmanufacturePayload.splice(elementPosition, 1);
    this.setState(
      {
        selectedManufacturers: tempmanufacturePayload,
      },
      () => {
        this.setState(
          {
            selectedManufacturerNames:
              this.state.selectedManufacturers &&
              this.state.selectedManufacturers.map((data) => {
                return data.manufactureName;
              }),
            selectedManufacturerIds:
              this.state.selectedManufacturers &&
              this.state.selectedManufacturers.map((data) => {
                return data.manufactureId;
              }),
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`manufactureId`] =
              this.state.selectedManufacturerIds.toString();
            console.log("ndsjcfjdk", apiPayload);
            this.setState(
              {
                payload: apiPayload,
              },
              () => {
                // this.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  priceDropdownOpen = () => {
    if (!this.state.minPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.minPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    if (!this.state.maxPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.maxPrice;
      this.setState({
        inputpayload: temp,
      });
    }
    this.setState({
      priceDropdown: !this.state.priceDropdown,
    });
  };

  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    console.log("hfh", inputControl.value);
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  handlePriceRange = () => {
    let { minPrice, maxPrice } = this.state.inputpayload;
    this.setState(
      {
        minPrice,
        maxPrice,
        priceDropdown: !this.state.priceDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`priceRange`] = {};
        apiPayload[`priceRange`].fromPrice = this.state.minPrice;
        apiPayload[`priceRange`].toPrice = this.state.maxPrice;
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  locationDropdownOpen = () => {
    this.setState({
      locationDropdown: !this.state.locationDropdown,
    });
  };

  handleMylocationModelClose = () => {
    this.setState({
      locationModel: false,
    });
  };

  handleMylocationModel = () => {
    this.setState(
      {
        mylocation: true,
      },
      () => {
        this.state.currentLocation
          ? this.setState({
              locationModel: true,
            })
          : this.props.dispatch(getCurrentLocation());
      }
    );
  };

  handleNationWide = () => {
    this.setState(
      {
        nationWide: true,
      },
      () => {
        this.state.currentLocation
          ? this.setState({
              countryShortName: this.state.currentLocation.countryShortName,
            })
          : this.props.dispatch(getCurrentLocation());
      }
    );
  };

  updateUserAddress = (data) => {
    console.log("nficf", data);
    if (data) {
      this.setState(
        {
          address_line1: data.address,
          city: data.city,
          zipCode: data.zipCode,
          country: data.country,
          isAddressSelected: true,
          countryShortName: data.countryShortName,
        },
        () => {
          this.setState({
            locationModel: false,
          });
        }
      );
    } else {
      this.setState({ isAddressSelected: false });
    }
  };

  lengthDropdownOpen = () => {
    if (this.state.lengthDropdown == true) {
      this.setState({
        minLength: 20,
        maxLength: 50,
        lengthValue: [20, 50],
      });
    }
    this.setState({
      lengthDropdown: !this.state.lengthDropdown,
    });
  };

  handleLengthRange = () => {
    this.setState(
      {
        lengthDropdown: !this.state.lengthDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`lengthlength`] = {};
        apiPayload[`lengthlength`].fromLength = this.state.minLength.toString();
        apiPayload[`lengthlength`].toLength = this.state.maxLength.toString();
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  handleSliderLegthRange = (event, newValue) => {
    this.setState(
      {
        lengthValue: newValue,
      },
      () => {
        this.handleMinMaxValues(this.state.lengthValue);
      }
    );
  };

  handleMinMaxValues = (value) => {
    let minLength = value[0];
    let maxLength = value[1];
    this.setState({
      minLength,
      maxLength,
    });
  };

  handleOnchangeLengthInput = (name) => (event) => {
    let inputControl = event.target;
    if (name == "minLength") {
      this.setState(
        {
          maxLength: 50,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minLength, this.state.maxLength];
          this.setState({
            lengthValue: newValue,
          });
        }
      );
    } else if (name == "maxLength") {
      this.setState(
        {
          minLength: 20,
          [name]: inputControl.value,
        },
        () => {
          let newValue = [this.state.minLength, this.state.maxLength];
          this.setState({
            lengthValue: newValue,
          });
        }
      );
    }
  };

  render() {
    const { BoatPost } = this.props;
    return (
      <Wrapper>
        <div className="col-12 p-0 pt-3">
          {/* <FilterSec
            boatspage={true}
            onlyFilters={true}
            handleTypeOnSelect={this.handleTypeOnSelect}
            inputpayload={this.state.inputpayload}
            selectedManufacturerNames={this.state.selectedManufacturerNames}
            BoatsSubCategoriesList={this.props.BoatsSubCategoriesList}
            selectedBoatsType={this.state.selectedBoatsType}
            condition={this.state.condition}
            priceDropdownOpen={this.priceDropdownOpen}
            handleOnchangeInput={this.handleOnchangeInput}
            handlePriceRange={this.handlePriceRange}
            priceDropdown={this.state.priceDropdown}
            hasWarranty={this.state.hasWarranty}
            locationDropdownOpen={this.locationDropdownOpen}
            locationDropdown={this.state.locationDropdown}
            handleMylocationModel={this.handleMylocationModel}
            currentLocation={this.state.currentLocation}
            handleMylocationModelClose={this.handleMylocationModelClose}
            locationModel={this.state.locationModel}
            handleNationWide={this.handleNationWide}
            updateUserAddress={this.updateUserAddress}
            lengthDropdownOpen={this.lengthDropdownOpen}
            lengthDropdown={this.state.lengthDropdown}
            handleLengthRange={this.handleLengthRange}
            handleSliderLegthRange={this.handleSliderLegthRange}
            lengthValue={this.state.lengthValue}
            handleOnchangeLengthInput={this.handleOnchangeLengthInput}
            maxLength={this.state.maxLength}
            minLength={this.state.minLength}
          /> */}
          <h6 className="heading">Boats for Sale:</h6>
          <div className="row m-0 my-2">
            {BoatPost &&
              BoatPost.map((item, index) =>
                index <= 1 ? (
                  <div key={index} className="col-md-4 col-lg-3 p-0 boatCard">
                    <CustomLink href="/boat-detail">
                      <a target="_blank">
                        <ProductCard
                          Productdetails={item}
                          // wishlist={true}
                          professionalpage={true}
                        />
                      </a>
                    </CustomLink>
                    {/* <img
                      src={Wishlist_Icon}
                      id={index}
                      className="wishlistIcon"
                      onClick={this.handleWishlistPost.bind(this, index)}
                    ></img> */}
                  </div>
                ) : (
                  ""
                )
              )}
          </div>
        </div>
        <style jsx>
          {`
            .heading {
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0219vw !important;
              text-transform: initial;
              margin: 1.464vw 0 0.732vw 0;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .wishlistIcon {
              position: absolute;
              bottom: 0.732vw;
              right: 0.732vw;
              width: 1.024vw;
            }
            .boatCard {
              cursor: pointer;
              max-width: 11.713vw;
              min-height: 13.909vw;
              box-shadow: 0 0px 6px 0 rgba(48, 56, 97, 0.1);
              margin-right: 0.878vw !important;
              position: relative;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    filterPostData: state.filterPostData,
    BoatsSubCategoriesList: state.BoatsSubCategoriesList,
  };
};

export default connect(mapStateToProps)(ProfessionalBoatPage);
