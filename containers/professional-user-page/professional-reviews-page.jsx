import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  FONTGREY_COLOR,
  GREY_VARIANT_1,
  Seller_1,
  THEME_COLOR,
  GREY_VARIANT_3,
  GREY_VARIANT_2,
  Chevron_Down_Green,
  GREEN_COLOR,
  Seller_5,
  Profile_Logo,
  BG_LightGREY_COLOR,
  BOX_SHADOW_GREY,
} from "../../lib/config";
import StarRating from "../../components/rating/star-rating";
import ButtonComp from "../../components/button/button";

class ProfessionalReviewsPage extends Component {
  render() {
    return (
      <Wrapper>
        <div className="col-12 p-0 pt-3">
          <h6 className="heading">34 Reviews for Nate Cantalupo:</h6>
          <div className="row m-0 ReviewSec">
            <div className="col-4 p-0 customerInfo">
              <div className="row m-0">
                <div className="col-auto pl-0 pr-md-1">
                  <img src={Seller_1} className="customerDp"></img>
                </div>
                <div className="col-7 p-0">
                  <h6 className="customerName">Jennifer Randall</h6>
                  <StarRating value={4} disableHover={true} readOnly={true} />
                </div>
              </div>
              <div className="d-flex align-items-center">
                <div className="like_btn">
                  <ButtonComp>Like</ButtonComp>
                </div>
                <p className="reviewDate">March 12, 2020</p>
              </div>
            </div>
            <div className="col p-0">
              <p className="reviewDesc">
                Ullamco elit excepteur officia aute aute laborum eiusmod mollit
                irure ea. Non eu aliquip proident excepteur Lorem voluptate
                laborum. Quis adipisicing sunt pariatur culpa amet qui qui elit
                aute aliqua. Adipisicing mollit sit incididunt elit voluptate
                esse deserunt enim adipisicing tempor enim minim nostrud
                exercitation.
              </p>
              <div className="readMore_btn">
                <ButtonComp>
                  Read More{" "}
                  <img src={Chevron_Down_Green} className="chevronIcon"></img>
                </ButtonComp>
              </div>
            </div>
          </div>
          <div className="row m-0 ReviewSec">
            <div className="col-4 p-0 customerInfo">
              <div className="row m-0">
                <div className="col-auto pl-0 pr-md-1">
                  <img src={Profile_Logo} className="customerDp"></img>
                </div>
                <div className="col-7 p-0">
                  <h6 className="customerName">Jennifer Randall</h6>
                  <StarRating value={4} disableHover={true} readOnly={true} />
                </div>
              </div>
              <div className="d-flex align-items-center">
                <div className="like_btn">
                  <ButtonComp>Like</ButtonComp>
                </div>
                <p className="reviewDate">March 12, 2020</p>
              </div>
            </div>
            <div className="col p-0">
              <p className="reviewDesc">
                Ullamco elit excepteur officia aute aute laborum eiusmod mollit
                irure ea. Non eu aliquip proident excepteur Lorem voluptate
                laborum. Quis adipisicing sunt pariatur culpa amet qui qui elit
                aute aliqua. Adipisicing mollit sit incididunt elit voluptate
                esse deserunt enim adipisicing tempor enim minim nostrud
                exercitation.
              </p>
              {/* <div className="readMore_btn">
                <ButtonComp>
                  Read More{" "}
                  <img src={Chevron_Down_Green} className="chevronIcon"></img>
                </ButtonComp>
              </div> */}
            </div>
          </div>
          <div className="row m-0 ReviewSec">
            <div className="col-4 p-0 customerInfo">
              <div className="row m-0">
                <div className="col-auto pl-0 pr-md-1">
                  <img src={Seller_5} className="customerDp"></img>
                </div>
                <div className="col-7 p-0">
                  <h6 className="customerName">Jennifer Randall</h6>
                  <StarRating value={4} disableHover={true} readOnly={true} />
                </div>
              </div>
              <div className="d-flex align-items-center">
                <div className="like_btn">
                  <ButtonComp>Like</ButtonComp>
                </div>
                <p className="reviewDate">March 12, 2020</p>
              </div>
            </div>
            <div className="col p-0">
              <p className="reviewDesc">
                Ullamco elit excepteur officia aute aute laborum eiusmod mollit
                irure ea. Non eu aliquip proident excepteur Lorem voluptate
                laborum. Quis adipisicing sunt pariatur culpa amet qui qui elit
                aute aliqua. Adipisicing mollit sit incididunt elit voluptate
                esse deserunt enim adipisicing tempor enim minim nostrud
                exercitation.
              </p>
              <div className="readMore_btn">
                <ButtonComp>
                  Read More{" "}
                  <img src={Chevron_Down_Green} className="chevronIcon"></img>
                </ButtonComp>
              </div>
            </div>
          </div>
          <div className="loadAllReviews_btn">
            <ButtonComp>Load All Reviews</ButtonComp>
          </div>
        </div>
        <style jsx>
          {`
            .ReviewSec {
              padding: 0.878vw 0;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .customerInfo {
              max-width: 16.837vw;
            }
            .heading {
              font-size: 1.094vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0219vw !important;
              text-transform: initial;
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .reviewDesc {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0;
              color: ${FONTGREY_COLOR};
            }
            .reviewDate {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 0;
              padding: 0 0.585vw;
              font-family: "Open Sans" !important;
            }
            .customerDp {
              width: 2.196vw !important;
              object-fit: cover;
              margin-right: 0.366vw;
            }
            .customerName {
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 0.219vw;
              font-family: "Open Sans-Semibold" !important;
            }
            :global(.like_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0;
              padding: 0 0.585vw;
              text-transform: initial;
              background: none;
              border-radius: 0;
              border-right: 0.0732vw solid ${GREY_VARIANT_2};
              color: ${THEME_COLOR};
              position: relative;
            }
            :global(.like_btn button:hover) {
              background: none;
            }
            :global(.like_btn button:focus),
            :global(.like_btn button:active) {
              background: none;
              outline: none;
            }
            :global(.like_btn button span) {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
            }
            :global(.readMore_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0;
              padding: 0;
              text-transform: initial;
              background: none;
              border-radius: 0;
              border: none;
              color: ${GREEN_COLOR};
              position: relative;
            }
            :global(.readMore_btn button:hover) {
              background: none;
            }
            :global(.readMore_btn button:focus),
            :global(.readMore_btn button:active) {
              background: none;
              outline: none;
            }
            :global(.readMore_btn button span) {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
            }
            .chevronIcon {
              width: 0.512vw;
              margin-left: 0.366vw;
            }
            :global(.loadAllReviews_btn button) {
              width: fit-content;
              min-width: fit-content;
              margin: 0.585vw 0;
              padding: 0.439vw 1.464vw;
              text-transform: initial;
              background: ${BG_LightGREY_COLOR};
              border-radius: 0;
              border: none;
              box-shadow: 0px 0px 2px 0px ${BOX_SHADOW_GREY} !important;
              color: ${GREY_VARIANT_1};
              position: relative;
            }
            :global(.loadAllReviews_btn button:hover) {
              background: ${BG_LightGREY_COLOR};
              box-shadow: 0px 0px 2px 0px ${BOX_SHADOW_GREY} !important;
            }
            :global(.loadAllReviews_btn button:focus),
            :global(.loadAllReviews_btn button:active) {
              background: ${BG_LightGREY_COLOR};
              outline: none;
              box-shadow: 0px 0px 2px 0px ${BOX_SHADOW_GREY} !important;
            }
            :global(.loadAllReviews_btn button span) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProfessionalReviewsPage;
