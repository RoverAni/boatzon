import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Professional_User_Banner,
  Email_White_Icon,
  WHITE_COLOR,
  GREEN_COLOR,
  Orange_Color,
  Review_Icon,
  Follow_Grey_Icon,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  Facebook_Blue_Icon,
  BG_LightGREY_COLOR,
  Twitter_Blue_Icon,
  GREY_VARIANT_2,
  Edit_Icon,
  GREY_VARIANT_3,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import StarRating from "../../components/rating/star-rating";
import Tabs from "../../components/tabs/tabs";
import ProfessionalBoatPage from "./professional-boat-page";
import ProfessionalProductsPage from "./professional-products-page";
import ProfessionalServicesPage from "./professional-services-page";
import ProfessionalReviewsPage from "./professional-reviews-page";
import ProfessionalDirectionPage from "./professional-direction-page";
import EditProfessionalPage from "../edit-professional-page/edit-professional-page";
import ProfessionalGalleryPage from "./professional-gallery-page";
import { getProfessionals } from "../../services/professionals";
import { getFollowManufactures } from "../../services/manufacturer";
import { getOwnPosts } from "../../services/my-listing";

class ProfessionalUserPage extends Component {
  state = {
    openPage: false,
    allPosts: [],
  };

  componentDidMount() {
    this.setState({
      openPage: true,
    });
    this.handleGetSpecialityList();
    this.handleGetFollowManufacturer();
    this.getAllPosts();
  }

  getAllPosts = async () => {
    let res = await getOwnPosts({ limit: 20, offset: 0 });
    this.setState({ allPosts: res.data.data });
    console.log("{ all posts }", res.data.data);
  };

  handleGetSpecialityList = () => {
    getProfessionals()
      .then((res) => {
        console.log("res", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              specialityList: response.data,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleGetFollowManufacturer = () => {
    getFollowManufactures()
      .then((res) => {
        console.log("res", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              followedManufacturer: response.data,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err");
      });
  };

  // Function for changing screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleEditProProfile = () => {
    this.updateScreen(
      <EditProfessionalPage
        ProfileData={this.props.ProfileData}
        handleBusinessProProfile={this.handleBusinessProProfile}
        specialityList={this.state.specialityList}
        followedManufacturer={this.state.followedManufacturer}
      />
    );
  };

  handleBusinessProProfile = () => {
    this.updateScreen();
  };
  render() {
    let data;
    if (this.props.isDynamicBusinessProfile) {
      let [dyData] = this.props && this.props.dynamicBusinessProfileData && this.props.dynamicBusinessProfileData.data;
      data = dyData;
    }
    const Boats = <ProfessionalBoatPage BoatPost={this.props.boatsPost} />;
    const Products = <ProfessionalProductsPage ProductPost={this.state.allPosts} />;
    const Services = <ProfessionalServicesPage />;
    const Reviews = <ProfessionalReviewsPage />;
    const Photos = <ProfessionalGalleryPage proUserGallery={this.props.proUserGallery || (data && data.postImageData)} />;
    const Directions = <ProfessionalDirectionPage />;

    const { ProfileData } = this.props;
    let userLocation;
    if (this.props.isDynamicBusinessProfile) {
      userLocation = data.location.split(",");
    } else {
      userLocation = ProfileData && ProfileData.location.split(",");
    }

    return (
      <Wrapper>
        {this.state.openPage ? (
          <Wrapper>
            <div className="ProfessionalUserProfile_BannerSec"></div>
            <div className="container p-md-0">
              <div className="screenWidth mx-auto p-0">
                {!this.state.currentScreen ? (
                  <div className="row m-0 ProfessionalUserProfile_ContentSec">
                    <div className="edit_btn">
                      <ButtonComp onClick={this.handleEditProProfile}>
                        <img src={Edit_Icon} className="editIcon"></img>
                        Edit
                      </ButtonComp>
                    </div>
                    <div className="col-md-3 LeftSec col-lg-2 px-2">
                      <img
                        src={(data && data.businessLogo) || (data && data.profilePicUrl) || ProfileData.businessLogo}
                        className="profileDp"
                      ></img>
                      <div className="contactMe_btn">
                        <ButtonComp>
                          <img src={Email_White_Icon} className="contactMeIcon"></img>
                          Contact Me
                        </ButtonComp>
                      </div>
                      <div className="review_btn">
                        <ButtonComp>
                          <img src={Review_Icon} className="reviewIcon"></img>
                          Write Review
                        </ButtonComp>
                      </div>
                      <div className="follow_btn">
                        <ButtonComp>
                          <img src={Follow_Grey_Icon} className="followIcon"></img>
                          follow
                        </ButtonComp>
                      </div>
                      <div className="d-flex align-items-center justify-content-between boderTop">
                        <div className="col-6 p-0 media_btn">
                          <ButtonComp>
                            <img src={Facebook_Blue_Icon} className="facebookIcon"></img>
                            Share
                          </ButtonComp>
                        </div>
                        <div className="col-6 p-0 media_btn text-right">
                          <ButtonComp>
                            <img src={Twitter_Blue_Icon} className="twitterIcon"></img>
                            Tweet
                          </ButtonComp>
                        </div>
                      </div>

                      <div className="py-2 align-items-center professionalUser_Sec">
                        <p className="label">Specialization:</p>
                        <ul className="list-unstyled d-flex align-items-center flex-wrap specializationList">
                          {ProfileData.speciality && ProfileData.speciality.map((data, index) => <li key={index}>{data.name}</li>)}
                        </ul>
                      </div>
                    </div>
                    <div className="col-md col-lg-10 py-1">
                      <h2 className="userName">{(data && data.businessName) || ProfileData.businessName}</h2>
                      <div className="d-flex align-items-center">
                        <StarRating
                          value={(data && data.ratedByCount) || ProfileData.ratedByCount || 0}
                          startFontSize={12}
                          disableHover={true}
                          readOnly={true}
                        />
                        <span className="reviwesNum">{(data && data.totalRating) || ProfileData.totalRating || 0} reviews</span>
                      </div>
                      <span className="professionalUserLocation">
                        {userLocation[userLocation.length - 3] + "," + userLocation[userLocation.length - 1]}
                      </span>
                      <div className="my-4">
                        <Tabs
                          normalsmalltabs={true}
                          tabs={[
                            {
                              label: `Boats`,
                            },
                            {
                              label: `Products`,
                            },
                            {
                              label: `Services`,
                            },
                            {
                              label: `Reviews`,
                            },
                            {
                              label: `Photos`,
                            },
                            {
                              label: `Directions`,
                            },
                          ]}
                          tabcontent={[
                            { content: Boats },
                            { content: Products },
                            { content: Services },
                            { content: Reviews },
                            { content: Photos },
                            { content: Directions },
                          ]}
                        />
                      </div>
                    </div>
                  </div>
                ) : (
                  this.state.currentScreen
                )}
              </div>
            </div>
          </Wrapper>
        ) : (
          ""
        )}

        <style jsx>
          {`
            .ProfessionalUserProfile_BannerSec {
              background-image: url(${Professional_User_Banner});
              min-height: 10.98vw;
              background-repeat: no-repeat;
              background-size: cover;
              position: relative;
            }
            .profileDp {
              width: 100%;
              border-radius: 0.292vw;
              object-fit: cover;
              height: 10.98vw;
            }
            .editIcon {
              width: 0.658vw;
              margin-right: 0.439vw;
            }
            .edit_btn {
              position: absolute;
              top: 0;
              right: 0.732vw;
              z-index: 1;
            }
            :global(.edit_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: transparent;
              color: ${BG_LightGREY_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.edit_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.edit_btn button:focus),
            :global(.edit_btn button:active) {
              background: transparent;
              outline: none;
              box-shadow: none;
            }
            :global(.edit_btn button:hover) {
              background: transparent;
            }

            :global(.contactMe_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.585vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.contactMe_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.contactMe_btn button:focus),
            :global(.contactMe_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.contactMe_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .contactMeIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 0.805vw;
            }
            :global(.review_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Orange_Color};
              color: ${WHITE_COLOR};
              margin: 0.585vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.review_btn button span) {
              font-family: "Museo-Sans-Cyrl" !important;
              letter-spacing: 0.0292vw !important;
              font-weight: 600;
              font-size: 0.805vw;
            }
            :global(.review_btn button:focus),
            :global(.review_btn button:active) {
              background: ${Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.review_btn button:hover) {
              background: ${Orange_Color};
            }
            .reviewIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.585vw;
              width: 0.805vw;
            }
            :global(.follow_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.585vw 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.follow_btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.805vw;
            }
            :global(.follow_btn button:focus),
            :global(.follow_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.follow_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .followIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.585vw;
              width: 0.658vw;
            }
            :global(.media_btn button) {
              width: 95%;
              padding: 0.219vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_1};
              border: 0.0732vw solid ${GREY_VARIANT_3};
              margin: 0.585vw 0;
              text-transform: capitalize;
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.658vw;
              position: relative;
            }
            :global(.media_btn button span) {
              font-family: "Museo-Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.658vw;
            }
            :global(.media_btn button:focus),
            :global(.media_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.media_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .boderTop {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .facebookIcon {
              margin-right: 0.512vw;
              margin-bottom: 0.146vw;
              width: 0.512vw;
            }
            .twitterIcon {
              margin-right: 0.512vw;
              width: 0.805vw;
              margin-bottom: 0.146vw;
            }
            .professionalUser_Sec .label {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
            }
            .specializationList li {
              background: ${BG_LightGREY_COLOR};
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              padding: 0.292vw 0.732vw;
              font-size: 0.658vw;
              margin-right: 0.366vw;
              margin-top: 0.439vw;
              cursor: pointer;
            }
            .ProfessionalUserProfile_ContentSec {
              position: relative;
              top: -5.856vw;
              z-index: 1;
              width: 100%;
            }
            .userName {
              color: ${WHITE_COLOR};
              font-size: 1.317vw;
              text-transform: capitalize;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0.366vw;
            }
            .reviwesNum {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              margin-left: 0.585vw;
              text-transform: capitalize;
            }
            .professionalUserLocation {
              font-size: 0.805vw;
              color: ${WHITE_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              margin-bottom: 0;
              text-transform: capitalize;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProfessionalUserPage;
