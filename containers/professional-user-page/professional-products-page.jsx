import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import ProductCard from "../post-card/product-card";
import CustomLink from "../../components/Link/Link";
import {
  FONTGREY_COLOR,
  Wishlist_Icon,
  Saved_Blue_Icon,
} from "../../lib/config";
import ProductsFilterSec from "../filter-sec/products-filter-sec";
// redux component
import Context from "../../context/context";

class ProfessionalProductPage extends Component {
  state = {
    openpage: false,
    selectedProductType: [],
    selectedProduct: [],
    selectedProductManufacturerType: [],
    inputpayload: {},
    priceDropdown: false,
    ProductTypes: [],
    radiusDropdown: false,
    postedinputpayload: {},
    payload: {
      category: "Products",
    },
    selectedProductNames: [],
  };

  handleWishlistPost = (id) => {
    let srcImg = document
      .getElementById(id)
      .src.replace("http://localhost:7122/", "/");
    if (srcImg == `${Wishlist_Icon}`) {
      document.getElementById(id).src = `${Saved_Blue_Icon}`;
    } else if (srcImg == `${Saved_Blue_Icon}`) {
      document.getElementById(id).src = `${Wishlist_Icon}`;
    }
  };

  // handlePostProductsAPI = (payload) => {
  //   this.props.dispatch(getFilterPost(payload));
  // };

  handleSelectedProductList = (selectedProduct) => {
    let temppayload = [...this.state.selectedProduct];
    let elementIndex = temppayload.findIndex((data) => {
      return data.subCategoryName === selectedProduct.subCategoryName;
    });
    elementIndex == -1
      ? temppayload.push(selectedProduct)
      : temppayload.splice(elementIndex, 1);

    this.setState(
      {
        selectedProduct: { ...temppayload },
      },
      () => {
        let ProductTypePayload =
          this.state.selectedProduct &&
          this.state.selectedProduct.length &&
          this.state.selectedProduct.map((data) => {
            return data.filter && data.filter.length
              ? data.filter[0].values.split(",")
              : [];
          });
        let tempPayload = ProductTypePayload
          ? ProductTypePayload.length > 1
            ? ProductTypePayload[ProductTypePayload.length - 1].concat(
                this.state.ProductTypes
              )
            : ProductTypePayload[ProductTypePayload.length - 1]
          : "";
        this.setState(
          {
            ProductTypes: { ...tempPayload },
          },
          () => {
            let apiPayload = { ...this.state.payload };
            apiPayload[`subCategory`] =
              this.state.selectedProduct &&
              this.state.selectedProduct.length &&
              this.state.selectedProduct
                .map((data) => {
                  return data.subCategoryName;
                })
                .toString()
                .replace(/,/g, " OR ");
            console.log("ndsjcfjdk", apiPayload);
            this.setState(
              {
                payload: { ...apiPayload },
              },
              () => {
                // this.handlePostProductsAPI(this.state.payload);
              }
            );
          }
        );
      }
    );
  };

  handleTypeOnSelect = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        if (this.state.condition == "All Conditions") {
          apiPayload[`${name}`] = "";
        } else if (name == "selectedProductType") {
          apiPayload[`type`] = this.state[name].toString();
        } else {
          apiPayload[`${name}`] = this.state[name];
        }

        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: { ...apiPayload },
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  handleManufacturerOnSelect = (selectedList, selectedItem) => {
    let selectedValue = [...this.state.selectedProductManufacturerType];
    selectedValue.push(selectedItem);
    this.setState(
      {
        selectedProductManufacturerType: selectedValue,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`manufactureId`] = this.state.selectedProductManufacturerType
          .map((data) => {
            return data.id;
          })
          .toString();
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: { ...apiPayload },
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  handleManufacturerOnRemove = (selectedList, removedItem) => {
    let selectedValue = [...this.state.selectedProductManufacturerType];
    var removeIndex = selectedValue
      .map((item) => {
        return item.id;
      })
      .indexOf(removedItem.id);
    selectedValue.splice(removeIndex, 1);
    this.setState(
      {
        selectedProductManufacturerType: selectedValue,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`manufactureId`] = this.state.selectedProductManufacturerType
          .map((data) => {
            return data.id;
          })
          .toString();
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: { ...apiPayload },
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  radiusDropdownOpen = () => {
    this.state.currentLocation
      ? this.setState({
          radiusDropdown: !this.state.radiusDropdown,
        })
      : this.props.dispatch(getCurrentLocation());
  };

  onChangeRadius = (event, value) => {
    this.setState(
      {
        valueRadius: value,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`distanceMax`] = this.state.valueRadius;
        apiPayload[`latitude`] = this.state.currentLocation.latitude;
        apiPayload[`longitude`] = this.state.currentLocation.longitude;
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: { ...apiPayload },
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  priceDropdownOpen = () => {
    if (!this.state.minPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.minPrice;
      this.setState({
        inputpayload: { ...temp },
      });
    }
    if (!this.state.maxPrice) {
      let temp = { ...this.state.inputpayload };
      delete temp.maxPrice;
      this.setState({
        inputpayload: { ...temp },
      });
    }
    this.setState({
      priceDropdown: !this.state.priceDropdown,
    });
  };

  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    console.log("hfh", inputControl.value);
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      inputpayload: { ...tempPayload },
    });
  };

  handlePriceRange = () => {
    let { minPrice, maxPrice } = this.state.inputpayload;
    this.setState(
      {
        minPrice,
        maxPrice,
        priceDropdown: !this.state.priceDropdown,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`priceRange`] = {};
        apiPayload[`priceRange`].fromPrice = this.state.minPrice;
        apiPayload[`priceRange`].toPrice = this.state.maxPrice;
        console.log("ndsjcfjdk", apiPayload);
        this.setState(
          {
            payload: { ...apiPayload },
          },
          () => {
            // this.handlePostProductsAPI(this.state.payload);
          }
        );
      }
    );
  };

  // Function for postedinputpayload for selectInput
  handleOnSelectInput = (name) => (event) => {
    let inputControl = event;
    if (inputControl && inputControl.length > 0) {
      let tempArray = [...this.state[name]];
      tempArray = event ? event.map((options) => options.value) : [];
      this.setState({ [name]: tempArray }, () => {
        let temppayload = { ...this.state.postedinputpayload };
        temppayload[[name]] = [...this.state[name]];
        this.setState({ postedinputpayload: temppayload });
      });
    } else if (inputControl && inputControl.value) {
      let temppayload = { ...this.state.postedinputpayload };
      temppayload[[name]] = inputControl.value;
      this.setState(
        {
          postedinputpayload: { ...temppayload },
        },
        () => {
          let apiPayload = { ...this.state.payload };
          apiPayload[name] = this.state.postedinputpayload[name];
          console.log("ndsjcfjdk", apiPayload);
          this.setState(
            {
              payload: { ...apiPayload },
            },
            () => {
              // this.handlePostProductsAPI(this.state.payload);
            }
          );
        }
      );
    }
  };

  handleChangeCondition = (conditionName) => {
    this.setState(
      {
        condition: conditionName,
      },
      () => {
        let apiPayload = { ...this.state.payload };
        apiPayload[`condition`] = this.state.condition;
        this.props.handleUrlParams(`condition`, this.state.condition); // function to add filterdata in url params

        this.setState(
          {
            payload: apiPayload,
          },
          () => {
            // this.props.handlePostProductsAPI(this.state.payload); // API Call
          }
        );
      }
    );
  };

  render() {
    const { ProductPost } = this.props;
    return (
      <Context.Consumer>
        {(Context) => {
          return (
            <Wrapper>
              <div className="col-12 p-0 pt-3">
                <ProductsFilterSec
                  onlyFilters={true}
                  heading={`${Context.locale.all} ${Context.locale.products}`}
                  ProductsSubCategoriesList={
                    this.props.ProductsSubCategoriesList
                  }
                  handleSelectedProductList={this.handleSelectedProductList}
                  ProductTypes={this.state.ProductTypes}
                  handleTypeOnSelect={this.handleTypeOnSelect}
                  condition={this.state.condition}
                  handleChangeCondition={this.handleChangeCondition}
                  selectedProductType={this.state.selectedProductType}
                  selectedProductManufacturerType={
                    this.state.selectedProductManufacturerType
                  }
                  handleManufacturerOnSelect={this.handleManufacturerOnSelect}
                  handleManufacturerOnRemove={this.handleManufacturerOnRemove}
                  radiusDropdownOpen={this.radiusDropdownOpen}
                  radiusDropdown={this.state.radiusDropdown}
                  onChangeRadius={this.onChangeRadius}
                  valueRadius={this.state.valueRadius}
                  priceDropdownOpen={this.priceDropdownOpen}
                  handleOnchangeInput={this.handleOnchangeInput}
                  handlePriceRange={this.handlePriceRange}
                  inputpayload={this.state.inputpayload}
                  priceDropdown={this.state.priceDropdown}
                  handleOnSelectInput={this.handleOnSelectInput}
                />
                <h6 className="heading">Products for Sale:</h6>
                <div className="row m-0 my-2">
                  {ProductPost &&
                    ProductPost.map(
                      (item, index) => (
                        // index <= 2 ? (
                        <div
                          key={index}
                          className="col-md-4 col-lg-3 p-0 boatCard mb-3"
                        >
                          <CustomLink href="/product-detail">
                            <a target="_blank">
                              <ProductCard
                                Productdetails={item}
                                // wishlist={true}
                                professionalpage={true}
                              />
                            </a>
                          </CustomLink>
                          {/* <img
                      src={Wishlist_Icon}
                      id={index}
                      className="wishlistIcon"
                      onClick={this.handleWishlistPost.bind(this, index)}
                    ></img> */}
                        </div>
                      )
                      // ) : (
                      //   ""
                      // )
                    )}
                </div>
              </div>
              <style jsx>
                {`
                  .heading {
                    font-size: 1.098vw;
                    color: ${FONTGREY_COLOR};
                    letter-spacing: 0.0219vw !important;
                    text-transform: initial;
                    margin: 1.464vw 0 0.732vw 0;
                    font-family: "Museo-Sans" !important;
                    font-weight: 600;
                  }
                  .wishlistIcon {
                    position: absolute;
                    bottom: 0.732vw;
                    right: 0.732vw;
                    width: 1.024vw;
                  }
                  .boatCard {
                    cursor: pointer;
                    max-width: 11.713vw;
                    min-height: 13.909vw;
                    box-shadow: 0 0px 6px 0 rgba(48, 56, 97, 0.1);
                    margin-right: 0.878vw !important;
                    position: relative;
                  }
                  .wishlistIconFill {
                    width: 1.464vw;
                  }
                `}
              </style>
            </Wrapper>
          );
        }}
      </Context.Consumer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    filterPostData: state.filterPostData,
    ProductsSubCategoriesList: state.ProductsSubCategoriesList,
  };
};

export default connect(mapStateToProps)(ProfessionalProductPage);
