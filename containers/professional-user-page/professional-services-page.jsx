import React, { Component } from "react";
import { FONTGREY_COLOR } from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";

class ProfessionalServicesPage extends Component {
  render() {
    return (
      <Wrapper>
        <div className="col-12 p-0 pt-3 professionalsServicesPage">
          <h6 className="heading">Maneuverability At High Speeds:</h6>
          <p>
            WhethAfter doing donuts out off the coast of Southern Florida we
            gave this boat a bit of a speed test on our way back to the docks.
            Fifty! Sixty! Finally the speedometer read seventy miles per hour!
            They did tell me that the max horse power of 1,200 gives us a top
            speed of seventy-two miles per hour. Then I saw it. The wake of
            another small ship. Before I knew it, we were hitting the water ramp
            and I had to hold on tight.
          </p>
          <p>
            We launched into the air for what felt like 3 full seconds before
            coming back down. What a thrill, being momentarily lifted out of my
            seat, but I knew I had to prepare for the inevitable thud of the
            hull slamming back down into the water. Remarkably, this didn’t
            happen. The Invincible 35’s multi-hull easily sliced into the water
            and cushioned our landing, eating up the wake from the other boat as
            we came out clean on the other side. This made it all clear to me,
            Invincible Boats means power.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in
            tortor rhoncus, tempus metus sit amet, ultricies nulla. Phasellus
            odio eros, condimentum in faucibus ac, varius ac diam. Quisque
            fermentum placerat lacus ut egestas. Vestibulum sit amet rhoncus
            lorem, nec porttitor justo. Donec ultricies tincidunt faucibus. Sed
            dictum nulla at arcu sollicitudin malesuada. Suspendisse consequat
            convallis blandit. Aenean ornare augue vitae ornare pharetra. Mauris
            mattis aliquet leo a dictum.
          </p>
          <p>
            Nulla eget ex vitae sem ornare sodales egestas non justo. Fusce non
            pretium neque, vel luctus felis. Maecenas id ante sapien. Sed
            facilisis dictum purus varius accumsan. Aliquam erat volutpat.
            Mauris pharetra scelerisque tellus eget pretium. Nunc iaculis,
            lectus quis aliquet gravida, leo dolor fringilla felis, vel egestas
            ante odio nec nunc. In finibus diam libero, sed iaculis massa
            ultrices congue. Duis dictum orci sed purus mattis, at lacinia lacus
            hendrerit.
          </p>
          <p>
            Aenean felis lectus, varius eget varius id, ornare eu sem. Praesent
            eu pellentesque massa, ut mattis mauris. Phasellus imperdiet dui sed
            nulla elementum, vel dignissim ligula congue. Vivamus pellentesque
            laoreet sapien ac rhoncus. Nunc id eros velit. Quisque ut viverra
            nulla. Vestibulum vitae velit non quam pulvinar dictum nec a lorem.
            Suspendisse eu vulputate nibh, quis dignissim ipsum. Donec blandit,
            lectus et molestie sodales, purus velit vestibulum dolor, nec
            condimentum justo turpis a nisl. Nunc hendrerit congue velit et
            rhoncus. Mauris vel velit a ex dignissim porta in non elit. Donec
            sed erat vel massa tempor vestibulum vitae vel dui. Aliquam interdum
            metus nisi, sed placerat purus malesuada in. Aenean nec molestie
            ante, eu bibendum mauris. Phasellus et tempus purus, at lobortis
            orci.
          </p>
        </div>
        <style jsx>
          {`
            .professionalsServicesPage .heading {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              text-transform: initial;
              margin-bottom: 1.244vw;
              font-family: "Open Sans-SemiBold" !important;
            }
            .professionalsServicesPage p {
              font-size: 0.732vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
              margin-bottom: 0.878vw;
              color: ${FONTGREY_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProfessionalServicesPage;
