import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_3,
  Map_Marker_Filled,
  THEME_COLOR,
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
  Phone_Grey_Icon,
  THEME_COLOR_Opacity,
} from "../../lib/config";
// Google Map Components
import GoogleMapReact from "google-map-react";

const Marker = (props) => {
  const { color } = props;
  return (
    <div
      className="marker"
      style={{ backgroundColor: color, cursor: "pointer" }}
    />
  );
};

class ProfessionalDirectionPage extends Component {
  state = {
    openpage: false,
  };

  componentDidMount() {
    this.setState({
      openpage: true,
    });
  }

  render() {
    const headOfficeProps = {
      center: {
        lat: 28.000002,
        lng: -82.728244,
      },
      zoom: 11,
    };
    const SouthPointeProps = {
      center: {
        lat: 25.765624,
        lng: -80.126066,
      },
      zoom: 11,
    };
    const { openpage } = this.state;
    return (
      <Wrapper>
        {openpage ? (
          <div className="col-12 p-0 pt-3">
            <div className="row m-0 LocationSec">
              <div className="col-5 py-3">
                <div className="loacationCard">
                  <h6 className="loactionHeading">Head Office</h6>
                  <div className="row m-0 pt-3">
                    <div className="col-1 p-0">
                      <img
                        src={Map_Marker_Filled}
                        className="mapmarkerIcon"
                      ></img>
                    </div>
                    <div className="col-11 p-0">
                      <div className="addressDetails">
                        <h6>Marine Trader, Inc</h6>
                        <p>2600 McCormick Drive Clearwater, FL 33759</p>
                        <a target="_blank">View on map</a>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0 borderTop align-items-center pt-2">
                    <div className="col-1 p-0">
                      <img src={Phone_Grey_Icon} className="contactIcon"></img>
                    </div>
                    <div className="col-11 p-0">
                      <p className="sellerContactNum">(888) 719-3246</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-7 p-0">
                <div className="MapSec">
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: "AIzaSyAtrnJwdRbJXfbsH4fr28N1TJG64c7Lrc4",
                    }}
                    defaultCenter={headOfficeProps.center}
                    // zoom={this.props.zoom}
                    defaultZoom={headOfficeProps.zoom}
                    yesIWantToUseGoogleMapApiInternals
                    // onGoogleApiLoaded={({ map, maps }) => {
                    //   this.setState({ map, maps }, () => {
                    //     // if it is edit address, take lat long from the selecteed aaddress, plot that into map and return.
                    //     if (this.props.type == 1) {
                    //       const {
                    //         latitude,
                    //         longitude
                    //       } = this.props.selectedAddress;

                    //       latitude && longitude
                    //         ? this.setMarkerOnMap(latitude, longitude)
                    //         : "";
                    //       return;
                    //     }

                    //     currentLocation
                    //       ? this.setMarkerOnMap(
                    //           currentLocation.latitude,
                    //           currentLocation.longitude
                    //         )
                    //       : "";
                    //   });
                    // }}
                  >
                    <Marker
                      lat={28.000002}
                      lng={-82.728244}
                      color={THEME_COLOR_Opacity}
                    />
                  </GoogleMapReact>
                </div>
              </div>
            </div>
            <div className="row m-0 LocationSec">
              <div className="col-5 py-3">
                <div className="loacationCard">
                  <h6 className="loactionHeading">South Pointe Pier</h6>
                  <div className="row m-0 pt-3">
                    <div className="col-1 p-0">
                      <img
                        src={Map_Marker_Filled}
                        className="mapmarkerIcon"
                      ></img>
                    </div>
                    <div className="col-11 p-0">
                      <div className="addressDetails">
                        <h6>Marine Trader, Inc</h6>
                        <p>1 Washington Ave, Miami Beach, FL 33139</p>
                        <a target="_blank">View on map</a>
                      </div>
                    </div>
                  </div>
                  <div className="row m-0 borderTop align-items-center pt-2">
                    <div className="col-1 p-0">
                      <img src={Phone_Grey_Icon} className="contactIcon"></img>
                    </div>
                    <div className="col-11 p-0">
                      <p className="sellerContactNum">(305) 673-7779</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-7 p-0">
                <div className="MapSec">
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: "AIzaSyAtrnJwdRbJXfbsH4fr28N1TJG64c7Lrc4",
                    }}
                    defaultCenter={SouthPointeProps.center}
                    // zoom={this.props.zoom}
                    defaultZoom={SouthPointeProps.zoom}
                    yesIWantToUseGoogleMapApiInternals
                    // onGoogleApiLoaded={({ map, maps }) => {
                    //   this.setState({ map, maps }, () => {
                    //     // if it is edit address, take lat long from the selecteed aaddress, plot that into map and return.
                    //     if (this.props.type == 1) {
                    //       const {
                    //         latitude,
                    //         longitude
                    //       } = this.props.selectedAddress;

                    //       latitude && longitude
                    //         ? this.setMarkerOnMap(latitude, longitude)
                    //         : "";
                    //       return;
                    //     }

                    //     currentLocation
                    //       ? this.setMarkerOnMap(
                    //           currentLocation.latitude,
                    //           currentLocation.longitude
                    //         )
                    //       : "";
                    //   });
                    // }}
                  >
                    <Marker
                      lat={25.765624}
                      lng={-80.126066}
                      color={THEME_COLOR_Opacity}
                    />
                  </GoogleMapReact>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <style jsx>
          {`
            :global(.marker) {
              position: absolute;
              top: 50%;
              left: 50%;
              width: 5.124vw;
              height: 5.124vw;
              background-color: #000;
              border-radius: 100%;
              user-select: none;
              transform: translate(-50%, -50%);
            }
            :global(.marker:hover) {
              z-index: 1;
            }
            .LocationSec {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              margin-bottom: 1.098vw !important;
            }
            :global(.MapSec .gm-fullscreen-control) {
              height: 1.83vw !important;
              width: 1.83vw !important;
            }
            :global(.MapSec .gm-fullscreen-control img) {
              height: 1.024vw !important;
              width: 1.024vw !important;
            }
            :global(.MapSec .gm-bundled-control, .MapSec
                .gm-bundled-control-on-bottom) {
              bottom: 3.294vw !important;
              right: 2.196vw !important;
            }
            :global(.MapSec .gm-bundled-control > div > div button img, .MapSec
                .gm-bundled-control-on-bottom
                > div
                > div
                button
                img) {
              height: 0.951vw !important;
              width: 0.951vw !important;
            }
            :global(.MapSec .gm-bundled-control > div > div > div, .MapSec
                .gm-bundled-control-on-bottom
                > div
                > div
                > div) {
              margin: 0 !important;
            }
            :global(.MapSec .gm-bundled-control > div > div button, .MapSec
                .gm-bundled-control-on-bottom
                > div
                > div
                button) {
              width: 2.196vw !important;
              height: 1.83vw !important;
            }

            :global(.MapSec .gm-bundled-control > div > div, .MapSec
                .gm-bundled-control-on-bottom
                > div
                > div) {
              width: 2.196vw !important;
              height: 3.66vw !important;
            }

            .MapSec {
              width: 100%;
              height: 13.909vw;
              position: relative;
            }
            .loactionHeading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .mapmarkerIcon {
              width: 0.732vw;
              margin-bottom: 0.951vw;
            }
            .addressDetails h6 {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.512vw;
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
            }
            .addressDetails p {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0;
              max-width: 14.641vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .addressDetails a {
              font-size: 0.805vw;
              text-decoration: none;
              color: ${THEME_COLOR};
              cursor: pointer;
              margin-bottom: 0.366vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .borderTop {
              border-top: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .contactIcon {
              width: 0.878vw;
              margin-bottom: 0.366vw;
            }
            .sellerContactNum {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProfessionalDirectionPage;
