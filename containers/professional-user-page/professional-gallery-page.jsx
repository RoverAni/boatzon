import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { FONTGREY_COLOR } from "../../lib/config";
import MasonaryLayout from "../../components/masonry-layout/masonry-layout";

class ProfessionalGalleryPage extends Component {
  render() {
    const { proUserGallery } = this.props;
    return (
      <Wrapper>
        <div className="col-12 p-0 pt-3">
          <h6 className="heading">Photos:</h6>
          <div className="row m-0 my-2">
            <MasonaryLayout>
              {proUserGallery &&
                proUserGallery.map((data, index) => (
                  <div key={index} className="boatCard">
                    <img src={data.mainUrl}></img>
                  </div>
                ))}
            </MasonaryLayout>
          </div>
        </div>
        <style jsx>
          {`
            .heading {
              font-size: 1.098vw;
              color: ${FONTGREY_COLOR};
              letter-spacing: 0.0219vw !important;
              text-transform: initial;
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .boatCard {
              cursor: pointer;
              box-shadow: 0 0px 6px 0 rgba(48, 56, 97, 0.1);
              position: relative;
            }
            .boatCard img {
              width: 100%;
              object-fit: cover;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ProfessionalGalleryPage;
