import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  FONTGREY_COLOR,
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_9,
  Long_Arrow_Left,
  THEME_COLOR_Opacity,
  Globe_Grey_Icon,
  SelectInput_Chevron_Down_Grey,
  GREY_VARIANT_2,
  GREY_VARIANT_10,
  GREEN_COLOR,
  Video_Icon_Blue,
  Video_Icon_White,
  calendar_blue,
  calendar_white,
  BG_LightGREY_COLOR,
  CallNow,
  scheduleTimesV2,
  scheduleTimesV3,
} from "../../lib/config";
import { connect } from "react-redux";
import { Calendar } from "antd";
import moment from "moment";
import Snackbar from "../../components/snackbar";
import { getCookie } from "../../lib/session";
import ButtonComp from "../../components/button/button";
import { userSelectedToChat } from "../../redux/actions/chat/chat";
import { storeWebrtcData } from "../../redux/actions/webrtc/webrtc";
import {
  sendAVCallRequest,
  getStatusForAVCalls,
  getAllRequestForAVCalls,
  callUser,
} from "../../services/calling";
import {
  getAppointmentData,
  setInPersonAppointment,
} from "../../services/my-appointment";

class VideoCallAppointment extends Component {
  state = {
    // selectedDate: moment(new Date().getTime()).startOf("day"),
    selectedDate: moment().startOf("day"),
    callNow: false,
    selected: true,
    timeSelectedForCall: "",
    open: false,
    variant: "error",
    usermessage: "",
    inpersonMeetingsList: [],
    meetingTimings: scheduleTimesV2,
    duplicatedTimes: scheduleTimesV3,
  };

  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  /** this function is to create in video meeting */
  scheduleVideoMeeting = () => {
    let _meetingDate =
      parseInt(this.state.newDate) + parseInt(this.state.timeSelectedForCall);
    let {
      postId,
      memberMqttId,
      place,
      latitude,
      longitude,
      productName,
      description,
    } = this.props && this.props.boatDetail;
    let obj = {
      fromId: getCookie("mqttId"),
      toId: memberMqttId,
      secretId: postId.toString(),
      docId: "xxx",
      meetingDate: _meetingDate.toString(),
      latitude: latitude.toString(),
      longitude: longitude.toString(),
      location: place,
      title: productName,
      description: description,
    };
    sendAVCallRequest(obj)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            variant: "success",
            usermessage: "Video appointment scheduled successfully...",
            open: true,
          });
          this.props && this.props.isVideoMeeting(true);
          this.props.setMeetingTimestamp(_meetingDate);
          if (this.props && this.props.isDirect) {
            this.props.directNextPage();
          } else {
            this.props.nextPage();
          }
        }
      })
      .catch((err) => {
        this.setState({
          variant: "error",
          usermessage: err.message || "Something went wrong...",
          open: false,
        });
        console.log("fail scheduleVideoMeeting", err);
      });
  };

  /** this function is to create in person appointment */
  scheduleInPersonAppointment = () => {
    let _meetingDate =
      parseInt(this.state.newDate) + parseInt(this.state.timeSelectedForCall);
    let {
      postId,
      memberMqttId,
      place,
      latitude,
      longitude,
      productName,
      description,
    } = this.props && this.props.boatDetail;
    let data = {
      fromId: getCookie("mqttId"),
      toId: memberMqttId,
      secretId: postId.toString(),
      docId: "xxx",
      meetingDate: _meetingDate.toString(),
      meetingType: 2,
      latitude: latitude.toString(),
      longitude: longitude.toString(),
      location: place,
      title: productName,
      description: description,
    };
    console.log("sch app 2", data);
    setInPersonAppointment(data)
      .then((res) => {
        if (res.status === 200) {
          this.props.setMeetingTimestamp(_meetingDate);
          this.setState({
            variant: "success",
            usermessage: "In Person appointment scheduled successfully...",
            open: true,
          });
          if (this.props && this.props.isDirect) {
            this.props.directNextPage();
          } else {
            this.props.nextPage();
          }
        }
      })
      .catch((err) => {
        this.setState({
          variant: "error",
          usermessage: err.message || "Something went wrong...",
          open: false,
        });
        console.log("fail scheduleVideoMeeting", err);
      });
  };

  makeAppointment = () => {
    if (this.props.isInPersonMeetingScreen) {
      this.scheduleInPersonAppointment();
    } else {
      this.scheduleVideoMeeting();
    }
  };

  toggleSected = () => this.setState({ selected: !this.state.selected });

  selectTimeForCall = (time) => {
    // check for
    this.setState({ timeSelectedForCall: time });
  };

  onPanelChange = (value, mode) => {
    console.log("onPanelChange", value, mode);
  };

  wipeExisitingMeetings = () => {
    let meetingTimings1 = [...this.state.meetingTimings];
    for (let j = 0; j < meetingTimings1.length; j++) {
      meetingTimings1[j].isInPerson = false;
    }
    this.setState({ meetingTimings: meetingTimings1 });
  };

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevState && prevState.selectedDate && prevState.selectedDate.date() !== this.state.selectedDate.date()) {
  //     this.wipeExisitingMeetings();
  //   }
  // }

  handleOnDateChange = (value) => {
    this.setState({
      selectedDate: value,
      newDate:
        moment(
          `${value.year()}-${value.format("M")}-${value.date()} 00:00:00`
        ).unix() * 1000,
    });
    this.wipeExisitingMeetings();
    let existingMeetings = [...this.state.inpersonMeetingsList];
    try {
      if (existingMeetings.length > 0) {
        let startTimeStamp =
          moment(
            `${value.year()}-${value.format("M")}-${value.date()} 00:00:00`
          ).unix() * 1000; // this will check for appointments on selected day;
        let endTimeStamp =
          moment(
            `${value.year()}-${value.format("M")}-${value.date()} 23:59:59`
          ).unix() * 1000; // this will check for appointments on selected day;

        /** this will get all the dates for selected day */
        let allMeetingsForSelectedDate = existingMeetings.filter(
          (k) => k.startDate > startTimeStamp && k.startDate < endTimeStamp
        );

        try {
          if (
            allMeetingsForSelectedDate &&
            allMeetingsForSelectedDate.length > 0
          ) {
            let meetingTimings = [...this.state.meetingTimings];
            let reservedAppointments = allMeetingsForSelectedDate.map((k) =>
              meetingTimings.filter(
                (v) => k.startDate - (startTimeStamp + v.ms) == 0
              )
            );
            let arr = reservedAppointments.flat(1); // to remove array from array of arrays = [[1], [2]] => [1], [2]
            if (arr && arr.length > 0) {
              for (let i = 0; i < arr.length; i++) {
                for (let j = 0; j < meetingTimings.length; j++) {
                  if (arr[i].ms === meetingTimings[j].ms) {
                    meetingTimings[j].isInPerson = true;
                  }
                }
              }
              this.setState({ meetingTimings: meetingTimings });
            } else {
              this.wipeExisitingMeetings();
            }
          } else {
            this.wipeExisitingMeetings();
          }
        } catch (ee) {
          this.wipeExisitingMeetings();
        }
      }
    } catch (e) {
      this.wipeExisitingMeetings();
    }
  };

  disabledDate = (current) => {
    return current && current < moment().endOf("day");
  };

  handlePrevMonth = (value) => {
    const newValue = value.clone();
    newValue.month(parseInt(value.month() - 1, 10));
    this.setState({
      selectedDate: newValue,
    });
  };

  handleNextMonth = (value) => {
    const newValue = value.clone();
    newValue.month(parseInt(value.month() + 1, 10));
    this.setState({
      selectedDate: newValue,
    });
  };

  /** this function is to initiate video call with target user */
  startCall = () => {
    let newRoomId = new Date().getTime().toString();
    let obj = {
      type: "video",
      room: newRoomId,
      to: this.props.userSelectedToChat.recipientId,
      // callId: this.props.userSelectedToChat.meAsABuyerCallID,
    };
    console.log("startCall");
    callUser(obj)
      .then((res) => {
        let newObj = { ...res.data.data };
        newObj["callId"] = res.data.data.callId;
        newObj["twilioToken"] = res.data.data.twilioToken;
        newObj["room"] = newRoomId;
        this.props._storeWebrtcData(newObj);
      })
      .catch((err) => console.log("err", err));
  };

  showWarning = () => {
    this.setState({ usermessage: "Please select time", open: true });
  };

  handeMonthNames = (monthValue) => {
    switch (monthValue) {
      case 0:
        return "January";
        break;
      case 1:
        return "February";
        break;
      case 2:
        return "March";
        break;
      case 3:
        return "April";
        break;
      case 4:
        return "May";
        break;
      case 5:
        return "June";
        break;
      case 6:
        return "July";
        break;
      case 7:
        return "August";
        break;
      case 8:
        return "September";
        break;
      case 9:
        return "October";
        break;
      case 10:
        return "November";
        break;
      case 11:
        return "December";
        break;
    }
  };

  handleDayNames = (dayValue) => {
    switch (dayValue) {
      case 0:
        return "Sunday";
        break;
      case 1:
        return "Monday";
        break;
      case 2:
        return "Tuesday";
        break;
      case 3:
        return "Wednesday";
        break;
      case 4:
        return "Thrusday";
        break;
      case 5:
        return "Friday";
        break;
      case 6:
        return "Saturday";
        break;
    }
  };

  /** function to start video call */
  initiateVideoCall = () => {
    if (
      (this.props.userSelectedToChat.offerType === "1" &&
        this.props.userSelectedToChat.initiated &&
        this.props.userSelectedToChat.callStatus === 1) ||
      (this.props.userSelectedToChat.offerType === "1" &&
        this.props.userSelectedToChat.initiated)
    ) {
      this.props.onClose();
      this.props.closeDrawer();
      this.startCall();
      console.log("init call");
    } else if (!("offerType" in this.props.userSelectedToChat)) {
      this.setState({
        variant: "warning",
        usermessage: "Please create offer before making video call",
        open: true,
      });
    } else {
      this.setState({
        variant: "warning",
        usermessage: "Please wait until your request gets approved",
        open: true,
      });
    }
  };

  componentDidMount() {
    let { memberMqttId } = this.props && this.props.boatDetail;
    let _AllChats = [...this.props.allChats];
    let index = _AllChats.findIndex((k) => k.receiverId === memberMqttId);
    this.setState({ selectedDate: moment().startOf("day") });
    console.log("found", index);
    if (index > -1) {
      this.props.selectUserToChat(_AllChats[index]);
      this.GetStatusForAVCalls(memberMqttId);
      this.GetAllRequestForAVCalls(memberMqttId);
    }
    getAppointmentData({
      userId: getCookie("mqttId"),
      type: 2,
      startDate: parseInt(new Date().getTime()),
      endDate: moment().endOf("month").format("x"),
    }).then((res) => {
      // console.log("res1", res);
    });
    getAppointmentData({
      userId: getCookie("mqttId"),
      type: 1,
      startDate: parseInt(new Date().getTime()),
      endDate: moment().endOf("month").format("x"),
    }).then((res) => {
      this.setState({ inpersonMeetingsList: res.data.data });
      // console.log("res2", res);
    });
  }

  GetStatusForAVCalls = (toId) => {
    getStatusForAVCalls(toId)
      .then((res) => {
        let guyWithRequest = { ...this.props.userSelectedToChat };
        guyWithRequest["meAsABuyerCallRequest"] = res.data.data.status;
        guyWithRequest["meAsABuyerCallWith"] = res.data.data.toId;
        guyWithRequest["meAsABuyerCallID"] = res.data.data._id;
        this.props.selectUserToChat(guyWithRequest);
      })
      .catch((err) => console.log("err", err));
  };

  componentWillUnmount() {
    this.wipeExisitingMeetings();
    this.props && this.props.isDirect
      ? () => {}
      : this.props.resetToVideoAppointment();
  }

  /** if status
   * 1. the buyer has requested so prompt is shown
   * 2. you have accepted the request
   * 3. rejected request
   */
  GetAllRequestForAVCalls = (fromId) => {
    getAllRequestForAVCalls(fromId)
      .then((res) => {
        let guyWithRequest = { ...this.props.userSelectedToChat };
        guyWithRequest["callStatus"] = res.data.data.status;
        guyWithRequest["callFrom"] = res.data.data.fromId;
        guyWithRequest["_id"] = res.data.data._id;
        this.getCallID(res.data.data._id);
        this.props.selectUserToChat(guyWithRequest);
        if (res.data.data.status === 0) {
          this.setState({ showModal: !this.state.showModal });
        }
      })
      .catch((err) => console.log("err", err));
  };

  render() {
    console.log("boatDetail", this.props.boatDetail);
    // console.log("inpersonMeetingsList", this.state.inpersonMeetingsList);
    const CalenderHeader = ({ value, type, onChange, onTypeChange }) => {
      // console.log("fnfiej", onChange);
      return (
        <div className="calenderHeader d-flex align-items-center justify-content-between">
          <h6 className="m-0">
            {this.handeMonthNames(value.month())} {value.year()}
          </h6>{" "}
          <div>
            <img
              src={Long_Arrow_Left}
              className="leftarrowIcon"
              onClick={this.handlePrevMonth.bind(this, value)}
            ></img>
            <img
              src={Long_Arrow_Left}
              className="rightarrowIcon"
              onClick={this.handleNextMonth.bind(this, value)}
            ></img>
          </div>
        </div>
      );
    };
    return (
      <Wrapper>
        <div className="py-3 MessagePostPage">
          <div className="closeIcon" onClick={() => this.props.onClose()}>
            <img src={Close_Icon}></img>
          </div>
          <div className="col-12 p-0">
            <div className="m-0">
              <div className="col-12">
                {this.state.selected ? (
                  <>
                    {this.props.isInPersonMeetingScreen ? (
                      ""
                    ) : (
                      <ButtonComp
                        className="reqMeetingNow mt-5"
                        onClick={this.toggleSected}
                      >
                        <img src={Video_Icon_Blue} className="phoneIcon1"></img>
                        Request Video Meeting
                      </ButtonComp>
                    )}

                    <ButtonComp
                      className={
                        this.props.isInPersonMeetingScreen
                          ? "scheduleDateAndTime mt-5"
                          : "scheduleDateAndTime mt-3"
                      }
                      onClick={() =>
                        this.props.isInPersonMeetingScreen
                          ? ""
                          : this.toggleSected()
                      }
                    >
                      <img src={calendar_white} className="phoneIcon2"></img>
                      Schedule Date & Time
                    </ButtonComp>
                    <div className="m-0 my-3">
                      <div className="col-12 px-0">
                        <div className="Calender mt-4">
                          <Calendar
                            fullscreen={false}
                            value={this.state.selectedDate}
                            onPanelChange={this.onPanelChange}
                            onChange={this.handleOnDateChange}
                            headerRender={CalenderHeader}
                            disabledDate={this.disabledDate}
                          />
                        </div>
                      </div>
                      <div className="col-12 px-0">
                        <div>
                          <h6 className="m-0 todayDate">
                            {this.handleDayNames(this.state.selectedDate.day())}
                            ,{" "}
                            {this.handeMonthNames(
                              this.state.selectedDate.month()
                            )}{" "}
                            {this.state.selectedDate.date()}
                          </h6>{" "}
                          <p className="timeStamp">
                            <img
                              src={Globe_Grey_Icon}
                              className="globeIcon"
                            ></img>
                            <span>{new Date().toString()}</span>
                            <img
                              src={SelectInput_Chevron_Down_Grey}
                              className="chevronIcon"
                            ></img>
                          </p>
                          <ul className="list-unstyled d-flex align-items-center flex-wrap justify-content-between">
                            {this.state.meetingTimings &&
                              this.state.meetingTimings.map((k, i) =>
                                k.isInPerson || k.isVideo ? (
                                  <li className="timeCardForInPerson" key={i}>
                                    {k.time}
                                  </li>
                                ) : (
                                  <li
                                    className={
                                      this.state.timeSelectedForCall === k.ms
                                        ? "timeCard "
                                        : "timeCardNotSelected"
                                    }
                                    key={i}
                                    onClick={() => this.selectTimeForCall(k.ms)}
                                  >
                                    {k.time}
                                  </li>
                                )
                              )}
                          </ul>
                        </div>
                      </div>
                      <div
                        className={
                          this.state.timeSelectedForCall
                            ? "scheduleSubmit px-0"
                            : "disabledSubmitBtn px-0"
                        }
                      >
                        <ButtonComp
                          onClick={
                            this.state.timeSelectedForCall
                              ? () => this.makeAppointment()
                              : () => this.showWarning()
                          }
                        >
                          Submit
                        </ButtonComp>
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    <ButtonComp
                      className="reqMeetingNow mt-5"
                      onClick={this.toggleSected}
                    >
                      <img src={Video_Icon_White} className="phoneIcon1"></img>
                      Request Video Meeting
                    </ButtonComp>
                    <ButtonComp
                      className="scheduleDateAndTime mt-3"
                      onClick={this.toggleSected}
                    >
                      <img src={calendar_blue} className="phoneIcon2"></img>
                      Schedule Date & Time
                    </ButtonComp>
                    <div className="m-0 my-3">
                      <ButtonComp
                        className="scheduleDriveTestV2 px-0"
                        onClick={() => this.initiateVideoCall()}
                      >
                        <img src={CallNow} atl="call-now" />
                      </ButtonComp>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
        <Snackbar
          variant={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleSnackbarClose}
          vertical={"bottom"}
          horizontal={"left"}
        />
        <style jsx>
          {`
            .leftSec {
              max-width: 15.373vw;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.5625vw;
              cursor: pointer !important;
              height: 1.5625vw;
              z-index: 999 !important;
            }
            .MessagePostPage {
              width: 25.622vw;
              padding: 1.098vw;
              position: relative;
            }
            .heading {
              font-size: 1.171vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              margin-bottom: 0;
              padding: 0.732vw 0 0 0;
            }
            .AptBtnSec {
              padding: 1.317vw 0;
            }
            .videoAPt_btn {
              margin-bottom: 0.366vw;
            }
            :global(.videoAPt_btn button) {
              width: 100%;
              height: 2.342vw;
              padding: 0;
              background: ${THEME_COLOR};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.videoAPt_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${WHITE_COLOR};
              position: relative;
            }
            :global(.videoAPt_btn button:focus),
            :global(.videoAPt_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.videoAPt_btn button:hover) {
              background: ${THEME_COLOR};
            }
            .videoIcon {
              position: absolute;
              top: 50%;
              left: 0.585vw;
              transform: translate(0, -50%);
              width: 0.878vw;
            }
            .userIcon {
              position: absolute;
              top: 50%;
              left: 0.585vw;
              transform: translate(0, -50%);
              width: 0.951vw;
            }
            :global(.personApt_btn button) {
              width: 100%;
              height: 2.342vw;
              padding: 0;
              background: ${GREY_VARIANT_9};
              margin: 0;
              border-radius: 0.146vw;
              text-transform: capitalize;
              position: relative;
            }
            :global(.personApt_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_1};
              position: relative;
            }
            :global(.personApt_btn button:focus),
            :global(.personApt_btn button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.personApt_btn button:hover) {
              background: ${GREY_VARIANT_9};
            }
            .appointmentInfo {
              display: flex;
            }
            .appointmentInfo span {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_1};
            }
            .clockIcon {
              width: 0.732vw;
              margin-right: 0.732vw;
            }
            .mapMarkerIcon {
              width: 0.951vw;
              margin-right: 0.732vw;
              margin-bottom: 1.317vw;
            }
            :global(.leftarrowIcon) {
              width: 0.732vw;
              height: 0.878vw;
              margin-right: 0.585vw;
              cursor: pointer;
            }
            :global(.rightarrowIcon) {
              width: 0.732vw;
              height: 0.878vw;
              transform: rotate(180deg);
              cursor: pointer;
            }
            :global(.calenderHeader h6),
            .todayDate {
              font-family: "Museo-Sans" !important;
              font-size: 0.9375vw;
              font-style: normal;
              font-weight: 700;
              letter-spacing: 0.0292vw !important;
              color: ${FONTGREY_COLOR};
              position: relative;
            }
            .timeStamp {
              margin: 0.585vw 0;
            }
            .timeStamp span {
              font-family: "Open Sans" !important;
              //   font-size: 0.658vw;
              font-size: 0.73vw;
              font-style: normal;
              font-weight: 700;
              letter-spacing: 0.0292vw !important;
              color: ${GREY_VARIANT_1};
            }
            .globeIcon {
              width: 0.732vw;
              margin-right: 0.732vw;
            }
            .chevronIcon {
              width: 0.658vw;
              margin-left: 0.732vw;
            }
            .timeCard {
              width: 47.5%;
              border: 0.0732vw solid ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${WHITE_COLOR};
              background: ${THEME_COLOR};
              text-align: center;
              padding: 0.512vw 0;
              margin-bottom: 0.585vw;
              cursor: pointer;
            }
            .timeCardForInPerson {
              width: 47.5%;
              border: 0.0732vw solid #d1d1d1;
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${WHITE_COLOR};
              // background: #7c5c3c;
              background: #d1d1d1;
              text-align: center;
              padding: 0.512vw 0;
              margin-bottom: 0.585vw;
              cursor: not-allowed;
            }
            .timeCardNotSelected {
              width: 47.5%;
              border: 0.0732vw solid ${THEME_COLOR};
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.658vw;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
              color: ${THEME_COLOR};
              text-align: center;
              padding: 0.512vw 0;
              margin-bottom: 0.585vw;
              cursor: pointer;
            }
            .nextArrow {
              width: 0.878vw;
              margin-left: 0.732vw;
            }
            .backArrow {
              width: 0.878vw;
              margin-right: 0.732vw;
            }
            :global(.back_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${GREY_VARIANT_10};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.back_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.back_btn button:focus),
            :global(.back_btn button:active) {
              background: ${GREY_VARIANT_10};
              outline: none;
              box-shadow: none;
            }
            :global(.back_btn button:hover) {
              background: ${GREY_VARIANT_10};
            }
            :global(.next_btn button) {
              width: fit-content;
              padding: 0.366vw 2.562vw;
              background: ${THEME_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.next_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
              letter-spacing: 0.0219vw !important;
              color: ${WHITE_COLOR};
            }
            :global(.next_btn button:focus),
            :global(.next_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.next_btn button:hover) {
              background: ${THEME_COLOR};
            }

            :global(.Calender .ant-picker-calendar .ant-picker-panel) {
              border: none !important;
              padding: 1.098vw 0 !important;
              width: 100% !important;
            }
            :global(.Calender .ant-picker-calendar-mini .ant-picker-content) {
              width: 100% !important;
              height: auto;
            }
            :global(.Calender
                .ant-picker-calendar
                .ant-picker-panel
                .ant-picker-content) {
              //   width: auto !important;
              width: 100% !important;
            }
            :global(.Calender .ant-picker-calendar) {
              font-size: 0.732vw;
              font-family: "Museo-Sans" !important;
              font-style: normal;
              letter-spacing: 0.0292vw !important;
            }
            :global(.Calender .ant-picker-cell-disabled::before) {
              border-radius: 50% !important;
              background: ${WHITE_COLOR};
            }
            :global(.Calender
                .ant-picker-cell-disabled
                .ant-picker-cell-inner) {
              color: ${GREY_VARIANT_2};
            }
            :global(.Calender
                .ant-picker-cell-in-view.ant-picker-cell-selected
                .ant-picker-cell-inner) {
              border-radius: 50% !important;
              background: ${THEME_COLOR} !important;
              color: ${WHITE_COLOR} !important;
            }
            :global(.Calender .ant-picker-cell .ant-picker-cell-inner) {
              z-index: 1;
            }
            :global(.Calender
                .ant-picker-cell-in-view.ant-picker-cell-today
                .ant-picker-cell-inner::before) {
              border: none !important;
            }
            :global(.Calender
                .ant-picker-cell:not(.ant-picker-cell-disabled):not(.ant-picker-cell-selected)
                .ant-picker-cell-inner) {
              border-radius: 50% !important;
              background: ${THEME_COLOR_Opacity};
              color: ${THEME_COLOR};
            }
            :global(.Calender
                .ant-picker-cell:hover:not(.ant-picker-cell-selected):not(.ant-picker-cell-range-start):not(.ant-picker-cell-range-end):not(.ant-picker-cell-range-hover-start):not(.ant-picker-cell-range-hover-end)
                .ant-picker-cell-inner) {
              border-radius: 50% !important;
            }
            :global(.Calender .ant-picker-content td, .Calender
                .ant-picker-content
                th) {
              max-width: 2.122vw;
              min-width: fit-content;
            }
            :global(.Calender .ant-picker-content th) {
              color: ${GREY_VARIANT_2};
            }
            :global(.Calender .ant-picker-content tbody) {
              padding: 0.732vw 0 0 0;
            }
            :global(.Calender .ant-picker-content tr) {
              display: flex;
              align-items: center;
              justify-content: space-evenly;
              width: auto !important;
            }

            :global(.reqMeetingNow) {
              background: ${this.state.selected
                ? BG_LightGREY_COLOR
                : THEME_COLOR} !important;
              border: 1px solid ${THEME_COLOR};
              width: 100%;
              border-radius: 0px !important;
              padding: 0.5vw;
            }
            :global(.reqMeetingNow span) {
              color: ${this.state.selected ? THEME_COLOR : WHITE_COLOR};
              font-weight: 700;
              font-family: "Museo-Sans" !important;
              font-size: 0.8375vw;
            }

            :global(.scheduleDateAndTime) {
              background: ${this.state.selected
                ? THEME_COLOR
                : BG_LightGREY_COLOR} !important;
              width: 100%;
              border-radius: 0px !important;
              padding: 0.5vw;
            }
            :global(.scheduleDateAndTime span) {
              color: ${this.state.selected ? WHITE_COLOR : THEME_COLOR};
              font-weight: 700;
              font-family: "Museo-Sans" !important;
              font-size: 0.8375vw;
            }
            :global(.Calender div div h6) {
              font-family: "Museo-Sans" !important;
              font-weight: 700;
              font-size: 0.9375vw !important;
            }
            :global(.scheduleSubmit button) {
              width: 100%;
              padding: 0.466vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              font-weight: 700;
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.scheduleSubmit button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.9375vw;
            }
            :global(.scheduleSubmit button:focus),
            :global(.scheduleSubmit button:active) {
              outline: none;
              box-shadow: none;
            }
            :global(.scheduleSubmit button:hover) {
              opacity: 0.8 !important;
              cursor: pointer;
              background: ${GREEN_COLOR};
            }
            :global(.disabledSubmitBtn button) {
              width: 100%;
              padding: 0.466vw 0;
              background: ${GREY_VARIANT_9};
              color: ${GREY_VARIANT_1};
              font-weight: 700;
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.disabledSubmitBtn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.9375vw;
            }
            .phoneIcon1 {
              position: absolute;
              left: 1.5vw;
              top: 0.75vw;
              width: 1.4vw;
            }
            .phoneIcon2 {
              position: absolute;
              left: 1.5vw;
              top: 0.59vw;
              width: 1.2vw;
            }
            :global(.scheduleDriveTestV2) {
              width: 100%;
              padding: 0.766vw 0;
              border-radius: 0px !important;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              border-radius: none !important;
              font-weight: 700;
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.scheduleDriveTestV2 span img) {
              width: 6vw !important;
            }
            :global(.scheduleDriveTestV2:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.scheduleDriveTestV2:hover) {
              background: ${GREEN_COLOR};
              opacity: 0.8;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userSelectedToChat: state.userSelectedToChat,
    allChats: state.allChats,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectUserToChat: (data) => dispatch(userSelectedToChat(data)),
    _storeWebrtcData: (data) => dispatch(storeWebrtcData(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoCallAppointment);
