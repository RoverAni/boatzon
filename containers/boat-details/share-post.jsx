import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  FONTGREY_COLOR_Dark,
  Facebook_ShareIcon,
  Twitter_ShareIcon,
  Email_ShareIcon,
  Insta_ShareIcon,
  WhatsApp_ShareIcon,
  GREY_VARIANT_1,
  FONTGREY_COLOR,
  GREY_VARIANT_3,
  THEME_COLOR,
  BG_LightGREY_COLOR,
  THEME_COLOR_Opacity,
} from "../../lib/config";
import InputBox from "../../components/input-box/input-box";
import ButtonComp from "../../components/button/button";
import { handleFBShare, handleEmailShare } from "../../lib/share/handleShare";

class SharePost extends Component {
  state = {
    inputpayload: {},
  };
  // Funtion for inputpayload
  handleOnchangeInput = (name) => (event) => {
    let inputControl = event.target;
    let tempPayload = this.state.inputpayload;
    tempPayload[[name]] = inputControl.value;
    this.setState({
      inputpayload: tempPayload,
    });
  };
  render() {
    return (
      <Wrapper>
        <div className="col-12 py-4 ShareModel">
          <img src={Close_Icon} className="closeIcon" onClick={this.props.onClose}></img>
          <h5 className="heading">Share</h5>
          <div className="d-flex align-items-center justify-content-between">
            <div className="imgDiv" onClick={() => handleFBShare(this.props.productDetail)}>
              <img src={Facebook_ShareIcon} className="shareIcon"></img>
              <span>Facebook</span>
            </div>
            <div className="imgDiv">
              <img src={Twitter_ShareIcon} className="shareIcon"></img>
              <span>Twitter</span>
            </div>
            <div className="imgDiv" onClick={() => handleEmailShare(this.props.productDetail)}>
              <img src={Email_ShareIcon} className="shareIcon"></img>
              <span>Email</span>
            </div>
            <div className="imgDiv">
              <img src={Insta_ShareIcon} className="shareIcon"></img>
              <span>Instagram</span>
            </div>
            <div className="imgDiv">
              <img src={WhatsApp_ShareIcon} className="shareIcon"></img>
              <span>WhatsApp</span>
            </div>
          </div>
          <div className="row m-0">
            <div className="col-9 p-0">
              <div className="FormInput">
                <InputBox
                  type="text"
                  className="inputBox form-control"
                  name="post_link"
                  placeholder="Post Link"
                  onChange={this.handleOnchangeInput("post_link")}
                  autoComplete="off"
                ></InputBox>
              </div>
            </div>
            <div className="col-3 pr-0">
              <div className="copy_btn">
                <ButtonComp>Copy</ButtonComp>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            :global(.MuiBackdrop-root) {
              background: linear-gradient(0deg, rgba(17, 39, 56, 0.7), rgba(17, 39, 56, 0.7));
            }
            .ShareModel {
              width: 24.158vw;
              padding: 1.098vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              right: 1.098vw;
              width: 0.732vw;
              cursor: pointer;
            }
            .heading {
              margin: 0 0 1.098vw 0;
              font-size: 1.405vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .imgDiv {
              margin-right: 0.732vw;
              cursor: pointer;
              text-align: center;
            }
            .shareIcon {
              width: 3.66vw;
              object-fit: cover;
            }
            .imgDiv span {
              font-family: "Open Sans" !important;
              color: ${GREY_VARIANT_1};
              font-size: 0.732vw;
            }
            .ShareModel .FormInput {
              padding-top: 0.732vw;
            }
            :global(.ShareModel .FormInput .inputBox) {
              width: 100%;
              height: 2.196vw;
              padding: 0 0.732vw;
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.292vw;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.805vw;
              letter-spacing: 0.0366vw;
              font-family: "Open Sans" !important;
            }
            :global(.ShareModel .FormInput .inputBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              font-size: 0.805vw;
              box-shadow: 0 0 0 0.2rem ${THEME_COLOR_Opacity};
            }
            :global(.inputBox::placeholder) {
              font-size: 0.805vw;
              font-family: "Open Sans" !important;
              color: ${FONTGREY_COLOR};
            }
            :global(.copy_btn button) {
              width: 100%;
              height: 2.196vw;
              margin: 0.732vw 0 0 0;
              text-transform: initial;
              background: ${BG_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              position: relative;
            }
            :global(.copy_btn button span) {
              font-family: "Open Sans" !important;
              font-size: 0.878vw;
            }
            :global(.copy_btn button:hover) {
              background: ${BG_LightGREY_COLOR};
            }
            :global(.copy_btn button:focus),
            :global(.copy_btn button:active) {
              background: ${BG_LightGREY_COLOR};
              outline: none;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SharePost;
