import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  Professionals_White_Icon,
  THEME_COLOR,
  WHITE_COLOR,
  Dark_Blue_Color,
  GREY_VARIANT_2,
  BG_LightGREY_COLOR,
  FONTGREY_COLOR_Dark,
  Orange_Color,
  Border_LightGREY_COLOR,
  Border_LightGREY_COLOR_2,
  FONTGREY_COLOR,
  GREEN_COLOR,
  GREY_VARIANT_13,
  Video_Icon_White,
  TestDrive,
  User_White_Icon,
  GREY_VARIANT_9,
} from "../../lib/config";
import TextAreaBox from "../../components/input-box/text-area-input";
import { connect } from "react-redux";
import { storeBoatData } from "../../redux/actions/boats/boats";
import { askSeller } from "../../services/chat";
import Router from "next/router";
import ButtonComp from "../../components/button/button";
import VideoCallAppointment from "./video-call-appointment";
import AcceptedMeetingInformation from "./accepted-meeting-info";

let textHelper = ["Hi, Is it still available?", "Schedule In-Person Meeting", "Schedule Video Meeting", "Schedule Sea Trail"];

class MessageSellerModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
      videoCallAppointment: false,
      currentPage: 0, // this will be used in switch case to navigate between views
      isInPersonMeetingScreen: false,
    };
  }

  nextPage = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
  };

  goBackPage = () => {
    console.log("clicked back");
    if (this.state.currentPage === 0) return;
    this.setState({ currentPage: this.state.currentPage - 1 });
  };

  resetPage = () => {
    this.setState({ currentPage: 0 });
  };

  copyToInputText = (i) => {
    let d = document.getElementById(`tx_${i}`);
    this.setState({ input: d.innerText });
  };

  /** send text message and navigate the screen */
  updateDataAndNavigate = () => {
    let { memberMqttId, userMqttId, postId } = this.props.boatDetail;
    let { username } = this.props.userProfileData;
    let newTime = new Date().getTime().toString();
    let obj = {
      name: username,
      from: userMqttId,
      to: memberMqttId,
      payload: btoa(this.state.input),
      id: newTime,
      timestamp: newTime,
      secretId: postId.toString(),
      type: "0",
    };
    askSeller(obj)
      .then((res) => {
        setTimeout(() => {
          Router.push("/messenger");
        }, 300);
        console.log("askSeller success", res);
      })
      .catch((err) => console.log("askSeller err", err));
    this.props.onNextClickNavigateToMessengerPage();
  };

  renderContentBasedOnPage = (pageNo) => {
    switch (pageNo) {
      case 0:
        return this.currentPageContent();
      case 1:
        return (
          <VideoCallAppointment
            closeDrawer={this.props.onClose}
            onClose={this.resetPage}
            boatDetail={this.props.boatDetail}
            nextPage={this.nextPage}
            isInPersonMeetingScreen={this.state.isInPersonMeetingScreen}
            resetToVideoAppointment={this.resetToVideoAppointment}
            setMeetingTimestamp={this.props.setMeetingTimestamp}
            isVideoMeeting={this.props.isVideoMeeting}
          />
        );
      case 2:
        return (
          <AcceptedMeetingInformation
            isInPersonMeetingScreen={this.state.isInPersonMeetingScreen}
            boatDetail={this.props.boatDetail}
            meetingTimestamp={this.props.meetingTimestamp}
            onClose={this.props.onClose}
            isVideoMeeting={this.props.isVideoMeeting}
            resetPage={this.resetPage}
          />
        );
      default:
        return this.currentPageContent();
    }
  };

  resetToVideoAppointment = () => {
    this.setState({ isInPersonMeetingScreen: false });
  };

  /** this function has the actual content of this page */
  currentPageContent = () => {
    return (
      <div className="col-12 MessagePostPage">
        <div className="closeIcon" onClick={this.props.onClose}>
          <img src={Close_Icon}></img>
        </div>

        <div className="drawerContent px-3">
          <h5 className="heading">Messenger</h5>
          <div className="row m-0 d-flex align-items-center">
            <div className="col-2 pl-0 pr-md-1">
              <img src={this.props.boatDetail.mainUrl} className="sellerProfilePic"></img>
            </div>
            <div className="col-5 p-0">
              <span className="sellerType">
                <img src={Professionals_White_Icon}></img>
                dealer
              </span>
              <h6 className="sellerName">{this.props.boatDetail.memberFullName}</h6>
            </div>
          </div>
          <div className="MessageContent_Sec">
            <div className="FormInput">
              <label>Message</label>
              <TextAreaBox
                type="textarea"
                className="textareaBox form-control"
                placeholder="I’m interested in your 2014 Cobia 220 Dual Console and would like more information. Thank you!"
                name="cust_message"
                value={this.state.input}
                onChange={(e) => this.setState({ input: e.target.value })}
                // onChange={this.handleOnchangeInput(`cust_message`)}
                autoComplete="off"
              ></TextAreaBox>
            </div>
            <div className="FormInput">
              {/* <label>Add message to the seller:</label> */}
              <ul className="list-unstyled d-flex flex-wrap align-items-center predictionMsgList">
                {textHelper.map((k, i) => (
                  <li key={i} id={`tx_${i}`} onClick={() => this.copyToInputText(i)}>
                    {k}
                  </li>
                ))}
              </ul>
            </div>
            <div className="Send_btn">
              <ButtonComp onClick={this.updateDataAndNavigate}>Send</ButtonComp>
            </div>
            <hr />
            <div className="mt-5 pt-3">
              <h6 className="title">Schedule an Appointment</h6>
              <div className="scheduleInPerson">
                <ButtonComp
                  onClick={() => {
                    this.setState({ isInPersonMeetingScreen: true });
                    this.nextPage();
                  }}
                >
                  <img src={User_White_Icon} className="phoneIcon"></img>
                  Schedule In-Person Meeting
                </ButtonComp>
              </div>
              <div className="scheduleVideoMeeting" onClick={this.nextPage}>
                <ButtonComp>
                  <img src={Video_Icon_White} className="phoneIcon"></img>
                  Schedule Video Meeting{" "}
                </ButtonComp>
              </div>
              <div className="scheduleDriveTest">
                <ButtonComp>
                  <img src={TestDrive} className="phoneIcon"></img>
                  Schedule Test Drive
                </ButtonComp>
              </div>
            </div>
            {/* <div className="mt-5 pt-3">
            <h6 className="title">Video Appointment</h6>
            <div className="videoAppointmentSec">
              <img src={Left_Top_Border} className="leftTop"></img>
              <img src={Right_Top_Border} className="RightTop"></img>
              <img src={Right_Bottom_Border} className="RightBottom"></img>
              <img src={Left_Bottom_Border} className="LeftBottom"></img>
              <img src={Battery_Icon} className="BatteryIcon"></img>
              <div className="recordSpan">
                <span></span>
                <p>Rec</p>
              </div>
              <div className="requestVideo_btn">
                <ButtonComp>
                  <img src={Video_Icon_White} className="videoIcon"></img>Request Video
                </ButtonComp>
              </div>
            </div>
          </div> */}
          </div>
        </div>
        <style jsx>
          {`
            .videoIcon {
              width: 1.024vw;
              margin-right: 0.366vw;
            }
            .recordSpan span {
              width: 0.732vw;
              height: 0.732vw;
              background: ${GREY_VARIANT_13};
              margin-right: 0.366vw;
              border-radius: 50%;
            }
            .recordSpan p {
              margin: 0;
              font-size: 0.951vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${GREY_VARIANT_2};
              text-transform: uppercase;
            }
            .recordSpan {
              position: absolute;
              top: 1.098vw;
              left: 1.317vw;
              display: flex;
              align-items: center;
            }
            .videoAppointmentSec {
              background: ${Border_LightGREY_COLOR};
              width: 100%;
              height: 10.98vw;
              position: relative;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .BatteryIcon {
              position: absolute;
              top: 1.464vw;
              right: 1.464vw;
              width: 1.244vw;
            }
            .leftTop {
              position: absolute;
              top: 0.732vw;
              left: 0.732vw;
              width: 2.196vw;
            }
            .RightTop {
              position: absolute;
              top: 0.732vw;
              right: 0.732vw;
              width: 2.196vw;
            }
            .RightBottom {
              position: absolute;
              bottom: 0.732vw;
              right: 0.732vw;
              width: 2.196vw;
            }
            .LeftBottom {
              position: absolute;
              bottom: 0.732vw;
              left: 0.732vw;
              width: 2.196vw;
            }
            :global(.MuiBackdrop-root) {
              background: linear-gradient(0deg, rgba(17, 39, 56, 0.7), rgba(17, 39, 56, 0.7));
            }
            .drawerContent {
              margin: 2.196vw 0 1.098vw 0;
            }
            .MessagePostPage {
              width: 25.622vw;
              padding: 1.098vw;
              position: relative;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.5625vw;
              cursor: pointer !important;
              height: 1.5625vw;
            }
            .heading {
              font-size: 1.464vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              color: ${FONTGREY_COLOR_Dark};
            }
            .sellerProfilePic {
              width: 100%;
              margin: 0.366vw 0;
            }
            .sellerType {
              background: ${THEME_COLOR};
              font-size: 0.439vw;
              color: ${WHITE_COLOR};
              text-transform: uppercase;
              padding: 0.219vw 0.585vw;
              font-family: "Open Sans" !important;
              border-radius: 0.219vw;
            }
            .sellerType img {
              width: 0.585vw;
              margin-bottom: 0.219vw;
              margin-right: 0.219vw;
            }
            .sellerName {
              font-size: 1.171vw;
              color: ${Dark_Blue_Color};
              margin-bottom: 0.219vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            .MessageContent_Sec .FormInput {
              margin: 1.098vw 0;
            }
            .MessageContent_Sec .FormInput label {
              font-size: 0.805vw;
              margin: 0;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.MessageContent_Sec .FormInput .textareaBox) {
              display: block;
              width: 100%;
              height: 7.32vw;
              padding: 0.439vw 0;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: none;
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR_2};
              border-radius: 0 !important;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 14px;
              transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.MessageContent_Sec .FormInput .textareaBox:focus) {
              color: ${FONTGREY_COLOR};
              background-color: #fff;
              border-color: ${THEME_COLOR};
              outline: 0;
              box-shadow: none;
            }
            :global(.textareaBox::placeholder) {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .Send_btn {
              display: flex;
              justify-content: flex-end;
            }
            :global(.requestVideo_btn button) {
              width: fit-content;
              padding: 0.366vw 0.732vw;
              background: ${GREY_VARIANT_2};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
              border-radius: 0 !important;
            }
            :global(.requestVideo_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              display: flex;
              align-items: center;
            }
            :global(.requestVideo_btn button:focus),
            :global(.requestVideo_btn button:active) {
              background: ${GREY_VARIANT_2};
              outline: none;
              box-shadow: none;
            }
            :global(.requestVideo_btn button:hover) {
              background: ${GREY_VARIANT_2};
            }

            :global(.Send_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0;
              text-transform: capitalize;
            }
            :global(.Send_btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
            }
            :global(.Send_btn button:focus),
            :global(.Send_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.Send_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .predictionMsgList {
              margin: 0.366vw 0 0.732vw 0;
            }
            .predictionMsgList li {
              background: ${Orange_Color};
              color: ${WHITE_COLOR};
              padding: 0.585vw 0.732vw;
              margin-right: 0.585vw;
              margin-top: 0.585vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 0.732vw;
            }
            .title {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.732vw;
            }
            :global(.scheduleInPerson button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.scheduleInPerson button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.scheduleInPerson button:focus),
            :global(.scheduleInPerson button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.scheduleInPerson button:hover) {
              background: ${GREY_VARIANT_9};
            }

            :global(.scheduleVideoMeeting button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Orange_Color};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.scheduleVideoMeeting button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.scheduleVideoMeeting button:focus),
            :global(.scheduleVideoMeeting button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.scheduleVideoMeeting button:hover) {
              background: ${GREY_VARIANT_9};
            }

            :global(.scheduleDriveTest button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.scheduleDriveTest button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.scheduleDriveTest button:focus),
            :global(.scheduleDriveTest button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.scheduleDriveTest button:hover) {
              background: ${GREY_VARIANT_9};
            }
            .phoneIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 0.951vw;
            }
          `}
        </style>
      </div>
    );
  };

  render() {
    return <Wrapper>{this.renderContentBasedOnPage(this.state.currentPage)}</Wrapper>;
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateBoatData: (data) => dispatch(storeBoatData(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageSellerModel);
