import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import {
  Close_Icon,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  GREEN_COLOR,
  FONTGREY_COLOR_Dark,
  ma_calendar,
  ma_clock,
  ma_map,
  ma_tick,
  ma_calendarV2,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import { sendAVCallRequest } from "../../services/calling";
import { getCookie } from "../../lib/session";
import moment from "moment";

class AcceptedMeetingInformation extends Component {
  componentDidMount() {
    // let { postId, memberMqttId } = this.props && this.props.boatDetail;
    // let obj = {
    //   fromId: getCookie("mqttId"),
    //   toId: memberMqttId,
    //   secretId: postId.toString(),
    //   docId: "xxx",
    // };

    let { receiverId, productId } = this.props.boatDetail || this.props.productDetail;
    let obj = {
      fromId: getCookie("mqttId"),
      toId: receiverId,
      secretId: productId,
      docId: "xxx",
    };
    sendAVCallRequest(obj)
      .then((res) => {
        console.log("res", res);
        // this.props.onClose();
      })
      .catch((err) => console.log("err", err));
  }

  render() {
    console.log("meetingTimestamp", this.props.meetingTimestamp);
    return (
      <Wrapper>
        <div className="py-3 MessagePostPage">
          <div
            className="closeIcon"
            onClick={() => {
              this.props.onClose();
              this.props.resetPage();
              this.props.isVideoMeeting(false);
            }}
          ></div>
          <img src={Close_Icon}></img>
          <div className="col-12 mt-5">
            <div className="row">
              <div className="greenCircle"></div>
            </div>
          </div>
          <div className="col-12 pt-3">
            <h3 className="congratulations">Congratulations!</h3>
            <p className="in_person_meeting_schedule">
              {this.props && this.props.isVideo ? "Video meeting scheduled!" : "In-person meeting scheduled!"}{" "}
            </p>
          </div>
          <div className="col-12 pt-3">
            <div className="row">
              <div className="col-2">
                <img src={ma_calendar} alt="calendar" />
              </div>
              <div className="col-10 infoText">
                {moment(this.props && this.props.meetingTimestamp).format("dddd")},{" "}
                {moment(this.props && this.props.meetingTimestamp).format("MMMM")}{" "}
                {moment(this.props && this.props.meetingTimestamp).format("D")}{" "}
                {moment(this.props && this.props.meetingTimestamp).format("HH")}:
                {moment(this.props && this.props.meetingTimestamp).format("mm")} EST
              </div>
            </div>
            <div className="row pt-2">
              <div className="col-2">
                <img src={ma_map} alt="map" />
              </div>
              <div className="col-10 infoText">
                {(this.props && this.props.boatDetail && this.props.boatDetail.place) ||
                  (this.props && this.props.productDetail && this.props.productDetail.place)}
              </div>
            </div>
            <div className="row pt-2">
              <div className="col-2">
                <img src={ma_clock} alt="clock" />
              </div>
              <div className="col-10 infoText">Typically 30 to 60 min</div>
            </div>
          </div>
          <div className="col-12 pt-3">
            <ButtonComp
              className="ma_complete"
              onClick={() => {
                this.props.onClose();
                this.props.resetPage();
                this.props.isVideoMeeting(false);
              }}
            >
              <img src={ma_tick} className="phoneIconV3"></img>
              Complete
            </ButtonComp>
            <ButtonComp className="ma_add_to_calendar pt-2">
              <img src={ma_calendarV2} className="phoneIconV4"></img>
              Add To My Calendar
            </ButtonComp>
          </div>
        </div>
        <style jsx>
          {`
            :global(.ma_add_to_calendar) {
              width: 100%;
              padding: 0.766vw 0;
              border-radius: 0px !important;
              background: ${WHITE_COLOR};
              border: 1px solid ${GREEN_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.ma_add_to_calendar span) {
              color: ${GREEN_COLOR};
              font-weight: 700 !important;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .closeIcon {
              position: absolute;
              top: 1.098vw;
              left: 1.098vw;
              width: 1.5625vw;
              cursor: pointer !important;
              height: 1.5625vw;
              z-index: 999 !important;
            }
            .MessagePostPage {
              width: 25.622vw;
              padding: 1.098vw;
              position: relative;
            }
            :global(.congratulations) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-weight: 700 !important;
              font-size: 1.45vw !important;
              color: ${FONTGREY_COLOR_Dark};
            }
            :global(.in_person_meeting_schedule) {
              color: ${GREY_VARIANT_2} !important;
              font-weight: 700 !important;
              font-size: 0.83 vw !important;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.ma_complete) {
              width: 100%;
              padding: 0.766vw 0;
              border-radius: 0px !important;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              border-radius: none !important;
              font-weight: 700;
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.ma_complete span) {
              color: ${WHITE_COLOR};
              font-weight: 700 !important;
            }
            :global(.greenCircle) {
              height: 100px;
              width: 100px;
              background: ${GREEN_COLOR};
              border-radius: 50%;
            }
            :global(.infoText) {
              color: ${FONTGREY_COLOR} !important;
              font-size: 0.83 vw !important;
            }
            :global(.phoneIconV3) {
              position: absolute;
              left: 0.732vw;
              top: 0.9vw;
              width: 1.3vw !important;
            }
            :global(.phoneIconV4) {
              position: absolute;
              left: 0.732vw;
              top: 0.4vw;
              width: 1.3vw !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default AcceptedMeetingInformation;
