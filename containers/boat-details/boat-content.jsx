import React, { Component } from "react";
import { connect } from "react-redux";

import Wrapper from "../../hoc/Wrapper";
import {
  GREY_VARIANT_1,
  GREEN_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_2,
  Map_Marker_Green,
  Border_LightGREY_COLOR,
  BOX_SHADOW_GREY,
  GREY_VARIANT_3,
  GREY_VARIANT_11,
  THEME_COLOR,
  Dark_Blue_Color,
  Reverse_Grey_Icon,
  User_Grey_Icon,
  Video_Grey_Icon,
  Chat_White_Icon,
  Phone_White_Icon,
  Orange_Color,
  GREY_VARIANT_9,
  Inventory_Grey_Icon,
  Insurance_Grey_Icon,
  Financing_Grey_Icon,
  History_Grey_Icon,
  Follow_Grey_Icon,
  Light_Blue_Color,
  Boat_Front_Icon_Grey,
  Phone_Grey_Icon,
  Map_Marker_Filled,
  Long_Arrow_Right,
  Share_Grey_Icon,
  BG_LightGREY_COLOR,
  THEME_COLOR_Opacity,
  Saved_Green_Icon,
  Wishlist_Icon,
  Profile_Logo,
  Dollar_Symbol_Grey_Icon,
  Question_Grey_Icon,
  PriceBg_Img,
} from "../../lib/config";
import ButtonComp from "../../components/button/button";
import CustomList from "../../components/custom-list/custom-list";
import NestedList from "../../components/custom-list/nested-list";
import StarRating from "../../components/rating/star-rating";
import MainDrawer from "../../components/Drawer/Drawer";
import Router from "next/router";
// Google Map Components
import GoogleMapReact from "google-map-react";
import MessageSellerModel from "./message-seller-model";
import Model from "../../components/model/model";
import SharePost from "./share-post";
import {
  unfollowPost,
  followPost,
  unfollowPeople,
  followPeople,
} from "../../services/following";
import currencies from "../../translations/currency.json";
import MakeOfferModel from "../product-details/make-offer-model";
import CustomLink from "../../components/Link/Link";
import {
  findDayAgo,
  formatDate,
} from "../../lib/date-operation/date-operation";
import InputBox from "../../components/input-box/input-box";
import { NumberValidator } from "../../lib/validation/validation";
import InfoTooltip from "../../components/tooltip/info-tooltip";
import { getCookie } from "../../lib/session";
import LoginPage from "../auth-modals/login/login-page";
import VideoCallAppointment from "./video-call-appointment";
import AcceptedMeetingInformation from "./accepted-meeting-info";
import Snackbar from "../../components/snackbar";
import debounce from "lodash.debounce";

const Marker = (props) => {
  const { color } = props;
  return (
    <div
      className="marker"
      style={{ backgroundColor: color, cursor: "pointer" }}
    />
  );
};

class BoatContent extends Component {
  state = {
    openPage: false,
    boat_details: true,
    boat_description: true,
    advanced_boat_details: false,
    dimensions_weights: false,
    engine_details: false,
    boatzon_insurance: true,
    boatzon_financing: true,
    boathistory_report: false,
    boat_transport: false,
    askSeller: false,
    sharePost: false,
    videoCallReq: false,
    makeOffer: false,
    inputpayload: {},
    loginPage: false,
    currentPage: 0,
    openVideoAppointment: false,
    openRegularMeeting: false,
    isInPersonMeetingScreen: false,
    isVideo: false,
    meetingTimestamp: "",
    callInfo: false,
  };

  setMeetingTimestamp = (time) => this.setState({ meetingTimestamp: time });

  resetToVideoAppointment = () => {
    this.setState({ isInPersonMeetingScreen: false });
  };

  isVideoMeeting = (bool) => this.setState({ isVideo: bool });

  toggleVideoAppointmentDrawer = () =>
    this.setState({
      openVideoAppointment: !this.state.openVideoAppointment,
    });

  toggleRegularMeetingDrawer = () =>
    this.setState({ openRegularMeeting: !this.state.openRegularMeeting });

  directNextPage = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
  };

  directGoBackPage = () => {
    console.log("clicked back / executed");
    if (this.state.currentPage === 0) return;
    this.setState({ currentPage: this.state.currentPage - 1 });
  };

  directResetPage = () => {
    console.log("clicked back / executed");
    this.setState({ currentPage: 0 });
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState(
      {
        AuthPass,
        Saved: this.props.boatDetail.likeStatus,
        Follow: this.props.boatDetail.followRequestStatus,
      },
      () => {
        if (this.props.boatDetail.engineCount > 0) {
          let engineDetails = [];
          for (let i = 1; i <= this.props.boatDetail.engineCount; i++) {
            let engineObject = {};
            engineObject.engine_make = this.props.boatDetail[`engineMake${i}`];
            engineObject.engine_model = this.props.boatDetail[
              `engineModel${i}`
            ];
            engineObject.engine_year = this.props.boatDetail[`engineYear${i}`];
            engineObject.engine_horse_power = this.props.boatDetail[
              `engineHorsePower${i}`
            ];
            engineObject.engine_hour = this.props.boatDetail[`engineHour${i}`];
            engineObject.engine_type = this.props.boatDetail[`engineType${i}`];
            engineObject.engine_fuel_type = this.props.boatDetail[
              `engineFuelType${i}`
            ];
            // engineObject.engine_warranty_till =
            engineDetails.push(engineObject);
          }
          this.setState({
            engineDetails,
          });
        }
      }
    );
  }

  handleListToggle = (name) => {
    this.setState({
      [name]: !this.state[name],
    });
  };

  handleAskSellerModel = () => {
    this.setState({
      askSeller: !this.state.askSeller,
    });
  };

  /** on click of this function, the drawer will close and router will navigate to messenger page */
  onNextClickNavigateToMessengerPage = () => {
    this.setState({
      askSeller: !this.state.askSeller,
    });
    setTimeout(() => {
      Router.push("/messenger");
    }, 500);
  };

  static defaultProps = {
    center: {
      lat: 28.000002,
      lng: -82.728244,
    },
    zoom: 11,
  };

  handleShareBoatPost = () => {
    this.setState({
      sharePost: !this.state.sharePost,
    });
  };

  // Function to redirect to login modal
  handleLogin = () => {
    this.setState({
      loginPage: !this.state.loginPage,
    });
  };

  // Function for the Notification (Snakbar)
  handleSnackbarClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleUnfollowPost = (postId) => {
    if (this.state.AuthPass) {
      let payload = {
        postId: postId.toString(),
        label: "Photo",
      };
      unfollowPost(payload)
        .then((res) => {
          console.log("res", res);
          let response = res.data;
          if (response) {
            if (response.code == 200) {
              this.setState({
                Saved: false,
                // usermessage: "Successfully Unsaved Post",
                // variant: "sucess",
                // open: true,
                // vertical: "bottom",
                // horizontal: "left",
              });
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
        });
    } else {
      this.handleLogin();
    }
  };

  handleFollowPost = (postId) => {
    if (this.state.AuthPass) {
      let payload = {
        postId: postId.toString(),
        label: "Photo",
      };
      followPost(payload)
        .then((res) => {
          console.log("res", res);
          let response = res.data;
          if (response) {
            if (response.code == 200) {
              this.setState({
                Saved: true,
                // usermessage: "Successfully Saved Post",
                // variant: "sucess",
                // open: true,
                // vertical: "bottom",
                // horizontal: "left",
              });
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
        });
    } else {
      this.handleLogin();
    }
  };

  handleUnfollowPeople = (personName) => {
    if (this.state.AuthPass) {
      let payload = {
        membername: personName,
      };
      unfollowPeople(payload)
        .then((res) => {
          console.log("resFollow", res);
          let response = res.data;
          if (response.code == 200) {
            this.setState({
              Follow: false,
              // usermessage: "Successfully UnFollowed",
              // variant: "sucess",
              // open: true,
              // vertical: "bottom",
              // horizontal: "left",
            });
          }
        })
        .catch((err) => {
          console.log("errFollow", err);
        });
    } else {
      this.handleLogin();
    }
  };

  handleFollowPeople = (personName) => {
    if (this.state.AuthPass) {
      let payload = {
        membername: personName,
      };
      followPeople(payload)
        .then((res) => {
          console.log("resFollow", res);
          let response = res.data;
          if (response.code == 200) {
            this.setState({
              Follow: true,
              // usermessage: "Successfully Followed",
              // variant: "sucess",
              // open: true,
              // vertical: "bottom",
              // horizontal: "left",
            });
          }
        })
        .catch((err) => {
          console.log("errFollow", err);
        });
    } else {
      this.handleLogin();
    }
  };

  /** function to toggle video call modal */
  toggleVideoCallReq = () =>
    this.setState({ videoCallReq: !this.state.videoCallReq });

  handleCurrencySymbol = (currency) => {
    let CurrencyArray = Object.keys(currencies).map((i) => currencies[i]);
    let currencySymbol = CurrencyArray.filter((item) => {
      return item.code == currency;
    }).map((data) => {
      return data.symbol_native;
    });
    return currencySymbol;
  };

  handleMakeOfferModel = () =>
    this.setState({ makeOffer: !this.state.makeOffer });

  /** function to toggle price dropdown */
  handleOnchangeInput = (event) => {
    let inputControl = event.target;
    if (inputControl.value < this.props.boatDetail && this.props.boatDetail.price) {
      this.setState(
        {
          [inputControl.name]: inputControl.value,
        },
        () => {
          this.handleLoanEstimation(this.state[inputControl.name]);
        }
      );
    }
  };

  handleLoanEstimation = debounce((value) => {
    // deductedPrice
    let price = this.props.boatDetail && this.props?.boatDetail.price;
    let deductedPrice = price - value;

    let intr = 4.5 / 1200;
    let term = 20 * 12; // 20 years
    let loanQuote =
      (deductedPrice * intr) / (1 - Math.pow(1 / (1 + intr), term));
    // let insuranceQuote;
    // if (deductedPrice >= 1 && deductedPrice < 150000) {
    //   insuranceQuote = (deductedPrice * 0.0388) / 12;
    // } else if (deductedPrice >= 150000) {
    //   insuranceQuote = (deductedPrice * 0.0263) / 12;
    // }
    this.setState({
      deductedPrice,
      estLoanQuote: loanQuote,
      // estInsuranceQuote: insuranceQuote,
    });
  }, 500);

  handleBuyWithBoatzon = (boatDetail) => {
    const ProductName =
      (boatDetail && boatDetail.productName) ||
      (boatDetail && boatDetail.title);
    if (this.state.AuthPass) {
      Router.push(
        `/buy-with-boatzon/${ProductName.replace(/ /g, "-")}?pid=${
          boatDetail.postId
        }`
      );
    } else {
      this.handleLogin();
    }
  };

  toggleCallInfo = (phoneNumber) => {
    this.setState({ callInfo: !this.state.callInfo });
  };

  render() {
    const {
      AuthPass,
      variant,
      usermessage,
      vertical,
      horizontal,
      open,
    } = this.state;
    const { boatDetail, userProfileData } = this.props;
    const ProductName = boatDetail && boatDetail.productName || boatDetail && boatDetail.title;
    const buyWithBoatzonMsg = (
      <p className="buyWithBoatzonMsg">
        {boatDetail && boatDetail.membername == userProfileData?.username
          ? "You can not buy your own products"
          : "Get preapproved to purchase this boat and get wholesale reates as a Boatzon member."}
      </p>
    );
    let price = boatDetail && boatDetail.price;
    let intr = 4.5 / 1200;
    let term = 20 * 12; // 20 years
    let loanQuote = (price * intr) / (1 - Math.pow(1 / (1 + intr), term));
    let insuranceQuote;
    if (price >= 1 && price < 150000) {
      insuranceQuote = (price * 0.0388) / 12;
    } else if (price >= 150000) {
      insuranceQuote = (price * 0.0263) / 12;
    }

    let BoatDetailsArr = [
      {
        label: "Condition",
        value: boatDetail && boatDetail.condition || "N/A",
      },
      { label: "Type", value: boatDetail && boatDetail.subCategory || "N/A" },
      {
        label: "Manufacturer",
        value: boatDetail && boatDetail.manufactor || "N/A",
      },
      { label: "Model", value: boatDetail && boatDetail.boatModel || "N/A" },
      { label: "Year", value: boatDetail && boatDetail.year || "N/A" },
      {
        label: "Length (In Feet)",
        value: boatDetail && boatDetail.length ? Math.round(boatDetail && boatDetail.length) : "N/A",
      },
      {
        label: "Location",
        value: `${boatDetail && boatDetail.city}, ${
          boatDetail && boatDetail.countrySname ? boatDetail.countrySname.toUpperCase() : ""
        }`,
      },
    ];

    let EngineDetailsArr = [
      {
        label: "Engine Make",
        value: boatDetail && boatDetail.engineMake1 || "N/A",
      },
      {
        label: "Engine Model",
        value: boatDetail && boatDetail.engineModel1 || "N/A",
      },
      {
        label: "Engine Year",
        value: boatDetail && boatDetail.engineYear1 || "N/A",
      },
      {
        label: "Horse Power",
        value: boatDetail && boatDetail.engineHorsePower1 || "N/A",
      },
      {
        label: "Engine Hours",
        value: boatDetail && boatDetail.engineHour1 || "N/A",
      },
      {
        label: "Number of Engines",
        value: boatDetail && boatDetail.engineCount || "N/A",
      },
      {
        label: "Engine Type",
        value: boatDetail && boatDetail.engineType1 || "N/A",
      },
      {
        label: "Fuel Type",
        value: boatDetail && boatDetail.engineFuelType1 || "N/A",
      },
      {
        label: "Warranty Till",
        value: boatDetail && boatDetail.warrantyDate1
          ? formatDate(boatDetail.warrantyDate1, "MMMM YYYY")
          : "No Warranty",
      },
    ];

    let AdvancedBoatDetailsArr = [];

    boatDetail && boatDetail.trailer
      ? AdvancedBoatDetailsArr.push({
          label: "Trailer",
          value: boatDetail && boatDetail.trailer,
        })
      : "";

    boatDetail && boatDetail.fuelType
      ? AdvancedBoatDetailsArr.push({
          label: "Fuel Type",
          value: boatDetail && boatDetail.fuelType,
        })
      : "";

    boatDetail && boatDetail.hullMaterial
      ? AdvancedBoatDetailsArr.push({
          label: "Hull Material",
          value: boatDetail && boatDetail.hullMaterial,
        })
      : "";

    boatDetail && boatDetail.hullShape
      ? AdvancedBoatDetailsArr.push({
          label: "Hull Shape",
          value: boatDetail && boatDetail.hullShape,
        })
      : "";

    let Dimensions_WeightsArr = [];

    boatDetail && boatDetail.nominalLength
      ? Dimensions_WeightsArr.push({
          label: "Nominal Length",
          value: boatDetail && boatDetail.nominalLength,
        })
      : "";

    boatDetail && boatDetail.overallLength
      ? Dimensions_WeightsArr.push({
          label: "Overall Length",
          value: boatDetail && boatDetail.overallLength,
        })
      : "";

    boatDetail && boatDetail.beam
      ? Dimensions_WeightsArr.push({
          label: "Beam",
          value: boatDetail && boatDetail.beam,
        })
      : "";

    boatDetail && boatDetail.draft
      ? Dimensions_WeightsArr.push({
          label: "Draft",
          value: boatDetail && boatDetail.draft,
        })
      : "";

    boatDetail && boatDetail.dryWeight
      ? Dimensions_WeightsArr.push({
          label: "Dry Weight",
          value: boatDetail && boatDetail.dryWeight,
        })
      : "";

    boatDetail && boatDetail.fuelTankCapacity
      ? Dimensions_WeightsArr.push({
          label: "Fuel Tank Capacity",
          value: boatDetail && boatDetail.fuelTankCapacity,
        })
      : "";

    boatDetail && boatDetail.heads
      ? Dimensions_WeightsArr.push({
          label: "Heads",
          value: boatDetail && boatDetail.heads,
        })
      : "";

    const { handleSnackbarClose } = this;
    console.log("[boatDetail]", boatDetail);
    return (
      <Wrapper>
        <div className="container p-md-0">
          <div className="screenWidth mx-auto py-4">
            <div className="row">
              <div className="col-11 mx-auto">
                <div className="row">
                  <div className="col-8">
                    <div className="positon-relative py-3">
                      <div className="row m-0 align-items-center justify-content-between">
                        <p className="publishedDate">
                          {`Posted`} {findDayAgo(boatDetail && boatDetail.postedOn)} {`in`}{" "}
                          <a target="_blank">
                            {boatDetail && boatDetail.category} / {boatDetail && boatDetail.subCategory}
                          </a>
                        </p>

                        <div className="d-flex align-items-center">
                          <div className="saved_btn">
                            <ButtonComp
                              onClick={
                                this.state.Saved
                                  ? this.handleUnfollowPost.bind(
                                      this,
                                      boatDetail && boatDetail.postId
                                    )
                                  : this.handleFollowPost.bind(
                                      this,
                                      boatDetail && boatDetail.postId
                                    )
                              }
                            >
                              <img
                                src={
                                  this.state.Saved
                                    ? Saved_Green_Icon
                                    : Wishlist_Icon
                                }
                                className="savedIcon"
                              ></img>
                              {this.state.Saved ? "Saved" : "Save"}
                            </ButtonComp>
                          </div>
                          <div className="share_btn">
                            <ButtonComp onClick={this.handleShareBoatPost}>
                              <img
                                src={Share_Grey_Icon}
                                className="shareIcon"
                              ></img>
                              Share
                            </ButtonComp>
                          </div>
                        </div>
                      </div>
                      <div className="row m-0 justify-content-between">
                        <div className="col-6 p-0">
                          <h4 className="postTitle">{ProductName}</h4>
                        </div>
                        <div className="col-6 p-0">
                          <div className="text-right d-flex flex-column">
                            <div className="price_btn">
                              <ButtonComp>
                                {this.handleCurrencySymbol(
                                  boatDetail && boatDetail.currency
                                )}{" "}
                                {boatDetail && boatDetail.price
                                  ? new Number(boatDetail.price).toLocaleString(
                                      "en-US"
                                    )
                                  : ""}
                              </ButtonComp>
                            </div>
                            <span className="emiOptions">
                              Buy From ${Math.round(loanQuote)} / Month
                            </span>
                            <span className="insuranceOption">
                              Insurance From ${Math.round(insuranceQuote)} /
                              Month
                            </span>
                          </div>
                        </div>
                      </div>
                      <p className="pickupLocation">
                        <img src={Map_Marker_Green}></img>
                        <a target="_blank" className="loation">
                          {boatDetail && boatDetail.city},{" "}
                          <span>
                            {boatDetail && boatDetail.countrySname
                              ? boatDetail && boatDetail.countrySname
                              : ""}
                          </span>
                        </a>{" "}
                        / Local Pickup (18 miles away)
                      </p>

                      <CustomList
                        name="Boat Details"
                        open={this.state.boat_details}
                        onClick={this.handleListToggle.bind(
                          this,
                          "boat_details"
                        )}
                      >
                        <table className="boatdetails_Sec">
                          <tbody>
                            {BoatDetailsArr &&
                              BoatDetailsArr.map((data) => (
                                <tr className="borderBottom">
                                  <th>
                                    <p>{data.label}</p>
                                  </th>
                                  <td>{data.value}</td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                      </CustomList>
                      {boatDetail && boatDetail.description && (
                        <CustomList
                          name="Boat Description"
                          open={this.state.boat_description}
                          onClick={this.handleListToggle.bind(
                            this,
                            "boat_description"
                          )}
                        >
                          <div className="boatdescription_Sec">
                            <div className="desc_Sec">
                              <p className="desc">{boatDetail && boatDetail.description}</p>
                            </div>
                          </div>
                        </CustomList>
                      )}
                      {boatDetail && boatDetail.engineCount > 0 ? (
                        <div className="position-relative">
                          <CustomList
                            name="Engine Details"
                            open={this.state.engine_details}
                            onClick={this.handleListToggle.bind(
                              this,
                              "engine_details"
                            )}
                          >
                            <table className="boatdetails_Sec">
                              <tbody>
                                {EngineDetailsArr &&
                                  EngineDetailsArr.map((data) => (
                                    <tr className="borderBottom">
                                      <th>
                                        <p>{data.label}</p>
                                      </th>
                                      <td>{data.value}</td>
                                    </tr>
                                  ))}
                              </tbody>
                            </table>
                          </CustomList>
                        </div>
                      ) : (
                        ""
                      )}

                      {AdvancedBoatDetailsArr.length > 0 && (
                        <CustomList
                          name="Advanced Boat Details"
                          open={this.state.advanced_boat_details}
                          onClick={this.handleListToggle.bind(
                            this,
                            "advanced_boat_details"
                          )}
                        >
                          <table className="boatdetails_Sec">
                            <tbody>
                              {AdvancedBoatDetailsArr &&
                                AdvancedBoatDetailsArr.map((data) => (
                                  <tr className="borderBottom">
                                    <th>
                                      <p>{data.label}</p>
                                    </th>
                                    <td>{data.value}</td>
                                  </tr>
                                ))}
                            </tbody>
                          </table>
                        </CustomList>
                      )}
                      {Dimensions_WeightsArr.length > 0 && (
                        <CustomList
                          name="Dimensions & Weights"
                          open={this.state.dimensions_weights}
                          onClick={this.handleListToggle.bind(
                            this,
                            "dimensions_weights"
                          )}
                        >
                          <table className="boatdetails_Sec">
                            <tbody>
                              {Dimensions_WeightsArr &&
                                Dimensions_WeightsArr.map((data) => (
                                  <tr className="borderBottom">
                                    <th>
                                      <p>{data.label}</p>
                                    </th>
                                    <td>{data.value}</td>
                                  </tr>
                                ))}
                            </tbody>
                          </table>
                        </CustomList>
                      )}
                      <div className="d-flex justify-content-end position-relative">
                        <div className="d-flex align-items-center buyBoat_BtnSec">
                          <div className="text-right mx-2">
                            <p className="m-0 priceValue">
                              ${Math.round(loanQuote)}
                            </p>
                            <p className="m-0 priceLabel">Est. Monthly</p>
                          </div>
                          <div className="buyBoat_Btn">
                            <ButtonComp
                              onClick={
                                AuthPass
                                  ? boatDetail &&
                                    userProfileData &&
                                    boatDetail.membername ==
                                      userProfileData.username
                                    ? ""
                                    : this.handleBuyWithBoatzon.bind(
                                        this,
                                        boatDetail
                                      )
                                  : this.handleLogin
                              }
                            >
                              <img
                                src={Long_Arrow_Right}
                                className="arrowRight"
                              ></img>{" "}
                              {"Start Purchase"}
                            </ButtonComp>
                          </div>
                          <InfoTooltip
                            tooltipContent={buyWithBoatzonMsg}
                            placement="top"
                            color={Light_Blue_Color}
                            disableHoverListener={!AuthPass ? true : false}
                          >
                            <img
                              src={Question_Grey_Icon}
                              className="questIcon"
                            ></img>
                          </InfoTooltip>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-4 rightSideSec">
                    <div className="row m-0 sellerDetails_Sec">
                      <div className="col-12 p-0">
                        <div className="sellerCard">
                          <div className="row m-0 pb-2 align-items-center">
                            <div className="col-3 pl-0 pr-md-1">
                              <img
                                src={
                                  boatDetail && boatDetail.memberProfilePicUrl
                                    ? boatDetail.memberProfilePicUrl
                                    : Profile_Logo
                                }
                                className="sellerProfilePic"
                              ></img>
                            </div>
                            <div className="col-9 p-0">
                              <div className="position-relative">
                                {/* {userProfileData && userProfileData.business ? (
                                  <span className="sellerType">
                                    <img src={Professionals_White_Icon}></img>
                                    dealer
                                  </span>
                                ) : (
                                  ""
                                )} */}
                                <h6 className="sellerName">
                                  {boatDetail && boatDetail.memberFullName}
                                </h6>
                                <StarRating
                                  startFontSize={"0.938vw"}
                                  value={4}
                                  disableHover={true}
                                  readOnly={true}
                                />
                              </div>
                              <div className="follow_btn">
                                <ButtonComp
                                  onClick={
                                    this.state.Follow
                                      ? this.handleUnfollowPeople.bind(
                                          this,
                                          boatDetail.membername
                                        )
                                      : this.handleFollowPeople.bind(
                                          this,
                                          boatDetail.membername
                                        )
                                  }
                                >
                                  <img
                                    src={
                                      this.state.Follow
                                        ? Follow_Grey_Icon
                                        : Follow_Grey_Icon
                                    }
                                    className="followIcon"
                                  ></img>
                                  {this.state.Follow ? "following" : "follow"}
                                </ButtonComp>
                              </div>
                            </div>
                          </div>
                          {/* <a href={`tel:${this.props && this.props.currentOrderStatus && this.props.currentOrderStatus.storePhone}`}></a> */}
                          {userProfileData && userProfileData.business == 1 ? (
                            this.state.callInfo ? (
                              <div className={"callInfo_btn"}>
                                <ButtonComp
                                  onClick={() => this.toggleCallInfo()}
                                >
                                  <img
                                    src={Phone_White_Icon}
                                    className="phoneIcon"
                                  ></img>
                                  <a
                                    style={{
                                      color: WHITE_COLOR,
                                      fontWeight: "bold",
                                      fontFamily: "Museo-Sans-Cyrl-Semibold",
                                      fontSize: "0.805vw",
                                    }}
                                    href={`tel:${userProfileData.phoneNumber}`}
                                  >
                                    Call {userProfileData.phoneNumber}
                                  </a>
                                </ButtonComp>
                              </div>
                            ) : (
                              <div className={"callDetails_btn"}>
                                <ButtonComp
                                  onClick={() => this.toggleCallInfo()}
                                >
                                  <img
                                    src={Phone_White_Icon}
                                    className="phoneIcon"
                                  ></img>
                                  Call For Details
                                </ButtonComp>
                              </div>
                            )
                          ) : (
                            ""
                          )}

                          <Wrapper>
                            <div className="chat_btn">
                              <ButtonComp
                                onClick={
                                  AuthPass
                                    ? boatDetail &&
                                      userProfileData &&
                                      boatDetail.membername ==
                                        userProfileData.username
                                      ? ""
                                      : this.handleAskSellerModel
                                    : this.handleLogin
                                }
                              >
                                <img
                                  src={Chat_White_Icon}
                                  className="phoneIcon"
                                ></img>
                                Ask
                              </ButtonComp>
                            </div>
                            {/* <div className="offer_btn">
                              <ButtonComp
                                onClick={
                                  AuthPass
                                    ? boatDetail &&
                                      userProfileData &&
                                      boatDetail.membername ==
                                        userProfileData.username
                                      ? ""
                                      : this.handleMakeOfferModel
                                    : this.handleLogin
                                }
                              >
                                <img
                                  src={Make_Offer_White_Icon}
                                  className="phoneIcon"
                                ></img>
                                Make Offer
                              </ButtonComp>
                            </div> */}
                          </Wrapper>

                          <div className="py-2">
                            <div className="video_btn">
                              <ButtonComp
                                onClick={
                                  AuthPass
                                    ? boatDetail &&
                                      userProfileData &&
                                      boatDetail.membername ==
                                        userProfileData.username
                                      ? ""
                                      : this.toggleVideoAppointmentDrawer
                                    : this.handleLogin
                                }
                              >
                                <img
                                  src={Video_Grey_Icon}
                                  className="phoneIcon"
                                ></img>
                                Video Appoinment
                              </ButtonComp>
                            </div>
                            <div className="video_btn">
                              <ButtonComp
                                onClick={
                                  AuthPass
                                    ? boatDetail &&
                                      userProfileData &&
                                      boatDetail.membername ==
                                        userProfileData.username
                                      ? ""
                                      : () => {
                                          this.toggleVideoAppointmentDrawer();
                                          this.setState({
                                            isInPersonMeetingScreen: true,
                                          });
                                        }
                                    : this.handleLogin
                                }
                              >
                                <img
                                  src={User_Grey_Icon}
                                  className="phoneIcon"
                                ></img>
                                In person Appointment
                              </ButtonComp>
                            </div>
                            <div className="video_btn">
                              <ButtonComp>
                                <img
                                  src={Reverse_Grey_Icon}
                                  className="phoneIcon"
                                ></img>
                                I Have a Trade-In
                              </ButtonComp>
                            </div>
                          </div>
                          <div className="inventory_btn">
                            <ButtonComp>
                              <img
                                src={Inventory_Grey_Icon}
                                className="phoneIcon"
                              ></img>
                              View All Inventory
                            </ButtonComp>
                          </div>
                        </div>

                        <div className="preApprovedCard">
                          <div className="row m-0 align-items-start justify-content-center priceDiv">
                            <div className="col-6 text-center p-0 px-2 whiteBorder">
                              <h3 className="priceHeading">
                                <img
                                  src={Dollar_Symbol_Grey_Icon}
                                  className="dollarIcon"
                                ></img>
                                <span>
                                  {this.state.estLoanQuote
                                    ? Math.round(this.state.estLoanQuote)
                                    : Math.round(loanQuote)}
                                </span>
                              </h3>
                              <p className="label">Estimated Monthly Payment</p>
                            </div>
                            <div className="col-6 text-center p-0 px-2">
                              <h3 className="priceHeading">
                                <img
                                  src={Dollar_Symbol_Grey_Icon}
                                  className="dollarIcon"
                                ></img>
                                <span>
                                  {this.state.cashDown
                                    ? this.state.cashDown
                                    : "0"}
                                </span>
                              </h3>
                              <p className="label">Cash Down</p>
                            </div>
                          </div>
                          <div className="section BorderBottom">
                            <div className="d-flex align-items-center justify-content-between sectionPadding">
                              <div>
                                <h5 className="labelName">Boat Price</h5>
                              </div>
                              <div>
                                <p className="labelValue">
                                  {this.handleCurrencySymbol(
                                    boatDetail && boatDetail.currency
                                  )}{" "}
                                  {this.state.cashDown
                                    ? new Number(
                                        this.state.deductedPrice
                                      ).toLocaleString("en-US")
                                    : boatDetail &&
                                      new Number(
                                        boatDetail.price
                                      ).toLocaleString("en-US")}
                                </p>
                              </div>
                            </div>
                            <div className="d-flex align-items-center justify-content-between sectionPadding">
                              <div>
                                <h5 className="labelName">Shipping</h5>
                              </div>
                              <div>
                                <p className="labelValue">N/A</p>
                              </div>
                            </div>
                            <div className="d-flex align-items-center justify-content-between sectionPadding">
                              <div>
                                <h5 className="labelName">Cash Down</h5>
                              </div>
                              <div>
                                <InputBox
                                  type="text"
                                  className="inputBox"
                                  name="cashDown"
                                  value={this.state.cashDown}
                                  onChange={this.handleOnchangeInput}
                                  onKeyPress={NumberValidator}
                                  autoComplete="off"
                                  placeholder="$0"
                                ></InputBox>
                              </div>
                            </div>
                          </div>
                          <div className="section">
                            <div className="d-flex align-items-center justify-content-between sectionPadding">
                              <div>
                                <h5 className="labelName">APR</h5>
                              </div>
                              <div>
                                <p className="labelValue">4.5%</p>
                              </div>
                            </div>
                            <div className="d-flex align-items-center justify-content-between sectionPadding">
                              <div>
                                <h5 className="labelName">
                                  Estimated Amount Financed
                                </h5>
                              </div>
                              <div>
                                <p className="labelValue">
                                  ${Math.round(loanQuote)}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="preApproved_Btn">
                            <ButtonComp>
                              <img
                                src={Long_Arrow_Right}
                                className="arrowRightIcon"
                              ></img>{" "}
                              Get Prequalified
                            </ButtonComp>
                          </div>
                        </div>
                        <div className="MapSec">
                          <GoogleMapReact
                            bootstrapURLKeys={{
                              key: "AIzaSyAtrnJwdRbJXfbsH4fr28N1TJG64c7Lrc4",
                            }}
                            center={{
                              lat:
                                (boatDetail && boatDetail.latitude) ||
                                28.000002,
                              lng:
                                (boatDetail && boatDetail.longitude) ||
                                -82.728244,
                            }}
                            zoom={6}
                            yesIWantToUseGoogleMapApiInternals
                          >
                            <Marker
                              lat={
                                (boatDetail && boatDetail.latitude) || 28.000002
                              }
                              lng={
                                (boatDetail && boatDetail.longitude) ||
                                -82.728244
                              }
                              color={THEME_COLOR_Opacity}
                            />
                          </GoogleMapReact>
                          <div className="loacationCard p-3 mx-2">
                            <div className="col-12 p-0">
                              <div className="row m-0 borderBottom align-items-center">
                                <div className="col-1 p-0">
                                  <img
                                    src={Phone_Grey_Icon}
                                    className="contactIcon"
                                  ></img>
                                </div>
                                <div className="col-11 p-0">
                                  <p className="sellerContactNum">
                                    {(boatDetail &&
                                      boatDetail.memberPhoneNumber) ||
                                      "(888) 719-3246"}
                                  </p>
                                </div>
                              </div>
                              <div className="row m-0 pt-3">
                                <div className="col-1 p-0">
                                  <img
                                    src={Map_Marker_Filled}
                                    className="mapmarkerIcon"
                                  ></img>
                                </div>
                                <div className="col-11 p-0">
                                  <div className="addressDetails">
                                    {/* <h6>Marine Trader, Inc</h6> */}
                                    <p>{boatDetail && boatDetail.place}</p>
                                    <a target="_blank">View on map</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="boatzonServices_Sec py-3">
                          <h6 className="serviceHeading">Boatzon Services</h6>
                          <NestedList
                            name={
                              <p className="m-0">
                                <img
                                  src={Insurance_Grey_Icon}
                                  className="insuranceIcon"
                                ></img>
                                <span className="mx-2 serviceTitle">
                                  Boatzon Insurance
                                </span>
                              </p>
                            }
                            open={this.state.boatzon_insurance}
                            onClick={this.handleListToggle.bind(
                              this,
                              "boatzon_insurance"
                            )}
                          >
                            <div className="boatzonInsurance_Sec">
                              <p>
                                This boat qualifies for Boatzon Insurance. We
                                offer wholesale insurance prices to members.
                              </p>
                              <CustomLink
                                href={`/insurance/${ProductName.replace(
                                  / /g,
                                  "-"
                                )}?pid=${boatDetail.postId}`}
                              >
                                <ButtonComp>Get Insurance Now</ButtonComp>
                              </CustomLink>
                            </div>
                          </NestedList>
                          <NestedList
                            name={
                              <p className="m-0">
                                <img
                                  src={Financing_Grey_Icon}
                                  className="financingIcon"
                                ></img>
                                <span className="mx-2 serviceTitle">
                                  Boatzon Financing
                                </span>
                              </p>
                            }
                            open={this.state.boatzon_financing}
                            onClick={this.handleListToggle.bind(
                              this,
                              "boatzon_financing"
                            )}
                          >
                            <div className="boatzonFinancing_Sec">
                              <p>
                                Financing this boat with us in a few clicks and
                                get the lowest payment as a Boatzon member
                              </p>
                              <CustomLink
                                href={`/loan-financing/${ProductName.replace(
                                  / /g,
                                  "-"
                                )}?pid=${boatDetail.postId}`}
                              >
                                <ButtonComp>Finance this boat now</ButtonComp>
                              </CustomLink>
                            </div>
                          </NestedList>
                          <NestedList
                            name={
                              <p className="m-0">
                                <img
                                  src={History_Grey_Icon}
                                  className="historyIcon"
                                ></img>
                                <span className="mx-2 serviceTitle">
                                  Boat History Report
                                </span>
                              </p>
                            }
                            open={this.state.boathistory_report}
                            onClick={this.handleListToggle.bind(
                              this,
                              "boathistory_report"
                            )}
                          ></NestedList>
                          <NestedList
                            name={
                              <p className="m-0">
                                <img
                                  src={Boat_Front_Icon_Grey}
                                  className="boatIcon"
                                ></img>
                                <span className="mx-2 serviceTitle">
                                  Boat Transport
                                </span>
                              </p>
                            }
                            open={this.state.boat_transport}
                            onClick={this.handleListToggle.bind(
                              this,
                              "boat_transport"
                            )}
                          ></NestedList>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Login Model */}
        <Model
          open={this.state.loginPage}
          onClose={this.handleLogin}
          authmodals={true}
        >
          <LoginPage onClose={this.handleLogin} />
        </Model>

        {/* Create Post Drawer */}
        <MainDrawer
          open={this.state.askSeller}
          onClose={this.handleAskSellerModel}
          anchor="right"
        >
          <MessageSellerModel
            boatDetail={this.props.boatDetail}
            onClose={this.handleAskSellerModel}
            setMeetingTimestamp={this.setMeetingTimestamp}
            meetingTimestamp={this.state.meetingTimestamp}
            isVideoMeeting={this.isVideoMeeting}
            onNextClickNavigateToMessengerPage={
              this.onNextClickNavigateToMessengerPage
            }
          />
        </MainDrawer>

        <MainDrawer
          open={this.state.openVideoAppointment}
          onClose={this.toggleVideoAppointmentDrawer}
          anchor="right"
        >
          {this.state.currentPage === 0 ? (
            <VideoCallAppointment
              boatDetail={this.props.boatDetail}
              onClose={this.toggleVideoAppointmentDrawer}
              isDirect={true}
              directGoBackPage={this.directGoBackPage}
              directNextPage={this.directNextPage}
              directResetPage={this.directResetPage}
              isInPersonMeetingScreen={this.state.isInPersonMeetingScreen}
              isVideoMeeting={this.isVideoMeeting}
              setMeetingTimestamp={this.setMeetingTimestamp}
            />
          ) : this.state.currentPage === 1 ? (
            <AcceptedMeetingInformation
              isDirect={true}
              boatDetail={this.props.boatDetail}
              onClose={this.toggleVideoAppointmentDrawer}
              resetPage={this.directResetPage}
              isVideo={this.state.isVideo}
              isVideoMeeting={this.isVideoMeeting}
              meetingTimestamp={this.state.meetingTimestamp}
            />
          ) : (
            ""
          )}
        </MainDrawer>

        <Model open={this.state.makeOffer} onClose={this.handleMakeOfferModel}>
          <MakeOfferModel
            onClose={this.handleMakeOfferModel}
            boatDetail={this.props.boatDetail}
            handleReviewOfferPage={this.props.handleReviewOfferPage}
          />
        </Model>

        {/** request for video call */}
        {/* <Model open={this.state.videoCallReq} onClose={this.toggleVideoCallReq}>
          <RequestVideoCallModal
            type="Request"
            onClose={this.toggleVideoCallReq}
            boatDetail={this.props.boatDetail}
          />
        </Model> */}

        {/* Login Model */}
        <Model open={this.state.sharePost} onClose={this.handleShareBoatPost}>
          <SharePost onClose={this.handleShareBoatPost} />
        </Model>

        {/* Snakbar Components */}
        <Snackbar
          variant={variant}
          message={usermessage}
          open={open}
          onClose={handleSnackbarClose}
          vertical={vertical}
          horizontal={horizontal}
        />

        <style jsx>
          {`
            :global(.buyWithBoatzonMsg) {
              font-size: 0.658vw;
              font-family: "Open Sans" !important;
              color: ${WHITE_COLOR};
              margin: 0;
              padding: 0.366vw;
            }
            .loation {
              text-transform: capitalize;
            }
            .loation span,
            #countryName {
              text-transform: uppercase;
            }
            :global(.offer_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.offer_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.offer_btn button:focus),
            :global(.offer_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.offer_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.share_btn button) {
              width: 4.026vw;
              min-width: 4.026vw;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_2};
              margin: 0;
              text-align: right;
              text-transform: capitalize;
              position: relative;
              box-shadow: none;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
            }
            :global(.share_btn button span) {
              width: fit-content;
              margin: 0 0 0 auto;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              text-align: right;
              float: right;
              justify-content: flex-end;
            }
            :global(.share_btn button:focus),
            :global(.share_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.share_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .savedIcon,
            .shareIcon {
              width: 0.732vw;
              margin-right: 0.366vw;
              margin-top: 0.0732vw;
            }
            :global(.saved_btn button) {
              width: 4.026vw;
              min-width: 4.026vw;
              text-align: right !important;
              padding: 0.439vw 0;
              background: ${WHITE_COLOR};
              color: ${GREEN_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
              box-shadow: none;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
            }
            :global(.saved_btn button span) {
              width: fit-content;
              margin: 0 0 0 auto;
              font-family: "Open Sans" !important;
              font-size: 0.732vw;
              text-align: right;
              float: right;
              justify-content: flex-end;
            }
            :global(.saved_btn button:focus),
            :global(.saved_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.saved_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .insuranceIcon,
            .financingIcon,
            .boatIcon {
              width: 1.024vw;
              margin-bottom: 0.146vw;
            }
            .historyIcon {
              width: 0.732vw;
              margin-bottom: 0.146vw;
            }
            :global(.MapSec > div > div > div) {
              top: -3.66vw !important;
            }
            :global(.gm-control-active, .gm-fullscreen-control) {
              top: 3.66vw !important;
            }
            :global(.marker) {
              position: absolute;
              top: 50%;
              left: 50%;
              width: 5.124vw;
              height: 5.124vw;
              background-color: #000;
              border-radius: 100%;
              user-select: none;
              transform: translate(-50%, -50%);
            }
            :global(.marker:hover) {
              z-index: 1;
            }
            .serviceTitle {
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
              font-weight: normal;
              color: ${FONTGREY_COLOR};
            }
            .followIcon {
              width: 0.521vw;
              margin-right: 0.366vw;
            }
            .loacationCard {
              background: ${WHITE_COLOR};
              position: absolute;
              bottom: 0.732vw;
              width: 95%;
            }
            .arrowRight {
              width: 1.098vw;
              margin-right: 0.585vw;
            }
            .arrowRightIcon {
              position: absolute;
              top: 50%;
              left: 1.464vw;
              transform: translate(0, -50%);
              width: 1.098vw;
            }
            .MapSec {
              width: 100%;
              height: 24.158vw;
              margin-top: 1.464vw;
              position: relative;
            }
            .contactIcon {
              width: 0.878vw;
              margin-bottom: 0.658vw;
            }
            .mapmarkerIcon {
              width: 0.732vw;
              margin-bottom: 0.658vw;
            }
            .addressDetails h6 {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.658vw;
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
            }
            .addressDetails p {
              font-size: 0.805vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.366vw;
              max-width: 14.641vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .addressDetails a {
              font-size: 0.805vw;
              text-decoration: none;
              color: ${THEME_COLOR};
              cursor: pointer;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .sellerContactNum {
              font-size: 0.951vw;
              color: ${FONTGREY_COLOR};
              margin-bottom: 0.658vw;
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
            }
            .publishedDate {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_1};
              margin-bottom: 10px;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0073vw !important;
            }
            .publishedDate:first-letter {
              text-transform: capitalize;
            }
            .publishedDate a {
              font-size: 0.732vw;
              color: ${GREEN_COLOR};
              text-transform: capitalize;
              text-decoration: none;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0073vw !important;
            }
            .buyBoat_BtnSec {
              position: fixed;
              bottom: 0;
              left: 49.3%;
              transform: translateX(-50%);
              z-index: 1;
              background: #f5f5f5;
              width: 23.177vw;
              // box-shadow: 0px 0px 4px 1px ${BOX_SHADOW_GREY} !important;
              padding: 0.366vw;
            }
            .priceValue {
              font-size: 1.094vw;
              color: ${GREEN_COLOR};
              font-family: "Museo-Sans-Cyrl-SemiBold" !important;
              text-align: center;
            }
            .priceLabel {
              font-size: 0.833vw;
              color: ${GREEN_COLOR};
              font-family: "Open Sans-SemiBold" !important;
            }
            .questIcon {
              margin: 0 0.585vw;
              cursor: pointer;
            }
            :global(.buyBoat_Btn button) {
              width: 100%;
              padding: 0.439vw 1.098vw;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.buyBoat_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.951vw;
            }
            :global(.buyBoat_Btn button:focus),
            :global(.buyBoat_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.buyBoat_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .postTitle {
              font-size: 1.405vw;
              color: ${FONTGREY_COLOR};
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              margin-bottom: 0.732vw;
              max-width: 90%;
            }
            :global(.price_btn button) {
              width: fit-content;
              min-width: fit-content;
              padding: 0.146vw 0.585vw;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.366vw 0;
              text-transform: capitalize;
              position: relative;
              font-weight: 500;
            }
            :global(.price_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              letter-spacing: 0.0219vw !important;
              font-size: 1.024vw;
            }
            :global(.price_btn button:focus),
            :global(.price_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.price_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .emiOptions {
              font-size: 0.732vw;
              color: ${GREEN_COLOR};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .insuranceOption {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .pickupLocation {
              align-items: center;
              font-size: 0.878vw;
              color: ${GREY_VARIANT_1};
              font-family: "Open Sans" !important;
              letter-spacing: 0.0146vw !important;
            }
            .pickupLocation img {
              width: 0.732vw;
              margin-right: 0.219vw;
              margin-bottom: 0.0732vw;
            }
            .pickupLocation a {
              color: ${GREEN_COLOR};
              font-size: 0.878vw;
              font-family: "Open Sans-SemiBold" !important;
              letter-spacing: 0.0146vw !important;
            }
            .boatdetails_Sec {
              width: 100%;
            }
            .engineLabel {
              font-size: 0.915vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin: 0.878vw 0 0.585vw 0;
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .boatdetails_Sec th {
              width: 30%;
              font-weight: normal;
            }
            .boatdetails_Sec th p {
              font-size: 0.841vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-bottom: 0.219vw;
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .boatdetails_Sec td {
              width: 70%;
              color: ${GREY_VARIANT_2};
              font-size: 0.841vw;
              text-transform: capitalize;
              font-family: "Open Sans" !important;
              padding: 0.366vw 1.171vw;
            }
            .borderBottom {
              border-bottom: 0.0732vw solid ${Border_LightGREY_COLOR};
            }
            .borderBottom1 {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .desc_Sec {
              padding: 0 0 1.464vw 0;
            }
            .descHeading {
              font-size: 0.841vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-bottom: 0.219vw;
              font-family: "Open Sans-Semibold" !important;
              letter-spacing: 0.0219vw !important;
            }
            .desc {
              font-size: 0.768vw;
              color: ${FONTGREY_COLOR};
              text-transform: initial;
              margin-bottom: 0;
              opacity: 0.8;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            .rightSideSec {
              max-width: 21.51vw;
            }
            .sellerDetails_Sec {
              position: relative;
              top: -5.124vw;
              z-index: 0;
              width: 100%;
            }
            .sellerCard {
              border: 0.0732vw solid ${GREY_VARIANT_3};
              box-shadow: 0px 0px 0.5px 0px ${BOX_SHADOW_GREY} !important;
              padding: 1.464vw 1.83vw;
              background: ${WHITE_COLOR};
            }
            .sellerProfilePic {
              width: 3.294vw;
              height: 3.294vw;
              border-radius: 50%;
              object-fit: cover;
            }
            .sellerType {
              background: ${THEME_COLOR};
              font-size: 0.439vw;
              color: ${BG_LightGREY_COLOR};
              text-transform: uppercase;
              padding: 0.146vw 0.292vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
              border-radius: 0.219vw;
            }
            .sellerType img {
              width: 0.439vw;
              margin-bottom: 0.146vw;
              margin-right: 0.219vw;
            }
            .sellerName {
              font-size: 1.024vw;
              color: ${Dark_Blue_Color};
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            .follow_btn {
              position: absolute;
              bottom: 3px;
              right: 0;
            }
            :global(.follow_btn button) {
              width: fit-content;
              padding: 0.219vw 0.292vw;
              background: ${Border_LightGREY_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0;
              text-transform: capitalize;
              font-size: 0.625vw;
              font-family: "Open Sans" !important;
            }
            :global(.follow_btn button span) {
              font-size: 0.625vw;
              font-family: "Open Sans" !important;
            }
            :global(.follow_btn button:focus),
            :global(.follow_btn button:active) {
              background: ${Border_LightGREY_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.follow_btn button:hover) {
              background: ${Border_LightGREY_COLOR};
            }
            .phoneIcon {
              position: absolute;
              left: 0.732vw;
              top: 0.732vw;
              width: 0.951vw;
            }
            :global(.callDetails_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${THEME_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.callDetails_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.callDetails_btn button:focus),
            :global(.callDetails_btn button:active) {
              background: ${THEME_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.callDetails_btn button:hover) {
              background: ${THEME_COLOR};
            }

            :global(.callInfo_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.callInfo_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.callInfo_btn button:focus),
            :global(.callInfo_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.callInfo_btn button:hover) {
              background: ${GREEN_COLOR};
            }

            :global(.chat_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${Orange_Color};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
            }
            :global(.chat_btn button span) {
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              font-size: 0.805vw;
            }
            :global(.chat_btn button:focus),
            :global(.chat_btn button:active) {
              background: ${Orange_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.chat_btn button:hover) {
              background: ${Orange_Color};
            }
            :global(.video_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${GREY_VARIANT_9};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
              position: relative;
            }
            :global(.video_btn button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.video_btn button:focus),
            :global(.video_btn button:active) {
              background: ${GREY_VARIANT_9};
              outline: none;
              box-shadow: none;
            }
            :global(.video_btn button:hover) {
              background: ${GREY_VARIANT_9};
            }
            :global(.inventory_btn button) {
              width: 100%;
              padding: 0.366vw 0;
              background: ${WHITE_COLOR};
              color: ${GREY_VARIANT_1};
              margin: 0.732vw 0 0.732vw 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
              border: 0.146vw solid ${GREY_VARIANT_2};
            }
            :global(.inventory_btn button span) {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.805vw;
            }
            :global(.inventory_btn button:focus),
            :global(.inventory_btn button:active) {
              background: ${WHITE_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.inventory_btn button:hover) {
              background: ${WHITE_COLOR};
            }
            .serviceHeading {
              font-size: 1.024vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              padding: 1.098vw 0;
              font-family: "Museo-Sans-Cyrl-Semibold" !important;
              margin: 0;
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .boatzonInsurance_Sec p,
            .boatzonFinancing_Sec p {
              font-size: 0.768vw;
              color: ${FONTGREY_COLOR};
              text-transform: capitalize;
              margin-botton: 0.366vw;
              font-family: "Open Sans" !important;
              letter-spacing: 0.0219vw !important;
            }
            :global(.boatzonInsurance_Sec button) {
              width: 70%;
              padding: 0.585vw 0;
              background: ${GREEN_COLOR};
              color: ${GREY_VARIANT_11};
              margin: 0.732vw 0 0.732vw 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.boatzonInsurance_Sec button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.boatzonInsurance_Sec button:focus),
            :global(.boatzonInsurance_Sec button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.boatzonInsurance_Sec button:hover) {
              background: ${GREEN_COLOR};
            }
            :global(.boatzonFinancing_Sec button) {
              width: 70%;
              padding: 0.585vw 0;
              background: ${Light_Blue_Color};
              color: ${GREY_VARIANT_11};
              margin: 0.732vw 0 0.732vw 0;
              text-transform: capitalize;
              font-weight: 500;
              position: relative;
            }
            :global(.boatzonFinancing_Sec button span) {
              font-family: "Museo-Sans" !important;
              font-size: 0.805vw;
            }
            :global(.boatzonFinancing_Sec button:focus),
            :global(.boatzonFinancing_Sec button:active) {
              background: ${Light_Blue_Color};
              outline: none;
              box-shadow: none;
            }
            :global(.boatzonFinancing_Sec button:hover) {
              background: ${Light_Blue_Color};
            }
            .preApprovedCard {
              margin-top: 1.464vw;
              border: 1px solid ${GREY_VARIANT_3};
              box-sizing: border-box;
              border-radius: 0.146vw;
            }
            .priceDiv {
              background: url(${PriceBg_Img});
              background-repeat: no-repeat;
              background-size: cover;
              background-position: center center;
              padding: 0.878vw 0;
              border-radius: 0.146vw 0.146vw 0 0;
              height: 6.146vw;
            }
            .whiteBorder {
              border-right: 1px solid ${WHITE_COLOR};
            }
            .priceHeading {
              margin: 0;
              display: flex;
              align-items: center;
              justify-content: center;
            }
            .priceHeading span {
              font-family: "Museo-Sans" !important;
              font-size: 1.667vw;
              font-weight: 600;
              color: ${BG_LightGREY_COLOR};
            }
            .dollarIcon {
              height: 1.317vw;
            }
            .label {
              margin: 0;
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.833vw;
              color: ${WHITE_COLOR};
              opacity: 0.7;
              line-height: 1;
            }
            .sectionPadding {
              padding: 0.658vw 0 0 0;
            }
            .section {
              padding: 0.585vw 0 0.732vw 0;
              margin: 0 0.878vw;
            }
            .BorderBottom {
              border-bottom: 0.0732vw solid ${GREY_VARIANT_3};
            }
            .labelName {
              font-family: "Museo-Sans" !important;
              font-size: 0.878vw;
              font-weight: 600;
              color: ${FONTGREY_COLOR};
              margin: 0;
            }
            .labelValue {
              font-family: "Open Sans-SemiBold" !important;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_2};
              margin: 0;
            }
            :global(.sectionPadding .inputBox) {
              display: block;
              max-width: 7.32vw !important;
              padding: 0.292vw 0.366vw 0.292vw 0.366vw;
              line-height: 1.5;
              background-color: #fff;
              background-clip: padding-box;
              border: 0.0732vw solid ${GREY_VARIANT_3};
              border-radius: 0.146vw;
              box-sizing: border-box;
              transition: border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              font-family: "Open Sans" !important;
            }
            :global(.sectionPadding .inputBox:focus) {
              color: ${GREY_VARIANT_2};
              background-color: #fff;
              outline: 0;
              box-shadow: none;
            }
            :global(.sectionPadding .inputBox::placeholder) {
              font-size: 0.732vw;
              color: ${GREY_VARIANT_2};
              text-transform: capitalize;
              font-family: "Open Sans" !important;
            }
            .preApproved_Btn {
              text-align: center;
              position: relative;
            }
            :global(.preApproved_Btn button) {
              width: 92%;
              padding: 0.439vw 1.098vw;
              background: ${GREEN_COLOR};
              color: ${WHITE_COLOR};
              margin: 0.366vw 0 0.878vw 0;
              text-transform: capitalize;
              position: relative;
              border-radius: 0.146vw;
            }
            :global(.preApproved_Btn button span) {
              font-family: "Museo-Sans" !important;
              font-weight: 600;
              font-size: 0.878vw;
            }
            :global(.preApproved_Btn button:focus),
            :global(.preApproved_Btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.preApproved_Btn button:hover) {
              background: ${GREEN_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(BoatContent);
