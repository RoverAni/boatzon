import React from "react";
import Wrapper from "../../hoc/Wrapper";
import Button from "../../components/button/customButton";
import { sendAVCallRequest, acceptOrRejectVideoCallRequest, callUser, initiateCall } from "../../services/calling";
import { getCookie } from "../../lib/session";
import { storeWebrtcData } from "../../redux/actions/webrtc/webrtc";
import { userSelectedToChat } from "../../redux/actions/chat/chat";
import { connect } from "react-redux";

class RequestVideoCallModal extends React.Component {
  /** function to send video call request to seller */
  SendAVCallRequest = () => {
    if (this.props.type === "ChatRequest") {
      let { productId, receiverId } = this.props && this.props.userSelectedToChat;
      let obj = {
        fromId: getCookie("mqttId"),
        toId: receiverId,
        secretId: productId,
        docId: "xxx",
      };
      sendAVCallRequest(obj)
        .then((res) => {
          console.log("res", res);
          this.props.onClose();
        })
        .catch((err) => console.log("err", err));
    } else {
      let { postId, memberMqttId } = this.props && this.props.boatDetail;
      let obj = {
        fromId: getCookie("mqttId"),
        toId: memberMqttId,
        secretId: postId.toString(),
        docId: "xxx",
      };
      sendAVCallRequest(obj)
        .then((res) => {
          console.log("res", res);
          this.props.onClose();
        })
        .catch((err) => console.log("err", err));
    }
  };

  /** function to accept video call request as a buyer
   * status 1: accept, 2: reject
   */
  AcceptOrRejectVideoCallRequest = (status) => {
    let { _id, secretId } = this.props.userSelectedToChat;
    let existingUserObj = { ...this.props.userSelectedToChat };
    let obj = {
      requestId: _id,
      secretId: secretId,
      docId: "xxx",
      status: status,
    };
    acceptOrRejectVideoCallRequest(obj)
      .then((res) => {
        if (obj.status === 2) {
          existingUserObj["status"] = 2; // rejected
          this.props._updateUserSelectedToChat(existingUserObj);
        } else if (obj.status === 1) {
          existingUserObj["status"] = 1; // accepted
          this.props._updateUserSelectedToChat(existingUserObj);
        }
        console.log("res", res);
        this.props.onClose();
      })
      .catch((err) => console.log("err", err));
  };

  /** this function is to initiate video call with target user */
  startCall = () => {
    let newRoomId = new Date().getTime().toString();
    let obj = {
      type: "video",
      room: newRoomId,
      to: this.props.userSelectedToChat.recipientId,
      // callId: this.props.userSelectedToChat.meAsABuyerCallID,
    };
    console.log("startCall");
    callUser(obj)
      .then((res) => {
        let newObj = { ...res.data.data };
        newObj["callId"] = res.data.data.callId;
        newObj["twilioToken"] = res.data.data.twilioToken;
        newObj["room"] = newRoomId;
        this.props._storeWebrtcData(newObj);
      })
      .catch((err) => console.log("err", err));
  };

  toRenderView = (type) => {
    console.log("MMMMM", type);
    switch (type) {
      case "Request":
        return (
          <div className="col-12 p-4">
            <div className="row">
              <div className="col-12 main_title text-center mb-4">Request video call ?</div>
            </div>
            <div className="row">
              <div className="col-6 text-center">
                <Button className="req_video_call_btn" text={"Yes"} handler={this.SendAVCallRequest} />
              </div>
              <div className="col-6 text-center">
                <Button className="req_video_call_btn_accept" text={"No"} handler={this.props.onClose} />
              </div>
            </div>
          </div>
        );
      case "ChatRequest":
        return (
          <div className="col-12 p-4">
            <div className="row">
              <div className="col-12 main_title text-center mb-4">Request video call ?</div>
            </div>
            <div className="row">
              <div className="col-6 text-center">
                <Button className="req_video_call_btn" text={"Yes"} handler={this.SendAVCallRequest} />
              </div>
              <div className="col-6 text-center">
                <Button className="req_video_call_btn_accept" text={"No"} handler={this.props.onClose} />
              </div>
            </div>
          </div>
        );
      case "AcceptOrReject":
        return (
          <div className="col-12 p-4">
            <div className="row">
              <div className="col-12 main_title text-center mb-4">Do you want to initiate call with buyer</div>
            </div>
            <div className="row">
              <div className="col-6 text-center">
                <Button className="req_video_call_btn" text={"Yes"} handler={() => this.startCall()} />
              </div>
              <div className="col-6 text-center">
                <Button className="req_video_call_btn_accept" text={"No"} handler={() => this.props.onClose()} />
              </div>
            </div>
          </div>
        );
      case "AsSellerAcceptIncomingCallRequest":
        return (
          <div className="col-12 p-4">
            <div className="row">
              <div className="col-12 main_title text-center mb-4">
                Do you want to accept call request from {this.props.userSelectedToChat.userName} ?
              </div>
            </div>
            <div className="row">
              <div className="col-6 text-center">
                <Button className="req_video_call_btn" text={"Yes"} handler={() => this.AcceptOrRejectVideoCallRequest(1)} />
              </div>
              <div className="col-6 text-center">
                <Button className="req_video_call_btn_accept" text={"No"} handler={() => this.AcceptOrRejectVideoCallRequest(2)} />
              </div>
            </div>
          </div>
        );
    }
  };

  render() {
    return (
      <Wrapper>
        {this.toRenderView(this.props.type)}
        <style jsx>{`
          :global(.main_title) {
            font-size: 1.024vw;
            color: #385898;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
          }
          :global(.req_video_call_btn) {
            cursor: pointer;
            border: none !important;
            background: #378bcb;
            color: #f5f5f5;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.805vw;
            padding: 0.366vw 1.5vw;
          }
          :global(.req_video_call_btn_accept) {
            cursor: pointer;
            border: none !important;
            color: #f5f5f5;
            background: #27c397;
            font-family: "Museo-Sans-Cyrl-Semibold" !important;
            font-size: 0.805vw;
            padding: 0.366vw 1.5vw;
          }
        `}</style>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userSelectedToChat: state.userSelectedToChat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _storeWebrtcData: (data) => dispatch(storeWebrtcData(data)),
    _updateUserSelectedToChat: (userObj) => dispatch(userSelectedToChat(userObj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestVideoCallModal);
