export const Languages = [{
    name: "English",
    code: "en"
},
{
    name: "Malay",
    code: "ms"
},
{
    name: "Arabic",
    code: "ar"
}
];