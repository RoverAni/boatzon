export const ProPlanData = [
  {
    plan_name: "Small Boat Dealer",
    number_of_boats: "Less than 50 Boats",
    monthly_price: "$99/mo",
  },
  {
    plan_name: "Large Boat Dealer",
    number_of_boats: "More than 50 Boats",
    monthly_price: "$299/mo",
  },
];
