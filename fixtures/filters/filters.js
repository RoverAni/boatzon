import { PRICE_H_TO_L, PRICE_L_TO_H, POPULARITY, SORT_BY } from "../../lib/config";

export const FilterLabels = [
    SORT_BY,
    // "Relevance",
    POPULARITY,
    PRICE_L_TO_H,
    PRICE_H_TO_L
];