export const CAT_NAME_Q = "fname";
export const SUB_CAT_NAME_Q = "sname";
export const SUB_SUB_CAT_NAME_Q = "tname";
export const BRAND_NAME_Q = "bname";
export const COLOUR_NAME_Q = "colour";
export const SIZE_NAME_Q = "size";
export const MIN_PRICE_NAME_Q = "minprice";
export const MAX_PRICE_NAME_Q = "maxprice";
export const FACET_NAME_Q = "facet_";
export const BEST_DEALS_Q = "best_deals";
export const SEARCH_NAME_Q = "q";

export const SORT_ASC_Q = "price_asc";
export const SORT_DESC_Q = "price_desc";
export const SORT_RECENT_Q = "recency_desc";

export const SORT_Q = "sort";

export const ALL_FILTER_PARAMS = [CAT_NAME_Q, SUB_CAT_NAME_Q, SUB_SUB_CAT_NAME_Q, BRAND_NAME_Q, COLOUR_NAME_Q, SIZE_NAME_Q, MIN_PRICE_NAME_Q, MAX_PRICE_NAME_Q, FACET_NAME_Q, BEST_DEALS_Q, SEARCH_NAME_Q, SORT_Q];