import {
  Professionals_1_IMG,
  Professionals_2_IMG,
  Professionals_3_IMG,
  Professionals_4_IMG
} from "../../lib/config";

export const ProfessionalsImgList = [
  Professionals_1_IMG,
  Professionals_2_IMG,
  Professionals_3_IMG,
  Professionals_4_IMG
];
