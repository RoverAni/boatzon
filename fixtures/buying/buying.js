import { Boat_1_IMG, Boat_2_IMG, Boat_3_IMG, Seller_4, Seller_2, Seller_1 } from "../../lib/config";

export const BuyingBoatsList = [
  {
    id: 0,
    boat_img: Boat_1_IMG,
    boat_name: "Ocean Alexander 120",
    boat_seller_name: "Marine Max",
    boat_seller_pic: Seller_2,
    boat_price: "$25.000",
    boat_availabilty_status: "Available",

  },
  {
    id: 1,
    boat_img: Boat_2_IMG,
    boat_name: "2020 Sportsman Open 232 Center Console23'",
    boat_seller_name: "Brandon Leon",
    boat_seller_pic: Seller_4,
    boat_price: "$950.000",
    boat_availabilty_status: "Available",

  },
  {
    id: 2,
    boat_img: Boat_3_IMG,
    boat_name: "Shipmarry 4606",
    boat_seller_name: "Ellie Wax",
    boat_seller_pic: Seller_1,
    boat_price: "$1.150.000",
    boat_availabilty_status: "Sold",

  },
];
