import { Mastercard_Icon, Avh_Card_Icon } from "../../lib/config";

export const SavedCardData = [
  {
    id: 0,
    cardLogo: Mastercard_Icon,
    cardName: "MasterCard",
    expiryData: "expires 12/21",
    cardNumber: "**** **** **** 8154",
    cardValid: true,
  },
  {
    id: 1,
    cardLogo: Avh_Card_Icon,
    cardName: "ACH",
    expiryData: "Nate Cantalupo",
    cardNumber: "299389638",
    cardValid: true,
  },
  {
    id: 2,
    cardLogo: Avh_Card_Icon,
    cardName: "ACH",
    expiryData: "Nate Cantalupo",
    cardNumber: "299389638",
    cardValid: false,
  },
  {
    id: 3,
    cardLogo: Mastercard_Icon,
    cardName: "MasterCard",
    expiryData: "expires 10/25",
    cardNumber: "**** **** **** 9663",
    cardValid: false,
  },
];
