export const INDEX_PAGE = "/";
export const PROD_LIST_PAGE = "/product-list";
export const CAT_LIST_PAGE = "/category-list";
export const PROD_DETAIL_PAGE = "/product";
export const CAT_URL = "categories";
export const BRAND_DETAIL_PAGE = "brand-list";
export const BEST_DEALS = "best-deals";
export const BRAND_URL = "brands";
export const POST_REVIEW = "post-review";
export const OFFERS_URL = "offers";
export const SEARCH_URL = "search";
export const PROFILE_URL = "/profile";
export const WISHLIST_URL = "/profile/favourites";
export const ORDER_URL = "/profile/orders";
export const CANCEL_ORDER_URL = "/cancel-order";