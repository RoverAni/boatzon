import { Item_1, Seller_1, Item_2, Seller_2, Item_3, Seller_3, Item_4, Seller_4, Item_5, Seller_5 } from "../../lib/config";

export const TransactionRecords = [
  {
    itemImg: Item_1,
    itemName: "Ocean Alexander 214",
    date: "Nov. 19, 2019",
    transactionId: "162398",
    sellerImg: Seller_1,
    sellerName: "Susan2015",
    price: "$12,999",
    status: "Time to ship",
  },

  {
    itemImg: Item_2,
    itemName: "Ocean shipmarine",
    date: "Jan. 18, 2020",
    transactionId: "262391",
    sellerImg: Seller_2,
    sellerName: "James do",
    price: "$18,195",
    status: "Canceled",
  },

  {
    itemImg: Item_3,
    itemName: "SunMarine",
    date: "Feb. 01, 2020",
    transactionId: "562138",
    sellerImg: Seller_3,
    sellerName: "max Star",
    price: "$1,152",
    status: "offer accepted",
  },

  {
    itemImg: Item_4,
    itemName: "Shipbo36",
    date: "Oct. 23, 2019",
    transactionId: "262652",
    sellerImg: Seller_4,
    sellerName: "John snow",
    price: "$196",
    status: "Payment scheduled",
  },
  {
    itemImg: Item_5,
    itemName: "Motorboat1",
    date: "Feb. 01, 2019",
    transactionId: "598741",
    sellerImg: Seller_5,
    sellerName: "Megan Mott",
    price: "$19,123",
    status: "Payment sent",
  },
];
