import {
  Boat_1_IMG,
  Boat_2_IMG,
  Boat_3_IMG,
  Seller_4,
  Seller_2,
  Seller_1,
} from "../../lib/config";

export const SellingBoatsList = [
  {
    id: 0,
    boat_img: Boat_1_IMG,
    boat_name: "Ocean Alexander 120",
    sellingboat_listing_status: "Active",
    view_number: "556",
    clicked_number: "62",
    comment_number: "1",
    boat_price: "$40,050",
    selling_status: "Refund",
  },
  {
    id: 1,
    boat_img: Boat_2_IMG,
    boat_name: "Ocean Ship206",
    sellingboat_listing_status: "Active",
    view_number: "65",
    clicked_number: "15",
    comment_number: "12",
    boat_price: "$12,999",
    selling_status: "Pending",
  },
  {
    id: 2,
    boat_img: Boat_3_IMG,
    boat_name: "Shipmarry 4606",
    sellingboat_listing_status: "InActive",
    view_number: "4599",
    clicked_number: "720",
    comment_number: "0",
    boat_price: "$11,999",
    selling_status: "Cancel",
  },
];
