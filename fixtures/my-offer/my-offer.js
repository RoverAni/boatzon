import { Item_1, Messanger_2, Item_2, Messanger_1, Item_3, Messanger_3 } from "../../lib/config";

export const ChatListData = [
  {
    id: 0,
    profile_pic: Messanger_1,
    messenger_name: "Marine Max",
    manufacturer_name: "Bayliner Element E21",
    msg_date: "5-4-2020",
    product_img: Item_1,
    location: "Aventura, FL",
    odd_comments: ["Hi, I'm Marine Max. How can I help you?"],
    even_comments: [
      "Is the price negotiable? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta placerat ornare. Etiam tristique pretium aliquet. Ut a vestibulum erat. Aliquam erat volutpat.",
    ],
  },
  {
    id: 1,
    profile_pic: Messanger_2,
    messenger_name: "Brandon Leon",
    manufacturer_name: "2020 Sportsman Open 232 Center Console23'",
    msg_date: "3-4-2020",
    location: "Aventura, FL",
    product_img: Item_2,
    odd_comments: ["Hi, I'm Marine Max. How can I help you?"],
    even_comments: [
      "Is the price negotiable? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta placerat ornare. Etiam tristique pretium aliquet. Ut a vestibulum erat. Aliquam erat volutpat.",
    ],
  },
];

export const ArchiveChatListData = [
  {
    id: 0,
    profile_pic: Messanger_1,
    name: "Marine Max",
    manufacture_name: "Bayliner",
    msg_date: "5-4-2020",
    product_img: Item_1,
    location: "Aventura, FL",
    product_type: "selling",
    odd_comments: ["Hi, I'm Marine Max. How can I help you?"],
    even_comments: [
      "Is the price negotiable? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta placerat ornare. Etiam tristique pretium aliquet. Ut a vestibulum erat. Aliquam erat volutpat.",
    ],
  },
  {
    id: 1,
    profile_pic: Messanger_2,
    name: "Brandon Leon",
    manufacture_name: "Ocean Alexander 120",
    msg_date: "3-4-2020",
    location: "Aventura, FL",
    product_img: Item_2,
    product_type: "selling",
    odd_comments: ["Hi, I'm Marine Max. How can I help you?"],
    even_comments: [
      "Is the price negotiable? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta placerat ornare. Etiam tristique pretium aliquet. Ut a vestibulum erat. Aliquam erat volutpat.",
    ],
  },
  {
    id: 2,
    profile_pic: Messanger_3,
    name: "Ellie Wax",
    manufacture_name: "Oceanview 25",
    msg_date: "28-3-2020",
    location: "Aventura, FL",
    product_img: Item_3,
    product_type: "buying",
    odd_comments: ["Hi, I'm Marine Max. How can I help you?"],
    even_comments: [
      "Is the price negotiable? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta placerat ornare. Etiam tristique pretium aliquet. Ut a vestibulum erat. Aliquam erat volutpat.",
    ],
  },
];
