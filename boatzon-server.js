const express = require("express");
const next = require("next");
const cors = require("cors");
// var cookieParser = require('cookie-parser');
const { parse } = require("url");
const port = 7122;
const dev = true;
const app = next({ dev });
var path = require("path");
const handle = app.getRequestHandler();

const compression = require("compression");
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

app.prepare().then(() => {
  const server = express();
  server.use(compression());
  // server.use(cookieParser());

  server.get("/", (req, res) => {
    // console.log("REGISER", register)
    // register();
    const actualPage = "/";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  /** service worker register start */
  // server.get("/service-worker.js", (req, res) => {
  //   // Don't cache service worker is a best practice (otherwise clients wont get emergency bug fix)
  //   res.set("Cache-Control", "no-store, no-cache, must-revalidate, proxy-revalidate");
  //   res.set("Content-Type", "application/javascript");
  //   res.sendFile(path.join(__dirname, "static", "service-worker.js"));
  // });
  server.get("/sw.js", (req, res) => {
    // Don't cache service worker is a best practice (otherwise clients wont get emergency bug fix)
    res.set("Cache-Control", "no-store, no-cache, must-revalidate, proxy-revalidate");
    res.set("Content-Type", "application/javascript");
    res.sendFile(path.join(__dirname, "static", "sw.js"));
  });
  /** service worker register ends */
  server.get("/firebase-messaging-sw.js", async (req, res) => {
    // Don't cache service worker is a best practice (otherwise clients wont get emergency bug fix)
    res.sendFile(path.join(__dirname, "static", "firebase-messaging-sw.js"));
  });
  // server.get("/firebase-messaging-sw.js", async (req, res) => {
  //   // Don't cache service worker is a best practice (otherwise clients wont get emergency bug fix)
  //   res.set("Cache-Control", "no-store, no-cache, must-revalidate, proxy-revalidate");
  //   res.set("Content-Type", "application/javascript");

  //   return res.send(`

  //               importScripts("https://www.gstatic.com/firebasejs/7.6.2/firebase-app.js");
  //               importScripts("https://www.gstatic.com/firebasejs/7.6.2/firebase-messaging.js");

  //               // Your web app's Firebase configuration

  //               var firebaseConfig = {
  //                 apiKey: "AIzaSyBHCU7ffm0dwNDlS2we0DnoDneZRiASlck",
  //                 authDomain: "boatzon-a1f03.firebaseapp.com",
  //                 databaseURL: "https://boatzon-a1f03.firebaseio.com",
  //                 projectId: "boatzon-a1f03",
  //                 storageBucket: "boatzon-a1f03.appspot.com",
  //                 appId: "1:1029574855542:web:bc8dde1e512c",
  //               };
  //               // Initialize Firebase
  //               firebase.initializeApp(firebaseConfig);

  //               //Retrieve Firebase Messaging object.
  //               const messaging = firebase.messaging();

  //           `);
  // });

  // my-boats Page
  server.get("/my-boats", (req, res) => {
    const actualPage = "/my-boats";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // boats Page
  server.get("/boats", (req, res) => {
    const actualPage = "/boats";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // engines Page
  server.get("/engines", (req, res) => {
    const actualPage = "/engines";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // trailers Page
  server.get("/trailers", (req, res) => {
    const actualPage = "/trailers";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // products Page
  server.get("/products", (req, res) => {
    const actualPage = "/products";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // professionals/:section/:section Page
  server.get("/professionals/:section/:section", (req, res) => {
    const actualPage = "/professionals";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // discussions Page
  server.get("/discussions", (req, res) => {
    const actualPage = "/discussions";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // services Page
  server.get("/services/:section", (req, res) => {
    const actualPage = "/services";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // loan-financing services Page
  server.get("/loan-financing/:section", (req, res) => {
    const actualPage = "/loan-financing";
    const queryParams = { pid: req.query.pid };
    app.render(req, res, actualPage, queryParams);
  });

  // insurance services Page
  server.get("/insurance/:section", (req, res) => {
    const actualPage = "/insurance";
    const queryParams = { pid: req.query.pid };
    app.render(req, res, actualPage, queryParams);
  });

  // product-detail Page
  server.get("/product-detail/:name", (req, res) => {
    const actualPage = "/product-detail";
    const queryParams = { pid: req.query.pid };
    app.render(req, res, actualPage, queryParams);
  });

  // boat-detail Page
  server.get("/boat-detail/:name", (req, res) => {
    const actualPage = "/boat-detail";
    const queryParams = { pid: req.query.pid };
    app.render(req, res, actualPage, queryParams);
  });

  // // engine-detail Page
  // server.get("/engine-detail/:name", (req, res) => {
  //   const actualPage = "/engine-detail";
  //   const queryParams = { pid: req.query.pid };
  //   app.render(req, res, actualPage, queryParams);
  // });

  // // trailer-detail Page
  // server.get("/trailer-detail/:name", (req, res) => {
  //   const actualPage = "/trailer-detail";
  //   const queryParams = { pid: req.query.pid };
  //   app.render(req, res, actualPage, queryParams);
  // });

  // profile/:section Page
  server.get("/user-profile/:section", (req, res) => {
    const actualPage = "/user-profile";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // professional-user-profile/:section Page
  server.get("/professional-user-profile/:section", (req, res) => {
    const actualPage = "/professional-user-profile";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // other user professional profile (dynamic)
  server.get("/professional-other-profile/:name", (req, res) => {
    const actualPage = "/professional-other-profile";
    const queryParams = { name: req.params.name };
    app.render(req, res, actualPage, queryParams);
  });

  // my-account Page
  server.get("/my-account/:section", (req, res) => {
    const actualPage = "/my-account";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // buy-with-boatzon Page
  server.get("/buy-with-boatzon/:section", (req, res) => {
    const actualPage = "/buy-with-boatzon";
    const queryParams = { pid: req.query.pid };
    app.render(req, res, actualPage, queryParams);
  });

  // purchases-sales Page
  server.get("/purchases-sales/:section", (req, res) => {
    const actualPage = "/purchases-sales";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // my-offers Page
  server.get("/my-offers", (req, res) => {
    const actualPage = "/my-offers";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // following Page
  server.get("/following", (req, res) => {
    const actualPage = "/following";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // buying Page
  server.get("/buying", (req, res) => {
    const actualPage = "/buying";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // discussion-details Page
  server.get("/discussion-details/:name", (req, res) => {
    const actualPage = "/discussion-details";
    const queryParams = { d_id: req.query.d_id };

    app.render(req, res, actualPage, queryParams);
  });

  // messenger Page
  server.get("/messenger", (req, res) => {
    const actualPage = "/messenger";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // pro-membership-plans Page
  server.get("/pro-membership-plans", (req, res) => {
    const actualPage = "/pro-membership-plans";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // professional-signup Page
  server.get("/professional-signup", (req, res) => {
    const actualPage = "/professional-signup";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // selling Page
  server.get("/selling", (req, res) => {
    const actualPage = "/selling";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // reset/:section Page
  server.get("/reset/:section", (req, res) => {
    const actualPage = "/reset";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // verify-email/:section Page
  server.get("/verify-email/:section", (req, res) => {
    const actualPage = "/verify-email";
    const queryParams = {};
    app.render(req, res, actualPage, queryParams);
  });

  // server.get("/firebase-messaging-sw.js", async (req, res) => {
  //   // Don't cache service worker is a best practice (otherwise clients wont get emergency bug fix)
  //   res.sendFile(path.join(__dirname, "lib", "firebase-config", "firebase.js"));
  // });

  server.get("*", (req, res) => {
    // checkCookies(req, res, (err, result) => {
    //   if (result) {
    return handle(req, res);
    //   }
    // });
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
