import { get, postWithToken } from "../lib/request";

/** get own boats and products combined */
export const getOwnPosts = (payload) => {
  const endPoint = `/profile/posts`;
  return postWithToken(endPoint, payload);
};

export const getTracking = (payload) => {
  const endPoint = `/tracking?type=${payload.type}&trackingNumber=${payload.trackingNumber}`;
  return get(endPoint);
};

export const getPostInsights = (payload) => {
  const endPoint = `/insights`;
  return postWithToken(endPoint, payload);
};

/** how many people has made offers [array] */
export const acceptedOffersList = (payload) => {
  const endPoint = `/acceptedOffers`;
  return postWithToken(endPoint, payload);
};

export const getOwnEnginePosts = (payload) => {
  const endPoint = `/engine/get?postId=${payload.postId}`;
  return get(endPoint);
};

export const getOwnTrailerPosts = () => {
  const endPoint = `/trailer/get`;
  return get(endPoint);
};
