import { post } from "../lib/request";

export const proEmailVerfication = (payload) => {
  const endPoint = `/businessEmailCheck`;
  return post(endPoint, payload);
};

export const proUserNameVerfication = (payload) => {
  const endPoint = `/businessUsernameCheck`;
  return post(endPoint, payload);
};

export const proRegister = (payload) => {
  const endPoint = `/businessRegister`;
  return post(endPoint, payload);
};
