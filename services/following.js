import { postWithToken, putWithToken } from "../lib/request";

export const followPost = (payload) => {
  const endPoint = `/like`;
  return postWithToken(endPoint, payload);
};

export const unfollowPost = (payload) => {
  const endPoint = `/unlike`;
  return postWithToken(endPoint, payload);
};

export const getFollowingPost = (payload) => {
  const endPoint = `/likedPosts`;
  return postWithToken(endPoint, payload);
};

export const followPeople = (payload) => {
  const endPoint = `/follow/${payload.membername}`;
  return postWithToken(endPoint, {});
};

export const unfollowPeople = (payload) => {
  const endPoint = `/unfollow/${payload.membername}`;
  return postWithToken(endPoint, {});
};

export const getfollowingPeople = (payload) => {
  const endPoint = `/home`;
  return postWithToken(endPoint, payload);
};

export const getfollowingBusiness = (payload) => {
  const endPoint = `/getFollowing`;
  return postWithToken(endPoint, payload);
};

export const followDiscussion = (payload) => {
  const endPoint = `/followDiscussion`;
  return postWithToken(endPoint, payload);
};

export const unfollowDiscussion = (payload) => {
  const endPoint = `/unFollowDiscussion`;
  return putWithToken(endPoint, payload);
};
