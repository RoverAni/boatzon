import {
  deleteReq,
  get,
  post,
  postWithToken,
  putWithToken,
} from "../lib/request";

export const getDiscussions = (payload) => {
  const endPoint = `/discussion/guest`;
  return post(endPoint, payload);
};

export const getDiscussionsPostLogin = (payload) => {
  const endPoint = `/discussion/?type=${payload.type}&title=${
    payload.title ? payload.title : ""
  }&categoryId=${payload.categoryId ? payload.categoryId : ""}&manufacturerId=${
    payload.manufacturerId ? payload.manufacturerId : ""
  }&discussionId=${payload.discussionId ? payload.discussionId : ""}&limit=${
    payload.limit ? payload.limit : 10
  }&offset=${payload.offset ? payload.offset : 0}&following=${
    payload.following ? payload.following : false
  }`;
  return get(endPoint);
};

export const getDiscussionDetails = (payload) => {
  const endPoint = `/discussionById/?_id=${payload.id}`;
  return get(endPoint);
};

export const addDiscussion = (payload) => {
  const endPoint = `/discussion`;
  return postWithToken(endPoint, payload);
};

export const postComment = (payload) => {
  const endPoint = `/comment`;
  return postWithToken(endPoint, payload);
};

export const editComment = (payload) => {
  const endPoint = `/comment`;
  return putWithToken(endPoint, payload);
};

export const deleteComment = (payload) => {
  const endPoint = `/comment`;
  return deleteReq(endPoint, payload);
};

export const deleteDiscussion = (payload) => {
  const endPoint = `/discussion`;
  return deleteReq(endPoint, payload);
};

export const editDiscussion = (payload) => {
  const endPoint = `/discussion`;
  return putWithToken(endPoint, payload);
};
