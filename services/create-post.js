import { get, postWithToken } from "../lib/request";

export const createPost = (payload) => {
  const endPoint = `/product/v2`;
  return postWithToken(endPoint, payload);
};

export const createBoatPost = (payload) => {
  const endPoint = `/boat/v2`;
  return postWithToken(endPoint, payload);
};

export const getEngineData = () => {
  const endPoint = `/engine`;
  return get(endPoint);
};

export const getTrailerData = () => {
  const endPoint = `/trailerList`;
  return get(endPoint);
};

export const createtrailerPost = (payload) => {
  const endPoint = `/trailer/post`;
  return postWithToken(endPoint, payload);
};

export const createEnginePost = (payload) => {
  const endPoint = `/engine/post`;
  return postWithToken(endPoint, payload);
};
