import { putWithToken } from "../lib/request";

export const editPost = (payload) => {
  const endPoint = `/product/v2`;
  return putWithToken(endPoint, payload);
};

export const editEnginePost = (payload) => {
  const endPoint = `/engine/post`;
  return putWithToken(endPoint, payload);
};

export const editTrailerPost = (payload) => {
  const endPoint = `/trailer/post`;
  return putWithToken(endPoint, payload);
};
