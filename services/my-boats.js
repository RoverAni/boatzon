import { postWithToken } from "../lib/request";

export const AllFollowingPosts = (data) => {
  const endPoint = `/allPosts/users/m`;
  let payload = {
    limit: "20",
    offset: "0",
    ...data,
  };
  return postWithToken(endPoint, payload);
};
