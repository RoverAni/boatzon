import { API_URL, AuthPassword } from "../lib/config";
import { ParseToken } from "../lib/parsers/token-parser";
import { post, postWithToken } from "../lib/request";
import { getCookie } from "../lib/session";

export const EmailVerfication = (payload) => {
  const endPoint = `/emailCheck`;
  return post(endPoint, payload);
};

export const UserNameVerfication = (payload) => {
  const endPoint = `/usernameCheck`;
  return post(endPoint, payload);
};

export const PhoneNumberVerfication = (payload) => {
  const endPoint = `/phoneNumberCheck`;
  return post(endPoint, payload);
};

export const registerUser = (payload) => {
  const endPoint = `/register`;
  return post(endPoint, payload);
};

export const userLogin = (payload) => {
  const endPoint = `/login`;
  return post(endPoint, payload);
};

export const forgotPassword = (payload) => {
  const endPoint = `/resetPassword`;
  return post(endPoint, payload);
};

export const resetPassword = (payload) => {
  const endPoint = `/changepassword`;
  return post(endPoint, payload);
};

export const userLogout = () => {
  const endPoint = `/logout`;
  return postWithToken(endPoint, {});
};

export const inviteNewUser = (payload) => {
  let data = {
    method: "POST",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/invite-register`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getAllInvitedUsers = () => {
  let data = {
    method: "GET",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/invitedAccount?userId=${getCookie("mqttId")}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const updateUserSubAccount = (payload) => {
  let data = {
    method: "PUT",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/invitedAccount`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const editAndUpdateSubUserAccount = (payload) => {
  let data = {
    method: "PATCH",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/invitedAccount`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
