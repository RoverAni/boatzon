import { get, postWithToken, putWithToken } from "../lib/request";

export const getManufactures = (data) => {
  let searchKey = `${data.searchKey ? data.searchKey : ""}`;
  const endPoint = `/getManufacture?type=${data.type}&limit=${data.limit}&offset=${data.offset}&searchText=${searchKey}`;
  return get(endPoint);
};

export const getFollowManufactures = () => {
  const endPoint = `/followManufactor`;
  return get(endPoint);
};

export const postfollowManufactures = (payload) => {
  const endPoint = `/followManufactor`;
  return postWithToken(endPoint, payload);
};

export const updatefollowManufacturers = (payload) => {
  const endPoint = `/followManufactor`;
  return putWithToken(endPoint, payload);
};

export const getModels = (data) => {
  let searchKey = `${data.searchKey ? data.searchKey : ""}`;
  const endPoint = `/getModel?manufactureId=${data.manufactureId}&limit=${data.limit}&offset=${data.offset}&searchText=${searchKey}`;
  return get(endPoint);
};
