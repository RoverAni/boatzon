import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";
import { get, post, postWithToken, putWithToken } from "../lib/request";

export const getUserProfileData = () => {
  const endPoint = `/editProfile`;
  return postWithToken(endPoint, {});
};

export const newEmailCheck = (data) => {
  const endPoint = `/email/me`;
  let payload = {
    token: ParseToken(getCookie("token")),
    ...data,
  };
  return post(endPoint, payload);
};

export const saveNewEmail = (data) => {
  const endPoint = `/email/me?verificationToken=${data.verificationToken}&authToken=${data.authToken}`;
  return get(endPoint);
};

export const saveEditProfileData = (payload) => {
  const endPoint = `/saveProfile`;
  return postWithToken(endPoint, payload);
};

export const newPhoneNumber = (payload) => {
  const endPoint = `/profile/phoneNumber`;
  return postWithToken(endPoint, payload);
};

export const newPhoneNumberVerifyOtp = (payload) => {
  const endPoint = `/profile/phoneNumber`;
  return putWithToken(endPoint, payload);
};
