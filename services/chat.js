import { API_CHAT_URL, AuthPassword } from "../lib/config";
import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";
import axios from "axios";

export const getChatsWithPageNo = (pageNo) => {
  let headers = {
    authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
    token: ParseToken(getCookie("token")),
  };

  return axios.get(`${API_CHAT_URL}/Chats/${pageNo}`, {
    headers: headers,
  });
};

export const getChatsForSelectedUser = (timestamp, chatId, pageSize) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
      },
    };
    let status = undefined;
    fetch(`${API_CHAT_URL}/Messages/${chatId}/${timestamp}/${pageSize}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const askSeller = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_CHAT_URL}/askSeller`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
