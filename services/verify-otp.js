import { post } from "../lib/request";

export const requestForOTP = (payload) => {
  const endPoint = `/otp`;
  return post(endPoint, payload);
};

export const verifyOTP = (payload) => {
  const endPoint = `/verify-otp`;
  return post(endPoint, payload);
};
