import { API_VIDEO_URL, AuthPassword } from "../lib/config";
import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";

export const getAllRequestForAVCalls = (fromId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/request?fromId=${fromId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const acceptOrRejectVideoCallRequest = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PUT",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/request`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const sendAVCallRequest = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/request`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getStatusForAVCalls = (toId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/status?toId=${toId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const callUser = (obj) => {
  console.log("call user ->", obj);
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/call`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getCallID = (callId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/call/${callId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getHistoryOfCalls = () => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/history`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

/** this is for incoming call to accept */
export const acceptCallInviteV1 = (callId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PUT",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/call/${callId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const initiateCall = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PUT",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/call/invite`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const disconnectVideoCall = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "DELETE",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_VIDEO_URL}/call`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
