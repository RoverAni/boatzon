import { AuthPassword, API_URL, STRIPE_API } from "../lib/config";
import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";

export const storeShippingAddress = (obj, mode) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: mode === "editMode" ? "PUT" : "POST",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };
    let url;
    if (mode === "editMode") {
      url = `${API_URL}/shippingAddress?id=${obj.userId}`;
    } else {
      url = `${API_URL}/shippingAddress`;
    }
    fetch(url, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getShippingCost = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    fetch(`${API_URL}/shippingCost?productId=${obj.productId}&userId=${obj.userId}&type=0`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getAllShippingAddress = (skip, limit) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    fetch(`${API_URL}/shippingAddress?userId=${getCookie("mqttId")}&skip=${skip}&limit=${limit}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const submitLeadsForwarding = (object) => {
  console.log("promise", object);
  return new Promise((resolve, reject) => {
    let data = {
      method: "PUT",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(object),
    };
    fetch(`${API_URL}/leadsForwarding`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getLeadsForwarding = () => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    fetch(`${API_URL}/leadsForwarding`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getTransaction = (object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
    };
    fetch(
      `${API_URL}/transaction?productId=${object.productId}&buyerId=${object.buyerId}&rateId=${object.rateId}&providerName=${object.providerName}&orderId=${object.orderId}&sellerId=${object.sellerId}`,
      data
    )
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const buyNow = (object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
        token: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(object),
    };
    fetch(`${STRIPE_API}/v1/payment`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
