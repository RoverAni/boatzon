import { getCookie } from "../lib/session";
import { API_URL, AuthPassword } from "../lib/config";
import { get, postWithToken, putWithToken } from "../lib/request";
import { ParseToken } from "../lib/parsers/token-parser";
// import XMLParser from "react-xml-parser";

export const getStoreBuyingInfo = (payload) => {
  const endPoint = `/userInsurance?userId=${payload.mqttId}&productId=${payload.postId}`;
  return get(endPoint);
};

export const getStoreBuyingInfoUser = (payload) => {
  const endPoint = `/userInsuranceDirect?userId=${payload.mqttId}`;
  return get(endPoint);
};

export const storeBuyingInfo = (payload) => {
  const endPoint = `/userInsurance`;
  return putWithToken(endPoint, payload);
};

export const storeBuyingInfoUser = (payload) => {
  const endPoint = `/userInsuranceDirect`;
  return putWithToken(endPoint, payload);
};

export const postCreditScore = (payload) => {
  const endPoint = `/creditScore`;
  return postWithToken(endPoint, payload);
};

export const checkCreditScore = (payload) => {
  let i = 0;
  let newAddress = "";
  while (payload.address[i] !== ",") {
    newAddress += payload.address[i++];
  }
  // return new Promise((resolve, reject) => {
  //   let data = {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "text/plain",
  //       "Content-Encoding": "gzip",
  //       Vary: "Accept-Encoding",
  //       "Cache-Control": "private",
  //       "X-Info": "2-10222-10223 SNNy RT(1600768313640 11059) q(0 1 1 -1) r(13 13) U5",
  //     },
  //     body: `ACCOUNT=boatzonpq&PASSWD=Test12345!&PASS=2&PROCESS=PCCREDIT&NAME=${
  //       payload.first_name + " " + payload.last_name
  //     }&ADDRESS=${newAddress}&CITY=${payload.city}&STATE=${payload.stateName.value}&ZIP=${
  //       payload.zipCode
  //     }&PRODUCT=PREQUALIFY&BUREAU=XPN&PHONE=${payload.mobile}
  //     `,
  //   };
  //   data.headers["Content-Length"] = JSON.stringify(data).length.toString();
  //   fetch(`https://www.700CreditSolution.com/XCRS/Service.aspx`, data)
  //     .then((res) => {
  //       return resolve(res);
  //     })
  //     .catch((err) => {
  //       return reject(err);
  //     });
  // });
  return new Promise((resolve, reject) => {
    var myHeaders = new Headers();
    myHeaders.append("ACCOUNT", "boatzonpq");
    myHeaders.append("PASSWD", "Test12345!");
    myHeaders.append("PRODUCT", "PREQUALIFY");
    myHeaders.append("BUREAU", "TU");
    myHeaders.append("PASS", "2");
    myHeaders.append("PROCESS", "PCCREDIT");
    myHeaders.append("Content-Type", "text/plain");
    myHeaders.append("NAME", payload.first_name + " " + payload.last_name);
    myHeaders.append("ADDRESS", newAddress);
    myHeaders.append("CITY", payload.city);
    myHeaders.append("STATE", payload.stateName.value);
    myHeaders.append("ZIP", payload.zipCode);
    myHeaders.append("PHONE", payload.mobile);

    var raw = `ACCOUNT=boatzonpq&PASSWD=Test12345!&PASS=2&PROCESS=PCCREDIT&NAME=${
      payload.first_name + " " + payload.last_name
    }&ADDRESS=${newAddress}&CITY=${payload.city}&STATE=${
      payload.stateName.value
    }&ZIP=${payload.zipCode}&PRODUCT=PREQUALIFY&BUREAU=XPN&PHONE=${
      payload.mobile
    }`;

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.700CreditSolution.com/XCRS/Service.aspx", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        return resolve(result);
      })
      .catch((error) => console.log("error", error));
  });
};

export const markAsSold = (obj) => {
  let data = {
    method: "POST",
    headers: {
      authorization:
        "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(obj),
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/markSold`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const soldElseWhere = (obj) => {
  let data = {
    method: "POST",
    headers: {
      authorization:
        "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(obj),
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/sold/elseWhere`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getBuyersForPost = (obj) => {
  let data = {
    method: "POST",
    headers: {
      authorization:
        "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(obj),
  };
  return new Promise((resolve, reject) => {
    fetch(`${API_URL}/acceptedOffers`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
