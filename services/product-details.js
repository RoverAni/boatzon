import { API_NY_URL, AuthPassword, commonHeader } from "../lib/config";
import axios from "axios";
import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";
import { postWithToken } from "../lib/request";

export const productDetailsData = (payload) => {
  const endPoint = `/getPostsById/users`;
  return postWithToken(endPoint, payload);
};

export const makeOffer = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        ...commonHeader,
        token: ParseToken(getCookie("token")),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };

    let status = undefined;
    fetch(`${API_NY_URL}/makeOffer`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const validateParcel = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        ...commonHeader,
        token: ParseToken(getCookie("token")),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    };

    let status = undefined;
    fetch(`${API_NY_URL}/validateParcel`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
