import { post } from "../lib/request";

export const postSearchData = (payload) => {
  const endPoint = `/fullSearch`;
  return post(endPoint, payload);
};
