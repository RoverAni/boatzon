import { getCookie } from "../lib/session";
import axios from "axios";
import { API_VIDEO_URL, AuthPassword } from "../lib/config";
import { ParseToken } from "../lib/parsers/token-parser";

export const getAppointmentData = (payload) => {
  const endPoint =
    API_VIDEO_URL + `/appointment?id=${payload.userId}&type=${payload.type}&fromDate=${payload.startDate}&toDate=${payload.endDate}`;
  const headers = {
    authorization: getCookie("token"),
    lan: "en",
    "Content-Type": "application/json",
  };
  return axios.get(endPoint, { headers });
};

export const setInPersonAppointment = (object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        authorization: ParseToken(getCookie("token")),
        lan: "en",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(object),
    };

    let status = undefined;
    fetch(`${API_VIDEO_URL}/request`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
