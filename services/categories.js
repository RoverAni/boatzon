import { get } from "../lib/request";

// export const getCategories = () => {
//   let headers = {
//     authorization:
//       "Basic " + new Buffer("basicAuth:" + AuthPassword).toString("base64"),
//   };

//   return axios.get(API_NY_URL + "/getCategories", {
//     headers: headers,
//   });
// };

export const getBoatsSubCategories = () => {
  let endPoint = `/subCategory?categoryName=Boats`;
  return get(endPoint);
};

export const getProductsSubCategories = () => {
  let endPoint = `/subCategory?categoryName=Products`;
  return get(endPoint);
};


export const getSubSubCateogry = (subCategory) => {
  let endPoint = `/subSubCategory?subCategory=${subCategory}&categoryName=Products`;
  return get(endPoint);
};

export const getSubSubCateogryV2 = (subCategory, subSubCateogry) => {
  let endPoint = "";
  if (getSubSubCateogry == "") {
    endPoint = `/subSubCategory?subCategory=${subCategory}&categoryName=Products`;
  } else {
    endPoint = `/subSubCategory=${subSubCateogry}&subCategory=${subCategory}&categoryName=Products`;
  }
  return get(endPoint);
};
