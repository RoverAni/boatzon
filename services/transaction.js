import { STRIPE_API } from "../lib/config";
import { AuthPassword } from "../lib/config";
import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";
import { get } from "../lib/request";

export const getSetupIntent = () => {
  let data = {
    method: "GET",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/setupIntent`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getCustomer = () => {
  let data = {
    method: "GET",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/customer?userId=${getCookie("mqttId")}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const addCardForCustomer = (obj) => {
  let data = {
    method: "POST",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(obj),
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/customer`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getConnectedStripeAccount = (userId) => {
  let data = {
    method: "GET",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/connectAccount?userId=${userId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getConnectedBalance = (userId) => {
  let data = {
    method: "GET",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/connectBalance?userId=${userId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getConnectLink = (userId) => {
  let data = {
    method: "GET",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/connectLink?userId=${userId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const deleteCard = (paymentMethod) => {
  let data = {
    method: "DELETE",
    headers: {
      authorization: "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
      token: ParseToken(getCookie("token")),
      lan: "en",
      "Content-Type": "application/json",
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${STRIPE_API}/v1/paymentMethod?paymentMethod=${paymentMethod}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// 1
export const postCreateCharge__Stripe = (object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
      },
      body: JSON.stringify(object),
    };
    let status = undefined;
    fetch(`${STRIPE_API}/v1/createCharge`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// 2
export const postCaptureCharge__Stripe = (object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
      },
      body: JSON.stringify(object),
    };
    let status = undefined;
    fetch(`${STRIPE_API}/v1/captureCharge`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const doCharge = (object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
      },
      body: JSON.stringify(object),
    };
    let status = undefined;
    fetch(`${STRIPE_API}/v1/charge`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
