import { ParseToken } from "../lib/parsers/token-parser";
import { getCookie } from "../lib/session";
import { post, postWithToken } from "../lib/request";

export const filterProductsData = (data) => {
  let payload = {
    ...data,
  };
  let AuthPass = getCookie("authPass");
  if (AuthPass) {
    payload["name"] = ParseToken(getCookie("userName"));
  }
  const endPoint = `/filterProduct`;
  return post(endPoint, payload);
};

export const filterProductsDataPostLogin = (data) => {
  const endPoint = `/filterProduct`;
  let payload = {
    name: ParseToken(getCookie("userName")),
    ...data,
  };
  return postWithToken(endPoint, payload);
};

export const filterEnginesData = (data) => {
  let payload = {
    ...data,
  };
  let AuthPass = getCookie("authPass");
  if (AuthPass) {
    payload["name"] = ParseToken(getCookie("userName"));
  }
  const endPoint = `/filterEngine`;
  return post(endPoint, payload);
};

export const filterTrailersData = (data) => {
  let payload = {
    ...data,
  };
  let AuthPass = getCookie("authPass");
  if (AuthPass) {
    payload["name"] = ParseToken(getCookie("userName"));
  }
  const endPoint = `/filterTrailer`;
  return post(endPoint, payload);
};
