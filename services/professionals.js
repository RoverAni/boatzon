import { get, postWithToken, putWithToken, post } from "../lib/request";

export const getProfessionals = () => {
  const endPoint = `/getProfessional`;
  return get(endPoint);
};

export const postLoginProfessionalsData = (payload) => {
  const endPoint = `/businessBySpeciality/guest`;
  return post(endPoint, payload);
};

export const afterLoginProfessionalsData = (payload) => {
  const endPoint = `/businessBySpeciality/user`;
  return postWithToken(endPoint, payload);
};

export const getProProfilePhotos = (payload) => {
  const endPoint = `/businessProfile/photo/?username=${payload.username}`;
  return get(endPoint);
};

export const editProProfile = (payload) => {
  const endPoint = `/businessProfile`;
  return putWithToken(endPoint, payload);
};

export const professionalSearch = (payload) => {
  const endPoint = `/professionalFilter`;
  return postWithToken(endPoint, payload);
};

export const getProfessionalBoatData = (payload) => {
  const endPoint = `/profile/posts/${payload.membername}`;
  return postWithToken(endPoint, payload);
};
