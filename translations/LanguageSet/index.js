// eslint-disable-next-line linebreak-style
const en = require('../en.json');
const fr = require('../fr.json');

export const LanguageSet = {
  'en': en,
  'fr': fr,
}
;
