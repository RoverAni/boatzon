import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";

import _JSXStyle from "styled-jsx/style";

import { WHITE_COLOR, GREEN_COLOR, BG_LightGREY_COLOR } from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";

const useStyles = makeStyles((theme) => ({
  buttonRoot: {
    margin: (props) => props.margin || "0.585vw",
    fontFamily: (props) =>
      `${props.fontFamily} !important` || "Museo-Sans-Cyrl-Light !important",
    letterSpacing: "0.0366vw !important",
    color: (props) => props.color,
    background: (props) => props.background,
    height: (props) => props.height,
    width: "100%",
    textTransform: "capitalize",
    "&:focus, &:active, &:hover": {
      boxShadow: "none !important",
      outline: "none !important",
      color: (props) => props.color,
      background: (props) => props.background,
    },
  },
  label: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontFamily: (props) =>
      `${props.fontFamily} !important` || "Museo-Sans-Cyrl-Light !important",
    fontSize: (props) => props.fontSize,
  },
  buttonProgress: {
    color: BG_LightGREY_COLOR,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    "&:hover": {
      backgroundColor: "transparent",
      opacity: 0.9,
    },
  },
}));

export default function CircularProgressButton(props) {
  const { cssStyles } = props;
  const classes = useStyles(cssStyles);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const timer = React.useRef();

  useEffect(() => {
    handleTimeout();
  }, []);

  const handleTimeout = () => {
    return () => {
      clearTimeout(timer.current);
    };
  };

  const { buttonText, type, styles } = props;

  return (
    <Wrapper>
      <div className="position-relative">
        <Button
          classes={{ root: classes.buttonRoot, label: classes.label }}
          disabled={props.loading || props.disabled}
          onClick={props.onClick}
          type={type || "button"}
          style={styles ? { ...styles } : {}}
        >
          {buttonText}
        </Button>
        {props.loading && (
          <CircularProgress size={23} className={classes.buttonProgress} />
        )}
      </div>
    </Wrapper>
  );
}
