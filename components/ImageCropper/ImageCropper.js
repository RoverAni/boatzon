import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import Cropper from "cropperjs";
import ButtonComp from "../../components/button/button";
import { GREEN_COLOR, BG_LightGREY_COLOR, Close_Icon } from "../../lib/config";

class ImageCropper extends Component {
  state = {};
  handleCroppingTool = () => {
    const image = document.getElementById("crop_image");
    const cropper = new Cropper(image, [
      {
        aspectRatio: 2 / 3,
      },
    ]);
    image.addEventListener("ready", (e) => {
      const result = cropper.getCroppedCanvas({
        maxHeight: 500,
        maxWidth: 500,
      });
      console.log("CROPPED", result, result.toDataURL());
      this.setState({
        croppedImg: result.toDataURL(),
      });
    });
    image.addEventListener("cropend", (event) => {
      const result = cropper.getCroppedCanvas({
        maxHeight: 500,
        maxWidth: 500,
      });
      console.log("CROPPED", result, result.toDataURL());
      this.setState({
        croppedImg: result.toDataURL(),
      });
    });
  };

  render() {
    //
    return (
      <Wrapper>
        <div className="CroppingDiv">
          <div className="col-12">
            <div className="row justify-content-center p-3">
              <div className="col-12 p-0">
                <img
                  src={Close_Icon}
                  className="closeIcon"
                  onClick={this.props.onClose}
                ></img>
                <h6 className="heading">Crop the Image</h6>
                <img
                  id="crop_image"
                  src={this.props.currentImg}
                  alt="Cropping Image"
                  onLoad={this.handleCroppingTool}
                ></img>
                <div className="upload_btn">
                  <ButtonComp
                    onClick={this.props.handleUploadMedia.bind(
                      this,
                      this.state.croppedImg
                    )}
                  >
                    Upload
                  </ButtonComp>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .heading {
              font-size: 1.346vw;
              font-family: "Open Sans-SemiBold" !important;
              margin-bottom: 0.732vw;
              letter-spacing: 0.021vw !important;
            }
            :global(.MuiDialog-paperWidthSm) {
              max-width: initial !important;
              max-height: initial !important;
              position: relative !important;
            }
            #crop_image {
              max-width: 41.245vw;
            }
            .upload_btn {
              float: right;
            }
            :global(.upload_btn button) {
              width: fit-content;
              padding: 0.366vw 1.464vw;
              background: ${GREEN_COLOR};
              color: ${BG_LightGREY_COLOR};
              margin: 0.732vw 0 0 0;
              text-transform: capitalize;
            }
            :global(.upload_btn button span) {
              font-size: 0.951vw;
              font-family: "Museo-Sans" !important;
              font-weight: 600;
            }
            :global(.upload_btn button:focus),
            :global(.upload_btn button:active) {
              background: ${GREEN_COLOR};
              outline: none;
              box-shadow: none;
            }
            :global(.upload_btn button:hover) {
              background: ${GREEN_COLOR};
            }
            .closeIcon {
              position: absolute;
              top: 0;
              right: 0;
              width: 0.732vw;
              cursor: pointer;
            }
            // // media query
            // @media only screen and (min-width: 768px) and (max-width: 992px) {
            //   :global(.upload_btn button span),
            //   :global(.inactive_Btn button span) {
            //     font-size: 12px;
            //   }
            //   .heading {
            //     font-size: 1rem;
            //   }
            //   .closeIcon {
            //     width: 9px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 993px) and (max-width: 1199px) {
            //   :global(.upload_btn button span),
            //   :global(.inactive_Btn button span) {
            //     font-size: 12px;
            //   }
            //   .heading {
            //     font-size: 1rem;
            //   }
            //   .closeIcon {
            //     width: 9px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 1200px) and (max-width: 1400px) {
            //   :global(.upload_btn button span),
            //   :global(.inactive_Btn button span) {
            //     font-size: 13px;
            //   }
            //   .heading {
            //     font-size: 1.15rem;
            //   }
            //   .closeIcon {
            //     width: 10px;
            //   }
            // }
            // // media query
            // @media only screen and (min-width: 1920px) {
            //   :global(.upload_btn button span),
            //   :global(.inactive_Btn button span) {
            //     font-size: 17px;
            //   }
            //   .heading {
            //     font-size: 1.5rem;
            //   }
            //   .closeIcon {
            //     width: 14px;
            //   }
            // }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default ImageCropper;
