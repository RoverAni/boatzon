// Main React Components
import React, { Component } from "react";


// wrapping component
import Wrapper from "../../hoc/Wrapper";

// material-ui components
import Rating from "@material-ui/lab/Rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Star from "@material-ui/icons/Star";

class StarRating extends Component {
  render() {
    const { value, startFontSize, professionalPage } = this.props;
    return (
      <Wrapper>
        <Rating
          icon={<StarIcon style={{ fontSize: startFontSize || 10, marginBottom: "0px" }} />}
          emptyIcon={
            professionalPage ? (
              <Star style={{ fontSize: startFontSize || 10, color: "#d1d1d1", marginBottom: "0px" }} />
            ) : (
              <StarBorderIcon style={{ fontSize: startFontSize || 10, color: "#ffb400", marginBottom: "0px" }} />
            )
          }
          value={value}
          max={5}
          {...this.props}
        />

        <style jsx>
          {`
            :global(.MuiIconButton-label) {
              justify-content: flex-start !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default StarRating;
