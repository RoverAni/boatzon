import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { Multiselect } from "multiselect-react-dropdown";

class MultiSelectFropdown extends Component {

    
  render() {
    const { options, name, onSelect } = this.props;
    return;
    <Wrapper>
      <Multiselect
        options={options}
        // onSelect={onSelect}
        // onRemove={this.onRemove}
        displayValue={name}
        showCheckbox={true}
      />
    </Wrapper>;
  }
}

export default MultiSelectFropdown;
