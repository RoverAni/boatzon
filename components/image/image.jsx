import React from "react";
import { useAmp } from "next/amp";

import Wrapper from "../../hoc/wrapper";
import _JSXStyle from "styled-jsx/style";

const Image = props => {
  const { src, className, ...other } = props;
  return (
    <Wrapper>
      {useAmp() ? (
        <amp-img
          src={props.src}
          layout="fixed"
          className={props.className}
          {...other}
        />
      ) : (
        <img src={props.src} className={props.className} {...other} />
      )}
      <style jsx>
        {`
          .appLogo {
            width: 110px;
          }

          .appLogo:hover {
            cursor: pointer;
            transform: scale(1.1);
            transition: 0.3s;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default Image;
