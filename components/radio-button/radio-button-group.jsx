import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    color: "#999"
  },
  formControl: {
    margin: theme.spacing(3)
  },
  group: {
    margin: theme.spacing(1, 0)
  }
}));

export default function RadioButtonsGroup(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState("female");

  function handleChange(event) {
    setValue(event.target.value);
    props.onRadioChange ? props.onRadioChange(event.target.value) : "";
  }

  const { label, buttonGroupData } = props;

  console.log("EVENT --> ", value);

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">{label}</FormLabel>
        <RadioGroup
          aria-label={label}
          name={label}
          className={classes.group}
          value={value}
          onChange={handleChange}
        >
          {buttonGroupData && buttonGroupData.length > 0
            ? buttonGroupData.map((radioBtn, radioBtnIndex) => (
                <FormControlLabel
                  key={"radio-btn-" + radioBtnIndex}
                  value={radioBtn.value}
                  control={<Radio />}
                  label={radioBtn.label}
                  checked={value == radioBtn.value}
                />
              ))
            : ""}

          {/* <FormControlLabel value="male" control={<Radio />} label="Male" />
          <FormControlLabel value="other" control={<Radio />} label="Other" /> */}
        </RadioGroup>
      </FormControl>
    </div>
  );
}
