import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { withStyles } from "@material-ui/core/styles";

import Pagination from "@material-ui/lab/Pagination";
import {
  THEME_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_1,
  BG_LightGREY_COLOR,
} from "../../lib/config";

const styles = (theme) => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(2),
    },
  },
});

class PaginationComp extends Component {
  render() {
    const { classes, count } = this.props;
    return (
      <Wrapper>
        <div className={classes.root}>
          <Pagination
            count={count}
            shape="rounded"
            hidePrevButton
            hideNextButton
            size="small"
          />
        </div>
        <style jsx>
          {`
            :global(.MuiPaginationItem-page) {
              font-size: 11px !important;
              height: 25px;
              width: 23px;
              min-width: fit-content !important;
              border-radius: 0.146vw;
              color: ${GREY_VARIANT_1} !important;
              font-family: "Open Sans-SemiBold" !important;
              border: 0.0732vw solid ${BG_LightGREY_COLOR};
              margin-right: 2px;
            }
            :global(.MuiPaginationItem-page:active),
            :global(.MuiPaginationItem-page:focus) {
              outline: none !important;
              border: none !important;
            }
            :global(.MuiPaginationItem-page.Mui-selected) {
              background: ${THEME_COLOR} !important;
              color: ${WHITE_COLOR} !important;
              border: none !important;
            }
            // media query
            @media only screen and (min-width: 768px) and (max-width: 992px) {
              :global(.MuiPaginationItem-page) {
                font-size: 9px !important;
                height: 22px;
                width: 19px;
              }
            }
            // media query
            @media only screen and (min-width: 993px) and (max-width: 1199px) {
              :global(.MuiPaginationItem-page) {
                font-size: 9px !important;
                height: 22px;
                width: 19px;
              }
            }
            // media query
            @media only screen and (min-width: 1200px) and (max-width: 1400px) {
              :global(.MuiPaginationItem-page) {
                font-size: 10px !important;
                height: 23px;
                width: 20px;
              }
            }
            // media query
            @media only screen and (min-width: 1920px) {
              :global(.MuiPaginationItem-page) {
                font-size: 13px !important;
                height: 28px;
                width: 25px;
                margin-right: 3px;
              }
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(PaginationComp);
