import React, { Component } from "react";

class PaginationHOC extends Component {
  state = {
    scrolling: false,
    page: this.props.page || 0,
  };

  async componentDidMount() {
    document.body.addEventListener("scroll", this.handleScroll);
  }

  detectBottomTouch = (bottomDifferenceInPixel = 0) => {
    return (
      document.body.offsetHeight + document.body.scrollTop >=
      document.body.scrollHeight - bottomDifferenceInPixel
    );
  };

  handleScroll = () => {
    if (
      this.detectBottomTouch(400) &&
      !this.state.scrolling &&
      this.props.products &&
      this.props.products.length > 0 &&
      this.props.products.length !== 200
    ) {
      this.loadMoreData();
    }
  };

  loadMoreData = () => {
    let page = parseInt(this.state.page) + 1;
    this.setState(
      {
        page: page,
        scrolling: true,
      },
      async () => {
        try {
          await this.props.getItems(page, true);
          this.setState({
            scrolling: false,
          });
        } catch (e) {
          this.setState({
            scrolling: true,
          });
        }
      }
    );
  };

  componentDidUpdate(prevProps) {
    if (prevProps.page !== this.props.page && this.props.page == 0) {
      this.setState({
        page: this.props.page,
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
    // console.log("innerHight ",window.innerHeight);
  }
  render() {
    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
}

export default PaginationHOC;
