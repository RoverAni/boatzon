import React from 'react'
import NextHead from 'next/head'
import { APP_NAME, OG_TITLE, OG_DESC, WEB_LINK, DESK_LOGO, OG_LOGO, SMALL_LOGO, FB_APP_ID } from '../../lib/config';

const defaultTitle = APP_NAME;
const defaultOGTitle = OG_TITLE;
const defaultDescription = OG_DESC
const defaultOGURL = WEB_LINK
const defaultOGImage = DESK_LOGO

const CustomHead = props => (
    <NextHead>
        <meta charSet="UTF-8" />
        <title>{props.title || defaultTitle}</title>
        <meta
            name="description"
            content={props.description || defaultDescription}
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <meta property="og:url" content={props.url || defaultOGURL} />
        {/* <meta property="og:url" content={props.url || defaultOGURL} /> */}
        <meta property="og:title" content={props.ogTitle || defaultOGTitle || defaultTitle} />
        <meta
            property="og:description"
            content={props.description || defaultDescription}
        />
        <meta property="og:type" content="website" />

        {/* twitter */}
        <meta name="twitter:site" content={props.url || defaultOGURL} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:image" content={props.ogImage || defaultOGImage} />

        {/* og image */}
        <meta property="og:image" content={props.ogImage || OG_LOGO} />
        <meta property="og:image:width" content="512" />
        <meta property="og:image:height" content="512" />

        <meta property="fb:app_id" content={FB_APP_ID} />

        <link href={SMALL_LOGO} rel="icon" />
    </NextHead>
)


export default CustomHead
