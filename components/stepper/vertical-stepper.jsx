import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {
  GREY_VARIANT_3,
  Orange_Color,
  Light_Blue_Color,
  GREY_VARIANT_2,
} from "../../lib/config";

const styles = (theme) => ({
  Iconroot: {
    color: "#eaeaf0",
    display: "flex",
    height: 22,
    alignItems: "center",
  },
  labelCompleted: {
    color: `${GREY_VARIANT_2} !important`,
  },
  labelActive: {
    color: `${Orange_Color} !important`,
  },
  circleActive: {
    width: 8,
    height: 8,
    borderRadius: "50%",
    backgroundColor: `${Orange_Color}`,
  },
  circleCompleted: {
    width: 8,
    height: 8,
    borderRadius: "50%",
    backgroundColor: `${Light_Blue_Color} !important`,
  },
  circle: {
    width: 8,
    height: 8,
    borderRadius: "50%",
    backgroundColor: `${GREY_VARIANT_3}`,
  },
  completed: {
    color: "#784af4",
    zIndex: 1,
    fontSize: 18,
  },
  root: {
    width: "100%",
    display: "flex",
  },
  stepperRoot: {
    padding: "15px 20px 15px 0",
    // "& div:nth-child(2)": {
    //   position: "relative",
    //   top: "-4px",
    // },
    // "& div:nth-child(3)": {
    //   position: "relative",
    //   top: "-12px",
    // },
    // "& div:nth-child(4)": {
    //   position: "relative",
    //   top: "-16px",
    // },
    // "& div:nth-child(5)": {
    //   position: "relative",
    //   top: "-23px",
    // },
    // "& div:nth-child(6)": {
    //   position: "relative",
    //   top: "-28px",
    // },
    // "& div:nth-child(7)": {
    //   position: "relative",
    //   top: "-36px",
    // },
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  connectorActive: {
    "& $connectorLine": {
      borderColor: `${Light_Blue_Color} !important`,
    },
  },
  connectorCompleted: {
    "& $connectorLine": {
      borderColor: `${Light_Blue_Color} !important`,
    },
  },
  connectorVertical: {
    padding: "0 !important",
    marginLeft: "3px !important",
  },
  connectorLine: {
    height: "100%",
    borderLeftWidth: "2px",
    borderColor: `${GREY_VARIANT_3}`,
  },
  label: {
    fontSize: "12px !important",
    fontFamily: `"Open Sans-SemiBold" !important`,
    color: `${GREY_VARIANT_3}`,
    "&:first-letter": {
      textTransform: "capitalize",
    },
  },
});

class VerticalStepper extends React.Component {
  state = {
    activeStep: 0,
  };

  QontoStepIcon = (props) => {
    const { classes } = this.props;
    const { completed, active } = props;

    return (
      <div classes={{ root: classes.Iconroot }}>
        {active ? (
          <div className={classes.circleActive} />
        ) : completed ? (
          <div className={classes.circleCompleted} />
        ) : (
          <div className={classes.circle} />
        )}
      </div>
    );
  };

  handleNext = () => {
    this.setState((state) => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState((state) => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  handleStep = (step) => () => {
    this.setState({ activeStep: step });
  };

  render() {
    const { classes, getStepContent, getSteps } = this.props;
    const { activeStep } = this.state;
    const steps = getSteps();
    const connector = (
      <StepConnector
        classes={{
          vertical: classes.connectorVertical,
          line: classes.connectorLine,
          active: classes.connectorActive,
          completed: classes.connectorCompleted,
        }}
      />
    );
    return (
      <div className={classes.root}>
        <Stepper
          activeStep={activeStep}
          connector={connector}
          orientation="vertical"
          classes={{ root: classes.stepperRoot }}
        >
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel
                StepIconComponent={this.QontoStepIcon}
                classes={{
                  label: classes.label,
                  active: classes.labelActive,
                  completed: classes.labelCompleted,
                }}
                onClick={this.handleStep(index)}
              >
                {label}
              </StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          <Typography className={classes.instructions}>
            {getStepContent(activeStep)}
          </Typography>
        </div>
      </div>
    );
  }
}

VerticalStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(VerticalStepper);
