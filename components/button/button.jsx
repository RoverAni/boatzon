// Main React Components
import React from "react";

// material-ui components
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  buttonRoot: {
    margin: (props) => props.margin || "0.585vw",
    fontFamily: (props) => props.fontFamily || "Museo-Sans-Cyrl-Light",
    letterSpacing: "0.0366vw !important",
    color: (props) => props.color,
    background: (props) => props.background,
    height: (props) => props.height,
    "&:focus, &:active": {
      boxShadow: "none !important",
      outline: "none !important",
    },
  },
  label: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: (props) => props.fontSize,
  },
}));

export default function ButtonComp(props) {
  const { cssStyles } = props;
  const classes = useStyles(cssStyles);

  return (
    <Button
      classes={{ root: classes.buttonRoot, label: classes.label }}
      {...props}
    >
      {props.children}
    </Button>
  );
}
