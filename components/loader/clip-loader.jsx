import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { css } from "@emotion/core";
// First way to import
import ClipLoader from "react-spinners/ClipLoader";
import { THEME_COLOR } from "../../lib/config";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class PageClipLoader extends Component {
  render() {
    const { loading, size } = this.props;

    return (
      <Wrapper>
        <div className="sweet-loading">
          <ClipLoader
            css={override}
            size={size}
            color={THEME_COLOR}
            loading={loading}
          />
        </div>
      </Wrapper>
    );
  }
}

export default PageClipLoader;
