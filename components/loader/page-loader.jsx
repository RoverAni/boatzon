import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { css } from "@emotion/core";
// First way to import
import { CircleLoader } from "react-spinners";
import { THEME_COLOR } from "../../lib/config";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class PageLoader extends Component {
  render() {
    const { loading, color } = this.props;

    return (
      <Wrapper>
        <div className="sweet-loading">
          <CircleLoader
            css={override}
            size={50}
            color={color ? color : THEME_COLOR}
            loading={loading}
          />
        </div>
      </Wrapper>
    );
  }
}

export default PageLoader;
