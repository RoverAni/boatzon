import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { css } from "@emotion/core";
// First way to import
import BeatLoader from "react-spinners/BeatLoader";
import { THEME_COLOR } from "../../lib/config";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class PageBeatLoader extends Component {
  render() {
    const { loading, size } = this.props;

    return (
      <Wrapper>
        <div className="sweet-loading">
          <BeatLoader
            css={override}
            size={size}
            color={THEME_COLOR}
            loading={loading || false}
          />
        </div>
      </Wrapper>
    );
  }
}

export default PageBeatLoader;
