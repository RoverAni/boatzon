import React from "react";
import PropTypes from "prop-types";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core";
import Wrapper from "../../hoc/Wrapper";

const styles = (theme) => ({
  TopPaper: {
    boxShadow: "0px 8px 10px -5px rgba(0,0,0,0.2) !important",
    zIndex: "1 !important",
    borderTop: "0.073vw solid #dee2e6 !important",
    top: "5.856vw !important",
  },
  TopRoot: {
    position: "fixed",
    zIndex: "1 !important",
    top: "5.856vw !important",
    "& .MuiBackdrop-root": {
      top: "5.856vw !important",
    },
  },
});

class MainDrawer extends React.Component {
  render() {
    const {
      open,
      onOpen,
      onClose,
      anchor,
      discussionDrawer,
      classes,
    } = this.props;

    return (
      <Wrapper>
        <Drawer
          open={open}
          onOpen={onOpen}
          onClose={onClose}
          anchor={anchor}
          classes={
            discussionDrawer
              ? {
                  paper: classes.TopPaper,
                  root: classes.TopRoot,
                }
              : {}
          }
        >
          {this.props.children}
        </Drawer>
      </Wrapper>
    );
  }
}

MainDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainDrawer);
