import Link from "next/link";

const CustomLink = (props) => {
  const { href, as } = props;
  return (
    <Link href={href} as={as}>
      <a>{props.children}</a>
    </Link>
  );
};

export default CustomLink;
