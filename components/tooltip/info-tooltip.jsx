// Main React Components
import React, { Component } from "react";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import {
  WHITE_COLOR,
  Light_Blue_Color,
  BG_LightGREY_COLOR,
} from "../../lib/config";
// Prop-Type Checker
import PropTypes from "prop-types";
import Wrapper from "../../hoc/Wrapper";

const styles = (theme) => ({
  tooltipWhiteLabel: {
    background: `${BG_LightGREY_COLOR} !important`,
    color: `${WHITE_COLOR} !important`,
    fontFamily: `"Open Sans" !important`,
  },
  tooltipWhiteArrowLabel: {
    color: `${BG_LightGREY_COLOR} !important`,
  },
  tooltipPopper: {
    marginTop: "-0.585vw !important",
  },
  tooltipLabel: {
    background: `${Light_Blue_Color} !important`,
    color: `${WHITE_COLOR} !important`,
    fontFamily: `"Open Sans" !important`,
  },
  tooltipArrowLabel: {
    color: `${Light_Blue_Color} !important`,
  },
});

class InfoTooltip extends Component {
  render() {
    const { tooltipContent, placement, classes, whiteBg } = this.props;
    return (
      <Wrapper>
        <Tooltip
          classes={
            whiteBg
              ? {
                  popperArrow: classes.tooltipPopper,
                  tooltipArrow: classes.tooltipWhiteLabel,
                  arrow: classes.tooltipWhiteArrowLabel,
                }
              : {
                  tooltipArrow: classes.tooltipLabel,
                  arrow: classes.tooltipArrowLabel,
                }
          }
          title={tooltipContent}
          placement={placement}
          arrow
          {...this.props}
        >
          {this.props.children}
        </Tooltip>
        <style jsx>
          {`
            // :global(.MuiTooltip-popper) {
            //   position: absolute;
            //   bottom: -32% !important;
            //   left: 51.7% !important;
            //   transform: unset !important;
            // }
            :global(.MuiTooltip-tooltip) {
              background: ${this.props.color} !important;
              max-width: 15.373vw;
            }
            :global(.MuiTooltip-arrow) {
              color: ${this.props.color} !important;
            }
            :global(.MuiTooltip-tooltipPlacementBottom) {
              margin: 0.585vw 0;
            }
            :global(.MuiTooltip-tooltipPlacementTop) {
              margin: 0.585vw 0;
            }
            :global(.MuiTooltip-tooltipPlacementRight) {
              max-width: 12.445vw;
              margin: 0 0.585vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

InfoTooltip.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InfoTooltip);
