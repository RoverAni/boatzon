import React from "react";
import Dialog from "@material-ui/core/Dialog";
import { withStyles } from "@material-ui/core/styles";

const styles = (theme) => ({
  paper: {
    background: "transparent !important",
    overflow: "hidden !important",
    boxShadow: "none !important",
  },
});

class Model extends React.Component {
  render() {
    const { classes, authmodals } = this.props;

    return (
      <div>
        <Dialog
          open={this.props.open || false}
          onClose={this.props.onClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          classes={authmodals ? { paper: classes.paper } : {}}
          disableScrollLock={true}
        >
          {this.props.children}
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(Model);
