// Main React Components
import React, { Component } from "react";

// Prop-Type Checker
import PropTypes from "prop-types";

// materail-UI Components
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

// asstes and colors
import {
  THEME_COLOR,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  BG_LightGREY_COLOR,
  Orange_Color,
  Red_Color,
  GREY_VARIANT_1,
  GREY_VARIANT_8,
} from "../../lib/config";
import PageLoader from "../loader/page-loader";
import Wrapper from "../../hoc/Wrapper";

function TabContainer(props) {
  return <Typography component="div">{props.children}</Typography>;
}

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: "transparent",
  },
  tabLabel: {
    textTransform: "capitalize !important",
    minWidth: "50%",
    padding: "0.292vw 0",
    borderRadius: "0",
    fontSize: "0.878vw",
    alignItems: "center",
    fontFamily: `"Museo-Sans" !important`,
    fontWeight: "600 !important",
    height: "100%",
    minHeight: "1.830vw !important",
    backgroundColor: `${GREY_VARIANT_3} !important`,
    color: `${WHITE_COLOR} !important`,
    "&:focus, &:active": {
      outline: "none",
      backgroundColor: `${THEME_COLOR} !important`,
      color: `${BG_LightGREY_COLOR} !important`,
      fontWeight: "600 !important",
    },
    "& $span": {
      height: "100% !important",
      fontFamily: `"Museo-Sans" !important`,
      fontWeight: "600",
      color: `${WHITE_COLOR} !important`,
    },
    "&:first-child": {
      borderTopLeftRadius: "0.146vw !important",
      borderBottomLeftRadius: "0.146vw !important",
    },
    "&:last-child": {
      borderTopRightRadius: "0.146vw !important",
      borderBottomRightRadius: "0.146vw !important",
    },
  },
  indicator: {
    background: "none",
    top: 0,
  },
  flexRoot: {
    width: "100%",
    minHeight: "auto",
  },
  scroller: {
    height: "2.489vw !important",
  },
  flexContainer: {
    width: "100%",
    height: "100%",
    // justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  tabColor: {
    color: `${FONTGREY_COLOR} !important`,
    opacity: 1,
    fontWeight: "500 !important",
  },
  selected: {
    backgroundColor: `${THEME_COLOR} !important`,
    color: `${BG_LightGREY_COLOR} !important`,
    fontFamily: `"Museo-Sans" !important`,
    fontWeight: "600",
    "& $span": {
      fontFamily: `"Museo-Sans" !important`,
      fontWeight: "600",
      opacity: "1",
      color: `${BG_LightGREY_COLOR} !important`,
    },
  },
  wrapper: {
    display: "flex !important",
    flexDirection: "row !important",
  },
});

class VerticalTabs extends Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, tabs, tabcontent, planHeading, saveMsg } = this.props;
    const { value } = this.state;

    return (
      <Wrapper>
        <div className={classes.root}>
          <div className="row m-0">
            <div className="col-3 Tabs_Sec">
              <h5 className="planHeading">{planHeading}</h5>
              <Tabs
                {...this.props}
                value={value}
                onChange={this.handleChange}
                classes={{
                  root: classes.flexRoot,
                  indicator: classes.indicator,
                  scroller: classes.scroller,
                  flexContainer: classes.flexContainer,
                }}
              >
                {tabs.map((tab, key) => (
                  <Tab
                    key={key}
                    label={tab.label}
                    icon={value == key ? tab.activeIcon : tab.inactiveIcon}
                    classes={{
                      root: classes.tabLabel,
                      selected: classes.selected,
                      textColorInherit: classes.tabColor,
                      wrapper: classes.wrapper,
                    }}
                  />
                ))}
              </Tabs>
              <p className="saveMsg">{saveMsg}</p>
            </div>
            <div className="col pr-0">
              {tabcontent.map((tabcontent, key) =>
                key === value ? (
                  <TabContainer key={key}>{tabcontent.content}</TabContainer>
                ) : (
                  ""
                )
              )}
            </div>
          </div>
        </div>
        <style jsx>
          {`
            .min-padding {
              padding: 0 0.732vw;
            }
            .minArchive-padding {
              padding: 0 1.464vw;
            }
            .Tabs_Sec {
              max-width: 13.909vw;
            }
            .planHeading {
              font-size: 1.171vw;
              font-family: "Museo-Sans" !important;
              color: ${GREY_VARIANT_8};
              font-weight: 600;
              margin: 1.317vw 0;
            }
            .saveMsg {
              font-size: 0.805vw;
              font-family: "Open Sans-SemiBold" !important;
              color: ${THEME_COLOR};
              margin: 0.585vw 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

VerticalTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(VerticalTabs);
