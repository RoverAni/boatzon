// Main React Components
import React, { Component } from "react";

// Prop-Type Checker
import PropTypes from "prop-types";

// materail-UI Components
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

// asstes and colors
import {
  THEME_COLOR,
  Border_LightGREY_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  GREY_VARIANT_3,
  BG_LightGREY_COLOR,
  Orange_Color,
  Light_Yellow_Color,
  Red_Color,
  GREY_VARIANT_1,
  Inactive_Border_Icon,
  Active_Border_Icon,
} from "../../lib/config";

import Wrapper from "../../hoc/Wrapper";

function TabContainer(props) {
  return (
    <Typography className={props.chattabs ? "chatTabcontent" : props.preApprovedPage ? "col-9 mx-auto px-2 p-0" : ""} component="div">
      {props.children}
    </Typography>
  );
}

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  rootServicesQuote: {
    width: "100%",
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  Discussionroot: {
    flexGrow: 1,
    backgroundColor: "none !important",
  },
  chatbuttonLabel: {
    textTransform: "capitalize !important",
    minWidth: "3.660vw",
    border: `0.0732vw solid ${GREY_VARIANT_3}`,
    padding: "0.292vw 1.098vw",
    margin: "0 0.366vw 0 0 !important",
    height: "100% !important",
    alignItems: "center",
    fontSize: "0.732vw",
    minHeight: "1.464vw !important",
    lineHeight: "inherit !important",
    backgroundColor: `${WHITE_COLOR} !important`,
    color: `${GREY_VARIANT_1} !important`,
    "&:focus, &:active": {
      outline: "none",
      minHeight: "1.464vw !important",
      backgroundColor: `${GREY_VARIANT_3} !important`,
      color: `${FONTGREY_COLOR} !important`,
    },
    "& $span": {
      height: "auto !important",
      fontFamily: `"Open Sans" !important`,
      color: `${GREY_VARIANT_1} !important`,
    },
  },

  transactionbuttonLabel: {
    textTransform: "capitalize !important",
    minWidth: "50%",
    padding: "0.366vw 0.732vw",
    margin: "0 !important",
    height: "100%",
    fontSize: "0.805vw",
    alignItems: "center",
    minHeight: "100% !important",
    backgroundColor: `${WHITE_COLOR} !important`,
    color: `${GREY_VARIANT_1} !important`,
    "&:focus, &:active": {
      outline: "none",
      margin: "0 !important",
      backgroundColor: `${THEME_COLOR} !important`,
      color: `${WHITE_COLOR} !important`,
      fontWeight: "500 !important",
    },
    "& $span": {
      height: "100%",
      fontFamily: `"Open Sans" !important`,
      color: `${GREY_VARIANT_1} !important`,
    },
  },
  adddiscussionbuttonLabel: {
    textTransform: "uppercase !important",
    minWidth: "50%",
    fontSize: "0.805vw",
    padding: "0.292vw 0vw",
    alignItems: "center",
    height: "2.342vw !important",
    borderRadius: "0",
    minHeight: "2.342vw !important",
    backgroundColor: `${WHITE_COLOR} !important`,
    color: `${FONTGREY_COLOR} !important`,
    "&:focus, &:active": {
      outline: "none",
      backgroundColor: `${Orange_Color} !important`,
      color: `${WHITE_COLOR} !important`,
    },
    "& $span": {
      height: "100% !important",
      fontFamily: `"Open Sans-SemiBold" !important`,
      display: "flex",
      lineHeight: "1 !important",
      alignItems: "center",
      "& $span": {
        lineHeight: "1 !important",
      },
    },
    "&:first-child": {
      borderTopLeftRadius: "0.146vw !important",
      borderBottomLeftRadius: "0.146vw !important",
    },
    "&:last-child": {
      borderTopRightRadius: "0.146vw !important",
      borderBottomRightRadius: "0.146vw !important",
    },
  },
  priceDropdownLabel: {
    textTransform: "capitalize !important",
    minWidth: "50%",
    padding: "0.585vw 1.098vw",
    borderRadius: "0",
    fontSize: "0.805vw",
    height: "100% !important",
    minHeight: "100% !important",
    backgroundColor: `${BG_LightGREY_COLOR} !important`,
    color: `${GREY_VARIANT_1} !important`,
    "&:focus, &:active": {
      outline: "none",
      backgroundColor: `${THEME_COLOR} !important`,
      color: `${WHITE_COLOR} !important`,
    },
    "& $span": {
      fontFamily: `"Open Sans" !important`,
      color: `${GREY_VARIANT_1} !important`,
    },
    "&:first-child": {
      borderTopLeftRadius: "0.146vw !important",
      borderBottomLeftRadius: "0.146vw !important",
    },
    "&:last-child": {
      borderTopRightRadius: "0.146vw !important",
      borderBottomRightRadius: "0.146vw !important",
    },
  },
  tabButtonLabel: {
    textTransform: "uppercase !important",
    minWidth: "49%",
    // padding: "0.732vw 0.658vw",
    padding: "0.588vw 0.4815vw",
    borderRadius: "0",
    fontSize: "0.833vw",
    height: "2.917vw !important",
    minHeight: "100% !important",
    backgroundColor: `${GREY_VARIANT_3} !important`,
    color: `${BG_LightGREY_COLOR} !important`,
    marginTop: "0.313vw",
    "&:focus, &:active": {
      outline: "none",
      backgroundColor: `${THEME_COLOR} !important`,
      color: `${BG_LightGREY_COLOR} !important`,
    },
    "& $span": {
      fontFamily: `"Museo-Sans" !important`,
      color: `${BG_LightGREY_COLOR} !important`,
    },
    "&:first-child, &:nth-child(3)": {
      borderTopLeftRadius: "0.146vw !important",
      borderBottomLeftRadius: "0.146vw !important",
    },
    "&:last-child, &:nth-child(2)": {
      borderTopRightRadius: "0.146vw !important",
      borderBottomRightRadius: "0.146vw !important",
    },
  },
  tabLabel: {
    textTransform: "uppercase !important",
    minWidth: "50%",
    fontSize: "0.805vw",
    minHeight: "1.464vw !important",
    color: `${GREY_VARIANT_1} !important`,
    letterSpacing: "0.0292vw !important",
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
    "& span": {
      fontFamily: `"Museo-Sans" !important`,
      letterSpacing: "0.0219vw !important",
      opacity: "0.9",
      color: `${GREY_VARIANT_1} !important`,
    },
  },
  tabLabelMyBoat: {
    textTransform: "capitalize !important",
    minWidth: "auto",
    minHeight: "1.464vw !important",
    fontWeight: "599",
    fontSize: "0.878vw",
    color: `${FONTGREY_COLOR} !important`,
    opacity: "0.7 !important",
    letterSpacing: "0.0366vw !important",
    fontFamily: `"Open Sans-SemiBold" !important`,
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
    "& span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
    },
  },
  tabLabelFollowingSmall: {
    textTransform: "capitalize !important",
    minWidth: "auto",
    fontSize: "0.805vw",
    minHeight: "1.464vw !important",
    color: `${GREY_VARIANT_1} !important`,
    opacity: 1,
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
    "& span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
      opacity: 1,
    },
  },
  tabLabelPreApproved: {
    textTransform: "capitalize !important",
    minWidth: "50% !important",
    fontSize: "0.805vw",
    minHeight: "1.464vw !important",
    padding: "0.439vw 0",
    fontWeight: "599",
    borderBottom: "0.732vw solid transparent",
    borderImage: `url(${Inactive_Border_Icon}) 45% round`,
    color: `${GREY_VARIANT_1} !important`,
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
    "& span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
    },
    [theme.breakpoints.up(1900)]: {
      borderBottom: "1.024vw solid transparent",
      borderImage: `url(${Inactive_Border_Icon}) 31% round`,
    },
  },
  tabLabelServices: {
    textTransform: "capitalize !important",
    minWidth: "unset !important",
    maxWidth: "unset !important",
    fontSize: "0.805vw",
    minHeight: "1.464vw !important",
    padding: "0.439vw 0",
    fontWeight: "599",
    borderBottom: "0.951vw solid transparent",
    borderImage: `url(${Inactive_Border_Icon}) 45% round`,
    color: `${GREY_VARIANT_1} !important`,
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
    "& span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
    },
    [theme.breakpoints.up(1900)]: {
      borderBottom: "1.024vw solid transparent",
      borderImage: `url(${Inactive_Border_Icon}) 31% round`,
    },
  },
  tabLabelSmall: {
    textTransform: "capitalize !important",
    minWidth: "auto",
    fontSize: "0.805vw",
    padding: "0.439vw 0.625vw",
    minHeight: "1.464vw !important",
    fontWeight: "599",
    color: `${FONTGREY_COLOR} !important`,
    letterSpacing: "0.0366vw !important",
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
    "& span": {
      fontFamily: `"Open Sans-SemiBold" !important`,
    },
  },
  indicator: {
    background: `${THEME_COLOR}`,
    bottom: "0",
  },
  indicatorMyBoat: {
    background: `${THEME_COLOR}`,
    bottom: "0",
  },
  indicatorServices: {
    background: "none",
    bottom: "0",
  },
  indicatorButton: {
    background: "none",
    top: 0,
  },
  flexRoot: {
    width: "100%",
    minHeight: "auto",
  },
  flexRootServices: {
    width: "auto",
    minHeight: "auto",
  },
  flexRootServicesQuote: {
    width: "100%",
    minHeight: "auto",
  },
  scroller: {
    height: "2.781vw",
  },
  scrollerServices: {
    height: "100%",
    height: "auto",
  },
  scrollerMyBoat: {
    minHeight: "2.781vw",
  },
  scrollerFollowing: {
    height: "2.342vw",
    minHeight: "2.489vw",
  },
  scrollerButton: {
    height: "100%",
  },
  scrollerDiscussionButton: {
    height: "2.342vw !important",
  },
  scrollerTransactionButton: {
    height: "2.342vw",
  },
  scrollerChatButton: {
    height: "auto",
    marginBottom: "0.585vw",
  },
  flexContainerServices: {
    width: "100%",
    alignItems: "center",
    position: "relative",
    margin: "0 auto",
    maxHeight: "fit-content",
    height: "100%",
    alignItems: "center",
    margin: "0 auto",
  },
  flexContainerMyBoat: {
    width: "100%",
    minHeight: "2.635vw",
    // justifyContent: "center",
    alignItems: "center",
    position: "relative",
    margin: "0 auto",
    backgroundColor: `${BG_LightGREY_COLOR}`,
  },
  flexContainerFollowing: {
    width: "100%",
    minHeight: "2.342vw",
    // justifyContent: "center",
    alignItems: "center",
    position: "relative",
    margin: "0 auto",
    backgroundColor: `${BG_LightGREY_COLOR}`,
  },
  flexContainer: {
    borderBottom: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    width: "100%",
    minHeight: "2.781vw",
    position: "relative",
    // justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  flexContainerChatButton: {
    width: "100%",
    borderRadius: "0.146vw",
    position: "relative",
    height: "1.683vw",
    // justifyContent: "center",
    alignItems: "center",
    position: "relative",
    margin: "0 auto",
  },
  flexContainerTransactionButton: {
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    width: "100%",
    borderRadius: "0.146vw",
    height: "2.342vw",
    position: "relative",
    // justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
    "& $button:first-child": {
      borderTopLeftRadius: "0.146vw",
      borderBottomLeftRadius: "0.146vw",
    },
    "& $button:last-child": {
      borderTopRightRadius: "0.146vw",
      borderBottomRightRadius: "0.146vw",
    },
  },
  flexContainerButton: {
    // borderBottom: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    width: "100%",
    maxHeight: "fit-content",
    justifyContent: "space-between",
    flexWrap: "wrap",
    alignItems: "center",
    margin: "0 auto",
  },
  flexContainerpriceDropdownButton: {
    width: "100%",
    maxHeight: "fit-content",
    height: "100%",
    alignItems: "center",
    margin: "0 auto",
  },
  tabColor: {
    color: `${FONTGREY_COLOR} !important`,
    opacity: 1,
  },
  tabColorServices: {
    color: `${GREY_VARIANT_1} !important`,
    opacity: 1,
  },
  tabColorFollowing: {
    color: `${GREY_VARIANT_1} !important`,
    opacity: 1,
  },
  tabButtonColor: {
    color: `${WHITE_COLOR} !important`,
    opacity: 1,
  },
  adddiscussionbuttonColor: {
    color: `${FONTGREY_COLOR} !important`,
    opacity: 1,
    fontWeight: "500 !important",
  },
  selected: {
    color: `${THEME_COLOR} !important`,
    fontFamily: "Museo-Sans-Cyrl-Semibold !important",
    letterSpacing: "0.0292vw !important",
    "& $span": {
      color: `${THEME_COLOR} !important`,
      fontFamily: "Museo-Sans-Cyrl-Semibold !important",
      letterSpacing: "0.0292vw !important",
      opacity: "1",
    },
  },
  selectedMyBoat: {
    color: `${THEME_COLOR} !important`,
    fontFamily: `"Open Sans-SemiBold" !important`,
    letterSpacing: "0.0292vw !important",
    "& $span": {
      color: `${THEME_COLOR} !important`,
      fontFamily: `"Open Sans-SemiBold" !important`,
      letterSpacing: "0.0292vw !important",
      opacity: "1",
    },
  },
  selectedServices: {
    minWidth: "unset !important",
    maxWidth: "unset !important",
    color: `${THEME_COLOR} !important`,
    borderBottom: "0.951vw solid transparent",
    borderImage: `url(${Active_Border_Icon}) 45% round`,
    fontFamily: `"Open Sans-SemiBold" !important`,
    "& $span": {
      color: `${THEME_COLOR} !important`,
      fontFamily: `"Open Sans-SemiBold" !important`,
      opacity: "1",
    },
    [theme.breakpoints.up(1900)]: {
      borderBottom: "1.024vw solid transparent",
      borderImage: `url(${Active_Border_Icon}) 31% round`,
    },
  },
  selectedPreApproved: {
    minWidth: "50%!important",
    color: `${THEME_COLOR} !important`,
    borderBottom: "0.732vw solid transparent",
    borderImage: `url(${Active_Border_Icon}) 45% round`,
    fontFamily: `"Open Sans-SemiBold" !important`,
    "& $span": {
      color: `${THEME_COLOR} !important`,
      fontFamily: `"Open Sans-SemiBold" !important`,
      opacity: "1",
    },
    [theme.breakpoints.up(1900)]: {
      borderBottom: "1.024vw solid transparent",
      borderImage: `url(${Active_Border_Icon}) 31% round`,
    },
  },
  selectedButton: {
    backgroundColor: `${THEME_COLOR} !important`,
    color: `${BG_LightGREY_COLOR} !important`,
    fontFamily: `"Museo-Sans" !important`,
  },
  selectedpriceDropdownButton: {
    backgroundColor: `${THEME_COLOR} !important`,
    color: `${WHITE_COLOR} !important`,
    fontFamily: `"Open Sans" !important`,
    "& $span": {
      fontFamily: `"Open Sans" !important`,
      opacity: "1",
      color: `${WHITE_COLOR} !important`,
    },
  },
  selectedChatButton: {
    minHeight: "1.464vw !important",
    margin: "0 0.366vw 0 0 !important",
    backgroundColor: `${GREY_VARIANT_3} !important`,
    color: `${FONTGREY_COLOR} !important`,
    fontFamily: `"Open Sans" !important`,
    "& $span": {
      height: "auto !important",
      fontFamily: `"Open Sans" !important`,
      opacity: "1",
      color: `${FONTGREY_COLOR} !important`,
    },
  },
  selectedTransactionButton: {
    backgroundColor: `${THEME_COLOR} !important`,
    color: `${WHITE_COLOR} !important`,
    fontFamily: `"Open Sans" !important`,
    "& $span": {
      height: "auto !important",
      fontFamily: `"Open Sans" !important`,
      opacity: "1",
      color: `${WHITE_COLOR} !important`,
    },
  },
  selectedAddDiscussionButton: {
    backgroundColor: `${Orange_Color} !important`,
    opacity: "1 !important",
    color: `${WHITE_COLOR} !important`,
    fontFamily: `"Open Sans-SemiBold" !important`,
  },
  wrapper: {
    display: "flex !important",
    flexDirection: "row !important",
  },
  servicesWrapper: {
    display: "flex !important",
    flexDirection: "column !important",
  },
});

class SimpleTabs extends Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
    if (this.props && this.props.hasManualTabSelector) {
      this.props.setTabValue(value);
    }
  };

  render() {
    const {
      classes,
      tabs,
      tabcontent,
      tabbutton,
      myboatstabs,
      normalsmalltabs,
      adddiscussionbutton,
      tabtransactionbutton,
      chattabs,
      archivechat,
      followingsmalltabs,
      loanfinancingservices,
      servicesValue,
      handleDiscussionChange,
      addDiscussionValue,
      handleTabChange,
      createPostValue,
      editProProfile,
      noValue,
      messengerTabs,
      priceDropdown,
      preApprovedPage,
      loanValue,
      editPostValue,
      handleEditTabChange,
    } = this.props;
    const { value } = this.state;

    return (
      <Wrapper>
        <div
          className={
            adddiscussionbutton || tabtransactionbutton || chattabs
              ? classes.Discussionroot
              : loanfinancingservices
              ? classes.rootServicesQuote
              : classes.root
          }
        >
          <div
            className={myboatstabs ? "container p-md-0" : chattabs && archivechat ? "minArchive-padding" : chattabs ? "min-padding" : ""}
          >
            <div
              className={
                myboatstabs
                  ? "screenWidth mx-auto d-flex"
                  : normalsmalltabs || followingsmalltabs
                  ? " "
                  : priceDropdown
                  ? "col-7 p-0"
                  : adddiscussionbutton
                  ? "mx-auto"
                  : tabtransactionbutton
                  ? "col-3 d-flex p-0"
                  : preApprovedPage
                  ? "col-9 mx-auto px-2 p-0"
                  : loanfinancingservices
                  ? "d-flex"
                  : "d-flex"
              }
              style={adddiscussionbutton ? { width: "42%" } : {}}
            >
              <Tabs
                {...this.props}
                value={
                  loanfinancingservices
                    ? servicesValue
                    : preApprovedPage
                    ? loanValue
                    : adddiscussionbutton
                    ? addDiscussionValue
                    : tabbutton
                    ? createPostValue
                    : followingsmalltabs
                    ? editPostValue
                    : editProProfile
                    ? noValue
                    : value
                }
                onChange={
                  loanfinancingservices
                    ? ""
                    : preApprovedPage
                    ? ""
                    : adddiscussionbutton
                    ? handleDiscussionChange
                    : tabbutton
                    ? handleTabChange
                    : followingsmalltabs
                    ? handleEditTabChange
                    : this.handleChange
                }
                classes={
                  tabbutton
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorButton,
                        scroller: classes.scrollerButton,
                        flexContainer: classes.flexContainerButton,
                      }
                    : adddiscussionbutton
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorButton,
                        scroller: classes.scrollerDiscussionButton,
                        flexContainer: classes.flexContainerButton,
                      }
                    : tabtransactionbutton
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorButton,
                        scroller: classes.scrollerTransactionButton,
                        flexContainer: classes.flexContainerTransactionButton,
                      }
                    : chattabs
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorButton,
                        scroller: classes.scrollerChatButton,
                        flexContainer: classes.flexContainerChatButton,
                      }
                    : myboatstabs
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorMyBoat,
                        scroller: classes.scrollerMyBoat,
                        flexContainer: classes.flexContainerMyBoat,
                      }
                    : normalsmalltabs
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorMyBoat,
                        scroller: classes.scrollerMyBoat,
                        flexContainer: classes.flexContainerMyBoat,
                      }
                    : loanfinancingservices
                    ? {
                        root: classes.flexRootServicesQuote,
                        indicator: classes.indicatorServices,
                        scroller: classes.scrollerServices,
                        flexContainer: classes.flexContainerServices,
                      }
                    : preApprovedPage
                    ? {
                        root: classes.flexRootServices,
                        indicator: classes.indicatorServices,
                        scroller: classes.scrollerServices,
                        flexContainer: classes.flexContainerServices,
                      }
                    : followingsmalltabs
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorMyBoat,
                        scroller: classes.scrollerFollowing,
                        flexContainer: classes.flexContainerFollowing,
                      }
                    : priceDropdown
                    ? {
                        root: classes.flexRoot,
                        indicator: classes.indicatorButton,
                        scroller: classes.scrollerButton,
                        flexContainer: classes.flexContainerpriceDropdownButton,
                      }
                    : {
                        root: classes.flexRoot,
                        indicator: classes.indicator,
                        scroller: classes.scroller,
                        flexContainer: classes.flexContainer,
                      }
                }
              >
                {tabs.map((tab, key) => (
                  <Tab
                    key={key}
                    label={tab.label}
                    icon={
                      adddiscussionbutton
                        ? addDiscussionValue == key
                          ? tab.activeIcon
                          : tab.inactiveIcon
                        : loanfinancingservices
                        ? servicesValue == key
                          ? tab.activeIcon
                          : tab.inactiveIcon
                        : value == key
                        ? tab.activeIcon
                        : tab.inactiveIcon
                    }
                    classes={
                      tabbutton
                        ? {
                            root: classes.tabButtonLabel,
                            selected: classes.selectedButton,
                            textColorInherit: classes.tabButtonColor,
                            wrapper: classes.wrapper,
                          }
                        : priceDropdown
                        ? {
                            root: classes.priceDropdownLabel,
                            selected: classes.selectedpriceDropdownButton,
                            textColorInherit: classes.tabButtonColor,
                            wrapper: classes.wrapper,
                          }
                        : adddiscussionbutton
                        ? {
                            root: classes.adddiscussionbuttonLabel,
                            selected: classes.selectedAddDiscussionButton,
                            textColorInherit: classes.adddiscussionbuttonColor,
                            wrapper: classes.wrapper,
                          }
                        : tabtransactionbutton
                        ? {
                            root: classes.transactionbuttonLabel,
                            selected: classes.selectedTransactionButton,
                            textColorInherit: classes.adddiscussionbuttonColor,
                            wrapper: classes.wrapper,
                          }
                        : chattabs
                        ? {
                            root: classes.chatbuttonLabel,
                            selected: classes.selectedChatButton,
                            textColorInherit: classes.adddiscussionbuttonColor,
                            wrapper: classes.wrapper,
                          }
                        : myboatstabs
                        ? {
                            root: classes.tabLabelMyBoat,
                            selected: classes.selectedMyBoat,
                            textColorInherit: classes.tabColor,
                            wrapper: classes.wrapper,
                          }
                        : normalsmalltabs
                        ? {
                            root: classes.tabLabelSmall,
                            selected: classes.selectedMyBoat,
                            textColorInherit: classes.tabColor,
                            wrapper: classes.wrapper,
                          }
                        : loanfinancingservices
                        ? {
                            root: classes.tabLabelServices,
                            selected: classes.selectedServices,
                            textColorInherit: classes.tabColorServices,
                            wrapper: classes.servicesWrapper,
                          }
                        : preApprovedPage
                        ? {
                            root: classes.tabLabelPreApproved,
                            selected: classes.selectedPreApproved,
                            textColorInherit: classes.tabColorServices,
                            wrapper: classes.servicesWrapper,
                          }
                        : followingsmalltabs
                        ? {
                            root: classes.tabLabelFollowingSmall,
                            selected: classes.selectedMyBoat,
                            textColorInherit: classes.tabColorFollowing,
                            wrapper: classes.wrapper,
                          }
                        : {
                            root: classes.tabLabel,
                            selected: classes.selected,
                            textColorInherit: classes.tabColor,
                            wrapper: classes.wrapper,
                          }
                    }
                  />
                ))}
              </Tabs>
            </div>
          </div>
          {tabcontent.map((tabcontent, key) =>
            loanfinancingservices ? (
              key === servicesValue ? (
                <TabContainer key={key}>{tabcontent.content}</TabContainer>
              ) : (
                ""
              )
            ) : preApprovedPage ? (
              key === loanValue ? (
                <TabContainer key={key} preApprovedPage={preApprovedPage}>
                  {tabcontent.content}
                </TabContainer>
              ) : (
                ""
              )
            ) : adddiscussionbutton ? (
              key === addDiscussionValue ? (
                <TabContainer key={key}>{tabcontent.content}</TabContainer>
              ) : (
                ""
              )
            ) : tabbutton ? (
              key === createPostValue ? (
                <TabContainer key={key}>{tabcontent.content}</TabContainer>
              ) : (
                ""
              )
            ) : followingsmalltabs ? (
              key === editPostValue ? (
                <TabContainer key={key}>{tabcontent.content}</TabContainer>
              ) : (
                ""
              )
            ) : key === value ? (
              <TabContainer chattabs={chattabs} key={key}>
                {tabcontent.content}
              </TabContainer>
            ) : (
              ""
            )
          )}
        </div>
        <style jsx>
          {`
            .min-padding {
              padding: 0 1.098vw;
            }
            .minArchive-padding {
              padding: 0 1.464vw;
            }
            :global(.tabcontent) {
              padding: 0 0 0 0.366vw;
            }
            :global(.chatTabcontent) {
              height: calc(30vw);
              overflow-y: scroll;
              padding: 0 0 0 0.366vw;
            }
            :global(.LoanServicesPage .MuiTab-root) {
              width: calc(100% / ${this.props.tabs.length});
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

SimpleTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(SimpleTabs);
