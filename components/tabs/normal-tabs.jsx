// Main React Components
import React, { Component } from "react";

// Prop-Type Checker
import PropTypes from "prop-types";

// materail-UI Components
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Paper from "@material-ui/core/Paper";
// asstes and colors
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  Border_LightGREY_COLOR,
  BOX_SHADOW_GREY,
} from "../../lib/config";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import CustomLink from "../Link/Link";
import ServiceDropdown from "../../containers/services-dropdown/service-dropdown";

function TabContainer(props) {
  return <Typography component="div">{props.children}</Typography>;
}

const styles = (theme) => ({
  tabLabel: {
    textTransform: "capitalize !important",
    minHeight: "1.464vw !important",
    color: `${FONTGREY_COLOR} !important`,
    fontSize: "0.878vw",
    padding: "0.366vw 0.732vw",
    minWidth: "auto",
    opacity: "1 !important",
    lineHeight: "1.464vw !important",
    letterSpacing: "0.0292vw !important",
    "&:focus, &:active, &:hover": {
      outline: "none",
      color: `${THEME_COLOR} !important`,
    },
    "& span": {
      fontFamily: "Open Sans-SemiBold !important",
    },
  },
  indicator: {
    background: `${THEME_COLOR}`,
    top: "0",
  },
  flexRoot: {
    width: "100%",
    minHeight: "auto",
  },
  scroller: {
    height: "auto",
  },
  flexContainer: {
    maxHeight: "2.562vw",
    justifyContent: "space-evenly",
    alignItems: "center",
    position: "relative",
  },
  tabColor: {
    color: `${FONTGREY_COLOR} !important`,
    opacity: 1,
  },
  tabColor1: {
    color: `${FONTGREY_COLOR} !important`,
    opacity: 1,
    // "&:hover": {
    //    borderBottom: `2px solid ${THEME_COLOR}`,
    // },
  },
  selected: {
    color: `${THEME_COLOR} !important`,
    opacity: "1 !important",
    fontFamily: "Open Sans-SemiBold !important",
  },
  borderTop: {
    borderTop: `0.0732vw solid ${Border_LightGREY_COLOR}`,
  },
  menuItem: {
    fontFamily: "Open Sans-SemiBold !important",
    textTransform: "capitalize !important",
    fontSize: "0.805vw",
    "&:hover": {
      color: `${THEME_COLOR}`,
    },
    "&:focus, &:active": {
      outline: "none",
      color: `${THEME_COLOR}`,
    },
  },
  menuList: {
    padding: "0.366vw 0",
  },
});

class NormalTabs extends Component {
  state = {
    dropdown: false,
  };

  handlePopoverOpen = () => {
    this.setState({
      dropdown: true,
    });
  };

  handlePopoverClose = () => {
    this.setState({
      dropdown: false,
    });
  };

  /** tabColor1 has hover effect (with 2px bottom blue color) */

  render() {
    const {
      classes,
      tabs,
      tabcontent,
      ladingpagetabs,
      value,
      onChangeTab,
    } = this.props;

    return (
      <Wrapper>
        {/* <div className="position-relative"> */}
        <div className={ladingpagetabs ? "col-12 p-0 mx-auto MainTabs" : ""}>
          <div className={ladingpagetabs ? "container p-md-0" : ""}>
            <div className={ladingpagetabs ? "screenWidth mx-auto" : ""}>
              <div className={ladingpagetabs ? "row m-0" : ""}>
                <div
                  className={
                    ladingpagetabs
                      ? "col-md-8 col-lg-7 col-xl-7 mx-auto d-flex position-relative"
                      : ""
                  }
                >
                  <Tabs
                    {...this.props}
                    value={value || false}
                    onChange={onChangeTab}
                    classes={{
                      root: classes.flexRoot,
                      indicator: classes.indicator,
                      scroller: classes.scroller,
                      flexContainer: classes.flexContainer,
                    }}
                  >
                    {tabs.map((tab, key) => (
                      <Tab
                        key={key}
                        label={tab.label}
                        classes={{
                          root: classes.tabLabel,
                          selected: classes.selected,
                          textColorInherit: ladingpagetabs
                            ? classes.tabColor1
                            : classes.tabColor,
                        }}
                        onMouseEnter={
                          tab.label == "Services" ? this.handlePopoverOpen : () => {}
                        }
                      />
                    ))}
                  </Tabs>
                  {this.state.dropdown ? (
                    <Paper
                      className="paper"
                      onMouseLeave={this.handlePopoverClose}
                    >
                      <MenuList
                        classes={{ root: classes.menuList }}
                        className="container mx-auto"
                      >
                        <ServiceDropdown />
                      </MenuList>
                    </Paper>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className={classes.borderTop}>
          {tabcontent.map((tabcontent, key) =>
            key === value ? (
              <TabContainer key={key}>{tabcontent.content}</TabContainer>
            ) : (
              ""
            )
          )}
        </div>
        {/* </div> */}

        <style jsx>
          {`
            :global(.paper) {
              width: 245%;
              position: absolute;
              top: 101%;
              left: 50%;
              transform: translate(-50%, 0);
              box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.1);
              border-radius: 0;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

NormalTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(NormalTabs);
