// Main React Components
import React, { Component } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Wrapper from "../../hoc/Wrapper";

const styles = theme => ({
  typography: {
    padding: "0 1.171vw"
  },
  popoverPaper: {
    top: "13.909vw !important",
    left: "14.641vw !important"
  }
});

class SimplePopover extends Component {
  render() {
    const { classes, handleClick, open, button } = this.props;

    return (
      <Wrapper>
        <Button onClick={handleClick}>{button}</Button>
        <Popover
          open={open}
          onClose={handleClick}
          classes={{
            paper: classes.popoverPaper
          }}
        >
          <Typography className={classes.typography}>
            {this.props.children}
          </Typography>
        </Popover>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(SimplePopover);
