import React from "react";
import _JSXStyle from "styled-jsx/style";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";

import { handleBreadCrumbs, getMyURL } from "../../lib/breadcrumbs/breadcrumbs";
import {
  FONTGREY_COLOR,
  FONT_FAMILY,
  GREY_VARIANT_2,
  THEME_COLOR,
} from "../../lib/config";

const useStyles = makeStyles((theme) => ({
  boatsBreadcrumbsLink: {
    fontSize: "0.625vw",
    color: `${FONTGREY_COLOR}`,
    textTransform: "capitalize",
    "& a": {
      fontFamily: "Open Sans !important",
      letterSpacing: "0.0146vw !important",
      cursor: "pointer",
    },
  },
  breadcrumbsLink: {
    fontSize: "0.625vw",
    color: `${GREY_VARIANT_2}`,
    textTransform: "capitalize",
    "& a": {
      fontFamily: "Open Sans !important",
      letterSpacing: "0.0146vw !important",
      cursor: "pointer",
    },
  },
  separator: {
    fontSize: "0.878vw",
    margin: "0 0.219vw",
    color: `${GREY_VARIANT_2}`,
  },
  LastNode: {
    fontSize: "0.625vw",
    color: `${THEME_COLOR}`,
    textTransform: "capitalize",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.0146vw !important",
  },
}));

export default function CustomBreadCrumbs(props) {
  const classes = useStyles();

  const { breadcrumbs, separator, boatsPage } = props;
  return (
    <>
      <Breadcrumbs
        aria-label="breadcrumb"
        classes={
          boatsPage
            ? { li: classes.boatsBreadcrumbsLink, separator: classes.separator }
            : { li: classes.breadcrumbsLink, separator: classes.separator }
        }
        separator={separator}
      >
        {breadcrumbs &&
          breadcrumbs.map((item, key) =>
            key === breadcrumbs.length - 1 ? (
              <Typography className={classes.LastNode}>{item}</Typography>
            ) : (
              <Link
                target="_blank"
                key={"bs-" + key}
                value={key}
                onClick={() => handleBreadCrumbs(breadcrumbs, key)}
              >
                {item}
              </Link>
            )
          )}
      </Breadcrumbs>
    </>
  );
}
