// Main React Components
import React from "react";
// Prop-Type Checker
import PropTypes from "prop-types";
// material-ui components
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import {
  GREY_VARIANT_3,
  GREY_VARIANT_1,
  WHITE_COLOR,
  Minus_Grey_Icon,
  Plus_Lightgrey_Icon,
  FONTGREY_COLOR,
  BOX_SHADOW_GREY,
  Chevron_Down,
  Chevron_Down_Blue,
  Chevron_Right,
  Chevron_Left_Blue,
  Chevron_Left,
  Chevron_Down_White,
  BG_LightGREY_COLOR,
  THEME_COLOR,
  GREY_VARIANT_2,
  THEME_COLOR_Opacity,
  BLACK_COLOR,
  GREY_VARIANT_17,
  GREY_VARIANT_8,
  Chevron_Left_Darkgrey,
  Chevron_Down_DarkGrey,
} from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";

const styles = (theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    borderBottom: `0.0732vw solid ${GREY_VARIANT_3}`,
    padding: "0",
    marginBottom: "0.732vw",
    // boxShadow: `0px 0px 1px 0px ${BOX_SHADOW_GREY}`
  },
  productsRoot: {
    width: "100%",
    backgroundColor: `${BG_LightGREY_COLOR}`,
    padding: "0",

    // marginBottom: "0.732vw",
  },
  servicesRoot: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    borderBottom: `none !important`,
    padding: "0",
    borderRadius: "0.219vw",
    marginBottom: "0.732vw",
    // boxShadow: `0px 0px 1px 0px ${BOX_SHADOW_GREY}`
  },
  professionalsRoot: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    padding: "0",
    marginBottom: "0.732vw",
  },
  accSettingRoot: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    padding: "0",
  },
  primary: {
    fontSize: "0.915vw",
    fontWeight: "600",
    opacity: "1",
    color: `${FONTGREY_COLOR} !important`,
    fontFamily: "Museo-Sans-Cyrl !important",
    letterSpacing: "0.0366vw !important",
  },
  servicesPrimary: {
    opacity: "1",
    padding: "0",
    fontSize: "0.878vw",
    cursor: "pointer",
  },
  professionalsPrimary: {
    opacity: "1",
    cursor: "pointer",
    fontSize: "0.878vw",
    color: `${THEME_COLOR} !important`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0146vw !important",
  },
  productsPrimary: {
    opacity: "1",
    cursor: "pointer",
    fontSize: "0.878vw",
    textTransform: "capitalize",
    // color: `${GREY_VARIANT_8} !important`,
    color: "#4B5777",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0146vw !important",
    "&:hover": {
      background: `${GREY_VARIANT_17} !important`,
    },
  },
  productsPrimaryBlack: {
    opacity: "1",
    cursor: "pointer",
    fontSize: "0.878vw",
    textTransform: "capitalize",
    color: `${GREY_VARIANT_8} !important`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0146vw !important",
    "&:hover": {
      background: `${GREY_VARIANT_17} !important`,
    },
  },
  accSettingPrimary: {
    opacity: "1",
    cursor: "pointer",
    fontSize: "0.833vw",
    color: `${GREY_VARIANT_2} !important`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0146vw !important",
  },
  list: {
    padding: "0.366vw 0.146vw",
    "&:hover": {
      background: "none !important",
    },
  },
  productsList: {
    padding: "0px 0.585vw !important",
    "& div": {
      // margin: "0px",
    },
    "&:hover": {
      background: `${GREY_VARIANT_17} !important`,
    },
    "& $primary": {
      fontFamily: "Open Sans !important",
      fontSize: "0.878vw",
      color: `${FONTGREY_COLOR} !important`,
    },
  },
  professionalsList: {
    padding: "0.366vw 0.146vw",
    borderBottom: `0.0732vw solid ${BG_LightGREY_COLOR}`,
    "&:hover": {
      background: "none !important",
    },
  },
  accSettingList: {
    padding: "0.219vw 0.732vw",
    background: `${BG_LightGREY_COLOR} !important`,
    "&:hover": {
      background: `${BG_LightGREY_COLOR} !important`,
    },
  },
  servicesList: {
    borderTopRightRadius: "0.219vw",
    borderTopLeftRadius: "0.219vw",
    borderBottom: "none !important",
    padding: "0.366vw 0.146vw",
    background: `${WHITE_COLOR} !important`,
    "&:hover": {
      background: "none !important",
    },
  },
  professionalsOpenList: {
    borderBottom: `0.0732vw solid ${BG_LightGREY_COLOR}`,
    padding: "0.366vw 0.146vw",
    background: `${WHITE_COLOR} !important`,
    "& $primary": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1",
    },
  },
  productsOpenList: {
    background: `${BG_LightGREY_COLOR} !important`,
    padding: "0px 0.585vw !important",
    "& div": {
      // margin: "0px",
    },
    "& $primary": {
      fontFamily: "Open Sans !important",
      fontSize: "0.878vw",
      color: `${GREY_VARIANT_8} !important`,

      opacity: "1",
    },
  },
  accSettingOpenList: {
    padding: "0.219vw 0.732vw",
    background: `${THEME_COLOR_Opacity} !important`,
    "& $primary": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1",
    },
  },
  servicesOpenList: {
    borderTopRightRadius: "0.219vw",
    borderTopLeftRadius: "0.219vw",
    borderBottom: "none !important",
    padding: "0.366vw 0.146vw",
    background: `${THEME_COLOR} !important`,
    color: `${WHITE_COLOR} !important`,
    "& $primary": {
      color: `${WHITE_COLOR} !important`,
      opacity: "1",
    },
    "& $servicesPrimary": {
      color: `${WHITE_COLOR} !important`,
      opacity: "1",
    },
  },
  openList: {
    padding: "0.366vw 0.146vw",
    background: `${WHITE_COLOR} !important`,
    "& $primary": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1",
    },
  },
  secondary: {
    float: "right",
    margin: "0 0.366vw",
    fontFamily: "Museo-Sans-Cyrl !important",
    letterSpacing: "0.0366vw !important",
    fontSize: "0.805vw",
  },
  Chevron: {
    fontSize: "1.317vw",
  },
  container: {
    padding: "0.732vw 0",
  },
  productsContainer: {
    padding: "0px !important",
  },
  servicesContainer: {
    padding: 0,
    borderBottomRightRadius: "0.219vw",
    borderBottomLeftRadius: "0.219vw",
  },
  accSettingContainer: {
    padding: 0,
    background: `${BG_LightGREY_COLOR} !important`,
  },
});

class NestedList extends React.Component {
  render() {
    const { classes, name, subname, open, onClick, plus, professionals, serviceslist, activeLabel, inactiveLabel, accSetting, productsPage } = this.props;

    return (
      <Wrapper>
        <List
          component="nav"
          className={
            professionals
              ? classes.professionalsRoot
              : productsPage
              ? classes.productsRoot
              : accSetting
              ? classes.accSettingRoot
              : serviceslist
              ? classes.servicesRoot
              : classes.root
          }
        >
          <ListItem
            onClick={onClick}
            button
            className={
              open
                ? professionals
                  ? classes.professionalsOpenList
                  : productsPage
                  ? classes.productsOpenList
                  : accSetting
                  ? classes.accSettingOpenList
                  : serviceslist
                  ? classes.servicesOpenList
                  : classes.openList
                : professionals
                ? classes.professionalsList
                : productsPage
                ? classes.productsList
                : accSetting
                ? classes.accSettingList
                : serviceslist
                ? classes.servicesList
                : classes.list
            }
          >
            <ListItemText
              primary={serviceslist ? (open ? activeLabel : inactiveLabel) : name}
              classes={
                professionals
                  ? { primary: classes.professionalsPrimary }
                  : accSetting
                  ? { primary: classes.accSettingPrimary }
                  : productsPage
                  ? { primary: open ? classes.productsPrimaryBlack : classes.productsPrimary }
                  : serviceslist
                  ? { primary: classes.servicesPrimary }
                  : { primary: classes.primary }
              }
            />
            <ListItemText secondary={subname} classes={{ secondary: classes.secondary }} />
            <div className={accSetting || productsPage ? "p-0" : "px-3"}>
              <span>
                {open ? (
                  professionals ? (
                    <img src={Chevron_Down_Blue} className="chevronDownBlue"></img>
                  ) : productsPage ? (
                    <img src={Chevron_Down_DarkGrey} className="chevronDarkGrey"></img>
                  ) : accSetting ? (
                    <img src={Chevron_Down_Blue} className="chevronDownBlueAccSetting"></img>
                  ) : serviceslist ? (
                    <img src={Chevron_Down_White} className="chevronDownBlue"></img>
                  ) : (
                    <img src={Chevron_Down} width="0.658vw" className="chevronDown"></img>
                  )
                ) : professionals ? (
                  <img src={Chevron_Left_Blue} className="chevronLeftBlue"></img>
                ) : productsPage ? (
                  <img src={Chevron_Left_Darkgrey} className="chevronLeftDarkGrey"></img>
                ) : accSetting ? (
                  <img src={Chevron_Left} className="chevronRightBlue"></img>
                ) : serviceslist ? (
                  <img src={Chevron_Left} className="chevronLeftBlue"></img>
                ) : (
                  <img src={Chevron_Right} width="0.439vw" className="chevronRight"></img>
                )}
              </span>
            </div>
          </ListItem>
          <Collapse
            in={open}
            timeout="auto"
            unmountOnExit
            classes={
              accSetting
                ? { container: classes.accSettingContainer }
                : productsPage
                ? { container: classes.productsContainer }
                : serviceslist
                ? { container: classes.servicesContainer }
                : { container: classes.container }
            }
          >
            {this.props.children}
          </Collapse>
        </List>
        <style jsx>
          {`
            .chevronDownBlue {
              width: 0.658vw;
            }
            .chevronLeftBlue {
              width: 0.292vw;
            }
            .chevronDownBlueAccSetting {
              width: 0.512vw;
            }
            .chevronDarkGrey {
              width: 0.658vw;
            }
            .chevronLeftDarkGrey {
              width: 0.439vw;
            }
            .chevronRightBlue {
              width: 0.292vw;
              margin-bottom: 2px;
              transform: rotate(180deg);
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

NestedList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NestedList);
