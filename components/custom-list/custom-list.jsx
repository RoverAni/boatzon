// Main React Components
import React from "react";
// Prop-Type Checker
import PropTypes from "prop-types";
// material-ui components
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import {
  GREY_VARIANT_3,
  GREY_VARIANT_1,
  WHITE_COLOR,
  Minus_Grey_Icon,
  Plus_Lightgrey_Icon,
  FONTGREY_COLOR,
  BOX_SHADOW_GREY,
  UnCheck_icon,
} from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";

const styles = (theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    border: `0.0732vw solid ${GREY_VARIANT_3}`,
    padding: "0",
    marginBottom: "0.732vw",
    // boxShadow: `0px 0px 1px 0px ${BOX_SHADOW_GREY}`
  },
  buyingFlowRoot: {
    width: "100%",
    borderBottom: `0.0732vw solid ${GREY_VARIANT_3}`,
    backgroundColor: theme.palette.background.paper,
    border: `none`,
    padding: "0 !important",
    marginBottom: "0.732vw",
  },
  nested: {
    paddingLeft: "1.171vw",
  },
  buyingFlowPrimary: {
    opacity: "1",
    color: `${FONTGREY_COLOR} !important`,
    fontFamily: `"Open Sans-Semibold" !important`,
    letterSpacing: "0.0366vw !important",
    fontSize: "0.915vw",
  },
  primary: {
    opacity: "1",
    color: `${GREY_VARIANT_1} !important`,
    fontFamily: `"Museo-Sans" !important`,
    fontWeight: "600",
    letterSpacing: "0.0366vw !important",
    fontSize: "0.915vw !important",
  },
  list: {
    padding: "0.366vw 0.146vw !important",
    "&:hover": {
      background: "none !important",
    },
  },
  openList: {
    padding: "0.366vw 0.146vw !important",
    background: `${WHITE_COLOR} !important`,
    "& $primary": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1",
    },
  },
  buyingFlowOpenList: {
    padding: "0.219vw 0 !important",
    background: `${WHITE_COLOR} !important`,
    "& $primary": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1",
    },
  },
  buyingFlowList: {
    padding: "0.219vw 0 !important",
    "&:hover": {
      background: "none !important",
    },
  },
  secondary: {
    float: "right",
    margin: "0 0.366vw",
    fontFamily: "Museo-Sans-Cyrl !important",
    letterSpacing: "0.0366vw !important",
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: "0.805vw",
    },
  },
  Chevron: {
    fontSize: "1.317vw",
  },
  container: {
    padding: "0.366vw 1.171vw 1.171vw 1.171vw",
  },
});

class CustomList extends React.Component {
  render() {
    const {
      classes,
      name,
      subname,
      open,
      onClick,
      plus,
      buyingFlow,
    } = this.props;

    return (
      <Wrapper>
        <List
          component="nav"
          className={buyingFlow ? classes.buyingFlowRoot : classes.root}
        >
          <ListItem
            onClick={onClick}
            button
            className={
              open
                ? buyingFlow
                  ? classes.buyingFlowOpenList
                  : classes.openList
                : buyingFlow
                ? classes.buyingFlowList
                : classes.list
            }
          >
            {/* {open ? (
              buyingFlow ? (
                <img src={UnCheck_icon} className="checkIcon"></img>
              ) : (
                <img src={UnCheck_icon} className="checkIcon"></img>
              )
            ) : (
              ""
            )} */}
            <ListItemText
              primary={name}
              classes={
                buyingFlow
                  ? { root: classes.nested, primary: classes.buyingFlowPrimary }
                  : { root: classes.nested, primary: classes.primary }
              }
            />
            <ListItemText
              secondary={subname}
              classes={{ secondary: classes.secondary }}
            />
            <div className="px-3">
              <span>
                {open ? (
                  <img src={Minus_Grey_Icon} width="10px"></img>
                ) : (
                  <img src={Plus_Lightgrey_Icon} width="10px"></img>
                )}
              </span>
            </div>
          </ListItem>
          <Collapse
            in={open}
            timeout="auto"
            unmountOnExit
            classes={
              buyingFlow
                ? { container: classes.container }
                : { container: classes.container }
            }
          >
            {this.props.children}
          </Collapse>
        </List>
        <style jsx>
          {`
            .checkIcon {
              width: 0.732vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

CustomList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomList);
