import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import { Border_LightGREY_COLOR, GREY_VARIANT_2, THEME_COLOR } from "../../lib/config";
import Wrapper from "../../hoc/Wrapper";

const styles = (theme) => ({
  root: {
    width: 100 + theme.spacing(3) * 2,
  },
  cashDownRoot: {
    width: "95%",
    textAlign: "center",
    margin: "0 auto",
  },
  rootIOSslider: {
    color: "#3880ff",
    height: "0.146vw",
    padding: "1.098vw 0",
  },
  thumb: {
    height: "1.317vw",
    width: "1.317vw",
    backgroundColor: `${Border_LightGREY_COLOR}`,
    boxShadow: iOSBoxShadow,
    marginTop: "-0.658vw",
    marginLeft: "-0.658vw",
    boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.25)",
    "&:focus, &:hover, &$active": {
      boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.25)",
    },
  },
  cashDownthumb: {
    height: "0.732vw",
    width: "0.732vw",
    backgroundColor: `${THEME_COLOR}`,
    boxShadow: iOSBoxShadow,
    marginTop: "-0.366vw",
    marginLeft: "-0.366vw",
    boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.25)",
    "&:focus, &:hover, &$active": {
      boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.25)",
    },
  },
  cashDownvalueLabel: {
    display: "none",
  },
  valueLabel: {
    // left: "calc(-50% + 0.439vw)",
    left: "0.7vw",
    top: "-1.464vw",
    "& span": {
      display: "flex",
      alignItems: "center",
      background: "transparent",
      color: "#717C99",
      fontFamily: `"Museo-Sans" !important`,
      textTransform: "capitalize",
      fontSize: "0.805vw",
      transform: "rotate(0deg)",
      "& span": {
        display: "flex",
        alignItems: "center",
        "&:after": {
          content: `"Miles"`,
          display: "block",
          fontSize: "0.805vw",
          padding: "0 0.366vw",
        },
      },
    },
  },
  track: {
    height: "0.146vw",
  },
  rail: {
    height: "0.146vw",
    opacity: 0.5,
    backgroundColor: `${Border_LightGREY_COLOR}`,
  },
  mark: {
    backgroundColor: `${Border_LightGREY_COLOR}`,
    height: "0.366vw",
    marginTop: "-0.292vw",
    width: "0.0732vw",
  },
  markActive: {
    opacity: 1,
    backgroundColor: "currentColor",
  },
  markLabel: {
    fontSize: "0.658vw",
    fontFamily: `"Open Sans" !important`,
    color: `${GREY_VARIANT_2}`,
    top: "1.683vw",
  },
});

const iOSBoxShadow = "0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)";

const marks = [
  {
    value: 0,
  },
  {
    value: 200,
  },
  {
    value: 570,
  },
  {
    value: 1000,
  },
];

class CustomizedSlider extends Component {
  render() {
    const { classes, onChange, value, cashDown, cashMarks, monthlyPayment, name } = this.props;
    return (
      <Wrapper>
        <div className={cashDown || monthlyPayment ? classes.cashDownRoot : classes.root}>
          <Slider
            classes={
              cashDown || monthlyPayment
                ? {
                    root: classes.rootIOSslider,
                    thumb: classes.cashDownthumb,
                    valueLabel: classes.cashDownvalueLabel,
                    track: classes.track,
                    rail: classes.rail,
                    mark: classes.mark,
                    markActive: classes.markActive,
                    markLabel: classes.markLabel,
                  }
                : {
                    root: classes.rootIOSslider,
                    thumb: classes.thumb,
                    valueLabel: classes.valueLabel,
                    track: classes.track,
                    rail: classes.rail,
                    mark: classes.mark,
                    markActive: classes.markActive,
                  }
            }
            aria-label="ios slider"
            defaultValue={cashDown || monthlyPayment ? 0 : 0}
            marks={cashDown || monthlyPayment ? cashMarks : marks}
            value={value}
            min={cashDown ? 0 : monthlyPayment ? 0 : 0}
            max={cashDown ? 15000 : monthlyPayment ? 10000 : 1000}
            onChange={onChange}
            valueLabelDisplay="on"
            name={name}
          />
        </div>
        <style jsx>
          {`
            .MuiSlider-marklabel::nth-last-of-type(1) {
              left: 95% !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}
export default withStyles(styles)(CustomizedSlider);
