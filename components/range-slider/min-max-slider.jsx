import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import { THEME_COLOR, Border_LightGREY_COLOR } from "../../lib/config";

const styles = (theme) => ({
  root: {
    width: "100%",
  },
});

const iOSBoxShadow =
  "0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)";

const IOSSlider = withStyles({
  root: {
    color: `${THEME_COLOR}`,
    height: 2,
    padding: "1.098vw 0",
  },
  thumb: {
    height: "0.732vw",
    width: "0.732vw",
    backgroundColor: `${THEME_COLOR}`,
    boxShadow: iOSBoxShadow,
    marginTop: "-0.366vw",
    marginLeft: "-0.366vw",
    boxShadow: "none !important",
    "&:focus, &:hover, &$active": {
      boxShadow: "none !important",
    },
  },
  track: {
    height: "0.146vw",
  },
  rail: {
    height: "0.146vw",
    opacity: 0.5,
    backgroundColor: `${Border_LightGREY_COLOR}`,
  },
  mark: {
    backgroundColor: `${Border_LightGREY_COLOR}`,
    height: "0.366vw",
    width: "0.0732vw",
    marginTop: "-0.292vw",
  },
  markActive: {
    opacity: 1,
    backgroundColor: "currentColor",
  },
})(Slider);

class MinMaxRangeSlider extends Component {
  render() {
    const { classes, value, handleChange, max, min, marks } = this.props;
    return (
      <div className={classes.root}>
        <IOSSlider
          value={value}
          onChange={handleChange}
          valueLabelDisplay="off"
          marks={marks}
          max={max}
          min={min}
          aria-label="ios slider"
        />
      </div>
    );
  }
}

export default withStyles(styles)(MinMaxRangeSlider);
