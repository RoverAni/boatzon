import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { Slider, withStyles } from "@material-ui/core";
import {
  THEME_COLOR,
  BG_LightGREY_COLOR,
  Border_LightGREY_COLOR,
  Border_LightGREY_COLOR_1,
} from "../../lib/config";

const iOSBoxShadow =
  "0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)";

const styles = (theme) => ({
  root: {
    padding: "0.585vw 0",
  },
  colorPrimary: {
    color: `${THEME_COLOR}`,
  },
  thumb: {
    position: "relative !important",
    backgroundColor: `${Border_LightGREY_COLOR}`,
    boxShadow: iOSBoxShadow,
    boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.25)",
    "&:focus, &:hover, &$active": {
      boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.25)",
    },
  },
  valueLabel: {
    right: "-3.660vw",
    top: "50%",
    right: "-5.856vw",
    transform: `translate(0, -50%) !important`,
    zIndex: "0 !important",
    borderRadius: "0 !important",
    "& > $span": {
      borderRadius: "0 !important",
      transform: `rotate(0deg) !important`,
      width: "100% !important",
      height: "1.464vw !important",
      padding: "0.219vw 0.732vw !important",
      "&:after": {
        content: `"" !important`,
        display: "ruby-base !important",
        position: "absolute !important",
        width: "0 !important",
        height: "0 !important",
        zIndex: "0 !important",
        borderWidth: "0.292vw 0.878vw 0.292vw 0 !important",
        borderStyle: "solid !important",
        borderColor: `transparent ${THEME_COLOR} transparent transparent !important`,
        top: "50% !important",
        transform: "translate(0px, -50%) !important",
        left: "-0.658vw !important",
      },
    },
    "& > $span > $span": {
      borderRadius: "0 !important",
      transform: `rotate(0deg) !important`,
      width: "100% !important",
      height: "auto !important",
      padding: "0 !important",
      fontSize: "0.732vw !important",
      fontFamily: "Museo-Sans !important",
      fontWeight: "lighter !important",
      position: "relative !important",
      color: `${BG_LightGREY_COLOR}`,
      "&:after": {
        content: `" miles" !important`,
        fontSize: "0.805vw !important",
        display: "ruby-base !important",
        left: "0.732vw !important",
      },
    },
  },
  rail: {
    height: 2,
    opacity: 0.5,
    backgroundColor: `${Border_LightGREY_COLOR_1}`,
  },
});

class RangeSlider extends Component {
  render() {
    const { classes, value, onChange } = this.props;

    return (
      <Wrapper>
        <Slider
          classes={{
            root: classes.root,
            colorPrimary: classes.colorPrimary,
            thumb: classes.thumb,
            rail: classes.rail,
            valueLabel: classes.valueLabel,
          }}
          defaultValue={value}
          min={0}
          max={1000}
          onChange={onChange}
          aria-labelledby="discrete-slider-always"
          valueLabelDisplay="on"
          {...this.props}
        />
      </Wrapper>
    );
  }
}

export default withStyles(styles)(RangeSlider);
