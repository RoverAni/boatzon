// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// react select npm package
import Select, { components } from "react-select";

// asstes and colors
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR,
  GREY_VARIANT_1,
  SelectInput_Chevron_Down_Grey,
  SelectInput_Chevron_Down_Blue,
  Border_LightGREY_COLOR_2,
  GREY_VARIANT_2,
  GREY_VARIANT_3,
  Business_Profile_Icon,
  Professional_Type_Icon,
  GREEN_COLOR,
  Chevron_Down_White,
  THEME_COLOR_Opacity,
  Close_Light_Grey_Icon,
} from "../../lib/config";
import { AsyncPaginate } from "react-select-async-paginate";

const myAppointmentPagecustomStyles = {
  option: (provided, state) => ({
    ...provided,
    // border: `0.0732vw solid ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.805vw",
    fontFamily: "Museo-Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "3.66vw",
    minHeight: "2.342vw",
    height: "auto",
    margin: "0.366vw 0",
    border: `0.0732vw solid ${GREY_VARIANT_3}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${GREY_VARIANT_3}`,
    },
    "&:focus, &:active": {
      borderColor: `${GREY_VARIANT_3}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Museo-Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Museo-Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${FONTGREY_COLOR}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Museo-Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "3 !important",
    marginTop: "3px",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "7.32vw",
    minHeight: "2.342vw",
    height: "auto",
    margin: "0.366vw 0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans-SemiBold !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans-SemiBold !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans-SemiBold !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const editProProfilecustomStyles = {
  option: (provided, state) => ({
    ...provided,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.878vw",
    fontSize: "0.878vw",
    fontFamily: "Open Sans !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0 0.878vw",
    flexWrap: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "fit-content",
    minHeight: "2.489vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${GREY_VARIANT_3}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${GREY_VARIANT_3}`,
    },
    "&:focus, &:active": {
      borderColor: `${GREY_VARIANT_3}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.878vw",
    width: "0",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.878vw",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.878vw",
    color: `${FONTGREY_COLOR}`,
    textTransform: "capitalize !important",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const servicescustomStyles = {
  option: (provided, state) => ({
    ...provided,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "fit-content",
    minHeight: "2.196vw",
    height: "auto",
    margin: "0.366vw 0 0 0",
    border: `0.0732vw solid ${GREY_VARIANT_3}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${GREY_VARIANT_3}`,
    },
    "&:focus, &:active": {
      borderColor: `${GREY_VARIANT_3}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${FONTGREY_COLOR}`,
    textTransform: "capitalize !important",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  valueContainer: (provided) => ({
    ...provided,
    flexWrap: "nowrap",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const currencycustomStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${GREY_VARIANT_1}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.732vw",
    fontFamily: "Museo-Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "fit-content !important",
    minHeight: "2.196vw",
    height: "auto",
    margin: "0 0 0 0.219vw !important",
    border: `none`,
    flexWrap: `nowrap !important`,
    boxShadow: "none !important",
    background: "none !important",
    "& div": {
      padding: "0 !important",
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    width: "0 !important",
    "& div > input": {
      width: "0 !important",
      color: `${FONTGREY_COLOR} !important`,
      fontFamily: "Museo-Sans !important",
      letterSpacing: " 0.021vw !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Museo-Sans !important",
    textTransform: "capitalize",
    position: "relative",
    top: "0",
    transform: "unset",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    width: "fit-content !important",
    padding: "0 0.512vw 0 0 !important",
    color: `${FONTGREY_COLOR}`,
    textTransform: "capitalize !important",
    fontFamily: "Museo-Sans !important",
    position: "relative",
    top: "0",
    transform: "unset",
  }),
  valueContainer: (provided) => ({
    ...provided,
    display: "flex",
    flexWrap: "nowrap",
    alignItems: "center",
    padding: "0 0.512vw 0 0 !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    right: "0 !important",
    top: "1.976vw",
    left: "-0.366vw",
    right: "unset !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const stateTypeStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    border: "none",
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.292vw 0.585vw 0.146vw 0.585vw",
    fontSize: "0.878vw",
    fontFamily: "Open Sans !important",
    margin: "0 !important",
    textAlign: "left",
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "100px",
    minHeight: "2.489vw",
    height: "auto",
    display: "flex",
    margin: "0",
    border: "none",
    borderRadius: "0",
    borderBottom: `0.0732vw solid ${Border_LightGREY_COLOR_2}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR_2}`,
    },
    "&:focus, &:active": {
      borderColor: `${THEME_COLOR}`,
    },
  }),
  valueContainer: (provided) => ({
    ...provided,
    padding: "0",
    flexWrap: "nowrap",
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.878vw",
    width: "0",
    "& div input": {
      // color: `${GREY_VARIANT_2}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    display: "inline-block !important",
    fontSize: "0.878vw",
    color: `${GREY_VARIANT_3} !important`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
    padding: "0 0 0 0.292vw",
  }),
  singleValue: (provided) => ({
    ...provided,
    display: "inline-block !important",
    fontSize: "0.878vw",
    color: `${FONTGREY_COLOR}`,
    padding: "0 0 0 0.292vw",
    textTransform: "capitalize !important",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),

  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const professionalTypeStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    border: "none",
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.292vw 0.585vw 0.146vw 0.585vw",
    fontSize: "0.878vw",
    fontFamily: "Open Sans !important",
    margin: "0 !important",
    textAlign: "left",
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "7.32vw",
    minHeight: "2.489vw",
    height: "auto",
    display: "flex",
    margin: "0.366vw 0",
    border: "none",
    borderRadius: "0",
    borderBottom: `0.0732vw solid ${Border_LightGREY_COLOR_2}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR_2}`,
    },
    "&:focus, &:active": {
      borderColor: `${THEME_COLOR}`,
    },
  }),
  valueContainer: (provided) => ({
    ...provided,
    flexWrap: "nowrap",
    "&:before": {
      content: `url(${Professional_Type_Icon})`,
      border: "none !important",
      borderRight: "none !important",
      borderLeft: "none !important",
      margin: "0 0.439vw 0 0 !important",
      position: "relative",
      top: "0.146vw",
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.878vw",
    width: "0",
    "& div input": {
      // color: `${GREY_VARIANT_2}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    display: "inline-block !important",
    fontSize: "0.878vw",
    color: `${GREY_VARIANT_3} !important`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
    padding: "0 0 0 0.292vw",
  }),
  singleValue: (provided) => ({
    ...provided,
    display: "inline-block !important",
    fontSize: "0.878vw",
    color: `${FONTGREY_COLOR}`,
    padding: "0 0 0 0.292vw",
    textTransform: "capitalize !important",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const MinStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused ? `${THEME_COLOR}` : `${THEME_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.366vw 0.732vw",
    fontSize: "0.768vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    cursor: "pointer",
    textAlign: "left",
    "&:last-child": {
      borderBottom: "none !important",
    },
    "&:first-child": {
      color: `${THEME_COLOR} !important`,
      opacity: state.isFocused ? "0.6" : "1",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minHeight: "auto",
    height: "auto",
    margin: "0 0 0 0.219vw !important",
    border: `none`,
    boxShadow: "none !important",
    background: "none !important",
    "& div": {
      padding: "0 !important",
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    width: "0 !important",
    "& div input": {
      width: "0 !important",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.768vw",
    color: `${THEME_COLOR}`,
    fontFamily: "Open Sans-SemiBold !important",
    textTransform: "capitalize",
    position: "relative",
    top: "0",
    transform: "unset",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.768vw",
    width: "100%",
    padding: "0 0.366vw !important",
    color: `${THEME_COLOR}`,
    textTransform: "capitalize !important",
    fontFamily: "Open Sans-SemiBold !important",
    position: "relative",
    top: "0",
    transform: "unset",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "7.320vw !important",
    maxWidth: "fit-content !important",
    right: "0 !important",
    top: "1.244vw",
    margin: "0 !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const buttoncustomStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: `0.073vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: 10,
    fontSize: "0.805vw !important",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    "&:last-child": {
      opacity: 1,
      padding: "0 !important",
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: " 7.320vw",
    height: "auto",
    margin: "0.366vw 0",
    border: `0.073vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw !important",
    width: "0 !important",
    "& div input": {
      width: "0 !important",
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Open Sans-SemiBold !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw !important",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Open Sans-SemiBold !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    width: "100%",
    textTransform: "capitalize !important",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans-SemiBold !important",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    margin: "0",
    padding: "0 !important",
    minWidth: "8.052vw !important",
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const discussionStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    color: `${GREY_VARIANT_1}`,
    width: "100%",
    opacity: "1 !important",
    minWidth: "7.320vw",
    minHeight: "2.342vw !important",
    margin: "0 !important",
    border: `none`,
    borderRadius: "0.146vw !important",
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    width: "0",
    fontSize: "0.805vw",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      opacity: "1",
      fontFamily: "Open Sans-SemiBold !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    opacity: "1",
    fontFamily: "Open Sans-SemiBold !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    width: "100% !important",
    textTransform: "capitalize !important",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans-SemiBold !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "max-content !important",
    width: "max-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    minWidth: "8.052vw !important",
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const productsPagecustomStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.729166vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: state.isSelected
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),

  container: (provided, state) => ({
    ...provided,
    width: "100%",
  }),

  multiValueRemove: (provided, state) => ({
    ...provided,
    display: "none",
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.278vw 0.585vw",
    flexWrap: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    maxWidth: "100%",
    minWidth: "7.32vw",
    minHeight: "2.187vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  multiValue: (provided, state) => ({
    ...provided,
    backgroundColor: "transparent",
    margin: "0 !important",
    fontSize: "0.729166vw",
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.729166vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.729166vw",
    color: `${GREY_VARIANT_3}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.729166vw",
    // color: `${GREY_VARIANT_1}`,
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
    zIndex: "2 !important",
  }),
};

const financeBoatcustomStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.729166vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.278vw 0 0.278vw 0.585vw",
    flexWrap: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "7.32vw",
    minHeight: "2.187vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.729166vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.729166vw",
    color: `${GREY_VARIANT_3}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.729166vw",
    color: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};
const homePageStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.729vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.439vw 0.585vw",
    flexWrap: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "98%",
    minHeight: "2.500vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    borderRadius: "0.146vw",
    boxShadow: "none !important",
    padding: "0 0.585vw 0 0.366vw",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.729vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.729vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.729vw",
    // color: `${GREY_VARIANT_1}`,
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const addDiscussionStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: "1 !important",
    backgroundColor: state.isSelected
      ? `${THEME_COLOR_Opacity}`
      : `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.729vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.439vw 0.585vw",
    flexWrap: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "98%",
    minHeight: "2.500vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    borderRadius: "0.146vw",
    boxShadow: "none !important",
    padding: "0 0.585vw 0 1.366vw",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.729vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.729vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.729vw",
    // color: `${GREY_VARIANT_1}`,
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    display: "none !important",
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
    width: "98%",
    marginTop: "0 !important",
    boxShadow: "none !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "14.063vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #fff",
  }),
};

const boatsPagecustomStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.729166vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: state.isSelected
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  multiValueRemove: (provided, state) => ({
    ...provided,
    display: "none",
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.278vw 0.585vw",
    flexWrap: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    maxWidth: "100%",
    minWidth: "7.32vw",
    minHeight: "2.187vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  multiValue: (provided, state) => ({
    ...provided,
    backgroundColor: "transparent",
    margin: "0 !important",
    fontSize: "0.729166vw",
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.729166vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.729166vw",
    color: `${GREY_VARIANT_3}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.729166vw",
    // color: `${GREY_VARIANT_1}`,
    color: `${GREY_VARIANT_1}`,
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
    zIndex: "2 !important",
  }),
};

const sellingPagecustomStyles = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.278vw 0.585vw",
    flexWrap: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "7.32vw",
    minHeight: "1.830vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    // color: `${GREY_VARIANT_1}`,
    color: "#FFF",
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const markSoldCustomStyle = {
  option: (provided, state) => ({
    ...provided,
    // borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    textTransform: "capitalize !important",
    padding: "0.219vw 0.732vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.036vw !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
    "&:last-child": {
      borderBottom: "none !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0.278vw 0.585vw",
    flexWrap: "nowrap",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "7.32vw",
    minHeight: "1.830vw",
    height: "auto",
    margin: "0",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    boxShadow: "none !important",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
    "&:focus, &:active": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    // color: `${GREY_VARIANT_1}`,
    color: "#000",
    textTransform: "capitalize !important",
    // width: "100%",
    fontFamily: "Open Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    minWidth: "fit-content !important",
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

class SelectInput extends Component {
  render() {
    const DropdownIndicator = (props) => {
      return (
        <components.DropdownIndicator {...props}>
          {this.props.noboxstyle ? (
            <img
              src={SelectInput_Chevron_Down_Blue}
              className="blueChevronIcon"
            ></img>
          ) : this.props.financeBoat &&
            props.getValue() &&
            props.getValue().length > 0 ? (
            <img src={Chevron_Down_White} className="blueChevronIcon"></img>
          ) : (
            <img
              src={SelectInput_Chevron_Down_Grey}
              className="greyChevronIcon"
            ></img>
          )}
        </components.DropdownIndicator>
      );
    };

    const ValueContainer = ({ children, ...props }) => {
      const { getValue, hasValue } = props;
      const nbValues = getValue().length;
      if (!hasValue || nbValues < 2) {
        return (
          <components.ValueContainer {...props}>
            {children}
          </components.ValueContainer>
        );
      } else {
        return (
          <components.ValueContainer {...props}>
            <span className="multi-value">{`${nbValues} selected`}</span>
          </components.ValueContainer>
        );
      }
    };

    const ClearIndicator = (props) => {
      return (
        <components.ClearIndicator {...props}>
          <img src={Close_Light_Grey_Icon} className="greyChevronIcon"></img>
        </components.ClearIndicator>
      );
    };

    // console.log("color selected", this.props.setColor);
    const {
      placeholder,
      options,
      onChange,
      noboxstyle,
      buttonOption,
      discussionselectinput,
      professionalType,
      stateType,
      servicesSelect,
      editProProfile,
      currency,
      sellingPage,
      markSold,
      myAppointmentPage,
      boatsPage,
      financeBoat,
      homePage,
      addDiscussion,
      asyncSelect,
      isMulti,
      productsPage,
    } = this.props;
    return (
      <Wrapper>
        {asyncSelect ? (
          <AsyncPaginate
            placeholder={placeholder}
            classNamePrefix="react-select"
            styles={
              noboxstyle
                ? MinStyles
                : buttonOption
                ? buttoncustomStyles
                : discussionselectinput
                ? discussionStyles
                : professionalType
                ? professionalTypeStyles
                : stateType
                ? stateTypeStyles
                : servicesSelect
                ? servicescustomStyles
                : editProProfile
                ? editProProfilecustomStyles
                : currency
                ? currencycustomStyles
                : sellingPage
                ? sellingPagecustomStyles
                : myAppointmentPage
                ? myAppointmentPagecustomStyles
                : markSold
                ? markSoldCustomStyle
                : boatsPage
                ? boatsPagecustomStyles
                : productsPage
                ? productsPagecustomStyles
                : financeBoat
                ? financeBoatcustomStyles
                : homePage
                ? homePageStyles
                : addDiscussion
                ? addDiscussionStyles
                : customStyles
            }
            components={
              isMulti
                ? { DropdownIndicator, ValueContainer, ClearIndicator }
                : { DropdownIndicator }
            }
            onChange={onChange}
            // menuIsOpen
            {...this.props}
          ></AsyncPaginate>
        ) : (
          <Select
            placeholder={placeholder}
            styles={
              noboxstyle
                ? MinStyles
                : buttonOption
                ? buttoncustomStyles
                : discussionselectinput
                ? discussionStyles
                : professionalType
                ? professionalTypeStyles
                : stateType
                ? stateTypeStyles
                : servicesSelect
                ? servicescustomStyles
                : editProProfile
                ? editProProfilecustomStyles
                : currency
                ? currencycustomStyles
                : sellingPage
                ? sellingPagecustomStyles
                : myAppointmentPage
                ? myAppointmentPagecustomStyles
                : markSold
                ? markSoldCustomStyle
                : boatsPage
                ? boatsPagecustomStyles
                : productsPage
                ? productsPagecustomStyles
                : financeBoat
                ? financeBoatcustomStyles
                : homePage
                ? homePageStyles
                : addDiscussion
                ? addDiscussionStyles
                : customStyles
            }
            classNamePrefix="react-select"
            components={{ DropdownIndicator }}
            options={options}
            onChange={onChange}
            // menuIsOpen
            {...this.props}
          />
        )}
        <style jsx>
          {`
            :global(.greyChevronIcon) {
              width: 0.658vw;
              margin-top: 0.219vw;
            }
            :global(.blueChevronIcon) {
              width: 0.585vw;
              margin-left: 0.366vw;
              margin-top: 0.219vw;
            }
            :global(.multi-value) {
              background-color: transparent;
              margin: 0 !important;
              font-size: 0.805vw;
              color: ${GREY_VARIANT_1};
              text-transform: capitalize !important;
              // width: 100%;
              font-family: Open Sans !important;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default SelectInput;
