// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// react select npm package
import Select, { components } from "react-select";

// asstes and colors
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR_1,
  Border_LightGREY_COLOR,
  GREY_VARIANT_2,
  GREY_VARIANT_1,
} from "../../lib/config";
import { withStyles } from "@material-ui/core";

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: `0.0732vw dotted ${Border_LightGREY_COLOR_1}`,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    padding: "0.366vw 0.585vw !important",
    fontSize: "0.878vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0366vw !important",
    margin: "0 !important",
    textAlign: "left",
  }),
  control: (provided, state) => ({
    ...provided,
    cursor: "pointer",
    width: "100%",
    minWidth: "8.784vw",
    minHeight: "2.196vw",
    height: "auto",
    color: `${GREY_VARIANT_1}`,
    margin: "0",
    border: `none !important`,
    borderRight: "none !important",
    boxShadow: "none !important",
    borderLeft: "none",
    "&:hover": {
      borderColor: `${Border_LightGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.878vw",
    "& div input": {
      color: `${GREY_VARIANT_1}`,
      fontFamily: "Open Sans-SemiBold !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.878vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans-SemiBold !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.878vw",
    width: "100%",
    fontFamily: "Open Sans-SemiBold !important",
  }),
  multiValue: (provided) => ({
    ...provided,
    fontSize: "0.878vw",
    fontFamily: "Open Sans-SemiBold !important",
    display: "flex",
    backgroundColor: `${THEME_COLOR}`,
    color: `${WHITE_COLOR}`,
    margin: "0 0.366vw",
    "& div": {
      color: `${WHITE_COLOR}`,
      fontFamily: "Open Sans-SemiBold !important",
      fontSize: "0.878vw",
      fontWeight: "100",
    },
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    margin: "0 !important",
    zIndex: "2 !important",
  }),
};
const styles = (theme) => ({
  label: {
    fontSize: "11px",
    margin: "0",
    padding: "0 0.366vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0366vw !important",
    color: `${GREY_VARIANT_2}`,
  },
  inputBox: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
    borderRadius: "0.292vw",
    minHeight: "2.489vw !important",
  },
});

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <div></div>
    </components.DropdownIndicator>
  );
};

class SelectLeftLabel extends Component {
  render() {
    const { classes, placeholder, options, onChange, label } = this.props;
    return (
      <Wrapper>
        <div className={classes.inputBox}>
          <label className={classes.label}>{label}</label>
          <Select
            placeholder={placeholder}
            components={{ DropdownIndicator }}
            styles={customStyles}
            classNamePrefix="react-select"
            options={options}
            onChange={onChange}
            {...this.props}
          />
        </div>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(SelectLeftLabel);
