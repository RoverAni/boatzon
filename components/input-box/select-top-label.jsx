// Main React Components
import React, { Component } from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// react select npm package
import Select, { components } from "react-select";
import { AsyncPaginate } from "react-select-async-paginate";

// asstes and colors
import {
  THEME_COLOR,
  FONTGREY_COLOR,
  WHITE_COLOR,
  Border_LightGREY_COLOR,
  GREY_VARIANT_2,
  SelectInput_Chevron_Down_Grey,
  Border_LightGREY_COLOR_2,
  BG_LightGREY_COLOR,
  Dropdown_1,
  Orange_Color,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
} from "../../lib/config";
import { withStyles } from "@material-ui/core";

const customStyles1 = {
  option: (provided, state) => ({
    ...provided,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${GREY_VARIANT_1}`,
    opacity: "1",
    backgroundColor: `${WHITE_COLOR}`,
    padding: "0.219vw 0.732vw !important",
    fontSize: "0.805vw",
    fontFamily: "Open Sans !important",
    margin: "0 !important",
    textAlign: "left",
    cursor: "pointer",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    width: "100%",
    cursor: "pointer",
    minWidth: "100%",
    minHeight: "2.0497vw !important",
    margin: "0",
    border: "none",
    borderLeft: "none",
    boxShadow: "none !important",
    padding: "0",
    "&:hover": {
      outline: "none",
      border: "none !important",
      boxShadow: "none !important",
    },
    "&:focus, &:active": {
      outline: "none",
      border: "none !important",
      boxShadow: "none !important",
    },
    "& div": {
      padding: "0 !important",
      fontFamily: "Museo-Sans !important",
      color: `${FONTGREY_COLOR}`,
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Museo-Sans !important",
      letterSpacing: "0.021vw !important",
      textTransform: "capitalize",
    },
  }),
  multiValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    fontFamily: "Open Sans !important",
    display: "flex",
    color: `${BG_LightGREY_COLOR}`,
    margin: "0.219vw 0.366vw 0.219vw 0 !important",
    "&:first-child": {
      backgroundColor: `${Dropdown_1}`,
    },
    "&:nth-child(2)": {
      backgroundColor: `${Orange_Color}`,
    },
    "& div:first-child": {
      paddingRight: "0 !important",
    },
    "& div": {
      color: `${BG_LightGREY_COLOR}`,
      fontFamily: "Open Sans !important",
      fontSize: "0.805vw",
      fontWeight: "100",
      padding: "0.219vw 0.366vw !important",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_3} !important`,
    fontFamily: "Museo-Sans !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    // width: "100%",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Museo-Sans !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "10.248vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const customStylesGetPreApproved = {
  option: (provided, state) => ({
    ...provided,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    padding: "0.292vw 0.585vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans !important",
    letterSpacing: "0.029vw !important",
    margin: "0 !important",
    cursor: "pointer",
    textAlign: "left",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0",
  }),
  control: (provided, state) => ({
    ...provided,
    width: "100%",
    cursor: "pointer",
    minWidth: "8.784vw",
    minHeight: "2.342vw !important",
    margin: "0",
    border: "none",
    borderLeft: "none",
    boxShadow: "none !important",
    padding: "0 0.585vw",
    "&:hover": {
      outline: "none",
      border: "none !important",
      boxShadow: "none !important",
    },
    "&:focus, &:active": {
      outline: "none",
      border: "none !important",
      boxShadow: "none !important",
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Open Sans !important",
      letterSpacing: "0.029vw !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_2}`,
    fontFamily: "Open Sans !important",
    letterSpacing: "0.029vw !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    width: "100%",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Open Sans !important",
    letterSpacing: "0.029vw !important",
    textAlign: "left",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "11.713vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    padding: "0.292vw 0.585vw",
    fontSize: "0.805vw",
    fontFamily: "Museo-Sans !important",
    letterSpacing: "0.029vw !important",
    margin: "0 !important",
    cursor: "pointer",
    textAlign: "left",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    width: "100%",
    cursor: "pointer",
    minWidth: "8.784vw",
    minHeight: "2.342vw !important",
    margin: "0",
    border: "none",
    borderLeft: "none",
    boxShadow: "none !important",
    padding: "0 0.585vw",
    "&:hover": {
      outline: "none",
      border: "none !important",
      boxShadow: "none !important",
    },
    "&:focus, &:active": {
      outline: "none",
      border: "none !important",
      boxShadow: "none !important",
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Museo-Sans !important",
      letterSpacing: "0.029vw !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Museo-Sans !important",
    letterSpacing: "0.029vw !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    width: "100%",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Museo-Sans !important",
    letterSpacing: "0.029vw !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "11.713vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const customStylesEditPost = {
  option: (provided, state) => ({
    ...provided,
    color:
      state.isSelected || state.isFocused
        ? `${THEME_COLOR}`
        : `${FONTGREY_COLOR}`,
    opacity: state.isFocused ? "0.6" : "1",
    backgroundColor: `${WHITE_COLOR}`,
    padding: "0.292vw 0.585vw",
    fontSize: "0.805vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.029vw !important",
    margin: "0 !important",
    cursor: "pointer",
    textAlign: "left",
    "&:first-child": {
      color: `${FONTGREY_COLOR} !important`,
      opacity: "1 !important",
    },
  }),
  control: (provided, state) => ({
    ...provided,
    width: "100%",
    cursor: "pointer",
    minWidth: "8.784vw",
    minHeight: "2.342vw !important",
    margin: "0.366vw 0 !important",
    border: `0.073vw solid ${Border_LightGREY_COLOR}`,
    borderRadius: "0.146vw",
    boxShadow: "none !important",
    padding: "0",
    "&:hover": {
      outline: "none",
      border: `0.073vw solid ${Border_LightGREY_COLOR}`,
      boxShadow: "none !important",
    },
    "&:focus, &:active": {
      outline: "none",
      border: `0.073vw solid ${Border_LightGREY_COLOR}`,
      boxShadow: "none !important",
    },
  }),
  input: (provided) => ({
    ...provided,
    display: "inline-flex",
    fontSize: "0.805vw",
    width: "0",
    "& div input": {
      color: `${FONTGREY_COLOR}`,
      fontFamily: "Open Sans-SemiBold !important",
      letterSpacing: "0.029vw !important",
      textTransform: "capitalize",
    },
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.029vw !important",
    textTransform: "capitalize",
  }),
  singleValue: (provided) => ({
    ...provided,
    fontSize: "0.805vw",
    width: "100%",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.029vw !important",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0.366vw !important",
  }),
  menu: (provided) => ({
    ...provided,
    zIndex: "2 !important",
  }),
  menuList: (provided) => ({
    ...provided,
    maxHeight: "11.713vw !important",
    scrollbarWidth: `thin !important`,
    scrollbarColor: "#c4c4c4 #f5f5f5",
  }),
};

const styles = (theme) => ({
  label: {
    fontSize: "0.732vw",
    fontFamily: "Museo-Sans !important",
    letterSpacing: "0.021vw !important",
    margin: "0",
    padding: "0 1.171vw",
    color: `${GREY_VARIANT_2}`,
    [theme.breakpoints.up("sm")]: {
      margin: "0",
    },
    [theme.breakpoints.up("md")]: {
      margin: "0",
    },
    [theme.breakpoints.up(1200)]: {
      margin: "0.292vw 0 0 0",
    },
    [theme.breakpoints.up(1400)]: {
      margin: "0.439vw 0 0 0",
    },
    [theme.breakpoints.up(1900)]: {
      margin: "0.585vw 0 0 0",
    },
  },
  label1: {
    fontSize: "0.732vw",
    fontFamily: "Museo-Sans !important",
    fontWeight: "lighter !important",
    margin: "0",
    padding: "0",
    color: `${GREY_VARIANT_2}`,
  },
  inputBox: {
    height: "auto",
    border: `0.073vw solid ${Border_LightGREY_COLOR}`,
    borderRadius: "0.292vw",
    padding: "0",
    cursor: "pointer",
  },
  inputBox1: {
    height: "auto",
    border: "none",
    borderBottom: `0.073vw solid ${Border_LightGREY_COLOR_2}`,
    borderRadius: "0",
    cursor: "pointer",
  },
  inputBoxGetPreApproved: {
    height: "auto",
    border: "none",
    borderBottom: `0.073vw solid ${Border_LightGREY_COLOR_2}`,
    borderRadius: "0",
    cursor: "pointer",
  },
  inputBoxEditPost: {
    height: "auto",
    border: `none`,
    borderRadius: "0.292vw",
    padding: "0.366vw 0",
    cursor: "pointer",
  },
  labelEditPost: {
    fontSize: "0.805vw",
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.021vw !important",
    margin: "0",
    padding: "0",
    color: `${FONTGREY_COLOR}`,
  },
});

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <img
        src={SelectInput_Chevron_Down_Grey}
        className="greyChevronIcon"
      ></img>
    </components.DropdownIndicator>
  );
};

class SelectTopLabel extends Component {
  render() {
    const {
      classes,
      placeholder,
      options,
      onChange,
      label,
      bottomborder,
      editPost,
      getPreApprovedBorder,
      asyncSelect,
      mandatory,
    } = this.props;
    return (
      <Wrapper>
        <div
          className={
            bottomborder
              ? classes.inputBox1
              : getPreApprovedBorder
              ? classes.inputBoxGetPreApproved
              : editPost
              ? classes.inputBoxEditPost
              : classes.inputBox
          }
        >
          {label == "" ? (
            ""
          ) : (
            <label
              className={
                bottomborder
                  ? classes.label1
                  : editPost
                  ? classes.labelEditPost
                  : classes.label
              }
            >
              {label} {mandatory ? <span className="mandatory">*</span> : ""}
            </label>
          )}
          {asyncSelect ? (
            <AsyncPaginate
              placeholder={placeholder}
              classNamePrefix="react-select"
              styles={
                bottomborder
                  ? customStyles1
                  : getPreApprovedBorder
                  ? customStylesGetPreApproved
                  : editPost
                  ? customStylesEditPost
                  : customStyles
              }
              components={{ DropdownIndicator }}
              onChange={onChange}
              {...this.props}
            ></AsyncPaginate>
          ) : (
            <Select
              placeholder={placeholder}
              classNamePrefix="react-select"
              styles={
                bottomborder
                  ? customStyles1
                  : getPreApprovedBorder
                  ? customStylesGetPreApproved
                  : editPost
                  ? customStylesEditPost
                  : customStyles
              }
              options={options}
              components={{ DropdownIndicator }}
              onChange={onChange}
              {...this.props}
            />
          )}
        </div>
        <style jsx>
          {`
            :global(.greyChevronIcon) {
              width: 0.658vw;
              margin: 0 0 0 0.219vw !important;
            }
            :global(.react-select__control--is-disabled) {
              background: ${WHITE_COLOR};
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(SelectTopLabel);
