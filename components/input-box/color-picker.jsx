// Main React Components
import React from "react";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

// react select npm package
import Select, { components } from "react-select";
import {
  Border_LightGREY_COLOR,
  Border_LightGREY_COLOR_2,
  colorPickerBg,
  FONTGREY_COLOR,
  GREY_VARIANT_1,
  GREY_VARIANT_3,
  SelectInput_Chevron_Down_Grey,
  THEME_COLOR,
  WHITE_COLOR,
} from "../../lib/config";

function ColorPicker(props) {
  const { placeholder, options, onChange, boatsPage } = props;

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      backgroundColor: `${WHITE_COLOR}`,
      letterSpacing: "0.036vw !important",
      margin: "0 !important",
      cursor: "pointer",
      padding: "8px 0",
      width: "2.82vw !important",
    }),
    control: (provided, state) => ({
      ...provided,
      cursor: "pointer",
      width: "100%",
      minWidth: "7.32vw",
      minHeight: "2.342vw",
      height: "auto",
      margin: "0.366vw 0",
      padding: "0",
      border: "none",
      borderBottom: `0.073vw solid ${Border_LightGREY_COLOR_2}`,
      borderRadius: "0",
      boxShadow: "none !important",
      "&:hover": {
        borderColor: `${Border_LightGREY_COLOR_2}`,
      },
      "&:focus, &:active": {
        borderColor: `${Border_LightGREY_COLOR_2}`,
      },
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      paddingLeft: "0",
    }),
    input: (provided) => ({
    ...provided,
      display: "inline-flex",
      fontSize: "0.805vw",
      width: "0",
      "& div input": {
        color: `${GREY_VARIANT_1}`,
        fontFamily: "Open Sans-SemiBold !important",
        textTransform: "capitalize",
      },
    }),
    indicatorSeparator: (provided) => ({
    ...provided,
      display: "none",
    }),
    placeholder: (provided) => ({
    ...provided,
      fontSize: "0.805vw",
      color: `${GREY_VARIANT_3} !important`,
      fontFamily: "Museo-Sans !important",
      textTransform: "capitalize",
    }),
    singleValue: (provided) => ({
    ...provided,
      fontSize: "0.805vw",
      color: `${GREY_VARIANT_1}`,
      textTransform: "capitalize !important",
      width: "100%",
      fontFamily: "Open Sans-SemiBold !important",
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      padding: "0.366vw !important",
    }),
    menu: (provided) => ({
      ...provided,
      minWidth: "fit-content !important",
      zIndex: "2 !important",
      height: "auto",
      top: "70%",
      right: "0",
      minWidth: "unset !important",
      padding: "0",
    }),
    menuList: (provided) => ({
      ...provided,
      maxHeight: "10.248vw !important",
      scrollbarWidth: `thin !important`,
      scrollbarColor: "#c4c4c4 #f5f5f5",
      display: "flex",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "center",
    }),
  };

  const boatsStyles = {
    option: (provided, state) => ({
      ...provided,
      backgroundColor: `${WHITE_COLOR}`,
      letterSpacing: "0.036vw !important",
      margin: "0 !important",
      cursor: "pointer",
      padding: "8px 0",
      width: "2.82vw !important",
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      padding: "0.278vw 0.585vw",
      flexWrap: "nowrap",
    }),
    control: (provided, state) => ({
      ...provided,
      cursor: "pointer",
      width: "100%",
      minWidth: "7.32vw",
      minHeight: "2.187vw",
      height: "auto",
      margin: "0",
      border: `0.0732vw solid ${Border_LightGREY_COLOR}`,
      boxShadow: "none !important",
      "&:hover": {
        borderColor: `${Border_LightGREY_COLOR}`,
      },
      "&:focus, &:active": {
        borderColor: `${Border_LightGREY_COLOR}`,
      },
    }),
    input: (provided) => ({
    ...provided,
      display: "inline-flex",
      fontSize: "0.729166vw",
      width: "0",
      "& div input": {
        color: `${GREY_VARIANT_1}`,
        fontFamily: "Open Sans !important",
        textTransform: "capitalize",
      },
    }),
    indicatorSeparator: (provided) => ({
    ...provided,
      display: "none",
    }),
    placeholder: (provided) => ({
    ...provided,
      fontSize: "0.729166vw",
      color: `${GREY_VARIANT_3}`,
      fontFamily: "Open Sans !important",
      textTransform: "capitalize",
    }),
    singleValue: (provided) => ({
    ...provided,
      fontSize: "0.729166vw",
      color: `${GREY_VARIANT_1}`,
      textTransform: "capitalize !important",
      width: "100%",
      fontFamily: "Open Sans !important",
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      padding: "0.366vw !important",
    }),
    menu: (provided) => ({
      ...provided,
      minWidth: "fit-content !important",
      zIndex: "2 !important",
    }),
    menuList: (provided) => ({
      ...provided,
      maxHeight: "10.248vw !important",
      scrollbarWidth: `thin !important`,
      scrollbarColor: "#c4c4c4 #f5f5f5",
      display: "flex",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "space-around",
    }),
  };

  const DropdownIndicator = (props) => {
    return (
      <components.DropdownIndicator {...props}>
        <img
          src={SelectInput_Chevron_Down_Grey}
          className="greyChevronIcon"
        ></img>
      </components.DropdownIndicator>
    );
  };

  const formatOptionLabel = ({ value, label }, event) =>
    event && event.context == "value" ? (
      <div className="d-flex justify-content-between align-items-center">
        <div
          style={{
            color: "#4B5777",
            fontSize: "0.805vw",
            fontFamily: "Museo-Sans !important",
          }}
        >
          {label}
        </div>
        <div
          style={{
            background: `${value}`,
            width: "1.458vw",
            height: "1.458vw",
            borderRadius: "50%",
            boxShadow: "0px 1px 15px rgba(0, 0, 0, 0.1)",
          }}
        ></div>
      </div>
    ) : (
      <div className="d-flex flex-column align-items-center">
        <div
          style={{
            background: `${value}`,
            width: "1.458vw",
            height: "1.458vw",
            borderRadius: "50%",
            boxShadow: "0px 1px 15px rgba(0, 0, 0, 0.1)",
          }}
        ></div>
        <div
          style={{
            color: "#98A1B9",
            fontSize: "0.469vw",
            paddingTop: "0.260vw",
          }}
        >
          {label}
        </div>
      </div>
    );

  return (
    <Wrapper>
      <>
        <Select
          placeholder={placeholder}
          styles={boatsPage ? boatsStyles : customStyles}
          classNamePrefix="react-select"
          components={{ DropdownIndicator }}
          options={options}
          formatOptionLabel={formatOptionLabel}
          onChange={onChange}
          // menuIsOpen
          {...props}
        />
      </>
    </Wrapper>
  );
}

export default ColorPicker;
