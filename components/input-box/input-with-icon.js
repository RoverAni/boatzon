import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";

class InputWithIcon extends Component {
  render() {
    const { inputAdornment, classes } = this.props;
    return (
      <Wrapper>
        <Input
          {...this.props}
          id="input-with-icon-adornment"
          startAdornment={<InputAdornment position="start">{inputAdornment}</InputAdornment>}
        />
      </Wrapper>
    );
  }
}

export default InputWithIcon;
