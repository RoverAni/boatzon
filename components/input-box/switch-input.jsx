import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { FormControlLabel, Switch } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import {
  BLACK_COLOR,
  GREY_VARIANT_2,
  Switch_Off,
  Switch_On,
  GREEN_COLOR,
  FONTGREY_COLOR,
} from "../../lib/config";

const styles = (theme) => ({
  labelRoot: {
    width: "100%",
    margin: "0 !important",
    padding: "0",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  newsletterLabelRoot: {
    width: "100%",
    margin: "0 !important",
    padding: "0",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  messengerSettingLabelRoot: {
    width: "100%",
    margin: "0 !important",
    padding: "0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  newsletterLabel: {
    color: `${FONTGREY_COLOR} !important`,
    textTransform: "capitalize",
    fontFamily: `"Open Sans-SemiBold" !important`,
    [theme.breakpoints.up("sm")]: {
      fontSize: "10px",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "10px",
    },
    [theme.breakpoints.up(1200)]: {
      fontSize: "11px",
    },
    [theme.breakpoints.up(1400)]: {
      fontSize: "12px",
    },
    [theme.breakpoints.up(1900)]: {
      fontSize: "15px",
    },
  },
  label: {
    width: "100%",
    fontSize: "14px",
    color: `${BLACK_COLOR} !important`,
    [theme.breakpoints.up("sm")]: {
      fontSize: "10px",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "10px",
    },
    [theme.breakpoints.up(1200)]: {
      fontSize: "11px",
    },
    [theme.breakpoints.up(1400)]: {
      fontSize: "12px",
    },
    [theme.breakpoints.up(1900)]: {
      fontSize: "15px",
    },
  },
  switchRoot: {
    padding: "12px 0",
    [theme.breakpoints.up("sm")]: {
      width: "56px !important",
      height: "46px",
    },
    [theme.breakpoints.up("md")]: {
      width: "62px !important",
      height: "48px",
    },
    [theme.breakpoints.up(1200)]: {
      width: "69px !important",
      height: "50px",
    },
    [theme.breakpoints.up(1400)]: {
      width: "74px !important",
      height: "53px",
    },
    [theme.breakpoints.up(1900)]: {
      width: "78px !important",
      height: "54px",
    },
  },
  newsletterSwitchRoot: {
    [theme.breakpoints.up("sm")]: {
      width: "74px !important",
      height: "46px",
    },
    [theme.breakpoints.up("md")]: {
      width: "78px !important",
      height: "48px",
    },
    [theme.breakpoints.up(1200)]: {
      width: "81px !important",
      height: "50px",
    },
    [theme.breakpoints.up(1400)]: {
      width: "86px !important",
      height: "53px",
    },
    [theme.breakpoints.up(1900)]: {
      width: "89px !important",
      height: "55px",
    },
  },
  messengerSettingSwitchRoot: {
    position: "relative",
    left: "12px",
    [theme.breakpoints.up("sm")]: {
      width: "74px !important",
      height: "46px",
    },
    [theme.breakpoints.up("md")]: {
      width: "78px !important",
      height: "48px",
    },
    [theme.breakpoints.up(1200)]: {
      width: "81px !important",
      height: "50px",
    },
    [theme.breakpoints.up(1400)]: {
      width: "86px !important",
      height: "53px",
    },
    [theme.breakpoints.up(1900)]: {
      width: "89px !important",
      height: "55px",
    },
  },
  locationToggleSwitchRoot: {
    position: "relative",
    right: "17px",
    [theme.breakpoints.up("sm")]: {
      width: "74px !important",
      height: "46px",
    },
    [theme.breakpoints.up("md")]: {
      width: "78px !important",
      height: "48px",
    },
    [theme.breakpoints.up(1200)]: {
      width: "81px !important",
      height: "50px",
    },
    [theme.breakpoints.up(1400)]: {
      width: "86px !important",
      height: "53px",
    },
    [theme.breakpoints.up(1900)]: {
      width: "89px !important",
      height: "55px",
    },
  },
  newsletterswitchThumb: {
    [theme.breakpoints.up("sm")]: {
      width: "16px",
      height: "16px",
    },
    [theme.breakpoints.up("md")]: {
      width: "18px",
      height: "18px",
    },
    [theme.breakpoints.up(1200)]: {
      width: "20px",
      height: "20px",
    },
    [theme.breakpoints.up(1400)]: {
      width: "22px",
      height: "22px",
    },
    [theme.breakpoints.up(1900)]: {
      width: "24px",
      height: "24px",
    },
  },
  switchThumb: {
    [theme.breakpoints.up("sm")]: {
      width: "16px",
      height: "16px",
    },
    [theme.breakpoints.up("md")]: {
      width: "18px",
      height: "18px",
    },
    [theme.breakpoints.up(1200)]: {
      width: "20px",
      height: "20px",
    },
    [theme.breakpoints.up(1400)]: {
      width: "22px",
      height: "22px",
    },
    [theme.breakpoints.up(1900)]: {
      width: "24px",
      height: "24px",
    },
  },
  newslettersswitchTrack: {
    borderRadius: "30px",
    backgroundColor: `${GREY_VARIANT_2} !important`,
    width: "90%",
    margin: "0 0 0 auto",
  },
  switchTrack: {
    borderRadius: "30px",
    backgroundColor: `${GREY_VARIANT_2} !important`,
    width: "80%",
    margin: "0 0 0 auto",
  },
  newsletterswitchChecked: {
    color: "white !important",
    backgroundColor: `none !important`,
    transform: "translate(72%, -50%) !important",
    [theme.breakpoints.up("sm")]: {
      transform: "translate(70%, -50%) !important",
    },
    [theme.breakpoints.up("md")]: {
      transform: "translate(72%, -50%) !important",
    },

    "&:hover": {
      backgroundColor: `transparent !important`,
    },
    "&::before": {
      content: `url(${Switch_On})`,
      zIndex: "1",
      width: "5px",
      position: "absolute",
      top: "40%",
      left: "40%",
      transform: "translate(-40%, -50%)",
      [theme.breakpoints.up("sm")]: {
        left: "39%",
        width: "3px !important",
        transform: "translate(-63%, -53%)",
      },
      [theme.breakpoints.up("md")]: {
        width: "5px !important",
        left: "40%",
        transform: "translate(-40%, -50%)",
      },
    },
    "& .MuiIconButton-label": {
      "&::before": {
        content: `" " !important`,
        zIndex: "2",
        width: "15px",
        position: "absolute",
        top: "40%",
        left: "40%",
        transform: "translate(-30%, -49%)",
      },
    },
  },
  switchChecked: {
    color: "white !important",
    backgroundColor: `none !important`,
    [theme.breakpoints.up("sm")]: {
      transform: "translate(64%, -51%) !important",
    },
    [theme.breakpoints.up("md")]: {
      transform: "translate(69%, -51%) !important",
    },
    [theme.breakpoints.up(1200)]: {
      transform: "translate(77%, -50%) !important",
    },
    "&:hover": {
      backgroundColor: `transparent !important`,
    },
    "&::before": {
      content: `url(${Switch_On})`,
      zIndex: "1",
      width: "5px",
      position: "absolute",
      top: "40%",
      left: "40%",
      [theme.breakpoints.up("sm")]: {
        left: "39%",
        width: "3px !important",
        transform: "translate(-63%, -53%)",
      },
      [theme.breakpoints.up("md")]: {
        width: "5px !important",
        left: "40%",
        transform: "translate(-40%, -51%)",
      },
    },
    "& .MuiIconButton-label": {
      "&::before": {
        content: `" " !important`,
        zIndex: "2",
        width: "15px",
        position: "absolute",
        top: "40%",
        left: "40%",
        transform: "translate(-30%, -49%)",
      },
    },
  },
  switchColor: {
    "&$switchChecked + $switchTrack": {
      backgroundColor: `${GREEN_COLOR} !important`,
      opacity: "1",
    },
  },
  newsletterswitchColor: {
    "&$newsletterswitchChecked + $newslettersswitchTrack": {
      backgroundColor: `${GREEN_COLOR} !important`,
      opacity: "1",
    },
  },
  newsletterswitchBase: {
    backgroundColor: `none !important`,
    top: "50%",
    left: "12%",
    transform: "translate(5%, -50%)",
    zIndex: 1,
    "&:hover": {
      backgroundColor: `transparent !important`,
    },
    "& .MuiIconButton-label": {
      "&::before": {
        content: `url(${Switch_Off})`,
        zIndex: "2",
        width: "15px",
        position: "absolute",
        top: "40%",
        left: "40%",
        transform: "translate(-30%, -51%)",
      },
    },
  },
  switchBase: {
    backgroundColor: `none !important`,
    top: "50%",
    left: "12%",
    zIndex: 1,
    [theme.breakpoints.up("sm")]: {
      transform: "translate(-5%, -51%)",
    },
    [theme.breakpoints.up("md")]: {
      transform: "translate(1%, -51%)",
    },
    [theme.breakpoints.up(1200)]: {
      transform: "translate(1%, -51%)",
    },
    "&:hover": {
      backgroundColor: `transparent !important`,
    },
    "& .MuiIconButton-label": {
      "&::before": {
        content: `url(${Switch_Off})`,
        zIndex: "2",
        width: "15px",
        position: "absolute",
        top: "40%",
        left: "40%",
        [theme.breakpoints.up("sm")]: {
          width: "10px",
          transform: "translate(-25%, -51%)",
        },
        [theme.breakpoints.up("md")]: {
          width: "10px",
          transform: "translate(-23%, -51%)",
        },
        [theme.breakpoints.up(1200)]: {
          transform: "translate(-22%, -51%)",
        },
      },
    },
  },
  switchInputBox: {
    width: "100%",
    left: 0,
  },
});

const onImg = <img src={Switch_On}></img>;
class SwitchInput extends Component {
  render() {
    const {
      label,
      classes,
      triggerOnChange,
      onChange,
      value,
      newsletter,
      checked,
      messengerSetting,
      locationToggle,
    } = this.props;

    return (
      <Wrapper>
        {triggerOnChange && triggerOnChange ? (
          <FormControlLabel
            classes={
              locationToggle
                ? {
                    root: classes.newsletterLabelRoot,
                    label: classes.newsletterLabel,
                  }
                : messengerSetting
                ? {
                    root: classes.messengerSettingLabelRoot,
                    label: classes.newsletterLabel,
                  }
                : newsletter
                ? {
                    root: classes.newsletterLabelRoot,
                    label: classes.newsletterLabel,
                  }
                : {
                    root: classes.labelRoot,
                    label: classes.label,
                  }
            }
            control={
              <Switch
                classes={
                  locationToggle
                    ? {
                        root: classes.locationToggleSwitchRoot,
                        thumb: classes.newsletterswitchThumb,
                        track: classes.newslettersswitchTrack,
                        checked: classes.newsletterswitchChecked,
                        colorSecondary: classes.newsletterswitchColor,
                        switchBase: classes.newsletterswitchBase,
                        input: classes.switchInputBox,
                      }
                    : newsletter
                    ? {
                        root: classes.newsletterSwitchRoot,
                        thumb: classes.newsletterswitchThumb,
                        track: classes.newslettersswitchTrack,
                        checked: classes.newsletterswitchChecked,
                        colorSecondary: classes.newsletterswitchColor,
                        switchBase: classes.newsletterswitchBase,
                        input: classes.switchInputBox,
                      }
                    : messengerSetting
                    ? {
                        root: classes.messengerSettingSwitchRoot,
                        thumb: classes.newsletterswitchThumb,
                        track: classes.newslettersswitchTrack,
                        checked: classes.newsletterswitchChecked,
                        colorSecondary: classes.newsletterswitchColor,
                        switchBase: classes.newsletterswitchBase,
                        input: classes.switchInputBox,
                      }
                    : {
                        root: classes.switchRoot,
                        thumb: classes.switchThumb,
                        track: classes.switchTrack,
                        checked: classes.switchChecked,
                        colorSecondary: classes.switchColor,
                        switchBase: classes.switchBase,
                        input: classes.switchInputBox,
                      }
                }
                onChange={onChange}
                value={value}
                checked={checked}
              />
            }
            label={label}
            labelPlacement="start"
          />
        ) : (
          " "
        )}
      </Wrapper>
    );
  }
}

export default withStyles(styles)(SwitchInput);
