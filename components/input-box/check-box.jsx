import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import { FormControlLabel, Checkbox, withStyles } from "@material-ui/core";
import {
  THEME_COLOR,
  GREY_VARIANT_1,
  WHITE_COLOR,
  FONTGREY_COLOR,
} from "../../lib/config";
import clsx from "clsx";

const styles = (theme) => ({
  labelRoot: {
    margin: "0",
  },
  LoanSmallLabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0219vw !important",
    fontSize: "0.805vw",
  },
  Loanlabel: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    color: `${FONTGREY_COLOR}`,
    fontFamily: "Open Sans-SemiBold !important",
    letterSpacing: "0.0219vw !important",
    fontSize: "0.878vw",
  },
  label: {
    padding: "0 0.366vw 0 0.219vw",
    position: "relative",
    left: "-0.366vw",
    color: `${GREY_VARIANT_1}`,
    fontFamily: "Open Sans !important",
    letterSpacing: "0.0219vw !important",
    fontSize: "0.732vw",
  },
  root: {
    paddingLeft: "0",
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  checked: {
    color: `${THEME_COLOR} !important`,
  },
  plainicon: {
    borderRadius: 3,
    width: "1.024vw",
    height: "1.024vw",
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto rgba(19,124,189,.6)",
      outlineOffset: 2,
    },
    "input:hover ~ &": {
      backgroundColor: "#ebf1f5",
    },
    "input:disabled ~ &": {
      boxShadow: "none",
      background: "rgba(206,217,224,.5)",
    },
  },
  icon: {
    borderRadius: 3,
    width: "1.024vw !important",
    height: "1.024vw !important",
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto rgba(19,124,189,.6)",
      outlineOffset: 2,
    },
    "input:hover ~ &": {
      backgroundColor: "#ebf1f5",
    },
    "input:disabled ~ &": {
      boxShadow: "none",
      background: "rgba(206,217,224,.5)",
    },
  },
  plaincheckedIcon: {
    backgroundColor: `${WHITE_COLOR}`,
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: "1.024vw",
      height: "1.024vw",
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%232A3656'/%3E%3C/svg%3E\")",
      content: '""',
    },
    "input:hover ~ &": {
      backgroundColor: `${WHITE_COLOR}`,
    },
  },
  checkedIcon: {
    backgroundColor: `${THEME_COLOR}`,
    width: "1.024vw !important",
    height: "1.024vw !important",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: "0.146vw !important",
      height: "0.146vw !important",
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
      content: '""',
    },
    "input:hover ~ &": {
      backgroundColor: `${THEME_COLOR}`,
    },
  },
});

class CheckBox extends Component {
  render() {
    const {
      onChange,
      value,
      label,
      classes,
      plaincheckbox,
      getLoanApproval,
      loanText,
    } = this.props;
    return (
      <Wrapper>
        <FormControlLabel
          classes={
            getLoanApproval
              ? loanText
                ? { root: classes.labelRoot, label: classes.LoanSmallLabel }
                : { root: classes.labelRoot, label: classes.Loanlabel }
              : {
                  root: classes.labelRoot,
                  label: classes.label,
                }
          }
          control={
            <Checkbox
              classes={{
                root: classes.root,
                checked: classes.checked,
              }}
              checkedIcon={
                <span
                  className={
                    plaincheckbox
                      ? clsx(classes.plainicon, classes.plaincheckedIcon)
                      : clsx(classes.icon, classes.checkedIcon)
                  }
                />
              }
              icon={
                <span
                  className={plaincheckbox ? classes.plainicon : classes.icon}
                />
              }
              {...this.props}
              onChange={onChange}
              value={value}
            />
          }
          label={label}
        />
      </Wrapper>
    );
  }
}

export default withStyles(styles)(CheckBox);
