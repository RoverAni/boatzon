// Main React Components
import React from "react";

const InputBox = props => {
  const { type } = props;
  return <input type={type} autoComplete="off" {...props} />;
};

export default InputBox;
