import React, { Component } from "react";
import Video from "twilio-video";
import Button from "../button/customButton";
import Card from "@material-ui/core/Card";
import { acceptCallInviteV1, disconnectVideoCall } from "../../services/calling";
import * as assets from "../../lib/config";

export default class VideoComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      identity: null,
      roomName: "",
      roomNameErr: false, // Track error for room name TextField
      previewTracks: null,
      localMediaAvailable: false,
      hasJoinedRoom: false,
      activeRoom: "", // Track the current active room,
      showbuttons: false,
      connectingText: false,
    };
    this.joinRoom = this.joinRoom.bind(this);
    // this.handleRoomNameChange = this.handleRoomNameChange.bind(this);
    this.roomJoined = this.roomJoined.bind(this);
    this.leaveRoom = this.leaveRoom.bind(this);
    this.detachTracks = this.detachTracks.bind(this);
    this.detachParticipantTracks = this.detachParticipantTracks.bind(this);
  }

  // handleRoomNameChange(e) {
  // let roomName = e.target.value;
  // this.setState({ roomName });
  // }

  joinRoom() {
    if (!this.state.roomName.trim()) {
      this.setState({ roomNameErr: true });
      return;
    }

    console.log("Joining room '" + this.state.roomName + "'...");
    let connectOptions = {
      name: this.state.roomName,
    };

    if (this.state.previewTracks) {
      connectOptions.tracks = this.state.previewTracks;
    }

    // Join the Room with the token from the server and the
    // LocalParticipant's Tracks.
    Video.connect(this.state.token, connectOptions).then(this.roomJoined, (error) => {
      alert("Could not connect to Twilio: " + error.message);
    });
  }

  attachTracks(tracks, container) {
    tracks.forEach((track) => {
      container.appendChild(track.attach());
    });
  }

  // Attaches a track to a specified DOM container
  attachParticipantTracks(participant, container) {
    var tracks = Array.from(participant.tracks.values());
    this.attachTracks(tracks, container);
  }

  detachTracks(tracks) {
    tracks.forEach((track) => {
      track.detach().forEach((detachedElement) => {
        detachedElement.remove();
      });
    });
    this.leaveRoom();
  }

  componentDidMount() {
    /* this check is to make sure, object has keys length of more than 5 i.e incoming call else , self made call */
    if (this.props.isIncomingCall > 4) {
      this.setState({ roomName: this.props.roomName, token: this.props.token, identity: new Date().getTime().toString() }, () => {
        acceptCallInviteV1(this.props.callId)
          .then((res) => {
            console.log("success connect from logged in users end", res);
          })
          .catch((err) => console.log("failed to connect from logged in users end", err));
      });
    } else {
      this.setState(
        {
          roomName: this.props.roomName,
          token: this.props.token,
          identity: new Date().getTime().toString(),
          showbuttons: true,
        },
        () => this.joinRoom()
      );
    }
  }

  detachParticipantTracks(participant) {
    var tracks = Array.from(participant.tracks.values());
    this.detachTracks(tracks);
  }

  roomJoined(room) {
    // Called when a participant joins a room
    console.log("Joined as '" + this.state.identity + "'");
    this.setState({
      activeRoom: room,
      localMediaAvailable: true,
      hasJoinedRoom: true,
      connectingText: true,
    });

    // Attach LocalParticipant's Tracks, if not already attached.
    var previewContainer = this.refs.localMedia;
    if (!previewContainer.querySelector("video")) {
      this.attachParticipantTracks(room.localParticipant, previewContainer);
    }

    // Attach the Tracks of the Room's Participants.
    room.participants.forEach((participant) => {
      console.log("Already in Room: '" + participant.identity + "'");

      var previewContainer = this.refs.remoteMedia;
      this.attachParticipantTracks(participant, previewContainer);
    });

    // When a Participant joins the Room, log the event.
    room.on("participantConnected", (participant) => {
      console.log("Joining: '" + participant.identity + "'");
    });

    // When a Participant adds a Track, attach it to the DOM.
    room.on("trackAdded", (track, participant) => {
      console.log(participant.identity + " added track: " + track.kind);
      this.setState({ connectingText: false });
      var previewContainer = this.refs.remoteMedia;

      this.attachTracks([track], previewContainer);
    });

    // When a Participant removes a Track, detach it from the DOM.
    room.on("trackRemoved", (track, participant) => {
      console.log(participant.identity + " removed track: " + track.kind);
      this.detachTracks([track]);
    });

    // When a Participant leaves the Room, detach its Tracks.
    room.on("participantDisconnected", (participant) => {
      console.log("Participant '" + participant.identity + "' left the room");
      this.detachParticipantTracks(participant);
    });

    // Once the LocalParticipant leaves the room, detach the Tracks
    // of all Participants, including that of the LocalParticipant.
    room.on("disconnected", () => {
      if (this.state.previewTracks) {
        this.state.previewTracks.forEach((track) => {
          track.stop();
        });
      }
      this.detachParticipantTracks(room.localParticipant);
      room.participants.forEach(this.detachParticipantTracks);
      this.state.activeRoom = null;
      this.setState({ hasJoinedRoom: false, localMediaAvailable: false });
    });
  }

  leaveRoom() {
    if (this.state.activeRoom) {
      this.state.activeRoom.disconnect();
      this.setState({ hasJoinedRoom: false, localMediaAvailable: false, showbuttons: false });
    }
    let obj = {
      callId: this.props.callId,
      callFrom: "noconnect",
    };
    disconnectVideoCall(obj)
      .then((res) => {
        this.props.wipeWebrtcData();
        console.log("success disconnect call", res);
      })
      .catch((err) => console.log("disconnect call err", err));
  }

  render() {
    // Only show video track after user has joined a room
    let showLocalTrack = this.state.localMediaAvailable ? (
      <div className="yourVideo">
        <div ref="localMedia" />
      </div>
    ) : (
      ""
    );
    return (
      <div>
        <div className={this.state.hasJoinedRoom ? "main_window col-12 px-0" : "not_started_main_window col-12 px-0"}>
          <div className="row mx-0">
            <div className="col-12 px-0">
              <div className="callerVideo" ref="remoteMedia" id="remote-media" />
              {showLocalTrack}
            </div>
            {this.state.hasJoinedRoom ? (
              ""
            ) : (
              <div className="col-12 callersText text-center py-3">
                {this.props.isIncomingCall === 3 || this.props.isIncomingCall === 4
                  ? `Please wait, Calling ${this.props.UserSelectedToChat && this.props.UserSelectedToChat.userName}`
                  : `Incoming call from ${this.props.webrtc && this.props.webrtc.userName}`}
              </div>
            )}
            {this.props.isIncomingCall > 4 ? (
              <div className="col-12 accept_reject_buttons">
                <Button
                  className="pickUpCall mx-2"
                  text={
                    <div>
                      <img src={assets.PickUpCall} alt="pick up call" height={30} width={30} />
                    </div>
                  }
                  handler={this.joinRoom}
                />
                <Button
                  handler={this.leaveRoom}
                  className="disconnectCall mx-2"
                  text={
                    <div>
                      <img src={assets.PickUpCall} alt="pick up call" height={30} width={30} />
                    </div>
                  }
                />
              </div>
            ) : (
              <div className="col-12 accept_reject_buttons">
                <Button
                  handler={this.leaveRoom}
                  className="disconnectCall mx-2"
                  text={
                    <div>
                      <img src={assets.PickUpCall} alt="pick up call" height={30} width={30} />
                    </div>
                  }
                />
              </div>
            )}
          </div>
          {this.state.connectingText ? <div className="connectingText">Connecting...</div> : ""}
        </div>

        <style jsx>
          {`
            :global(.callerVideo) {
              height: ${this.state.hasJoinedRoom ? "calc(80vh - 60px)" : "0px"};
              width: "inherit";
            }
            :global(.callerVideo video) {
              height: ${this.state.hasJoinedRoom ? "inherit" : "0px"};
              width: 100%;
              object-fit: unset;
            }
            :global(.pickUpCall) {
              background: #57b54a !important;
              border-radius: 50%;
              height: 50px;
              width: 50px;
              display: flex;
              justify-content: center;
              align-items: center;
              border: none !important;
              cursor: pointer;
            }
            :global(.disconnectCall) {
              background: #ee4426 !important;
              border-radius: 50%;
              height: 50px;
              width: 50px;
              display: flex;
              justify-content: center;
              align-items: center;
              border: none !important;
              cursor: pointer;
            }
            :global(.disconnectCall img) {
              transform: rotate(265deg);
            }
            :global(.yourVideo) {
              height: ${this.state.hasJoinedRoom ? "20vh" : "0px"};
              position: absolute;
              top: 20px !important;
              right: 20px !important;
              width: ${this.state.hasJoinedRoom ? "20vw" : "0px"};
              height: 20vh !important;
            }
            :global(.yourVideo div video) {
              position: absolute;
              top: 0px !important;
              right: -15px;
              height: ${this.state.hasJoinedRoom ? "20vh" : "0px"};
              width: ${this.state.hasJoinedRoom ? "20vw !important" : "0px"};
            }
            :global(.callersText) {
              font-family: "Open Sans-SemiBold" !important;
              font-size: calc((112.5% + 0.25vw));
            }
            :global(.main_window) {
              height: inherit !important;
              object-fit: unset;
              width: inherit;
            }
            :global(.accept_reject_buttons) {
              bottom: 0px !important;
              height: 60px;
              display: flex;
              width: inherit !important;
              justify-content: center;
              align-items: center;
            }
            :global(.not_started_main_window) {
              height: 150px !important;
              width: inherit !important;
            }
            :global(.connectingText) {
              position: absolute;
              color: #ffffff;
              font-size: 4.162vw;
              margin-bottom: 0;
              font-family: "Museo-Sans-Cyrl-Bolder" !important;
              margin: 0 0 0 auto;
              text-align: right;
              text-shadow: 0px 4px 10px rgba(0, 0, 0, 0.35);
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
            }
          `}
        </style>
      </div>
    );
  }
}
