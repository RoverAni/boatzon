import React, { Component } from "react";
import Wrapper from "../../hoc/Wrapper";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Calender_Blue_Icon } from "../../lib/config";

class DatesRangePicker extends Component {
  render() {
    const { startDate, endDate, onApply, value } = this.props;
    return (
      <Wrapper>
        <DateRangePicker
          startDate={startDate}
          endDate={endDate}
          onApply={onApply}
        >
          <input type="text" id="daterange" name="daterange" value={value} />
          <img src={Calender_Blue_Icon} className="calenderIcon"></img>
        </DateRangePicker>
      </Wrapper>
    );
  }
}

export default DatesRangePicker;
