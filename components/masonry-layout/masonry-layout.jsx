// Main React Components
import React, { Component } from "react";

// react masonry npm package
import Masonry from "react-masonry-css";

// wrapping component
import Wrapper from "../../hoc/Wrapper";

class MasonryLayout extends Component {
  render() {
    const breakpointColumnsObj = {
      default: 5,
      1900: 5,
      1550: 5,
      1200: 4,
      992: 3,
    };
    const { breakpointCols } = this.props;
    return (
      <Wrapper>
        <Masonry
          breakpointCols={
            breakpointCols ? breakpointCols : breakpointColumnsObj
          }
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {this.props.children}
        </Masonry>
        <style jsx>
          {`
            :global(.my-masonry-grid) {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              margin-left: 0;
              width: auto;
              justify-content: start;
              width: 100%;
            }
            :global(.my-masonry-grid_column) {
              padding-left: 0;
              background-clip: padding-box;
              // padding-right: 1.098vw;
              padding: 0.549vw;
            }
            :global(.my-masonry-grid_column > div) {
              margin-bottom: 1.098vw;
            }
          `}
        </style>
      </Wrapper>
    );
  }
}

export default MasonryLayout;
