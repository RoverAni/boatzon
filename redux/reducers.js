// eslint-disable-next-line no-unused-vars
import * as actionTypes from "./actions/actionTypes";
import { DEFAULT_LANGUAGE } from "../lib/config";
import { updateObject } from "../shared/utility";
import { LanguageSet } from "../translations/LanguageSet";

const languages = LanguageSet;

export const initialState = {
  language: DEFAULT_LANGUAGE,
  locale: LanguageSet[DEFAULT_LANGUAGE],
  boatData: {},
  userSelectedToChat: {},
  allChats: [],
  allMessagesForSelectedUser: [],
  webrtc: {},
};

const updateLocale = (state, action) => {
  return updateObject(state, {
    locale: languages[action.selectedLang],
    selectedLang: action.selectedLang,
  });
};

const storeWebrtcData = (state, action) => {
  return updateObject(state, { webrtc: action.data });
};

const updateCurrentLocation = (state, action) => {
  return updateObject(state, { currentLocation: action.data });
};

const updateSpecilityData = (state, action) => {
  return updateObject(state, { specialities: action.data });
};

const updateAPILoading = (state, action) => {
  return updateObject(state, { apiLoading: action.data });
};

const updateReduxState = (state, action) => {
  return updateObject(state, { [`${action.stateName}`]: action.data });
};

const updateBoatsSubCategories = (state, action) => {
  return updateObject(state, { BoatsSubCategoriesList: action.data });
};

const updateProductsSubCategories = (state, action) => {
  return updateObject(state, { ProductsSubCategoriesList: action.data });
};

const updateManufacturers = (state, action) => {
  return updateObject(state, { ManufacturersList: action.data });
};

const updateSignupData = (state, action) => {
  return updateObject(state, {
    signUpData: action.data,
  });
};

const updateEngineData = (state, action) => {
  return updateObject(state, { engineData: action.data });
};

const updateTrailerData = (state, action) => {
  return updateObject(state, { TrailerData: action.data });
};

const updateUserProfileData = (state, action) => {
  return updateObject(state, { userProfileData: action.data });
};

const updateFilterPostData = (state, action) => {
  return updateObject(state, { filterPostData: action.data });
};

const updateApiCallLoaderData = (state, action) => {
  return updateObject(state, { apiCallLoader: action.data });
};

const updateBoatFilterPayloadData = (state, action) => {
  return updateObject(state, { filterBoatPayloadData: action.data });
};

const updateProductFilterPayloadData = (state, action) => {
  return updateObject(state, { filterProductPayloadData: action.data });
};

const updateDiscussionData = (state, action) => {
  return updateObject(state, { discussionCommentsData: action.data });
};

const updateUserSelectedToChatData = (state, action) => {
  return updateObject(state, {
    userSelectedToChat: action.data,
  });
};

const storeAllChatData = (state, action) => {
  return updateObject(state, {
    allChats: action.data,
  });
};

const storeSelectedUserAllMessages = (state, action) => {
  let prevData = action.data;
  /** this function will check for duplicate message and remove them */
  const uniqueArray = prevData.filter((thing, index) => {
    return (
      index ===
      prevData.findIndex((obj) => {
        return JSON.stringify(obj) === JSON.stringify(thing);
      })
    );
  });
  return updateObject(state, {
    allMessagesForSelectedUser: uniqueArray,
  });
};

const addNewChatToSelectChat = (state, action) => {
  return {
    ...state,
    allMessagesForSelectedUser: [
      action.data,
      ...state.allMessagesForSelectedUser,
    ],
  };
};

const removeSelectedUserAllMessages = (state, action) => {
  return {
    ...state,
    allMessagesForSelectedUser: [],
  };
};

const updateAcknowledgementForLatestMessage = (state, action) => {
  console.log("update ack", action);
  let allMessages = [...state.allMessagesForSelectedUser];
  let index = allMessages.findIndex(
    (k) => k.id === action.messageId || k.messageId === action.messageId
  );
  allMessages[index].status = action.status;
  return {
    ...state,
    allMessagesForSelectedUser: allMessages,
  };
};

const updateBoatData = (state, action) => {
  return updateObject(state, { boatData: action.data });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.INIT_LANGUAGE_CHANGE:
      return updateLocale(state, action);

    case actionTypes.SET_APILOADING:
      return updateAPILoading(state, action);

    case actionTypes.SET_REDUX_STATE:
      return updateReduxState(state, action);

    case actionTypes.SET_GEO_LOCATION:
      return updateCurrentLocation(state, action);

    case actionTypes.SET_BOATS_SUB_CATEGORIES:
      return updateBoatsSubCategories(state, action);

    case actionTypes.SET_PRODUCTS_SUB_CATEGORIES:
      return updateProductsSubCategories(state, action);

    case actionTypes.SET_MANUFACTURER:
      return updateManufacturers(state, action);

    case actionTypes.SET_SIGNUPDATA:
      return updateSignupData(state, action);

    case actionTypes.SET_ENGINE_DATA:
      return updateEngineData(state, action);

    case actionTypes.SET_TRAILER_DATA:
      return updateTrailerData(state, action);

    case actionTypes.SET_USER_DATA:
      return updateUserProfileData(state, action);

    case actionTypes.SET_POST_DATA:
      return updateFilterPostData(state, action);

    case actionTypes.SET_API_LOADER:
      return updateApiCallLoaderData(state, action);

    case actionTypes.SET_BOAT_FILTER_PAYLOAD:
      return updateBoatFilterPayloadData(state, action);

    case actionTypes.SET_PRODUCT_FILTER_PAYLOAD:
      return updateProductFilterPayloadData(state, action);

    case actionTypes.SET_DISCUSSION_COMMENT_DATA:
      return updateDiscussionData(state, action);
    // create your cases and update state here

    case actionTypes.USER_SELECTED_TO_CHAT: {
      return updateUserSelectedToChatData(state, action);
    }

    case actionTypes.STORE_BOAT_DATA: {
      return updateBoatData(state, action);
    }
    /** this will store left side all chats */
    case actionTypes.STORE_SINGLE_USER_ALL_MESSAGES: {
      return storeSelectedUserAllMessages(state, action);
    }
    /** this will store user selected  all chats */
    case actionTypes.STORE_ALL_CHAT_DATA: {
      return storeAllChatData(state, action);
    }

    /** this will update user selected  all chats, with every new message */
    case actionTypes.ADD_NEW_CHAT_TO_SELECTED_USER: {
      return addNewChatToSelectChat(state, action);
    }

    case actionTypes.STORE_WEBRTC_DATA: {
      return storeWebrtcData(state, action);
    }

    case actionTypes.REMOVE_SINGLE_USER_ALL_MESSAGES: {
      return removeSelectedUserAllMessages(state, action);
    }

    case actionTypes.UPDATE_ACKNOWLEDGEMENT: {
      return updateAcknowledgementForLatestMessage(state, action);
    }

    case actionTypes.SET_SPECILITY:
      return updateSpecilityData(state, action);

    default:
      return state;
  }
};

export default reducer;
