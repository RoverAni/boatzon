import { SET_POST_DATA, GET_POST_DATA, SET_BOAT_FILTER_PAYLOAD, SET_PRODUCT_FILTER_PAYLOAD, SET_API_LOADER } from "../actionTypes";

export const setFilterPost = (data) => {
  return {
    type: SET_POST_DATA,
    data: data,
  };
};

export const getFilterPost = (data) => {
  return {
    type: GET_POST_DATA,
    data: data,
  };
};

export const setApiCallLoader = (data) => {
  return {
    type: SET_API_LOADER,
    data: data,
  };
}

export const setBoatFilterPayload = (data) => {
  return {
    type: SET_BOAT_FILTER_PAYLOAD,
    data: data,
  };
};

export const setProductFilterPayload = (data) => {
  return {
    type: SET_PRODUCT_FILTER_PAYLOAD,
    data: data,
  };
};
