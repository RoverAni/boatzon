import { STORE_WEBRTC_DATA } from "../actionTypes";

export const storeWebrtcData = (data) => {
  return {
    type: STORE_WEBRTC_DATA,
    data: data,
  };
};
