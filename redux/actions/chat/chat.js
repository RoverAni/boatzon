import {
  USER_SELECTED_TO_CHAT,
  STORE_ALL_CHAT_DATA,
  STORE_SINGLE_USER_ALL_MESSAGES,
  ADD_NEW_CHAT_TO_SELECTED_USER,
  UPDATE_ACKNOWLEDGEMENT,
  REMOVE_SINGLE_USER_ALL_MESSAGES,
  STORE_ALL_CHAT_DATA_FAIL,
  STORE_ALL_CHAT_DATA_INIT,
  STORE_SINGLE_USER_ALL_MESSAGES_FAIL,
  STORE_SINGLE_USER_ALL_MESSAGES_INIT,
} from "../actionTypes";

export const userSelectedToChat = (data) => {
  return {
    type: USER_SELECTED_TO_CHAT,
    data: data,
  };
};

export const storeAllChatData = (data) => {
  return {
    type: STORE_ALL_CHAT_DATA,
    data: data,
  };
};

export const storeAllChatDataInit = (pageNo) => {
  return {
    type: STORE_ALL_CHAT_DATA_INIT,
    pageNo: pageNo,
  };
};

export const storeDataFailed = () => {
  return {
    type: STORE_ALL_CHAT_DATA_FAIL,
  };
};

export const onScrollInit = (timestamp, chatId, lastIndex) => {
  return {
    type: STORE_SINGLE_USER_ALL_MESSAGES_INIT,
    timestamp: timestamp,
    chatId: chatId,
    lastIndex: lastIndex,
  };
};

export const storeAllChatsOfSelectedUser = (data) => {
  return {
    type: STORE_SINGLE_USER_ALL_MESSAGES,
    data: data,
  };
};

export const onScrollFail = () => {
  return {
    type: STORE_SINGLE_USER_ALL_MESSAGES_FAIL,
  };
};

export const addNewChatToSelectChat = (data) => {
  return {
    type: ADD_NEW_CHAT_TO_SELECTED_USER,
    data: data,
  };
};

export const updateAcknowledgement = (messageId, status) => {
  return {
    type: UPDATE_ACKNOWLEDGEMENT,
    messageId: messageId,
    status: status,
  };
};

export const removeSelectedUserAllMessages = () => {
  return {
    type: REMOVE_SINGLE_USER_ALL_MESSAGES,
  };
};
