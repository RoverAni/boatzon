import { GET_MANUFACTURER, SET_MANUFACTURER } from "../actionTypes";

export const getManufacturers = (data) => {
  return {
    type: GET_MANUFACTURER,
    data: data,
  };
};

export const setManufacturers = (data) => {
  return {
    type: SET_MANUFACTURER,
    data: data,
  };
};
