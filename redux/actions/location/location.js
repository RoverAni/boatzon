import { GET_GEO_LOCATION, SET_GEO_LOCATION } from "../actionTypes";

export const getCurrentLocation = (data) => {
  return {
    type: GET_GEO_LOCATION,
    data: data,
  };
};

export const setCurrentLocation = (data) => {
  return {
    type: SET_GEO_LOCATION,
    data: data,
  };
};
