import { GET_SIGNUPDATA, SET_SIGNUPDATA } from "../actionTypes";

export const getSignupData = (data) => {
  return {
    type: GET_SIGNUPDATA,
    data: data,
  };
};

export const setSignupData = (data) => {
  return {
    type: SET_SIGNUPDATA,
    data: data,
  };
};
