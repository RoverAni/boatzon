import {
  GET_BOATS_SUB_CATEGORIES,
  SET_BOATS_SUB_CATEGORIES,
  GET_PRODUCTS_SUB_CATEGORIES,
  SET_PRODUCTS_SUB_CATEGORIES,
  GET_ENGINE_DATA,
  SET_ENGINE_DATA,
  GET_TRAILER_DATA,
  SET_TRAILER_DATA,
} from "../actionTypes";

export const getBoatsSubCategoryData = (data) => {
  return {
    type: GET_BOATS_SUB_CATEGORIES,
    data: data,
  };
};

export const setBoatsSubCategoryData = (data) => {
  return {
    type: SET_BOATS_SUB_CATEGORIES,
    data: data,
  };
};

export const getProductsSubCategoryData = (data) => {
  return {
    type: GET_PRODUCTS_SUB_CATEGORIES,
    data: data,
  };
};

export const setProductsSubCategoryData = (data) => {
  return {
    type: SET_PRODUCTS_SUB_CATEGORIES,
    data: data,
  };
};

export const getEngineData = (data) => {
  return {
    type: GET_ENGINE_DATA,
    data: data,
  };
};

export const setEngineData = (data) => {
  return {
    type: SET_ENGINE_DATA,
    data: data,
  };
};

export const getTrailersData = (data) => {
  return {
    type: GET_TRAILER_DATA,
    data: data,
  };
};

export const setTrailersData = (data) => {
  return {
    type: SET_TRAILER_DATA,
    data: data,
  };
};
