import { STORE_BOAT_DATA } from "../actionTypes";

export const storeBoatData = (data) => {
  return {
    type: STORE_BOAT_DATA,
    data: data,
  };
};
