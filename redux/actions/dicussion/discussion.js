import {
  SET_DISCUSSION_COMMENT_DATA,
  GET_SPECILITY,
  SET_SPECILITY,
} from "../actionTypes";

export const setDiscussionComments = (data) => {
  return {
    type: SET_DISCUSSION_COMMENT_DATA,
    data: data,
  };
};

export const getSpecilities = (data) => {
  return {
    type: GET_SPECILITY,
    data: data,
  };
};

export const setSpecilities = (data) => {
  return {
    type: SET_SPECILITY,
    data: data,
  };
};
