import { GET_USER_DATA, SET_USER_DATA } from "../actionTypes";

export const getUserData = (data) => {
  return {
    type: GET_USER_DATA,
    data: data,
  };
};

export const setUserData = (data) => {
  return {
    type: SET_USER_DATA,
    data: data,
  };
};