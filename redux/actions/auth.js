import * as actionTypes from "./actionTypes";

export const guestLogin = () => {
  return {
    type: actionTypes.AUTH_INIT_GUEST_LOGIN,
  };
};

export const serverLogin = () => {
  return {
    type: actionTypes.SERVER_LOGIN,
  };
};

export const setApiLoading = (data) => {
  return {
    type: actionTypes.SET_APILOADING,
    data: data,
  };
};

export const setReduxState = (stateName, data) => {
  return {
    type: actionTypes.SET_REDUX_STATE,
    data: data,
    stateName: stateName,
  };
};
