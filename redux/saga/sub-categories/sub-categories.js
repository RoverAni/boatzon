import {
  getBoatsSubCategories,
  getProductsSubCategories,
} from "../../../services/categories";
import { all, call, put, take, takeLatest } from "redux-saga/effects";
import {
  setBoatsSubCategoryData,
  setProductsSubCategoryData,
  setEngineData,
  setTrailersData,
} from "../../actions/sub-categories/sub-categories";
import { getEngineData, getTrailerData } from "../../../services/create-post";
import { removeCookie } from "../../../lib/session";
import Router from "next/router";

export function* getBoatsSubCategoriesSaga() {
  try {
    const BoatsSubCategoriesResponse = yield getBoatsSubCategories();
    console.log("nvdufh", BoatsSubCategoriesResponse);
    if (BoatsSubCategoriesResponse)
      yield put(setBoatsSubCategoryData(BoatsSubCategoriesResponse.data.data));
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      Router.push(`/`);
    }
  }
}

export function* getProductsSubCategoriesSaga() {
  try {
    const ProductsSubCategoriesResponse = yield getProductsSubCategories();
    console.log("nvdufh", ProductsSubCategoriesResponse);

    if (ProductsSubCategoriesResponse)
      yield put(
        setProductsSubCategoryData(ProductsSubCategoriesResponse.data.data)
      );
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      Router.push(`/`);
    }
  }
}

export function* getEngineDataListSaga() {
  try {
    const EngineDataResponse = yield getEngineData();
    if (EngineDataResponse)
      yield put(setEngineData(EngineDataResponse.data.data));
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      Router.push(`/`);
    }
  }
}

export function* getTrailersDataListSaga() {
  try {
    const TrailersDataResponse = yield getTrailerData();
    if (TrailersDataResponse)
      yield put(setTrailersData(TrailersDataResponse.data.data));
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      Router.push(`/`);
    }
  }
}
