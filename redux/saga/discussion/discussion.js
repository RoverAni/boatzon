import { removeCookie } from "../../../lib/session";
import { getProfessionals } from "../../../services/professionals";
import { setSpecilities } from "../../actions/dicussion/discussion";
import { put } from "redux-saga/effects";

export function* getSpecilitieListSaga() {
  try {
    const Response = yield getProfessionals();
    console.log("djwid", Response);
    if (Response) yield put(setSpecilities(Response.data.data));
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      window.location.reload();
    }
  }
}
