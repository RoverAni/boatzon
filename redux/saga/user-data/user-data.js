import { all, call, put, take, takeLatest } from "redux-saga/effects";
import { getUserProfileData } from "../../../services/userProfile";
import { setUserData } from "../../actions/user-data/user-data";
import { removeCookie } from "../../../lib/session";

export function* getUserDataListSaga() {
  try {
    const UserProfileResponse = yield getUserProfileData();

    if (UserProfileResponse)
      yield put(setUserData(UserProfileResponse.data.data));
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      window.location.href = "/";
    }
  }
}
