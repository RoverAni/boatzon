import { put } from "redux-saga/effects";
import { setCurrentLocation } from "../../actions/location/location";
import { getLocation } from "../../../lib/location/geo-location";
import { removeCookie, getCookie } from "../../../lib/session";
import {
  setupFetchLocation,
  setupLocation,
} from "../../../lib/data-handler/data-handler";

export function* getUserLocation() {
  try {
    let fetchLocation = getCookie("fetchLocation");
    let x = fetchLocation
      ? JSON.parse(getCookie("currentLocation"))
      : yield getLocation();
    console.log("location-->", x);
    setupFetchLocation(x);
    setupLocation(x);

    yield put(setCurrentLocation(x));
  } catch (ex) {
    console.log("GET LOCATION  SAGA -->", ex);
  }
}
