import { put, takeLatest, all } from "redux-saga/effects";
import * as actionDispatcher from "../../actions/chat/chat";
import { getChatsWithPageNo, getChatsForSelectedUser } from "../../../services/chat";

export function* getAllChats(action) {
  try {
    const response = yield getChatsWithPageNo(action.pageNo);
  } catch (e) {
    console.log("error getting chat", e);
    yield put(actionDispatcher.storeDataFailed());
  }
}

export function* getAllChatsForSelectedUser(action) {
  try {
    const response = yield getChatsForSelectedUser(action.timestamp, action.chatId, action.lastIndex);
  } catch (e) {
    console.log("error getting chat", e);
    yield put(actionDispatcher.onScrollFail());
  }
}
