/* eslint-disable require-jsdoc */
import { takeEvery, all } from "redux-saga/effects";
import * as actionTypes from "../actions/actionTypes";
import { serverLogin } from "./saga";
import { getUserLocation } from "./location/location";
import {
  getBoatsSubCategoriesSaga,
  getProductsSubCategoriesSaga,
  getEngineDataListSaga,
  getTrailersDataListSaga,
} from "./sub-categories/sub-categories";
import { ManufacturersListSaga } from "./manufacturer/manufacturer";
import { getUserDataListSaga } from "./user-data/user-data";
import { getFilterPostListSaga } from "./post-data/post-data";
import { getAllChats, getAllChatsForSelectedUser } from "./chat/chat";
import { getSpecilitieListSaga } from "./discussion/discussion";

export function* watchAuth() {
  yield takeEvery(actionTypes.DEMO_ACTION_TYPE, serverLogin);
}

export function* watchAllChats() {
  yield takeEvery(actionTypes.STORE_ALL_CHAT_DATA_INIT, getAllChats);
}

export function* watchAllSingleUserChats() {
  yield takeEvery(
    actionTypes.STORE_SINGLE_USER_ALL_MESSAGES_INIT,
    getAllChatsForSelectedUser
  );
}

export function* getCurrentLocation() {
  yield takeEvery(actionTypes.GET_GEO_LOCATION, getUserLocation);
}

export function* getBoatsSubCategories() {
  yield takeEvery(
    actionTypes.GET_BOATS_SUB_CATEGORIES,
    getBoatsSubCategoriesSaga
  );
}

export function* getProductsSubCategories() {
  yield takeEvery(
    actionTypes.GET_PRODUCTS_SUB_CATEGORIES,
    getProductsSubCategoriesSaga
  );
}

export function* getManufacturers() {
  yield takeEvery(actionTypes.GET_MANUFACTURER, ManufacturersListSaga);
}

export function* getEngineData() {
  yield takeEvery(actionTypes.GET_ENGINE_DATA, getEngineDataListSaga);
}

export function* getTarilerData() {
  yield takeEvery(actionTypes.GET_TRAILER_DATA, getTrailersDataListSaga);
}

export function* getUserProfile() {
  yield takeEvery(actionTypes.GET_USER_DATA, getUserDataListSaga);
}

export function* getFilterPostList() {
  yield takeEvery(actionTypes.GET_POST_DATA, getFilterPostListSaga);
}

export function* getSpecilitieList() {
  yield takeEvery(actionTypes.GET_SPECILITY, getSpecilitieListSaga);
}

export function* rootSaga() {
  yield all([
    watchAuth(),
    getCurrentLocation(),
    getBoatsSubCategories(),
    getProductsSubCategories(),
    getManufacturers(),
    getEngineData(),
    getTarilerData(),
    getUserProfile(),
    getFilterPostList(),
    watchAllChats(),
    watchAllSingleUserChats(),
    getSpecilitieList(),
  ]);
}
