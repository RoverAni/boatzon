import { put } from "redux-saga/effects";
import {
  filterProductsData,
  filterProductsDataPostLogin,
} from "../../../services/filter-products";
import {
  setFilterPost,
  setApiCallLoader,
} from "../../actions/post-data/post-data";
import { getCookie, removeCookie, setCookie } from "../../../lib/session";

export function* getFilterPostListSaga(data) {
  try {
    let AuthPass = getCookie("authPass");
    yield put(setApiCallLoader(true));
    const filterPostResponse = AuthPass
      ? yield filterProductsDataPostLogin(data.data)
      : yield filterProductsData(data.data);
    if (filterPostResponse) {
      yield put(setFilterPost(filterPostResponse.data.data));
      yield put(setApiCallLoader(false));
    }
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      window.location.reload();
    }
  }
}
