import { getManufactures } from "../../../services/manufacturer";
import { all, call, put, take, takeLatest } from "redux-saga/effects";
import { setManufacturers } from "../../actions/manufacturer/manufacturer";
import { removeCookie } from "../../../lib/session";

export function* ManufacturersListSaga() {
  try {
    const ManufacturerResponse = yield getManufactures();
    console.log("nvdufh", ManufacturerResponse);

    if (ManufacturerResponse)
      yield put(setManufacturers(ManufacturerResponse.data.data));
  } catch (ex) {
    console.log("CATE in GET CART SAGA -->", ex);
    if (ex == "Error: Request failed with status code 401") {
      removeCookie("token");
      removeCookie("authPass");
      removeCookie("mqttId");
      removeCookie("uid");
      removeCookie("userName");
      removeCookie("ucity");
      window.location.reload();
    }
  }
}
