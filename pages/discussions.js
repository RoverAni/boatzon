// Main React Components
import React from "react";
import { connect } from "react-redux";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getSpecilities } from "../redux/actions/dicussion/discussion";

// AMP Config
// export const config = { amp: false };

class Discussions extends React.Component {
  componentDidMount() {
    this.props.dispatch(getCurrentLocation());
    this.props.dispatch(getSpecilities());
  }

  render() {
    const { locale } = this.props;

    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader ogTitle={"Discussions"}></MainHeader>

          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          <AppMenu value={6} />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Discussions);
