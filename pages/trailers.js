// Main React Components
import React from "react";
import fetch from "isomorphic-unfetch";
import Router, { withRouter } from "next/router";
import dynamic from "next/dynamic";

// imported components
import MainHeader from "../containers/headers/mainHeader.jsx";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);

// reusable component
const PaginationHOC = dynamic(
  () => import("../components/pagination/paginationWithScreen"),
  { ssr: false }
);

// redux actions
import { getCurrentLocation } from "../redux/actions/location/location";

// redux context component
import Context from "../context/context";
import { connect } from "react-redux";

// asstes and colors
import { API_NY_URL, commonHeader } from "../lib/config";
import { getCookiees } from "../lib/session";

// URLSearchParams package
import "url-search-params-polyfill";

import { getTrailersData } from "../redux/actions/sub-categories/sub-categories";
import { setApiLoading } from "../redux/actions/auth";
import { filterTrailersData } from "../services/filter-products";
import { isEmpty } from "../lib/global";

// AMP Config
// export const config = { amp: false };

/** Functin to filterProduct API call */
async function getFilteredDataWithPagination(payload, token) {
  let headers = { ...commonHeader };
  if (token !== "") {
    /** API header with login token */
    headers.token = token;
  }
  const data = await fetch(API_NY_URL + "/filterTrailer", {
    method: "POST",
    headers,
    body: payload,
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      return [];
    });
  return data;
}

class Trailers extends React.Component {
  /**  Function to filterProduct API call before page load */
  static async getInitialProps({ ctx }) {
    const urlString = ctx.req && ctx.req.query;
    let token = getCookiees("token", ctx.req);
    let getFilterProductData = {};
    let userName;
    if (token) {
      userName = getCookiees("userName", ctx.req);
      const payload = new URLSearchParams(); // API body payload with login
      payload.append("category", `Trailers`);
      payload.append("limit", `5`);
      payload.append("offset", `0`);
      payload.append("name", `${userName}`);

      getFilterProductData = await getFilteredDataWithPagination(
        payload,
        token
      );
    } else {
      const payload = new URLSearchParams(); // API body payload without login
      payload.append("category", `Trailers`);
      payload.append("limit", `5`);
      payload.append("offset", `0`);
      getFilterProductData = await getFilteredDataWithPagination(
        payload,
        token
      );
    }

    return {
      getFilterProductData,
      token,
      userName,
      urlString,
    };
  }

  state = {
    urlString: this.props.urlString,
    materialUI: false,
    filterPostData:
      this.props.getFilterProductData &&
      this.props.getFilterProductData.code == 200
        ? this.props.getFilterProductData.data
        : [],
    payload: { category: "Trailers" },
    hasMore: true,
    refreshPagination: false,
  };

  componentDidMount() {
    this.setState({
      materialUI: true,
      urlString: isEmpty(this.state.urlString)
        ? this.props.router.query
        : this.state.urlString,
    });
    this.props.dispatch(getTrailersData());
    this.props.dispatch(getCurrentLocation());
  }

  componentDidUpdate(prevProps) {
    if (prevProps.router !== this.props.router) {
      this.setState({
        urlString: this.props.router.query,
      });
    }
  }

  /** Funtion to prevent API  */
  handlePostProductsAPI = (payload) => {
    this.setState(
      {
        payload,
        filterPostData: [],
        hasMore: true,
        page: 0,
      },
      () => {
        this.handleCallAPI("5", 0, false, true);
      }
    );
  };

  handleCallAPI = (
    limit = "5",
    page = 0,
    paginationLoader = false,
    apiLoader = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        let Payload = {
          limit: limit,
          offset: page * limit,
          ...this.state.payload,
        };
        this.props.dispatch(setApiLoading(apiLoader));
        this.setState({
          paginationLoader,
          page,
        });
        let result = await filterTrailersData(Payload);
        let response = result.data;
        let oldList = [...this.state.filterPostData];
        let newList = response && response.data;
        if (response && response.code == 200) {
          this.setState({
            filterPostData: [...oldList, ...newList],
            hasMore: true,
            paginationLoader: false,
          });
        } else if (response && response.code == 204) {
          this.setState({
            hasMore: false,
            filterPostData: oldList,
            paginationLoader: false,
          });
        } else {
          return rej();
        }
        this.props.dispatch(setApiLoading(false));
        res();
      } catch (err) {
        this.setState({
          filterPostData: [],
          hasMore: false,
          paginationLoader: false,
        });
        this.props.dispatch(setApiLoading(false));
      }
    });
  };

  /** funtion to add url parameter on addition of filters */
  handleUrlParams = (key, value) => {
    let oldRouter =
      this.props.router && !isEmpty(this.props.router.query)
        ? this.props.router.query
        : this.state.urlString
        ? this.state.urlString
        : {};

    if (value === "") {
      if (key === "loan") {
        delete oldRouter.fromLoan;
        delete oldRouter.toLoan;
      }
      delete oldRouter[key];
      Router.push(
        {
          pathname: this.props.router.pathname,
          query: { ...oldRouter },
        },
        {
          pathname: this.props.router.pathname,
          query: { ...oldRouter },
        },
        { scroll: false, shallow: true }
      );
    } else if (typeof value === "object") {
      let newRouter = { ...oldRouter, ...value };
      Router.push(
        {
          pathname: this.props.router.pathname,
          query: { ...newRouter },
        },
        {
          pathname: this.props.router.pathname,
          query: { ...newRouter },
        },
        { scroll: false, shallow: true }
      );
    } else {
      Router.push(
        {
          pathname: this.props.router.pathname,
          query: { ...oldRouter, [key]: value },
        },
        {
          pathname: this.props.router.pathname,
          query: { ...oldRouter, [key]: value },
        },
        { scroll: false, shallow: true }
      );
    }
  };

  getList = async (page = 0) => {
    return new Promise(async (res, rej) => {
      try {
        if (this.state.hasMore) {
          await this.handleCallAPI("5", page, true);
        }
        res();
      } catch (e) {
        rej();
      }
    });
  };

  render() {
    const { locale } = this.props;
    const { urlString } = this.state;
    return (
      <Context.Provider value={{ locale: locale }}>
        {this.state.materialUI ? (
          <PaginationHOC
            page={this.state.page}
            products={this.state.filterPostData}
            getItems={this.getList}
            hasMore={this.state.hasMore}
          >
            {/* Header Section */}
            <MainHeader ogTitle={"Trailers"}></MainHeader>
            <div className="App">
              {/* Navbar */}
              <Navbar />
            </div>
            <AppMenu
              value={3}
              handlePostProductsAPI={this.handlePostProductsAPI}
              handleUrlParams={this.handleUrlParams}
              urlString={urlString}
              filterPostData={this.state.filterPostData}
              paginationLoader={this.state.paginationLoader}
              TrailerData={this.props.TrailerData}
            />
            <BackToTop />
            {/* Footer */}
            <Footer />
          </PaginationHOC>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    TrailerData: state.TrailerData,
  };
};

export default connect(mapStateToProps)(withRouter(Trailers));
