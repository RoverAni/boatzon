// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const ProfessionalUserPage = dynamic(
  () => import("../containers/professional-user-page/professional-user-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getCookiees } from "../lib/session";
import { API_NY_URL, commonHeader } from "../lib/config";
import {
  getProfessionalBoatData,
  getProProfilePhotos,
} from "../services/professionals";

import "url-search-params-polyfill";

class ProfessionalsUserProfile extends React.Component {
  static async getInitialProps({ ctx }) {
    let token = getCookiees("token", ctx.req);
    let memberName = ctx.query.name;
    const getProfile = await fetch(API_NY_URL + "/businessProfile", {
      method: "get",
      headers: {
        ...commonHeader,
        token: token,
      },
    });

    const getProfileData = await getProfile.json();

    let userName = getCookiees("userName", ctx.req);
    var payload = new URLSearchParams();
    payload.append("username", `${userName}`);

    const getBusinessPost = await fetch(API_NY_URL + "/businessProfile/post", {
      method: "post",
      headers: {
        ...commonHeader,
        token: token,
      },
      body: payload,
    });

    const getBusinessPostData = await getBusinessPost.json();

    payload.append("membername", memberName);
    const dynamicBusinessProfile = await fetch(
      API_NY_URL + "/businessProfile/memberName",
      {
        method: "post",
        headers: {
          ...commonHeader,
          token: token,
        },
        body: payload,
      }
    );

    const dynamicBusinessProfileData = await dynamicBusinessProfile.json();

    return {
      getProfileData,
      getBusinessPostData,
      dynamicBusinessProfileData,
      userName,
    };
  }

  state = {
    ProfileData:
      this.props.getProfileData.code == 200
        ? this.props.getProfileData.data[0]
        : "",
    AllPost:
      this.props.getBusinessPostData.code == 200
        ? this.props.getBusinessPostData.data
        : [],
    allBoatsAndProducts: [],
    boatsPost: [],
    userName: this.props.userName,
  };

  componentDidMount() {
    let tempAllPost = [...this.state.AllPost];
    let ProductPost =
      tempAllPost &&
      tempAllPost.filter((data) => {
        return data.category == "Products";
      });

    let BoatPost =
      tempAllPost &&
      tempAllPost.filter((data) => {
        return data.category == "Boats";
      });

    this.setState({
      ProductPost,
      BoatPost,
    });

    this.handleProProfilePhoto();
    this.props.dispatch(getCurrentLocation());

    this.getAllSoldData();
    this.getAllUnsoldData();
  }

  getAllSoldData = async () => {
    let data = [...this.state.allBoatsAndProducts];
    let boats = [...this.state.boatsPost];
    let obj = {
      limit: 20,
      offset: 0,
      sold: "1",
      membername: "wolkswagen",
    };
    let res = await getProfessionalBoatData(obj);
    if (res.data.code === 200) {
      let ex = res.data.data.filter((k) => k.category === "Boats");
      boats.push(...ex);
      data.push(...res.data.data);
      this.setState({ allBoatsAndProducts: data, boatsPost: boats });
    }
  };

  getAllUnsoldData = async () => {
    let data = [...this.state.allBoatsAndProducts];
    let boats = [...this.state.boatsPost];
    let obj = {
      limit: 20,
      offset: 0,
      sold: "0",
      membername: "wolkswagen",
    };
    let res = await getProfessionalBoatData(obj);
    if (res.data.code === 200) {
      let ex = res.data.data.filter((k) => k.category === "Boats");
      boats.push(...ex);
      data.push(...res.data.data);
      this.setState({ allBoatsAndProducts: data, boatsPost: boats });
    }
  };

  handleProProfilePhoto = () => {
    let payload = {
      username: this.state.userName,
    };

    getProProfilePhotos(payload)
      .then((res) => {
        console.log("res---->", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              proUserGallery: response.data,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err---->", err);
      });
  };

  render() {
    const { locale, dynamicBusinessProfileData } = this.props;
    const { ProfileData, ProductPost, BoatPost, proUserGallery, boatsPost } =
      this.state;
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          <AppMenu />
          <ProfessionalUserPage
            ProfileData={ProfileData}
            BoatPost={BoatPost}
            ProductPost={ProductPost}
            proUserGallery={proUserGallery}
            dynamicBusinessProfileData={dynamicBusinessProfileData}
            isDynamicBusinessProfile={true}
            boatsPost={boatsPost}
          />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(ProfessionalsUserProfile);
