// Main React Components
import React from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../../hoc/Wrapper";
import dynamic from "next/dynamic";


// imported components
import MainHeader from "../../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../../containers/navbar/navbar"), {ssr: false,});
const Footer = dynamic(() => import("../../containers/footer/footer"), {ssr: false,});
const AppMenu = dynamic(() => import("../../containers/app-menu/app-menu"), {ssr: false,});
const BackToTop = dynamic(() => import("../../containers/back-to-top/back-to-top"),{ ssr: false });
const AccessibilityCommitment = dynamic(() => import("../../containers/static-pages/accessibility-commitment"),{ ssr: false });
const AboutUs = dynamic(() => import("../../containers/static-pages/about-us"),{ ssr: false });
const CaSupplyChainDisclosure = dynamic(() => import("../../containers/static-pages/ca-supply-chain-disclosure"),{ ssr: false });
const CommunityGuidelines = dynamic(() => import("../../containers/static-pages/community-guidelines"),{ ssr: false });
const FinancialPrivacyPolicy = dynamic(() => import("../../containers/static-pages/financial-privacy-policy"),{ ssr: false });
const PostingRules = dynamic(() => import("../../containers/static-pages/posting-rules"),{ ssr: false });
const PrivacyPolicy = dynamic(() => import("../../containers/static-pages/privacy-policy"),{ ssr: false });
const TermsOfService = dynamic(() => import("../../containers/static-pages/terms-of-service"),{ ssr: false });
const ShippingPolicies = dynamic(() => import("../../containers/static-pages/shipping-policies"),{ ssr: false });
const ResponsibleDisclosurePolicy = dynamic(() => import("../../containers/static-pages/responsible-disclosure-policy"),{ ssr: false });
const ProhibitedItemsGuidelines = dynamic(() => import("../../containers/static-pages/prohibited-items-guidelines"),{ ssr: false });

// redux context component
import Context from "../../context/context";

class Index extends React.Component {
  static async getInitialProps({ ctx }) {
    const { query = {} } = ctx;
    return {
      query,
    };
  }

  state = {
    tab: this.props.query.slug,
  };

  handleStaticPage = () => {
    const components = {
      "accessibility-commitment": AccessibilityCommitment,
      "about-us": AboutUs,
      "ca-supply-chain-disclosure":  CaSupplyChainDisclosure,
      "community-guidelines": CommunityGuidelines,
      "financial-privacy-policy": FinancialPrivacyPolicy,
      "posting-rules": PostingRules,
      "privacy-policy": PrivacyPolicy,
      "terms-of-service": TermsOfService,
      "shipping-policies": ShippingPolicies,
      "responsible-disclosure-policy": ResponsibleDisclosurePolicy,
      "prohibited-items-guidelines": ProhibitedItemsGuidelines
    };
    const Component = components[this.state.tab];

    return <Component />;
  };

  render() {
    const { locale } = this.props;
    console.log("jdwidj", this.state.tab);
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
              {/* Navbar */}
              <Navbar />
          </div>
          {/* Landing Page */}
          <AppMenu />
          {/* {this.handleStaticPage()} */}
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Index);
