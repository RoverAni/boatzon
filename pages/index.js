// Main React Components
import React from "react";
import { connect } from "react-redux";
// wrapping component
import Wrapper from "../hoc/Wrapper";
import dynamic from "next/dynamic";

// Custom Style Imports

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const HomePage = dynamic(() => import("../containers/home-page/home-page"), {
  ssr: false,
});

// redux context component
import Context from "../context/context";

import { getCurrentLocation } from "../redux/actions/location/location";

class Index extends React.Component {
  componentDidMount() {
    this.props.dispatch(getCurrentLocation());
  }

  render() {
    const { locale } = this.props;
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>
          {/* Landing Page */}
          <AppMenu />

          {/* Home Page */}
          <HomePage />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Index);
