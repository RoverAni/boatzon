// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const MyAppointmentsPage = dynamic(
  () => import("../containers/my-appointment-page/my-appointments-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCookie } from "../lib/session";
import { formatDate } from "../lib/date-operation/date-operation";
import moment from "moment";
import { getAppointmentData } from "../services/my-appointment";

// AMP Config
// export const config = { amp: false };

class MyAppointment extends React.Component {
  // static async getInitialProps(ctx) {
  //   const startDate = formatDate(moment().startOf("week"), "x");
  //   const endDate = formatDate(moment().endOf("week"), "x");
  //   const userId = getCookiees("mqttId", ctx.ctx.req);
  //   const type = 1; // type 1,2 if set to 1 you will get opponents user details like phone and name
  //   console.log("fnfush", startDate, endDate);
  //   const getAppointments = await fetch(
  //     API_VIDEO_URL +
  //       `/appointment?id=${userId}&type=${type}&fromDate=${startDate}&toDate=${endDate}`,
  //     {
  //       method: "get",
  //       headers: {
  //         authorization: getCookiees("token", ctx.ctx.req),
  //         lan: "en",
  //       },
  //     }
  //   );
  //   console.log("getAppointmentsData", getAppointments);
  //   const getAppointmentsData = await getAppointments.json();
  //   console.log("getAppointmentsData", getAppointmentsData);
  //   return {
  //     getAppointmentsData,
  //   };
  // }

  state = {
    openPage: false,
    // appointmentsData:
    //   this.props.getAppointmentsData &&
    //   this.props.getAppointmentsData.data &&
    //   this.props.getAppointmentsData.data.length
    //     ? this.props.getAppointmentsData.data
    //     : [],
  };

  componentDidMount() {
    const startDate = formatDate(moment().startOf("week"), "x");
    const endDate = formatDate(moment().endOf("week"), "x");
    this.handleAppointmentAPICall(startDate, endDate);
    this.setState({
      openPage: true,
    });
  }

  handleAppointmentAPICall = (startDate, endDate) => {
    let payload = {
      startDate,
      endDate,
      userId: getCookie("mqttId"),
      type: 1, // type 1,2 if set to 1 you will get opponents user details like phone and name
    };

    getAppointmentData(payload)
      .then((res) => {
        console.log("fnifj", res);
        if (res.status == 200) {
          this.setState({
            appointmentsData:
              res && res.data && res.data.data ? res.data.data : [],
          });
        }
      })
      .catch((err) => {
        console.log("fnifj", err);
      });
  };

  render() {
    console.log("fefh--startDate", formatDate(moment(), "x"));
    console.log("fefh--endDate", formatDate(moment(), "x"));

    const { locale } = this.props;
    const { openPage, appointmentsData } = this.state;

    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          <AppMenu />
          <MyAppointmentsPage
            appointmentsData={appointmentsData}
            handleAppointmentAPICall={this.handleAppointmentAPICall}
          />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    currentLocation: state.currentLocation,
    userProfileData: state.userProfileData,
  };
};

export default connect(mapStateToProps)(MyAppointment);
