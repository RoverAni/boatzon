// Main React Components
import React from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../hoc/Wrapper";
import dynamic from "next/dynamic";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const FollowingPage = dynamic(
  () => import("../containers/following-page/following-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCurrentLocation } from "../redux/actions/location/location";

// AMP Config
// export const config = { amp: false };

class Following extends React.Component {
  state = {
    materialUI: false,
  };

  componentDidMount() {
    this.setState({
      materialUI: true,
    });
    this.props.dispatch(getCurrentLocation());
  }

  render() {
    const { locale } = this.props;

    return (
      <Context.Provider value={{ locale: locale }}>
        {this.state.materialUI ? (
          <Wrapper>
            {/* Header Section */}
            <MainHeader></MainHeader>

            <div className="App">
              {/* Navbar */}
              <Navbar />
            </div>

            <AppMenu />
            <FollowingPage />
            <BackToTop />
            {/* Footer */}
            <Footer />
          </Wrapper>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Following);
