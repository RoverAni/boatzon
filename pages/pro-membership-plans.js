// Main React Components
import React from "react";
import { connect } from "react-redux";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const ProfessionalNavbar = dynamic(
  () => import("../containers/professional-navbar/professional-navbar"),
  { ssr: false }
);
const MembershipPlansPage = dynamic(
  () => import("../containers/membership-plans-page/membership-plans-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";

// AMP Config
// export const config = { amp: false };

class ProMembershipPlans extends React.Component {
  render() {
    const { locale } = this.props;

    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <ProfessionalNavbar />
          </div>

          <MembershipPlansPage />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(ProMembershipPlans);
