/*
In production the stylesheet is compiled to .next/static/style.css and served from /_next/static/style.css

You have to include it into the page using either next/head or a custom _document.js, as is being done in this file.
*/
/*eslint no-unused-vars: 0*/
import Document, { Html, Head, Main, NextScript } from "next/document";
import _JSXStyle from "styled-jsx/style";

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    let gSource = `https://maps.googleapis.com/maps/api/js?key=AIzaSyBHCU7ffm0dwNDlS2we0DnoDneZRiASlck&libraries=places`;
    // let gSource = `https://maps.googleapis.com/maps/api/js?key=AIzaSyB5Xj_33Ld1cVJeUoZzzNMkSiAto_CCZrA&libraries=places`;

    return (
      <Html>
        <Head>
          {/* BootstrapCDN */}
          {/* Bootstrap-4 CDN */}
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          />
          <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" />
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" />
          <script
            src="https://cdnjs.cloudflare.com/ajax/libs/javascript.util/0.12.12/javascript.util.min.js"
            integrity="sha512-oHBLR38hkpOtf4dW75gdfO7VhEKg2fsitvHZYHZjObc4BPKou2PGenyxA5ZJ8CCqWytBx5wpiSqwVEBy84b7tw=="
            crossOrigin="anonymous"
          ></script>
          {/* Swiper CDN */}
          <script src="/static/js/swiper.js"></script>

          <script src="https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js"></script>
          <script src="https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js"></script>

          {/* <!-- Main Quill library --> */}
          <script src="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.7/quill.min.js"></script>

          {/* <!-- Theme included stylesheets --> */}
          <link
            href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.7/quill.snow.min.css"
            rel="stylesheet"
          />
          <link
            href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.7/quill.bubble.min.css"
            rel="stylesheet"
          />
          <script src="https://js.stripe.com/v3/"></script>

          {/* <!-- Core build with no theme, formatting, non-essential modules --> */}
          <link
            href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.7/quill.core.min.css"
            rel="stylesheet"
          />
          <script src="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.7/quill.core.min.js"></script>

          {/* Google Map Js  */}
          <script type="text/javascript" src={gSource}></script>

          {/* Cropper CDN */}
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css"
          />

          <link
            href="//db.onlinewebfonts.com/c/a78cfad3beb089a6ce86d4e280fa270b?family=Calibri"
            rel="stylesheet"
            type="text/css"
          />

          <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.js"></script>
        </Head>
        <body>
          <Main />
          <NextScript />

          {/* <!-- Google Tag Manager (noscript) --> */}
          <noscript>
            <iframe
              // src="https://www.googletagmanager.com/ns.html?id=GTM-MLNCCST"
              height="0"
              width="0"
              style={{ display: "none", visibility: "hidden" }}
            ></iframe>
          </noscript>
          {/* <!-- End Google Tag Manager (noscript) --> */}
        </body>
      </Html>
    );
  }
}
