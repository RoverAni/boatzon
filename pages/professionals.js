// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import {
  postLoginProfessionalsData,
  afterLoginProfessionalsData,
  professionalSearch,
} from "../services/professionals";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getCookiees, getCookie } from "../lib/session";
import { API_NY_URL, commonHeader } from "../lib/config";
import "url-search-params-polyfill";

// AMP Config
// export const config = { amp: false };

function getProfessionalList() {
  return new Promise((resolve, reject) => {
    fetch(API_NY_URL + "/getProfessional", {
      method: "get",
      headers: { ...commonHeader },
    })
      .then((response) => {
        console.log("bfefudfde", response);
        return response.json();
      })
      .then((data) => {
        resolve(data.data);
      });
  });
}

class Professionals extends React.Component {
  static async getInitialProps({ ctx }) {
    let apiCalls = [];

    apiCalls.push(getProfessionalList());
    let [professionalListData] = await Promise.all(apiCalls);
    let token = getCookiees("token", ctx.req);
    let ProfessionalsData = "";
    let professionalData = "";
    console.log("nfefji", professionalListData);
    if (professionalListData) {
      var urlencoded = new URLSearchParams();
      urlencoded.append("specialityId", `${professionalListData[0].nodeId}`);
      if (token) {
        ProfessionalsData = await fetch(
          API_NY_URL + "/businessBySpeciality/user",
          {
            method: "post",
            headers: {
              ...commonHeader,
              token: token,
            },
            body: urlencoded,
          }
        );
      } else {
        ProfessionalsData = await fetch(
          API_NY_URL + "/businessBySpeciality/guest",
          {
            method: "post",
            headers: {
              ...commonHeader,
            },
            body: urlencoded,
          }
        );
      }
      console.log("fegeg", ProfessionalsData);
      professionalData = await ProfessionalsData.json();
    }

    return {
      professionalListData,
      professionalData,
    };
  }

  state = {
    ProfessionalsList: this.props.professionalListData,
    ProfessionalsData: this.props.professionalData.data,
    ClonedData:
      this.props.professionalData && this.props.professionalData.data
        ? [...this.props.professionalData.data]
        : [],
    distance: "",
    obj: {
      inputText: "",
      distanceMax: "",
    },
  };

  handler = (event) => {
    let inputControl = event.target;
    let tempPayload = { ...this.state.inputpayload };
    tempPayload[inputControl.name] = inputControl.value;
    this.setState({
      obj: { ...tempPayload },
    });
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      AuthPass,
    });
    this.props.dispatch(getCurrentLocation());
  }

  resetData = () => this.setState({ ProfessionalsData: this.state.ClonedData });

  ProfessionalsDataAPI = (payload) => {
    if (this.state.AuthPass) {
      afterLoginProfessionalsData(payload)
        .then((res) => {
          this.setState({
            ProfessionalsData: res.data.data,
          });
        })
        .catch((err) => {
          console.log("err-->", err);
        });
    } else {
      postLoginProfessionalsData(payload)
        .then((res) => {
          this.setState({
            ProfessionalsData: res.data.data,
          });
        })
        .catch((err) => {
          console.log("err-->", err);
        });
    }
  };

  /** filter on search + on accordions */
  professionalSearchByInputText = (speciality = "", subSpeciality = "") => {
    let { inputText, distanceMax } = this.state.obj;
    let { latitude, longitude } = this.props.currentLocation;
    let obj = {};
    obj["longitude"] = longitude;
    obj["latitude"] = latitude;
    obj["distance"] = distanceMax;
    obj["search"] = inputText;
    obj["skip"] = "0";
    obj["limit"] = "20";
    obj["speciality"] = speciality;
    obj["subSpeciality"] = subSpeciality;
    professionalSearch(obj)
      .then((res) => {
        if (res.data.message.length > 0) {
          console.log("[professionalSearch] res", res.data.message);
          this.setState({ professionalSearchData: res.data.message });
          this.setUpdatedProfessionalData(res.data.message);
        } else {
          this.setState({ ProfessionalsData: [] });
        }
      })
      .catch((err) => {
        console.log("[professionalSearch] err", err);
      });
  };

  setUpdatedProfessionalData = (data) =>
    this.setState({ ProfessionalsData: data });

  render() {
    console.log("obj", this.state.obj);
    const { locale } = this.props;
    const { ProfessionalsList, ProfessionalsData } = this.state;

    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader ogTitle={"Professionals"}></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          <AppMenu
            value={5}
            ProfessionalsDataAPI={this.ProfessionalsDataAPI}
            resetData={this.resetData}
            ProfessionalsList={ProfessionalsList}
            ProfessionalsData={ProfessionalsData}
            setUpdatedProfessionalData={this.setUpdatedProfessionalData}
            professionalSearchByInputText={this.professionalSearchByInputText}
            handler={this.handler}
            obj={this.state.obj}
          />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Professionals);
