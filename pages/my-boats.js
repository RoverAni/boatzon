// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { commonHeader, API_NY_URL } from "../lib/config";
import { getCookiees } from "../lib/session";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getManufactures } from "../services/manufacturer";
import { setApiLoading, setReduxState } from "../redux/actions/auth";

// AMP Config
// export const config = { amp: false };

class Myboats extends React.Component {
  static async getInitialProps(ctx) {
    const getManufacturer = await fetch(API_NY_URL + "/followManufactor", {
      method: "get",
      headers: {
        ...commonHeader,
        token: getCookiees("token", ctx.ctx.req),
      },
    });
    const getManufacturerData = await getManufacturer.json();

    return {
      getManufacturerData,
    };
  }

  state = {
    allManufacturersList: [],
    total_count: "",
  };

  componentDidMount() {
    this.props.dispatch(getCurrentLocation());
  }

  // Async Boat Manufacturer List API call
  handleAllGetManufacturerList = (
    limit = 10,
    page,
    loader = false,
    search,
    paginationLoader = false
  ) => {
    return new Promise(async (res, rej) => {
      try {
        let payload = {
          searchKey: search,
          limit: limit,
          offset: page * limit,
          type: "0", // 0-all manufactureres,1-boats and products,2,engines,3-trailers,4-popular manufactures
        };
        this.props.dispatch(setApiLoading(loader));
        this.props.dispatch(
          setReduxState("paginationLoader", paginationLoader)
        );
        let result = await getManufactures(payload);
        console.log("djwidj", result);
        let response = result.data;
        let oldPayload = [...this.state.allManufacturersList];
        let newPayload =
          response && response.data && response.data.length > 0
            ? response.data
            : [];
        if (response.code == 200) {
          this.setState({
            allManufacturersList: search
              ? [...newPayload]
              : [...oldPayload, ...newPayload],
            total_count: response && response.count ? response.count : 0,
          });
        }

        if (response.code == 204) {
          this.props.dispatch(setApiLoading(false));
          this.props.dispatch(setReduxState("paginationLoader", false));
          this.setState({
            allManufacturersList: search
              ? [...newPayload]
              : [...oldPayload, ...newPayload],
            total_count: response && response.count ? response.count : 0,
          });
          return rej();
        }
        this.props.dispatch(setApiLoading(false));
        this.props.dispatch(setReduxState("paginationLoader", false));
        res();
      } catch (err) {
        this.setState({
          allManufacturersList: [],
          total_count: "",
        });
        this.props.dispatch(setApiLoading(false));
        this.props.dispatch(setReduxState("paginationLoader", false));
      }
    });
  };

  render() {
    const { locale, currentLocation } = this.props;
    const { allManufacturersList, total_count } = this.state;
    console.log("dwidj", this.props.getManufacturerData);
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader ogTitle={"My Boats"}></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          <AppMenu
            value={0}
            getManufacturerData={
              this.props.getManufacturerData &&
              this.props.getManufacturerData.data
                ? this.props.getManufacturerData.data
                : []
            }
            allManufacturersList={allManufacturersList}
            handleAllGetManufacturerList={this.handleAllGetManufacturerList}
            currentLocation={currentLocation}
            total_count={total_count}
          />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(Myboats);
