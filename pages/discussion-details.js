// Main React Components
import React from "react";
import { connect } from "react-redux";

// wrapping component
import Wrapper from "../hoc/Wrapper";
import dynamic from "next/dynamic";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const DiscussionDetailPage = dynamic(
  () => import("../containers/discussion-detail-page/discussion-detail-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCurrentLocation } from "../redux/actions/location/location";
import {
  getDiscussionDetails,
  getDiscussionsPostLogin,
} from "../services/discussions";
import { getCookie } from "../lib/session";
import { followDiscussion, unfollowDiscussion } from "../services/following";
import {
  getSpecilities,
  setDiscussionComments,
} from "../redux/actions/dicussion/discussion";

// AMP Config
// export const config = { amp: false };

class DiscussionDetails extends React.Component {
  static async getInitialProps({ ctx }) {
    let discussion_Id = ctx.query.d_id;

    return {
      discussion_Id,
    };
  }

  state = {
    openPage: false,
  };

  componentDidMount() {
    this.props.dispatch(getCurrentLocation());
    this.props.dispatch(getSpecilities());
    let AuthPass = getCookie("authPass");
    let uid = getCookie("uid");
    this.setState(
      {
        AuthPass,
        uid: parseInt(uid, 10),
      },
      this.handleGetDiscussionDetail()
    );
  }

  handleGetDiscussionDetail = () => {
    let payload = {
      id: this.props.discussion_Id.toString(),
    };
    getDiscussionDetails(payload)
      .then((res) => {
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState(
              {
                discussionDetailsData: res.data.data[0],
              },
              () => {
                this.props.dispatch(
                  setDiscussionComments(
                    this.state.discussionDetailsData &&
                      this.state.discussionDetailsData.comment
                  )
                );
                this.handleRelatedDiscussion();
                this.handleMoreDiscussion();
                let followStatus =
                  this.state.discussionDetailsData &&
                  this.state.discussionDetailsData.followers &&
                  this.state.discussionDetailsData.followers.length > 0 &&
                  this.state.discussionDetailsData.followers.includes(
                    this.state.uid
                  );

                this.setState({
                  followStatus,
                });
              }
            );
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleRelatedDiscussion = () => {
    let payload = {
      type: this.state.discussionDetailsData.type,
      categoryId: this.state.discussionDetailsData.categoryId,
      discussionId: this.props.discussion_Id.toString(),
    };
    if (this.state.AuthPass) {
      getDiscussionsPostLogin(payload)
        .then((res) => {
          let response = res.data;
          if (response) {
            if (response.code == 200) {
              this.setState({
                relatedDiscussionsData: res.data.message,
              });
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
        });
    } else {
      // Login Model
    }
  };

  handleMoreDiscussion = () => {
    let payload = {
      type: this.state.discussionDetailsData.type,
      discussionId: this.props.discussion_Id.toString(),
    };
    if (this.state.AuthPass) {
      getDiscussionsPostLogin(payload)
        .then((res) => {
          let response = res.data;
          if (response) {
            if (response.code == 200) {
              this.setState(
                {
                  MoreDiscussionsData: res.data.message,
                },
                () => {
                  this.setState({
                    openPage: true,
                  });
                }
              );
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
        });
    } else {
      // Login Model
    }
  };

  handleFollowDiscussion = (discussionId) => {
    let payload = {
      discussionId: discussionId.toString(),
    };

    followDiscussion(payload)
      .then((res) => {
        console.log("resFollow", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              followStatus: true,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  handleUnfollowDiscussion = (discussionId) => {
    let payload = {
      discussionId: discussionId.toString(),
    };

    unfollowDiscussion(payload)
      .then((res) => {
        console.log("resFollow", res.data);
        let response = res.data;
        if (response) {
          if (response.code == 200) {
            this.setState({
              followStatus: false,
            });
          }
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  render() {
    const { locale, discussionCommentsData } = this.props;
    const {
      discussionDetailsData,
      relatedDiscussionsData,
      MoreDiscussionsData,
      openPage,
      followStatus,
    } = this.state;
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>

          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          <AppMenu />
          <DiscussionDetailPage
            followStatus={followStatus}
            openPage={openPage}
            discussionDetailsData={discussionDetailsData}
            relatedDiscussionsData={relatedDiscussionsData}
            MoreDiscussionsData={MoreDiscussionsData}
            discussionCommentsData={discussionCommentsData}
            handleFollowDiscussion={this.handleFollowDiscussion}
            handleUnfollowDiscussion={this.handleUnfollowDiscussion}
          />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    discussionCommentsData: state.discussionCommentsData,
  };
};

export default connect(mapStateToProps)(DiscussionDetails);
