// Main React Components
import React from "react";
import { connect } from "react-redux";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const ResetPassword = dynamic(
  () => import("../containers/auth-modals/reset-password/reset-password"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";

// AMP Config
// export const config = { amp: false };

class Reset extends React.Component {
  state = {
    loader: true,
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        loader: false,
      });
    }, 300);
  }

  render() {
    const { locale } = this.props;
    const { loader } = this.state;

    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>
          <ResetPassword />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Reset);
