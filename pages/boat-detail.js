// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const BoatDetails = dynamic(
  () => import("../containers/boat-details/boat-details"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";

import { API_NY_URL, commonHeader } from "../lib/config";
import { getCookiees } from "../lib/session";
import { getCookie } from "../lib/session";
import { storeAllChatDataInit } from "../redux/actions/chat/chat";
// AMP Config
// export const config = { amp: false };

import "url-search-params-polyfill";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getUserData } from "../redux/actions/user-data/user-data";

class BoatDetail extends React.Component {
  static async getInitialProps({ ctx }) {
    let postId = await ctx.query.pid;
    let token = getCookiees("token", ctx.req);
    let postBoatDetails = "";

    var urlencoded = new URLSearchParams();
    urlencoded.append("postId", `${postId}`);

    if (token) {
      postBoatDetails = await fetch(API_NY_URL + "/getPostsById/users", {
        method: "post",
        headers: {
          ...commonHeader,
          token: token,
        },
        body: urlencoded,
      });
    } else {
      postBoatDetails = await fetch(API_NY_URL + "/getPostsById/guests", {
        method: "post",
        headers: {
          ...commonHeader,
        },
        body: urlencoded,
      });
    }
    const postBoatDetailsData = await postBoatDetails.json();
    console.log("fjwij", postBoatDetailsData);
    return {
      postBoatDetailsData,
    };
  }

  state = {
    boatDetail: this.props.postBoatDetailsData.data
      ? this.props.postBoatDetailsData.data[0]
      : "",
    openPage: false,
    currentPage: 0,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      openPage: true,
    });
    if (AuthPass) {
      this.props.dispatch(getUserData());
      if (this.props.allChats && this.props.allChats.length === 0) {
        this.props.dispatch(storeAllChatDataInit(this.state.currentPage));
      }
    }
    this.props.dispatch(getCurrentLocation());
  }

  render() {
    const { locale } = this.props;
    const { boatDetail, openPage } = this.state;
    const productSeo = boatDetail ? boatDetail.productName : "";
    return (
      <Context.Provider value={{ locale: locale }}>
        {openPage ? (
          <Wrapper>
            {/* Header Section */}
            <MainHeader ogTitle={productSeo ? productSeo : ""}></MainHeader>

            <div className="App">
              {/* Navbar */}
              <Navbar />
            </div>

            <AppMenu />
            {/* Post Details */}
            <BoatDetails boatDetail={boatDetail} />
            <BackToTop />
            {/* Footer */}
            <Footer />
          </Wrapper>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    allChats: state.allChats,
  };
};

export default connect(mapStateToProps)(BoatDetail);
