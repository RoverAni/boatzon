// Main React Components
import React from "react";
import { connect } from "react-redux";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader.jsx";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const UserProfilePage = dynamic(
  () => import("../containers/user-profile-page/user-profile-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { API_NY_URL, AuthPassword } from "../lib/config";
import { getCookiees } from "../lib/session";

// AMP Config
// export const config = { amp: false };

class UserProfile extends React.Component {
  static async getInitialProps(ctx) {
    let data = {
      limit: "20",
      offset: "0",
    };
    const getOwnPosts = await fetch(API_NY_URL + "/profile/posts", {
      method: "post",
      headers: {
        authorization:
          "Basic " + new Buffer("basicAuth:" + AuthPassword).toString("base64"),
        token: getCookiees("token", ctx.ctx.req),
      },
      body: JSON.stringify(data),
    });

    const getOwnPostsData = await getOwnPosts.json();
    // console.log("getOwnPostsData", getOwnPostsData);
    return {
      getOwnPostsData: getOwnPostsData.data,
    };
  }
  state = {
    loader: true,
    ownBoatsPost:
      this.props.getOwnPostsData && this.props.getOwnPostsData.length > 0
        ? this.props.getOwnPostsData.filter((k) => k.category == "Boats")
        : [],
    ownOroductsPost:
      this.props.getOwnPostsData && this.props.getOwnPostsData.length > 0
        ? this.props.getOwnPostsData.filter((k) => k.category == "Products")
        : [],
    allProductsAndBoats: this.props.getOwnPostsData,
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        loader: false,
      });
    }, 300);
  }

  render() {
    const { locale, userProfileInfo } = this.props;
    const { loader, ownBoatsPost, ownOroductsPost } = this.state;
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>
          <AppMenu />
          <UserProfilePage
            userProfileInfo={userProfileInfo}
            ownBoatsPost={ownBoatsPost}
            ownProductsPost={ownOroductsPost}
          />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userProfileInfo: state.userProfileData,
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(UserProfile);
