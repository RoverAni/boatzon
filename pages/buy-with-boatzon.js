// Main React Components
import React from "react";
import fetch from "isomorphic-unfetch";
import Router, { withRouter } from "next/router";
import dynamic from "next/dynamic";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const BuyWithBoatzonPage = dynamic(
  () => import("../containers/buy-with-boatzon-page/buy-with-boatzon-page"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { connect } from "react-redux";

// redux actions
import { getCurrentLocation } from "../redux/actions/location/location";
import {
  getBoatsSubCategoryData,
  getEngineData,
} from "../redux/actions/sub-categories/sub-categories";

// asstes and colors
import { API_NY_URL, commonHeader } from "../lib/config";
import { getCookiees, getCookie } from "../lib/session";

// URLSearchParams package
import "url-search-params-polyfill";
import Wrapper from "../hoc/Wrapper";

// AMP Config
// export const config = { amp: false };

class BuyWithBoatzon extends React.Component {
  static async getInitialProps({ ctx }) {
    let postId = await ctx.query.pid;
    let token = getCookiees("token", ctx.req);
    let mqttId = getCookiees("mqttId", ctx.req);

    let postBoatDetails = "";
    let buyBoatDetails = "";
    let getUserProfile = "";

    var urlencoded = new URLSearchParams();
    urlencoded.append("postId", `${postId}`);
    if (token) {
      postBoatDetails = await fetch(API_NY_URL + "/getPostsById/users", {
        method: "post",
        headers: {
          ...commonHeader,
          token: token,
        },
        body: urlencoded,
      });

      buyBoatDetails = await fetch(
        API_NY_URL + `/userInsurance?userId=${mqttId}&productId=${postId}`,
        {
          method: "get",
          headers: {
            ...commonHeader,
            token: token,
          },
        }
      );

      getUserProfile = await fetch(API_NY_URL + "/editProfile", {
        method: "post",
        headers: {
          ...commonHeader,
          token: token,
        },
        body: {},
      });
    } else {
      postBoatDetails = await fetch(API_NY_URL + "/getPostsById/guests", {
        method: "post",
        headers: {
          ...commonHeader,
        },
        body: urlencoded,
      });
    }
    console.log("buyBoatDetails", postBoatDetails);
    console.log("buyBoatDetails", buyBoatDetails);
    console.log("getUserProfile", getUserProfile);
    const postBoatDetailsData = await postBoatDetails.json();
    const buyBoatDetailsData = await buyBoatDetails.json();
    const getUserProfileData = await getUserProfile.json();
    return {
      postBoatDetailsData,
      buyBoatDetailsData,
      getUserProfileData,
      mqttId,
      postId,
    };
  }

  state = {
    materialUI: false,
    buyBoatDetailsData: {},
    userProfileData: this.props.getUserProfileData.data,
    buyBoatDetailsData:
      this.props.buyBoatDetailsData.code == 200 &&
      this.props.buyBoatDetailsData.data.length
        ? this.props.buyBoatDetailsData.data[0]
        : {},
    postBoatDetailsData: this.props.postBoatDetailsData.data
      ? this.props.postBoatDetailsData.data[0]
      : {},
    userId: this.props.mqttId || getCookie("mqttId"),
    productId: this.props.postId || this.props.router.query.pid,
  };

  componentDidMount() {
    this.setState({
      materialUI: true,
    });
    this.props.dispatch(getCurrentLocation());
    this.props.dispatch(getEngineData());
    this.props.dispatch(getBoatsSubCategoryData());
  }

  render() {
    const { locale } = this.props;
    const {
      buyBoatDetailsData,
      postBoatDetailsData,
      userProfileData,
      userId,
      productId,
    } = this.state;
    return (
      <Context.Provider value={{ locale: locale }}>
        {this.state.materialUI ? (
          <Wrapper>
            {/* Header Section */}
            <MainHeader ogTitle={"Boats"}></MainHeader>

            <div className="App">
              {/* Navbar */}
              <Navbar />
            </div>

            <AppMenu />
            <BuyWithBoatzonPage
              buyBoatDetailsData={buyBoatDetailsData}
              userProfileData={userProfileData}
              postBoatDetailsData={postBoatDetailsData}
              userId={userId}
              productId={productId}
            />
            <BackToTop />
            {/* Footer */}
            <Footer />
          </Wrapper>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(withRouter(BuyWithBoatzon));
