import App from "next/app";
import React from "react";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import createStore from "../redux/store";
// import { getPushToken, firebase } from "../lib/firebase-config/firebase-config";
import registerWorker from "../lib/registerWorker";
import MqttHOC from "../lib/mqtt/MqttHOC";
// import { resolve } from "url";
// import { firebaseCloudMessaging } from "../lib/fcm/webPush";
import { getCookie } from "../lib/session";
import { setToken } from "../lib/fcm/handleNotification";

import "../public/scss/App.scss";
import "../public/css/index.css";
import "../public/css/react-intl-tel-input.css";
import "../public/css/react-bootstarp-daterangepicker.css";
import "../public/css/react-picky.css";
import "../public/css/quill-image-uploader.css";
import "../public/css/antd.css";
import "../public/css/swiper.css";

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });
    }

    return {
      pageProps,
    };
  }

  componentDidMount() {
    this.handleMqttId();
    // Router.events.on("routeChangeComplete", () => {
    //   handleSlidetoTop();
    // });
  }

  handleMqttId = async () => {
    if (getCookie("mqttId")) {
      registerWorker().then(() => {
        setToken(getCookie("mqttId"));
        navigator.serviceWorker.addEventListener("message", (data) => {
          Notification.requestPermission(function (result) {
            if (result === "granted") {
              navigator.serviceWorker.ready.then(function (registration) {
                console.log("registration", registration);
                registration.showNotification(
                  data.data["firebase-messaging-msg-data"].notification.title
                    ? data.data["firebase-messaging-msg-data"].notification
                        .title
                    : "Message from Boatzon",
                  {
                    body: data.data["firebase-messaging-msg-data"].notification
                      .body,
                    vibrate: [200, 100, 200, 100, 200, 100, 200],
                    // icon: require("../static/images/app_logo.png"),
                    // image: require("../static/images/app_logo.png"),
                  }
                );
              });
            }
          });
        });
      });
    }
  };

  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <Provider store={store}>
        <MqttHOC store={store}>
          <Component {...pageProps} />
        </MqttHOC>
      </Provider>
    );
  }
}

export default withRedux(createStore)(MyApp);
// export default withRedux(createStore)(withReduxSaga({ async: true })(MyApp))
