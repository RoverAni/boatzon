// Main React Components
import React from "react";
import fetch from "isomorphic-unfetch";
import Router, { withRouter } from "next/router";
import dynamic from "next/dynamic";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { connect } from "react-redux";

// redux actions
import { getCurrentLocation } from "../redux/actions/location/location";
import {
  setFilterPost,
  getFilterPost,
} from "../redux/actions/post-data/post-data";

// reusable component
const PaginationHOC = dynamic(
  () => import("../components/pagination/paginationWithScreen"),
  { ssr: false }
);

// services function
import { filterProductsDataPostLogin } from "../services/filter-products";

// asstes and colors
import { API_NY_URL, AuthPassword, commonHeader } from "../lib/config";
import { getCookiees } from "../lib/session";
import {
  getProductsSubCategories,
  getSubSubCateogry,
  getSubSubCateogryV2,
} from "../services/categories";
// URLSearchParams package
import "url-search-params-polyfill";
import { isEmpty } from "../lib/global";
import { getProductsSubCategoryData } from "../redux/actions/sub-categories/sub-categories";

// AMP Config
export const config = { amp: false };

async function getFilteredDataWithPagination(payload, token) {
  let headers = { ...commonHeader };
  if (token !== "") {
    headers.token = token;
  }
  const data = await fetch(API_NY_URL + "/filterProduct", {
    method: "POST",
    headers,
    body: payload,
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      return [];
    });
  return data;
}

function getProfessionalList() {
  return new Promise((resolve, reject) => {
    fetch(API_NY_URL + "/getProfessional", {
      method: "get",
      headers: { ...commonHeader },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        resolve(data.data);
      });
  });
}

class Products extends React.Component {
  static async getInitialProps({ ctx }) {
    let urlString = ctx.req && ctx.req.query;
    let token = getCookiees("token", ctx.req);
    let getFilterProductData = {};
    let userName;
    if (token) {
      userName = getCookiees("userName", ctx.req);
      const payload = new URLSearchParams(); // API body payload with login
      payload.append("category", `Products`);
      payload.append("limit", `20`);
      payload.append("offset", `0`);
      payload.append("name", `${userName}`);
      console.log("dwnidjw", payload);
      getFilterProductData = await getFilteredDataWithPagination(
        payload,
        token
      );
    } else {
      const payload = new URLSearchParams(); // API body payload without login
      payload.append("category", `Products`);
      payload.append("limit", `20`);
      payload.append("offset", `0`);
      getFilterProductData = await getFilteredDataWithPagination(
        payload,
        token
      );
    }

    let apiCalls = [];

    apiCalls.push(getProfessionalList());
    let [professionalListData] = await Promise.all(apiCalls);
    let ProfessionalsData = "";
    let professionalData = "";

    if (professionalListData) {
      var urlencoded = new URLSearchParams();
      urlencoded.append("specialityId", `${professionalListData[0].nodeId}`);
      if (token) {
        ProfessionalsData = await fetch(
          API_NY_URL + "/businessBySpeciality/user",
          {
            method: "post",
            headers: {
              ...commonHeader,
              token: token,
            },
            body: urlencoded,
          }
        );
      } else {
        ProfessionalsData = await fetch(
          API_NY_URL + "/businessBySpeciality/guest",
          {
            method: "post",
            headers: {
              ...commonHeader,
            },
            body: urlencoded,
          }
        );
      }
      professionalData = await ProfessionalsData.json();
    }
    console.log("djwo", getFilterProductData);
    return {
      getFilterProductData,
      professionalListData,
      professionalData,
      token,
      userName,
      urlString,
    };
  }

  state = {
    limit: 20,
    offset: 0,
    preventAPICallOnFilter: true,
    urlString: this.props.urlString,
    ProfessionalsList: this.props.professionalListData,
    materialUI: false,
    productCategories: [],
    categoryArray: [],
  };

  componentDidMount() {
    this.setState({
      materialUI: true,
      urlString: isEmpty(this.state.urlString)
        ? this.props.router.query
        : this.state.urlString,
    });
    this.props.dispatch(getProductsSubCategoryData());
    this.props.dispatch(getCurrentLocation());
    if (this.props.getFilterProductData) {
      this.props.dispatch(setFilterPost(this.props.getFilterProductData.data));
    }
    getProductsSubCategories().then((res) => {
      let data = res.data.data;
      let result = data.map(function (el) {
        var o = Object.assign({}, el);
        o["subSubCategoryList"] = [];
        o["isOpen"] = false;
        return o;
      });
      this.setState({ productCategories: result });
    });
  }

  handlePostProductsAPI = (payload) => {
    console.log("index.js handlePostProductsAPI payload", payload);
    this.setState(
      {
        payload,
      },
      () => {
        let Payload = {
          limit: this.state.limit.toString(),
          ...this.state.payload,
        };
        this.props.dispatch(getFilterPost(Payload));
      }
    );
  };

  updateBoatsData = (newData) => {
    if (this.state.preventAPICallOnFilter) {
      this.props.dispatch(
        setFilterPost([...this.props.filterPostData, ...newData])
      );
    }
  };

  updateOffset = (newOffset) => {
    if (this.state.preventAPICallOnFilter) {
      this.setState({ offset: this.state.offset + newOffset });
    }
  };

  updateBoatsDataOnScroll = (offset) => {
    if (this.state.preventAPICallOnFilter) {
      let Payload = {
        limit: this.state.limit,
        category: "Products",
        offset: offset,
      };
      return new Promise((resolve, reject) => {
        filterProductsDataPostLogin(Payload)
          .then((res) => {
            return resolve(res.data.data);
          })
          .catch((err) => {
            return reject(err);
          });
      });
    }
  };

  productTaxonomy = (subCategory, subSubCategory) => {
    let { latitude, longitude } = this.props.currentLocation;
    let payload = {
      limit: 20,
      category: "Products",
      offset: 0,
      subCategory: subCategory,
      subSubCategory: subSubCategory,
      latitude: latitude,
      longitude: longitude,
    };
    return new Promise((resolve, reject) => {
      filterProductsDataPostLogin(payload)
        .then((res) => {
          return resolve(res.data.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  };

  handleUrlParams = (key, value) => {
    let oldRouter =
      this.props.router && !isEmpty(this.props.router.query)
        ? this.props.router.query
        : this.state.urlString
        ? this.state.urlString
        : {};
    if (typeof value === "object") {
      let newRouter = { ...oldRouter, ...value };
      Router.push({
        pathname: this.props.router.pathname,
        query: { ...newRouter },
      });
    } else {
      Router.push({
        pathname: this.props.router.pathname,
        query: { ...oldRouter, [key]: value },
      });
    }
  };

  preventAPICallOnFilterFunction = (boolean) => {
    this.setState({ preventAPICallOnFilter: boolean });
  };

  /** this will only trigger API once and populate the subSubCategories inside the main category */
  _getProductSubSubCategoriesFunc = (name, index) => {
    let existingArr = [...this.state.productCategories];
    if (this.state.productCategories[index].subSubCategoryList.length === 0) {
      getSubSubCateogry(name).then((res) => {
        existingArr[index].subSubCategoryList = res.data.data
          ? res.data.data
          : [];
        this.setState({ productCategories: existingArr });
      });
    }
    if (!this.state.productCategories[index].isOpen) {
      let checkIfOpen = existingArr.findIndex((k) => k.isOpen === true);
      if (checkIfOpen == -1) {
        existingArr[index].isOpen = true;
        this.setState({ productCategories: existingArr });
      } else {
        existingArr[checkIfOpen].isOpen = false;
        existingArr[index].isOpen = true;
        this.setState({ productCategories: existingArr });
      }
    }
  };

  appendToCategory = (name) => {
    let realIndex;
    for (let index = 0; index < this.state.productCategories.length; index++) {
      if (this.state.productCategories[index].subCategoryName === name) {
        realIndex = index;
      }
      for (
        let subIndex = 0;
        subIndex <
        this.state.productCategories[index].subSubCategoryList.length;
        subIndex++
      ) {
        if (
          this.state.productCategories[index].subSubCategoryList[subIndex]
            .subCategoryName === name
        ) {
          realIndex = index;
        }
      }
    }
    let categoryArray = [...this.state.categoryArray];
    if (
      this.state.productCategories[realIndex].isOpen === false &&
      categoryArray.length === 0
    ) {
      // console.log("%c A", "background: #F53170; color: #FFF; font-size: 18px;");
      categoryArray.push(name);
      console.log("added v1");
    } else if (
      this.state.productCategories[realIndex].isOpen &&
      categoryArray.length > 0 &&
      categoryArray.length < 2
    ) {
      // console.log("%c B", "background: #F53170; color: #FFF; font-size: 18px;");
      if (categoryArray[0] === name) {
      } else {
        console.log("added v2");
        this.productTaxonomy(categoryArray[0], name).then((res) => {
          this.props.dispatch(setFilterPost(res));
          console.log("[productTaxonomy]", res);
        });
        categoryArray.push(name);
      }
    } else if (
      !this.state.productCategories[realIndex].isOpen &&
      (categoryArray.length === 1 || categoryArray.length === 2)
    ) {
      // console.log("%c C", "background: #F53170; color: #FFF; font-size: 18px;");
      categoryArray = [];
      categoryArray.push(name);
      console.log("added v3");
    } else {
      // console.log("%c D", "background: #F53170; color: #FFF; font-size: 18px;");
      if (categoryArray[0] === name) {
        let newArr = new Array(1);
        newArr[0] = name;
        categoryArray = newArr;
        console.log("added v4");
      } else {
        console.log("added v5");
        categoryArray[1] = name;
        this.productTaxonomy(categoryArray[0], name).then((res) => {
          this.props.dispatch(setFilterPost(res));
          console.log("[productTaxonomy]", res);
        });
      }
    }
    this.setState({ categoryArray: categoryArray });
  };

  render() {
    const { locale } = this.props;
    const { urlString } = this.state;
    // console.log("categoryArray", this.state.categoryArray);
    console.log("productCategories", this.state.productCategories);
    return (
      <Context.Provider value={{ locale: locale }}>
        {this.state.materialUI ? (
          <PaginationHOC
            offset={this.state.offset}
            products={this.props && this.props.filterPostData}
            apiCall={this.updateBoatsDataOnScroll}
            updatePosts={this.updateBoatsData}
            updateOffset={this.updateOffset}
          >
            <MainHeader ogTitle={"Products"}></MainHeader>

            <div className="App">
              <Navbar />
            </div>

            <AppMenu
              value={4}
              urlString={urlString}
              isRealProductsPage={true}
              handleUrlParams={this.handleUrlParams}
              ProfessionalsList={this.state.ProfessionalsList}
              handlePostProductsAPI={this.handlePostProductsAPI}
              _productCategories={this.state.productCategories}
              _getProductSubSubCategoriesFunc={
                this._getProductSubSubCategoriesFunc
              }
              _categoryArray={this.state.categoryArray}
              _appendToCategory={this.appendToCategory}
              preventAPICallOnFilterFunction={
                this.preventAPICallOnFilterFunction
              }
            />
            <BackToTop />

            <Footer />
          </PaginationHOC>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    selectedLang: state.selectedLang,
    locale: state.locale,
    filterPostData: state.filterPostData,
  };
};

export default connect(mapStateToProps)(withRouter(Products));
