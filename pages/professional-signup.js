// Main React Components
import React from "react";
import { connect } from "react-redux";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const ProfessionalSignupPage = dynamic(
  () =>
    import("../containers/professional-signup-page/professional-signup-page"),
  { ssr: false }
);
const ProfessionalNavbar = dynamic(
  () => import("../containers/professional-navbar/professional-navbar"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getCookie } from "../lib/session";
import { getUserData } from "../redux/actions/user-data/user-data";

// AMP Config
// export const config = { amp: false };

class ProfessionalSignup extends React.Component {
  componentDidMount() {
    this.props.dispatch(getCurrentLocation());
    let AuthPass = getCookie("authPass");
    this.setState(
      {
        AuthPass,
      },
      () => {
        if (AuthPass) {
          this.props.dispatch(getUserData());
        }
      }
    );
  }

  render() {
    const { locale, currentLocation } = this.props;

    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>

          <div className="App">
            {/* Navbar */}
            <ProfessionalNavbar />
          </div>

          <ProfessionalSignupPage currentLocation={currentLocation} />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(ProfessionalSignup);
