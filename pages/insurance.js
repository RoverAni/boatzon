// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const QuoteServices = dynamic(
  () => import("../containers/services-page/quote-services"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import {
  getBoatsSubCategoryData,
  getEngineData,
} from "../redux/actions/sub-categories/sub-categories";
import { getCurrentLocation } from "../redux/actions/location/location";

import { API_NY_URL, commonHeader } from "../lib/config";
import { getCookiees } from "../lib/session";

// URLSearchParams package
import "url-search-params-polyfill";

// AMP Config
// export const config = { amp: false };

class Insurance extends React.Component {
  static async getInitialProps({ ctx }) {
    let postId = await ctx.query.pid;
    let token = getCookiees("token", ctx.req);
    let mqttId = getCookiees("mqttId", ctx.req);
    console.log("fnejf", postId, token, mqttId);

    let postBoatDetails = "";
    let buyBoatDetails = "";
    let getUserProfile = "";

    var urlencoded = new URLSearchParams();
    urlencoded.append("postId", `${postId}`);
    if (postId) {
      if (token) {
        postBoatDetails = await fetch(API_NY_URL + "/getPostsById/users", {
          method: "post",
          headers: {
            ...commonHeader,
            token: token,
          },
          body: urlencoded,
        });

        if (mqttId) {
          buyBoatDetails = await fetch(
            API_NY_URL + `/userInsurance?userId=${mqttId}&productId=${postId}`,
            {
              method: "get",
              headers: {
                ...commonHeader,
                token: token,
              },
            }
          );
        }
      } else {
        postBoatDetails = await fetch(API_NY_URL + "/getPostsById/guests", {
          method: "post",
          headers: {
            ...commonHeader,
          },
          body: urlencoded,
        });
      }
    } else {
      if (mqttId) {
        buyBoatDetails = await fetch(
          API_NY_URL + `/userInsuranceDirect?userId=${mqttId}`,
          {
            method: "get",
            headers: {
              ...commonHeader,
              token: token,
            },
          }
        );
      }
    }
    if (token) {
      getUserProfile = await fetch(API_NY_URL + "/editProfile", {
        method: "post",
        headers: {
          ...commonHeader,
          token: token,
        },
        body: {},
      });
    }
    console.log("postBoatDetails", postBoatDetails);
    console.log("buyBoatDetails", buyBoatDetails);
    console.log("getUserProfile", getUserProfile);
    const postBoatDetailsData = postId ? await postBoatDetails.json() : {};
    const buyBoatDetailsData = await buyBoatDetails.json();
    const getUserProfileData = await getUserProfile.json();
    return {
      postBoatDetailsData,
      buyBoatDetailsData,
      getUserProfileData,
      mqttId,
      postId,
    };
  }

  state = {
    materialUI: false,
    userProfileData: this.props.getUserProfileData.data,
    buyBoatDetailsData:
      this.props.buyBoatDetailsData.code == 200 &&
      this.props.buyBoatDetailsData.data.length
        ? this.props.buyBoatDetailsData.data[0]
        : {},
    postBoatDetailsData: this.props.postBoatDetailsData.data
      ? this.props.postBoatDetailsData.data[0]
      : {},
    userId: this.props.mqttId || getCookie("mqttId"),
    productId: this.props.postId,
  };

  componentDidMount() {
    this.setState({
      materialUI: true,
    });
    this.props.dispatch(getBoatsSubCategoryData());
    this.props.dispatch(getCurrentLocation());
    this.props.dispatch(getEngineData());
  }

  render() {
    const { locale } = this.props;
    const {
      buyBoatDetailsData,
      postBoatDetailsData,
      userProfileData,
      userId,
      productId,
    } = this.state;
    console.log("fwfd-->postBoatDetails", postBoatDetailsData);
    console.log("fwfd-->buyBoatDetails", buyBoatDetailsData);
    console.log("fwfd-->getUserProfile", userProfileData);

    return (
      <Context.Provider value={{ locale: locale }}>
        {this.state.materialUI ? (
          <Wrapper>
            {/* Header Section */}
            <MainHeader></MainHeader>

            <div className="App">
              {/* Navbar */}
              <Navbar />
            </div>

            <AppMenu />
            <QuoteServices
              buyBoatDetailsData={buyBoatDetailsData}
              userProfileData={userProfileData}
              postBoatDetailsData={postBoatDetailsData}
              userId={userId}
              productId={productId}
            />
            <BackToTop />
            {/* Footer */}
            <Footer />
          </Wrapper>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Insurance);
