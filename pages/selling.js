// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const SellingPage = dynamic(
  () => import("../containers/selling-page/selling-page"),
  { ssr: false }
);
const PageLoader = dynamic(() => import("../components/loader/page-loader"), {
  ssr: false,
});

// redux context component
import Context from "../context/context";
import { getCurrentLocation } from "../redux/actions/location/location";
import {
  getBoatsSubCategoryData,
  getEngineData,
  getProductsSubCategoryData,
  getTrailersData,
} from "../redux/actions/sub-categories/sub-categories";
import { getOwnPosts } from "../services/my-listing";
import Router from "next/router";

// AMP Config
// export const config = { amp: false };

class Selling extends React.Component {
  // static async getInitialProps(ctx) {
  //   let data = {
  //     limit: "20",
  //     offset: "0",
  //   };
  //   const getOwnPosts = await fetch(API_NY_URL + "/profile/posts", {
  //     method: "post",
  //     headers: {
  //       authorization:
  //         "Basic " + new Buffer("basicAuth:" + AuthPassword).toString("base64"),
  //       token: getCookiees("token", ctx.ctx.req),
  //     },
  //     body: JSON.stringify(data),
  //   });
  //   console.log("getOwnPosts", getOwnPosts);

  //   const getOwnPostsData = await getOwnPosts.json();
  //   console.log("getOwnPostsData", getOwnPostsData);
  //   return {
  //     getOwnPostsData,
  //   };
  // }

  state = {
    getOwnPostsData: [],
    openPage: false,
  };

  componentDidMount() {
    this.handleGetOwnPostData();
    this.props.dispatch(getBoatsSubCategoryData());
    this.props.dispatch(getProductsSubCategoryData());
    this.props.dispatch(getCurrentLocation());
    this.props.dispatch(getEngineData());
    this.props.dispatch(getTrailersData());
  }

  handleGetOwnPostData = () => {
    let payload = {
      limit: "20",
      offset: "0",
    };

    getOwnPosts(payload)
      .then((res) => {
        console.log("fhwf-->res", res);
        let response = res.data;
        if (response) {
          this.setState({
            getOwnPostsData: response && response.data ? response.data : [],
            openPage: true,
          });
        }
      })
      .catch((err) => {
        this.setState({
          getOwnPostsData: [],
          openPage: true,
        });
        if (err.message == "Request failed with status code 401") {
          Router.push("/");
        }
      });
  };

  render() {
    const { locale } = this.props;
    const { getOwnPostsData, openPage } = this.state;
    console.log("getOwnPostsData", this.state.getOwnPostsData);
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>
          <AppMenu />
          {openPage ? (
            <SellingPage
              openPage={openPage}
              getOwnPostsData={getOwnPostsData}
            />
          ) : (
            <div className="Loader">
              <PageLoader loading={!openPage} />
            </div>
          )}
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    currentLocation: state.currentLocation,
  };
};

export default connect(mapStateToProps)(Selling);
