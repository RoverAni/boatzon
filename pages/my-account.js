// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";
import dynamic from "next/dynamic";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
const Navbar = dynamic(() => import("../containers/navbar/navbar"), {
  ssr: false,
});
const Footer = dynamic(() => import("../containers/footer/footer"), {
  ssr: false,
});
const AppMenu = dynamic(() => import("../containers/app-menu/app-menu"), {
  ssr: false,
});
const BackToTop = dynamic(
  () => import("../containers/back-to-top/back-to-top"),
  { ssr: false }
);
const MyAccountPage = dynamic(
  () => import("../containers/my-account-page/index"),
  { ssr: false }
);

// redux context component
import Context from "../context/context";
import { getCookiees } from "../lib/session";
import { API_NY_URL, commonHeader } from "../lib/config";

// AMP Config
// export const config = { amp: false };

class Profile extends React.Component {
  static async getInitialProps({ ctx }) {
    let token = getCookiees("token", ctx.req);
    let getUserProfileData = {};

    if (token) {
      const getUserProfile = await fetch(API_NY_URL + "/editProfile", {
        method: "post",
        headers: {
          ...commonHeader,
          token: token,
        },
        body: {},
      });
      getUserProfileData = await getUserProfile.json();
    }

    console.log("fnejd", getUserProfileData);
    return {
      getUserProfileData,
    };
  }

  state = {
    userProfileData: this.props.getUserProfileData.data,
  };

  render() {
    const { locale } = this.props;
    console.log("userProfileData", this.state.userProfileData);
    return (
      <Context.Provider value={{ locale: locale }}>
        <Wrapper>
          {/* Header Section */}
          <MainHeader></MainHeader>
          <div className="App">
            {/* Navbar */}
            <Navbar />
          </div>

          {/* Landing Page */}
          <AppMenu />
          <MyAccountPage userProfileData={this.state.userProfileData} />
          <BackToTop />
          {/* Footer */}
          <Footer />
        </Wrapper>
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
  };
};

export default connect(mapStateToProps)(Profile);
