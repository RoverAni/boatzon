// Main React Components
import React from "react";
import { connect } from "react-redux";
import fetch from "isomorphic-unfetch";

// wrapping component
import Wrapper from "../hoc/Wrapper";

// imported components
import MainHeader from "../containers/headers/mainHeader";
import Navbar from "../containers/navbar/navbar";
import Footer from "../containers/footer/footer";

// redux context component
import Context from "../context/context";
import BackToTop from "../containers/back-to-top/back-to-top";
import AppMenu from "../containers/app-menu/app-menu";
import { API_NY_URL, commonHeader } from "../lib/config";
import { getCookiees } from "../lib/session";
import { getCookie } from "../lib/session";
import { storeAllChatDataInit } from "../redux/actions/chat/chat";
// AMP Config
// export const config = { amp: false };

import "url-search-params-polyfill";
import { getCurrentLocation } from "../redux/actions/location/location";
import { getUserData } from "../redux/actions/user-data/user-data";
import EngineDetails from "../containers/engine-details/engine-details";

class EngineDetail extends React.Component {
  static async getInitialProps({ ctx }) {
    let postId = await ctx.query.pid;
    let token = getCookiees("token", ctx.req);
    let postEngineDetails = "";
    if (token) {
      postEngineDetails = await fetch(
        API_NY_URL + `/engine/get?postId=${postId}`,
        {
          method: "get",
          headers: {
            ...commonHeader,
            token: token,
          },
        }
      );
    } else {
      postEngineDetails = await fetch(
        API_NY_URL + `/engine/guest?postId=${postId}`,
        {
          method: "get",
          headers: {
            ...commonHeader,
          },
        }
      );
    }

    const postEngineDetailsData = await postEngineDetails.json();
    return {
      postEngineDetailsData,
    };
  }

  state = {
    engineDetail: this.props.postEngineDetailsData?.data
      ? this.props.postEngineDetailsData.data[0]
      : "",
    openPage: false,
    currentPage: 0,
  };

  componentDidMount() {
    let AuthPass = getCookie("authPass");
    this.setState({
      openPage: true,
    });
    if (AuthPass) {
      this.props.dispatch(getUserData());
      // if (this.props.allChats && this.props.allChats.length === 0) {
      //   this.props.dispatch(storeAllChatDataInit(this.state.currentPage));
      // }
    }
    this.props.dispatch(getCurrentLocation());
  }

  render() {
    const { locale } = this.props;
    const { engineDetail, openPage } = this.state;
    const productSeo =
      (engineDetail && engineDetail.productName) ||
      (engineDetail && engineDetail.title);
    return (
      <Context.Provider value={{ locale: locale }}>
        {openPage ? (
          <Wrapper>
            {/* Header Section */}
            <MainHeader ogTitle={productSeo ? productSeo : ""}></MainHeader>
            <div className="App">
              {/* Navbar */}
              <Navbar />
            </div>

            <AppMenu />
            {/* Post Details */}
            <EngineDetails engineDetail={engineDetail} />
            <BackToTop />
            {/* Footer */}
            <Footer />
          </Wrapper>
        ) : (
          ""
        )}
      </Context.Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLang: state.selectedLang,
    locale: state.locale,
    allChats: state.allChats,
  };
};

export default connect(mapStateToProps)(EngineDetail);
