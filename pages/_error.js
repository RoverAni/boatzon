import React, { Component } from "react";
// wrapping component
import Wrapper from "../hoc/Wrapper";

class NoIndexFound extends Component {
  render() {
    return (
      <Wrapper>
        <h1>Page not found</h1>
      </Wrapper>
    );
  }
}

export default NoIndexFound;
