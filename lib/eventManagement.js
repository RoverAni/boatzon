export const attachListener = (element, event, callback) => {
  element.addEventListener(event, callback);
};

export const removeListener = (element, event, callback) => {
  element.addEventListener(event, callback);
};
