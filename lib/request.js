import axios from "axios";
import { setCookie, getCookie, removeCookie } from "./session";
import { API_NY_URL, commonHeader } from "./config";
import {
  successHandler,
  errorHandler,
  requestHandler,
} from "./api-handlers/handlers";
import { ParseToken } from "../lib/parsers/token-parser";

const API_HOST = API_NY_URL;

const getUrl = (endpoint) => API_HOST + endpoint;

// axios.interceptors.response.use(
//   response => successHandler(response),
//   error => errorHandler(error)
// )

// axios.interceptors.request.use(
//   request => requestHandler(request)
// )

export const post = async (endpoint, data) => {
  return axios.post(getUrl(endpoint), data, {
    headers: { ...commonHeader },
  });
};

export const patch = async (endpoint, data) => {
  return axios.patch(getUrl(endpoint), data, {
    headers: { ...commonHeader },
  });
};

export const get = async (endpoint, otherHeaders) => {
  axios.defaults.headers.common["token"] = await ParseToken(
    getCookie("token", "")
  );
  return axios.get(getUrl(endpoint), {
    headers: { ...commonHeader, ...otherHeaders },
  });
};

export const postWithToken = async (endpoint, data) => {
  axios.defaults.headers.common["token"] = await ParseToken(
    getCookie("token", "")
  );
  return axios.post(getUrl(endpoint), data, {
    headers: { ...commonHeader },
  });
};

export const patchWithToken = async (endpoint, data) => {
  axios.defaults.headers.common["token"] = await ParseToken(
    getCookie("token", "")
  );
  return axios.patch(getUrl(endpoint), data, {
    headers: { ...commonHeader },
  });
};

export const putWithToken = async (endpoint, data) => {
  axios.defaults.headers.common["token"] = await ParseToken(
    getCookie("token", "")
  );
  return axios.put(getUrl(endpoint), data, {
    headers: { ...commonHeader },
  });
};

export const deleteReq = async (endpoint, data) => {
  axios.defaults.headers.common["token"] = await ParseToken(
    getCookie("token", "")
  );
  return axios.delete(getUrl(endpoint), {
    headers: {
      ...commonHeader,
    },
    data: { ...data },
  });
};
