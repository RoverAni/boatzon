// const registerWorker = () => {
//     if ('serviceWorker' in navigator) {

//         // console.log("()()()", process);

//         // if (process.client) {

//             // console.log("GOT IT")
//             navigator.serviceWorker.register('/static/service-worker.js')
//                 .then(function () {
//                     console.log('Service Worker Registered');
//                 })
//                 .catch((err) => {
//                     console.log("OPOP", err)
//                     console.log(err)
//                 });
//         }
//     // }
// }

// export default registerWorker;

const registerWorker = () => {
  return new Promise((resolve, reject) => {
    if ("serviceWorker" in navigator) {
      navigator.serviceWorker
        .register("/sw.js")
        .then(function (sw) {
          console.log("Service Worker Registered", sw);
          return resolve();
        })
        .catch((err) => {
          console.log("OPOP", err);
          console.log(err);
        });
    }
  });
};

export default registerWorker;
