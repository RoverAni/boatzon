import Router from "next/router";

import { INDEX_PAGE, SEARCH_URL, PROD_LIST_PAGE } from "../../fixtures/path/path";

export const GoToHome = () => {
    Router.push(INDEX_PAGE);
}

export const GoBackURL = () => {
    Router.back();
}

export const GoToSearchPage = (searchQuery) => {
    // Router.push(`${PROD_LIST_PAGE}/${SEARCH_URL}/${searchQuery}?ent=${true}`);
    Router.push(`${PROD_LIST_PAGE}/${SEARCH_URL}/${searchQuery}?q=${searchQuery}`);
}