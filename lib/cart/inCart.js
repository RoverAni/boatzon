// Returns true if the product is already in cart, false otherwise
export const inCart = (product, cart) => {
    // let existIndex = cart ? cart.findIndex((cartItem) => cartItem._id === product || cartItem.childProductId === product || cartItem.centralProductId === product) : -1;
    let existIndex = cart ? cart.findIndex((cartItem) => cartItem._id === product || cartItem.childProductId === product || cartItem.centralProductId === product) : -1;
    return existIndex;
}


export const inCartPDP = (product, cart) => {
    // let existIndex = cart ? cart.findIndex((cartItem) => cartItem._id === product || cartItem.childProductId === product || cartItem.centralProductId === product) : -1;
    let existIndex = cart ? cart.findIndex((cartItem) => cartItem.unitId === product || cartItem.unitId === product) : -1;
    return existIndex;
}