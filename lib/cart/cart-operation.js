import createStore from '../../redux/store/store';

import { getCart, editCart } from "../../services/cart";
import { getCartAction, setCartAction, setCartProducts, addToCartAction } from "../../redux/actions/cart/cart";
import { getAllProductsFromCart } from "../../lib/product/product";
import { getUserType } from "../../lib/user-actions/getUserType";
import { getCookie } from "../session"

// const store = createStore();

export const addToCart = (event, product) => {
    event.stopPropagation();
}




const updateCart = (product, operationType, newQuantity, callback, productInCart) => {


    let finalQty = operationType == 0 ? newQuantity :
        operationType == 1 ? newQuantity + 1 : newQuantity - 1;

    let isDistributor = product.mouData && product.mouData.mouQty ? true : false;

    // isDistributor ? finalQty = product.mouData.mouQty : ''; // keep qty from api, in case of distributor ( to maintain minimum order qty )

    console.log("QTY SHOULD BE --> ", isDistributor, product.mouData, finalQty, operationType, newQuantity)


    let finalAction = finalQty == 0 ? 3 : operationType == 1 || operationType == 2 ? 2 : 2;

    let createCartPayload = {
        centralProductId: product.parentProductId,
        productId: product.childProductId,
        unitId: product.unitId,
        storeId: "0",
        // storeId: product.supplier && product.supplier.id ? product.supplier.id : "0",
        newQuantity: finalQty,
        action: productInCart > -1 ? finalAction : 1,
        offers: product.offers || {}
    };


    editCart(createCartPayload)
        .then((data) => {

            getCart()
                .then((data) => {

                    let cartData = data.data.data;
                    let cartProducts = getAllProductsFromCart(cartData); // extract products from the all stores into 1 array

                    // dispatch cart actions to get new updated cart from red
                    callback(setCartAction(cartData));
                    callback(setCartProducts(cartProducts));

                });
        })
        .catch((err) => {
            console.log("CART API --> ", err);
        })
}

const patchCartQty = (product, newQuantity, callback, productInCart) => {

    let createCartPayload = {
        centralProductId: product.parentProductId || product.centralProductId,
        productId: product._id || product.childProductId || product.centralProductId,
        unitId: product.unitId,
        // storeId: product.storeId ? product.storeId : product.supplier && product.supplier.id ? product.supplier.id : "0",
        storeId: "0",
        newQuantity: newQuantity,
        action: newQuantity == 0 ? 3 : 2,
        offers: product.offers || {}
    };

    editCart(createCartPayload)
        .then((data) => {

            getCart()
                .then((data) => {

                    let cartData = data.data.data;
                    let cartProducts = getAllProductsFromCart(cartData); // extract products from the all stores into 1 array

                    // dispatch cart actions to get new updated cart from red
                    callback(setCartAction(cartData));
                    callback(setCartProducts(cartProducts));

                });
        })
        .catch((err) => {
            console.log("CART API --> ", err);
        })
}

const createCart = (product, operationType, newQuantity, callback) => {

    let finalQty = 1;
    let isDistributor = product.mouData && product.mouData.mouQty ? true : false;
    isDistributor ? finalQty = product.mouData.mouQty : ''; // keep qty from api, in case of distributor ( to maintain minimum order qty )

    let createCartPayload = {
        centralProductId: product.parentProductId,
        productId: product.childProductId,
        unitId: product.unitId,
        userType: "1",
        // storeId: product.supplier && product.supplier.id ? product.supplier.id : "0",
        storeId: "0",
        quantity: finalQty || 1,
        cartType: 2,
        offers: product.offers || {},
        city: getCookie("ucity"),
        storeTypeId: 8
    };

    callback(addToCartAction(createCartPayload));

    console.log("Creating Cart --> ", createCartPayload)

}


// API actions values -->
// 1- add new product in already created cart 
// 2- when user increase/descrease quantity for the product which is already in cart
// 3- to delete the product from cart

export const manageUserCart = (event, operationType, product, myCart, newQuantity, callback, pageType) => {

    event ? event.stopPropagation() : '';

    let cartProducts = getAllProductsFromCart(myCart);
    let prodIndex = cartProducts && pageType == "PDP" ? cartProducts.findIndex((cartProduct) => cartProduct.unitId == product.unitId) :
        cartProducts ? cartProducts.findIndex((cartProduct) => cartProduct.centralProductId == product.parentProductId) : 0;

    // console.log("PROD  in CART -- ?", prodIndex, cartProducts, product);

    switch (operationType) {
        case 0:
            // Initial Cart Creation

            if (myCart && myCart.sellers && myCart.sellers && myCart.sellers.length > 0) {

                console.log("()()()(O editing cart")

                // if (pageType == "PDP" && prodIndex == -1) {
                //     prodIndex = 1;
                // }

                // call cart patch api to update old cart
                updateCart(product, operationType, newQuantity, callback, prodIndex);


            } else {

                // call cart post api to create fresh cart
                createCart(product, operationType, 1, callback);

            }

            break;
        case 1:
            // Increase in Quantity

            // call cart patch api to update old cart
            updateCart(product, operationType, newQuantity, callback, prodIndex);


            break;
        case 2:
            // Decrease in Quantity

            // call cart patch api to update old cart
            updateCart(product, operationType, newQuantity, callback, prodIndex);


            break;
        case 3:
            // Change Quantity from dropdown

            // call cart patch api to update cart independently
            patchCartQty(product, newQuantity, callback, prodIndex);


            break;
    }
}