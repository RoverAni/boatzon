import { QTY_UNITS } from "../config";
import { inCartPDP, inCart } from './inCart';

import { CartQtyOptions } from "../../fixtures/cart/cart";


// return cart product quantity unit based on type of user - pdp page
export const getCartProductUnitPDP = (cartProducts, isDistributor, ProductData, selectedColorVariant, selectedSizeVariant) => {

    let currentProductQtyUnit =
        isDistributor && cartProducts &&
            inCartPDP(
                selectedSizeVariant
                    ? selectedSizeVariant.unitId
                    : selectedColorVariant
                        ? selectedColorVariant.unitId
                        : ProductData.unitId,
                cartProducts
            ) >= 0
            ? cartProducts[
                inCartPDP(
                    selectedSizeVariant
                        ? selectedSizeVariant.unitId
                        : selectedColorVariant
                            ? selectedColorVariant.unitId
                            : ProductData.unitId,
                    cartProducts
                )
            ].packageType
            : QTY_UNITS;

    return currentProductQtyUnit ? currentProductQtyUnit.charAt(0).toUpperCase() + currentProductQtyUnit.slice(1) : currentProductQtyUnit;
}

// return cart product quantity unit based on type of user - any pages other than pdp
export const getCartProductUnit = (cartProducts, ProductData, isDistributor) => {
    let currentProductQtyUnitIndex = inCart(ProductData._id, cartProducts);
    let unit = isDistributor && currentProductQtyUnitIndex > -1 ? cartProducts[currentProductQtyUnitIndex].packageType : QTY_UNITS;
    return unit ? unit.charAt(0).toUpperCase() + unit.slice(1) : unit;
}

// return cart product quantity unit based on type of user
export const getCartProductQtyPDP = (cartProducts, ProductData, selectedColorVariant, selectedSizeVariant) => {

    let currentProductQty =
        cartProducts &&
            inCartPDP(
                selectedSizeVariant
                    ? selectedSizeVariant.unitId
                    : selectedColorVariant
                        ? selectedColorVariant.unitId
                        : ProductData.unitId,
                cartProducts
            ) >= 0
            ? cartProducts[
                inCartPDP(
                    selectedSizeVariant
                        ? selectedSizeVariant.unitId
                        : selectedColorVariant
                            ? selectedColorVariant.unitId
                            : ProductData.unitId,
                    cartProducts
                )
            ].quantity
            : 0;


    return currentProductQty;
}

export const getExtraQuantity = (minQty) => {
    return Array.from(new Array(minQty - 1)).map((item, index) => {
        return CartQtyOptions.length + index + 1
    })
}