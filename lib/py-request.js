import axios from "axios";
import { setCookie, getCookie, removeCookie } from './session';
import { API_PY_URL, DEFAULT_LANGUAGE } from "./config";
import { successHandler, errorHandler, requestHandler } from "./api-handlers/handlers";
import { ParseToken } from "../lib/parsers/token-parser";
import { getCurrentLanguage } from "./language/getCurrentLanguage";

const API_HOST = API_PY_URL

const getUrl = endpoint => API_HOST + endpoint;

// axios.interceptors.response.use(
//   response => successHandler(response),
//   error => errorHandler(error)
// )

// axios.interceptors.request.use(
//   request => requestHandler(request)
// )

let language = getCurrentLanguage();

const commonHeader = {
    "Content-Type": "application/json",
    "language": language,
    "currencysymbol": "INR",
    "currencycode": "INR",
    "platform": "3"
}

export const post = async (endpoint, data) => {
    return axios.post(getUrl(endpoint), data, {
        headers: commonHeader
    });
};

export const patch = async (endpoint, data) => {
    return axios.patch(getUrl(endpoint), data, {
        headers: commonHeader
    });
};

export const get = async (endpoint, extraHeaders) => {

    axios.defaults.headers.common['authorization'] = await ParseToken(getCookie('token', ''))
    return axios.get(getUrl(endpoint), {
        headers: { ...commonHeader, "language": language, ...extraHeaders }
    });
};


export const postWithToken = async (endpoint, data, extraHeaders) => {
    axios.defaults.headers.common['authorization'] = await ParseToken(getCookie('token', ''))
    return axios.post(getUrl(endpoint), data, {
        headers: { ...commonHeader, ...extraHeaders, "language": language }
    });
};

export const patchWithToken = async (endpoint, data) => {
    axios.defaults.headers.common['authorization'] = await ParseToken(getCookie('token', ''))
    return axios.patch(getUrl(endpoint), data, {
        headers: commonHeader
    });
};

export const putWithToken = async (endpoint, data) => {
    axios.defaults.headers.common['authorization'] = await ParseToken(getCookie('token', ''))
    return axios.put(getUrl(endpoint), data, {
        headers: commonHeader
    });
};

export const deleteReq = async (endpoint, data) => {
    axios.defaults.headers.common['authorization'] = await ParseToken(getCookie('token', ''))
    return axios.delete(getUrl(endpoint), { data: { ...data } }, {
        headers: {
            ...commonHeader,
            // headers
        },
        data: data
    });
};
