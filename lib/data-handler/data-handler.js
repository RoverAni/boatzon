import { setCookie, getCookie } from "../session";
import {
  AUTH_PASS,
  USER_TYPE,
  CITY,
  COUNTRY,
  LAT,
  LONG,
} from "../../lib/config";

export const setupToken = (token, withUserAuth) => {
  setCookie("token", token);
  setCookie(AUTH_PASS, true);
};

export const setupTokenOnLoad = (token) => {
  if (
    (!getCookie(AUTH_PASS) && !getCookie("token")) ||
    getCookie("token") == "undefined"
  ) {
    setCookie("token", token);
  }
};

export const setupUserId = (uid) => {
  setCookie("uid", uid);
};

export const setupFetchLocation = (location) => {
  console.log("location", location);
  setCookie("fetchLocation", true);
};

export const setupLocation = (location) => {
  setCookie("currentLocation", location);
};

export const setupBusinessUserId = (uid) => {
  setCookie("buid", uid);
};

export const setupUserName = (name) => {
  setCookie("userName", name);
};

export const setupBusinessName = (name) => {
  setCookie("businessName", name);
};

export const setupMongoId = (mongoId) => {
  setCookie("mongoId", mongoId);
};

export const setupMqttId = (mqttId) => {
  setCookie("mqttId", mqttId);
};

export const setupUserLocation = (lat, long) => {
  setCookie(LAT, lat);
  setCookie(LONG, long);
};

export const setupUserCity = (city) => {
  setCookie("ucity", city);
};

export const setupUserCountry = (country) => {
  setCookie(COUNTRY, country);
};

export const setupUserType = (uType) => {
  setCookie(USER_TYPE, uType);
};
