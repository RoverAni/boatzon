import Paho from "paho-mqtt";
import { addNewChatToSelectChat } from "../../redux/actions/chat/chat";
import { getCookie } from "../session";

const hasUndefined = (target) => {
  for (var member in target) {
    if (target[member] === undefined || target[member] === null) return true;
  }
  return false;
};

/** function to publish message when logged in user send message to target user */
export const PublishMessage = (payload, client, store) => {
  if (hasUndefined(payload)) {
    console.log("some fields are missing message cannot be sent over mqtt");
  } else {
    let message = new Paho.Message(JSON.stringify(payload));
    let topic = "Message/" + payload.to;
    message.qos = 2;
    message.destinationName = topic;
    client.send(message);
    payload.status = 1;
    store.dispatch(addNewChatToSelectChat(payload));
  }
};

/** function to publish acknowledgements when clicked on user chat */
export const PublishAcknowledgement = (payload, client, store) => {
  let topic;
  if ("senderId" in payload.obj) {
    topic = "Acknowledgement/" + payload.obj.senderId;
  } else {
    topic = "Acknowledgement/" + payload.obj.from;
  }
  let obj = {};
  obj["doc_id"] = "";
  obj["from"] = payload.obj.receiverId || payload.obj.to;
  obj["msgIds"] = [payload.obj.messageId || payload.obj.id];
  obj["secretId"] = payload.obj.secretId;
  obj["status"] = payload.newStatus;
  obj["to"] = payload.obj.senderId || payload.obj.from;
  let message = new Paho.Message(JSON.stringify(obj));
  message.qos = 2;
  message.destinationName = topic;
  message.retain = false;
  client.send(message);
};
