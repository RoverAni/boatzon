import { Subject } from "rxjs";

export const messageSubject = new Subject();
export const acknowledgementSubject = new Subject();
export const notification = new Subject();
