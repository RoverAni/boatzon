import { messageSubject, acknowledgementSubject } from "../../lib/mqtt/subjects";

export const sendMessageOverMqtt = async (payload) => {
  messageSubject.next(payload);
};

export const sendAcknowledgementsOverMqtt = async (payload) => {
  acknowledgementSubject.next(payload);
};
