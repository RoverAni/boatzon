import React, { Component } from "react";
import Paho from "paho-mqtt";
import { getCookiees, setCookie, getCookie } from "../../lib/session";
import {
  storeAllChatData,
  storeAllChatsOfSelectedUser,
  addNewChatToSelectChat,
  updateAcknowledgement,
  userSelectedToChat,
} from "../../redux/actions/chat/chat";
import { storeWebrtcData } from "../../redux/actions/webrtc/webrtc";
import { messageSubject, acknowledgementSubject } from "./subjects";
import { connect } from "react-redux";
import { PublishMessage, PublishAcknowledgement } from "./MQTTPublish";
import { sendAcknowledgementsOverMqtt } from "../../lib/mqtt/utils";
import { getChatsWithPageNo } from "../../services/chat";
import MatUIModal from "../../components/model/centerModal";
import VideoComponent from "../../components/video-call/video-call";

let client = "";

class MQTTHOC extends React.Component {
  state = {
    currentPage: 0,
  };

  callAPIToGetLatestChat = () => {
    getChatsWithPageNo(this.state.currentPage)
      .then((res) => {
        console.log("response", res.data);
      })
      .catch((err) => console.log("err", err));
  };
  constructor(props) {
    super(props);
    try {
      this.messageSubjectRxObject = messageSubject.subscribe((payload) => {
        PublishMessage(payload, client, this.props.store);
      });
      this.acknowledgementSubjectRxObject = acknowledgementSubject.subscribe((payload) => {
        PublishAcknowledgement(payload, client, this.props.store);
      });
    } catch (e) {}
  }
  onMessageArrived = (message) => {
    try {
      let payload = JSON.parse(message.payloadString);
      console.log(payload, message.topic);

      // in order not to speard chat objects again and again, it is initialized once and then CRUD will be performed
      let allChats = [...this.props.allChats];
      let checkIfExistingUser = allChats.findIndex((k) => payload.from === k.senderId || payload.from === k.recipientId);
      console.log("checkIfExistingUser", checkIfExistingUser);

      if (message.topic === getCookie("mqttId")) {
        this.props._storeWebrtcData(payload.data);
      }

      /** this case when <C> mounts and all left side data is store in AllChats [...] array */
      if (message.topic.includes("GetChats/" + getCookie("mqttId"))) {
        this.props.storeLeftSideAllChats([...this.props.allChats, ...payload.chats]);
      }

      /** this case it when logged in user clicks on left side object, in response the fetched data is stored in redux */
      if (message.topic.includes("GetMessages")) {
        let allMessages = payload.messages;
        let filterMessagesWhereStatusNotEqualToThree = allMessages.filter((k) => k.status !== 3 && k.senderId !== getCookie("mqttId"));
        /** this function will loop every message with status not equal to 3 and, publish them with status 3 */
        if (filterMessagesWhereStatusNotEqualToThree.length > 0)
          filterMessagesWhereStatusNotEqualToThree.forEach((k) => sendAcknowledgementsOverMqtt({ obj: k, newStatus: "3" }));
        this.props._storeAllChatsOfSelectedUser([...this.props.allMessagesForSelectedUser, ...payload.messages]);
      }

      /** this function is to append incoming messages */
      if (message.topic.includes(`Message/${getCookiees("mqttId")}`)) {
        if (checkIfExistingUser > -1) {
          /** this means, incoming messages are received from the user where the chat is currently active so send ack 3 else 2 */
          if (this.props.userSelectedToChat.chatId === payload.chatId) {
            sendAcknowledgementsOverMqtt({ obj: payload, newStatus: "3" });
            this.props._addNewChatToSelectChat(payload);
          } else {
            sendAcknowledgementsOverMqtt({ obj: payload, newStatus: "2" });
          }
        } else {
          /** if the index doesn't match, API will get called to get allChats with latest message updates */
          this.callAPIToGetLatestChat();
        }
        // console.log("incoming message", payload);
      }

      /** this function is to get acknowledgements */
      if (message.topic.includes(`Acknowledgement/${getCookiees("mqttId")}`)) {
        if (checkIfExistingUser > -1) {
          if (this.props.userSelectedToChat.senderId === payload.from) {
            console.log("received acknowledgement");
            this.props._updateAcknowledgementForLatestMessage(payload.msgIds[0], parseInt(payload.status));
          }
          console.log("incoming message", payload);
        }
      }

      /** this function is to get message offers  */
      if (message.topic.includes(`MessageOffer/${getCookiees("mqttId")}`)) {
        console.log("here", payload);
        let findExistingOffer = allChats.findIndex((k) => k.productId === payload.productId);
        if (findExistingOffer > -1) {
          if (this.props.userSelectedToChat.productId === payload.productId) {
            let loggedInUser = { ...this.props.userSelectedToChat };
            loggedInUser["isSold"] = payload.isSold;
            loggedInUser["offerType"] = payload.offerType;
            this.props._updateLoggedInUserObject(loggedInUser);
            this.props._addNewChatToSelectChat(payload);
          }
        } else {
          this.props.storeLeftSideAllChats([payload, ...this.props.allChats]);
        }
      }

      // if (message.topic.includes(MQTT_TOPIC.blockUser)) {
      //   userBlock.next(mqttData);
      //   console.log("block user", mqttData);
      //   return;
      // }
      // if (mqttData.topic && mqttData.topic.includes(MQTT_TOPIC.Message)) {
      //   console.log("mqtt dataaerived", mqttData); // ur data comes over here
      //   newMessageCame(this.props.store, mqttData);
      // } else {
      //   if (mqttData.chatId) {
      //     console.log("mqtt dataaerived", mqttData); // ur data comes over here
      //     ackCame(this.props.store, mqttData);
      //   } else if (mqttData.secretId) {
      //     userTypingStatus.next(mqttData);
      //   } else if (typeof mqttData.lastSeenEnabled != "undefined") {
      //     let userId = getCookie("userId");
      //     if (mqttData.status == 0 && mqttData.userId == userId) {
      //       onlineStatus(client);
      //     } else if (mqttData.userId != userId) {
      //       console.log("get chat acknolagement", mqttData);
      //       if (mqttData.status == 0) {
      //         console.log("", this.props.store.getState().chat.userList);
      //         this.props.store.getState().chat.userList[mqttData.userId]
      //           ? (userList[mqttData.userId] = setTimeout(() => {
      //               this.props.store.dispatch(changeUserStatus(mqttData));
      //             }, 3000))
      //           : this.props.store.dispatch(changeUserStatus(mqttData));
      //         console.log("get chat acknolagement offline", mqttData);
      //       } else {
      //         console.log("scjsacnjsnjcc asdsad", userList);
      //         userList[mqttData.userId] &&
      //           clearTimeout(userList[mqttData.userId]);
      //         this.props.store.dispatch(changeUserStatus(mqttData));
      //         console.log("get chat acknolagement online", mqttData);
      //       }
      //     }
      //   } else if (typeof mqttData.secretId != "undefined") {
      //   }
      //   // sendAck(this.props.store, mqttData);
      // }
    } catch (e) {
      console.log("message derived error", e);
    }
  };
  connection = () => {
    let userId = getCookiees("uid");
    console.log("userId", userId);
    if (userId) {
      let clientId = userId;
      client = new Paho.Client("wss://devmqtt.boatzon.com:2083/mqtt", clientId);

      console.log("mqtt connected [chat]", client.isConnected());

      client.connect({
        userName: "3embed",
        password: "3embed",
        useSSL: true,
        mqttVersion: 3,
        keepAliveInterval: 3,
        // willMessage: offlineStatus(),
        cleanSession: false,
        reconnect: true,
        onFailure: (e) => {
          console.log("mqtt connection fail", client.isConnected());
          this.connection();
        },

        onSuccess: (res) => {
          console.log("onSuccess", res);
          console.log("mqtt connected [chat]", client.isConnected());
          try {
            console.log("inside try block");
            client.subscribe(getCookie("mqttId"), { qos: 1 });
            client.subscribe("GetMessages/" + getCookie("mqttId"), { qos: 1 });
            client.subscribe("Messages/" + getCookie("mqttId"), { qos: 1 });
            client.subscribe("GetChats/" + getCookie("mqttId"), { qos: 1 });
            client.subscribe("MessageOffer/" + getCookie("mqttId"), { qos: 1 });
            client.subscribe("Acknowledgement/" + getCookie("mqttId"), { qos: 2 });
            client.subscribe("Calls/" + getCookie("mqttId"), { qos: 2 });
            client.subscribe("Call/" + getCookie("mqttId"), { qos: 1 });
            client.subscribe("call/" + getCookie("mqttId"), { qos: 1 });
            client.subscribe("CallsAvailability/" + getCookie("mqttId"), { qos: 2 });
            client.subscribe("Product/" + getCookie("mqttId"), { qos: 1 });
            //   { qos: 1 };
            // client.subscribe(MQTT_TOPIC.Calls + "/" + userId, { qos: 1 });
            // client.subscribe(MQTT_TOPIC.UserUpdates + "/" + userId, { qos: 1 });
            // client.subscribe(MQTT_TOPIC.FetchMessages + "/" + userId, {
            //   qos: 1,
            // });
            // client.subscribe(MQTT_TOPIC.typ + "/" + userId);
            // client.subscribe(MQTT_TOPIC.OnlineStatus + "/" + userId, {
            //   qos: 1,
            // });

            // onlineStatus(client);
            // client.subscribe(MQTT_TOPIC.OnlineStatus + "/" + userId);
            // message.destinationName = "Message/5e91625772b5bd0031ba5887";
            // client.send(message);
            // setInterval(() => {
            //   onlineStatus(client);
            // }, 5000);

            client.onMessageArrived = this.onMessageArrived;
          } catch (e) {
            console.log("mqtt connection error", e);
          }
        },
      });

      client.onConnectionLost = () => {
        setCookie("connected", "false");
      };
    }
  };

  componentDidMount() {
    this.connection();
  }

  wipeWebrtcData = () => {
    this.props.removeWebrtcData({});
  };

  render() {
    console.log("mqtt webrtc data", this.props.webrtc);
    return (
      <div>
        {this.props.children}
        <MatUIModal isOpen={this.props.webrtc && this.props.webrtc.callId} toggle={this.wipeWebrtcData} width={"80%"}>
          <VideoComponent
            UserSelectedToChat={this.props.userSelectedToChat}
            wipeWebrtcData={this.wipeWebrtcData}
            callId={this.props.webrtc && this.props.webrtc.callId}
            roomName={this.props.webrtc && this.props.webrtc.room}
            token={this.props.webrtc && this.props.webrtc.twilioToken}
            isIncomingCall={Object.keys(this.props.webrtc).length}
            webrtc={this.props.webrtc}
          />
        </MatUIModal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allChats: state.allChats,
    userSelectedToChat: state.userSelectedToChat,
    allMessagesForSelectedUser: state.allMessagesForSelectedUser,
    webrtc: state.webrtc,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _storeWebrtcData: (data) => dispatch(storeWebrtcData(data)),
    storeLeftSideAllChats: (data) => dispatch(storeAllChatData(data)), // to get left side all chat objects [{...}, {...}]
    _storeAllChatsOfSelectedUser: (obj) => dispatch(storeAllChatsOfSelectedUser(obj)), // to get selected user all existing messages
    _addNewChatToSelectChat: (obj) => dispatch(addNewChatToSelectChat(obj)), // to append new message for selected user chat,
    _updateAcknowledgementForLatestMessage: (msgId, status) => dispatch(updateAcknowledgement(msgId, status)),
    _updateLoggedInUserObject: (obj) => dispatch(userSelectedToChat(obj)),
    removeWebrtcData: (obj) => dispatch(storeWebrtcData(obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MQTTHOC);

// const publish = (type) => {
//   let publishObj = {};
//   let uid = getCookie("uid");
//   if (type === "Login") {
//     publishObj = {
//       status: 1,
//       onlineStatus: 1,
//       userId: uid,
//       _id: uid,
//       lastSeenEnabled: true,
//       lastOnline: new Date().getTime(),
//     };
//     if (hasUndefined(publishObj)) {
//       console.log("cannot publish onlineStatus on undefined");
//     } else {
//       client.publish(`OnlineStatus/${uid}`, JSON.stringify(publishObj), 0, true);
//     }
//   }
// };

// // export const sendMessageUsingMQTT = (senderId, userImage, chatId, isMatchedUser, payload, timestamp, name) => {
// export const sendMessageUsingMQTT = (obj) => {
//   if (hasUndefined(obj)) {
//     console.log("cannot send any message of sendMessageUsingMQTT on undefined");
//   } else {
//     funcToGenerateUniqueId(obj.id);
//     client.publish(`Message/${obj.to}`, JSON.stringify(obj), 1, false);
//   }
// };

// export const LogoutMQTTCalled = (userID) => {
//   let publishObj = {};
//   publishObj["status"] = 0;
//   publishObj["onlineStatus"] = 0;
//   publishObj["_id"] = userID;
//   publishObj["timestamp"] = new Date().getTime();
//   publishObj["userId"] = userID;
//   publishObj["lastSeenEnabled"] = true;
//   publishObj["lastOnline"] = new Date().getTime();
//   if (hasUndefined(publishObj)) {
//     console.log("cannot logout on undefined object LogoutMQTTCalled");
//   } else {
//     // client.unsubscribe("Message" + "/" + userID, logs);
//     // client.unsubscribe("Acknowledgement" + "/" + userID, logs);
//     // client.unsubscribe("Calls" + "/" + userID, logs);
//     // client.unsubscribe("UserUpdates" + "/" + userID, logs);
//     // client.unsubscribe("FetchMessages" + "/" + userID, logs);
//     // client.unsubscribe(userID, logs);
//     // client.publish(`OnlineStatus/${userID}`, JSON.stringify(publishObj), 0, true);
//     console.log("LogoutMQTTCalled(id)", userID);
//     setTimeout(() => {
//       // client.disconnect();
//     }, 300);
//   }
// };
