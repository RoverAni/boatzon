export const handleFirstName = (fullNameArr) => {
  switch (fullNameArr.length) {
    default:
      return fullNameArr[0];
      break;
  }
};

export const handleMiddleName = (fullNameArr) => {
  switch (fullNameArr.length) {
    case 1:
      return "";
      break;
    case 2:
      return "";
      break;
    default:
      return ` ${fullNameArr[1]}`;
      break;
  }
};

export const handleLastName = (fullNameArr) => {
  switch (fullNameArr.length) {
    case 1:
      return "";
      break;
    case 2:
      return fullNameArr[1];
      break;
    case 3:
      return fullNameArr[2];
      break;
    case 4:
      return `${fullNameArr[2]} ${fullNameArr[3]}`;
      break;
    default:
      return `${fullNameArr[2]} ${fullNameArr[3]} ${fullNameArr[4]}`;
      break;
  }
};
