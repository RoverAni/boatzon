export const APP_NAME = "Boatzon";
export const API_URL = "https://api.boatzon.com/api";
export const API_NY_URL = "https://api.boatzon.com/api";
export const API_VIDEO_URL = "https://video.boatzon.com";
export const HTTP_CHAT_URL = "http://18.219.207.76:5010";
export const STRIPE_API = "https://stripe.boatzon.com";
export const API_CHAT_URL = "https://client.boatzon.com";
export const AuthPassword =
  "&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb";
export const Privacy_Link_700CREDIT =
  "https://www.700dealer.com/QuickApp/PrivacyNotice.aspx?pnDlrid=15569";
export const Electronic_Signature_Disclosure_Link_700CREDIT =
  "https://www.700dealer.com/QuickApp/electronic_sign.html";

export const commonHeader = {
  authorization:
    "Basic " + new Buffer.from("basicAuth:" + AuthPassword).toString("base64"),
};
export const APP_LOGO = "/images/app_images/app_logo.svg";
export const APP_LOGO_WHITE = "/images/app_images/app_logo_white.svg";
export const APP_Footer_LOGO_WHITE =
  "/images/app_images/app_footer_logo.svg";
export const APP_PRO_LOGO = "/images/app_images/boatzonPro_logo.svg";
export const OG_IMAGE = "/images/app_images/main_og_logo.png";
export const SMALL_LOGO = "/images/app_images/small_logo.png";
export const ABOUT_BANNER = "/images/banner/aboutBanner.jpg";
export const ABOUT_MOBILE_IMG = "/images/banner/about-mobile-icons.png";
export const FAQ_BANNER = "/images/banner/faqBanner.png";
export const PRIVACY_BANNER = "/images/banner/termsBanner.png";
export const MOBILE_PRODUCT = "/images/dummy/MobileProduct.png";
export const PROFESSIONALS = "/images/assets/professionals.jpg";

// Social media Links
export const BZ_TWITTER_LINK = "https://twitter.com/boatzonapp";
export const BZ_FB_LINK = "https://www.facebook.com/boatzonapp";
export const BZ_INSTAGRAM_LINK = "https://www.instagram.com/boatzonapp/";
export const BZ_PINTEREST_LINK = "https://www.pinterest.com/boatzon/";

export const FOOTER_TEXT = `Copyright © 2018 ${APP_NAME} . All rights reserved`;

export const APP_BASE_COLOR = "#044404";

export const WEB_LINK = "https://stagingbz.com";
export const OG_LOGO = WEB_LINK + OG_IMAGE;

// HEAD TAG CONSTANTS - SEO
export const OG_Caption =
  "The simple and new way to buy and sell for the boating industry";
export const OG_DESC = `Boatzon gives you more ways to enjoy your boat to the fullest. With classes, events, to amazing places and more, you’re part of an entire family who is united by water.`;

// Icons
export const Search_White_Icon = "/images/icons/search_white.svg";
export const Search_Grey_Icon = "/images/icons/search_grey.svg";
export const Map_Marker = "/images/icons/map_marker_grey.svg";
export const Map_Marker_Green = "/images/icons/map_marker_green.svg";
export const Chevron_Left = "/images/icons/chevron_left.svg";
export const Chevron_Left_Blue = "/images/icons/chevron_left_blue.svg";
export const Chat_Icon_Grey = "/images/icons/chat_icon_grey.svg";
export const Chat_Icon_Green = "/images/icons/chat_icon_green.svg";
export const Chevron_Down = "/images/icons/chevron_down.svg";
export const Chevron_Down_DarkGrey =
  "/images/icons/chevron_down_darkgrey.svg";
export const Chevron_Down_WhiteNew =
  "/images/icons/chevron_down_whiteFat.svg";
export const Chevron_Down_Blue = "/images/icons/chevron_down_blue.svg";
export const Chevron_Down_White = "/images/icons/chevron_down_white.svg";
export const Profile_Logo = "/images/icons/profile_logo.svg";
export const ProfileName_Icon = "/images/icons/profileName_icon.svg";
export const Profile_Logo_Blue = "/images/icons/profile_logo_blue.svg";
export const Boat_White = "/images/icons/boat_white.svg";
export const Boat_Active = "/images/icons/boat_active.svg";
export const Boat_Inactive = "/images/icons/boat_inactive.svg";
export const Product_White = "/images/icons/product_white.svg";
export const Product_Active = "/images/icons/product_active.svg";
export const Product_Inactive = "/images/icons/product_inactive.svg";
export const Hamburger_Icon = "/images/icons/hamburger.svg";
export const Right_Arrow = "/images/icons/right_arrow.svg";
export const App_Store = "/images/icons/app_store.svg";
export const Google_Play = "/images/icons/google_play.svg";
export const Close_Icon = "/images/icons/close_icon.svg";
export const Close_Icon_Small = "/images/icons/close_icon_small.svg";
export const Close_Icon_Dark_Grey =
  "/images/icons/close_icon_dark_grey.svg";
export const Close_White_Icon = "/images/icons/close_white_icon.svg";
export const Close_Red_Icon = "/images/icons/close_icon_red.svg";
export const Email_Icon = "/images/icons/email.svg";
export const Password_Icon = "/images/icons/password.svg";
export const Password_Icon_White =
  "/images/icons/password_white_icon.svg";
export const Google_Icon = "/images/icons/google.svg";
export const Facebook_Icon = "/images/icons/facebook.svg";
export const Img_Download_Icon = "/images/icons/img_download_grey.svg";
export const Img_Download_White_Icon =
  "/images/icons/img_download_white.svg";
export const Price_Icon = "/images/icons/price_icon.svg";
export const Chevron_Right = "/images/icons/chevron_right.svg";
export const Chevron_Right_Darkgrey =
  "/images/icons/chevron_right_darkgrey.svg";
export const Chevron_Left_Darkgrey =
  "/images/icons/chevron_left_darkgrey.svg";
export const Long_Arrow_Left = "/images/icons/long_arrow_left.svg";
export const Long_Arrow_Left_LightGrey =
  "/images/icons/long_arrow_left_lightgrey.svg";
export const Long_Arrow_Left_Blue =
  "/images/icons/long_arrow_left_blue.svg";
export const Switch_On = "/images/icons/switch_on.svg";
export const Switch_Off = "/images/icons/switch_off.svg";
export const Plus_Icon = "/images/icons/plus_icon.svg";
export const Plus_Light_Icon = "/images/icons/plus_light.svg";
export const Anchor_Grey_Icon = "/images/icons/anchor_grey.svg";
export const Anchor_Blue_Icon = "/images/icons/anchor_blue.svg";
export const Boat_Front_Icon_Grey = "/images/icons/boat_grey_front.svg";
export const Boat_Front_Icon_DarkGrey =
  "/images/icons/boat_darkgrey_front.svg";
export const Boat_Front_Icon_Blue = "/images/icons/boat_blue_front.svg";
export const Discussion_Grey_Icon = "/images/icons/discussion_grey.svg";
export const Discussion_Blue_Icon = "/images/icons/discussion_blue.svg";
export const Offer_Grey_Icon = "/images/icons/offers_icon_grey.svg";
export const Offer_Blue_Icon = "/images/icons/offers_icon_blue.svg";
export const Boat_Front_Blue_Icon = "/images/icons/boat_front_blue.svg";
export const Boat_Front_Grey_Icon = "/images/icons/boat_front_grey.svg";
export const Logout_Grey_Icon = "/images/icons/logout_icon_grey.svg";
export const Logout_Blue_Icon = "/images/icons/logout_icon_blue.svg";
export const MyAcc_Grey_Icon = "/images/icons/myAcc_icon_grey.svg";
export const MyAcc_Blue_Icon = "/images/icons/myAcc_icon_blue.svg";
export const PublicProfile_Grey_Icon =
  "/images/iconsProfile_icon_grey.svg";
export const PublicProfile_Blue_Icon =
  "/images/iconsProfile_icon_blue.svg";
export const User_Blue_Icon = "/images/icons/user_icon_blue.svg";
export const Professionals_Blue_Icon =
  "/images/icons/professionals_icon_blue.svg";
export const Advice_Blue_Icon = "/images/icons/advice_icon_blue.svg";
export const Projects_Blue_Icon = "/images/icons/projects_icon_blue.svg";
export const Plus_Lightgrey_Icon = "/images/icons/plus_lightgrey.svg";
export const Minus_Grey_Icon = "/images/icons/minus_grey.svg";
export const Seller_ProfilePic = "/images/icons/seller_dp.svg";
export const Professionals_White_Icon =
  "/images/icons/professionals_icon_white.svg";
export const Inventory_Grey_Icon =
  "/images/icons/inventory_icon_grey.svg";
export const Reverse_Grey_Icon = "/images/icons/reverse_icon_grey.svg";
export const User_Grey_Icon = "/images/icons/user_icon_grey.svg";
export const User_White_Icon = "/images/icons/user_icon_white.svg";
export const Video_Grey_Icon = "/images/icons/video_icon_grey.svg";

export const CallNow = "/images/assets/call_now.png";
export const CallReqMeetingNow =
  "/images/assets/call_req_meeting_now.png";
export const CallSchDateAndTime =
  "/images/assets/call_sch_date_and_time.png";

export const Chat_White_Icon = "/images/icons/chat_icon_white.svg";
export const Mobile_White_Icon = "/images/icons/mobile_white_icon.svg";
export const Phone_White_Icon = "/images/icons/phone_icon_white.svg";
export const Dollar_White_Icon = "/images/icons/dollar_white_icon.svg";
export const Insurance_Grey_Icon =
  "/images/icons/insurance_icon_grey.svg";
export const Insurance_Dark_Grey_Icon =
  "/images/icons/insurance_icon_dark_grey.svg";

export const Insurance_White_Icon =
  "/images/icons/insurance_icon_white.svg";
export const Financing_Grey_Icon =
  "/images/icons/financing_icon_grey.svg";
export const Financing_Dark_Grey_Icon =
  "/images/icons/financing_icon_dark_grey.svg";

export const History_Grey_Icon = "/images/icons/history_icon_grey.svg";
export const Follow_Grey_Icon = "/images/icons/follow_icon_grey.svg";
export const Follow_Blue_Icon = "/images/icons/follow_icon_blue.svg";
export const Phone_Grey_Icon = "/images/icons/phone_icon_grey.svg";
export const Phone_Dark_Grey_Icon =
  "/images/icons/phone_icon_dark_grey.svg";
export const Map_Marker_Filled =
  "/images/icons/map_marker_grey_fill.svg";
export const Map_Marker_LightGrey_Filled =
  "/images/icons/map_marker_lightgrey_fill.svg";
export const Long_Arrow_Right = "/images/icons/long_arrow_right.svg";
export const Long_Arrow_Right_Green =
  "/images/icons/long_arrow_right_green.svg";

export const Back_To_Top = "/images/icons/back_to_top.svg";
export const LogIn_Icon = "/images/icons/logIn_icon.svg";
export const LogIn_Icon_White = "/images/icons/logIn_icon_white.svg";
export const SignUp_Icon = "/images/icons/signUp_icon.svg";
export const Suitecase_Grey_Icon =
  "/images/icons/suitecase_grey_icon.svg";
export const Chevron_Right_White =
  "/images/icons/chevron_right_white.svg";
export const Chevron_Left_White = "/images/icons/chevron_left_white.svg";
export const Acc_Settings_Blue = "/images/icons/acc_settings_blue.svg";
export const Acc_Settings_Grey = "/images/icons/acc_settings_grey.svg";
export const My_Sales_Blue = "/images/icons/my_sales_blue.svg";
export const My_Sales_Grey = "/images/icons/my_sales_grey.svg";
export const Transaction_History_Blue =
  "/images/icons/transaction_history_blue.svg";
export const Transaction_History_Grey =
  "/images/icons/transaction_history_grey.svg";
export const Payment_Methods_Blue =
  "/images/icons/payment_methods_blue.svg";
export const Payment_Methods_Grey =
  "/images/icons/payment_methods_grey.svg";
export const Deposit_Account_Blue =
  "/images/icons/deposit_account_blue.svg";
export const Deposit_Account_Grey =
  "/images/icons/deposit_account_grey.svg";
export const DummyProfilePic_Icon = "/images/icons/dummyProfilePic.svg";
export const Verified_Icon = "/images/icons/verified_icon.svg";
export const NotVerified_Icon = "/images/icons/notVerifies_icon.svg";
export const Password_Green = "/images/icons/password_green.svg";
export const Email_White_Icon = "/images/icons/email_white.svg";
export const View_Listing_icon = "/images/icons/view_listing_icon.svg";
export const Professional_Products_Icon =
  "/images/icons/professional_products_icon.svg";
export const Review_Icon = "/images/icons/review_icon.svg";
export const Share_Menu_Grey_Icon =
  "/images/icons/share_menu_grey_icon.svg";

export const Review_Grey_Icon = "/images/icons/review_icon_grey.svg";
export const Professional_Profile_Dp =
  "/images/icons/professional_profile_dp.svg";
export const Facebook_Blue_Icon = "/images/icons/facebook_blue_icon.svg";
export const Facebook_Blue_Icon_1 = "/images/icons/facebook_icon_1.svg";
export const Twitter_Blue_Icon = "/images/icons/twitter_blue_icon.svg";
export const Wishlist_Icon = "/images/icons/wishlist_icon.svg";
export const Problem_Icon = "/images/icons/problem_icon.svg";
export const Solution_Icon = "/images/icons/solution_icon.svg";
export const Tick_Bulletpoint_Icon =
  "/images/icons/tick_bulletpoint_icon.svg";
export const Add_Discussion_White_Icon =
  "/images/icons/add_discussion_white.svg";
export const Advice_Grey_Icon = "/images/icons/advice_grey_icon.svg";
export const Advice_White_Icon = "/images/icons/advice_white_icon.svg";
export const Advice_Blue_Icon_Tab = "/images/icons/advice_blue_icon.svg";
export const Project_Grey_Icon = "/images/icons/project_grey_icon.svg";
export const Project_White_Icon = "/images/icons/project_white_icon.svg";
export const Project_Blue_Icon_Tab =
  "/images/icons/project_blue_icon.svg";
export const User_Dp = "/images/icons/user_dp.svg";
export const Question_Grey_Icon = "/images/icons/question_icon_grey.svg";
export const Question_LightGrey_Icon =
  "/images/icons/question_icon_lightgrey.svg";
export const Question_LightBlue_Icon =
  "/images/icons/question_icon_lightblue.svg";
export const Long_Arrow_Left_White_Icon =
  "/images/icons/long_arrow_left_white.svg";
export const Long_Arrow_Right_White_Icon =
  "/images/icons/long_arrow_right_white.svg";
export const Long_Arrow_Right_Grey_Icon =
  "/images/icons/long_arrow_right_grey.svg";

export const Comment_Icon = "/images/icons/comment_icon.svg";
export const Search_Light_Grey_Icon =
  "/images/icons/search_light_grey_icon.svg";
export const SelectInput_Chevron_Down_Blue =
  "/images/icons/selectInput_chevron_down_blue.svg";
export const SelectInput_Chevron_Down_Grey =
  "/images/icons/selectInput_chevron_down_grey.svg";
export const Shop_Cart_White_Icon =
  "/images/icons/shop_cart_white_icon.svg";
export const Shipping_Blue_Icon = "/images/icons/shipping_blue_icon.svg";
export const Shipping_Green_Icon =
  "/images/icons/shipping_green_icon.svg";
export const Shipping_White_Icon =
  "/images/icons/shipping_white_icon.svg";
export const Make_Offer_White_Icon =
  "/images/icons/make_offer_white_icon.svg";
export const Make_Offer_Grey_Icon =
  "/images/icons/make_offer_grey_icon.svg";
export const Cart_White_Icon = "/images/icons/cart_white_icon.svg";
export const Cart_Grey_Icon = "/images/icons/cart_grey_icon.svg";
export const Selling_White_Icon = "/images/icons/selling_white_icon.svg";
export const Selling_Blue_Icon = "/images/icons/selling_blue_icon.svg";
export const Selling_Grey_Icon = "/images/icons/selling_grey_icon.svg";
export const Selling_Filled_Grey_Icon =
  "/images/icons/selling_filled_grey_icon.svg";
export const Selling_Filled_Blue_Icon =
  "/images/icons/selling_filled_blue_icon.svg";
export const My_Appointment_Grey_Icon =
  "/images/icons/my_appointment_grey_icon.svg";
export const My_Appointment_Blue_Icon =
  "/images/icons/my_appointment_blue_icon.svg";
export const View_Transacrtion_Blue_Icon =
  "/images/icons/view_transacrtion_blue_icon.svg";
export const Calender_Blue_Icon = "/images/icons/calender_blue_icon.svg";
export const Search_Blue_Icon = "/images/icons/search_blue_icon.svg";
export const Dollar_Symbol_Icon = "/images/icons/dollar_symbol_icon.svg";
export const Dollar_Symbol_Grey_Icon =
  "/images/icons/dollar_symbol_grey_icon.svg"; // grey 10%
export const Dollar_Icon = "/images/icons/dollar_icon.svg";
export const Selected_Checkbox_Green_Icon =
  "/images/icons/selected_checkbox_icon_green.svg";
export const Avh_Card_Icon = "/images/icons/avh_card_icon.svg";
export const Mastercard_Icon = "/images/icons/mastercard_icon.svg";
export const Add_Card_Green_Icon =
  "/images/icons/add_card_green_icon.svg";
export const Edit_Icon = "/images/icons/edit_icon.svg";
export const Edit_Icon_Grey = "/images/icons/edit_grey_icon.svg";
export const Navigation_Icon_Grey =
  "/images/icons/navigation_icon_grey.svg";
export const Navigation_Icon_Blue =
  "/images/icons/navigation_icon_blue.svg";
export const Close_Light_Grey_Icon =
  "/images/icons/close_light_grey_icon.svg";
export const Close_Light_Grey_Icon_Varient =
  "/images/icons/close_light_grey_icon_1.svg";
export const Close_Grey_Thin_Icon =
  "/images/icons/close_icon_grey_thin.svg";
export const Warning_Icon = "/images/icons/warning_icon.svg";
export const Info_Icon_Grey = "/images/icons/info_icon_grey.svg";
export const Info_Icon_Red = "/images/icons/info_icon_red.svg";

export const Loan_Financing_Icon_White =
  "/images/icons/loan_financing_icon_white.svg";
export const Selling_Green_Icon = "/images/icons/selling_green_icon.svg";
export const Buying_Blue_Icon = "/images/icons/buying_blue_icon.svg";
export const Buying_Grey_Icon = "/images/icons/buying_grey_icon.svg";
export const Archive_Grey_Icon = "/images/icons/archive_grey_icon.svg";
export const Archive_Blue_Icon = "/images/icons/archive_blue_icon.svg";
export const Notification_Green_Icon =
  "/images/icons/notification_green_icon.svg";
export const Sent_Icon_Grey = "/images/icons/sent_icon_grey.svg";
export const Sent_Single_Tick = "/images/icons/single_tick.svg";
export const Sent_Icon_Green = "/images/icons/sent_icon_green.svg";
export const My_Profile_Pic = "/images/icons/my-profile-pic.svg";
export const Popular_Icon = "/images/icons/popular_icon.svg";
export const Saved_Green_Icon = "/images/icons/saved_green_icon.svg";
export const Saved_Grey_Icon = "/images/icons/saved_grey_icon.svg";
export const Saved_Grey_Border_Icon =
  "/images/icons/saved_grey_border_icon.svg";
export const Like_Bordered_Grey_Icon =
  "/images/icons/like_bordered_grey_icon.svg";
export const Like_Filled_Grey_Icon =
  "/images/icons/like_filled_grey_icon.svg";
export const OFFER_ODD_COLOR = "#b2bdb5";
export const Saved_Blue_Icon = "/images/icons/saved_blue_icon.svg";
export const Share_Grey_Icon = "/images/icons/share_grey_icon.svg";
export const Question_Mark_Green_Icon =
  "/images/icons/question_mark_icon_green.svg";
export const Save_White_Icon = "/images/icons/save_white_icon.svg";
export const Master_Card_Grey_Icon =
  "/images/icons/master_card_grey_icon.svg";
export const Master_Card_DarkGrey_Icon =
  "/images/icons/master_card_darkgrey_icon.svg";
export const Visa_Card_Grey_Icon =
  "/images/icons/visa_card_grey_icon.svg";
export const American_Express_Card_Grey_Icon =
  "/images/icons/american_express_card_grey_icon.svg";
export const Discover_Card_Grey_Icon =
  "/images/icons/discover_card_grey_icon.svg";
export const Shipping_Protection_Icon =
  "/images/icons/shipping_protection_icon.svg";
export const Lock_Green_Icon = "/images/icons/lock_green_icon.svg";
export const PowedBy_Icon = "/images/icons/powedBy_icon.svg";
export const Chevron_Down_Green = "/images/icons/chevron_down_green.svg";
export const Stripe_Logo = "/images/icons/stripe_logo.png";
export const Checked_Darkgrey_Icon =
  "/images/icons/checked_darkgrey_icon.svg";
export const Radio_Darkgrey_Icon =
  "/images/icons/radio_darkgrey_icon.svg";
export const View_Transacrtion_Grey_Icon =
  "/images/icons/view_transacrtion_grey_icon.svg";
export const Available_Green_Icon =
  "/images/icons/available_green_icon.svg";
export const Sold_Grey_Icon = "/images/icons/sold_grey_icon.svg";
export const Archive_Blue_Filled_Icon =
  "/images/icons/achive_blue_filled_icon.svg";
export const Plus_Icon_White = "/images/icons/plus_icon_white.svg";
export const View_Grey_Filled_Icon =
  "/images/icons/view_grey_filled_icon.svg";
export const View_Grey_UnFilled_Icon =
  "/images/icons/view_grey_unfilled_icon.svg";
export const View_Green_Filled_Icon =
  "/images/icons/view_green_filled_icon.svg";
export const View_Blue_Filled_Icon =
  "/images/icons/view_blue_filled_icon.svg";
export const Hand_Grey_Icon = "/images/icons/hand_grey_icon.svg";
export const Pending_White_Icon = "/images/icons/pending_white_icon.svg";
export const Cancel_White_Icon = "/images/icons/cancel_white_icon.svg";
export const Refund_White_Icon = "/images/icons/refund_white_icon.svg";

export const Pending_Grey_Icon = "/images/icons/pending_grey_icon.svg";
export const Cancel_Grey_Icon = "/images/icons/cancel_grey_icon.svg";
export const Drop_Grey_Icon = "/images/icons/drop_grey_icon.svg";
export const Drop_White_Icon = "/images/icons/drop_white_icon.svg";
export const Refund_Grey_Icon = "/images/icons/refund_grey_icon.svg";
export const Completed_Grey_Icon =
  "/images/icons/completed_grey_icon.svg";
export const Completed_White_Icon =
  "/images/icons/completed_white_icon.svg";
export const Card1Active = "/images/icons/card1-active.svg";
export const Card1NotActive = "/images/icons/card1-notActive.svg";

export const Card2Active = "/images/icons/card2-active.svg";
export const Card2NotActive = "/images/icons/card2-notActive.svg";

export const Card3Active = "/images/icons/card3-active.svg";
export const Card3NotActive = "/images/icons/card3-notActive.svg";

export const Card4Active = "/images/icons/card4-active.svg";
export const Card4NotActive = "/images/icons/card4-notActive.svg";

export const Card5Active = "/images/icons/card5-active.svg";
export const Card5NotActive = "/images/icons/card5-notActive.svg";

export const Card6Active = "/images/icons/card6-active.svg";
export const Card6NotActive = "/images/icons/card6-notActive.svg";

export const Menu_Icon_Grey = "/images/icons/menu_icon_grey.svg";
export const Vector_Small = "/images/icons/vector_small.svg";
export const Customer_Icon = "/images/icons/customer_icon.svg";
export const Leads_To_Customers_Icon =
  "/images/icons/leads_to_customers_icon.svg";
export const Boat_Dealers_Icon = "/images/icons/boat_dealers_icon.svg";
export const Professionals_Feature_Icon =
  "/images/icons/professionals_feature_icon.svg";
export const Credit_Card_Processing_Icon =
  "/images/icons/credit_card_processing_icon.svg";
export const Boats_Products_Everywhere =
  "/images/icons/boats_products_everywhere.svg";
export const Tick_Bulletpoint_Icon_Green =
  "/images/icons/tick_bulletpoint_icon_green.svg";
export const Flag_Icon_Grey = "/images/icons/flag_icon_grey.svg";
export const Left_Bottom_Border = "/images/icons/left_bottom_border.svg";
export const Left_Top_Border = "/images/icons/left_top_border.svg";
export const Right_Bottom_Border =
  "/images/icons/right_bottom_border.svg";
export const Right_Top_Border = "/images/icons/right_top_border.svg";
export const Battery_Icon = "/images/icons/battery_icon.svg";
export const Video_Icon_White = "/images/icons/video_icon_white.svg";
export const Video_Icon_Grey1 = "/images/icons/video_icon_grey1.svg";
export const Video_Icon_Blue = "/images/icons/video_icon_blue.svg";
export const Play_Video_Icon = "/images/icons/play_video.svg";

export const calendar_blue = "/images/icons/calendar_blue.svg";
export const calendar_white = "/images/icons/calendar_white.svg";

export const reqMeetingNow = "/images/assets/reqMeetingNow.png";
export const scheduleDateAndTime =
  "/images/assets/scheduleDateAndTime.png";

export const ma_clock = "/images/icons/ma_clock.png";
export const ma_map = "/images/icons/ma_map.png";
export const ma_calendar = "/images/icons/ma_calendar.png";

export const ma_tick = "/images/icons/ma_tick.png";
export const ma_calendarV2 = "/images/icons/ma_calendarV2.png";

export const Business_Profile_Icon =
  "/images/icons/business_profile_icon.svg";
export const Professional_Type_Icon =
  "/images/icons/professional_type_icon.svg";
export const Phone_Outline_Icon_Grey =
  "/images/icons/phone_outline_icon_grey.svg";
export const Profile_Img1 = "/images/icons/profile_img1.svg";
export const Profile_Img2 = "/images/icons/profile_img2.svg";
export const Profile_Img3 = "/images/icons/profile_img3.svg";
export const City_Icon = "/images/icons/city_icon.svg";
export const ProAddress_Img1 = "/images/icons/proAddress_img1.svg";
export const Website_Icon = "/images/icons/website_icon.svg";
export const About_Icon = "/images/icons/about_icon.svg";
export const Award_Icon = "/images/icons/award_icon.svg";
export const Share_Details_Icon = "/images/icons/share_details_icon.svg";
export const Successful_SignUp = "/images/icons/successful_signUp.svg";

export const AccountInfo_Icon_Yellow =
  "/images/icons/accountInfo_icon_yellow.svg";
export const AccountInfo_Icon_White =
  "/images/icons/accountInfo_icon_white.svg";
export const BoatInfo_Icon_White =
  "/images/icons/boatInfo_icon_white.svg";
export const BoatInfo_Icon_Yellow =
  "/images/icons/boatInfo_icon_yellow.svg";
export const Insurance_Icon_Yellow =
  "/images/icons/insurance_icon_yellow.svg";
export const Insurance_Icon_White1 =
  "/images/icons/insurance_icon_white1.svg";
export const Loans_Icon_Yellow = "/images/icons/loans_icon_yellow.svg";
export const Loans_Icon_White = "/images/icons/loans_icon_white.svg";
export const Quotes_Icon_Grey = "/images/icons/quotes_icon_grey.svg";
export const Quotes_Icon_Yellow = "/images/icons/quotes_icon_yellow.svg";
export const Quote_Sale_Plan_Label =
  "/images/icons/quote_sale_plan_label.svg";
export const Camera_Icon = "/images/icons/camera_icon_grey.svg";
export const Delete_Grey_Icon = "/images/icons/delete_grey_icon.svg";
export const Delete_White_Icon = "/images/icons/delete_white_icon.svg";
export const Refresh_White_Icon = "/images/icons/refresh_white_icon.svg";
export const Inactive_Dot = "/images/icons/inactive_dot.svg";
export const Active_Dot = "/images/icons/active_dot.svg";

export const Purchase_Hamburger_Icon =
  "/images/icons/purchase_hamburger_icon.svg";
export const Cart_Dark_Grey_Icon =
  "/images/icons/cart_dark_grey_icon.svg";
export const Get_Approval_Icon = "/images/icons/get_approval_icon.svg";
export const Clock_Grey_Icon = "/images/icons/clock_icon_grey.svg";
export const Clock_Filled_Grey_Icon =
  "/images/icons/clock_filled_icon_grey.svg";
export const Globe_Grey_Icon = "/images/icons/globe_icon.svg";
export const Confirming_Loader = "/images/icons/confirming_loader.svg";

export const Radio_Button_Active =
  "/images/icons/radio_button_active.svg";
export const Radio_Button_Inactive =
  "/images/icons/radio_button_inactive.svg";
export const TestDrive = "/images/icons/testDrive.svg";
export const Make_Offer_Lightgrey_Icon =
  "/images/icons/make_offer_lightgrey_icon.svg";
export const Location_Icon_Blue = "/images/icons/location_icon_blue.svg";
export const Loation_Icon_Grey = "/images/icons/loation_icon_grey.svg";
export const Credit_Score_Meter_Icon =
  "/images/icons/credit_score_meter_icon.svg";
export const Hide_Icon_Grey = "/images/icons/hide_icon_grey.svg";
export const Video_Appointment_Icon =
  "/images/icons/video_appt_icon.svg";
export const User_Filled_Icon_Grey =
  "/images/icons/user_filled_icon_grey.svg";
export const Hamburger_Menu_Icon_Grey =
  "/images/icons/hamburger_menu_icon_grey.svg";

// DESIGN CONSTANT
export const MOBILE_SIZE = 576;
export const LARGE_TAB_SIZE = 992;
export const DEFAULT_OTP_TIMER = 60000; // 30 seconds

// API KEYS
export const FB_APP_ID = "339484586804608";

// API constants
export const Noraml_Signup_Type = 1;
export const Facebook_Signup_Type = 2;
export const Google_Signup_Type = 3;
export const Noraml_Login_Type = 1;
export const Facebook_Login_Type = 2;
export const Google_Login_Type = 3;
export const Web_deviceType = 3;

// DEFAULT Constants
export const DEFAULT_LANGUAGE = "en";
export const DEFAULT_PAGE_SIZE = 30;
export const DEFAULT_CURRENCY = "US$";
export const DEFAULT_FILTER_SHOW = 5;
export const DEFAULT_RATING = 0.1;
export const DEFAULT_USER_TYPE = 1;
export const DEFAULT_MIN_DISCOUNT = 0.9;

// DISCOUNT TYPES
export const FLAT = "FLAT";
export const PERCENTAGE = "PERCENTAGE";

// FILTERS SELECTIONS
export const SORT_BY = "Sort By:  ";
export const POPULARITY = "Popularity";
export const PRICE_L_TO_H = "Price-Low to High";
export const PRICE_H_TO_L = "Price-High to Low";

// AUTH CONSTANTS
export const AUTH_PASS = "authPass";
export const USER_TYPE = "userType";

// COOKIE CONSTANTS
export const LANG = "lang";
export const RTL = "rtl";
export const CITY = "citys";
export const COUNTRY = "country";
export const LAT = "lat";
export const LONG = "long";

// Social Media Links
export const FB_LINK = "https://www.facebook.com/5canale.com.my/";

// THEME COLORS
export const THEME_COLOR = "#378BCB";
export const LIGHT_THEME_BG = "#DEEEFA";
export const LIGHT_THEME_COLOR = "#D5F4EB";
export const BG_THEME_COLOR = "#FEEBF9";
export const THEME_COLOR_Opacity = "rgba(55, 139, 203, 0.25)";
export const THEME_COLOR_Opacity1 = "rgba(55, 139, 203, 0.1)";
export const THEME_COLOR_Opacity2 = "rgba(55, 139, 203, 0.70)";
export const Scrollbar_Thumb_COLOR = "rgb(151, 150, 150)";
export const Scrollbar_Track_COLOR = "rgb(207, 205, 205)";

// APP COLORS
export const WHITE_COLOR = "#FFFFFF";
export const BLACK_COLOR = "#000";
export const BOX_SHADOW_GREY = "rgba(0,0,0,0.15)";
export const BOX_SHADOW_GREY_1 = "rgba(0,0,0,0.1)";
export const BG_LightGREY_COLOR = "#F5F5F5"; // background color --> grey 10%
export const BG_LightGREY_COLOR_1 = "#DEDEDE"; // background color
export const BG_LightGREY_COLOR_2 = "#F1F3F5"; // background color
export const Border_LightGREY_COLOR = "#EAEAEA"; // border color --> grey 5%
export const Border_LightGREY_COLOR_1 = "#D1D4DC"; // border color
export const Border_LightGREY_COLOR_2 = "#D3D9E6"; // border color
export const FONTGREY_COLOR = "#4B5777"; // dark font grey color --> grey 60%
export const FONTGREY_COLOR_Dark = "#2A3656"; // dark font grey color --> grey 80%
export const YelloW_Color = "#FED500";
export const Light_Yellow_Color = "#FFF59F"; // Light Yellow color
export const Light_Yellow_Color_1 = "#FFFAE8"; // bg light yellow
export const GREEN_COLOR = "#27C397";
export const GREEN_COLOR_1 = "#9CE6D1";
export const GREEN_COLOR_Opacity = "rgba(39, 195, 151, 0.15)";
export const GREY_VARIANT_1 = "#717C99"; // grey 40%
export const GREY_VARIANT_2 = "#98A1B9"; // grey 20%
export const GREY_VARIANT_3 = "#D3D9E7"; // light font grey color --> grey 15%
export const GREY_VARIANT_5 = "#E4E4E4"; //
export const GREY_VARIANT_6 = "#FBFBFB"; // light bg grey color
export const GREY_VARIANT_7 = "#F8F8F8"; //
export const GREY_VARIANT_8 = "#1B243C"; // grey 100%
export const GREY_VARIANT_9 = "#F4F4F4"; //  grey less than 5%
export const GREY_VARIANT_10 = "#C0C7D9"; //  grey less than 15% new
export const GREY_VARIANT_11 = "#E4F0F5"; //  grey less than 15% new1
export const GREY_VARIANT_12 = "#E4E8F2"; //  grey less than 15% new2
export const GREY_VARIANT_13 = "#C4C4C4";
export const GREY_VARIANT_14 = "#909BB0";
export const GREY_VARIANT_15 = "#F3F3F3";
export const GREY_VARIANT_16 = "#E2E2E2";
export const GREY_VARIANT_17 = "#ECECEC";
export const Facebook_Blue = "#475993"; // facebook blue color
export const Facebook_Blue_1 = "#0776C6"; // facebook blue color varient
export const Dark_Blue_Color = "#385898"; // facebook blue color
export const Orange_Color = "#FFAB2D"; // orange color
export const Light_Blue_Color = "#00BDF2"; // light blue color
export const Red_Color = "#ED4E4E";
export const Red_Color_1 = "#FF0000";
export const DARK_GREY = "#707070";
export const GREY_VARIANT_4 = "#b0b0b0";
export const Stripe_Color = "#6772E5";
export const Yellow_Color = "#FEDB00";
export const Dark_Green_Color = "#097D57";

export const P_Color_1 = "#4EA77F";
export const P_Color_2 = "#89B661";
export const P_Color_3 = "#E9B857";
export const P_Color_4 = "#D88763";
export const P_Color_5 = "#E14B4B";
export const P_Color_6 = "#B867BA";
export const P_Color_7 = "#7D67BA";
export const P_Color_8 = "#415F98";

export const Colors_Arr = [
  P_Color_1,
  P_Color_2,
  P_Color_3,
  P_Color_4,
  P_Color_5,
  P_Color_6,
  P_Color_7,
  P_Color_8,
];

export const Dropdown_1 = "#78B851";

// APP FONTS
export const FONT_FAMILY = `"Museo-Sans-Cyrl" !important`;

export const CAT_SHOWMOBILE = "3";
export const QTY_UNITS = "Quantity";
export const FILE_UPLOAD_SIZE = 2000000; // currently in bytes - 2MB
export const IMAGE_UPLOAD_SIZE = 2000000; // currently in bytes - 2MB
export const VIDEO_UPLOAD_SIZE = 4000000; // currently in bytes - 4MB

// API TRIGGERS
export const SIGNUP_TRIGGER = "Customer Signup Verification Code";
export const RESEND_OTP_TRIGGER = "Resend Verification Code";

// FILTERS
export const COLOR_FILTER = "COLOR";
export const BRAND_FILTER = "BRAND";
export const SIZE_FILTER = "SIZE";
export const CUSTOMER_RATINGS_FILTER = "CUSTOMER RATINGS";
export const DISCOUNT_FILTER = "DISCOUNT";
export const PRICE_FILTER = "PRICE";

// FLAG VARIABLES
export const RETAILER_TYPE = 1;
export const DISTRIBUTOR_TYPE = 2;

// Navbar assets
export const Sell_Icon = "/images/assets/sell.svg";
export const Boatzon_ProfilePic = "/images/assets/boatzon_dp.svg";

// Banner assets
export const Banner_BG = "/images/assets/banner_bg.jpg";
export const Banner_HandImg = "/images/assets/banner_handphone.svg";
export const Vector_img = "/images/assets/vector.svg";
export const Loading_Page_Vector_img =
  "/images/assets/loading_page_vector.svg";

// Footer assets
export const Footer_BG = "/images/assets/footer_bg.png";
export const Play_Store_White_Icon =
  "/images/icons/play_store_white_icon.svg";
export const Apple_White_Icon = "/images/icons/apple_white_icon.svg";
export const Twitter_White_Icon = "/images/icons/twitter_white_icon.svg";
export const Facebook_White_Icon =
  "/images/icons/facebook_white_icon.svg";
export const Instagram_White_Icon = "/images/icons/insta_white_icon.svg";
export const LinkedIn_White_Icon =
  "/images/icons/linkedIn_white_icon.svg";
export const Pinterest_White_Icon =
  "/images/icons/pinterest_white_icon.svg";

// dummy Grids
export const ITEM_1_IMG = "/images/assets/grids/item_1.png";
export const ITEM_2_IMG = "/images/assets/grids/item_2.png";
export const ITEM_3_IMG = "/images/assets/grids/item_3.png";
export const ITEM_4_IMG = "/images/assets/grids/item_4.png";
export const ITEM_5_IMG = "/images/assets/grids/item_5.png";
export const ITEM_6_IMG = "/images/assets/grids/item_6.png";
export const ITEM_7_IMG = "/images/assets/grids/item_7.png";
export const ITEM_8_IMG = "/images/assets/grids/item_8.png";
export const ITEM_9_IMG = "/images/assets/grids/item_9.png";
export const ITEM_10_IMG = "/images/assets/grids/item_10.png";
export const Boat_1_IMG = "/images/assets/grids/boat_1.png";
export const Boat_2_IMG = "/images/assets/grids/boat_2.png";
export const Boat_3_IMG = "/images/assets/grids/boat_3.png";
export const Boat_4_IMG = "/images/assets/grids/boat_4.png";
export const Boat_5_IMG = "/images/assets/grids/boat_5.png";
export const Boat_6_IMG = "/images/assets/grids/boat_6.png";

export const Professionals_1_IMG =
  "/images/assets/grids/professional_1.svg";
export const Professionals_2_IMG =
  "/images/assets/grids/professional_2.svg";
export const Professionals_3_IMG =
  "/images/assets/grids/professional_3.svg";
export const Professionals_4_IMG =
  "/images/assets/grids/professional_4.svg";

// Professional-User-Page assets
export const Professional_User_Banner =
  "/images/assets/professional_user_banner.png";

//Search Dropdown assests
export const Map_Marked_Icon_Grey =
  "/images/icons/map_marked_icon_grey.svg";
export const Map_Marked_Icon_Blue =
  "/images/icons/map_marked_icon_blue.svg";
export const Globe_Icon_Grey = "/images/icons/globe_icon_grey.svg";
export const Globe_Icon_Blue = "/images/icons/globe_icon_blue.svg";
export const Flag_Usa_Icon_Grey = "/images/icons/flag_usa_icon_grey.svg";
export const Flag_Usa_Icon_Blue = "/images/icons/flag_usa_icon_blue.svg";
export const My_Location_Icon_Blue =
  "/images/icons/my_location_icon_blue.svg";
export const My_Location_Icon_Grey =
  "/images/icons/my_location_icon_grey.svg";

//Service-Page assets
export const Services_Banner = "/images/assets/services_banner.png";
export const Services_Footer_Banner =
  "/images/assets/services_footer_banner.png";
export const Insurance_Bg = "/images/assets/insurance_bg.png";
export const Boatzon_Loan_Financing =
  "/images/assets/boatzon_loan_financing.png";
export const Boatzon_Insurance = "/images/assets/boatzon_insurance.png";

//Discussion-Page assets
export const Discussion_Banner = "/images/assets/discussion_banner.png";
export const Discussion_Post_Img =
  "/images/assets/discussion_post_img.png";

//ProfileTransaction-Page assets
export const Item_1 = "/images/assets/transactionData/item_1.jpg";
export const Seller_1 = "/images/assets/transactionData/seller_1.jpg";
export const Item_2 = "/images/assets/transactionData/item_2.jpg";
export const Seller_2 = "/images/assets/transactionData/seller_2.jpg";
export const Item_3 = "/images/assets/transactionData/item_3.jpg";
export const Seller_3 = "/images/assets/transactionData/seller_3.jpg";
export const Item_4 = "/images/assets/transactionData/item_4.jpg";
export const Seller_4 = "/images/assets/transactionData/seller_4.jpg";
export const Item_5 = "/images/assets/transactionData/item_5.jpg";
export const Seller_5 = "/images/assets/transactionData/seller_5.jpg";
export const NoTransation_Dollar_Symbol =
  "/images/assets/noTransation_dollar_symbol.png";

//My-Offers assets
export const Messanger_1 = "/images/icons/messanger_1.svg";
export const Messanger_2 = "/images/icons/messanger_2.svg";
export const Messanger_3 = "/images/icons/messanger_3.svg";

export const MapPin = "/images/assets/map_pin.png";
export const MapSearch = "/images/assets/map_search.png";

export const NoArchive_Img = "/images/assets/noArchive_img.png";

// Following assets
export const NoFavFollowing_Img =
  "/images/assets/noFavFollowing_img.png";
export const NoPeopleFollowing_Img =
  "/images/assets/noPeopleFollowing_img.png";
export const NoBusinessFollowing_Img =
  "/images/assets/noBusinessFollowing_img.png";
export const NoDiscussionFollowing_Img =
  "/images/assets/noDiscussionFollowing_img.png";

// Banner assets
export const ProfessionalSignup_Banner_BG =
  "/images/assets/boatzonPro_Banner_img.png";
export const ProfessionalSignup_FooterBanner_BG =
  "/images/assets/professional_signup_footer_banner.png";

// Follow Manufacturer Page Logos
export const Bayliner_Logo =
  "/images/assets/manufacturerLogos/bayliner_logo.svg";
export const Bennington_Logo =
  "/images/assets/manufacturerLogos/bennington_logo.svg";
export const BostonWhaler_Logo =
  "/images/assets/manufacturerLogos/bostonWhaler_logo.svg";
export const Chaparral_Logo =
  "/images/assets/manufacturerLogos/chaparral_logo.png";
export const Contender_Logo =
  "/images/assets/manufacturerLogos/contender_logo.svg";
export const Malibu_Logo =
  "/images/assets/manufacturerLogos/malibu_logo.svg";
export const SeaDoo_Logo =
  "/images/assets/manufacturerLogos/seaDoo_logo.svg";
export const SeaRay_Logo =
  "/images/assets/manufacturerLogos/seaRay_logo.png";
export const SunTracker_Logo =
  "/images/assets/manufacturerLogos/sunTracker_logo.svg";
export const Tracker_Logo =
  "/images/assets/manufacturerLogos/tracker_logo.svg";
export const Yamaha_Logo =
  "/images/assets/manufacturerLogos/yamaha_logo.svg";

// Upload Profile Pic asstes
export const UploadPic_1 = "/images/assets/uploadPic_1.png";
export const UploadPic_2 = "/images/assets/uploadPic_2.png";

// Purchases & Sales asstes
export const Time_To_Ship_Orange_Icon =
  "/images/icons/time_to_ship_orange_icon.svg";
export const Canceled_Red_Icon = "/images/icons/canceled_red_icon.svg";
export const Offer_Accepted_Light_Blue_Icon =
  "/images/icons/offer_accepted_light_blue_icon.svg";
export const Payment_Scheduled_Blue_Icon =
  "/images/icons/payment_scheduled_blue_icon.svg";
export const Payment_Sent_Blue_Icon =
  "/images/icons/payment_sent_blue_icon.svg";

//Share Post asstes
export const WhatsApp_ShareIcon = "/images/icons/whatsApp_shareIcon.svg";
export const Insta_ShareIcon = "/images/icons/insta_shareIcon.svg";
export const Email_ShareIcon = "/images/icons/email_shareIcon.svg";
export const Twitter_ShareIcon = "/images/icons/twitter_shareIcon.svg";
export const Facebook_ShareIcon = "/images/icons/facebook_shareIcon.svg";

//Services Assests
export const Insurance_Video =
  "https://res.cloudinary.com/boatzon/video/upload/v1620267310/banner/insurance-video.mp4";
export const Loan_Financing_Video =
  "https://res.cloudinary.com/boatzon/video/upload/v1620267335/banner/loan-financing-video.mp4";
export const Personal_Info_Active_Icon =
  "/images/icons/personal_info_active_icon.svg";
export const Personal_Info_Inactive_Icon =
  "/images/icons/personal_info_inactive_icon.svg";
export const Boat_Info_Active_Icon =
  "/images/icons/boat_info_active_icon.svg";
export const Boat_Info_Inactive_Icon =
  "/images/icons/boat_info_inactive_icon.svg";
export const My_Loan_Active_Icon =
  "/images/icons/my_loan_active_icon.svg";
export const My_Loan_Inactive_Icon =
  "/images/icons/my_loan_inactive_icon.svg";
export const My_pre_Approval_Active_Icon =
  "/images/icons/my-pre_approval_active_icon.svg";
export const My_pre_Approval_Inactive_Icon =
  "/images/icons/my-pre_approval_inactive_icon.svg";
export const My_Insurance_Active_Icon =
  "/images/icons/my_insurance_active_icon.svg";
export const My_Insurance_Inactive_Icon =
  "/images/icons/my_insurance_inactive_icon.svg";
export const View_Quote_Active_Icon =
  "/images/icons/view_quote_active_icon.svg";
export const LoanType = "/images/assets/loan-type.png";
export const View_Quote_Inactive_Icon =
  "/images/icons/view_quote_inactive_icon.svg";
export const Active_Border_Icon = "/images/icons/active_border.svg";
export const Inactive_Border_Icon = "/images/icons/inactive_border.svg";
export const Service_Overview_Bg =
  "/images/assets/service_overview_bg.png";
export const Add_Insurance_Tab_Icon =
  "/images/assets/add_insurance_tab_icon.png";

// Get Pre Approved Page
export const Get_Pre_Approved_Bg =
  "/images/assets/get-pre-approved-bg.png";
export const Score_Meter_Icon = "/images/icons/score_meter_icon.svg";
export const Info_Icon_Light_Grey =
  "/images/icons/info_icon_light_grey.svg";
export const Loan_Approved_Bg = "/images/assets/loan-approved-bg.png";

// My Boats Assests
export const No_Boat_Post_Img = "/images/assets/no_boat_post_img.png";

//Chat Assets
export const EnabledAttachment = "/images/icons/enabled_attachment.svg";
export const DisabledAttachment =
  "/images/icons/disabled_attachment.svg";
export const MapAttachment = "/images/icons/map_attachment.svg";
export const ImageAttachment = "/images/icons/image_attachment.svg";
export const VideoCall = "/images/icons/video-call.svg";
export const PickUpCall = "/images/icons/phone-receive.svg";

export const Plan_Card_Bg = "/images/assets/plan_card_bg.png";
export const Placeholder_Image = "/images/icons/p_user.svg";
export const PlaceHolder_Boat = "/images/icons/p_yatch.svg";

export const firebaseApiKey = "AIzaSyBHCU7ffm0dwNDlS2we0DnoDneZRiASlck";
export const firebaseAuthDomain = "boatzon-a1f03.firebaseapp.com";
export const firebaseDatabaseURL = "https://boatzon-a1f03.firebaseio.com";
export const firebaseProjectId = "boatzon-a1f03";
export const firebaseStorageBucket = "boatzon-a1f03.appspot.com";
export const firebaseMessagingSenderId = "1029574855542";
export const firebaseAppId = "1:1029574855542:web:bc8dde1e512c362fff0857";
export const firebaseServiceURL = "https://iid.googleapis.com/iid/";
export const firebaseServerKey =
  "AAAA77dxi3Y:APA91bGHCY6AtAKzbjkObNNBBd5igPo2CuSVPGy1jU3CcGJlTKgZIx0ZqU-p-lc9DRXMEPFG0Fhgp2ojRdjl6KKWBY-ggzVX2FZyZhz77ty9Njy2A6rfSD1Uv8smLGaHhni-MP5uDyZn";
export const firebaseCertificateKey =
  "BBqRaQwLN2cUUd6GfoUO5A6ql36s9JQLHGbx_WW6g2tZyOaqf4S_2SgHp5ctUXURu9lYqKTc3f9JTCVWLljlqNM";

export const disabledMap = "/images/icons/products-page-map-white.svg";
export const enabledMap = "/images/icons/products-page-map.svg";
export const disabledTruck =
  "/images/icons/products-page-truck-white.svg";
export const enabledTruck = "/images/icons/products-page-truck.svg";
export const Heart = "/images/icons/heart.svg";
export const Clock = "/images/icons/clock.svg";
export const RightChevron = "/images/icons/right-arrow.svg";
export const scheduleTimes = [
  {
    label: "9:00 am",
    ms: 60000 * 9,
    hour: 9,
    time: "09:00:00",
  },
  {
    label: "10:00 am",
    ms: 60000 * 10,
    hour: 10,
    time: "10:00:00",
  },
  {
    label: "11:00 am",
    ms: 60000 * 11,
    hour: 11,
    time: "11:00:00",
  },
  {
    label: "12:00 pm",
    ms: 60000 * 12,
    hour: 12,
    time: "12:00:00",
  },
  {
    label: "1:00 pm",
    ms: 60000 * 13,
    hour: 13,
    time: "13:00:00",
  },
  {
    label: "2:00 pm",
    ms: 60000 * 14,
    hour: 14,
    time: "14:00:00",
  },
  {
    label: "3:00 pm",
    ms: 60000 * 15,
    hour: 15,
    time: "15:00:00",
  },
  {
    label: "4:00 pm",
    ms: 60000 * 16,
    hour: 16,
    time: "16:00:00",
  },
  {
    label: "5:00 pm",
    ms: 60000 * 17,
    hour: 17,
    time: "17:00:00",
  },
  {
    label: "6:00 pm",
    ms: 60000 * 18,
    hour: 18,
    time: "18:00:00",
  },
];

export const scheduleTimesV2 = [
  {
    time: "9.00 am",
    ms: 3600000 * 9,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "10.00 am",
    ms: 3600000 * 10,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "11.00 am",
    ms: 3600000 * 11,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "12.00 pm",
    ms: 3600000 * 12,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "1.00 pm",
    ms: 3600000 * 13,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "2.00 pm",
    ms: 3600000 * 14,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "3.00 pm",
    ms: 3600000 * 15,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "4.00 pm",
    ms: 3600000 * 16,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "5.00 pm",
    ms: 3600000 * 17,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "6.00 pm",
    ms: 3600000 * 18,
    isVideo: false,
    isInPerson: false,
  },
];

export const scheduleTimesV3 = [
  {
    time: "9.00 am",
    ms: 3600000 * 9,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "10.00 am",
    ms: 3600000 * 10,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "11.00 am",
    ms: 3600000 * 11,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "12.00 pm",
    ms: 3600000 * 12,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "1.00 pm",
    ms: 3600000 * 13,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "2.00 pm",
    ms: 3600000 * 14,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "3.00 pm",
    ms: 3600000 * 15,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "4.00 pm",
    ms: 3600000 * 16,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "5.00 pm",
    ms: 3600000 * 17,
    isVideo: false,
    isInPerson: false,
  },
  {
    time: "6.00 pm",
    ms: 3600000 * 18,
    isVideo: false,
    isInPerson: false,
  },
];

// New Boats Page
export const Dollar_Circled_White =
  "/images/icons/dollar_circle_icon_white.svg";
export const Star_White = "/images/icons/star_icon_white.svg";
export const Dollar_Circled_Grey =
  "/images/icons/dollar_circle_icon_grey.svg";
export const Star_Grey = "/images/icons/star_icon_grey.svg";
export const MonthlyDealsBg = "/images/assets/monthlyDealsBg.png";
export const PopularBg = "/images/assets/popularBg.png";

export const Manufacturer_1 = "/images/assets/manufacturer_1.png";
export const Manufacturer_2 = "/images/assets/manufacturer_2.png";
export const Manufacturer_3 = "/images/assets/manufacturer_3.png";
export const Manufacturer_4 = "/images/assets/manufacturer_4.png";
export const Manufacturer_5 = "/images/assets/manufacturer_5.png";
export const Manufacturer_6 = "/images/assets/manufacturer_6.png";

// New Home Page
export const Banner_Video =
  "https://res.cloudinary.com/boatzon/video/upload/v1620267001/banner/banner_video.mp4";
export const Banner_Img_2 = "/images/assets/banner/banner_img_2.png";
export const Banner_Img_3 = "/images/assets/banner/banner_img_3.png";
export const Banner_Img_4 = "/images/assets/banner/banner_img_4.png";
export const Banner_Chevron_Left =
  "/images/icons/banner-chevron-left.svg";
export const Banner_Chevron_Right =
  "/images/icons/banner-chevron-right.svg";
export const Vector_Img_White = "/images/icons/vector_img_white.svg";
export const Inventory_Bg = "/images/assets/banner/inventory_bg.png";
export const Selling_Bg = "/images/assets/banner/selling_bg.png";
export const Vector_Img_Orange = "/images/icons/vector_img_orange.svg";
export const Google_Play_Trsp =
  "/images/icons/playstore_transparent.svg";
export const Appstore_Trsp = "/images/icons/appstore_transparent.svg";
export const Wholesale_Bg = "/images/assets/banner/wholesale_bg.png";
export const Vector_Img_Grey = "/images/icons/vector_img_grey.svg";
export const Boatzon_Insurance_Icon =
  "/images/icons/Boatzon_Insurance.svg";
export const Boatzon_Financing_Icon =
  "/images/icons/Boatzon_Financing.svg";
export const Boatzon_Warranties_Icon =
  "/images/icons/Boatzon_Warranties.svg";
export const Service_Maintenance_Icon =
  "/images/icons/Service_&_Maintenance.svg";
export const Video_Play_Icon = "/images/icons/video-play-icon.svg";

// dummy engines imgs
export const Engine_1_IMG = "/images/assets/grids/engine_1.png";
export const Engine_2_IMG = "/images/assets/grids/engine_2.png";
export const Engine_3_IMG = "/images/assets/grids/engine_3.png";
export const Engine_4_IMG = "/images/assets/grids/engine_4.png";
export const Engine_5_IMG = "/images/assets/grids/engine_5.png";

// engines Grids
export const Engine_1 = "/images/assets/engine_brand_1.png";
export const Engine_2 = "/images/assets/engine_brand_2.png";
export const Engine_3 = "/images/assets/engine_brand_3.png";
export const Engine_4 = "/images/assets/engine_brand_4.png";
export const Engine_5 = "/images/assets/engine_brand_5.png";

// trailers Grids
export const Trailers_1 = "/images/assets/trailer_1.png";
export const Trailers_2 = "/images/assets/trailer_2.png";
export const Trailers_3 = "/images/assets/trailer_3.png";
export const Trailers_4 = "/images/assets/trailer_4.png";
export const Trailers_5 = "/images/assets/trailer_5.png";

// dummy trailers imgs
export const Trailer_1_IMG = "/images/assets/grids/trailer_img_1.png";
export const Trailer_2_IMG = "/images/assets/grids/trailer_img_2.png";
export const Trailer_3_IMG = "/images/assets/grids/trailer_img_3.png";
export const Trailer_4_IMG = "/images/assets/grids/trailer_img_4.png";
export const Trailer_5_IMG = "/images/assets/grids/trailer_img_5.png";

export const PriceBg_Img = "/images/assets/priceBg_img.png";

// new create post icons
export const Trailers_White = "/images/icons/trailers_white.svg";
export const Engines_White = "/images/icons/engines_white.svg";

export const InfoIcon = "/images/icons/info_icon.svg";

export const colorPickerBg = "/images/assets/colorPickerBg.png";

export const About_boatzon = "/images/assets/About_boatzon.png";

export const BuyNow_Video =
  "https://res.cloudinary.com/boatzon/video/upload/v1620266983/banner/buy-now-video.mp4";

export const Img_Placeholder = "/images/assets/img_placeholder.png";
export const UserPlaceholder =
  "/images/icons/male-placeholder-image.jpeg";
