
import { convertToFloat } from "../math/convertToFloat";
import { FLAT, PERCENTAGE } from "../config";

export const isDiscountedProduct = (ProductData, type) => {
    let isDiscounted = false;
    switch (type) {
        case "PDP":
            if (ProductData && ProductData.finalPriceList && convertToFloat(ProductData.finalPriceList.discountPrice) > 0.00) {
                isDiscounted = true;
            }
            break;
    }


    return isDiscounted;

}


export const getAllProductsFromCart = (cartData) => {
    let cartProducts = [];

    // take all sellers products into 1 array, so we can identify easily - cart products
    cartData && cartData.sellers && cartData.sellers.map((seller, sellerIndex) => {
        seller && seller.products && seller.products.map((product, productIndex) => {
            cartProducts.push(product);
        })
    })

    return cartProducts;
}

export const getDiscount = (finalPriceList, currencySymbol, discountType) => {
    switch (discountType) {
        case 0:
            return convertToFloat(finalPriceList.discountPrice) > 0.0 ? "Flat " + currencySymbol + "" + convertToFloat(finalPriceList.discountPrice) + " Off" : '';
        case 1:
            let finalPercentage = convertToFloat(finalPriceList.discountPrice, 2) / convertToFloat(finalPriceList.basePrice, 2) * 100;
            return convertToFloat(finalPercentage, 0) > 0.0 ? "Upto " + convertToFloat(finalPercentage, 0) + "% Off" : '';
        default:
            return false
    }
}