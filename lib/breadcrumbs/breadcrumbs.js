import Router from "next/router";

const NoRoutableData = ["engine mechanic", "general contractors"];

const isRoutable = (slug) => {
  return NoRoutableData.findIndex((item) => item === slug) < 0;
};

export const handleBreadCrumbs = (data, key) => {
  let clickedItem = data[key];
  let destinationUrl = getMyURL(data, key);
  console.log("MY URL is --> ", destinationUrl);
  if (isRoutable(clickedItem)) {
    Router.push(destinationUrl);
  } else {
    window.location.reload();
  }
};

export const getMyURL = (data, key) => {
  console.log("djwid", data, key);
  let destinationUrl = "";
  let slash = "/";

  switch (data[key]) {
    case "all professionals":
      destinationUrl = `/professionals/engine-service/${data[key].replace(
        " ",
        "-"
      )}`;
      break;
    case "All Boats":
      destinationUrl = `/boats`;
      break;
    case "Engines":
      destinationUrl = `/engines`;
      break;
    case "Trailers":
      destinationUrl = `/trailers`;
      break;
  }

  return destinationUrl;
};
