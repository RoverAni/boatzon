import React from "react";
import BoatDetails from "../../containers/create-post/boat-details";
import EngineDetails from "../../containers/create-post/engine-details";
import AdvanceBoatDetails from "../../containers/create-post/advance-boat-details";
import ProductDetails from "../../containers/create-post/product-details";
import DeliveryMethod from "../../containers/create-post/delivery-method";
import TrailersDetails from "../../containers/create-post/trailers-details";

export const BoatDetailsComponent = (scope) => {
  return (
    <BoatDetails
      key={scope.state.screen}
      updateScreen={scope.updateScreen}
      onClose={scope.props.onClose}
      finalpayload={scope.state.finalpayload}
      handleBackScreenBoat={scope.handleBackScreenBoat}
      handleEngineDetailsPage={scope.handleEngineDetailsPage}
    />
  );
};

export const EngineDetailsComponent = (scope) => {
  return (
    <EngineDetails
      key={scope.state.screen}
      updateScreen={scope.updateScreen}
      onClose={scope.props.onClose}
      engineData={scope.props.engineData}
      finalpayload={scope.state.finalpayload}
      handleBackScreenBoat={scope.handleBackScreenBoat}
      handleAdvanceBoatDetails={scope.handleAdvanceBoatDetails}
      handleDeliveryMethodPage={scope.handleDeliveryMethodPage}
      handleSuccessfullPost={scope.handleSuccessfullPost}
      handleBackScreenEngine={scope.handleBackScreenEngine}
    />
  );
};

export const AdvanceBoatDetailsComponent = (scope) => {
  return (
    <AdvanceBoatDetails
      key={scope.state.screen}
      updateScreen={scope.updateScreen}
      onClose={scope.props.onClose}
      engineData={scope.props.engineData}
      finalpayload={scope.state.finalpayload}
      handleBackScreenBoat={scope.handleBackScreenBoat}
      handleSuccessfullPost={scope.handleSuccessfullPost}
    />
  );
};

export const ProductDetailsComponent = (scope) => {
  return (
    <ProductDetails
      updateScreen={scope.updateScreen}
      onClose={scope.props.onClose}
      finalpayload={scope.state.finalpayload}
      handleBackScreen={scope.handleBackScreen}
      handleDeliveryMethodPage={scope.handleDeliveryMethodPage}
      remoteSnackbarHandler={scope.remoteSnackbarHandler}
    />
  );
};

export const DeliveryMethodComponent = (scope) => {
  return (
    <DeliveryMethod
      updateScreen={scope.updateScreen}
      onClose={scope.props.onClose}
      finalpayload={scope.state.finalpayload}
      handleBackScreen={scope.handleBackScreen}
      handleSuccessfullPost={scope.handleSuccessfullPost}
      currentLocation={scope.props.currentLocation}
      remoteSnackbarHandler={scope.remoteSnackbarHandler}
    />
  );
};

export const TrailerDetailsComponent = (scope) => {
  return (
    <TrailersDetails
      updateScreen={scope.updateScreen}
      onClose={scope.props.onClose}
      finalpayload={scope.state.finalpayload}
      handleBackScreen={scope.handleBackScreen}
      handleSuccessfullPost={scope.handleSuccessfullPost}
      currentLocation={scope.props.currentLocation}
    />
  );
};

export const SuccessfullPostComponent = (scope, ProductData) => {
  return (
    <SuccessfullPost
      onClose={scope.props.onClose}
      ProductData={ProductData}
      handleCreateNewPost={scope.handleCreateNewPost}
      updateScreen={scope.updateScreen}
      pageName={scope.state.pageName}
      handleBackScreen={scope.handleBackScreen}
      finalpayload={scope.state.finalpayload}
    />
  );
};
