import { PRICE_H_TO_L, PRICE_L_TO_H, POPULARITY, SORT_BY, DEFAULT_LANGUAGE } from "../config";

import { getCookie } from "../session";

import { LanguageSet } from "../../translations/LanguageSet/index";

const selectedLanguage = getCookie("lang") || DEFAULT_LANGUAGE;


export const convertFilterStrings = (filter) => {
    switch (filter) {
        case POPULARITY:
            return LanguageSet[selectedLanguage] ? LanguageSet[selectedLanguage]["popularFilter"] : POPULARITY;
        case SORT_BY:
            return LanguageSet[selectedLanguage] ? LanguageSet[selectedLanguage]["sortBy"] : SORT_BY;
        case PRICE_H_TO_L:
            return LanguageSet[selectedLanguage] ? LanguageSet[selectedLanguage]["priceHighToLow"] : PRICE_H_TO_L;
        case PRICE_L_TO_H:
            return LanguageSet[selectedLanguage] ? LanguageSet[selectedLanguage]["priceLowToHigh"] : PRICE_L_TO_H;
    }
}