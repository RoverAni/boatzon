export const getFullName = (lang) => {
    switch (lang) {
        case "ms":
            return "Malay";
        case "en":
            return "Eng";
        case "es":
            return "Span";
        case "ar":
            return "Arab";
        default:
            return "Eng"
    }
}