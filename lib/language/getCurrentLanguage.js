import { DEFAULT_LANGUAGE, LANG } from "../config";
import { getCookie } from "../session";

export const getCurrentLanguage = () => {
    return getCookie(LANG) || DEFAULT_LANGUAGE;
}