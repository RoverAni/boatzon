export const detectBottomTouch = (bottomDifferenceInPixel = 0) => {
  // returns true or false, based on scroll position, pass bottom pixel difference as argument if required
  console.log("test-->window.innerHeight", window.innerHeight);
  console.log("test-->window.scrollY", window.scrollY);
  console.log("test-->document.body.offsetHeight", document.body.offsetHeight);
  console.log("test-->bottomDifferenceInPixel", bottomDifferenceInPixel);

  return (
    window.innerHeight + window.scrollY >=
    document.body.offsetHeight - bottomDifferenceInPixel
  );
};
