import { convertToFloat } from "../math/convertToFloat";

export const getRatingType = (rating) => {

    const ratingStar = convertToFloat(rating, 1);

    if (ratingStar >= 1 && ratingStar < 2)
        return "worst";
    if (ratingStar >= 2 && ratingStar < 3)
        return "average";
    if (ratingStar >= 3 && ratingStar < 4)
        return "good";
    if (ratingStar >= 4 && ratingStar <= 5)
        return "great";

}