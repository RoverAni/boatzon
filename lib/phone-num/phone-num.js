import { CanadaAreaCodes } from "../../fixtures/react-intl-tel-input/AreaCodes";
import { getCountries, getStates } from "country-state-picker";

export const handlePreferredCountries = (countryCode, phoneNum) => {
  console.log("dwkd", countryCode, phoneNum);

  if (countryCode == "+1") {
    if (CanadaAreaCodes.includes(phoneNum.substring(0, 3))) {
      return ["ca"];
    } else return ["us"];
  } else {
    let tempPreferredCountries = getCountries()
      .filter((data) => {
        return data.dial_code == countryCode;
      })
      .map((item) => {
        return item.code;
      });
    console.log("dwkd", tempPreferredCountries);
    return tempPreferredCountries;
  }
};
