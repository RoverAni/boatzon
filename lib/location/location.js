export const getCountryFromGoogle = (results) => {
  let currentCountry = "";
  let mainLocIndex = -1;

  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the city is not selected from google result
      if (currentCountry.length < 1) {
        // check if the country is available in google result, (in case city not detected)
        mainLocIndex =
          location.address_components &&
          location.address_components.findIndex((data) => {
            return data.types.includes("country");
          });

        if (mainLocIndex != -1) {
          currentCountry = location.address_components[mainLocIndex].long_name;
        }
        console.log("country", currentCountry);
      }
    });

  return currentCountry;
};

export const getCountryShortNameFromGoogle = (results) => {
  let currentCountryshortname = "";
  let mainLocIndex = -1;
  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the city is not selected from google result
      if (currentCountryshortname.length < 1) {
        // check if the country is available in google result, (in case city not detected)
        mainLocIndex =
          location.address_components &&
          location.address_components.findIndex((data) => {
            return data.types.includes("country");
          });

        if (mainLocIndex != -1) {
          currentCountryshortname = location.address_components[mainLocIndex].short_name;
        }
        console.log("currentCountryshortname", currentCountryshortname);
      }
    });
  return currentCountryshortname;
};

export const getCityFromGoogle = (results) => {
  let currentCity = "";
  let mainLocIndex = -1;
  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the city is not selected from google result
      if (currentCity.length < 1) {
        // check if the country is available in google result, (in case city not detected)
        mainLocIndex =
          location.address_components &&
          location.address_components.findIndex((data) => {
            return data.types.includes("locality");
          });

        if (mainLocIndex == -1) {
          mainLocIndex =
            location.address_components &&
            location.address_components.findIndex((data) => {
              return data.types.includes("sublocality");
            });
        } else if (mainLocIndex == -1) {
          mainLocIndex =
            location.address_components &&
            location.address_components.findIndex((data) => {
              return data.types.includes("administrative_area_level_1");
            });
        } else if (mainLocIndex == -1) {
          mainLocIndex =
            location.address_components &&
            location.address_components.findIndex((data) => {
              return data.types.includes("administrative_area_level_2");
            });
        }

        if (mainLocIndex != -1) {
          currentCity = location.address_components[mainLocIndex].long_name;
        }
        console.log("locality", currentCity);
      }
    });

  return currentCity;
};

export const getAreaFromGoogle = (results) => {
  let currentArea = "";
  let mainLocIndex = -1;

  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the city is not selected from google result
      if (currentArea.length < 1) {
        // check if the country is available in google result, (in case city not detected)
        mainLocIndex =
          location.address_components &&
          location.address_components.findIndex((data) => {
            return data.types.includes("sublocality");
          });

        if (mainLocIndex != -1) {
          currentArea = location.address_components[mainLocIndex].long_name;
        }
        console.log("sublocality", currentArea);
      }
    });

  return currentArea;
};

export const getStateFromGoogle = (results) => {
  let currentState = "";
  let mainLocIndex = -1;

  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the state is not selected from google result
      if (currentState.length < 1) {
        // check if the country is available in google result, (in case state not detected)
        mainLocIndex =
          location.address_components &&
          location.address_components.findIndex((data) => {
            return data.types.includes("administrative_area_level_1");
          });
        if (mainLocIndex != -1) {
          currentState = location.address_components[mainLocIndex].short_name;
        }
        console.log("locality", currentState);
      }
    });

  return currentState;
};

export const getPincodeFromGoogle = (results) => {
  let currentPincode = "";
  let mainLocIndex = -1;
  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the pincode is not selected from google result

      if (currentPincode.length < 1) {
        // check if the country is available in google result, (in case pincode not detected)
        mainLocIndex = location.types.findIndex((typeOfLocation) => typeOfLocation === "postal_code");

        if (mainLocIndex > -1) {
          currentPincode = location.address_components[0].long_name.split(",")[0];
        }
      }
    });

  return currentPincode;
};

export const getZipCodeFromGoogle = (results) => {
  let zipCode = "";
  let mainLocIndex = -1;

  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the city is not selected from google result
      if (zipCode.length < 1) {
        // check if the country is available in google result, (in case city not detected)
        mainLocIndex =
          location.address_components &&
          location.address_components.findIndex((data) => {
            return data.types.includes("postal_code");
          });
        if (mainLocIndex != -1) {
          zipCode = location.address_components[mainLocIndex].long_name;
        }
        console.log("postal_code", zipCode);
      }
    });

  return zipCode;
};

export const getAddressFromGoogle = (results) => {
  let currentAddress = "";
  let mainLocIndex = -1;

  // to find out main locality - country
  results &&
    results.map((location, index) => {
      // check if the city is not selected from google result
      if (currentAddress.length < 1) {
        // check if the country is available in google result, (in case city not detected)
        mainLocIndex = location.types.findIndex((typeOfLocation) => typeOfLocation === "sublocality_level_1");

        if (mainLocIndex == -1) {
          mainLocIndex = location.types.findIndex((typeOfLocation) => typeOfLocation === "route");
        }

        if (mainLocIndex > -1) {
          currentAddress = location.formatted_address;
        }
      }
    });

  return currentAddress;
};

export const getMyAddress = async (location) => {
  console.log("location", location);
  return new Promise((resolve, reject) => {
    if (location) {
      return resolve(location);
    } else {
      return reject("File not found");
    }
  });
};
