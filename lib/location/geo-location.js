import {
  getAddressFromGoogle,
  getCityFromGoogle,
  getCountryFromGoogle,
  getCountryShortNameFromGoogle,
  getPincodeFromGoogle,
  getStateFromGoogle,
} from "./location";

export const getLocation = () => {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      let optionsPosition = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 0,
      };

      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;

          let geocoder = new google.maps.Geocoder();
          let latlng = new google.maps.LatLng(latitude, longitude);

          geocoder.geocode({ latLng: latlng }, (results, status) => {
            console.log("results", results); /*----> i don't get response */
            let x = {
              address: getAddressFromGoogle(results),
              latitude: latitude,
              longitude: longitude,
              city: getCityFromGoogle(results),
              country: getCountryFromGoogle(results),
              countryShortName: getCountryShortNameFromGoogle(results),
              zipCode: getPincodeFromGoogle(results),
              stateName: getStateFromGoogle(results),
            };
            return resolve(x);
          });
        },
        () => {},
        optionsPosition
      );
    } else {
      return reject(0);
    }
  });
};

export const getLocationfromLatLng = (latitude, longitude) => {
  return new Promise((resolve, reject) => {
    if (latitude && longitude) {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(latitude, longitude);

      geocoder.geocode({ latLng: latlng }, (results, status) => {
        console.log("results--:", results);
        let x = {
          address: getAddressFromGoogle(results),
          latitude: latitude,
          longitude: longitude,
          stateName: getStateFromGoogle(results),
          city: getCityFromGoogle(results),
          country: getCountryFromGoogle(results),
          countryShortName: getCountryShortNameFromGoogle(results),
          zipCode: getPincodeFromGoogle(results),
        };
        console.log("results--:", x);
        return resolve(x);
      });
    } else {
      return reject(0);
    }
  });
};

export const getLocationfromStateZipcode = (
  address,
  country,
  stateName,
  city,
  zipCode
) => {
  return new Promise((resolve, reject) => {
    if (stateName) {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode(
        { address: `${address} ${stateName} ${country} ${city} ${zipCode}` },
        (results, status) => {
          console.log("fbuefh--:", results);
          let x = {
            address: getAddressFromGoogle(results),
            latitude:
              results &&
              results.length > 0 &&
              results[0].geometry.location.lat(),
            longitude:
              results &&
              results.length > 0 &&
              results[0].geometry.location.lng(),
            stateName: getStateFromGoogle(results),
            city: getCityFromGoogle(results),
            country: getCountryFromGoogle(results),
            countryShortName: getCountryShortNameFromGoogle(results),
            zipCode: getPincodeFromGoogle(results),
          };
          console.log("fbuefh--:", x);
          return resolve(x);
        }
      );
    } else {
      return reject(0);
    }
  });
};
