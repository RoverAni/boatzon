export const firebase = () => {
  if ("Notification" in window) {
    if (!firebase.apps.length) {
      getPushToken();
    }
  }
};

export const getPushToken = () => {
  var firebaseConfig = {
    apiKey: "AIzaSyBHCU7ffm0dwNDlS2we0DnoDneZRiASlck",
    authDomain: "boatzon-a1f03.firebaseapp.com",
    databaseURL: "https://boatzon-a1f03.firebaseio.com",
    projectId: "boatzon-a1f03",
    storageBucket: "boatzon-a1f03.appspot.com",
    messagingSenderId: "1029574855542",
    appId: "1:1029574855542:web:bc8dde1e512c362fff0857",
  };

  firebase.initializeApp(firebaseConfig);

  const messaging = firebase.messaging();
  console.log("messaging", messaging, firebase);
  console.log("messaging", firebase);
  console.log("messaging", messaging.getToken());

  return messaging.getToken();

  // Notification.requestPermission()
  //   .then((permission) => {
  //     // console.log("Notification permission accepted.", permission);
  //     return messaging.getToken();
  //   })
  //   .then((token) => {
  //     console.log("token", token);
  //   })
  //   .catch((err) => {
  //     console.log("notification eror", err);
  //   });
};
