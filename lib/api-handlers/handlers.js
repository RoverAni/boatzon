let isSessionActive = true;

// API RESPONSE HANDLER - SUCCESS
export const successHandler = (data) => {
    // console.log("RESP --> ", data);
    return data;
}

// API RESPONSE HANDLER - ERROR
export const errorHandler = (error) => {
    // if (error && error.status === 401) {
    //     // isSessionActive = false;
    // }
    return error;
}

// API REQUEST HANDLER - ERROR
export const requestHandler = (request) => {
    // return request if user session is active, else send null to prevent api hit on server;
    // return isSessionActive ? request : null;
    return request;
}