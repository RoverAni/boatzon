export const EMAIL = "Email";
export const NEW_EMAIL = "Email";
export const NAME = "Name";
export const FULL_NAME = "FullName";
export const PASSWORD = "Password";
export const OLD_PASSWORD = "oldPassword";
export const NEW_PASSWORD = "newPassword";
export const CONF_PASS = "Confirm Password";
export const CMP_NAME = "Company Name";
export const CMP_REG_NO = "Commercial Registration Number";
export const CMP_REG_NO_EXP_DATE = "Commercial Registration Number Expiry Date";
export const WEBSITE = "Website";
export const PINCODE = "PinCode";
export const LOCALITY = "Locality";
export const STATE = "State";
export const CITY = "City";
export const HOME = "Home";
export const WORK = "Work";
export const MOBILE = "mobile";
export const COUNTRY_CODE = "countryCode";
export const F_NAME = "firstNane";
export const L_NAME = "lastName";
export const DOB = "dateOfBirth";


