export const emailValidator = (input) => {
  let pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  return pattern.test(input)
    ? true
    : input.length >= 1 && !pattern.test(input)
    ? false
    : true;
};

export const passwordValidator = (input) => {
  let pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

  return pattern.test(input) ? true : false;
};

export const websiteValidator = (input) => {
  let pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

  return pattern.test(input) ? true : false;
};

export const ssnValidator = (input) => {
  let pattern = /^(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}$/;
  return pattern.test(input) ? true : false;
};

export const requiredValidator = (input) => {
  if (input.value.length < 1) {
    return 0;
  } else {
    switch (input.name) {
      case "Email" || "email":
        if (!emailValidator(input.value)) return 2;
        break;
      case "Password" || "newPassword":
        if (!passwordValidator(input.value)) return 3;
        break;
      case "Website":
        if (!websiteValidator(input.value)) return 4;
        break;
      case "SSN":
        if (input.value.length <= 6) return 5;
        break;
    }
    return 1;
  }
};

export const TextValidator = (event) => {
  var keyCode = event.keyCode ? event.keyCode : event.which;

  if (
    !(
      (keyCode >= 97 && keyCode <= 122) ||
      (keyCode >= 65 && keyCode <= 90) ||
      (keyCode >= 48 && keyCode <= 57) ||
      keyCode === 32 ||
      keyCode === 8
    )
  ) {
    if (
      keyCode === 35 ||
      keyCode === 43 ||
      keyCode === 47 ||
      event.target.value.indexOf("!@") >= 0
    ) {
      event.preventDefault();
    }
  }
};

export const TextWithNumberValidator = (event) => {
  var keyCode = event.keyCode ? event.keyCode : event.which;

  if (
    !(
      (keyCode >= 97 && keyCode <= 122) ||
      (keyCode >= 65 && keyCode <= 90) ||
      (keyCode >= 48 && keyCode <= 57) ||
      keyCode === 32 ||
      keyCode === 8
    )
  ) {
    event.preventDefault();
  }
};

export const PureTextValidator = (event) => {
  var keyCode = event.keyCode ? event.keyCode : event.which;

  if (
    !(
      (keyCode >= 97 && keyCode <= 122) ||
      (keyCode >= 65 && keyCode <= 90) ||
      keyCode === 32 ||
      keyCode === 8
    )
  ) {
    event.preventDefault();
  }
};

export const NumberValidator = (event) => {
  let keyCode = event.keyCode ? event.keyCode : event.which;
  if (event.target.value.length == 0 && keyCode == 48) {
    event.preventDefault();
  } else if (!(keyCode >= 48 && keyCode <= 57)) {
    event.preventDefault();
  }
};

export const matchPasswordValidator = (pass, confirmPass) => {
  console.log(
    "MATH PASS --> ",
    pass,
    confirmPass,
    pass && confirmPass && pass === confirmPass
  );
  return pass && confirmPass && pass === confirmPass;

  // return pass.localeCompare(confirmPass);

  // Returns -1 if str1 is sorted before str2
  // Returns 0 if the two strings are equal
  // Returns 1 if str1 is sorted after str2
};

export const validatePhoneNumber = (controlId) => {
  let phoneInput = document.getElementById(controlId);
  phoneInput
    ? phoneInput.addEventListener("keypress", (event) => {
        let keyCode = event.keyCode;
        if (!(keyCode >= 48 && keyCode <= 57)) {
          event.preventDefault();
        }
      })
    : "";
};

export const ValidateName = (input) => {
  let inputVal = input.target.value;
  let NamePattern = /^[a-zA-Z\s]*$/;
  return NamePattern.test(inputVal);
};
