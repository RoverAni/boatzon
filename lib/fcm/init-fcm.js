import firebase from "firebase";
import request from "./restUtils";
import API_ENDPOINT from "./api-endpoint";
import { getCookie } from "../session";
import * as keys from "../config";

let messaging;

if (firebase.messaging.isSupported()) {
  const initializedFirebaseApp = firebase.initializeApp({
    apiKey: `${keys.firebaseApiKey}`,
    authDomain: `${keys.firebaseAuthDomain}`,
    databaseURL: `${keys.firebaseDatabaseURL}`,
    projectId: `${keys.firebaseProjectId}`,
    storageBucket: `${keys.firebaseStorageBucket}`,
    messagingSenderId: `${keys.firebaseMessagingSenderId}`,
    appId: `${keys.firebaseAppId}`,
  });
  messaging = initializedFirebaseApp.messaging();
}

if (firebase.messaging.isSupported()) {
  messaging.usePublicVapidKey(keys.firebaseCertificateKey);
}

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./firebase-messaging-sw.js")
    .then(function (registration) {
      console.log("Registration successful, scope is:", registration.scope);
    })
    .catch(function (err) {
      console.log("Service worker registration failed, error:", err);
    });
}

export function logoutFirebase() {
  if (firebase.messaging.isSupported()) {
    messaging
      .getToken()
      .then((token) => {
        var fbToken = token;
        messaging.deleteToken(token);
        return fbToken;
      })
      .then((fbToken) => {
        var reqBody = {
          to: `/topics/${getCookie("uid")}`,
          registration_tokens: [`${fbToken}`],
        };
        return request
          .doPostFirebase(API_ENDPOINT.firebaseBatchRemove, reqBody)
          .then((res) => {
            if (res.status !== 200) {
              console.log(res.results.error);
            }
          })
          .catch((e) => console.log("checkSubscription: " + e.message));
      })
      .catch((err) => {
        console.log("error deleting token :(");
      });
  }
}

export function subscribeToNotificationsOnCookie() {
  if (firebase.messaging.isSupported()) {
    messaging
      .getToken()
      .then((token) => {
        handleTokenRefresh(token);
        console.log("subscribeToNotifications on topic success", token);
      })
      .catch((err) => console.log("subscribeToNotifications on topic error", err));
  }
}

function handleTokenRefresh(token) {
  window.firebaseToken = token;
  return request
    .doGetFirebase(API_ENDPOINT.formatUrl(API_ENDPOINT.firebaseTokenDetails, token))
    .then((resp) => resp.json())
    .then((res) => {
      checkSubscription(res);
    })
    .catch((e) => console.log("handleRefreshToken: " + e.message));
}

function checkSubscription(resp) {
  if (resp) {
    if (resp.status !== 200 || !resp.authorizedEntity || !resp.rel) {
      console.log("checkSubscription", getCookie("uid"));
      var reqBody = {
        to: `/topics/${getCookie("uid")}`,
        registration_tokens: [`${window.firebaseToken}`],
      };
      return request
        .doPostFirebase(API_ENDPOINT.firebaseBatchAddApi, reqBody)
        .then((re) => relateAppInstanceWithTopic(re))
        .catch((e) => console.log("checkSubscription: " + e.message));
    }
  }
}

function relateAppInstanceWithTopic(res) {
  if (res.status === 200 && !res.results) {
    let params = [window.firebaseToken, `${getCookie("uid")}`];
    return request
      .doPostFirebase(API_ENDPOINT.formatUrl(API_ENDPOINT.mapTopicWithFirebaseInstance, ...params))
      .then((response) => {
        if (response.status === 200) {
          console.log("User is subscribed for push notification.");
        } else {
          console.log("User is not subscribed for push notification.");
        }
      })
      .catch((e) => console.log(e.message));
  }
}

function textdecode(str) {
  try {
    return decodeURIComponent(escape(atob(str)));
  } catch (e) {}
}

// displaying notifications locally (without using fcm)
// export const displayMessages = (object) => {
//   navigator.serviceWorker.getRegistration().then((reg) => {
//     var options = {
//       body: textdecode(object.payload),
//       vibrate: [150, 150, 150, 150, 75, 75, 150, 150, 150, 150, 450],
//       data: {
//         dateOfArrival: Date.now(),
//         primaryKey: 1,
//       },
//     };
//     reg.showNotification(object.name, options);
//   });
// };

if (firebase.messaging.isSupported()) {
  messaging.onTokenRefresh(function () {
    messaging
      .getToken()
      .then(function (refreshedToken) {
        handleTokenRefresh(refreshedToken)
          .then((resp) => {
            console.log("refresh token", resp);
            checkSubscription(resp);
          })
          .catch((err) => {
            console.log("error getting permission :(");
          });
      })
      .catch(function (err) {
        console.log("Unable to retrieve refreshed token ", err);
      });
  });
}

export { messaging };
