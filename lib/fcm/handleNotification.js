import { firebaseCloudMessaging } from "./webPush";
// import localforage from "localforage";
// import firebase from "firebase/app";
import "@firebase/messaging";
import firebase from "@firebase/app";
import { firebaseServerKey } from "../../lib/config";

export async function setToken(topic) {
  try {
    console.log("topic is -> ", topic);
    topic = topic || (await localStorage.getItem(FCM_TOPIC));
    const token = await firebaseCloudMessaging.init();
    subscribeTokenToTopic(token, topic);
    if (token) {
      console.log("token is", token);
      // getMessage();
    }
  } catch (error) {
    console.log("error in setToken FCM ", error);
  }
}

function subscribeTokenToTopic(token, topic) {
  try {
    fetch("https://iid.googleapis.com/iid/v1/" + token + "/rel/topics/" + topic, {
      method: "POST",
      headers: new Headers({
        Authorization: "key=" + firebaseServerKey,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("success subscribe to token", data);
      })
      .catch((error) => {
        console.error("Subscribed to", error);
      });
  } catch (e) {}
}

function textdecode(str) {
  try {
    return decodeURIComponent(escape(atob(str)));
  } catch (e) {}
}

// displaying notifications locally (without using fcm)
export const displayMessages = (object) => {
  navigator.serviceWorker.getRegistration().then((reg) => {
    var options = {
      body: textdecode(object.payload),
      vibrate: [150, 150, 150, 150, 75, 75, 150, 150, 150, 150, 450],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1,
      },
    };
    reg.showNotification(object.name, options);
  });
};

function getMessage() {
  const messaging = firebase.messaging();
  messaging.onMessage((payload) => {
    console.log("getMessage onMessage", payload);
    // let metaData = {};
    try {
      console.log("payload", payload);
      //   let { notification, title, data = {} } = paylod;
      //   if (data && data.metaData) {
      //     handlerNotification(paylod.data["pushType"], paylod);
      //   }
      //   let assetImage = "";
      //   try {
      // if (data && data.metaData) {
      // metaData = JSON.parse(validJSON(data.metaData));
      // console.log("paylod", metaData);
      // assetImage = metaData.assetImage || metaData.userProfilePic;
      // } else {
      // assetImage = data.image;
      // }
      //   console.log("paylod", data);

      // console.log("paylod", chatData, assetImage);
      //   } catch (e) {
      //     console.log("paylod", e);
      //   }
      // notification = {
      //   ...notification,
      //   icon: APP_LOGO,
      //   image: paylod.notification.image || assetImage || APP_LOGO,
      // };

      // let browserNotification = new Notification(
      //   notification.title || APP_NAME,
      //   notification
      // );

      // browserNotification.onclick = () => {
      //   let redirectionData = {};
      //   if (Object.keys(metaData).length > 0) {
      //     redirectionData = redirectionData;
      //   } else {
      //     console.log("sadadd", location.pathname);
      //     redirectionData = data;
      //   }
      //   notificationRedirection(redirectionData, true);
      // };
    } catch (e) {
      console.log("notification eroro", e);
    }
  });
}
