import * as keys from "../config";
// let firebaseService = "https://iid.googleapis.com/iid/v1/";

var API_ENDPOINT = {
  firebaseTokenDetails: `${keys.firebaseServiceURL}info/{0}?details=true`,
  firebaseBatchAddApi: `${keys.firebaseServiceURL}v1:batchAdd`,
  mapTopicWithFirebaseInstance: `${keys.firebaseServiceURL}v1/{0}/rel/topics/{1}`,
  firebaseBatchRemove: `${keys.firebaseServiceURL}v1:batchRemove`,

  formatUrl: function (url) {
    var args = Array.prototype.slice.call(arguments, 1);
    return url.replace(/{(\d+)}/g, function (match, number) {
      return args[number] ? args[number] : match;
    });
  },
};

export default API_ENDPOINT;
