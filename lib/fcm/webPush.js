import "@firebase/messaging";
import firebase from "@firebase/app";
import localforage from "localforage";
import { notification } from "../mqtt/subjects";
import * as keys from "../config";

const firebaseCloudMessaging = {
  //checking whether token is available in indexed DB
  tokenInlocalforage: async () => {
    const status = await Notification.requestPermission();
    if (status && status === "granted") {
      notification.next();
      return localforage.getItem("fcm_token");
    }
  },
  //initializing firebase app
  init: async function () {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: "AIzaSyBHCU7ffm0dwNDlS2we0DnoDneZRiASlck",
        authDomain: "boatzon-a1f03.firebaseapp.com",
        databaseURL: "https://boatzon-a1f03.firebaseio.com",
        projectId: "boatzon-a1f03",
        storageBucket: "boatzon-a1f03.appspot.com",
        appId: "1:1029574855542:web:bc8dde1e512c",
        messagingSenderId: "1029574855542",
      });
      ``;
      try {
        const messaging = firebase.messaging();
        messaging.usePublicVapidKey(keys.firebaseCertificateKey);
        const tokenInLocalForage = await this.tokenInlocalforage();
        //if FCM token is already there just return the token
        if (tokenInLocalForage !== null) {
          return tokenInLocalForage;
        }
        //requesting notification permission from browser
        const status = await Notification.requestPermission();

        console.log("fcm token", status);
        if (status && status === "granted") {
          notification.next();
          //getting token from FCM

          const fcm_token = await messaging.getToken();
          if (fcm_token) {
            //setting FCM token in indexed db using localforage
            localforage.setItem("fcm_token", fcm_token);
            console.log("fcm fcm_token", fcm_token);
            //return the FCM fcm_token after saving it
            return fcm_token;
          }
        }
      } catch (error) {
        console.log("fcm token", error);
        console.error(error);
        return null;
      }
    }
  },
};
export { firebaseCloudMessaging };
