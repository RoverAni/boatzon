import Fingerprint from "fingerprintjs";

export const getDeviceId = () => {
  var fp = new Fingerprint({
    canvas: true,
    ie_activex: true,
    screen_resolution: true,
  });

  var uid = fp.get();

  // create assign device id to window object and assign the value
  if (typeof window != "undefined") window.deviceID = String(uid);

  return window.deviceID;
};
