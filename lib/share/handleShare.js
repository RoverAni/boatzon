import { APP_NAME } from "../config";

export const handleEmailShare = (ProductData) => {
  const { productName, postId, category } = ProductData;
  let slug =
    category == "Products"
      ? "product-detail"
      : category == "Boats"
      ? "boat-detail"
      : "";
  let ProductShareUrl = `https://stagingbz.com/${slug}/${productName.replace(
    / /g,
    "-"
  )}?pid=${postId}`;
  let email = `mailto:?Subject=I ♥ this product on ${APP_NAME}!&body=${productName}. Here's the link ${ProductShareUrl}`;

  window.open(email);
};

export const handleFBShare = (ProductData) => {
  // href={`https://www.facebook.com/sharer/sharer.php?u=`} target="_blank"
  const { productName, postId, category } = ProductData;
  let slug =
    category == "Products"
      ? "product-detail"
      : category == "Boats"
      ? "boat-detail"
      : "";
  let ProductShareUrl = `https://stagingbz.com/${slug}/${productName.replace(
    / /g,
    "-"
  )}?pid=${postId}`;
  let fbShareUrl = `https://www.facebook.com/sharer/sharer.php?u=${ProductShareUrl}&quote=I ♥ this product on ${APP_NAME}!`;

  console.log("URL is --> ", fbShareUrl, window.location);

  window.open(
    fbShareUrl,
    "_blank" // <- This is what makes it open in a new window.
  );
};
