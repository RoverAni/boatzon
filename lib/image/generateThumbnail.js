export const handleImageUpload = async (file) => {
  return new Promise(async (res, rej) => {
    const imageFile = file[0];
    try {
      let name = "abc.jpg";
      var myFile = new File([imageFile], name);
      thumbnailify(URL.createObjectURL(myFile), 100, (thumb) => {
        myFile.thumb = thumb.replace(/^data:image\/[a-z]+;base64,/, "");
        res(myFile);
      });
    } catch (error) {
      rej(error);
      console.log(error);
    }
  });
};

export const thumbnailify = (base64Image, targetSize, callback) => {
  var img = new Image();
  img.onload = function () {
    var width = img.width,
      height = img.height,
      canvas = document.createElement("canvas"),
      ctx = canvas.getContext("2d");
    canvas.width = canvas.height = targetSize;
    ctx.drawImage(
      img,
      width > height ? (width - height) / 2 : 0,
      height > width ? (height - width) / 2 : 0,
      width > height ? height : width,
      width > height ? height : width,
      0,
      0,
      targetSize,
      targetSize
    );
    callback(canvas.toDataURL("image/png"));
  };
  img.src = base64Image;
};
