// Moment Js Components
import moment from "moment";

export const formatDate = (date, format) => {
  return moment(date).format(format);
};

export const timestampTomomentizedDate = (timestampDate) => {
  return moment.unix(timestampDate);
};

export const momentizedDate = (date) => {
  return moment(date);
};

export const formatOrderDate = (date) => {
  return moment(date).format("Do MMM hh:mm a");
};

export const findDayAgo = (date) => {
  // console.log("DATE", date);
  return moment(date).fromNow();
};

export const getYearsList = () => {
  const years = [];
  const dateStart = moment();
  const dateEnd = moment().subtract(1000, "y");

  while (dateEnd.diff(dateStart, "years") <= 0) {
    years.push(dateStart.format("YYYY"));
    dateStart.subtract(1, "year");
  }
  return years;
};
